﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GraphQlClient.Core;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Newtonsoft.Json;
// using Shopify.Unity;
using System.Data;

public class ShopifyQuery : MonoBehaviour
{

    public Products productScript;


    public GraphApi srgReference;

    int imageHeight = 100;
    int imageWidth = 500;

    //string authToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJlYXQiOjE2MDIyODgwMDAsInN1Yl90eXBlIjoyLCJ0b2tlbl90eXBlIjoxLCJjb3JzIjpbImh0dHA6Ly81MC43MS41MC42MSJdLCJjaWQiOjEsImlhdCI6MTU5MDYwODM2Nywic3ViIjoiaWUwYXlxNDZkbG1iNWw2MGV4aTJjbzdobGxxc2tqdyIsInNpZCI6MTAwMTE2MTAyMiwiaXNzIjoiQkMifQ._oMQDlnQtTL1iGVf5I6KMu1RQM6mVhoCFZH7EOy-pr9PNgo5ajttvwTCvJGW9D3o63eO803epIQXuEs25-Hb3A";

    string authToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJFUzI1NiJ9.eyJlYXQiOjE5MjQ4MTkyMDAsInN1Yl90eXBlIjoyLCJ0b2tlbl90eXBlIjoxLCJjb3JzIjpbImh0dHA6Ly81MC43MS41MC42MSJdLCJjaWQiOjEsImlhdCI6MTU5NTI2ODU4MSwic3ViIjoidHF2ajluN3c3NXZibzkxMDh6cXZlcXVraWV1ZDJpdCIsInNpZCI6MTAwMTE2NDM0NywiaXNzIjoiQkMifQ.oj2hmPIB0nT0VCG5zCKLKXB-XYYTWJzWyI7yK443MoVpqFK6vEnT3uUqahE31XWQZ93T5FsHi2im6UI3BWxt3Q";

    string getProductsQuery;
   

    // Start is called before the first frame update
    void Start()
    {

        getProductsQuery = "query MyFirstQuery {  site {    settings {      storeName}products {      edges {        node {          name          defaultImage{    url (width:" + imageWidth + ", height:" + imageHeight + ")          }sku prices{    retailPrice    {        value        currencyCode            }    price    {        value        currencyCode            }}        }      }    }  }}";


        string accessToken = "a02638d1d2c2542762f80e11c82bc830";
        string shopDomain = "srg-special-reserve-games.myshopify.com";

        
        //string accessToken = "92f4b5208281ca4562e6c98f59dbaf53";
        //string shopDomain = "wiotestshop.myshopify.com";

        // Init only needs to be called once
        // ShopifyBuy.Init(accessToken, shopDomain);



        test();
        //testUnitySDK();
    }


    public async void test()
    {
        //getShop();
        getProducts();
        //getCustomerAcess("skillen@electricmonkmedia.com", "testpassword8");
        //string customerAccessToken = await getCustomerAcess("cjskillen@gmail.com", "testpassword");
        //createNewCustomer();
        //string checkoutId = await createCheckout();
        //associateCustomerCheckout(checkoutId, customerAccessToken);
    }

    public async void getShop()
    {
        UnityWebRequest request = await srgReference.Post("GetStore", GraphApi.Query.Type.Query);



        //Debug.Log(request.downloadHandler.text);
    }

    public async void getProducts()
    {
        //GraphApi.Query getProducts = srgReference.GetQueryByName("GetProducts", GraphApi.Query.Type.Query);
        GraphApi.Query getProducts = srgReference.GetQueryByName("getproducts", GraphApi.Query.Type.Query);

        //Converts the JSON object to an argument string and sets the queries argument
        //getProducts.SetArgs(new { first = 10 });

        //Performs Post request to server

        //UnityWebRequest request = await srgReference.Post(getProducts);



        UnityWebRequest request = await HttpHandler.PostAsync("https://specialreservegames.com/graphql", getProductsQuery, authToken);







        var n = SimpleJSON.JSON.Parse(request.downloadHandler.text);

       


        int prodCount = n["data"]["site"]["products"]["edges"].Count;

        List<BCProduct> prodList = new List<BCProduct>();

        for (int i = 0; i < prodCount; i++)
        {


            BCProduct currProd = new BCProduct();

            //var images = (List<Shopify.Unity.Image>)product.images();
            //string _imageSrc = images.First().transformedSrc("compact");

            //var variants = (List<Shopify.Unity.ProductVariant>)product.variants();


            currProd._imageSrc = n["data"]["site"]["products"]["edges"][i]["node"]["defaultImage"]["url"].Value;

            //Debug.Log("img url is " + n["data"]["site"]["products"]["edges"][i]["node"]["defaultImage"]["url"].Value);
            //productOnShelf.productImageSprite = await LoadSpriteAsync(_imageSrc);     // adjust this (This is the game case render)
            //currProd.productPlatform = Platform("product.platform");                      // adjust this (Check the functions for viable strings. Misc is for something like Fetch.)
            //productOnShelf.titleText01 =product.productType();                                        // adjust this (This is what is displayed as the top line on the home page and single games pages, and the second line on the platforms pages, like 'SWITCH RESERVE', 'SWITCH SINGLE' or 'ART BOOK'.)
            currProd.titleText01 = n["data"]["site"]["products"]["edges"][i]["node"]["name"].Value; // product.title();                                   // adjust this (This is the other line, the title of the game, like 'HOTLINE MIAMI COLLECTION' or 'DOWNWELL'.)
            currProd.description = n["data"]["site"]["products"]["edges"][i]["node"]["description"].Value;                                 // adjust this (The short description blurb.)
            currProd.price = n["data"]["site"]["products"]["edges"][i]["node"]["prices"]["price"]["value"].Value;//= 0.00f; // product.price                                      // adjust this (This is a float right now, change it to string if you need to.)
            currProd.available = true; // product.available;                              // adjust this (This should indicate if the product is available or sold out, or not on sale yet I guess. The "Available Now" shelf only has products that are available, but other shelves will be different.)
            currProd.linkDestination = Pages.PageLink("product.gamePageUrl");             // adjust this (This is a link to the page for that game and should be formatted matching Pages.PageLink(), for example "games-hlmc" or "games-etg". It matches the last bit of the url on the website.)







            prodList.Add(currProd);
        }

        string prodName = n["data"]["site"]["products"]["edges"][0]["node"]["name"].Value;

        var product = n["data"]["site"]["products"]["edges"][0]["node"].Value;

        //List<Product> products = request.downloadHandler.text //use json to convert the products to a list of objects

        productScript.CreateAvailableNowShelf(prodList);





        //Debug.Log(request.downloadHandler.text);
    }

    //Task<string>
    public async void getCustomerAcess(string email, string password)
    {
        GraphApi.Query custAccess = srgReference.GetQueryByName("loginCustomer", GraphApi.Query.Type.Mutation);

        //Converts the JSON object to an argument string and sets the queries argument
        //custAccess.SetArgs(new { input = new { email = email, password = password } });

        custAccess.SetArgs(new { email = email, password = password });

        //Performs Post request to server
        UnityWebRequest request = await srgReference.Post(custAccess);



        //need to deserialize json response into an object

        string response = request.downloadHandler.text;



        var n = SimpleJSON.JSON.Parse(request.downloadHandler.text);

        //string accessToken = n["data"]["customerAccessTokenCreate"]["customerAccessToken"]["accessToken"].Value;


        getCustomer();

        //return accessToken;
    }

    public async void getCustomer()
    {
        GraphApi.Query customer = srgReference.GetQueryByName("GetCustomer", GraphApi.Query.Type.Query);

        //Converts the JSON object to an argument string and sets the queries argument
        //customer.SetArgs(new { customerAccessToken = accessToken });


        //Performs Post request to server
        UnityWebRequest request = await srgReference.Post(customer);



    }

    public async void createNewCustomer(string email, string password)
    {
        GraphApi.Query createCustomer = srgReference.GetQueryByName("createNewCustomer", GraphApi.Query.Type.Mutation);

        //Converts the JSON object to an argument string and sets the queries argument
        createCustomer.SetArgs(new { input = new { email = email, password = password } });

        //Performs Post request to server
        UnityWebRequest request = await srgReference.Post(createCustomer);



    }

    public async Task<string> createCheckout()
    {
        GraphApi.Query checkout = srgReference.GetQueryByName("createCheckout", GraphApi.Query.Type.Mutation);

        //Converts the JSON object to an argument string and sets the queries argument
        //checkout.SetArgs(new { first = 5 });


        List<LineItem> lineItems = new List<LineItem>();
        LineItem lineItem1 = new LineItem();
        lineItem1.variantId = "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zMDc1NzUzMzYxNDEzNQ==";
        lineItem1.quantity = 1;

        lineItems.Add(lineItem1);

        //string jsonString = JsonConvert.SerializeObject(lineItems, Formatting.Indented);
        //Debug.Log(jsonString);


        checkout.SetArgs(new { input = new { lineItems } });

        //Performs Post request to server
        UnityWebRequest request = await srgReference.Post(checkout);

        var n = SimpleJSON.JSON.Parse(request.downloadHandler.text);
        string checkoutId = n["data"]["checkoutCreate"]["checkout"]["id"].Value;

        return checkoutId;


    }

    public async void associateCustomerCheckout(string checkoutId, string customerAccessToken)
    {
        // GraphApi.Query checkout = srgReference.GetQueryByName("associateCheckoutCustomer", GraphApi.Query.Type.Mutation);

        // //Converts the JSON object to an argument string and sets the queries argument
        // checkout.SetArgs(new { checkoutId = checkoutId, customerAccessToken = customerAccessToken });

        // var cart = ShopifyBuy.Client().Cart();


        // // Launches the web view checkout experience overlaid on top of your game.
        // cart.CheckoutWithWebView(
        //     success: () => {
        //         Debug.Log("User finished purchase/checkout!");
        //     },
        //     cancelled: () => {
        //         Debug.Log("User cancelled out of the web checkout.");
        //     },
        //     failure: (e) => {
        //         Debug.Log("Something bad happened - Error: " + e);
        //     }
        // );


        // //var testInput = new { checkoutId = checkoutId, customerAccessToken = customerAccessToken };

        // //string jsonString = JsonConvert.SerializeObject(testInput, Formatting.Indented);
        // //Debug.Log(jsonString);
        // //checkout.SetArgs(jsonString);



        // //Performs Post request to server
        // UnityWebRequest request = await srgReference.Post(checkout);


    }

    public async void testUnitySDK()
    {
        // //GetCustomerToken();
        // //AddItemsToCart();

        // //Queries one page of products
        // ShopifyBuy.Client().products((products, error, after) =>
        // {
        //     if (error != null)
        //     {
        //         Debug.Log(error.Description);

        //         switch (error.Type)
        //         {
        //             // An HTTP error is actually Unity's WWW.error
        //             case ShopifyError.ErrorType.HTTP:
        //                 break;
        //             // Although it's unlikely, an invalid GraphQL query might be sent.
        //             // Report an issue to https://github.com/shopify/unity-buy-sdk/issues
        //             case ShopifyError.ErrorType.GraphQL:
        //                 break;
        //         };
        //     }
        //     else
        //     {
        //         Debug.Log("Here is the first page of products:");

        //         productScript.CreateAvailableNowShelf(products);

        //         // products is a List<Product>
        //         foreach (Product product in products)
        //         {



        //             //Debug.Log("Product Title: " + product.title());
        //             //Debug.Log("Product Description: " + product.descriptionHtml());
        //             //Debug.Log("Product image url: " + product.images());
        //             //Debug.Log("--------");
        //         }

        //         if (after != null)
        //         {
        //             Debug.Log("Here is the second page of products:");

        //             // Queries second page of products, as after is passed
        //             ShopifyBuy.Client().products((products2, error2, after2) =>
        //             {
        //                 foreach (Product product in products2)
        //                 {
        //                     //Debug.Log("Product Title: " + product.title());
        //                     //Debug.Log("Product Description: " + product.descriptionHtml());
        //                     //Debug.Log("--------");
        //                 }
        //             }, after: after);
        //         }
        //         else
        //         {
        //             Debug.Log("There was only one page of products.");
        //         }
        //     }
        // });

        // //Cart cart = new Cart();
        // //cart.CheckoutWithWebView


    }

    // private static void AddItemsToCart()
    // {

    //     //ShopifyBuy.Client().products((products, error, after) => {
    //     //    Cart cart = ShopifyBuy.Client().Cart();

    //     //    List<ProductVariant> firstProductVariants = (List<ProductVariant>)products[0].variants();
    //     //    ProductVariant firstProductFirstVariant = firstProductVariants[0];

    //     //    // The following example adds a line item using the first products first variant.
    //     //    // In this case, the cart will have 3 copies of the variant.
    //     //    cart.LineItems.AddOrUpdate(firstProductFirstVariant, 1);

    //     //    // The following will output the variant id which was setup using the first product variant
    //     //    Debug.Log("First line item's variant id is: " + cart.LineItems.All()[0].VariantId);

    //     //    //cart.
    //     //});


    //     //checkout code

    //     ShopifyBuy.Client().products((products, error, after) =>
    //     {
    //         var cart = ShopifyBuy.Client().Cart();
    //         var firstProduct = products[0];
    //         var firstProductVariants = (List<ProductVariant>)firstProduct.variants();
    //         ProductVariant productVariantToCheckout = firstProductVariants[0];

    //         cart.LineItems.AddOrUpdate(productVariantToCheckout, 1);

    //         // Checkout with the url in the Device Browser
    //         cart.GetWebCheckoutLink(
    //             success: (link) =>
    //             {
    //                 Application.OpenURL(link);
    //             },
    //             failure: (checkoutError) =>
    //             {
    //                 Debug.Log(checkoutError.Description);
    //             }
    //         );




    //         var key = "BL9QiRljozDhgfyfVHoK+l1l98fBY0x/in0rCYJxmTfnzJDWsX1+8l4HEa4LO0WeKQlYtuk8zcJtzimTMhr1UL8=";


    //         // Check to see if the user can make a payment through Apple Pay
    //         //cart.CanCheckoutWithNativePay((isNativePayAvailable) =>
    //         //{
    //         //    if (isNativePayAvailable)
    //         //    {
    //         //        cart.CheckoutWithNativePay(
    //         //            key,
    //         //            success: () =>
    //         //            {
    //         //                Debug.Log("User finished purchase/checkout!");
    //         //            },
    //         //            cancelled: () =>
    //         //            {
    //         //                Debug.Log("User cancelled out of the native checkout.");
    //         //            },
    //         //            failure: (e) =>
    //         //            {
    //         //                Debug.Log("Something bad happened - Error: " + e);
    //         //            }
    //         //        );
    //         //    }
    //         //});

    //         // Launches the web view checkout experience overlaid on top of your game.
    //         //cart.CheckoutWithWebView(
    //         //    success: () =>
    //         //    {
    //         //        Debug.Log("User finished purchase/checkout!");
    //         //    },
    //         //    cancelled: () =>
    //         //    {
    //         //        Debug.Log("User cancelled out of the web checkout.");
    //         //    },
    //         //    failure: (e) =>
    //         //    {
    //         //        Debug.Log("Something bad happened - Error: " + e);
    //         //    }
    //         //);
    //     });
    // }


    // private static void GetCustomerToken()
    // {
    //     CustomerAccessTokenCreateInput tokenInput = new CustomerAccessTokenCreateInput("cjskillen@gmail.com", "testpassword");

    //     string token = "";

    //     ShopifyBuy.Client().Mutation(
    //         buildQuery: (query) => query
    //         .customerAccessTokenCreate(customerQuery => customerQuery
    //         .customerAccessToken(custAccessToken => custAccessToken
    //         .accessToken()
    //         ),
    //         tokenInput
    //             //.shop(shopQuery => shopQuery
    //             //.name()
    //             //.primaryDomain(primaryDomainQuery => primaryDomainQuery
    //             //    .url()
    //             //    .host()
    //             ),
    //          callback: (result, error) =>
    //          {
    //              // Results in a QueryRoot instance
    //              //Debug.Log("customer token : " + result.customerAccessTokenCreate().customerAccessToken().accessToken());
    //              token = result.customerAccessTokenCreate().customerAccessToken().accessToken();

    //              GetCustomerWithToken(token);
    //          }
    //         );
    // }

    // private static void GetCustomerWithToken(string accessToken)
    // {


    //     if (accessToken != "")
    //     {
    //         ShopifyBuy.Client().Query(
    //             buildQuery: (query) => query
    //             .customer(
    //                 customer => customer.displayName(),
    //                 accessToken
    //                 ),
    //             callback: (result, error) =>
    //             {
    //                 // Results in a QueryRoot instance
    //                 Debug.Log("customer displayname is : " + result.customer().displayName());
    //             }
    //             );


    //     }
    // }
}

public class LineItem
{
    public string variantId;
    public int quantity;
}

public class assocCredentials
{
    public string checkoutId;
    public string customerAccessToken;

    public assocCredentials(string checkoutId, string accessToken)
    {
        this.checkoutId = checkoutId;
        this.customerAccessToken = accessToken;
    }
}
