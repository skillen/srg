﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BCProduct
{
    public string _imageSrc;
    public Image BrokenImageIcon;
    public Image productImage;

    
    [HideInInspector] public Pages.PageName linkDestination;
    [HideInInspector] public Sprite productImageSprite;
    [HideInInspector] public Products.ProductPlatform productPlatform;
    [HideInInspector] public string titleText01;
    [HideInInspector] public string titleText02;
    [HideInInspector] public string price;
    [HideInInspector] public string description;
    [HideInInspector] public bool available;

}
