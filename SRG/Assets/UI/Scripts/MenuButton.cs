﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MenuButton : MonoBehaviour {
    public GameObject BgOverlay;
    public Pages.PageName linkDestination;

    public UnityEvent clickEvent;

    public bool isSelected { get; private set; }


    public void MenuButtonClick() {

        clickEvent.Invoke();
    }

    public void HighlightMenuBtn() {
        if (!isSelected) {
            isSelected = true;
        }
        BgOverlay.SetActive(true);

        MenuButtons.NewActiveMenuBtn(this);
        MenuButtons.UnselectLastMenuBtn();
    }

    public void SelectMenuBtn() {
        HighlightMenuBtn();
        Pages.OpenPage(linkDestination);
    }

    public void UnselectMenuBtn() {
        if (isSelected) {
            isSelected = false;
        }

        BgOverlay.SetActive(false);
    }

}
