﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class MenuButtons : MonoBehaviour {

    public static MenuButton lastActiveMenuBtn;
    public static MenuButton newActiveMenuBtn;

    [SerializeField] private List<MenuButton> AllMenuButtons;

    private void Awake() {
        AllMenuButtons = GetComponentsInChildren<MenuButton>().ToList();
    }

    private void Start() {
        MenuButton homeBtn = AllMenuButtons.Find(x => x.linkDestination == Pages.PageName.Home);
        homeBtn.HighlightMenuBtn();
    }

    public static void NewActiveMenuBtn(MenuButton button) {
        newActiveMenuBtn = button;
    }

    public static void UnselectLastMenuBtn() {

        if (lastActiveMenuBtn) {
            lastActiveMenuBtn.UnselectMenuBtn();            
        }
        lastActiveMenuBtn = newActiveMenuBtn;
    }
}
