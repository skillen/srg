﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


public class Pages : MonoBehaviour {

    public enum PageName {

        Home,

        Games,
        Switch,
        PS4,
        PC,
        Developers,
        Podcast,

        Account,
        Settings,
        Support,

        About,

        Cart,
        Login,
        Signup,

        GamesHlmc,
        GamesMyfriendpedro,
        GamesDownwell,
        GamesMwc,
        GamesEtg,
        GamesGris,
        GamesMessenger,
        GamesMinit,
        GamesAbsolver,
        GamesRuiner,
        GamesStrafe,
        GamesSw2,
        GamesFetch
    };

    private static List<Page> AllPages;


    private void Awake() {
        AllPages = GetComponentsInChildren<Page>(true).ToList();
    }

    private void Start() {
        InitAllPages();
        OpenPage(PageName.Home);
    }

    private void InitAllPages() {
        OpenAllPages();
        CloseAllPages();
    }

    private static void SetPage(Page page, bool active) {
        if (page != null) {
            page.gameObject.SetActive(active);
        }
    }

    public static void CloseAllPages() {
        foreach (Page page in AllPages) {
            SetPage(page, false);
        }
    }

    public static void OpenAllPages() {
        foreach (Page page in AllPages) {
            SetPage(page, true);
        }
    }

    public static void OpenPage(PageName pageName) {

        CloseAllPages();

        Page openedPage = AllPages.Find(x => x.pageName == pageName);

        if (openedPage != null) {
            SetPage(openedPage, true);
        }
        else {
            Page firstPage = AllPages[0];
            SetPage(firstPage, true);
        }
    }


    public static Pages.PageName PageLink(string linkDestination) {

        switch (linkDestination) {
            
            case "home":
                return Pages.PageName.Home;

            case "games":
                return Pages.PageName.Games;
            case "games-switch":
                return Pages.PageName.Switch;
            case "games-ps4":
                return Pages.PageName.PS4;
            case "games-pc":
                return Pages.PageName.PC;
            case "developer-spotlight":
                return Pages.PageName.Developers;
            case "podcast":
                return Pages.PageName.Podcast;

            case "account":
                return Pages.PageName.Account;
            case "settings":
                return Pages.PageName.Settings;
            case "support":
                return Pages.PageName.Support;

            case "about":
                return Pages.PageName.About;            

            case "games-hlmc":
                return Pages.PageName.GamesHlmc;
            case "games-myfriendpedro":
                return Pages.PageName.GamesMyfriendpedro;
            case "games-downwell":
                return Pages.PageName.GamesDownwell;
            case "games-mwc":
                return Pages.PageName.GamesMwc;
            case "games-etg":
                return Pages.PageName.GamesEtg;
            case "games-gris":
                return Pages.PageName.GamesGris;
            case "games-messenger":
                return Pages.PageName.GamesMessenger;
            case "games-minit":
                return Pages.PageName.GamesMinit;
            case "games-absolver":
                return Pages.PageName.GamesAbsolver;
            case "games-ruiner":
                return Pages.PageName.GamesRuiner;
            case "games-strafe":
                return Pages.PageName.GamesStrafe;
            case "games-sw2":
                return Pages.PageName.GamesSw2;
            case "games-fetch":
                return Pages.PageName.GamesFetch;

            default:
                return Pages.PageName.Home;
        }
    }


}
