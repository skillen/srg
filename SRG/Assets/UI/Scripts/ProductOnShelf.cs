﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
// using Shopify.Examples.Helpers;

public class ProductOnShelf : MonoBehaviour {

    
    public string _imageSrc;
    public Image BrokenImageIcon;
    public Image productImage;

    public Image productBannerBGColor;
    public TMP_Text titleTextLine01;
    public TMP_Text titleTextLine02;
    public TMP_Text descriptionText;
    public TMP_Text priceText;

    public RectTransform addToCartBtn;
    public RectTransform soldOutBtn;

    [HideInInspector] public Pages.PageName linkDestination;
    [HideInInspector] public Sprite productImageSprite;
    [HideInInspector] public Products.ProductPlatform productPlatform;
    [HideInInspector] public string titleText01;
    [HideInInspector] public string titleText02;
    [HideInInspector] public string price;
    [HideInInspector] public string description;
    [HideInInspector] public bool available;


    public void LoadProduct() {

        if (!string.IsNullOrEmpty(_imageSrc))
        {
            StartCoroutine(
                ImageHelper.AssignImage(
                    _imageSrc,
                    productImage,
                    BrokenImageIcon
                )
            );
        }

        productBannerBGColor.color = BannerColor();
        titleTextLine01.text = titleText01;
        titleTextLine02.text = titleText02;
        descriptionText.text = description;
        priceText.text = price;

        addToCartBtn.gameObject.SetActive(available);
        soldOutBtn.gameObject.SetActive(!available);        
    }

    private Color BannerColor() {

        switch (productPlatform) {
            case Products.ProductPlatform.Switch:
                return GlobalColors.ProductBannerSwitch;
            case Products.ProductPlatform.PS4:
                return GlobalColors.ProductBannerPS4;
            case Products.ProductPlatform.PC:
                return GlobalColors.ProductBannerPC;
            case Products.ProductPlatform.Misc:
                return GlobalColors.ProductBannerMisc;
            default:
                return Color.black;
        }
    }

    public void OpenGamePage() {
        Pages.OpenPage(linkDestination);
    }


}
