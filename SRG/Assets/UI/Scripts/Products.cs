﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
// using Shopify.Unity;
using UnityEngine.UI;
using System.Linq;

/*

HOW TO PUT PRODUCTS ON SHELVES

- There will be different shelves for different selections of products (for eaxmple all games for ps4, all products on sale, etc).
- Each shelf can be created the same way, but with a different RectTransform reference for the place it goes.
- Right now, there is an "Available Now" shelf that is supposed to show all products that are currently for sale.
- Call 'CreateAvailableNowShelf()' for this specific shelf location.

- Somehow get the products that you want to display on the shelf.
- I've called the input 'string[] products' for now to be able to format it, those will probably be your json strings.
- Look at 'CreateShelf()' for all the data that needs to be passed with each product and adjust the string names 

- If you open the "Product On Shelf" prefab, there is an "AddToCart" button with the "AddToCart" script on it. The function is already connected to the button click, but it's still empty.

*/


public class Products : MonoBehaviour {

    public UnityEngine.UI.Image ProductImage;

    public enum ProductPlatform { Switch, PS4, PC, Misc };


    [Header("Prefabs")]
    public GameObject productOnShelfPrefab;


    [Header("Shelves")]
    public RectTransform availableNowShelfContent;


    // Replace with real data!
    public async void CreateShelf(List<BCProduct> products, RectTransform shelf) {
        foreach (BCProduct product in products) {

            GameObject instance = Instantiate(productOnShelfPrefab, shelf);

            ProductOnShelf productOnShelf = instance.GetComponent<ProductOnShelf>();

            productOnShelf._imageSrc = product._imageSrc;
            //productOnShelf.productImageSprite = await LoadSpriteAsync("product.imagePath");     // adjust this (This is the game case render)
            productOnShelf.productPlatform = Platform("product.platform");                      // adjust this (Check the functions for viable strings. Misc is for something like Fetch.)
            productOnShelf.titleText01 = "product.type";                                        // adjust this (This is what is displayed as the top line on the home page and single games pages, and the second line on the platforms pages, like 'SWITCH RESERVE', 'SWITCH SINGLE' or 'ART BOOK'.)
            productOnShelf.titleText01 = product.titleText01;                                   // adjust this (This is the other line, the title of the game, like 'HOTLINE MIAMI COLLECTION' or 'DOWNWELL'.)
            productOnShelf.description = product.description;                                  // adjust this (The short description blurb.)
            productOnShelf.price = product.price;//0.00f; // product.price                             // adjust this (This is a float right now, change it to string if you need to.)
            productOnShelf.available = true; // product.available;                              // adjust this (This should indicate if the product is available or sold out, or not on sale yet I guess. The "Available Now" shelf only has products that are available, but other shelves will be different.)
            productOnShelf.linkDestination = Pages.PageLink("product.gamePageUrl");             // adjust this (This is a link to the page for that game and should be formatted matching Pages.PageLink(), for example "games-hlmc" or "games-etg". It matches the last bit of the url on the website.)

            productOnShelf.LoadProduct();
        }
    }

    //overloaded method to get products as the shopify sdk object
    // public async void CreateShelf(List<Shopify.Unity.Product> products, RectTransform shelf)
    // {
    //     foreach (Shopify.Unity.Product product in products)
    //     {


            


    //         GameObject instance = Instantiate(productOnShelfPrefab, shelf);

    //         ProductOnShelf productOnShelf = instance.GetComponent<ProductOnShelf>();


    //         var images = (List<Shopify.Unity.Image>)product.images();
    //         string _imageSrc = images.First().transformedSrc("compact");

    //         var variants = (List<Shopify.Unity.ProductVariant>)product.variants();


    //         productOnShelf._imageSrc = _imageSrc;
    //         //productOnShelf.productImageSprite = await LoadSpriteAsync(_imageSrc);     // adjust this (This is the game case render)
    //         productOnShelf.productPlatform = Platform("product.platform");                      // adjust this (Check the functions for viable strings. Misc is for something like Fetch.)
    //         //productOnShelf.titleText01 =product.productType();                                        // adjust this (This is what is displayed as the top line on the home page and single games pages, and the second line on the platforms pages, like 'SWITCH RESERVE', 'SWITCH SINGLE' or 'ART BOOK'.)
    //         productOnShelf.titleText01 = product.title();                                   // adjust this (This is the other line, the title of the game, like 'HOTLINE MIAMI COLLECTION' or 'DOWNWELL'.)
    //         productOnShelf.description = product.description();                                  // adjust this (The short description blurb.)
    //         productOnShelf.price = variants.First().priceV2().amount().ToString("C"); //= 0.00f; // product.price                                      // adjust this (This is a float right now, change it to string if you need to.)
    //         productOnShelf.available = true; // product.available;                              // adjust this (This should indicate if the product is available or sold out, or not on sale yet I guess. The "Available Now" shelf only has products that are available, but other shelves will be different.)
    //         productOnShelf.linkDestination = Pages.PageLink("product.gamePageUrl");             // adjust this (This is a link to the page for that game and should be formatted matching Pages.PageLink(), for example "games-hlmc" or "games-etg". It matches the last bit of the url on the website.)

    //         productOnShelf.LoadProduct();
    //     }
    // }


    private async Task<Sprite> LoadSpriteAsync(string path) {

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path)) {

            await uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError) {
                Debug.Log(uwr.error);
                return null;
            }
            else {
                Texture2D texture = DownloadHandlerTexture.GetContent(uwr);

                if (texture != null) {
                    return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                }
                else {
                    Debug.Log("No texture found" + "\n\n");
                    return null;
                }
            }
        }
    }

    private Products.ProductPlatform Platform(string platform) {
        switch (platform) {
            case "switch":
                return Products.ProductPlatform.Switch;
            case "ps4":
                return Products.ProductPlatform.PS4;
            case "pc":
                return Products.ProductPlatform.PC;
            case "misc":
                return Products.ProductPlatform.Misc;
            default:
                return Products.ProductPlatform.Misc;
        }
    }



    public void CreateAvailableNowShelf(List<BCProduct> products) {
        CreateShelf(products, availableNowShelfContent);
    }

    // public void CreateAvailableNowShelf(List<Shopify.Unity.Product> products)
    // {
    //     CreateShelf(products, availableNowShelfContent);
    // }


}
