﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateSidebar : MonoBehaviour {
    public RectTransform navigationPopup;
    public float animationTime;
    public AnimationCurve animationCurve;

    private RectTransform rectTransform;
    private bool animating = false;
    private float pivotY;

    private void Awake() {
        rectTransform = this.transform.GetComponent<RectTransform>();
        pivotY = rectTransform.pivot.y;
    }

    private void OnEnable() {
        rectTransform.pivot = new Vector2(1, pivotY);

        StartCoroutine(MoveIn(1, 0, animationCurve, animationTime));
    }

    private IEnumerator MoveIn(float startPos, float endPos, AnimationCurve curve, float time) {

        animating = true;
        float timer = 0.0f;

        while (rectTransform.pivot.x > 0) {

            rectTransform.pivot = new Vector2(Mathf.Lerp(startPos, endPos, curve.Evaluate(timer / time)), pivotY);
            timer += Time.deltaTime;
            yield return null;
        }

        rectTransform.pivot = new Vector2(0, pivotY);
        animating = false;

    }

    public void DisableSidebar() {

        if (this.gameObject.activeInHierarchy) {
            StartCoroutine(MoveOut(0, 1, animationCurve, animationTime));
        }
    }


    private IEnumerator MoveOut(float startPos, float endPos, AnimationCurve curve, float time) {

        animating = true;
        float timer = 0.0f;

        while (rectTransform.pivot.x < 1) {

            rectTransform.pivot = new Vector2(Mathf.Lerp(startPos, endPos, curve.Evaluate(timer / time)), pivotY);
            timer += Time.deltaTime;
            yield return null;
        }

        rectTransform.pivot = new Vector2(1, pivotY);
        animating = false;
        navigationPopup.gameObject.SetActive(false);
    }
}
