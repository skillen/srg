﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CartButton : MonoBehaviour {
    public Pages.PageName linkDestination;
    public TMP_Text shoppingCartNumber;

    [SerializeField] private int _itemsInCart;

    public int ItemsInCart {
        get {
            return _itemsInCart;
        }
        set {
            _itemsInCart = value;
            UpdateCartNumber();
        }
    }

    private void Awake() {
        linkDestination = Pages.PageName.Cart;
    }

    public void OpenCartPage() {
        Pages.OpenPage(linkDestination);
    }

    private void UpdateCartNumber() {
        shoppingCartNumber.text = ItemsInCart.ToString();
    }
}
