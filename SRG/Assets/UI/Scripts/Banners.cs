﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;


/*

HOW TO MAKE BANNERS

- Banner data needs to include:
    1)  a path to a hosted image as a string, 
    2)  the sub-page destination where it is supposed to lead as a string. 
        This string should match the page url from the homepage for example "games-switch" or "games-hlmc". 
        All viable strings can be found and edited under Pages.PageLink().

- Call the matching Create function for each banner:
    CreateHomeBanner for the big banner on top of the home page,
    CreateFeatureBannerOne and Two for the two featured banners on the home page.
    CreateGamesBannerOne and Two for the two banners on top of the games and platform pages.

*/


public class Banners : MonoBehaviour {

    [Header("Prefabs")]
    public GameObject bannerPrefab;
    

    [Header("Banner spots")]
    public RectTransform homeBannerSpot;
    public RectTransform featuredBannerSpot01;
    public RectTransform featuredBannerSpot02;

    public RectTransform[] gamesBannerSpot01;
    public RectTransform[] gamesBannerSpot02;


    public async void CreateBanner(string imagePath, string pageDestination, RectTransform targetSpot) {

        GameObject instance = Instantiate(bannerPrefab, targetSpot);

        FeaturedBanner newBanner = instance.GetComponent<FeaturedBanner>();
        newBanner.bannerSprite = await LoadSpriteAsync(imagePath);
        newBanner.linkDestination = Pages.PageLink(pageDestination);
        newBanner.LoadNewImage();

    }

    private async Task<Sprite> LoadSpriteAsync(string path) {

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(path)) {

            await uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError) {
                Debug.Log(uwr.error);
                return null;
            }
            else {
                Texture2D texture = DownloadHandlerTexture.GetContent(uwr);

                if (texture != null) {
                    return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));
                }
                else {
                    Debug.Log("No texture found" + "\n\n");
                    return null;
                }
            }
        }
    }    


    public void CreateHomeBanner(string imagePath, string pageDestination) {
        CreateBanner(imagePath, pageDestination, homeBannerSpot);
    }

    public void CreateFeatureBannerOne(string imagePath, string pageDestination) {
        CreateBanner(imagePath, pageDestination, featuredBannerSpot01);
    }
    public void CreateFeatureBannerTwo(string imagePath, string pageDestination) {
        CreateBanner(imagePath, pageDestination, featuredBannerSpot02);
    }

    public void CreateGamesBannerOne(string imagePath, string pageDestination) {
        foreach (RectTransform spot in gamesBannerSpot01) {
            CreateBanner(imagePath, pageDestination, spot);
        }
    }
    public void CreateGamesBannerTwo(string imagePath, string pageDestination) {
        foreach (RectTransform spot in gamesBannerSpot02) {
            CreateBanner(imagePath, pageDestination, spot);
        }
    }
}
