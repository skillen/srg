﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeaturedBanner : MonoBehaviour {

    public Sprite bannerSprite;
    public Pages.PageName linkDestination;

    public void LoadNewImage() {
        FitToFrame();
        SetSprite();
    }


    public void FeaturedBannerClick() {

        Pages.OpenPage(linkDestination);
    }

    private void FitToFrame() {

        float aspectRatio = (float)bannerSprite.texture.width / (float)bannerSprite.texture.height;
        GetComponent<AspectRatioFitter>().aspectRatio = aspectRatio;

    }

    private void SetSprite() {
        GetComponent<Image>().sprite = bannerSprite;
    }

}
