﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalColors : MonoBehaviour
{  


    public Color32 productBannerPS4;
    public Color32 productBannerSwitch;
    public Color32 productBannerPC;
    public Color32 productBannerMisc;


    public static GlobalColors instance;

    private void Awake() {
        instance = this;
    }

    public static Color32 ProductBannerPS4 { get {return instance.productBannerPS4;} }
    public static Color32 ProductBannerSwitch { get {return instance.productBannerSwitch;} }
    public static Color32 ProductBannerPC { get {return instance.productBannerPC;} }
    public static Color32 ProductBannerMisc { get {return instance.productBannerMisc;} }

}
