﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Runtime.CompilerServices.TaskAwaiter`1<System.Int32> AwaitExtensions::GetAwaiter(System.Diagnostics.Process)
extern void AwaitExtensions_GetAwaiter_mEB5AE2C73D25B24345A4D588CDCBEF94BB4CA15E ();
// 0x00000002 System.Void AwaitExtensions::WrapErrors(System.Threading.Tasks.Task)
extern void AwaitExtensions_WrapErrors_mFBFB40AD9AAAFCF83081E69CCA87616CC9821D5F ();
// 0x00000003 WaitForUpdate Awaiters::get_NextFrame()
extern void Awaiters_get_NextFrame_mC0045B33363C69E85449F8AE40E571ACC70DC8D8 ();
// 0x00000004 UnityEngine.WaitForFixedUpdate Awaiters::get_FixedUpdate()
extern void Awaiters_get_FixedUpdate_m8BCBF549EBD0AF13ECE1FECF4A2C79AAE4579D81 ();
// 0x00000005 UnityEngine.WaitForEndOfFrame Awaiters::get_EndOfFrame()
extern void Awaiters_get_EndOfFrame_m5364D9F3FE1F8DAD4FA87A75F5E1D15383328C6A ();
// 0x00000006 UnityEngine.WaitForSeconds Awaiters::Seconds(System.Single)
extern void Awaiters_Seconds_m61DBAF2693A28D3A2C83A42D4369EEAE69CCD54C ();
// 0x00000007 UnityEngine.WaitForSecondsRealtime Awaiters::SecondsRealtime(System.Single)
extern void Awaiters_SecondsRealtime_mEE429F9B5B5B53E14206A82A176F2C4E848589EF ();
// 0x00000008 UnityEngine.WaitUntil Awaiters::Until(System.Func`1<System.Boolean>)
extern void Awaiters_Until_m7E2357E3CD320C04963A56AFC7132EAF02E25539 ();
// 0x00000009 UnityEngine.WaitWhile Awaiters::While(System.Func`1<System.Boolean>)
extern void Awaiters_While_m833748B426DF71FF354F32E70DDCFAAF3FADEF97 ();
// 0x0000000A System.Void Awaiters::.cctor()
extern void Awaiters__cctor_mFE77E43D738720B120E3C5C55284F2636FAF2489 ();
// 0x0000000B IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitForSeconds)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m2BEED4D645673F85EC103A9B5EEF6975E9637F15 ();
// 0x0000000C IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(WaitForUpdate)
extern void IEnumeratorAwaitExtensions_GetAwaiter_mA5EA65A4F139EFCC2A10D65AF14C506B44579389 ();
// 0x0000000D IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitForEndOfFrame)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m2EA7BB7DBFD93B942EF7BB90274E985903281828 ();
// 0x0000000E IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitForFixedUpdate)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m3CB17DE27A3DFD0B0F1A44CBA67D4D88C28934E5 ();
// 0x0000000F IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitForSecondsRealtime)
extern void IEnumeratorAwaitExtensions_GetAwaiter_mB454EAC688413478004A568667AC4E054F58CAEB ();
// 0x00000010 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitUntil)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m1EC0F3217495FCB71CE3AE7CE8E7A340D00FD99C ();
// 0x00000011 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WaitWhile)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m39FE7248F563012B06F24B7BC73D5D48125F7AF1 ();
// 0x00000012 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.AsyncOperation> IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.AsyncOperation)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m5D22984838DC7F2E1EAD023BE380C04DC906BA49 ();
// 0x00000013 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.Object> IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.ResourceRequest)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m2E585282227F335C8E5F67CF23F1EA6130261564 ();
// 0x00000014 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.WWW> IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.WWW)
extern void IEnumeratorAwaitExtensions_GetAwaiter_mA6AE09D71E728A13EFDE9452550E5526E3C4CF35 ();
// 0x00000015 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle> IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.AssetBundleCreateRequest)
extern void IEnumeratorAwaitExtensions_GetAwaiter_mDE60FC25BE1AF26CB9DCF94067E6FD732C1D69D5 ();
// 0x00000016 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.Object> IEnumeratorAwaitExtensions::GetAwaiter(UnityEngine.AssetBundleRequest)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m3F4A75D0658DC0FAA949C3D8CED39AAA348D493F ();
// 0x00000017 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<T> IEnumeratorAwaitExtensions::GetAwaiter(System.Collections.Generic.IEnumerator`1<T>)
// 0x00000018 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<System.Object> IEnumeratorAwaitExtensions::GetAwaiter(System.Collections.IEnumerator)
extern void IEnumeratorAwaitExtensions_GetAwaiter_m9B07E9C5C9585A7FF75C8BAB0ABDCF5633671EAB ();
// 0x00000019 IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter IEnumeratorAwaitExtensions::GetAwaiterReturnVoid(System.Object)
extern void IEnumeratorAwaitExtensions_GetAwaiterReturnVoid_mEF080BDE52ACF40C8BB849D1512A7AAD82EB45E9 ();
// 0x0000001A IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<T> IEnumeratorAwaitExtensions::GetAwaiterReturnSelf(T)
// 0x0000001B System.Void IEnumeratorAwaitExtensions::RunOnUnityScheduler(System.Action)
extern void IEnumeratorAwaitExtensions_RunOnUnityScheduler_mA461FC93AF317E942631EA5BE6F66B49325D0627 ();
// 0x0000001C System.Void IEnumeratorAwaitExtensions::Assert(System.Boolean)
extern void IEnumeratorAwaitExtensions_Assert_mFFC6E10802D3E5D1F47ED44EE0F1EB0597C8AA70 ();
// 0x0000001D System.Collections.IEnumerator TaskExtensions::AsIEnumerator(System.Threading.Tasks.Task)
extern void TaskExtensions_AsIEnumerator_m4B9BB89111A0563378CF03DB6BF8CDC6DA427615 ();
// 0x0000001E System.Collections.Generic.IEnumerator`1<T> TaskExtensions::AsIEnumerator(System.Threading.Tasks.Task`1<T>)
// 0x0000001F System.Runtime.CompilerServices.ConfiguredTaskAwaitable_ConfiguredTaskAwaiter WaitForBackgroundThread::GetAwaiter()
extern void WaitForBackgroundThread_GetAwaiter_mF5BD1E3BD5C178764B625FE68646AF8EB137E54A ();
// 0x00000020 System.Void WaitForBackgroundThread::.ctor()
extern void WaitForBackgroundThread__ctor_mE10A7B31DBF2EC6EE3A8DEC0DB62AE1948F35895 ();
// 0x00000021 System.Boolean WaitForUpdate::get_keepWaiting()
extern void WaitForUpdate_get_keepWaiting_m4C7CA0781BEAF7150C7FDC1A341DD0376CDC80BE ();
// 0x00000022 System.Void WaitForUpdate::.ctor()
extern void WaitForUpdate__ctor_mDFC8505EED32E1C469098E50255B7EA48F78DEF8 ();
// 0x00000023 UnityAsyncAwaitUtil.AsyncCoroutineRunner UnityAsyncAwaitUtil.AsyncCoroutineRunner::get_Instance()
extern void AsyncCoroutineRunner_get_Instance_m10C8781EEBC413DC0A36AC657C1FA83403E356D1 ();
// 0x00000024 System.Void UnityAsyncAwaitUtil.AsyncCoroutineRunner::Awake()
extern void AsyncCoroutineRunner_Awake_mC1105A959BC9D30D51863CF75F6911A100C37538 ();
// 0x00000025 System.Void UnityAsyncAwaitUtil.AsyncCoroutineRunner::.ctor()
extern void AsyncCoroutineRunner__ctor_mC25E35AD4A13667C11EF5D12DED05F8357E428AF ();
// 0x00000026 System.Void UnityAsyncAwaitUtil.SyncContextUtil::Install()
extern void SyncContextUtil_Install_m3EAC7DA8913F51452AE29D484CE92A31CDF8483D ();
// 0x00000027 System.Int32 UnityAsyncAwaitUtil.SyncContextUtil::get_UnityThreadId()
extern void SyncContextUtil_get_UnityThreadId_mBC168363841AD3A4828965BD7BED03AE614382E6 ();
// 0x00000028 System.Void UnityAsyncAwaitUtil.SyncContextUtil::set_UnityThreadId(System.Int32)
extern void SyncContextUtil_set_UnityThreadId_mB8AE28551E6501FB6AC50B1B5C3720676E1CCE12 ();
// 0x00000029 System.Threading.SynchronizationContext UnityAsyncAwaitUtil.SyncContextUtil::get_UnitySynchronizationContext()
extern void SyncContextUtil_get_UnitySynchronizationContext_m94CE8EED7254D6F33856A22A3151454DE6702774 ();
// 0x0000002A System.Void UnityAsyncAwaitUtil.SyncContextUtil::set_UnitySynchronizationContext(System.Threading.SynchronizationContext)
extern void SyncContextUtil_set_UnitySynchronizationContext_m1CABD405BCA72B4FCBE214B401B2A7AC15279197 ();
// 0x0000002B System.Void UnityAsyncAwaitUtil.AsyncUtilTests::Awake()
extern void AsyncUtilTests_Awake_m21AA61E9AAAD7135D68BDF9FC6B3E61C70D9CD1D ();
// 0x0000002C System.Void UnityAsyncAwaitUtil.AsyncUtilTests::OnGUI()
extern void AsyncUtilTests_OnGUI_m53F1ABD1DFA79B289AED0A97DAA011CB003708D6 ();
// 0x0000002D System.Collections.IEnumerator UnityAsyncAwaitUtil.AsyncUtilTests::RunAsyncFromCoroutineTest()
extern void AsyncUtilTests_RunAsyncFromCoroutineTest_m27A9B32F58EAA46D404F2DD17E844179A984913D ();
// 0x0000002E System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunMultipleThreadsTestAsync()
extern void AsyncUtilTests_RunMultipleThreadsTestAsync_m6F17943A35FCD2D3F05F4738C33BD1B7BDBA06EE ();
// 0x0000002F System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunMultipleThreadsTestAsyncWait()
extern void AsyncUtilTests_RunMultipleThreadsTestAsyncWait_mB53C41E8E793C297E058358AABC2328230D30462 ();
// 0x00000030 System.Void UnityAsyncAwaitUtil.AsyncUtilTests::PrintCurrentThreadContext(System.String)
extern void AsyncUtilTests_PrintCurrentThreadContext_m1FDC0DEB11D9D309B441F340F6AE2522B3262DA6 ();
// 0x00000031 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunAsyncFromCoroutineTest2()
extern void AsyncUtilTests_RunAsyncFromCoroutineTest2_m700D36F0D61D233EE2A134127F5A30D1D4F6A46C ();
// 0x00000032 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunWwwAsync()
extern void AsyncUtilTests_RunWwwAsync_m70F49593D14C1A62C79EB0A65606CC13284C23AA ();
// 0x00000033 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunOpenNotepadTestAsync()
extern void AsyncUtilTests_RunOpenNotepadTestAsync_m61A9BD7B304F2139B967506120AB462E309405B6 ();
// 0x00000034 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunUnhandledExceptionTestAsync()
extern void AsyncUtilTests_RunUnhandledExceptionTestAsync_mC0070DD1B7E4F5510B3F707595C32D05740C400E ();
// 0x00000035 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunTryCatchExceptionTestAsync()
extern void AsyncUtilTests_RunTryCatchExceptionTestAsync_m2D85BF1C754E0F8C822491AF0155DAB3839AE4D2 ();
// 0x00000036 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::NestedRunAsync()
extern void AsyncUtilTests_NestedRunAsync_mFB457D3D35245784C12D3016FCA94E71180CEBC7 ();
// 0x00000037 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::WaitThenThrowException()
extern void AsyncUtilTests_WaitThenThrowException_mE17D33EFE92783D57044AA4F0583D0C443677382 ();
// 0x00000038 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunAsyncOperationAsync()
extern void AsyncUtilTests_RunAsyncOperationAsync_mCA01B206181205A4B3054D32D6B586E7107DC06C ();
// 0x00000039 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::InstantiateAssetBundleAsync(System.String,System.String)
extern void AsyncUtilTests_InstantiateAssetBundleAsync_m7C4B8B8F1657956FA63EE45DD2B264D3113C965B ();
// 0x0000003A System.Threading.Tasks.Task`1<System.Byte[]> UnityAsyncAwaitUtil.AsyncUtilTests::DownloadRawDataAsync(System.String)
extern void AsyncUtilTests_DownloadRawDataAsync_mAF29295131E857AED3BC915F454F4F324340883A ();
// 0x0000003B System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunIEnumeratorTryCatchExceptionAsync()
extern void AsyncUtilTests_RunIEnumeratorTryCatchExceptionAsync_m7A218917382A96607B3B2DA64CB8737D12BC8CC8 ();
// 0x0000003C System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunIEnumeratorUnhandledExceptionAsync()
extern void AsyncUtilTests_RunIEnumeratorUnhandledExceptionAsync_m8E8DA8ECC75966CE8A9DCC5AD7F745CA1DB4C6BD ();
// 0x0000003D System.Collections.IEnumerator UnityAsyncAwaitUtil.AsyncUtilTests::WaitThenThrow()
extern void AsyncUtilTests_WaitThenThrow_mF52CB2814639A70A32F19D5C662E0540090BB660 ();
// 0x0000003E System.Collections.IEnumerator UnityAsyncAwaitUtil.AsyncUtilTests::WaitThenThrowNested()
extern void AsyncUtilTests_WaitThenThrowNested_mFBC0155CBFDE00E1B6DE4EB77C24DD18FE396762 ();
// 0x0000003F System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunIEnumeratorStringTestAsync()
extern void AsyncUtilTests_RunIEnumeratorStringTestAsync_m55DF8F978287A49008F9FD79C71762254271BD3A ();
// 0x00000040 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunIEnumeratorUntypedStringTestAsync()
extern void AsyncUtilTests_RunIEnumeratorUntypedStringTestAsync_m8CF7DD1A44183DBF3DD16E7533FBD41E8BA4FC5A ();
// 0x00000041 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunIEnumeratorTestAsync()
extern void AsyncUtilTests_RunIEnumeratorTestAsync_m516218BC3F406B2E6427904020021D6AD4A16158 ();
// 0x00000042 System.Collections.Generic.IEnumerator`1<System.String> UnityAsyncAwaitUtil.AsyncUtilTests::WaitForString()
extern void AsyncUtilTests_WaitForString_mC45ABEE57F5344AA7E7B522B0990D54C6E81DD33 ();
// 0x00000043 System.Collections.IEnumerator UnityAsyncAwaitUtil.AsyncUtilTests::WaitForStringUntyped()
extern void AsyncUtilTests_WaitForStringUntyped_m8E7C86489F348662566C6AC6FECCCCC1CF7EB1BC ();
// 0x00000044 System.Collections.IEnumerator UnityAsyncAwaitUtil.AsyncUtilTests::WaitABit()
extern void AsyncUtilTests_WaitABit_mAECFD702A0C6FBD072021975611F80F90F89E150 ();
// 0x00000045 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunReturnValueTestAsync()
extern void AsyncUtilTests_RunReturnValueTestAsync_mE8BF4B8CF8A6BDECE2F0877F0251E536FFCCC3DC ();
// 0x00000046 System.Threading.Tasks.Task`1<System.String> UnityAsyncAwaitUtil.AsyncUtilTests::GetValueExampleAsync()
extern void AsyncUtilTests_GetValueExampleAsync_mD96231290DF15DCA6BBD874319523076B258C08C ();
// 0x00000047 System.Threading.Tasks.Task UnityAsyncAwaitUtil.AsyncUtilTests::RunAwaitSecondsTestAsync()
extern void AsyncUtilTests_RunAwaitSecondsTestAsync_mE1DB2FADA79D7F2F5D44E91671D669AF019B0799 ();
// 0x00000048 System.Void UnityAsyncAwaitUtil.AsyncUtilTests::.ctor()
extern void AsyncUtilTests__ctor_mEB13DF37F40658334436A2622C67E004F09E5B15 ();
// 0x00000049 System.Void UnityAsyncAwaitUtil.TestButtonHandler::.ctor(UnityAsyncAwaitUtil.TestButtonHandler_Settings)
extern void TestButtonHandler__ctor_m9CE4FEB9DEB61A078E78411CD26E31A5606C5523 ();
// 0x0000004A System.Void UnityAsyncAwaitUtil.TestButtonHandler::Restart()
extern void TestButtonHandler_Restart_mB597A8AE47F465AAA2F01DAA41E3B78E98F9494A ();
// 0x0000004B System.Boolean UnityAsyncAwaitUtil.TestButtonHandler::Display(System.String)
extern void TestButtonHandler_Display_m85804E5AD6396E4DBBB6B51E32CC2A493384B8C6 ();
// 0x0000004C System.Void AwaitExtensions_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mA53D0AB5E685E966B6D76EAC5D1152ABCB9BE4BE ();
// 0x0000004D System.Void AwaitExtensions_<>c__DisplayClass0_0::<GetAwaiter>b__0(System.Object,System.EventArgs)
extern void U3CU3Ec__DisplayClass0_0_U3CGetAwaiterU3Eb__0_m0F070ED79DBC51EFB1CE5B79A8DD06AC4B85D1C9 ();
// 0x0000004E System.Void AwaitExtensions_<WrapErrors>d__1::MoveNext()
extern void U3CWrapErrorsU3Ed__1_MoveNext_m28A5E4348528630DBD297B485DC0E71F228F1175_AdjustorThunk ();
// 0x0000004F System.Void AwaitExtensions_<WrapErrors>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWrapErrorsU3Ed__1_SetStateMachine_m44A965410412BCEBC07EAED23CDB56EC0924F167_AdjustorThunk ();
// 0x00000050 System.Boolean IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1::get_IsCompleted()
// 0x00000051 T IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1::GetResult()
// 0x00000052 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1::Complete(T,System.Exception)
// 0x00000053 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
// 0x00000054 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1::.ctor()
// 0x00000055 System.Boolean IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter::get_IsCompleted()
extern void SimpleCoroutineAwaiter_get_IsCompleted_m2B7B75FC9B63334B7EA1200CFE80E655E958C236 ();
// 0x00000056 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter::GetResult()
extern void SimpleCoroutineAwaiter_GetResult_m2A63D7E8F28F2E51B505DD39F7C7A43A442483FD ();
// 0x00000057 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter::Complete(System.Exception)
extern void SimpleCoroutineAwaiter_Complete_m62AD5283DC9F88FE6060505D36A56454A3C23520 ();
// 0x00000058 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
extern void SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_mA6D30E4C3F26EF5FCB8603686CB374E72D9F5F65 ();
// 0x00000059 System.Void IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter::.ctor()
extern void SimpleCoroutineAwaiter__ctor_m042B984144C5A0B0A8DE8C38CDBE2928533207E5 ();
// 0x0000005A System.Void IEnumeratorAwaitExtensions_CoroutineWrapper`1::.ctor(System.Collections.IEnumerator,IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<T>)
// 0x0000005B System.Collections.IEnumerator IEnumeratorAwaitExtensions_CoroutineWrapper`1::Run()
// 0x0000005C System.String IEnumeratorAwaitExtensions_CoroutineWrapper`1::GenerateObjectTraceMessage(System.Collections.Generic.List`1<System.Type>)
// 0x0000005D System.Collections.Generic.List`1<System.Type> IEnumeratorAwaitExtensions_CoroutineWrapper`1::GenerateObjectTrace(System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerator>)
// 0x0000005E System.Collections.IEnumerator IEnumeratorAwaitExtensions_InstructionWrappers::ReturnVoid(IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter,System.Object)
extern void InstructionWrappers_ReturnVoid_m31C71330CED1E793FBDB4204BD367DFD2695C841 ();
// 0x0000005F System.Collections.IEnumerator IEnumeratorAwaitExtensions_InstructionWrappers::AssetBundleCreateRequest(IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle>,UnityEngine.AssetBundleCreateRequest)
extern void InstructionWrappers_AssetBundleCreateRequest_m1E199C596923F84CC063194513ABC329A47CAE5A ();
// 0x00000060 System.Collections.IEnumerator IEnumeratorAwaitExtensions_InstructionWrappers::ReturnSelf(IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<T>,T)
// 0x00000061 System.Collections.IEnumerator IEnumeratorAwaitExtensions_InstructionWrappers::AssetBundleRequest(IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.AssetBundleRequest)
extern void InstructionWrappers_AssetBundleRequest_m5F935DD5B5A1AF34B4155E3254DE55BB9D9C9E5D ();
// 0x00000062 System.Collections.IEnumerator IEnumeratorAwaitExtensions_InstructionWrappers::ResourceRequest(IEnumeratorAwaitExtensions_SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.ResourceRequest)
extern void InstructionWrappers_ResourceRequest_m7AA605E8A9CC3F28AA569588C39F38D43F7EA050 ();
// 0x00000063 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mFDB0D299BD058859BA71EB45C03CCC524637718B ();
// 0x00000064 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass8_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_m21AB2097D1F45AC1575F9F645C522E28B6B373C4 ();
// 0x00000065 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m87F4DAD414E64AC51610C818F5C3B28F7E79C2FB ();
// 0x00000066 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass10_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_mC84B8E2D02E8BD2EC2313D12E2FE671AFD2AB5C3 ();
// 0x00000067 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mA54925FED62FDB89BE6A1F261BE6298B1FD78258 ();
// 0x00000068 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass11_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CGetAwaiterU3Eb__0_m65F70E5864CA0A121A24456E2C0FF71D70A614D4 ();
// 0x00000069 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass12_0`1::.ctor()
// 0x0000006A System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass12_0`1::<GetAwaiter>b__0()
// 0x0000006B System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m610E3564F8DA6F6CB396FE01965C4B86B3A53683 ();
// 0x0000006C System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass13_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CGetAwaiterU3Eb__0_m4DEC856324DB7738F212B25D67858ADEB3A352F9 ();
// 0x0000006D System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m0A8E8B42D66541FB32664CB4888E2AA280246C27 ();
// 0x0000006E System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass14_0::<GetAwaiterReturnVoid>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CGetAwaiterReturnVoidU3Eb__0_m02E1717BD7F638835E5448D3FA5EDF4D75A44529 ();
// 0x0000006F System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass15_0`1::.ctor()
// 0x00000070 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass15_0`1::<GetAwaiterReturnSelf>b__0()
// 0x00000071 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mC7CA923290123DF40F42A90C626E3A4FB8D04A1F ();
// 0x00000072 System.Void IEnumeratorAwaitExtensions_<>c__DisplayClass16_0::<RunOnUnityScheduler>b__0(System.Object)
extern void U3CU3Ec__DisplayClass16_0_U3CRunOnUnitySchedulerU3Eb__0_mDD374EB0525A2314DAF64FFC9AECAA6937B63E97 ();
// 0x00000073 System.Void TaskExtensions_<AsIEnumerator>d__0::.ctor(System.Int32)
extern void U3CAsIEnumeratorU3Ed__0__ctor_mFBCE5616F85D85D5A7B7E6F983395678340EBC13 ();
// 0x00000074 System.Void TaskExtensions_<AsIEnumerator>d__0::System.IDisposable.Dispose()
extern void U3CAsIEnumeratorU3Ed__0_System_IDisposable_Dispose_m2805C396CB241787EE436BEA19A7C15D7410C2B3 ();
// 0x00000075 System.Boolean TaskExtensions_<AsIEnumerator>d__0::MoveNext()
extern void U3CAsIEnumeratorU3Ed__0_MoveNext_m5B51F5EEE1589D760928BC985E48EAF761840894 ();
// 0x00000076 System.Object TaskExtensions_<AsIEnumerator>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAsIEnumeratorU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF9391F073EEDE12A8DE53F76049D67D951EEA2D ();
// 0x00000077 System.Void TaskExtensions_<AsIEnumerator>d__0::System.Collections.IEnumerator.Reset()
extern void U3CAsIEnumeratorU3Ed__0_System_Collections_IEnumerator_Reset_mC96D57C0B356F9B6D52B31F468CABEEB089A61C4 ();
// 0x00000078 System.Object TaskExtensions_<AsIEnumerator>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CAsIEnumeratorU3Ed__0_System_Collections_IEnumerator_get_Current_m11801BAF812DB0406F608595637853B55244D1EC ();
// 0x00000079 System.Void TaskExtensions_<AsIEnumerator>d__1`1::.ctor(System.Int32)
// 0x0000007A System.Void TaskExtensions_<AsIEnumerator>d__1`1::System.IDisposable.Dispose()
// 0x0000007B System.Boolean TaskExtensions_<AsIEnumerator>d__1`1::MoveNext()
// 0x0000007C T TaskExtensions_<AsIEnumerator>d__1`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000007D System.Void TaskExtensions_<AsIEnumerator>d__1`1::System.Collections.IEnumerator.Reset()
// 0x0000007E System.Object TaskExtensions_<AsIEnumerator>d__1`1::System.Collections.IEnumerator.get_Current()
// 0x0000007F System.Void WaitForBackgroundThread_<>c::.cctor()
extern void U3CU3Ec__cctor_mF7F6BA3E6CC1EE24BFA38659F7BC11BBC21A43F0 ();
// 0x00000080 System.Void WaitForBackgroundThread_<>c::.ctor()
extern void U3CU3Ec__ctor_m6075E0522FAA899F43AFF716AB7D9276A07D1938 ();
// 0x00000081 System.Void WaitForBackgroundThread_<>c::<GetAwaiter>b__0_0()
extern void U3CU3Ec_U3CGetAwaiterU3Eb__0_0_mA923BFB5CDC378130FD6C0A028B56279942E22B4 ();
// 0x00000082 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::.ctor(System.Int32)
extern void U3CRunAsyncFromCoroutineTestU3Ed__6__ctor_mAE29AD40F69EFC8CF17CAA6521E052B7ECB4DBFA ();
// 0x00000083 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::System.IDisposable.Dispose()
extern void U3CRunAsyncFromCoroutineTestU3Ed__6_System_IDisposable_Dispose_m62FEA88182669E3BEBB04E3E370B7A98BDA65834 ();
// 0x00000084 System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::MoveNext()
extern void U3CRunAsyncFromCoroutineTestU3Ed__6_MoveNext_m065E8197E9B04B7B54DB65F20C58FFD6F7201C64 ();
// 0x00000085 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m583A6409E02200B879658557D45253501FE53CB0 ();
// 0x00000086 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::System.Collections.IEnumerator.Reset()
extern void U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_IEnumerator_Reset_m172A85AD1BD6AE45E58C417C5C822DD0E7285B08 ();
// 0x00000087 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_IEnumerator_get_Current_m079AD724E8C58063AF8262729C9A68E37895CA02 ();
// 0x00000088 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunMultipleThreadsTestAsync>d__7::MoveNext()
extern void U3CRunMultipleThreadsTestAsyncU3Ed__7_MoveNext_m315DF10B94E2BE226498A5D8D57BF2A64746B4E4_AdjustorThunk ();
// 0x00000089 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunMultipleThreadsTestAsync>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunMultipleThreadsTestAsyncU3Ed__7_SetStateMachine_m73F2DC70E4C26D11FA5D126F6475EA4C997B140B_AdjustorThunk ();
// 0x0000008A System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunMultipleThreadsTestAsyncWait>d__8::MoveNext()
extern void U3CRunMultipleThreadsTestAsyncWaitU3Ed__8_MoveNext_m696D99A52B1758C36F0ACF67ACFD248A4B105F65_AdjustorThunk ();
// 0x0000008B System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunMultipleThreadsTestAsyncWait>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunMultipleThreadsTestAsyncWaitU3Ed__8_SetStateMachine_mD443C3FAB919433B779492862D696A34CABAA7F0_AdjustorThunk ();
// 0x0000008C System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest2>d__10::MoveNext()
extern void U3CRunAsyncFromCoroutineTest2U3Ed__10_MoveNext_m498BF5D49DDF8EFCFF4ED3C1132BB29C59D53A10_AdjustorThunk ();
// 0x0000008D System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncFromCoroutineTest2>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunAsyncFromCoroutineTest2U3Ed__10_SetStateMachine_m804FA88575B02A39FA9AD642EE5CA006C347ED5E_AdjustorThunk ();
// 0x0000008E System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunWwwAsync>d__11::MoveNext()
extern void U3CRunWwwAsyncU3Ed__11_MoveNext_mDEA1D22871D2CBFCB45556681608ED7F61C326A2_AdjustorThunk ();
// 0x0000008F System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunWwwAsync>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunWwwAsyncU3Ed__11_SetStateMachine_mF070E762270D62251A6E64A029D3EEEBBE7663B4_AdjustorThunk ();
// 0x00000090 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunOpenNotepadTestAsync>d__12::MoveNext()
extern void U3CRunOpenNotepadTestAsyncU3Ed__12_MoveNext_m213E029BC0E07B8A8ABFC3458C256412DB14519E_AdjustorThunk ();
// 0x00000091 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunOpenNotepadTestAsync>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunOpenNotepadTestAsyncU3Ed__12_SetStateMachine_mB9AF7C0ADD7E760EDAAB8B434C3E9845CBE540EC_AdjustorThunk ();
// 0x00000092 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunUnhandledExceptionTestAsync>d__13::MoveNext()
extern void U3CRunUnhandledExceptionTestAsyncU3Ed__13_MoveNext_mD537D08F75F1C1B3001FF9ECD55B720CB299251D_AdjustorThunk ();
// 0x00000093 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunUnhandledExceptionTestAsync>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunUnhandledExceptionTestAsyncU3Ed__13_SetStateMachine_mC5464FD2F49DED13665C3C09267C4820C394B233_AdjustorThunk ();
// 0x00000094 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunTryCatchExceptionTestAsync>d__14::MoveNext()
extern void U3CRunTryCatchExceptionTestAsyncU3Ed__14_MoveNext_m4E061A1FF4BFAB1A270C17CF31A1A3CE398C06C7_AdjustorThunk ();
// 0x00000095 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunTryCatchExceptionTestAsync>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunTryCatchExceptionTestAsyncU3Ed__14_SetStateMachine_mA5A73C25618674118CADEF0103D8661A5F1BD193_AdjustorThunk ();
// 0x00000096 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<NestedRunAsync>d__15::MoveNext()
extern void U3CNestedRunAsyncU3Ed__15_MoveNext_m514891EE85ACA531508102DFBB30503D89828805_AdjustorThunk ();
// 0x00000097 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<NestedRunAsync>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CNestedRunAsyncU3Ed__15_SetStateMachine_m669EE53E27F124C402DD86F58B2B967F362B4AF8_AdjustorThunk ();
// 0x00000098 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowException>d__16::MoveNext()
extern void U3CWaitThenThrowExceptionU3Ed__16_MoveNext_m9BA1E3232E1FD567C805E6854602C8F41AD3D646_AdjustorThunk ();
// 0x00000099 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowException>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWaitThenThrowExceptionU3Ed__16_SetStateMachine_m210ED9D7A16E9D414A12D8D47DB5646B470025E0_AdjustorThunk ();
// 0x0000009A System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncOperationAsync>d__17::MoveNext()
extern void U3CRunAsyncOperationAsyncU3Ed__17_MoveNext_m2E60483AF901B7662434D3BE0F79E7798BEFC5D3_AdjustorThunk ();
// 0x0000009B System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAsyncOperationAsync>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunAsyncOperationAsyncU3Ed__17_SetStateMachine_mD8584D4C52423453794C6AD5C570D0D0536A6311_AdjustorThunk ();
// 0x0000009C System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<InstantiateAssetBundleAsync>d__18::MoveNext()
extern void U3CInstantiateAssetBundleAsyncU3Ed__18_MoveNext_m7664CD755804EF86E20CB3FCD9620FAA3A4EAE85_AdjustorThunk ();
// 0x0000009D System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<InstantiateAssetBundleAsync>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CInstantiateAssetBundleAsyncU3Ed__18_SetStateMachine_m995163F9BE843EA04645BD8F5B014F426B7F2729_AdjustorThunk ();
// 0x0000009E System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<DownloadRawDataAsync>d__19::MoveNext()
extern void U3CDownloadRawDataAsyncU3Ed__19_MoveNext_m73BD027862760F31034899377BDADB20B2FD0BE0_AdjustorThunk ();
// 0x0000009F System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<DownloadRawDataAsync>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CDownloadRawDataAsyncU3Ed__19_SetStateMachine_m6F58682586BA9C7E18A069D057ACC6F77B4A0DBF_AdjustorThunk ();
// 0x000000A0 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorTryCatchExceptionAsync>d__20::MoveNext()
extern void U3CRunIEnumeratorTryCatchExceptionAsyncU3Ed__20_MoveNext_m164E2925C1C569D092B6913A6641A8151C61632A_AdjustorThunk ();
// 0x000000A1 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorTryCatchExceptionAsync>d__20::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunIEnumeratorTryCatchExceptionAsyncU3Ed__20_SetStateMachine_mAB4BB6853840D54CCE570DE75BE835A6B8D8B6C9_AdjustorThunk ();
// 0x000000A2 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorUnhandledExceptionAsync>d__21::MoveNext()
extern void U3CRunIEnumeratorUnhandledExceptionAsyncU3Ed__21_MoveNext_m2ED054D50EF1A3385B6F935D67279B56415F7076_AdjustorThunk ();
// 0x000000A3 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorUnhandledExceptionAsync>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunIEnumeratorUnhandledExceptionAsyncU3Ed__21_SetStateMachine_m99C4A897EF4108F468CE7F941B7A5704FD0E4FAE_AdjustorThunk ();
// 0x000000A4 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::.ctor(System.Int32)
extern void U3CWaitThenThrowU3Ed__22__ctor_m9E30D3AF207A9061C15B4018FE098623F3233DFD ();
// 0x000000A5 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::System.IDisposable.Dispose()
extern void U3CWaitThenThrowU3Ed__22_System_IDisposable_Dispose_m13B61B824F5C887499AB2EBC4E12C88BF5D7BB7B ();
// 0x000000A6 System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::MoveNext()
extern void U3CWaitThenThrowU3Ed__22_MoveNext_m4892E13BB3660288A7D620611F13825F3FF13B94 ();
// 0x000000A7 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitThenThrowU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8704A604FBD336DA5E922AF69FA2182D1E672102 ();
// 0x000000A8 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::System.Collections.IEnumerator.Reset()
extern void U3CWaitThenThrowU3Ed__22_System_Collections_IEnumerator_Reset_m5911D60740F4B9EA897D34270799DFC809B6674A ();
// 0x000000A9 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrow>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CWaitThenThrowU3Ed__22_System_Collections_IEnumerator_get_Current_m234395CE73855C058405AF462367E50724CF2CB7 ();
// 0x000000AA System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::.ctor(System.Int32)
extern void U3CWaitThenThrowNestedU3Ed__23__ctor_mD4BC360B97C8E071AF785A22BBB5817437903BDE ();
// 0x000000AB System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::System.IDisposable.Dispose()
extern void U3CWaitThenThrowNestedU3Ed__23_System_IDisposable_Dispose_m9DFC1526C573C856FF49DBA9B876528914F9F9C3 ();
// 0x000000AC System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::MoveNext()
extern void U3CWaitThenThrowNestedU3Ed__23_MoveNext_m06BAA010BE797AA78AE299580811CD1D3F29D0BE ();
// 0x000000AD System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitThenThrowNestedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4DA5260EEEF1863DF5BE0CC9F45224629798A7B ();
// 0x000000AE System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitThenThrowNestedU3Ed__23_System_Collections_IEnumerator_Reset_m6B79B508642FF019D343D72D9F36847181880964 ();
// 0x000000AF System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitThenThrowNested>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitThenThrowNestedU3Ed__23_System_Collections_IEnumerator_get_Current_m1D6C70D2DA332C143476BEFEC90387147C73601F ();
// 0x000000B0 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorStringTestAsync>d__24::MoveNext()
extern void U3CRunIEnumeratorStringTestAsyncU3Ed__24_MoveNext_m653612C71AEC1175D6806811A2BEE4F84DAA554D_AdjustorThunk ();
// 0x000000B1 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorStringTestAsync>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunIEnumeratorStringTestAsyncU3Ed__24_SetStateMachine_m3CD16412FFDB097A36AE72AE8540FDA72716901F_AdjustorThunk ();
// 0x000000B2 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorUntypedStringTestAsync>d__25::MoveNext()
extern void U3CRunIEnumeratorUntypedStringTestAsyncU3Ed__25_MoveNext_m878D445B982B95E58337A4E67A35FDBDE62A3B94_AdjustorThunk ();
// 0x000000B3 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorUntypedStringTestAsync>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunIEnumeratorUntypedStringTestAsyncU3Ed__25_SetStateMachine_mC5030676D1C58527C112A20A690991DC9B0B4705_AdjustorThunk ();
// 0x000000B4 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorTestAsync>d__26::MoveNext()
extern void U3CRunIEnumeratorTestAsyncU3Ed__26_MoveNext_m59AA8544DCA07BF6BEC7F12998DCCFEF32837876_AdjustorThunk ();
// 0x000000B5 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunIEnumeratorTestAsync>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunIEnumeratorTestAsyncU3Ed__26_SetStateMachine_m2E0CC454CEA207ECDDAD0E3E3C8496C5F0D7A7C1_AdjustorThunk ();
// 0x000000B6 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::.ctor(System.Int32)
extern void U3CWaitForStringU3Ed__27__ctor_mC0258D2F8D6B378B318EA79CFC0642CE59F01F5A ();
// 0x000000B7 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::System.IDisposable.Dispose()
extern void U3CWaitForStringU3Ed__27_System_IDisposable_Dispose_m3A6329C23BBF49D6D62E6FB9E1D095FCE2FB5CC8 ();
// 0x000000B8 System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::MoveNext()
extern void U3CWaitForStringU3Ed__27_MoveNext_m9B0CB73ADF29AB26780D524088E2A79545438B0F ();
// 0x000000B9 System.String UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CWaitForStringU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mDE500D0A61D55D95B172A980D9E08965245BFAE0 ();
// 0x000000BA System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::System.Collections.IEnumerator.Reset()
extern void U3CWaitForStringU3Ed__27_System_Collections_IEnumerator_Reset_mCF82D8202DF7622BF188EB47ED30954F126075CB ();
// 0x000000BB System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForString>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForStringU3Ed__27_System_Collections_IEnumerator_get_Current_mF881CAD498717E0BFF6238730F8B187DC1F1C47F ();
// 0x000000BC System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::.ctor(System.Int32)
extern void U3CWaitForStringUntypedU3Ed__28__ctor_m35DF039024907C15216B12C77619EE16D8641943 ();
// 0x000000BD System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::System.IDisposable.Dispose()
extern void U3CWaitForStringUntypedU3Ed__28_System_IDisposable_Dispose_m5289354F41A9A8BE9DFC5AB2E8E5EAA1156DABAF ();
// 0x000000BE System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::MoveNext()
extern void U3CWaitForStringUntypedU3Ed__28_MoveNext_m2F628E6393EC614CF6F28A05961A315CC076D8D8 ();
// 0x000000BF System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForStringUntypedU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA96D5B5B679AD5AA29B167B3DC9C3D5D1EF872D2 ();
// 0x000000C0 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::System.Collections.IEnumerator.Reset()
extern void U3CWaitForStringUntypedU3Ed__28_System_Collections_IEnumerator_Reset_mA1B2DD7FF2AB9B53E5D336D004CE18438ACFA92F ();
// 0x000000C1 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitForStringUntyped>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForStringUntypedU3Ed__28_System_Collections_IEnumerator_get_Current_mCD56C01FA24DBE27922A8561DADD752D7818E298 ();
// 0x000000C2 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::.ctor(System.Int32)
extern void U3CWaitABitU3Ed__29__ctor_m1FF8180B50C70157444E85FA8650900A13F63D83 ();
// 0x000000C3 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::System.IDisposable.Dispose()
extern void U3CWaitABitU3Ed__29_System_IDisposable_Dispose_m3EC223858541A35CC6C96415AECC66BF7C1C4A03 ();
// 0x000000C4 System.Boolean UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::MoveNext()
extern void U3CWaitABitU3Ed__29_MoveNext_mAA76CB4C6D7ECD91562ABDB5CE2C32120E9D47AB ();
// 0x000000C5 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitABitU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F3C18559D90AA538760B5A8D16D983D14F1FFBF ();
// 0x000000C6 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::System.Collections.IEnumerator.Reset()
extern void U3CWaitABitU3Ed__29_System_Collections_IEnumerator_Reset_m6E679759D19B1BD9EB9B79A7333540BB09C140A5 ();
// 0x000000C7 System.Object UnityAsyncAwaitUtil.AsyncUtilTests_<WaitABit>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CWaitABitU3Ed__29_System_Collections_IEnumerator_get_Current_m7F80AD522AFD7D3F262DE68D4F2DFA2B5EF3E889 ();
// 0x000000C8 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunReturnValueTestAsync>d__30::MoveNext()
extern void U3CRunReturnValueTestAsyncU3Ed__30_MoveNext_mD84FE1A3512D2DFA9D018883C2794078AC8343EF_AdjustorThunk ();
// 0x000000C9 System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunReturnValueTestAsync>d__30::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunReturnValueTestAsyncU3Ed__30_SetStateMachine_m36B0D58BFAAC4C833FE8AD0584540C1B9531328A_AdjustorThunk ();
// 0x000000CA System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<GetValueExampleAsync>d__31::MoveNext()
extern void U3CGetValueExampleAsyncU3Ed__31_MoveNext_m0F8727A6E2EDEDC7165CFA674A9BBAB6DC9603D1_AdjustorThunk ();
// 0x000000CB System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<GetValueExampleAsync>d__31::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetValueExampleAsyncU3Ed__31_SetStateMachine_m6911845A7CE327C109805359C325BB5DAEDD6E72_AdjustorThunk ();
// 0x000000CC System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAwaitSecondsTestAsync>d__32::MoveNext()
extern void U3CRunAwaitSecondsTestAsyncU3Ed__32_MoveNext_mE1BF32943FBA2237D4678F1BC143AFA9204C0BD7_AdjustorThunk ();
// 0x000000CD System.Void UnityAsyncAwaitUtil.AsyncUtilTests_<RunAwaitSecondsTestAsync>d__32::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRunAwaitSecondsTestAsyncU3Ed__32_SetStateMachine_mE0FCF3112C5EFB57B3A130B96902EA854E38B63D_AdjustorThunk ();
// 0x000000CE System.Void UnityAsyncAwaitUtil.TestButtonHandler_Settings::.ctor()
extern void Settings__ctor_m5FF544A2AA4D7F6BFBD82287A559E306662B6117 ();
// 0x000000CF System.Void IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::.ctor(System.Int32)
// 0x000000D0 System.Void IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::System.IDisposable.Dispose()
// 0x000000D1 System.Boolean IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::MoveNext()
// 0x000000D2 System.Object IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x000000D3 System.Void IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::System.Collections.IEnumerator.Reset()
// 0x000000D4 System.Object IEnumeratorAwaitExtensions_CoroutineWrapper`1_<Run>d__3::System.Collections.IEnumerator.get_Current()
// 0x000000D5 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::.ctor(System.Int32)
extern void U3CReturnVoidU3Ed__0__ctor_m6ECA56201B010A62938ECAE0F957FFC339B7DBF5 ();
// 0x000000D6 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::System.IDisposable.Dispose()
extern void U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m6E88EC9930B7DA69323E97D6DDCEAD3A903F1303 ();
// 0x000000D7 System.Boolean IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::MoveNext()
extern void U3CReturnVoidU3Ed__0_MoveNext_m1EB4828252E3BF160DC820299ACF0A7B4DE85EB8 ();
// 0x000000D8 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0D8811A39D1A37005F9B7E9306B30EAC147357 ();
// 0x000000D9 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::System.Collections.IEnumerator.Reset()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_m9676721E3BFCAB238970E2AD832DA0DE58C7C70B ();
// 0x000000DA System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnVoid>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m34B9A9588DBBAB63F7CA472224E9C9BFAFE7807A ();
// 0x000000DB System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::.ctor(System.Int32)
extern void U3CAssetBundleCreateRequestU3Ed__1__ctor_m70DF1B723C237CA44F14BEE913C0BC524F186B6B ();
// 0x000000DC System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::System.IDisposable.Dispose()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m0516B299AADE31F30280F1A4AB9262CABFF98650 ();
// 0x000000DD System.Boolean IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::MoveNext()
extern void U3CAssetBundleCreateRequestU3Ed__1_MoveNext_m0684EF021FDB2A350603B14CA74EF00DC6A846BE ();
// 0x000000DE System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE8F290FF13C44270BFCDBF664BA6DE9A52870BE ();
// 0x000000DF System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_m41CF924FE6821CF0897742C3148244E7E78C96B8 ();
// 0x000000E0 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m128BAAE3C95A81C0D6CD9BE9BBD593FE126B50CA ();
// 0x000000E1 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::.ctor(System.Int32)
// 0x000000E2 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::System.IDisposable.Dispose()
// 0x000000E3 System.Boolean IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::MoveNext()
// 0x000000E4 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x000000E5 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::System.Collections.IEnumerator.Reset()
// 0x000000E6 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ReturnSelf>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x000000E7 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::.ctor(System.Int32)
extern void U3CAssetBundleRequestU3Ed__3__ctor_mD02BD9805A924A4A012E47EC1E2E991CEC60A706 ();
// 0x000000E8 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::System.IDisposable.Dispose()
extern void U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_m6D40E0EA00E7698A6F8DC8BB9427880CDBD2ED2B ();
// 0x000000E9 System.Boolean IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::MoveNext()
extern void U3CAssetBundleRequestU3Ed__3_MoveNext_mAE95A3C93B81B4259A5DDE15DE6C63C018380921 ();
// 0x000000EA System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m557FA9412D7C445D3C96E5F08BEF818075F5BC4F ();
// 0x000000EB System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_m213ED15DA83DBBA625A5FE0D9828001F4C0CEA45 ();
// 0x000000EC System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<AssetBundleRequest>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_mB96FD5D9EBE77CE62BE4B587AA3E529372A3069A ();
// 0x000000ED System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::.ctor(System.Int32)
extern void U3CResourceRequestU3Ed__4__ctor_mF6C21B89A1A409BDC6FE0E32A1A2DBDF2A02D0D5 ();
// 0x000000EE System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::System.IDisposable.Dispose()
extern void U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m7919A9182AA7A4588F77A0629F3F0BB8F4024E8D ();
// 0x000000EF System.Boolean IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::MoveNext()
extern void U3CResourceRequestU3Ed__4_MoveNext_m8F84D87B37A69D355B97D9DC4373ED56627261C6 ();
// 0x000000F0 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27D6D9FE82B6AB63547D6EBB00F00561FF5AABC2 ();
// 0x000000F1 System.Void IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::System.Collections.IEnumerator.Reset()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mEF6EE443CE637B7B467135F57A3F5F2E47CC4F12 ();
// 0x000000F2 System.Object IEnumeratorAwaitExtensions_InstructionWrappers_<ResourceRequest>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m6767E60475EFF12A9CF071280AC2427C5D62DE05 ();
static Il2CppMethodPointer s_methodPointers[242] = 
{
	AwaitExtensions_GetAwaiter_mEB5AE2C73D25B24345A4D588CDCBEF94BB4CA15E,
	AwaitExtensions_WrapErrors_mFBFB40AD9AAAFCF83081E69CCA87616CC9821D5F,
	Awaiters_get_NextFrame_mC0045B33363C69E85449F8AE40E571ACC70DC8D8,
	Awaiters_get_FixedUpdate_m8BCBF549EBD0AF13ECE1FECF4A2C79AAE4579D81,
	Awaiters_get_EndOfFrame_m5364D9F3FE1F8DAD4FA87A75F5E1D15383328C6A,
	Awaiters_Seconds_m61DBAF2693A28D3A2C83A42D4369EEAE69CCD54C,
	Awaiters_SecondsRealtime_mEE429F9B5B5B53E14206A82A176F2C4E848589EF,
	Awaiters_Until_m7E2357E3CD320C04963A56AFC7132EAF02E25539,
	Awaiters_While_m833748B426DF71FF354F32E70DDCFAAF3FADEF97,
	Awaiters__cctor_mFE77E43D738720B120E3C5C55284F2636FAF2489,
	IEnumeratorAwaitExtensions_GetAwaiter_m2BEED4D645673F85EC103A9B5EEF6975E9637F15,
	IEnumeratorAwaitExtensions_GetAwaiter_mA5EA65A4F139EFCC2A10D65AF14C506B44579389,
	IEnumeratorAwaitExtensions_GetAwaiter_m2EA7BB7DBFD93B942EF7BB90274E985903281828,
	IEnumeratorAwaitExtensions_GetAwaiter_m3CB17DE27A3DFD0B0F1A44CBA67D4D88C28934E5,
	IEnumeratorAwaitExtensions_GetAwaiter_mB454EAC688413478004A568667AC4E054F58CAEB,
	IEnumeratorAwaitExtensions_GetAwaiter_m1EC0F3217495FCB71CE3AE7CE8E7A340D00FD99C,
	IEnumeratorAwaitExtensions_GetAwaiter_m39FE7248F563012B06F24B7BC73D5D48125F7AF1,
	IEnumeratorAwaitExtensions_GetAwaiter_m5D22984838DC7F2E1EAD023BE380C04DC906BA49,
	IEnumeratorAwaitExtensions_GetAwaiter_m2E585282227F335C8E5F67CF23F1EA6130261564,
	IEnumeratorAwaitExtensions_GetAwaiter_mA6AE09D71E728A13EFDE9452550E5526E3C4CF35,
	IEnumeratorAwaitExtensions_GetAwaiter_mDE60FC25BE1AF26CB9DCF94067E6FD732C1D69D5,
	IEnumeratorAwaitExtensions_GetAwaiter_m3F4A75D0658DC0FAA949C3D8CED39AAA348D493F,
	NULL,
	IEnumeratorAwaitExtensions_GetAwaiter_m9B07E9C5C9585A7FF75C8BAB0ABDCF5633671EAB,
	IEnumeratorAwaitExtensions_GetAwaiterReturnVoid_mEF080BDE52ACF40C8BB849D1512A7AAD82EB45E9,
	NULL,
	IEnumeratorAwaitExtensions_RunOnUnityScheduler_mA461FC93AF317E942631EA5BE6F66B49325D0627,
	IEnumeratorAwaitExtensions_Assert_mFFC6E10802D3E5D1F47ED44EE0F1EB0597C8AA70,
	TaskExtensions_AsIEnumerator_m4B9BB89111A0563378CF03DB6BF8CDC6DA427615,
	NULL,
	WaitForBackgroundThread_GetAwaiter_mF5BD1E3BD5C178764B625FE68646AF8EB137E54A,
	WaitForBackgroundThread__ctor_mE10A7B31DBF2EC6EE3A8DEC0DB62AE1948F35895,
	WaitForUpdate_get_keepWaiting_m4C7CA0781BEAF7150C7FDC1A341DD0376CDC80BE,
	WaitForUpdate__ctor_mDFC8505EED32E1C469098E50255B7EA48F78DEF8,
	AsyncCoroutineRunner_get_Instance_m10C8781EEBC413DC0A36AC657C1FA83403E356D1,
	AsyncCoroutineRunner_Awake_mC1105A959BC9D30D51863CF75F6911A100C37538,
	AsyncCoroutineRunner__ctor_mC25E35AD4A13667C11EF5D12DED05F8357E428AF,
	SyncContextUtil_Install_m3EAC7DA8913F51452AE29D484CE92A31CDF8483D,
	SyncContextUtil_get_UnityThreadId_mBC168363841AD3A4828965BD7BED03AE614382E6,
	SyncContextUtil_set_UnityThreadId_mB8AE28551E6501FB6AC50B1B5C3720676E1CCE12,
	SyncContextUtil_get_UnitySynchronizationContext_m94CE8EED7254D6F33856A22A3151454DE6702774,
	SyncContextUtil_set_UnitySynchronizationContext_m1CABD405BCA72B4FCBE214B401B2A7AC15279197,
	AsyncUtilTests_Awake_m21AA61E9AAAD7135D68BDF9FC6B3E61C70D9CD1D,
	AsyncUtilTests_OnGUI_m53F1ABD1DFA79B289AED0A97DAA011CB003708D6,
	AsyncUtilTests_RunAsyncFromCoroutineTest_m27A9B32F58EAA46D404F2DD17E844179A984913D,
	AsyncUtilTests_RunMultipleThreadsTestAsync_m6F17943A35FCD2D3F05F4738C33BD1B7BDBA06EE,
	AsyncUtilTests_RunMultipleThreadsTestAsyncWait_mB53C41E8E793C297E058358AABC2328230D30462,
	AsyncUtilTests_PrintCurrentThreadContext_m1FDC0DEB11D9D309B441F340F6AE2522B3262DA6,
	AsyncUtilTests_RunAsyncFromCoroutineTest2_m700D36F0D61D233EE2A134127F5A30D1D4F6A46C,
	AsyncUtilTests_RunWwwAsync_m70F49593D14C1A62C79EB0A65606CC13284C23AA,
	AsyncUtilTests_RunOpenNotepadTestAsync_m61A9BD7B304F2139B967506120AB462E309405B6,
	AsyncUtilTests_RunUnhandledExceptionTestAsync_mC0070DD1B7E4F5510B3F707595C32D05740C400E,
	AsyncUtilTests_RunTryCatchExceptionTestAsync_m2D85BF1C754E0F8C822491AF0155DAB3839AE4D2,
	AsyncUtilTests_NestedRunAsync_mFB457D3D35245784C12D3016FCA94E71180CEBC7,
	AsyncUtilTests_WaitThenThrowException_mE17D33EFE92783D57044AA4F0583D0C443677382,
	AsyncUtilTests_RunAsyncOperationAsync_mCA01B206181205A4B3054D32D6B586E7107DC06C,
	AsyncUtilTests_InstantiateAssetBundleAsync_m7C4B8B8F1657956FA63EE45DD2B264D3113C965B,
	AsyncUtilTests_DownloadRawDataAsync_mAF29295131E857AED3BC915F454F4F324340883A,
	AsyncUtilTests_RunIEnumeratorTryCatchExceptionAsync_m7A218917382A96607B3B2DA64CB8737D12BC8CC8,
	AsyncUtilTests_RunIEnumeratorUnhandledExceptionAsync_m8E8DA8ECC75966CE8A9DCC5AD7F745CA1DB4C6BD,
	AsyncUtilTests_WaitThenThrow_mF52CB2814639A70A32F19D5C662E0540090BB660,
	AsyncUtilTests_WaitThenThrowNested_mFBC0155CBFDE00E1B6DE4EB77C24DD18FE396762,
	AsyncUtilTests_RunIEnumeratorStringTestAsync_m55DF8F978287A49008F9FD79C71762254271BD3A,
	AsyncUtilTests_RunIEnumeratorUntypedStringTestAsync_m8CF7DD1A44183DBF3DD16E7533FBD41E8BA4FC5A,
	AsyncUtilTests_RunIEnumeratorTestAsync_m516218BC3F406B2E6427904020021D6AD4A16158,
	AsyncUtilTests_WaitForString_mC45ABEE57F5344AA7E7B522B0990D54C6E81DD33,
	AsyncUtilTests_WaitForStringUntyped_m8E7C86489F348662566C6AC6FECCCCC1CF7EB1BC,
	AsyncUtilTests_WaitABit_mAECFD702A0C6FBD072021975611F80F90F89E150,
	AsyncUtilTests_RunReturnValueTestAsync_mE8BF4B8CF8A6BDECE2F0877F0251E536FFCCC3DC,
	AsyncUtilTests_GetValueExampleAsync_mD96231290DF15DCA6BBD874319523076B258C08C,
	AsyncUtilTests_RunAwaitSecondsTestAsync_mE1DB2FADA79D7F2F5D44E91671D669AF019B0799,
	AsyncUtilTests__ctor_mEB13DF37F40658334436A2622C67E004F09E5B15,
	TestButtonHandler__ctor_m9CE4FEB9DEB61A078E78411CD26E31A5606C5523,
	TestButtonHandler_Restart_mB597A8AE47F465AAA2F01DAA41E3B78E98F9494A,
	TestButtonHandler_Display_m85804E5AD6396E4DBBB6B51E32CC2A493384B8C6,
	U3CU3Ec__DisplayClass0_0__ctor_mA53D0AB5E685E966B6D76EAC5D1152ABCB9BE4BE,
	U3CU3Ec__DisplayClass0_0_U3CGetAwaiterU3Eb__0_m0F070ED79DBC51EFB1CE5B79A8DD06AC4B85D1C9,
	U3CWrapErrorsU3Ed__1_MoveNext_m28A5E4348528630DBD297B485DC0E71F228F1175_AdjustorThunk,
	U3CWrapErrorsU3Ed__1_SetStateMachine_m44A965410412BCEBC07EAED23CDB56EC0924F167_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SimpleCoroutineAwaiter_get_IsCompleted_m2B7B75FC9B63334B7EA1200CFE80E655E958C236,
	SimpleCoroutineAwaiter_GetResult_m2A63D7E8F28F2E51B505DD39F7C7A43A442483FD,
	SimpleCoroutineAwaiter_Complete_m62AD5283DC9F88FE6060505D36A56454A3C23520,
	SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_mA6D30E4C3F26EF5FCB8603686CB374E72D9F5F65,
	SimpleCoroutineAwaiter__ctor_m042B984144C5A0B0A8DE8C38CDBE2928533207E5,
	NULL,
	NULL,
	NULL,
	NULL,
	InstructionWrappers_ReturnVoid_m31C71330CED1E793FBDB4204BD367DFD2695C841,
	InstructionWrappers_AssetBundleCreateRequest_m1E199C596923F84CC063194513ABC329A47CAE5A,
	NULL,
	InstructionWrappers_AssetBundleRequest_m5F935DD5B5A1AF34B4155E3254DE55BB9D9C9E5D,
	InstructionWrappers_ResourceRequest_m7AA605E8A9CC3F28AA569588C39F38D43F7EA050,
	U3CU3Ec__DisplayClass8_0__ctor_mFDB0D299BD058859BA71EB45C03CCC524637718B,
	U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_m21AB2097D1F45AC1575F9F645C522E28B6B373C4,
	U3CU3Ec__DisplayClass10_0__ctor_m87F4DAD414E64AC51610C818F5C3B28F7E79C2FB,
	U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_mC84B8E2D02E8BD2EC2313D12E2FE671AFD2AB5C3,
	U3CU3Ec__DisplayClass11_0__ctor_mA54925FED62FDB89BE6A1F261BE6298B1FD78258,
	U3CU3Ec__DisplayClass11_0_U3CGetAwaiterU3Eb__0_m65F70E5864CA0A121A24456E2C0FF71D70A614D4,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass13_0__ctor_m610E3564F8DA6F6CB396FE01965C4B86B3A53683,
	U3CU3Ec__DisplayClass13_0_U3CGetAwaiterU3Eb__0_m4DEC856324DB7738F212B25D67858ADEB3A352F9,
	U3CU3Ec__DisplayClass14_0__ctor_m0A8E8B42D66541FB32664CB4888E2AA280246C27,
	U3CU3Ec__DisplayClass14_0_U3CGetAwaiterReturnVoidU3Eb__0_m02E1717BD7F638835E5448D3FA5EDF4D75A44529,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass16_0__ctor_mC7CA923290123DF40F42A90C626E3A4FB8D04A1F,
	U3CU3Ec__DisplayClass16_0_U3CRunOnUnitySchedulerU3Eb__0_mDD374EB0525A2314DAF64FFC9AECAA6937B63E97,
	U3CAsIEnumeratorU3Ed__0__ctor_mFBCE5616F85D85D5A7B7E6F983395678340EBC13,
	U3CAsIEnumeratorU3Ed__0_System_IDisposable_Dispose_m2805C396CB241787EE436BEA19A7C15D7410C2B3,
	U3CAsIEnumeratorU3Ed__0_MoveNext_m5B51F5EEE1589D760928BC985E48EAF761840894,
	U3CAsIEnumeratorU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF9391F073EEDE12A8DE53F76049D67D951EEA2D,
	U3CAsIEnumeratorU3Ed__0_System_Collections_IEnumerator_Reset_mC96D57C0B356F9B6D52B31F468CABEEB089A61C4,
	U3CAsIEnumeratorU3Ed__0_System_Collections_IEnumerator_get_Current_m11801BAF812DB0406F608595637853B55244D1EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__cctor_mF7F6BA3E6CC1EE24BFA38659F7BC11BBC21A43F0,
	U3CU3Ec__ctor_m6075E0522FAA899F43AFF716AB7D9276A07D1938,
	U3CU3Ec_U3CGetAwaiterU3Eb__0_0_mA923BFB5CDC378130FD6C0A028B56279942E22B4,
	U3CRunAsyncFromCoroutineTestU3Ed__6__ctor_mAE29AD40F69EFC8CF17CAA6521E052B7ECB4DBFA,
	U3CRunAsyncFromCoroutineTestU3Ed__6_System_IDisposable_Dispose_m62FEA88182669E3BEBB04E3E370B7A98BDA65834,
	U3CRunAsyncFromCoroutineTestU3Ed__6_MoveNext_m065E8197E9B04B7B54DB65F20C58FFD6F7201C64,
	U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m583A6409E02200B879658557D45253501FE53CB0,
	U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_IEnumerator_Reset_m172A85AD1BD6AE45E58C417C5C822DD0E7285B08,
	U3CRunAsyncFromCoroutineTestU3Ed__6_System_Collections_IEnumerator_get_Current_m079AD724E8C58063AF8262729C9A68E37895CA02,
	U3CRunMultipleThreadsTestAsyncU3Ed__7_MoveNext_m315DF10B94E2BE226498A5D8D57BF2A64746B4E4_AdjustorThunk,
	U3CRunMultipleThreadsTestAsyncU3Ed__7_SetStateMachine_m73F2DC70E4C26D11FA5D126F6475EA4C997B140B_AdjustorThunk,
	U3CRunMultipleThreadsTestAsyncWaitU3Ed__8_MoveNext_m696D99A52B1758C36F0ACF67ACFD248A4B105F65_AdjustorThunk,
	U3CRunMultipleThreadsTestAsyncWaitU3Ed__8_SetStateMachine_mD443C3FAB919433B779492862D696A34CABAA7F0_AdjustorThunk,
	U3CRunAsyncFromCoroutineTest2U3Ed__10_MoveNext_m498BF5D49DDF8EFCFF4ED3C1132BB29C59D53A10_AdjustorThunk,
	U3CRunAsyncFromCoroutineTest2U3Ed__10_SetStateMachine_m804FA88575B02A39FA9AD642EE5CA006C347ED5E_AdjustorThunk,
	U3CRunWwwAsyncU3Ed__11_MoveNext_mDEA1D22871D2CBFCB45556681608ED7F61C326A2_AdjustorThunk,
	U3CRunWwwAsyncU3Ed__11_SetStateMachine_mF070E762270D62251A6E64A029D3EEEBBE7663B4_AdjustorThunk,
	U3CRunOpenNotepadTestAsyncU3Ed__12_MoveNext_m213E029BC0E07B8A8ABFC3458C256412DB14519E_AdjustorThunk,
	U3CRunOpenNotepadTestAsyncU3Ed__12_SetStateMachine_mB9AF7C0ADD7E760EDAAB8B434C3E9845CBE540EC_AdjustorThunk,
	U3CRunUnhandledExceptionTestAsyncU3Ed__13_MoveNext_mD537D08F75F1C1B3001FF9ECD55B720CB299251D_AdjustorThunk,
	U3CRunUnhandledExceptionTestAsyncU3Ed__13_SetStateMachine_mC5464FD2F49DED13665C3C09267C4820C394B233_AdjustorThunk,
	U3CRunTryCatchExceptionTestAsyncU3Ed__14_MoveNext_m4E061A1FF4BFAB1A270C17CF31A1A3CE398C06C7_AdjustorThunk,
	U3CRunTryCatchExceptionTestAsyncU3Ed__14_SetStateMachine_mA5A73C25618674118CADEF0103D8661A5F1BD193_AdjustorThunk,
	U3CNestedRunAsyncU3Ed__15_MoveNext_m514891EE85ACA531508102DFBB30503D89828805_AdjustorThunk,
	U3CNestedRunAsyncU3Ed__15_SetStateMachine_m669EE53E27F124C402DD86F58B2B967F362B4AF8_AdjustorThunk,
	U3CWaitThenThrowExceptionU3Ed__16_MoveNext_m9BA1E3232E1FD567C805E6854602C8F41AD3D646_AdjustorThunk,
	U3CWaitThenThrowExceptionU3Ed__16_SetStateMachine_m210ED9D7A16E9D414A12D8D47DB5646B470025E0_AdjustorThunk,
	U3CRunAsyncOperationAsyncU3Ed__17_MoveNext_m2E60483AF901B7662434D3BE0F79E7798BEFC5D3_AdjustorThunk,
	U3CRunAsyncOperationAsyncU3Ed__17_SetStateMachine_mD8584D4C52423453794C6AD5C570D0D0536A6311_AdjustorThunk,
	U3CInstantiateAssetBundleAsyncU3Ed__18_MoveNext_m7664CD755804EF86E20CB3FCD9620FAA3A4EAE85_AdjustorThunk,
	U3CInstantiateAssetBundleAsyncU3Ed__18_SetStateMachine_m995163F9BE843EA04645BD8F5B014F426B7F2729_AdjustorThunk,
	U3CDownloadRawDataAsyncU3Ed__19_MoveNext_m73BD027862760F31034899377BDADB20B2FD0BE0_AdjustorThunk,
	U3CDownloadRawDataAsyncU3Ed__19_SetStateMachine_m6F58682586BA9C7E18A069D057ACC6F77B4A0DBF_AdjustorThunk,
	U3CRunIEnumeratorTryCatchExceptionAsyncU3Ed__20_MoveNext_m164E2925C1C569D092B6913A6641A8151C61632A_AdjustorThunk,
	U3CRunIEnumeratorTryCatchExceptionAsyncU3Ed__20_SetStateMachine_mAB4BB6853840D54CCE570DE75BE835A6B8D8B6C9_AdjustorThunk,
	U3CRunIEnumeratorUnhandledExceptionAsyncU3Ed__21_MoveNext_m2ED054D50EF1A3385B6F935D67279B56415F7076_AdjustorThunk,
	U3CRunIEnumeratorUnhandledExceptionAsyncU3Ed__21_SetStateMachine_m99C4A897EF4108F468CE7F941B7A5704FD0E4FAE_AdjustorThunk,
	U3CWaitThenThrowU3Ed__22__ctor_m9E30D3AF207A9061C15B4018FE098623F3233DFD,
	U3CWaitThenThrowU3Ed__22_System_IDisposable_Dispose_m13B61B824F5C887499AB2EBC4E12C88BF5D7BB7B,
	U3CWaitThenThrowU3Ed__22_MoveNext_m4892E13BB3660288A7D620611F13825F3FF13B94,
	U3CWaitThenThrowU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8704A604FBD336DA5E922AF69FA2182D1E672102,
	U3CWaitThenThrowU3Ed__22_System_Collections_IEnumerator_Reset_m5911D60740F4B9EA897D34270799DFC809B6674A,
	U3CWaitThenThrowU3Ed__22_System_Collections_IEnumerator_get_Current_m234395CE73855C058405AF462367E50724CF2CB7,
	U3CWaitThenThrowNestedU3Ed__23__ctor_mD4BC360B97C8E071AF785A22BBB5817437903BDE,
	U3CWaitThenThrowNestedU3Ed__23_System_IDisposable_Dispose_m9DFC1526C573C856FF49DBA9B876528914F9F9C3,
	U3CWaitThenThrowNestedU3Ed__23_MoveNext_m06BAA010BE797AA78AE299580811CD1D3F29D0BE,
	U3CWaitThenThrowNestedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF4DA5260EEEF1863DF5BE0CC9F45224629798A7B,
	U3CWaitThenThrowNestedU3Ed__23_System_Collections_IEnumerator_Reset_m6B79B508642FF019D343D72D9F36847181880964,
	U3CWaitThenThrowNestedU3Ed__23_System_Collections_IEnumerator_get_Current_m1D6C70D2DA332C143476BEFEC90387147C73601F,
	U3CRunIEnumeratorStringTestAsyncU3Ed__24_MoveNext_m653612C71AEC1175D6806811A2BEE4F84DAA554D_AdjustorThunk,
	U3CRunIEnumeratorStringTestAsyncU3Ed__24_SetStateMachine_m3CD16412FFDB097A36AE72AE8540FDA72716901F_AdjustorThunk,
	U3CRunIEnumeratorUntypedStringTestAsyncU3Ed__25_MoveNext_m878D445B982B95E58337A4E67A35FDBDE62A3B94_AdjustorThunk,
	U3CRunIEnumeratorUntypedStringTestAsyncU3Ed__25_SetStateMachine_mC5030676D1C58527C112A20A690991DC9B0B4705_AdjustorThunk,
	U3CRunIEnumeratorTestAsyncU3Ed__26_MoveNext_m59AA8544DCA07BF6BEC7F12998DCCFEF32837876_AdjustorThunk,
	U3CRunIEnumeratorTestAsyncU3Ed__26_SetStateMachine_m2E0CC454CEA207ECDDAD0E3E3C8496C5F0D7A7C1_AdjustorThunk,
	U3CWaitForStringU3Ed__27__ctor_mC0258D2F8D6B378B318EA79CFC0642CE59F01F5A,
	U3CWaitForStringU3Ed__27_System_IDisposable_Dispose_m3A6329C23BBF49D6D62E6FB9E1D095FCE2FB5CC8,
	U3CWaitForStringU3Ed__27_MoveNext_m9B0CB73ADF29AB26780D524088E2A79545438B0F,
	U3CWaitForStringU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mDE500D0A61D55D95B172A980D9E08965245BFAE0,
	U3CWaitForStringU3Ed__27_System_Collections_IEnumerator_Reset_mCF82D8202DF7622BF188EB47ED30954F126075CB,
	U3CWaitForStringU3Ed__27_System_Collections_IEnumerator_get_Current_mF881CAD498717E0BFF6238730F8B187DC1F1C47F,
	U3CWaitForStringUntypedU3Ed__28__ctor_m35DF039024907C15216B12C77619EE16D8641943,
	U3CWaitForStringUntypedU3Ed__28_System_IDisposable_Dispose_m5289354F41A9A8BE9DFC5AB2E8E5EAA1156DABAF,
	U3CWaitForStringUntypedU3Ed__28_MoveNext_m2F628E6393EC614CF6F28A05961A315CC076D8D8,
	U3CWaitForStringUntypedU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA96D5B5B679AD5AA29B167B3DC9C3D5D1EF872D2,
	U3CWaitForStringUntypedU3Ed__28_System_Collections_IEnumerator_Reset_mA1B2DD7FF2AB9B53E5D336D004CE18438ACFA92F,
	U3CWaitForStringUntypedU3Ed__28_System_Collections_IEnumerator_get_Current_mCD56C01FA24DBE27922A8561DADD752D7818E298,
	U3CWaitABitU3Ed__29__ctor_m1FF8180B50C70157444E85FA8650900A13F63D83,
	U3CWaitABitU3Ed__29_System_IDisposable_Dispose_m3EC223858541A35CC6C96415AECC66BF7C1C4A03,
	U3CWaitABitU3Ed__29_MoveNext_mAA76CB4C6D7ECD91562ABDB5CE2C32120E9D47AB,
	U3CWaitABitU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1F3C18559D90AA538760B5A8D16D983D14F1FFBF,
	U3CWaitABitU3Ed__29_System_Collections_IEnumerator_Reset_m6E679759D19B1BD9EB9B79A7333540BB09C140A5,
	U3CWaitABitU3Ed__29_System_Collections_IEnumerator_get_Current_m7F80AD522AFD7D3F262DE68D4F2DFA2B5EF3E889,
	U3CRunReturnValueTestAsyncU3Ed__30_MoveNext_mD84FE1A3512D2DFA9D018883C2794078AC8343EF_AdjustorThunk,
	U3CRunReturnValueTestAsyncU3Ed__30_SetStateMachine_m36B0D58BFAAC4C833FE8AD0584540C1B9531328A_AdjustorThunk,
	U3CGetValueExampleAsyncU3Ed__31_MoveNext_m0F8727A6E2EDEDC7165CFA674A9BBAB6DC9603D1_AdjustorThunk,
	U3CGetValueExampleAsyncU3Ed__31_SetStateMachine_m6911845A7CE327C109805359C325BB5DAEDD6E72_AdjustorThunk,
	U3CRunAwaitSecondsTestAsyncU3Ed__32_MoveNext_mE1BF32943FBA2237D4678F1BC143AFA9204C0BD7_AdjustorThunk,
	U3CRunAwaitSecondsTestAsyncU3Ed__32_SetStateMachine_mE0FCF3112C5EFB57B3A130B96902EA854E38B63D_AdjustorThunk,
	Settings__ctor_m5FF544A2AA4D7F6BFBD82287A559E306662B6117,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CReturnVoidU3Ed__0__ctor_m6ECA56201B010A62938ECAE0F957FFC339B7DBF5,
	U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_m6E88EC9930B7DA69323E97D6DDCEAD3A903F1303,
	U3CReturnVoidU3Ed__0_MoveNext_m1EB4828252E3BF160DC820299ACF0A7B4DE85EB8,
	U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7F0D8811A39D1A37005F9B7E9306B30EAC147357,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_m9676721E3BFCAB238970E2AD832DA0DE58C7C70B,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m34B9A9588DBBAB63F7CA472224E9C9BFAFE7807A,
	U3CAssetBundleCreateRequestU3Ed__1__ctor_m70DF1B723C237CA44F14BEE913C0BC524F186B6B,
	U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_m0516B299AADE31F30280F1A4AB9262CABFF98650,
	U3CAssetBundleCreateRequestU3Ed__1_MoveNext_m0684EF021FDB2A350603B14CA74EF00DC6A846BE,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE8F290FF13C44270BFCDBF664BA6DE9A52870BE,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_m41CF924FE6821CF0897742C3148244E7E78C96B8,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m128BAAE3C95A81C0D6CD9BE9BBD593FE126B50CA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CAssetBundleRequestU3Ed__3__ctor_mD02BD9805A924A4A012E47EC1E2E991CEC60A706,
	U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_m6D40E0EA00E7698A6F8DC8BB9427880CDBD2ED2B,
	U3CAssetBundleRequestU3Ed__3_MoveNext_mAE95A3C93B81B4259A5DDE15DE6C63C018380921,
	U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m557FA9412D7C445D3C96E5F08BEF818075F5BC4F,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_m213ED15DA83DBBA625A5FE0D9828001F4C0CEA45,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_mB96FD5D9EBE77CE62BE4B587AA3E529372A3069A,
	U3CResourceRequestU3Ed__4__ctor_mF6C21B89A1A409BDC6FE0E32A1A2DBDF2A02D0D5,
	U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m7919A9182AA7A4588F77A0629F3F0BB8F4024E8D,
	U3CResourceRequestU3Ed__4_MoveNext_m8F84D87B37A69D355B97D9DC4373ED56627261C6,
	U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27D6D9FE82B6AB63547D6EBB00F00561FF5AABC2,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_mEF6EE443CE637B7B467135F57A3F5F2E47CC4F12,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m6767E60475EFF12A9CF071280AC2427C5D62DE05,
};
static const int32_t s_InvokerIndices[242] = 
{
	2933,
	154,
	4,
	4,
	4,
	97,
	97,
	0,
	0,
	3,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	-1,
	0,
	0,
	-1,
	154,
	854,
	0,
	-1,
	996,
	23,
	89,
	23,
	4,
	23,
	23,
	3,
	106,
	164,
	4,
	154,
	23,
	23,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	105,
	28,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	26,
	23,
	9,
	23,
	27,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	-1,
	89,
	23,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	1,
	1,
	-1,
	1,
	1,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	23,
	23,
	23,
	23,
	-1,
	-1,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
};
static const Il2CppTokenRangePair s_rgctxIndices[10] = 
{
	{ 0x02000010, { 10, 2 } },
	{ 0x02000015, { 21, 3 } },
	{ 0x02000018, { 24, 1 } },
	{ 0x0200001B, { 27, 2 } },
	{ 0x02000038, { 12, 5 } },
	{ 0x0200003B, { 19, 2 } },
	{ 0x06000017, { 0, 5 } },
	{ 0x0600001A, { 5, 5 } },
	{ 0x0600001E, { 25, 2 } },
	{ 0x06000060, { 17, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[29] = 
{
	{ (Il2CppRGCTXDataType)2, 34727 },
	{ (Il2CppRGCTXDataType)3, 23987 },
	{ (Il2CppRGCTXDataType)2, 33479 },
	{ (Il2CppRGCTXDataType)3, 23988 },
	{ (Il2CppRGCTXDataType)3, 23989 },
	{ (Il2CppRGCTXDataType)2, 34728 },
	{ (Il2CppRGCTXDataType)3, 23990 },
	{ (Il2CppRGCTXDataType)2, 33481 },
	{ (Il2CppRGCTXDataType)3, 23991 },
	{ (Il2CppRGCTXDataType)3, 23992 },
	{ (Il2CppRGCTXDataType)2, 34729 },
	{ (Il2CppRGCTXDataType)3, 23993 },
	{ (Il2CppRGCTXDataType)3, 23994 },
	{ (Il2CppRGCTXDataType)2, 34730 },
	{ (Il2CppRGCTXDataType)3, 23995 },
	{ (Il2CppRGCTXDataType)3, 23996 },
	{ (Il2CppRGCTXDataType)2, 33496 },
	{ (Il2CppRGCTXDataType)2, 34731 },
	{ (Il2CppRGCTXDataType)3, 23997 },
	{ (Il2CppRGCTXDataType)2, 33512 },
	{ (Il2CppRGCTXDataType)3, 23998 },
	{ (Il2CppRGCTXDataType)2, 34732 },
	{ (Il2CppRGCTXDataType)3, 23999 },
	{ (Il2CppRGCTXDataType)3, 24000 },
	{ (Il2CppRGCTXDataType)3, 24001 },
	{ (Il2CppRGCTXDataType)2, 34733 },
	{ (Il2CppRGCTXDataType)3, 24002 },
	{ (Il2CppRGCTXDataType)3, 24003 },
	{ (Il2CppRGCTXDataType)2, 33553 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2DfirstpassCodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	242,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	10,
	s_rgctxIndices,
	29,
	s_rgctxValues,
	NULL,
};
