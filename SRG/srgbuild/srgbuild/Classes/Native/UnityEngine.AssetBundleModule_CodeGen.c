﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4 ();
// 0x00000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync_Internal(System.Byte[],System.UInt32)
extern void AssetBundle_LoadFromMemoryAsync_Internal_m6D139C7B39BD79290B9A1EA25B24205490FB9FEB ();
// 0x00000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromMemoryAsync(System.Byte[])
extern void AssetBundle_LoadFromMemoryAsync_m1CAB5C0D20C0348B33E782731B250602CE577DE3 ();
// 0x00000004 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String)
// 0x00000005 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5 ();
// 0x00000006 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075 ();
// 0x00000007 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3 ();
// 0x00000008 UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern void AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11 ();
// 0x00000009 System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern void AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE ();
// 0x0000000A UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1 ();
// 0x0000000B System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B ();
static Il2CppMethodPointer s_methodPointers[11] = 
{
	AssetBundle__ctor_mF8A0A863455A7DF958A966C86BE58E07535CD3A4,
	AssetBundle_LoadFromMemoryAsync_Internal_m6D139C7B39BD79290B9A1EA25B24205490FB9FEB,
	AssetBundle_LoadFromMemoryAsync_m1CAB5C0D20C0348B33E782731B250602CE577DE3,
	NULL,
	AssetBundle_LoadAssetAsync_m53B5A725689D7A0D5F8C002E2F2F693AE6E99AB5,
	AssetBundle_LoadAssetAsync_Internal_m2EF88B897D614B01D8DDBACC25C3FEA7E754B075,
	AssetBundle_Unload_m5B7DE41516A059545F8C493EC459EA0A9166EBB3,
	AssetBundleCreateRequest_get_assetBundle_mFBACEFA632A41C43D4394F6770059DDE0E35BA11,
	AssetBundleCreateRequest__ctor_m25AE702BB10868BE4521FBBF1481C36AF51501EE,
	AssetBundleRequest_get_asset_mCB977DEC51A59B1333AEEFC2AA189D2E5BF4FCF1,
	AssetBundleRequest__ctor_m752C12271B44EED163182E197EE783D18E36836B,
};
static const int32_t s_InvokerIndices[11] = 
{
	23,
	119,
	0,
	-1,
	105,
	105,
	31,
	14,
	23,
	14,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000004, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)1, 34597 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	11,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
