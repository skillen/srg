﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mB273193DD52B5C9A336427F024738D94C58BAD7A ();
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m0ED71E75DDB669B49D1D31F5F5BAD18A7090A124 ();
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m7DD70DAAA69617120883F56611434D0D0D42E3EC ();
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte[])
extern void NullableAttribute__ctor_mBE190C8A1BFD024F749C6314C354F44885600014 ();
// 0x00000005 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_mD8DE65331926EC11CBC38222484F6A7EF7D70F8B ();
// 0x00000006 System.Void System.Diagnostics.CodeAnalysis.NotNullAttribute::.ctor()
extern void NotNullAttribute__ctor_m79B49921B0928C20183F7133F04B89748935A463 ();
// 0x00000007 System.Void System.Diagnostics.CodeAnalysis.NotNullWhenAttribute::.ctor(System.Boolean)
extern void NotNullWhenAttribute__ctor_mD92C70B72B528AB3AF64F167537A59AEFDA946FC ();
// 0x00000008 System.Void System.Diagnostics.CodeAnalysis.MaybeNullAttribute::.ctor()
extern void MaybeNullAttribute__ctor_mEEB759014C4ACC6EC19C244D77F4953D138E1A20 ();
// 0x00000009 System.Void System.Diagnostics.CodeAnalysis.AllowNullAttribute::.ctor()
extern void AllowNullAttribute__ctor_mB0FA9F7895722BC30947A41FE32C2EF687130115 ();
// 0x0000000A System.Void Newtonsoft.Json.DefaultJsonNameTable::.cctor()
extern void DefaultJsonNameTable__cctor_m777387907B745619C20DA84466262DB733D1F329 ();
// 0x0000000B System.Void Newtonsoft.Json.DefaultJsonNameTable::.ctor()
extern void DefaultJsonNameTable__ctor_m5A0440F494B10AB256DAA10052F8BB07D701588E ();
// 0x0000000C System.String Newtonsoft.Json.DefaultJsonNameTable::Get(System.Char[],System.Int32,System.Int32)
extern void DefaultJsonNameTable_Get_m48891B9470C709211443C20664764BFE4A105182 ();
// 0x0000000D System.String Newtonsoft.Json.DefaultJsonNameTable::Add(System.String)
extern void DefaultJsonNameTable_Add_mB2D7C9B2427728AFA31D6F63F1FC75002125068A ();
// 0x0000000E System.String Newtonsoft.Json.DefaultJsonNameTable::AddEntry(System.String,System.Int32)
extern void DefaultJsonNameTable_AddEntry_mB67424E4B4EA9B81CED3EE08FF6E35B5FD1DEDF6 ();
// 0x0000000F System.Void Newtonsoft.Json.DefaultJsonNameTable::Grow()
extern void DefaultJsonNameTable_Grow_m087C859E722E13F86FD6F0C25BD7DB50364AA8A0 ();
// 0x00000010 System.Boolean Newtonsoft.Json.DefaultJsonNameTable::TextEquals(System.String,System.Char[],System.Int32,System.Int32)
extern void DefaultJsonNameTable_TextEquals_m9A4E903024CE7531B5778AB50A13FE38BCD17AF8 ();
// 0x00000011 System.Void Newtonsoft.Json.DefaultJsonNameTable_Entry::.ctor(System.String,System.Int32,Newtonsoft.Json.DefaultJsonNameTable_Entry)
extern void Entry__ctor_mD93E768008E8A5BBCD25F1115F6970D9BEF672DC ();
// 0x00000012 T[] Newtonsoft.Json.IArrayPool`1::Rent(System.Int32)
// 0x00000013 System.Void Newtonsoft.Json.IArrayPool`1::Return(T[])
// 0x00000014 System.Boolean Newtonsoft.Json.IJsonLineInfo::HasLineInfo()
// 0x00000015 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LineNumber()
// 0x00000016 System.Int32 Newtonsoft.Json.IJsonLineInfo::get_LinePosition()
// 0x00000017 System.Type Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterType()
extern void JsonContainerAttribute_get_ItemConverterType_m5D96D2834844FF0B7C16B3D48B7F79ABD1A0EB83 ();
// 0x00000018 System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_ItemConverterParameters()
extern void JsonContainerAttribute_get_ItemConverterParameters_m520C0C8C690384C3C197C5D712FA9E2F800D2BE3 ();
// 0x00000019 System.Type Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyType()
extern void JsonContainerAttribute_get_NamingStrategyType_m81A64DEDC6DAAD8779ACBBD9F642F92A7DD64155 ();
// 0x0000001A System.Object[] Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyParameters()
extern void JsonContainerAttribute_get_NamingStrategyParameters_m530DCA6B890697E0DB6F9A7D47D43417789148F0 ();
// 0x0000001B Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.JsonContainerAttribute::get_NamingStrategyInstance()
extern void JsonContainerAttribute_get_NamingStrategyInstance_m15380E8ABFA6198F97B2FC8614BABA0EEF4DA7E2 ();
// 0x0000001C System.Void Newtonsoft.Json.JsonContainerAttribute::set_NamingStrategyInstance(Newtonsoft.Json.Serialization.NamingStrategy)
extern void JsonContainerAttribute_set_NamingStrategyInstance_mE4363D98B671605F556978755D570249F5D907C4 ();
// 0x0000001D System.Func`1<Newtonsoft.Json.JsonSerializerSettings> Newtonsoft.Json.JsonConvert::get_DefaultSettings()
extern void JsonConvert_get_DefaultSettings_m0A5829E16356A29B346272512A584D9D26307A70 ();
// 0x0000001E System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern void JsonConvert_ToString_m5539D9C3EB7CE1EBBB1FAECE58392F9AA9B7D078 ();
// 0x0000001F System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern void JsonConvert_ToString_m04EAC879D3ACDC77DF51A996FA5E65EA22F61AC8 ();
// 0x00000020 System.String Newtonsoft.Json.JsonConvert::ToString(System.Single,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_mE6AE849A1CDEEF9BCAE53266B72BA24095EF9C98 ();
// 0x00000021 System.String Newtonsoft.Json.JsonConvert::EnsureFloatFormat(System.Double,System.String,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_EnsureFloatFormat_m124C426E6715F9834E03B83C4A59D8EE5FDE0493 ();
// 0x00000022 System.String Newtonsoft.Json.JsonConvert::ToString(System.Double,Newtonsoft.Json.FloatFormatHandling,System.Char,System.Boolean)
extern void JsonConvert_ToString_m60389DAA11D9D7A863D0A05667510C78B7E7F985 ();
// 0x00000023 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern void JsonConvert_EnsureDecimalPlace_m4F4F7BC16AF4D73DB62DCB9E66A53166945B6D98 ();
// 0x00000024 System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern void JsonConvert_EnsureDecimalPlace_mF1744F05E62345548DC2357DB3D6FE509F336809 ();
// 0x00000025 System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern void JsonConvert_ToString_mD0A25EA590A53D43045C3650CFF72B8A772CDEBD ();
// 0x00000026 System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern void JsonConvert_ToString_mBD0E940E54C81DB74188774D2E69EA954A9F8F10 ();
// 0x00000027 System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern void JsonConvert_ToString_m3AA6E9FF7A6738C7E330EB39CAADDB9DA407A06B ();
// 0x00000028 System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char,Newtonsoft.Json.StringEscapeHandling)
extern void JsonConvert_ToString_m8F2A67B6DD842552AA9B96F5003DE119A8D22357 ();
// 0x00000029 System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object)
extern void JsonConvert_SerializeObject_m062FD300E3D2077562830DD0932BEAAB287E60F7 ();
// 0x0000002A System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting)
extern void JsonConvert_SerializeObject_m8104C62BC781FB4C99515DADA67C044A0734A3F3 ();
// 0x0000002B System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.JsonConverter[])
extern void JsonConvert_SerializeObject_mE6A5323F38C9CC4E06F9C554A7A30E152314CFDC ();
// 0x0000002C System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_SerializeObject_m6243D043AB7C228CE5DCB69026DDF04570197E5D ();
// 0x0000002D System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_SerializeObject_mC903A1A78DA571EEA1D7FF6D2C29E2AA41AF35A1 ();
// 0x0000002E System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,System.Type,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_SerializeObject_m698382F545F441FF31EED69370F4B3D66B39471A ();
// 0x0000002F System.String Newtonsoft.Json.JsonConvert::SerializeObjectInternal(System.Object,System.Type,Newtonsoft.Json.JsonSerializer)
extern void JsonConvert_SerializeObjectInternal_m11EC9E5E449E36BEE41CFEC7B09E6E242B5958D9 ();
// 0x00000030 System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String)
extern void JsonConvert_DeserializeObject_m3BBFF9DA6FB0DC26B9CE39AF351B97F6A2313732 ();
// 0x00000031 System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonConvert_DeserializeObject_mDA0C11B56769B7A57E66BB464676BE6B7B7EFCAF ();
// 0x00000032 System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern void JsonConvert__cctor_m7CC8FB7137895A45DFEC72EBC9D247F764420A72 ();
// 0x00000033 System.Void Newtonsoft.Json.JsonConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000034 System.Object Newtonsoft.Json.JsonConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
// 0x00000035 System.Boolean Newtonsoft.Json.JsonConverter::CanConvert(System.Type)
// 0x00000036 System.Boolean Newtonsoft.Json.JsonConverter::get_CanRead()
extern void JsonConverter_get_CanRead_m9000CD8CB08FB4937904FD3C4DE248EC81C7CF26 ();
// 0x00000037 System.Boolean Newtonsoft.Json.JsonConverter::get_CanWrite()
extern void JsonConverter_get_CanWrite_m92AD594A45F983BBB6564660A66C320B6D33F8CE ();
// 0x00000038 System.Void Newtonsoft.Json.JsonConverter::.ctor()
extern void JsonConverter__ctor_m8FF84C02C0A9A2CCCD194D214D1C167A4FD4EF1F ();
// 0x00000039 System.Type Newtonsoft.Json.JsonConverterAttribute::get_ConverterType()
extern void JsonConverterAttribute_get_ConverterType_m48DC4328902ACEE7B13BFC3612683C11E175C14B ();
// 0x0000003A System.Object[] Newtonsoft.Json.JsonConverterAttribute::get_ConverterParameters()
extern void JsonConverterAttribute_get_ConverterParameters_m9D92AE344C1B162A9ED2D61FC89E18AC872697DD ();
// 0x0000003B System.Void Newtonsoft.Json.JsonConverterCollection::.ctor()
extern void JsonConverterCollection__ctor_mA5111AF227AE8E59AFFE6ED324C6851207890972 ();
// 0x0000003C System.Void Newtonsoft.Json.JsonException::.ctor()
extern void JsonException__ctor_m3F80CF7E07C8D0FA7F3F002872553725110ADCFC ();
// 0x0000003D System.Void Newtonsoft.Json.JsonException::.ctor(System.String)
extern void JsonException__ctor_mE7035AB3F35D9599B2E322778D6FE6CBDF264E47 ();
// 0x0000003E System.Void Newtonsoft.Json.JsonException::.ctor(System.String,System.Exception)
extern void JsonException__ctor_mD0F2E724723681D9A58F3005CCEF101CF601F8B1 ();
// 0x0000003F System.Void Newtonsoft.Json.JsonException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonException__ctor_m3C49AC07DDC28568B0FFE8A6EF27074D21FE262F ();
// 0x00000040 System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_WriteData()
extern void JsonExtensionDataAttribute_get_WriteData_m65FEF9E9BA6439913E87CBE3FFC41A14DE14C0F3 ();
// 0x00000041 System.Boolean Newtonsoft.Json.JsonExtensionDataAttribute::get_ReadData()
extern void JsonExtensionDataAttribute_get_ReadData_mBFF7CD91D4A3544891ABE93C24FCB6CF2964A5A3 ();
// 0x00000042 System.String Newtonsoft.Json.JsonNameTable::Get(System.Char[],System.Int32,System.Int32)
// 0x00000043 System.Void Newtonsoft.Json.JsonNameTable::.ctor()
extern void JsonNameTable__ctor_m2E6B4870C328D678E80DEF2A9EC0EB259977FED2 ();
// 0x00000044 Newtonsoft.Json.MemberSerialization Newtonsoft.Json.JsonObjectAttribute::get_MemberSerialization()
extern void JsonObjectAttribute_get_MemberSerialization_m1A6E56883ACE6421B24BA6FA3C73B48B25B82701 ();
// 0x00000045 System.Void Newtonsoft.Json.JsonPosition::.ctor(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition__ctor_m4BA076CE024C3FF82069500255938E4AA21A0218_AdjustorThunk ();
// 0x00000046 System.Int32 Newtonsoft.Json.JsonPosition::CalculateLength()
extern void JsonPosition_CalculateLength_m539687D796EBA5ED3178CAAEB315D74682D3151F_AdjustorThunk ();
// 0x00000047 System.Void Newtonsoft.Json.JsonPosition::WriteTo(System.Text.StringBuilder,System.IO.StringWriter&,System.Char[]&)
extern void JsonPosition_WriteTo_mD76A0378E52911640E329D0DCFCDDF4281AFA772_AdjustorThunk ();
// 0x00000048 System.Boolean Newtonsoft.Json.JsonPosition::TypeHasIndex(Newtonsoft.Json.JsonContainerType)
extern void JsonPosition_TypeHasIndex_m043EC579B05748A4926CF9530F2416756C45BF41 ();
// 0x00000049 System.String Newtonsoft.Json.JsonPosition::BuildPath(System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>,System.Nullable`1<Newtonsoft.Json.JsonPosition>)
extern void JsonPosition_BuildPath_mE563730C31113CF0AAEC5608455AC547494D7CA7 ();
// 0x0000004A System.String Newtonsoft.Json.JsonPosition::FormatMessage(Newtonsoft.Json.IJsonLineInfo,System.String,System.String)
extern void JsonPosition_FormatMessage_m1E221745622F3A5A217A03CC6C99A51DFC015921 ();
// 0x0000004B System.Void Newtonsoft.Json.JsonPosition::.cctor()
extern void JsonPosition__cctor_mE5BBF78165A85742910DCB97C7A378039B243AD3 ();
// 0x0000004C System.Type Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterType()
extern void JsonPropertyAttribute_get_ItemConverterType_m4B6B27AC08330039C98EEC8CD2ED6E0594100A80 ();
// 0x0000004D System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_ItemConverterParameters()
extern void JsonPropertyAttribute_get_ItemConverterParameters_m7541013D32024590A01A74A974E59C1E175D218F ();
// 0x0000004E System.Type Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyType()
extern void JsonPropertyAttribute_get_NamingStrategyType_m7E0DECD0FC26319048A77974A04FE992F789C484 ();
// 0x0000004F System.Object[] Newtonsoft.Json.JsonPropertyAttribute::get_NamingStrategyParameters()
extern void JsonPropertyAttribute_get_NamingStrategyParameters_mE3FE6F503EE89057BA2A8250714852382D9F753A ();
// 0x00000050 System.String Newtonsoft.Json.JsonPropertyAttribute::get_PropertyName()
extern void JsonPropertyAttribute_get_PropertyName_mF1066F5ABA5DB30B47C643523CC7A5B1320D99BC ();
// 0x00000051 Newtonsoft.Json.JsonReader_State Newtonsoft.Json.JsonReader::get_CurrentState()
extern void JsonReader_get_CurrentState_mE9257C8DE44E2E2FFC91CFE6CC38B4645926B87C ();
// 0x00000052 System.Boolean Newtonsoft.Json.JsonReader::get_CloseInput()
extern void JsonReader_get_CloseInput_m6B7725C8D9DC8A2B3402A81344A63498F0C7D724 ();
// 0x00000053 System.Void Newtonsoft.Json.JsonReader::set_CloseInput(System.Boolean)
extern void JsonReader_set_CloseInput_m16D053F85327E2A97A0B7677D994A2665921FF52 ();
// 0x00000054 System.Boolean Newtonsoft.Json.JsonReader::get_SupportMultipleContent()
extern void JsonReader_get_SupportMultipleContent_mC50C66DE453AD3A0F510C2A7D6AC28BA62A1698C ();
// 0x00000055 System.Void Newtonsoft.Json.JsonReader::set_SupportMultipleContent(System.Boolean)
extern void JsonReader_set_SupportMultipleContent_mABCCAD5810A129EE7F9CE71218BB3D71909EA6FA ();
// 0x00000056 Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::get_DateTimeZoneHandling()
extern void JsonReader_get_DateTimeZoneHandling_mB1E72523A4BC2644910C4E9855091456F32C05AA ();
// 0x00000057 System.Void Newtonsoft.Json.JsonReader::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonReader_set_DateTimeZoneHandling_m58DAFDBFA2B8851F83C8B4B82C7106314FAE151F ();
// 0x00000058 Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::get_DateParseHandling()
extern void JsonReader_get_DateParseHandling_mA75F45CBF7CD7A1B9C4DAA770867F9D597CE61E9 ();
// 0x00000059 System.Void Newtonsoft.Json.JsonReader::set_DateParseHandling(Newtonsoft.Json.DateParseHandling)
extern void JsonReader_set_DateParseHandling_m27B77AFE1688593BC0E8910293E33416481542E1 ();
// 0x0000005A Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::get_FloatParseHandling()
extern void JsonReader_get_FloatParseHandling_mE9263A993BE6C823745205847EAA59561E86034A ();
// 0x0000005B System.Void Newtonsoft.Json.JsonReader::set_FloatParseHandling(Newtonsoft.Json.FloatParseHandling)
extern void JsonReader_set_FloatParseHandling_mAC1FB4A0A6D47F54DB72DDB167D4A12618A48E0F ();
// 0x0000005C System.String Newtonsoft.Json.JsonReader::get_DateFormatString()
extern void JsonReader_get_DateFormatString_m2012D03E952DE4EAF3F24925DBD857CBCA11C8D2 ();
// 0x0000005D System.Void Newtonsoft.Json.JsonReader::set_DateFormatString(System.String)
extern void JsonReader_set_DateFormatString_m7072A352582E68458BCE43FF3CABA6F0A8FE4E5D ();
// 0x0000005E System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::get_MaxDepth()
extern void JsonReader_get_MaxDepth_mD820201F2F7D55EE7BEF65F5FA988922730FAA37 ();
// 0x0000005F System.Void Newtonsoft.Json.JsonReader::set_MaxDepth(System.Nullable`1<System.Int32>)
extern void JsonReader_set_MaxDepth_mA79BC9CBAB51AF7B187A77FD37D08A40CABAA198 ();
// 0x00000060 Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::get_TokenType()
extern void JsonReader_get_TokenType_m9D40984F9628FF1F4516F0192038EAAA0B08982A ();
// 0x00000061 System.Object Newtonsoft.Json.JsonReader::get_Value()
extern void JsonReader_get_Value_m5BB85A7BFB0978CF205098C18D7DB85178A519A5 ();
// 0x00000062 System.Type Newtonsoft.Json.JsonReader::get_ValueType()
extern void JsonReader_get_ValueType_mDA60FBE0FAC4390C33A0FAF15FEA236D0D8823DC ();
// 0x00000063 System.Int32 Newtonsoft.Json.JsonReader::get_Depth()
extern void JsonReader_get_Depth_mBF4A523E7448C8F3BA76E4F8771BC18B07395406 ();
// 0x00000064 System.String Newtonsoft.Json.JsonReader::get_Path()
extern void JsonReader_get_Path_m737454190C235173BD176363A79B3E928E5B0A37 ();
// 0x00000065 System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::get_Culture()
extern void JsonReader_get_Culture_m86CB2F35EDA466949BB203F7BD6106C859FB4088 ();
// 0x00000066 System.Void Newtonsoft.Json.JsonReader::set_Culture(System.Globalization.CultureInfo)
extern void JsonReader_set_Culture_m649FF7F5DC53B0C9389F20E07566BE7960D63AE7 ();
// 0x00000067 Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::GetPosition(System.Int32)
extern void JsonReader_GetPosition_mEC14FC2503F4EEF172EAAD966FE03A2EDBDC447E ();
// 0x00000068 System.Void Newtonsoft.Json.JsonReader::.ctor()
extern void JsonReader__ctor_m6435D6519810700AA3F206AD3C1F611EC27D3D78 ();
// 0x00000069 System.Void Newtonsoft.Json.JsonReader::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonReader_Push_m2CE8921ACE143040888CA7DDDBBC444BD6FB4295 ();
// 0x0000006A Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Pop()
extern void JsonReader_Pop_m31AD4B3BDF093712E8B0BA0C6799B95603556EDE ();
// 0x0000006B Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::Peek()
extern void JsonReader_Peek_mE5A6870F9310C854FCA8DBA769F3336950EC4C77 ();
// 0x0000006C System.Boolean Newtonsoft.Json.JsonReader::Read()
// 0x0000006D System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadAsInt32()
extern void JsonReader_ReadAsInt32_m9D143221536A9C552B78F03C18AB6257A6ACFD59 ();
// 0x0000006E System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::ReadInt32String(System.String)
extern void JsonReader_ReadInt32String_mFD4A4A7D68D9F14EFF26DEB20EB4A3EA8DC12C81 ();
// 0x0000006F System.String Newtonsoft.Json.JsonReader::ReadAsString()
extern void JsonReader_ReadAsString_m674CE8AE02859F88AFC75C3F9CE888C609B9830A ();
// 0x00000070 System.Byte[] Newtonsoft.Json.JsonReader::ReadAsBytes()
extern void JsonReader_ReadAsBytes_m9E20E94F6A91714519BEFE62718FCD834AAEC698 ();
// 0x00000071 System.Byte[] Newtonsoft.Json.JsonReader::ReadArrayIntoByteArray()
extern void JsonReader_ReadArrayIntoByteArray_mDDB2A8F383AEE45F2DCAF57A2817EBADA18F82C6 ();
// 0x00000072 System.Boolean Newtonsoft.Json.JsonReader::ReadArrayElementIntoByteArrayReportDone(System.Collections.Generic.List`1<System.Byte>)
extern void JsonReader_ReadArrayElementIntoByteArrayReportDone_m0495F5A1876A85F57188430C3AD5EC9495FCA289 ();
// 0x00000073 System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadAsDouble()
extern void JsonReader_ReadAsDouble_m31358D0C45601C032F809A54BA174A2347D2DA73 ();
// 0x00000074 System.Nullable`1<System.Double> Newtonsoft.Json.JsonReader::ReadDoubleString(System.String)
extern void JsonReader_ReadDoubleString_mD84724DBCD98CF499B10E87087EF74AAEF397F30 ();
// 0x00000075 System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadAsBoolean()
extern void JsonReader_ReadAsBoolean_m57B718A704F15C0E180EAAC1DDD96850AD205B30 ();
// 0x00000076 System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonReader::ReadBooleanString(System.String)
extern void JsonReader_ReadBooleanString_mE50CB9A6F524C2CD091571AC9C98F529958DEFCE ();
// 0x00000077 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadAsDecimal()
extern void JsonReader_ReadAsDecimal_m8EDF5399CFEAFE9968B320B78B63DF86C3DBB87D ();
// 0x00000078 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonReader::ReadDecimalString(System.String)
extern void JsonReader_ReadDecimalString_m4613D50A65F43F73D59DBCA51BAB57A4EF305C87 ();
// 0x00000079 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadAsDateTime()
extern void JsonReader_ReadAsDateTime_mE7FAACD30ACC184CCC01A7FE09AE64AD64E97586 ();
// 0x0000007A System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonReader::ReadDateTimeString(System.String)
extern void JsonReader_ReadDateTimeString_mAEDFCE1DDAD8E07B9A071298CCE710AAC7E85F14 ();
// 0x0000007B System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonReader::ReadAsDateTimeOffset()
extern void JsonReader_ReadAsDateTimeOffset_m26D64892F6994A956A39ED5A5D9F308F2B406B96 ();
// 0x0000007C System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonReader::ReadDateTimeOffsetString(System.String)
extern void JsonReader_ReadDateTimeOffsetString_mFEE9A470585E5E121B7E11D60FC4F01485A6546D ();
// 0x0000007D System.Void Newtonsoft.Json.JsonReader::ReaderReadAndAssert()
extern void JsonReader_ReaderReadAndAssert_mADBD0FDC473BA90557EECE8C5736BFD1C8D2083B ();
// 0x0000007E Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReader::CreateUnexpectedEndException()
extern void JsonReader_CreateUnexpectedEndException_m62AA28C49F7CCEAC1A71C0C59427EEF0E2621BC1 ();
// 0x0000007F System.Void Newtonsoft.Json.JsonReader::ReadIntoWrappedTypeObject()
extern void JsonReader_ReadIntoWrappedTypeObject_m94C238BD063EA6B635748029F49DDAA99973A0C2 ();
// 0x00000080 System.Void Newtonsoft.Json.JsonReader::Skip()
extern void JsonReader_Skip_m9BC7ACC43FC591D25DB584EDEE48E8565E81C7A2 ();
// 0x00000081 System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_SetToken_mF760E05707D8BBB9BAFE7F881EDCDCC5F041FDB9 ();
// 0x00000082 System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonReader_SetToken_m25B74EA504701DD5727894D38E21127DA43EFF71 ();
// 0x00000083 System.Void Newtonsoft.Json.JsonReader::SetToken(Newtonsoft.Json.JsonToken,System.Object,System.Boolean)
extern void JsonReader_SetToken_m03EFB6B1B50834851EE1000E0F930625DE3065BA ();
// 0x00000084 System.Void Newtonsoft.Json.JsonReader::SetPostValueState(System.Boolean)
extern void JsonReader_SetPostValueState_m84FF7954F1945C5AA9F3C12E7AC5DE7D82B14EC0 ();
// 0x00000085 System.Void Newtonsoft.Json.JsonReader::UpdateScopeWithFinishedValue()
extern void JsonReader_UpdateScopeWithFinishedValue_m4EF50B861EA117072A615EA48FA64A1FEFEBF064 ();
// 0x00000086 System.Void Newtonsoft.Json.JsonReader::ValidateEnd(Newtonsoft.Json.JsonToken)
extern void JsonReader_ValidateEnd_m95DCF892763FD53CC04B998713E7DACD7B133970 ();
// 0x00000087 System.Void Newtonsoft.Json.JsonReader::SetStateBasedOnCurrent()
extern void JsonReader_SetStateBasedOnCurrent_m287B22E68E1AD9A58596E549D67B96032398F119 ();
// 0x00000088 System.Void Newtonsoft.Json.JsonReader::SetFinished()
extern void JsonReader_SetFinished_m161F3CE2916D0B28CBAF7D7EB9483B8EB81D498C ();
// 0x00000089 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonReader::GetTypeForCloseToken(Newtonsoft.Json.JsonToken)
extern void JsonReader_GetTypeForCloseToken_mEC31AA4B686704C087546D2DB97DE4F5CAC1FCA5 ();
// 0x0000008A System.Void Newtonsoft.Json.JsonReader::System.IDisposable.Dispose()
extern void JsonReader_System_IDisposable_Dispose_m25183F5F0B76E01282CFE99189415B2A8F8AF338 ();
// 0x0000008B System.Void Newtonsoft.Json.JsonReader::Dispose(System.Boolean)
extern void JsonReader_Dispose_mE2A0BF3EB0E0C47A8544D206CF4291460BAFE194 ();
// 0x0000008C System.Void Newtonsoft.Json.JsonReader::Close()
extern void JsonReader_Close_mA4BD0D0E0F0BF1053F6E71D704690558AE4B3CEA ();
// 0x0000008D System.Void Newtonsoft.Json.JsonReader::ReadAndAssert()
extern void JsonReader_ReadAndAssert_m60377A4E982731616E2DD35DC0C776E555BD2403 ();
// 0x0000008E System.Void Newtonsoft.Json.JsonReader::ReadForTypeAndAssert(Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern void JsonReader_ReadForTypeAndAssert_mFC6BE5BBA870DDB1F58898805BA3C9148BF271D8 ();
// 0x0000008F System.Boolean Newtonsoft.Json.JsonReader::ReadForType(Newtonsoft.Json.Serialization.JsonContract,System.Boolean)
extern void JsonReader_ReadForType_mCA99B3BA3FED0DF53C6300F473E98D51BF90147B ();
// 0x00000090 System.Boolean Newtonsoft.Json.JsonReader::ReadAndMoveToContent()
extern void JsonReader_ReadAndMoveToContent_mC9B4355733551D774CF9BBB87CE54FC4BD8D55E9 ();
// 0x00000091 System.Boolean Newtonsoft.Json.JsonReader::MoveToContent()
extern void JsonReader_MoveToContent_m572417EEF0425D17E05D87991B6CC73345304D3E ();
// 0x00000092 Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::GetContentToken()
extern void JsonReader_GetContentToken_m18467A9E395500A24458B5071383A21746AFB267 ();
// 0x00000093 System.Void Newtonsoft.Json.JsonReaderException::.ctor()
extern void JsonReaderException__ctor_m17709A483BE5AFC82C9CD4C1392A1B482EA02612 ();
// 0x00000094 System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonReaderException__ctor_mE599F48E0D998BA4D43BA76F6B09FFD5FD974199 ();
// 0x00000095 System.Void Newtonsoft.Json.JsonReaderException::.ctor(System.String,System.String,System.Int32,System.Int32,System.Exception)
extern void JsonReaderException__ctor_m3F643A49E0E96AAD530F9DCE7041164D49D2B011 ();
// 0x00000096 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonReaderException_Create_mBFCDB362520B63E24F19F7F668A7D1A26D244393 ();
// 0x00000097 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonReaderException_Create_mCA27B10DAC0A21E578CB9A737471CA8CC8857ECC ();
// 0x00000098 Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonReaderException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonReaderException_Create_m1C6DF4EDA32CB02CD7EF0935DF9F923A200476BC ();
// 0x00000099 System.Void Newtonsoft.Json.JsonSerializationException::.ctor()
extern void JsonSerializationException__ctor_m634DBDBB07F3DD2925EE7471AF309BC81AFFA46F ();
// 0x0000009A System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String)
extern void JsonSerializationException__ctor_m362C8866E990FE51924BD56A23183697D88120B6 ();
// 0x0000009B System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String,System.Exception)
extern void JsonSerializationException__ctor_mC0D8B49862C150AB65EF289D9F9AEE4DD6FFD00D ();
// 0x0000009C System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonSerializationException__ctor_m8CCDC62941FAD083CE6596C4847D56F6A30C63F8 ();
// 0x0000009D System.Void Newtonsoft.Json.JsonSerializationException::.ctor(System.String,System.String,System.Int32,System.Int32,System.Exception)
extern void JsonSerializationException__ctor_mFC075421E04C9719868FE9892B672169E978972E ();
// 0x0000009E Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializationException_Create_mBD96D2D781F3F0136C9C7178922E6C132DCA463A ();
// 0x0000009F Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.JsonReader,System.String,System.Exception)
extern void JsonSerializationException_Create_mC7AC79438307341E50881B959F24FE18E002FA6E ();
// 0x000000A0 Newtonsoft.Json.JsonSerializationException Newtonsoft.Json.JsonSerializationException::Create(Newtonsoft.Json.IJsonLineInfo,System.String,System.String,System.Exception)
extern void JsonSerializationException_Create_m9513A22FA51B54FF18C132C275080310B0FDA5CF ();
// 0x000000A1 System.Void Newtonsoft.Json.JsonSerializer::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_add_Error_mB0BCA4E7A163D55B673D8F4EBDA37D01C836F77C ();
// 0x000000A2 System.Void Newtonsoft.Json.JsonSerializer::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializer_remove_Error_m8B2DEDCADD72698B1980AF73482C589B88AA2A8F ();
// 0x000000A3 System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializer_set_ReferenceResolver_mF8FC1A12F8D2D2216FD728747A541A03D2C85168 ();
// 0x000000A4 System.Void Newtonsoft.Json.JsonSerializer::set_SerializationBinder(Newtonsoft.Json.Serialization.ISerializationBinder)
extern void JsonSerializer_set_SerializationBinder_mD31607A4C5768610E17CF68DE5777F0679DE963C ();
// 0x000000A5 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::get_TraceWriter()
extern void JsonSerializer_get_TraceWriter_m1E6DE87F57E994741B993145B1759C3238047AE1 ();
// 0x000000A6 System.Void Newtonsoft.Json.JsonSerializer::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializer_set_TraceWriter_m1E9B2458F21E89A819B06AF56B42F9AFD65A0DDB ();
// 0x000000A7 System.Void Newtonsoft.Json.JsonSerializer::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializer_set_EqualityComparer_mB9463E38FEDF3D4C8EAD0B6A75C448BF619FB29B ();
// 0x000000A8 System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializer_set_TypeNameHandling_m5BFFBC69CCA954FE7114D213E2D9CC36342D9ACF ();
// 0x000000A9 System.Void Newtonsoft.Json.JsonSerializer::set_TypeNameAssemblyFormatHandling(Newtonsoft.Json.TypeNameAssemblyFormatHandling)
extern void JsonSerializer_set_TypeNameAssemblyFormatHandling_mDDAD0FE4E0E32119A1BB825905713396ECA2DE21 ();
// 0x000000AA System.Void Newtonsoft.Json.JsonSerializer::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializer_set_PreserveReferencesHandling_m40B87C043D190DCDF2B8235061FADF119886F731 ();
// 0x000000AB System.Void Newtonsoft.Json.JsonSerializer::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializer_set_ReferenceLoopHandling_m4A23E49D3E379B1388F59CE7D9934F05E3966F27 ();
// 0x000000AC System.Void Newtonsoft.Json.JsonSerializer::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializer_set_MissingMemberHandling_mDED52F36DE2DE2A69A03CBA1FB1A377E56A35503 ();
// 0x000000AD Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::get_NullValueHandling()
extern void JsonSerializer_get_NullValueHandling_mE0CE90ACD6A1D02EF50C278AF30AF75F54CC6681 ();
// 0x000000AE System.Void Newtonsoft.Json.JsonSerializer::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializer_set_NullValueHandling_m3AFA3FE7E0901A608245E5D66BF87418F8FFA324 ();
// 0x000000AF System.Void Newtonsoft.Json.JsonSerializer::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializer_set_DefaultValueHandling_mDA03DA259E4C3ECAAE2CEBA61FB576A2FF05965A ();
// 0x000000B0 System.Void Newtonsoft.Json.JsonSerializer::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializer_set_ObjectCreationHandling_m91A704DEC7B648681D14B0A143702F03B02C4B41 ();
// 0x000000B1 System.Void Newtonsoft.Json.JsonSerializer::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializer_set_ConstructorHandling_mAB93A88F957789A5137AF24DAF561D12E702A583 ();
// 0x000000B2 Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::get_MetadataPropertyHandling()
extern void JsonSerializer_get_MetadataPropertyHandling_m3BF51DB8C02F4B8BD2DBC1817482EBF4F915FD67 ();
// 0x000000B3 System.Void Newtonsoft.Json.JsonSerializer::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializer_set_MetadataPropertyHandling_m2163D5C589945BB28D6685E4F3D5DDFBC94D378D ();
// 0x000000B4 Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::get_Converters()
extern void JsonSerializer_get_Converters_m7E3D7FD78BAA52920392FECC1D162BACBB307A06 ();
// 0x000000B5 Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::get_ContractResolver()
extern void JsonSerializer_get_ContractResolver_m9DCE120E6F5FEED68FD45C9744F09D3DC3BF0DEE ();
// 0x000000B6 System.Void Newtonsoft.Json.JsonSerializer::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializer_set_ContractResolver_m23E7AC4F5B365A1433D65CC6520023A8FB7CCB2A ();
// 0x000000B7 System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::get_Context()
extern void JsonSerializer_get_Context_m3CCAF37318CD5D9B45D01F7446C78205CDBC2EF3 ();
// 0x000000B8 System.Void Newtonsoft.Json.JsonSerializer::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializer_set_Context_mC05152A1D91767ED3CB229CEE539E9824588F52B ();
// 0x000000B9 Newtonsoft.Json.Formatting Newtonsoft.Json.JsonSerializer::get_Formatting()
extern void JsonSerializer_get_Formatting_m526158D742DE52EF751C9B5F7CD3CBF2F81771DC ();
// 0x000000BA System.Void Newtonsoft.Json.JsonSerializer::set_Formatting(Newtonsoft.Json.Formatting)
extern void JsonSerializer_set_Formatting_m3E116ED355352780B0E2B666C1FC66B60CD08018 ();
// 0x000000BB System.Boolean Newtonsoft.Json.JsonSerializer::get_CheckAdditionalContent()
extern void JsonSerializer_get_CheckAdditionalContent_mFAFF37D6C1A793D156EEF98EB265296F68618BA4 ();
// 0x000000BC System.Void Newtonsoft.Json.JsonSerializer::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializer_set_CheckAdditionalContent_m32C104748C50C14A22C25E465B6C51ADEE7406A8 ();
// 0x000000BD System.Boolean Newtonsoft.Json.JsonSerializer::IsCheckAdditionalContentSet()
extern void JsonSerializer_IsCheckAdditionalContentSet_m0ECE9513F8BCFC08C239F228D5363E07FA0D5A6F ();
// 0x000000BE System.Void Newtonsoft.Json.JsonSerializer::.ctor()
extern void JsonSerializer__ctor_mAD92BF954F1F4AE0307D54EC58195F80C62584F6 ();
// 0x000000BF Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create()
extern void JsonSerializer_Create_m38AE1D6F6AC13F2B1886F944837888AAD480BBBB ();
// 0x000000C0 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::Create(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_Create_mD5BACE2C41A3569A68146E9ADBC2E16AE11BC534 ();
// 0x000000C1 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault()
extern void JsonSerializer_CreateDefault_mFEA7B08199B331CF80CE4B7E1051E362B117DDDD ();
// 0x000000C2 Newtonsoft.Json.JsonSerializer Newtonsoft.Json.JsonSerializer::CreateDefault(Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_CreateDefault_m8982D86A2D017A4AB3AD8F8F65CCDDD8A65C649E ();
// 0x000000C3 System.Void Newtonsoft.Json.JsonSerializer::ApplySerializerSettings(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonSerializerSettings)
extern void JsonSerializer_ApplySerializerSettings_m68F831B611204D7823B12684ED97AF46CAEAC6F2 ();
// 0x000000C4 T Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader)
// 0x000000C5 System.Object Newtonsoft.Json.JsonSerializer::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_Deserialize_mF21BAE6FBB07736E98FE3E568C97A789BC69F741 ();
// 0x000000C6 System.Object Newtonsoft.Json.JsonSerializer::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializer_DeserializeInternal_m4AA70E53989355DDCC5AA2C830504A08E0B3F7DB ();
// 0x000000C7 System.Void Newtonsoft.Json.JsonSerializer::SetupReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo&,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>&,System.Nullable`1<Newtonsoft.Json.DateParseHandling>&,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>&,System.Nullable`1<System.Int32>&,System.String&)
extern void JsonSerializer_SetupReader_m9BF978B3F5E872B4CAAD0DF7FB834A162564DD7A ();
// 0x000000C8 System.Void Newtonsoft.Json.JsonSerializer::ResetReader(Newtonsoft.Json.JsonReader,System.Globalization.CultureInfo,System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>,System.Nullable`1<Newtonsoft.Json.DateParseHandling>,System.Nullable`1<Newtonsoft.Json.FloatParseHandling>,System.Nullable`1<System.Int32>,System.String)
extern void JsonSerializer_ResetReader_mFFAC8C34425014083E41FCB09FF5B7CE32201118 ();
// 0x000000C9 System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_Serialize_m47CE5177BE3A0035C88BD9E194CEE0EB06315190 ();
// 0x000000CA System.Void Newtonsoft.Json.JsonSerializer::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializer_Serialize_m4D67925906BBB7B57B032021F0E27854800C7660 ();
// 0x000000CB Newtonsoft.Json.Serialization.TraceJsonReader Newtonsoft.Json.JsonSerializer::CreateTraceJsonReader(Newtonsoft.Json.JsonReader)
extern void JsonSerializer_CreateTraceJsonReader_m0622574AFAE5E8EB1F714FD1CF649DD92F78F19B ();
// 0x000000CC System.Void Newtonsoft.Json.JsonSerializer::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializer_SerializeInternal_m35515DF8607422337C334DAE42EDF9832A7F9BF6 ();
// 0x000000CD Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::GetReferenceResolver()
extern void JsonSerializer_GetReferenceResolver_m90EA406FF935E63E4F0502760993BBB529E8CB7A ();
// 0x000000CE Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Type)
extern void JsonSerializer_GetMatchingConverter_m18934A32207B9B0BE108A745033DC6DDC8B324C2 ();
// 0x000000CF Newtonsoft.Json.JsonConverter Newtonsoft.Json.JsonSerializer::GetMatchingConverter(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern void JsonSerializer_GetMatchingConverter_m1353F204DEC337892BDA3D6EE9FFDDF5EF6525C5 ();
// 0x000000D0 System.Void Newtonsoft.Json.JsonSerializer::OnError(Newtonsoft.Json.Serialization.ErrorEventArgs)
extern void JsonSerializer_OnError_m289078F0F035DB054768AE2F12C4A84E4FE5A3FA ();
// 0x000000D1 Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializerSettings::get_ReferenceLoopHandling()
extern void JsonSerializerSettings_get_ReferenceLoopHandling_mAFBACB55BFAA3628BF251BA6093398F8E4FABF09 ();
// 0x000000D2 Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializerSettings::get_MissingMemberHandling()
extern void JsonSerializerSettings_get_MissingMemberHandling_m38ACF479C56819F31A97FA1F699C4BF9509DFCEE ();
// 0x000000D3 Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializerSettings::get_ObjectCreationHandling()
extern void JsonSerializerSettings_get_ObjectCreationHandling_m2E22656E2D4167EF9E800F449CB150DDF8E9B8DF ();
// 0x000000D4 Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializerSettings::get_NullValueHandling()
extern void JsonSerializerSettings_get_NullValueHandling_m9B348EE414738F88AEE2B1177736BA48409D70D3 ();
// 0x000000D5 Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializerSettings::get_DefaultValueHandling()
extern void JsonSerializerSettings_get_DefaultValueHandling_mDF585B9DA1BFCB513677851151EB6FDE7D844032 ();
// 0x000000D6 System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter> Newtonsoft.Json.JsonSerializerSettings::get_Converters()
extern void JsonSerializerSettings_get_Converters_m2082CB93FEE2556D1DD20EE22FA96E91C91AD254 ();
// 0x000000D7 System.Void Newtonsoft.Json.JsonSerializerSettings::set_Converters(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>)
extern void JsonSerializerSettings_set_Converters_mE404F2FFA8927D289E012E487733071355E03582 ();
// 0x000000D8 Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializerSettings::get_PreserveReferencesHandling()
extern void JsonSerializerSettings_get_PreserveReferencesHandling_m3218CAD106363185DF0345F68ADA6F818E280BF3 ();
// 0x000000D9 Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameHandling()
extern void JsonSerializerSettings_get_TypeNameHandling_mF037D0B9A9BD973B3B489697FD61CFFCE2FB9071 ();
// 0x000000DA Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializerSettings::get_MetadataPropertyHandling()
extern void JsonSerializerSettings_get_MetadataPropertyHandling_mEB517B5FFC03BF2DDD812B93BC515E02B9FAEF8B ();
// 0x000000DB Newtonsoft.Json.TypeNameAssemblyFormatHandling Newtonsoft.Json.JsonSerializerSettings::get_TypeNameAssemblyFormatHandling()
extern void JsonSerializerSettings_get_TypeNameAssemblyFormatHandling_mABA42D2E33149757F1873B2CCBD45B81AD9F2923 ();
// 0x000000DC Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializerSettings::get_ConstructorHandling()
extern void JsonSerializerSettings_get_ConstructorHandling_mBB0D4BCACC023AA9A26B86584245804EB7284694 ();
// 0x000000DD Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializerSettings::get_ContractResolver()
extern void JsonSerializerSettings_get_ContractResolver_mC8D819BE1B3693E1662B298A4C0F148D9CD5121F ();
// 0x000000DE System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializerSettings::get_EqualityComparer()
extern void JsonSerializerSettings_get_EqualityComparer_m3D32A0754A35A1C75268260645EAB0CC9FA1EFDC ();
// 0x000000DF System.Func`1<Newtonsoft.Json.Serialization.IReferenceResolver> Newtonsoft.Json.JsonSerializerSettings::get_ReferenceResolverProvider()
extern void JsonSerializerSettings_get_ReferenceResolverProvider_m500B89B016F7144AC35F639C70A9BB1A81F87616 ();
// 0x000000E0 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializerSettings::get_TraceWriter()
extern void JsonSerializerSettings_get_TraceWriter_m2554972134F06B7A10FF17897569E0817C9907B2 ();
// 0x000000E1 Newtonsoft.Json.Serialization.ISerializationBinder Newtonsoft.Json.JsonSerializerSettings::get_SerializationBinder()
extern void JsonSerializerSettings_get_SerializationBinder_m3FE5BC66686F5A035E6BF30B7308FAAC7F92C327 ();
// 0x000000E2 System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializerSettings::get_Error()
extern void JsonSerializerSettings_get_Error_m132FFF7E6DAA4BA2910806152F01DC4EC06C9147 ();
// 0x000000E3 System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializerSettings::get_Context()
extern void JsonSerializerSettings_get_Context_m2217C6A8A494E2CD2DA054E1D8305BD3BD269686 ();
// 0x000000E4 System.Void Newtonsoft.Json.JsonSerializerSettings::.cctor()
extern void JsonSerializerSettings__cctor_mF16A8C54E3C80878C036C68EE9BD0C28518AEC48 ();
// 0x000000E5 System.Void Newtonsoft.Json.JsonSerializerSettings::.ctor()
extern void JsonSerializerSettings__ctor_m843CA829AC2170EBC1F50AA7A487380A6D16D3AF ();
// 0x000000E6 System.Void Newtonsoft.Json.JsonTextReader::.ctor(System.IO.TextReader)
extern void JsonTextReader__ctor_mD757FE66102CF9BDD13810CA37A3983C6B0495C6 ();
// 0x000000E7 Newtonsoft.Json.JsonNameTable Newtonsoft.Json.JsonTextReader::get_PropertyNameTable()
extern void JsonTextReader_get_PropertyNameTable_mCD88FDBD5C8CC99817BECAED46DC2AEACA54C96A ();
// 0x000000E8 System.Void Newtonsoft.Json.JsonTextReader::set_PropertyNameTable(Newtonsoft.Json.JsonNameTable)
extern void JsonTextReader_set_PropertyNameTable_m3DCC659AF803257DA259127CDC8981E2F13879FC ();
// 0x000000E9 System.Void Newtonsoft.Json.JsonTextReader::EnsureBufferNotEmpty()
extern void JsonTextReader_EnsureBufferNotEmpty_m513F7E24DC100EB1BEE528ADC0CE37E420F220F6 ();
// 0x000000EA System.Void Newtonsoft.Json.JsonTextReader::SetNewLine(System.Boolean)
extern void JsonTextReader_SetNewLine_mC677256B86304E019294AAC4063D23D67B5350EA ();
// 0x000000EB System.Void Newtonsoft.Json.JsonTextReader::OnNewLine(System.Int32)
extern void JsonTextReader_OnNewLine_m6B048A456F5BC924D60961806C51602E4AE79CE8 ();
// 0x000000EC System.Void Newtonsoft.Json.JsonTextReader::ParseString(System.Char,Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseString_m7AD39F57DFD2AE50822189F23D76D720FA39A132 ();
// 0x000000ED System.Void Newtonsoft.Json.JsonTextReader::ParseReadString(System.Char,Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseReadString_m75B563FB0DD5D0B814F26118DF905223E5568EBA ();
// 0x000000EE System.Void Newtonsoft.Json.JsonTextReader::BlockCopyChars(System.Char[],System.Int32,System.Char[],System.Int32,System.Int32)
extern void JsonTextReader_BlockCopyChars_m24FDD693272A89733F0287385CE27F79D5807B93 ();
// 0x000000EF System.Void Newtonsoft.Json.JsonTextReader::ShiftBufferIfNeeded()
extern void JsonTextReader_ShiftBufferIfNeeded_m8E175EB836A7AC8CEA617EFDD3629E8242629B62 ();
// 0x000000F0 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean)
extern void JsonTextReader_ReadData_m87C1A07DD90BF68CE245BED20ABA4997FF930535 ();
// 0x000000F1 System.Void Newtonsoft.Json.JsonTextReader::PrepareBufferForReadData(System.Boolean,System.Int32)
extern void JsonTextReader_PrepareBufferForReadData_m7092C65BF7691B6896327A21FFC7EF475705AC45 ();
// 0x000000F2 System.Int32 Newtonsoft.Json.JsonTextReader::ReadData(System.Boolean,System.Int32)
extern void JsonTextReader_ReadData_m9F47BF6E23F177F044F02108CD0C843F8A2C327F ();
// 0x000000F3 System.Boolean Newtonsoft.Json.JsonTextReader::EnsureChars(System.Int32,System.Boolean)
extern void JsonTextReader_EnsureChars_mD53AB0A9160D2526564C88CC63BF6BEF6AB4CA81 ();
// 0x000000F4 System.Boolean Newtonsoft.Json.JsonTextReader::ReadChars(System.Int32,System.Boolean)
extern void JsonTextReader_ReadChars_m9B138130964FE4738AB1C49878D2AF1CCB07F703 ();
// 0x000000F5 System.Boolean Newtonsoft.Json.JsonTextReader::Read()
extern void JsonTextReader_Read_mEE8C16634337970B5F1C65F5EC359F992CD3CD2F ();
// 0x000000F6 System.Nullable`1<System.Int32> Newtonsoft.Json.JsonTextReader::ReadAsInt32()
extern void JsonTextReader_ReadAsInt32_m2469B2B1E5A50D5D6FFF56839D74ACA81B48F038 ();
// 0x000000F7 System.Nullable`1<System.DateTime> Newtonsoft.Json.JsonTextReader::ReadAsDateTime()
extern void JsonTextReader_ReadAsDateTime_mF001194EA3CB46A1960BED74CEF20AB0352FB9E9 ();
// 0x000000F8 System.String Newtonsoft.Json.JsonTextReader::ReadAsString()
extern void JsonTextReader_ReadAsString_mED6FB834CC6F852E68B8E23C41B033DC5C9F374C ();
// 0x000000F9 System.Byte[] Newtonsoft.Json.JsonTextReader::ReadAsBytes()
extern void JsonTextReader_ReadAsBytes_m22C65609230391B135110B140A25CEA6612F6C28 ();
// 0x000000FA System.Object Newtonsoft.Json.JsonTextReader::ReadStringValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadStringValue_mFE19FD5EAF58D0D307C00B21EB0B450DC97A9413 ();
// 0x000000FB System.Object Newtonsoft.Json.JsonTextReader::FinishReadQuotedStringValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_FinishReadQuotedStringValue_m47A54F9C73474D1D4797AA751B81642F38E128AB ();
// 0x000000FC Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::CreateUnexpectedCharacterException(System.Char)
extern void JsonTextReader_CreateUnexpectedCharacterException_m0B919A9C1EFD7EB9F36815B8D19249B559DA5590 ();
// 0x000000FD System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonTextReader::ReadAsBoolean()
extern void JsonTextReader_ReadAsBoolean_m94AC14A953F4912D5477F1B3ADF6EB4EF0A62A4B ();
// 0x000000FE System.Void Newtonsoft.Json.JsonTextReader::ProcessValueComma()
extern void JsonTextReader_ProcessValueComma_mB8631556021FCB76F1B8555DE168A523CE1E55C9 ();
// 0x000000FF System.Object Newtonsoft.Json.JsonTextReader::ReadNumberValue(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ReadNumberValue_mD32A235B467A325B973FF183AA7B14344C347DEA ();
// 0x00000100 System.Object Newtonsoft.Json.JsonTextReader::FinishReadQuotedNumber(Newtonsoft.Json.ReadType)
extern void JsonTextReader_FinishReadQuotedNumber_m50221D0C91C3AED53B94F98B44EE99E3DD13935E ();
// 0x00000101 System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.JsonTextReader::ReadAsDateTimeOffset()
extern void JsonTextReader_ReadAsDateTimeOffset_m325F1FFC2497EC35DE57C2BB736F88CD810D3B95 ();
// 0x00000102 System.Nullable`1<System.Decimal> Newtonsoft.Json.JsonTextReader::ReadAsDecimal()
extern void JsonTextReader_ReadAsDecimal_m8C890DFD8722407E1D6B48AFCC9A74C71616DC8C ();
// 0x00000103 System.Nullable`1<System.Double> Newtonsoft.Json.JsonTextReader::ReadAsDouble()
extern void JsonTextReader_ReadAsDouble_m60DAD9A7FF93E0BABBC303769BE48AC5DC5D77E2 ();
// 0x00000104 System.Void Newtonsoft.Json.JsonTextReader::HandleNull()
extern void JsonTextReader_HandleNull_m6373EA41EA55987A3A9946CF7D218EF35F434B24 ();
// 0x00000105 System.Void Newtonsoft.Json.JsonTextReader::ReadFinished()
extern void JsonTextReader_ReadFinished_m9AF6D1992F4416B85F0850D3AC59F32BDBE7E2DB ();
// 0x00000106 System.Boolean Newtonsoft.Json.JsonTextReader::ReadNullChar()
extern void JsonTextReader_ReadNullChar_mC05A3E61BFA83892FE671635D25F437CDAFF522E ();
// 0x00000107 System.Void Newtonsoft.Json.JsonTextReader::EnsureBuffer()
extern void JsonTextReader_EnsureBuffer_m2089B726D8BD8CCBF2B02A6D92839ED3A50C26F1 ();
// 0x00000108 System.Void Newtonsoft.Json.JsonTextReader::ReadStringIntoBuffer(System.Char)
extern void JsonTextReader_ReadStringIntoBuffer_mFD2D9918238BE262788BA3432D0956A538C8A96E ();
// 0x00000109 System.Void Newtonsoft.Json.JsonTextReader::FinishReadStringIntoBuffer(System.Int32,System.Int32,System.Int32)
extern void JsonTextReader_FinishReadStringIntoBuffer_m6027917EB06E9D5A86C5ADD88B08A442682A0504 ();
// 0x0000010A System.Void Newtonsoft.Json.JsonTextReader::WriteCharToBuffer(System.Char,System.Int32,System.Int32)
extern void JsonTextReader_WriteCharToBuffer_m90E413DFB9121F57D1AB5A464DEFED08FC79AE00 ();
// 0x0000010B System.Char Newtonsoft.Json.JsonTextReader::ConvertUnicode(System.Boolean)
extern void JsonTextReader_ConvertUnicode_m39CD283E5DE3D0AE1F21B813966FADBE3D862B7E ();
// 0x0000010C System.Char Newtonsoft.Json.JsonTextReader::ParseUnicode()
extern void JsonTextReader_ParseUnicode_m3A9955A2F54E01141600B4F9535D0B9D74814BEC ();
// 0x0000010D System.Void Newtonsoft.Json.JsonTextReader::ReadNumberIntoBuffer()
extern void JsonTextReader_ReadNumberIntoBuffer_m6CD1125FC52E738F1B782E3A25DF5E2C3FAC1D81 ();
// 0x0000010E System.Boolean Newtonsoft.Json.JsonTextReader::ReadNumberCharIntoBuffer(System.Char,System.Int32)
extern void JsonTextReader_ReadNumberCharIntoBuffer_m5B05D56492B99BC40CAF9CE2C008342B2DD54ACB ();
// 0x0000010F System.Void Newtonsoft.Json.JsonTextReader::ClearRecentString()
extern void JsonTextReader_ClearRecentString_mEBA79BC7ED7D388F4D213743300990C9896D8581 ();
// 0x00000110 System.Boolean Newtonsoft.Json.JsonTextReader::ParsePostValue(System.Boolean)
extern void JsonTextReader_ParsePostValue_mABCAF889C3AAE744A4ED551AFAD67AD680826AC6 ();
// 0x00000111 System.Boolean Newtonsoft.Json.JsonTextReader::ParseObject()
extern void JsonTextReader_ParseObject_mAC24C4AA2D70E220DA36B86237293157911D9E26 ();
// 0x00000112 System.Boolean Newtonsoft.Json.JsonTextReader::ParseProperty()
extern void JsonTextReader_ParseProperty_m652D117928358983ACB9D4BD43B89829E2FD2587 ();
// 0x00000113 System.Boolean Newtonsoft.Json.JsonTextReader::ValidIdentifierChar(System.Char)
extern void JsonTextReader_ValidIdentifierChar_m8091FEC53B3583B0EAB8D67EA1E42C4EEEDFEBED ();
// 0x00000114 System.Void Newtonsoft.Json.JsonTextReader::ParseUnquotedProperty()
extern void JsonTextReader_ParseUnquotedProperty_mD0915F072E723B0B7B47D52E4118EF710A765472 ();
// 0x00000115 System.Boolean Newtonsoft.Json.JsonTextReader::ReadUnquotedPropertyReportIfDone(System.Char,System.Int32)
extern void JsonTextReader_ReadUnquotedPropertyReportIfDone_mC002DD09CE9C0FFE2D0FB2069439F735A8F6E582 ();
// 0x00000116 System.Boolean Newtonsoft.Json.JsonTextReader::ParseValue()
extern void JsonTextReader_ParseValue_mD2F36D571A869D97A8F6217B8B0288A8A0498B9D ();
// 0x00000117 System.Void Newtonsoft.Json.JsonTextReader::ProcessLineFeed()
extern void JsonTextReader_ProcessLineFeed_m448B054CE009287F09571D9B28F3564B283BA3EA ();
// 0x00000118 System.Void Newtonsoft.Json.JsonTextReader::ProcessCarriageReturn(System.Boolean)
extern void JsonTextReader_ProcessCarriageReturn_m275932A53EA9CA67A70D2FCA5173680A971DEDE1 ();
// 0x00000119 System.Void Newtonsoft.Json.JsonTextReader::EatWhitespace()
extern void JsonTextReader_EatWhitespace_m66536C21721CC37BE7F02995ABD04F41C5A98196 ();
// 0x0000011A System.Void Newtonsoft.Json.JsonTextReader::ParseConstructor()
extern void JsonTextReader_ParseConstructor_m3C123404371DFED28F04A86EB1248438390BC03F ();
// 0x0000011B System.Void Newtonsoft.Json.JsonTextReader::ParseNumber(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumber_m874FD99EC524E03531A299F64EFD9EE6A01915E8 ();
// 0x0000011C System.Void Newtonsoft.Json.JsonTextReader::ParseReadNumber(Newtonsoft.Json.ReadType,System.Char,System.Int32)
extern void JsonTextReader_ParseReadNumber_m6C29BF3BD841F7E5FA48043ADCB483E1B4FC4B84 ();
// 0x0000011D Newtonsoft.Json.JsonReaderException Newtonsoft.Json.JsonTextReader::ThrowReaderError(System.String,System.Exception)
extern void JsonTextReader_ThrowReaderError_m45EAB30E55AFDCE3AACC56471EA3866F1FC7A778 ();
// 0x0000011E System.Object Newtonsoft.Json.JsonTextReader::BigIntegerParse(System.String,System.Globalization.CultureInfo)
extern void JsonTextReader_BigIntegerParse_m70F36467D855F38C88BEF9994EDD909827292C5B ();
// 0x0000011F System.Void Newtonsoft.Json.JsonTextReader::ParseComment(System.Boolean)
extern void JsonTextReader_ParseComment_m1D2B00F2BDE8919D8B1227C07AA28AADFD5806F8 ();
// 0x00000120 System.Void Newtonsoft.Json.JsonTextReader::EndComment(System.Boolean,System.Int32,System.Int32)
extern void JsonTextReader_EndComment_mCC46197E5124182D7D6DA4F88F1BD6164F97693E ();
// 0x00000121 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.String)
extern void JsonTextReader_MatchValue_m1935A7A541C6CCC3E5F5A357FD1E315A3244B973 ();
// 0x00000122 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValue(System.Boolean,System.String)
extern void JsonTextReader_MatchValue_mC3BD53BF3DF0C54C2C992E2AB371F3BA63405266 ();
// 0x00000123 System.Boolean Newtonsoft.Json.JsonTextReader::MatchValueWithTrailingSeparator(System.String)
extern void JsonTextReader_MatchValueWithTrailingSeparator_mE11D3741D39DF8D6B38FAD8822E509BA4086734A ();
// 0x00000124 System.Boolean Newtonsoft.Json.JsonTextReader::IsSeparator(System.Char)
extern void JsonTextReader_IsSeparator_mF5989F18CD1654E53DCD1FE2DD23D3B5BCF674B2 ();
// 0x00000125 System.Void Newtonsoft.Json.JsonTextReader::ParseTrue()
extern void JsonTextReader_ParseTrue_m1A10CCF29E57F73C0A73ADF2ED1EAD4773E8A290 ();
// 0x00000126 System.Void Newtonsoft.Json.JsonTextReader::ParseNull()
extern void JsonTextReader_ParseNull_m7AA2765C38C516AF2C00F61A733CC858602B7044 ();
// 0x00000127 System.Void Newtonsoft.Json.JsonTextReader::ParseUndefined()
extern void JsonTextReader_ParseUndefined_mDF9F2DD5D7FBC845840EFD632CF795200A6DEED0 ();
// 0x00000128 System.Void Newtonsoft.Json.JsonTextReader::ParseFalse()
extern void JsonTextReader_ParseFalse_mB7784D257AE3B7CDB89E3644D07CEE2B08E5D10E ();
// 0x00000129 System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNegativeInfinity_mD9C1D4A929B849432287A1327A7A52B1F2F27D76 ();
// 0x0000012A System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNegativeInfinity(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberNegativeInfinity_m7962F2C985883617C0DBA6ED10F3B80F628B7763 ();
// 0x0000012B System.Object Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberPositiveInfinity_m3055DEA9060BAFE180CDF0625E2A737F4F56110B ();
// 0x0000012C System.Object Newtonsoft.Json.JsonTextReader::ParseNumberPositiveInfinity(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberPositiveInfinity_mBC768D585BD1832D28B9EABEC5743D918135F0CE ();
// 0x0000012D System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNaN(Newtonsoft.Json.ReadType)
extern void JsonTextReader_ParseNumberNaN_m581DCE13BE84E85D021143EADB3D4468351A6217 ();
// 0x0000012E System.Object Newtonsoft.Json.JsonTextReader::ParseNumberNaN(Newtonsoft.Json.ReadType,System.Boolean)
extern void JsonTextReader_ParseNumberNaN_m344AF9F28AF46B53873F374BEAB69230A976FC25 ();
// 0x0000012F System.Void Newtonsoft.Json.JsonTextReader::Close()
extern void JsonTextReader_Close_m7D3AF83A69FDABDC83B3B117F7A51D503B504C7D ();
// 0x00000130 System.Boolean Newtonsoft.Json.JsonTextReader::HasLineInfo()
extern void JsonTextReader_HasLineInfo_m8932706F6A3EAA4D2C38A5256B3C934E75D8529B ();
// 0x00000131 System.Int32 Newtonsoft.Json.JsonTextReader::get_LineNumber()
extern void JsonTextReader_get_LineNumber_m2C75B372D6352EA272C5768E3BB15A2A0A9536C4 ();
// 0x00000132 System.Int32 Newtonsoft.Json.JsonTextReader::get_LinePosition()
extern void JsonTextReader_get_LinePosition_m3711496BA16996996E34EA89D202C41405889D02 ();
// 0x00000133 Newtonsoft.Json.Utilities.Base64Encoder Newtonsoft.Json.JsonTextWriter::get_Base64Encoder()
extern void JsonTextWriter_get_Base64Encoder_m9916FB0D93EDCAEB9BABDD34A4C717201A193127 ();
// 0x00000134 System.Char Newtonsoft.Json.JsonTextWriter::get_QuoteChar()
extern void JsonTextWriter_get_QuoteChar_mE8C6518BDF2FB09C6958EFB2EC09C7226310210E ();
// 0x00000135 System.Void Newtonsoft.Json.JsonTextWriter::.ctor(System.IO.TextWriter)
extern void JsonTextWriter__ctor_mEB186DB56607F358929B1337AE02E17E1B6E9AB4 ();
// 0x00000136 System.Void Newtonsoft.Json.JsonTextWriter::Close()
extern void JsonTextWriter_Close_mFD4B3222BC5D7882EF56E3370BF2E6891FB01ECA ();
// 0x00000137 System.Void Newtonsoft.Json.JsonTextWriter::CloseBufferAndWriter()
extern void JsonTextWriter_CloseBufferAndWriter_m6ECC0059C657EF0CAD776CFA08922476F42FAF12 ();
// 0x00000138 System.Void Newtonsoft.Json.JsonTextWriter::WriteStartObject()
extern void JsonTextWriter_WriteStartObject_m7C36C434D9FDAE13C7205FB04FFFCC06B60D1CE2 ();
// 0x00000139 System.Void Newtonsoft.Json.JsonTextWriter::WriteStartArray()
extern void JsonTextWriter_WriteStartArray_mE0274CDEADFB8619324E45FAD9663A3A8D9176C5 ();
// 0x0000013A System.Void Newtonsoft.Json.JsonTextWriter::WriteStartConstructor(System.String)
extern void JsonTextWriter_WriteStartConstructor_m959AF4B291975C89E053DFF288FF6DC1C619388A ();
// 0x0000013B System.Void Newtonsoft.Json.JsonTextWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteEnd_mE2CB8CAB0080DEEEBD560A905C3DAB233AD3F848 ();
// 0x0000013C System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String)
extern void JsonTextWriter_WritePropertyName_m7EE962BBB56396D017F0FDF93288616CA17D01CD ();
// 0x0000013D System.Void Newtonsoft.Json.JsonTextWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonTextWriter_WritePropertyName_m76AB324E7BBE746DDAA78019A5C1E6301A868173 ();
// 0x0000013E System.Void Newtonsoft.Json.JsonTextWriter::OnStringEscapeHandlingChanged()
extern void JsonTextWriter_OnStringEscapeHandlingChanged_mF766E70B02C137B011568406F68F080B03ACE65B ();
// 0x0000013F System.Void Newtonsoft.Json.JsonTextWriter::UpdateCharEscapeFlags()
extern void JsonTextWriter_UpdateCharEscapeFlags_mB3CE53DE1620FFAF5A16F97282EC76D20BD03788 ();
// 0x00000140 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndent()
extern void JsonTextWriter_WriteIndent_m9181A051C2CF29CB20A07D00629CFF6FD06EC1E3 ();
// 0x00000141 System.Int32 Newtonsoft.Json.JsonTextWriter::SetIndentChars()
extern void JsonTextWriter_SetIndentChars_m2245B846F8FF5DAD6EF49EB5DBB5D858B865F937 ();
// 0x00000142 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueDelimiter()
extern void JsonTextWriter_WriteValueDelimiter_m32651A5CF35F02B1CE5F386C91D14AA818C8170C ();
// 0x00000143 System.Void Newtonsoft.Json.JsonTextWriter::WriteIndentSpace()
extern void JsonTextWriter_WriteIndentSpace_mA19A3749FEFFFA3110AA81B3CAB25637F3E882B1 ();
// 0x00000144 System.Void Newtonsoft.Json.JsonTextWriter::WriteValueInternal(System.String,Newtonsoft.Json.JsonToken)
extern void JsonTextWriter_WriteValueInternal_mE72D8571B70C94CC3B39580AC907AC0BE0CDE30D ();
// 0x00000145 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Object)
extern void JsonTextWriter_WriteValue_m936DFF76CD5F7BF8FC8FFB1744981F4F259509AD ();
// 0x00000146 System.Void Newtonsoft.Json.JsonTextWriter::WriteNull()
extern void JsonTextWriter_WriteNull_mC368D44D4B1F71828B087A543A5768F6656A2D5C ();
// 0x00000147 System.Void Newtonsoft.Json.JsonTextWriter::WriteUndefined()
extern void JsonTextWriter_WriteUndefined_m8FE318E3DCC52C57F4A41B8A12137DC3EA060144 ();
// 0x00000148 System.Void Newtonsoft.Json.JsonTextWriter::WriteRaw(System.String)
extern void JsonTextWriter_WriteRaw_m826731AE5302BFD1A81DF25A0919BC2DD86AA2F7 ();
// 0x00000149 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.String)
extern void JsonTextWriter_WriteValue_m9B2A4778399C3A08C95790D4856DA6A2ED4B2C7B ();
// 0x0000014A System.Void Newtonsoft.Json.JsonTextWriter::WriteEscapedString(System.String,System.Boolean)
extern void JsonTextWriter_WriteEscapedString_mB0719DBFB6D26A9479A0DA874975B5F21556C885 ();
// 0x0000014B System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int32)
extern void JsonTextWriter_WriteValue_m299FAED87D3F68CE9C088F1DB0FD2BC1EC54C81A ();
// 0x0000014C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt32)
extern void JsonTextWriter_WriteValue_m5ECF2F8B9AD0FD5AF5ACE14B9BC874281D5E5F70 ();
// 0x0000014D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int64)
extern void JsonTextWriter_WriteValue_m7DA7DA5711CDFB8DE2B92F624221F64F7CCADA43 ();
// 0x0000014E System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt64)
extern void JsonTextWriter_WriteValue_mA86B7915D9E8F5E97283DD32E84FC8A7A527D2E3 ();
// 0x0000014F System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Single)
extern void JsonTextWriter_WriteValue_m2F6A5344A24B8D97E08801625537A4F452695DFD ();
// 0x00000150 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonTextWriter_WriteValue_m2084CC543850E22C4F15F4F336AEECBE87C86067 ();
// 0x00000151 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Double)
extern void JsonTextWriter_WriteValue_mEBD8C1DD1151526E950599F58750DF20CCEF9EF8 ();
// 0x00000152 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonTextWriter_WriteValue_m10DF1AB911D0EE6221ED8A10DDDE43BAC465FBBB ();
// 0x00000153 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Boolean)
extern void JsonTextWriter_WriteValue_mF8A1B627CBE8CDD02C681197AD334D9C68BF1CA4 ();
// 0x00000154 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Int16)
extern void JsonTextWriter_WriteValue_m3229AFB5B29A399DF8C3B35EF0C2200F811DC0FC ();
// 0x00000155 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.UInt16)
extern void JsonTextWriter_WriteValue_m770F089CE64BD6D2513641F9FD788415B261EEEC ();
// 0x00000156 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Char)
extern void JsonTextWriter_WriteValue_m23296089CA7E0A28102308106FA3F8E4DE9E3237 ();
// 0x00000157 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte)
extern void JsonTextWriter_WriteValue_m10DDF98FAAFDFC317F6EBDC4F79C908B2A90DD5B ();
// 0x00000158 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.SByte)
extern void JsonTextWriter_WriteValue_m43236B80B922F4AEB866DDB31A80CE8F05BF3740 ();
// 0x00000159 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Decimal)
extern void JsonTextWriter_WriteValue_m84497D2E47F08E8B2710F42781D8BDFFEA6EBDFC ();
// 0x0000015A System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTime)
extern void JsonTextWriter_WriteValue_mDA794DB59E9A287DBA36E4517A42720336AC3B6E ();
// 0x0000015B System.Int32 Newtonsoft.Json.JsonTextWriter::WriteValueToBuffer(System.DateTime)
extern void JsonTextWriter_WriteValueToBuffer_m82C026B4817F510251DB11FDC59349C7F7E815CE ();
// 0x0000015C System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Byte[])
extern void JsonTextWriter_WriteValue_m90C8C538315261FFA4A3A625581DAA8164578AFF ();
// 0x0000015D System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.DateTimeOffset)
extern void JsonTextWriter_WriteValue_m5446770BD0943EC4472B55B84E50AC6E840BF306 ();
// 0x0000015E System.Int32 Newtonsoft.Json.JsonTextWriter::WriteValueToBuffer(System.DateTimeOffset)
extern void JsonTextWriter_WriteValueToBuffer_mD7B0FA27C9F0ADA8D7F22BFAE505D4CB486167D8 ();
// 0x0000015F System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Guid)
extern void JsonTextWriter_WriteValue_m80C96737E42BB9CBACD50D613BB0FEC0825AB442 ();
// 0x00000160 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.TimeSpan)
extern void JsonTextWriter_WriteValue_m0AFA1921B23FC347893FB2F17F7ED82F973A244E ();
// 0x00000161 System.Void Newtonsoft.Json.JsonTextWriter::WriteValue(System.Uri)
extern void JsonTextWriter_WriteValue_m5758ECAE2DDB277A82B65ED9A97A9FFED0992FE5 ();
// 0x00000162 System.Void Newtonsoft.Json.JsonTextWriter::WriteComment(System.String)
extern void JsonTextWriter_WriteComment_mA639FC5D7FDC2BDE6701EEA8D4E9F10298D740B3 ();
// 0x00000163 System.Void Newtonsoft.Json.JsonTextWriter::EnsureWriteBuffer()
extern void JsonTextWriter_EnsureWriteBuffer_m3FD2D3051076873D10AB8D935CBCFAD255CA68B3 ();
// 0x00000164 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.Int64)
extern void JsonTextWriter_WriteIntegerValue_mEFE39F09FE5158AF12FFEC321E08C97CAD94AE66 ();
// 0x00000165 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.UInt64,System.Boolean)
extern void JsonTextWriter_WriteIntegerValue_m9EDB3D5AD2DBB26EF55DB66A5B76A2B813739DCA ();
// 0x00000166 System.Int32 Newtonsoft.Json.JsonTextWriter::WriteNumberToBuffer(System.UInt64,System.Boolean)
extern void JsonTextWriter_WriteNumberToBuffer_m91A3362A091E4722B1CC92C3043CFC1C67FF146B ();
// 0x00000167 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.Int32)
extern void JsonTextWriter_WriteIntegerValue_m15E1116CE9EECE48EB9E92F99FEFC299A6605F9B ();
// 0x00000168 System.Void Newtonsoft.Json.JsonTextWriter::WriteIntegerValue(System.UInt32,System.Boolean)
extern void JsonTextWriter_WriteIntegerValue_m26963CDCA8631FAF88B3DFF733A20AC973115CF8 ();
// 0x00000169 System.Int32 Newtonsoft.Json.JsonTextWriter::WriteNumberToBuffer(System.UInt32,System.Boolean)
extern void JsonTextWriter_WriteNumberToBuffer_mBB58E0DE048A6531F83F54E49CE93988E5702F44 ();
// 0x0000016A Newtonsoft.Json.JsonWriter_State[][] Newtonsoft.Json.JsonWriter::BuildStateArray()
extern void JsonWriter_BuildStateArray_mB4029DF81934CCC7012A74F314D5F4EC4DC6FC69 ();
// 0x0000016B System.Void Newtonsoft.Json.JsonWriter::.cctor()
extern void JsonWriter__cctor_m84C6852A1CC9B573989805AD4A8E0ABDC8553ECD ();
// 0x0000016C System.Boolean Newtonsoft.Json.JsonWriter::get_CloseOutput()
extern void JsonWriter_get_CloseOutput_m5FF0E3B6BB76B31A6B0308D4F66D65A33F8D93FC ();
// 0x0000016D System.Void Newtonsoft.Json.JsonWriter::set_CloseOutput(System.Boolean)
extern void JsonWriter_set_CloseOutput_mC8CA7E44AA2074EC2BE542F8D14B27A592D6940B ();
// 0x0000016E System.Boolean Newtonsoft.Json.JsonWriter::get_AutoCompleteOnClose()
extern void JsonWriter_get_AutoCompleteOnClose_m622193FA7480DC2E94FF4CA30014D1FA51F17065 ();
// 0x0000016F System.Void Newtonsoft.Json.JsonWriter::set_AutoCompleteOnClose(System.Boolean)
extern void JsonWriter_set_AutoCompleteOnClose_m9F3B204A96239ABDFBC1C0298BC4A3810C413D5C ();
// 0x00000170 System.Int32 Newtonsoft.Json.JsonWriter::get_Top()
extern void JsonWriter_get_Top_m2354A453B0DD0528B5A462112696F96682FA7248 ();
// 0x00000171 Newtonsoft.Json.WriteState Newtonsoft.Json.JsonWriter::get_WriteState()
extern void JsonWriter_get_WriteState_m017AC175CF914096FD4AD0FF4807A89BFB97E26E ();
// 0x00000172 System.String Newtonsoft.Json.JsonWriter::get_ContainerPath()
extern void JsonWriter_get_ContainerPath_m03E4F990ECB42B167BBB8E897F2ADC0E3204A39D ();
// 0x00000173 System.String Newtonsoft.Json.JsonWriter::get_Path()
extern void JsonWriter_get_Path_mC3E57CCCA1D0C5B869FD19671230F7CC636F73B5 ();
// 0x00000174 Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::get_Formatting()
extern void JsonWriter_get_Formatting_m64705DF9A1D91C9491580597585197A3B303E5A4 ();
// 0x00000175 System.Void Newtonsoft.Json.JsonWriter::set_Formatting(Newtonsoft.Json.Formatting)
extern void JsonWriter_set_Formatting_m5DB6CD74DFDD9B8D14587DD95B933035F65489FA ();
// 0x00000176 Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::get_DateFormatHandling()
extern void JsonWriter_get_DateFormatHandling_m083B2D33EEA4E87F670B2F892484089DFC8BEF27 ();
// 0x00000177 System.Void Newtonsoft.Json.JsonWriter::set_DateFormatHandling(Newtonsoft.Json.DateFormatHandling)
extern void JsonWriter_set_DateFormatHandling_m2FDED018732705C95CA2FB7A78F92DAC7418CE72 ();
// 0x00000178 Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::get_DateTimeZoneHandling()
extern void JsonWriter_get_DateTimeZoneHandling_m93B5E389843B9BCA59F5DF0EF6CEB54000071E75 ();
// 0x00000179 System.Void Newtonsoft.Json.JsonWriter::set_DateTimeZoneHandling(Newtonsoft.Json.DateTimeZoneHandling)
extern void JsonWriter_set_DateTimeZoneHandling_mDA4E11034DC93715091D58173EDF791A8E290FA4 ();
// 0x0000017A Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::get_StringEscapeHandling()
extern void JsonWriter_get_StringEscapeHandling_m712D7D3A01F8FB07140D147094B14BC322EA5CD9 ();
// 0x0000017B System.Void Newtonsoft.Json.JsonWriter::set_StringEscapeHandling(Newtonsoft.Json.StringEscapeHandling)
extern void JsonWriter_set_StringEscapeHandling_m9E210B17B4858882560226E668706C097BC37972 ();
// 0x0000017C System.Void Newtonsoft.Json.JsonWriter::OnStringEscapeHandlingChanged()
extern void JsonWriter_OnStringEscapeHandlingChanged_m1A437C6B83EEF74B13CC68FC652CDD0815828C5F ();
// 0x0000017D Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::get_FloatFormatHandling()
extern void JsonWriter_get_FloatFormatHandling_m2B9D45FA45DAC4256D52E99F512C6AD1163ACB67 ();
// 0x0000017E System.Void Newtonsoft.Json.JsonWriter::set_FloatFormatHandling(Newtonsoft.Json.FloatFormatHandling)
extern void JsonWriter_set_FloatFormatHandling_m579A35628D92FE58E6D342A621C889ABA8684829 ();
// 0x0000017F System.String Newtonsoft.Json.JsonWriter::get_DateFormatString()
extern void JsonWriter_get_DateFormatString_m9CB1F8206E0FDD6CB2538DCA5A1762478453B9B9 ();
// 0x00000180 System.Void Newtonsoft.Json.JsonWriter::set_DateFormatString(System.String)
extern void JsonWriter_set_DateFormatString_m907BEAE6972D801EFC845C344F39AB900890AB10 ();
// 0x00000181 System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::get_Culture()
extern void JsonWriter_get_Culture_m43859002F83B5F3A286FF4CFC6DAC7C55BC9E23B ();
// 0x00000182 System.Void Newtonsoft.Json.JsonWriter::set_Culture(System.Globalization.CultureInfo)
extern void JsonWriter_set_Culture_m026FA089CF142BE15CF21A6C95DA59B8C18AEA22 ();
// 0x00000183 System.Void Newtonsoft.Json.JsonWriter::.ctor()
extern void JsonWriter__ctor_m66A258C60B929D8BD124F92C63348F8C4731E90E ();
// 0x00000184 System.Void Newtonsoft.Json.JsonWriter::UpdateScopeWithFinishedValue()
extern void JsonWriter_UpdateScopeWithFinishedValue_m53DF3F5D1797C278B22FBFBE0435A6E2922B766A ();
// 0x00000185 System.Void Newtonsoft.Json.JsonWriter::Push(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_Push_m6CEFC5AE6C8554750AE111E3D70A2F0E8BA14C5D ();
// 0x00000186 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Pop()
extern void JsonWriter_Pop_m8410D60A45BDA750D2CC7CF171B59771E70AAE41 ();
// 0x00000187 Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonWriter::Peek()
extern void JsonWriter_Peek_mC209157E349F1FDF5EA189E19EB4B673501D24F3 ();
// 0x00000188 System.Void Newtonsoft.Json.JsonWriter::Close()
extern void JsonWriter_Close_m6FBCF9BF49FC7530B4BD4DA2FE5AD92681525436 ();
// 0x00000189 System.Void Newtonsoft.Json.JsonWriter::WriteStartObject()
extern void JsonWriter_WriteStartObject_m3D06C9E26B6056E8ECE2369656BD9AC5BE0ED2C5 ();
// 0x0000018A System.Void Newtonsoft.Json.JsonWriter::WriteEndObject()
extern void JsonWriter_WriteEndObject_mE0DBD023E46A8E33E7EF19C0D0B8618521CC48A1 ();
// 0x0000018B System.Void Newtonsoft.Json.JsonWriter::WriteStartArray()
extern void JsonWriter_WriteStartArray_mFAA44AB50BDC729500551CF697896B3EF8A96014 ();
// 0x0000018C System.Void Newtonsoft.Json.JsonWriter::WriteEndArray()
extern void JsonWriter_WriteEndArray_mC83C124C0145E60569E14BF5207DCA47FFF52D14 ();
// 0x0000018D System.Void Newtonsoft.Json.JsonWriter::WriteStartConstructor(System.String)
extern void JsonWriter_WriteStartConstructor_mA969BDFE136EC5FC185DDC748D6943725C2410B4 ();
// 0x0000018E System.Void Newtonsoft.Json.JsonWriter::WriteEndConstructor()
extern void JsonWriter_WriteEndConstructor_m9951DD1CEB792CBF9FE360B24C2A03052032179F ();
// 0x0000018F System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String)
extern void JsonWriter_WritePropertyName_mD397F092D5C206547C215174EAAEF8264E22650B ();
// 0x00000190 System.Void Newtonsoft.Json.JsonWriter::WritePropertyName(System.String,System.Boolean)
extern void JsonWriter_WritePropertyName_m264E6F18C94285D9C7876865DE493938EB0C9546 ();
// 0x00000191 System.Void Newtonsoft.Json.JsonWriter::WriteEnd()
extern void JsonWriter_WriteEnd_mB41CC40E8D86D8A57CD7142ACEC315EC7B62FC81 ();
// 0x00000192 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader)
extern void JsonWriter_WriteToken_mDB5563BBD03F1B1A148112246AD516AE0E8EFA52 ();
// 0x00000193 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean)
extern void JsonWriter_WriteToken_m3B7E8026BD68E5F27DE19E912E4738B02B6A737D ();
// 0x00000194 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonWriter_WriteToken_m5A89C041AD8A697864BE3D0467171FFCF4AABD21 ();
// 0x00000195 System.Void Newtonsoft.Json.JsonWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean,System.Boolean,System.Boolean)
extern void JsonWriter_WriteToken_mE72503A890E625DCAAD1B20E7A3FCD4558FFB36A ();
// 0x00000196 System.Boolean Newtonsoft.Json.JsonWriter::IsWriteTokenIncomplete(Newtonsoft.Json.JsonReader,System.Boolean,System.Int32)
extern void JsonWriter_IsWriteTokenIncomplete_m60EC01D0CE713BF95D33B9A2C34132FC1A202BA1 ();
// 0x00000197 System.Int32 Newtonsoft.Json.JsonWriter::CalculateWriteTokenInitialDepth(Newtonsoft.Json.JsonReader)
extern void JsonWriter_CalculateWriteTokenInitialDepth_m363C2E8CB31589DBF514241530BA1AFC70A55A61 ();
// 0x00000198 System.Int32 Newtonsoft.Json.JsonWriter::CalculateWriteTokenFinalDepth(Newtonsoft.Json.JsonReader)
extern void JsonWriter_CalculateWriteTokenFinalDepth_m832F054479AAD477DFB90059431476B1D97C3B5A ();
// 0x00000199 System.Void Newtonsoft.Json.JsonWriter::WriteConstructorDate(Newtonsoft.Json.JsonReader)
extern void JsonWriter_WriteConstructorDate_mCF788E169C72B7EF6C6BB8BBDDBE9EBC1A6DB96B ();
// 0x0000019A System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_WriteEnd_mA0F3BC719E8DF7264D0110970169B59491AD6CBD ();
// 0x0000019B System.Void Newtonsoft.Json.JsonWriter::AutoCompleteAll()
extern void JsonWriter_AutoCompleteAll_m29E90D16C7F4B0CA8313B7B84A993E3BC0D1A7CC ();
// 0x0000019C Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonWriter::GetCloseTokenForType(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_GetCloseTokenForType_mB22E5F1F10D778E43C58711859522FE59D172B3B ();
// 0x0000019D System.Void Newtonsoft.Json.JsonWriter::AutoCompleteClose(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_AutoCompleteClose_m5C458AF9ED22C93B9EA0D17005986322B97C16B4 ();
// 0x0000019E System.Int32 Newtonsoft.Json.JsonWriter::CalculateLevelsToComplete(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_CalculateLevelsToComplete_mE52143CBF753EC86289FB73CAC879D50E164CD7E ();
// 0x0000019F System.Void Newtonsoft.Json.JsonWriter::UpdateCurrentState()
extern void JsonWriter_UpdateCurrentState_m0C204307995D802552DDF52683F9B7E701ADB1CB ();
// 0x000001A0 System.Void Newtonsoft.Json.JsonWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JsonWriter_WriteEnd_m859D5A13D90D3709504DD2D69C41F5377DCF059C ();
// 0x000001A1 System.Void Newtonsoft.Json.JsonWriter::WriteIndent()
extern void JsonWriter_WriteIndent_m34F017E8045B45DE46ACC0264B9BA8B9D8612CA6 ();
// 0x000001A2 System.Void Newtonsoft.Json.JsonWriter::WriteValueDelimiter()
extern void JsonWriter_WriteValueDelimiter_mEEEDD00E60D8C42DFC9F61FADEA7FB595A2ED694 ();
// 0x000001A3 System.Void Newtonsoft.Json.JsonWriter::WriteIndentSpace()
extern void JsonWriter_WriteIndentSpace_mDC8FFB2FD18AD9AA4DC0CEF462478AAF4C458C66 ();
// 0x000001A4 System.Void Newtonsoft.Json.JsonWriter::AutoComplete(Newtonsoft.Json.JsonToken)
extern void JsonWriter_AutoComplete_mEDC0587330A9C4B3D23143E7CCC4781A204059CE ();
// 0x000001A5 System.Void Newtonsoft.Json.JsonWriter::WriteNull()
extern void JsonWriter_WriteNull_m552D7BC51FEE5DB8AAD3F335F4F8E452ACE4A3D2 ();
// 0x000001A6 System.Void Newtonsoft.Json.JsonWriter::WriteUndefined()
extern void JsonWriter_WriteUndefined_mBE8A092DE095039F8075C7BE0EAD17A392939983 ();
// 0x000001A7 System.Void Newtonsoft.Json.JsonWriter::WriteRaw(System.String)
extern void JsonWriter_WriteRaw_mFF3946B43D5A76824F32C87BE18F1F3987A04EF6 ();
// 0x000001A8 System.Void Newtonsoft.Json.JsonWriter::WriteRawValue(System.String)
extern void JsonWriter_WriteRawValue_m145C8AFAA99A98312F883F3FA6CD38429AFB2632 ();
// 0x000001A9 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.String)
extern void JsonWriter_WriteValue_m777AE165C2E70C0C5C81EE7BBD269853272109E1 ();
// 0x000001AA System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int32)
extern void JsonWriter_WriteValue_mCD008395307C860CF687FB7D2BAB861659A716DC ();
// 0x000001AB System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt32)
extern void JsonWriter_WriteValue_mBB737FA5C808CAE31BCAEE10BEE06F6DABA0CD50 ();
// 0x000001AC System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int64)
extern void JsonWriter_WriteValue_m52F0AC08B76373382BD0292082A00A5A8F3A27B5 ();
// 0x000001AD System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt64)
extern void JsonWriter_WriteValue_m8020C6BD0E8C8868339401489703EBFAECF085E7 ();
// 0x000001AE System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Single)
extern void JsonWriter_WriteValue_m7FC0CF4A0258F72CED4F8BD91D0261BE048A50CB ();
// 0x000001AF System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Double)
extern void JsonWriter_WriteValue_mCAF862AEC66BFE336D9899BFAB4BD1E16EFB0078 ();
// 0x000001B0 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Boolean)
extern void JsonWriter_WriteValue_m14CF041566177F456CBB8674A5CEC3CB6C8A30C3 ();
// 0x000001B1 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Int16)
extern void JsonWriter_WriteValue_m3D818BA3F94F43D247E8A11C2B53186646207E4F ();
// 0x000001B2 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.UInt16)
extern void JsonWriter_WriteValue_mCF7BEA7ECF73067857A06EE5D56764F3950E984D ();
// 0x000001B3 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Char)
extern void JsonWriter_WriteValue_mD26835A94141F61E65C995EB6DEFF5C71929C2A0 ();
// 0x000001B4 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte)
extern void JsonWriter_WriteValue_mF49FAE8699F53462FEEB6E0841707289C22C08A5 ();
// 0x000001B5 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.SByte)
extern void JsonWriter_WriteValue_m06F820D0E98FAB1C995D971350D5707FAC990D36 ();
// 0x000001B6 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Decimal)
extern void JsonWriter_WriteValue_m793F9858AD678612B614CAEF45474B3FFDE60B07 ();
// 0x000001B7 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTime)
extern void JsonWriter_WriteValue_m7FD998131F74873AE1100ACE78972207F4B51531 ();
// 0x000001B8 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.DateTimeOffset)
extern void JsonWriter_WriteValue_mD9629C3F618774552AD17075645AC3CCF61A594A ();
// 0x000001B9 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Guid)
extern void JsonWriter_WriteValue_mE4992CE60A42487D0564D675E2FF2A5373917B5F ();
// 0x000001BA System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.TimeSpan)
extern void JsonWriter_WriteValue_mCDEFDC530CFCBD00A215958F7869CC7A0E8197A0 ();
// 0x000001BB System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern void JsonWriter_WriteValue_m6B4A111D549F6A348A8B435A4138E307B1CD98C7 ();
// 0x000001BC System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern void JsonWriter_WriteValue_m30641DD12FA3D89E4F74E3F07747C01491184102 ();
// 0x000001BD System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern void JsonWriter_WriteValue_mCB7878AE73DD921638D3485628B4FB16D35A7745 ();
// 0x000001BE System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern void JsonWriter_WriteValue_m809D95F4898B71DB8ADA09171E8249C4300989ED ();
// 0x000001BF System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern void JsonWriter_WriteValue_m35F418C494DAEB11DAB4B6BD47D742245760D2A1 ();
// 0x000001C0 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern void JsonWriter_WriteValue_m15BB97F0DD73344057BC854AADD111375E384AB8 ();
// 0x000001C1 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern void JsonWriter_WriteValue_mAA02BBF0FE3D01F558082573B2C9804C785F0A9B ();
// 0x000001C2 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern void JsonWriter_WriteValue_m61CCD71C3585F45DED94EB0D93CFBC2A304E992F ();
// 0x000001C3 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern void JsonWriter_WriteValue_m0D2AFB7EFA6A9853E18F9014CEB964F3C715F6B2 ();
// 0x000001C4 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern void JsonWriter_WriteValue_m61A87E8ADB473B2B3C20BCDC88FF65F0518BF182 ();
// 0x000001C5 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void JsonWriter_WriteValue_m22324F70BD305201ADD6D7FCABF7A15DAF29C805 ();
// 0x000001C6 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern void JsonWriter_WriteValue_mADA04F2D6A16CC432037347DA69DBDB6621899E5 ();
// 0x000001C7 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern void JsonWriter_WriteValue_m83D12F21167C1D6756F452D5512674FF930D49BD ();
// 0x000001C8 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern void JsonWriter_WriteValue_mC4FE7A1CA1A8F951BDABDA01774461DA577BEFA2 ();
// 0x000001C9 System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.DateTimeOffset>)
extern void JsonWriter_WriteValue_m6E0F2CD366ADFA5F7FC23DA5B7589B796385639C ();
// 0x000001CA System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern void JsonWriter_WriteValue_m1BFAF47F81C721C377418567826EECD8A2AF0223 ();
// 0x000001CB System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern void JsonWriter_WriteValue_mC99378DC7CE8C049523DC71BD646A2FBC305539C ();
// 0x000001CC System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Byte[])
extern void JsonWriter_WriteValue_m47876EA0CA86D0484525C60B52D3095B68BC1D5A ();
// 0x000001CD System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Uri)
extern void JsonWriter_WriteValue_mB39AE2C0D705E4CA2E5332B7E5CEBD41DC529FDB ();
// 0x000001CE System.Void Newtonsoft.Json.JsonWriter::WriteValue(System.Object)
extern void JsonWriter_WriteValue_m16403749D7AF4C994657AB3B6718A6F162A67F33 ();
// 0x000001CF System.Void Newtonsoft.Json.JsonWriter::WriteComment(System.String)
extern void JsonWriter_WriteComment_mCF1E16F4E31982BF2EA150A7634EEA1D23A39782 ();
// 0x000001D0 System.Void Newtonsoft.Json.JsonWriter::System.IDisposable.Dispose()
extern void JsonWriter_System_IDisposable_Dispose_m7A0CBF67068D4E0CFF85F56D2F3262075BC27BCF ();
// 0x000001D1 System.Void Newtonsoft.Json.JsonWriter::Dispose(System.Boolean)
extern void JsonWriter_Dispose_mEC2E7AE9B5F5789033A2505B764837C05F4FD0AF ();
// 0x000001D2 System.Void Newtonsoft.Json.JsonWriter::WriteValue(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.PrimitiveTypeCode,System.Object)
extern void JsonWriter_WriteValue_m3878E2A3F7645D06F0EC55819C340161B76C1665 ();
// 0x000001D3 System.Void Newtonsoft.Json.JsonWriter::ResolveConvertibleValue(System.IConvertible,Newtonsoft.Json.Utilities.PrimitiveTypeCode&,System.Object&)
extern void JsonWriter_ResolveConvertibleValue_m3895705B1775E01F4C23C80ECC7675FD6DB79999 ();
// 0x000001D4 Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriter::CreateUnsupportedTypeException(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonWriter_CreateUnsupportedTypeException_m86B2D476DFA392F9D04D90B20B75FCEA5E5B7DCD ();
// 0x000001D5 System.Void Newtonsoft.Json.JsonWriter::SetWriteState(Newtonsoft.Json.JsonToken,System.Object)
extern void JsonWriter_SetWriteState_m0A34DF0F4A4F65BC87F5D1149C6435B536EC53E2 ();
// 0x000001D6 System.Void Newtonsoft.Json.JsonWriter::InternalWriteEnd(Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteEnd_m5372D41D8643B46072A4FA66A6D28B13FDF84C5A ();
// 0x000001D7 System.Void Newtonsoft.Json.JsonWriter::InternalWritePropertyName(System.String)
extern void JsonWriter_InternalWritePropertyName_m2F31FD7BAA9621AAE842E822AACE0CE6BC3FFB64 ();
// 0x000001D8 System.Void Newtonsoft.Json.JsonWriter::InternalWriteRaw()
extern void JsonWriter_InternalWriteRaw_m223C778B3D873B446104D8313C7A6DD306248B82 ();
// 0x000001D9 System.Void Newtonsoft.Json.JsonWriter::InternalWriteStart(Newtonsoft.Json.JsonToken,Newtonsoft.Json.JsonContainerType)
extern void JsonWriter_InternalWriteStart_mC8B0B74D09E5AEEF030C9BA5EA1FAFF13E8265B3 ();
// 0x000001DA System.Void Newtonsoft.Json.JsonWriter::InternalWriteValue(Newtonsoft.Json.JsonToken)
extern void JsonWriter_InternalWriteValue_m99C4297885F09BC33721461E7B5B52EF3F939785 ();
// 0x000001DB System.Void Newtonsoft.Json.JsonWriter::InternalWriteComment()
extern void JsonWriter_InternalWriteComment_mEFE34AC1697C516BE19E2A16B8A6D3FE0361044D ();
// 0x000001DC System.Void Newtonsoft.Json.JsonWriterException::.ctor()
extern void JsonWriterException__ctor_m2C69D350D91879C4494B958A6E823EBA7EFBEA28 ();
// 0x000001DD System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void JsonWriterException__ctor_m1DADCE5C7A2141DFBC403D632181630126A962A6 ();
// 0x000001DE System.Void Newtonsoft.Json.JsonWriterException::.ctor(System.String,System.String,System.Exception)
extern void JsonWriterException__ctor_m1F9D74B17C17FC1425C67FE0AB5874E23A89645A ();
// 0x000001DF Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(Newtonsoft.Json.JsonWriter,System.String,System.Exception)
extern void JsonWriterException_Create_m5825DFE38AEB983B3A597F18661916702AD976E9 ();
// 0x000001E0 Newtonsoft.Json.JsonWriterException Newtonsoft.Json.JsonWriterException::Create(System.String,System.String,System.Exception)
extern void JsonWriterException_Create_mCDDC9024ACDB1460E49AF35A3502022C68DA30A1 ();
// 0x000001E1 System.Void Newtonsoft.Json.Utilities.Base64Encoder::.ctor(System.IO.TextWriter)
extern void Base64Encoder__ctor_m982CBB72C173A975E585F57B63D7AD5415533DAC ();
// 0x000001E2 System.Void Newtonsoft.Json.Utilities.Base64Encoder::ValidateEncode(System.Byte[],System.Int32,System.Int32)
extern void Base64Encoder_ValidateEncode_m8308A330730304551CAB18E05F986105ECBF4F7B ();
// 0x000001E3 System.Void Newtonsoft.Json.Utilities.Base64Encoder::Encode(System.Byte[],System.Int32,System.Int32)
extern void Base64Encoder_Encode_m27FFDF2EDE9DFC40DB04FCE17B50C4B803377D14 ();
// 0x000001E4 System.Void Newtonsoft.Json.Utilities.Base64Encoder::StoreLeftOverBytes(System.Byte[],System.Int32,System.Int32&)
extern void Base64Encoder_StoreLeftOverBytes_m6F384D73FB57B9B9DD948F41F2124DCC00F5130F ();
// 0x000001E5 System.Boolean Newtonsoft.Json.Utilities.Base64Encoder::FulfillFromLeftover(System.Byte[],System.Int32,System.Int32&)
extern void Base64Encoder_FulfillFromLeftover_m27B705EFA17278FC1B2DF4B1A10E2BCF837F7D8F ();
// 0x000001E6 System.Void Newtonsoft.Json.Utilities.Base64Encoder::Flush()
extern void Base64Encoder_Flush_m800E9F9994B0196447F754E52A3427911AD3342C ();
// 0x000001E7 System.Void Newtonsoft.Json.Utilities.Base64Encoder::WriteChars(System.Char[],System.Int32,System.Int32)
extern void Base64Encoder_WriteChars_m308B0DB7A2788256714998251D41C5D9B78BD68D ();
// 0x000001E8 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TFirst>,System.Collections.Generic.IEqualityComparer`1<TSecond>,System.String,System.String)
// 0x000001E9 System.Void Newtonsoft.Json.Utilities.BidirectionalDictionary`2::Set(TFirst,TSecond)
// 0x000001EA System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetByFirst(TFirst,TSecond&)
// 0x000001EB System.Boolean Newtonsoft.Json.Utilities.BidirectionalDictionary`2::TryGetBySecond(TSecond,TFirst&)
// 0x000001EC System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsNullOrEmpty(System.Collections.Generic.ICollection`1<T>)
// 0x000001ED System.Void Newtonsoft.Json.Utilities.CollectionUtils::AddRange(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEnumerable`1<T>)
// 0x000001EE System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::IsDictionaryType(System.Type)
extern void CollectionUtils_IsDictionaryType_mBB7E6392C2B3D65BF209A555F653C91826EE3DD5 ();
// 0x000001EF System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_mD08643BA723A89B494E122145D3375BA6CA3F6B7 ();
// 0x000001F0 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.CollectionUtils::ResolveEnumerableCollectionConstructor(System.Type,System.Type,System.Type)
extern void CollectionUtils_ResolveEnumerableCollectionConstructor_mBF195DB8E5EF6F11867DA8E7894DBD00D59C37B4 ();
// 0x000001F1 System.Int32 Newtonsoft.Json.Utilities.CollectionUtils::IndexOf(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x000001F2 System.Boolean Newtonsoft.Json.Utilities.CollectionUtils::Contains(System.Collections.Generic.List`1<T>,T,System.Collections.IEqualityComparer)
// 0x000001F3 System.Int32 Newtonsoft.Json.Utilities.CollectionUtils::IndexOfReference(System.Collections.Generic.List`1<T>,T)
// 0x000001F4 System.Void Newtonsoft.Json.Utilities.CollectionUtils::FastReverse(System.Collections.Generic.List`1<T>)
// 0x000001F5 System.Collections.Generic.IList`1<System.Int32> Newtonsoft.Json.Utilities.CollectionUtils::GetDimensions(System.Collections.IList,System.Int32)
extern void CollectionUtils_GetDimensions_mAB57BCD2D9BA63A2158ABFF46F965097DF6AF47C ();
// 0x000001F6 System.Void Newtonsoft.Json.Utilities.CollectionUtils::CopyFromJaggedToMultidimensionalArray(System.Collections.IList,System.Array,System.Int32[])
extern void CollectionUtils_CopyFromJaggedToMultidimensionalArray_m484084C058991DC9DB43FBADE418CA043CA7B35D ();
// 0x000001F7 System.Object Newtonsoft.Json.Utilities.CollectionUtils::JaggedArrayGetValue(System.Collections.IList,System.Int32[])
extern void CollectionUtils_JaggedArrayGetValue_mFD3099263FDCDB4707CAA843ABA307E228A6D55F ();
// 0x000001F8 System.Array Newtonsoft.Json.Utilities.CollectionUtils::ToMultidimensionalArray(System.Collections.IList,System.Type,System.Int32)
extern void CollectionUtils_ToMultidimensionalArray_m3089FD29439160A238C9BC17215697DB4FAC3C68 ();
// 0x000001F9 T[] Newtonsoft.Json.Utilities.CollectionUtils::ArrayEmpty()
// 0x000001FA System.Void Newtonsoft.Json.Utilities.CollectionUtils_EmptyArrayContainer`1::.cctor()
// 0x000001FB System.Object Newtonsoft.Json.Utilities.IWrappedCollection::get_UnderlyingCollection()
// 0x000001FC System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::.ctor(System.Collections.IList)
// 0x000001FD System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x000001FE System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::Add(T)
// 0x000001FF System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::Clear()
// 0x00000200 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::Contains(T)
// 0x00000201 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::CopyTo(T[],System.Int32)
// 0x00000202 System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::get_Count()
// 0x00000203 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::get_IsReadOnly()
// 0x00000204 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::Remove(T)
// 0x00000205 System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Utilities.CollectionWrapper`1::GetEnumerator()
// 0x00000206 System.Collections.IEnumerator Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000207 System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Add(System.Object)
// 0x00000208 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Contains(System.Object)
// 0x00000209 System.Int32 Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.IndexOf(System.Object)
// 0x0000020A System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.RemoveAt(System.Int32)
// 0x0000020B System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Insert(System.Int32,System.Object)
// 0x0000020C System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.get_IsFixedSize()
// 0x0000020D System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.Remove(System.Object)
// 0x0000020E System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.get_Item(System.Int32)
// 0x0000020F System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.IList.set_Item(System.Int32,System.Object)
// 0x00000210 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000211 System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::System.Collections.ICollection.get_SyncRoot()
// 0x00000212 System.Void Newtonsoft.Json.Utilities.CollectionWrapper`1::VerifyValueType(System.Object)
// 0x00000213 System.Boolean Newtonsoft.Json.Utilities.CollectionWrapper`1::IsCompatibleObject(System.Object)
// 0x00000214 System.Object Newtonsoft.Json.Utilities.CollectionWrapper`1::get_UnderlyingCollection()
// 0x00000215 System.Type Newtonsoft.Json.Utilities.TypeInformation::get_Type()
extern void TypeInformation_get_Type_mA706AD5CBD89E6285DD2560F770295B5A7531F5D ();
// 0x00000216 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.TypeInformation::get_TypeCode()
extern void TypeInformation_get_TypeCode_m8AB63E952423E39EA536D0B9E6BB3FCC4D2C9E7C ();
// 0x00000217 System.Void Newtonsoft.Json.Utilities.TypeInformation::.ctor(System.Type,Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void TypeInformation__ctor_m07FA50E1AF5C91D8721CB072C70FE9C53B1ADE86 ();
// 0x00000218 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type)
extern void ConvertUtils_GetTypeCode_mB4A5A95F8664AD1F0B3748E95E923C31B642FC6E ();
// 0x00000219 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Utilities.ConvertUtils::GetTypeCode(System.Type,System.Boolean&)
extern void ConvertUtils_GetTypeCode_m699C5A721CB1EACC55CCF6B3C5890E6955A3EC81 ();
// 0x0000021A Newtonsoft.Json.Utilities.TypeInformation Newtonsoft.Json.Utilities.ConvertUtils::GetTypeInformation(System.IConvertible)
extern void ConvertUtils_GetTypeInformation_mD536E4E084E45A00FD456508CF6B9F6F04B7298E ();
// 0x0000021B System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsConvertible(System.Type)
extern void ConvertUtils_IsConvertible_mCD4DD5F8C1E2BEE7DE97E874424EA85DC27AF0B8 ();
// 0x0000021C System.TimeSpan Newtonsoft.Json.Utilities.ConvertUtils::ParseTimeSpan(System.String)
extern void ConvertUtils_ParseTimeSpan_m172717B02C98E4CB3A0BCD91EA37C9DC2E2588AF ();
// 0x0000021D System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ConvertUtils::CreateCastConverter(Newtonsoft.Json.Utilities.StructMultiKey`2<System.Type,System.Type>)
extern void ConvertUtils_CreateCastConverter_m27A7B131089FEC17727AF43267B66E17E3A4CED5 ();
// 0x0000021E System.Numerics.BigInteger Newtonsoft.Json.Utilities.ConvertUtils::ToBigInteger(System.Object)
extern void ConvertUtils_ToBigInteger_mFF5B7E8EF9FC2BB3FAE33B1FAD0A849849FA7298 ();
// 0x0000021F System.Object Newtonsoft.Json.Utilities.ConvertUtils::FromBigInteger(System.Numerics.BigInteger,System.Type)
extern void ConvertUtils_FromBigInteger_m50624DDBAA5E23938CF330E81511276CBDA8A059 ();
// 0x00000220 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvert(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvert_m0FC1DF180180D199C32D2B547C31E927F3F2A527 ();
// 0x00000221 Newtonsoft.Json.Utilities.ConvertUtils_ConvertResult Newtonsoft.Json.Utilities.ConvertUtils::TryConvertInternal(System.Object,System.Globalization.CultureInfo,System.Type,System.Object&)
extern void ConvertUtils_TryConvertInternal_m683671B6926DFBB7890804B010D6D075A5BD5A45 ();
// 0x00000222 System.Object Newtonsoft.Json.Utilities.ConvertUtils::ConvertOrCast(System.Object,System.Globalization.CultureInfo,System.Type)
extern void ConvertUtils_ConvertOrCast_m00E1E1315ED0FDEAF662854041FEF18DA7065198 ();
// 0x00000223 System.Object Newtonsoft.Json.Utilities.ConvertUtils::EnsureTypeAssignable(System.Object,System.Type,System.Type)
extern void ConvertUtils_EnsureTypeAssignable_mC88A1375B57428FA087FABDA244A2FB271194198 ();
// 0x00000224 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::VersionTryParse(System.String,System.Version&)
extern void ConvertUtils_VersionTryParse_mB8A3CB61AAC8120693E11841566AFEBF4FCA30CC ();
// 0x00000225 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::IsInteger(System.Object)
extern void ConvertUtils_IsInteger_m1E8095C7D169840870AF7F158EDC07D2E3F57E37 ();
// 0x00000226 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int32TryParse(System.Char[],System.Int32,System.Int32,System.Int32&)
extern void ConvertUtils_Int32TryParse_mB81CE59DBD850DF031DAFFB2F0DDEB48A345BEF6 ();
// 0x00000227 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::Int64TryParse(System.Char[],System.Int32,System.Int32,System.Int64&)
extern void ConvertUtils_Int64TryParse_mE4F4963695B52AF1320D3EAA5F8263301C2EB69E ();
// 0x00000228 Newtonsoft.Json.Utilities.ParseResult Newtonsoft.Json.Utilities.ConvertUtils::DecimalTryParse(System.Char[],System.Int32,System.Int32,System.Decimal&)
extern void ConvertUtils_DecimalTryParse_m635343424FB64EC5169E97D45DD076C75E3AD41C ();
// 0x00000229 System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryConvertGuid(System.String,System.Guid&)
extern void ConvertUtils_TryConvertGuid_m4D8F89945C283C3499C33A8DEF956BD19E0B0B1E ();
// 0x0000022A System.Boolean Newtonsoft.Json.Utilities.ConvertUtils::TryHexTextToInt(System.Char[],System.Int32,System.Int32,System.Int32&)
extern void ConvertUtils_TryHexTextToInt_mE03A65879D470EFCAA7AE02A2F7502C144F510F2 ();
// 0x0000022B System.Void Newtonsoft.Json.Utilities.ConvertUtils::.cctor()
extern void ConvertUtils__cctor_mD36D5B3BC15AF6F53BCD3FFB1EBD935BBF2B7D0C ();
// 0x0000022C System.Void Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m7B6D25E211D4871A7CFEA6DC9C8DA57FA6263B47 ();
// 0x0000022D System.Object Newtonsoft.Json.Utilities.ConvertUtils_<>c__DisplayClass8_0::<CreateCastConverter>b__0(System.Object)
extern void U3CU3Ec__DisplayClass8_0_U3CCreateCastConverterU3Eb__0_m061CFEA2BA887591FDA47F65300203C0862BA9F5 ();
// 0x0000022E System.Void Newtonsoft.Json.Utilities.DateTimeParser::.cctor()
extern void DateTimeParser__cctor_mEA064906F2088A75549E50B47C17B7035DDFAB9F ();
// 0x0000022F System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse(System.Char[],System.Int32,System.Int32)
extern void DateTimeParser_Parse_m43B3FC0FA8161DDF4BD65D8BA4FDC932ECB5975A_AdjustorThunk ();
// 0x00000230 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseDate(System.Int32)
extern void DateTimeParser_ParseDate_mEE029E37D7888F1F1F565C62D2B1AD8A24C196B0_AdjustorThunk ();
// 0x00000231 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTimeAndZoneAndWhitespace(System.Int32)
extern void DateTimeParser_ParseTimeAndZoneAndWhitespace_m8D86188478104927B08DCC91471CA7F91D5D6DA1_AdjustorThunk ();
// 0x00000232 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseTime(System.Int32&)
extern void DateTimeParser_ParseTime_mEAF9A7B597931847CD06F3E63D9E89F8BDD8DDF5_AdjustorThunk ();
// 0x00000233 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseZone(System.Int32)
extern void DateTimeParser_ParseZone_mE7CBCC4DC51A530D5DBF1E68ADDA5171A977048A_AdjustorThunk ();
// 0x00000234 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse4Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse4Digit_m9155B9CDCD558218F7F08302E8613152CAD89829_AdjustorThunk ();
// 0x00000235 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::Parse2Digit(System.Int32,System.Int32&)
extern void DateTimeParser_Parse2Digit_mEE0C6006FE2D42510E2B0BCD19B365201AB04D57_AdjustorThunk ();
// 0x00000236 System.Boolean Newtonsoft.Json.Utilities.DateTimeParser::ParseChar(System.Int32,System.Char)
extern void DateTimeParser_ParseChar_m2BBAFB1B1FE2859BC8072E8423A57E6D04533437_AdjustorThunk ();
// 0x00000237 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::.cctor()
extern void DateTimeUtils__cctor_m648A889F08E03B5A42AD69EDD8CF48C0E2F6F212 ();
// 0x00000238 System.TimeSpan Newtonsoft.Json.Utilities.DateTimeUtils::GetUtcOffset(System.DateTime)
extern void DateTimeUtils_GetUtcOffset_m9F7AD533125BAED1CDBE01DFB144795B1D1B15AC ();
// 0x00000239 System.Xml.XmlDateTimeSerializationMode Newtonsoft.Json.Utilities.DateTimeUtils::ToSerializationMode(System.DateTimeKind)
extern void DateTimeUtils_ToSerializationMode_m4659F4806C6CBD8545C502E88084B146241F2E89 ();
// 0x0000023A System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::EnsureDateTime(System.DateTime,Newtonsoft.Json.DateTimeZoneHandling)
extern void DateTimeUtils_EnsureDateTime_m325F398FBDD29DBBEADEA941A77566F982286792 ();
// 0x0000023B System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToLocalTime(System.DateTime)
extern void DateTimeUtils_SwitchToLocalTime_m0D45766632CF27ECC5EC90E7DE50EB7A9694CCC5 ();
// 0x0000023C System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::SwitchToUtcTime(System.DateTime)
extern void DateTimeUtils_SwitchToUtcTime_mC8AF1BFBF807E1DA3FA4B9405340902752D671B2 ();
// 0x0000023D System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ToUniversalTicks_mAE971477C4D0745AB6DE0AEDC5062631725B6FEA ();
// 0x0000023E System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern void DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2485A1AD9F2B52CFD01A4BE3AD97F9C37107BC3C ();
// 0x0000023F System.Int64 Newtonsoft.Json.Utilities.DateTimeUtils::UniversialTicksToJavaScriptTicks(System.Int64)
extern void DateTimeUtils_UniversialTicksToJavaScriptTicks_m771D7B6833225843D320529D9AA6C8DEFB18F8E2 ();
// 0x00000240 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::ConvertJavaScriptTicksToDateTime(System.Int64)
extern void DateTimeUtils_ConvertJavaScriptTicksToDateTime_m5FAB48CFDDA9607DFB1917A23A1CCC1D73F91C43 ();
// 0x00000241 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeIso(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeIso_m7E06FF64EC2BA50B75E4A2732C1EF00CE11AD771 ();
// 0x00000242 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetIso(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetIso_m913E3D8DC898481197A0F98F5B6A850F1BA027A2 ();
// 0x00000243 System.DateTime Newtonsoft.Json.Utilities.DateTimeUtils::CreateDateTime(Newtonsoft.Json.Utilities.DateTimeParser)
extern void DateTimeUtils_CreateDateTime_m0FC2FC875264027406C3BF8AD6E42302683E0720 ();
// 0x00000244 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_m41E39A9DFBD97C2AE6AC56C4B504F42E993851FF ();
// 0x00000245 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTime(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTime_mD2262844C1A4B464337775148F3E4E54F6D7E74A ();
// 0x00000246 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(Newtonsoft.Json.Utilities.StringReference,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffset_m9B5B3485A46D66E27457822CC1EF6E0D17ADB5CC ();
// 0x00000247 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffset(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffset_m254A3FDDABDDF979844C0ADF79201B6BB462FDD9 ();
// 0x00000248 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseMicrosoftDate(Newtonsoft.Json.Utilities.StringReference,System.Int64&,System.TimeSpan&,System.DateTimeKind&)
extern void DateTimeUtils_TryParseMicrosoftDate_mC6EDFDD1538000260211463563578A2E41580E54 ();
// 0x00000249 System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeMicrosoft(Newtonsoft.Json.Utilities.StringReference,Newtonsoft.Json.DateTimeZoneHandling,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeMicrosoft_m69ACCE201D332547C54BA5280AC22E01DE675420 ();
// 0x0000024A System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeExact(System.String,Newtonsoft.Json.DateTimeZoneHandling,System.String,System.Globalization.CultureInfo,System.DateTime&)
extern void DateTimeUtils_TryParseDateTimeExact_m07E1106E3A362924998703F599A31181561CB2E3 ();
// 0x0000024B System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetMicrosoft(Newtonsoft.Json.Utilities.StringReference,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetMicrosoft_m605D9E1887EB40D7439F50ED2FB82E48C8CE7563 ();
// 0x0000024C System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryParseDateTimeOffsetExact(System.String,System.String,System.Globalization.CultureInfo,System.DateTimeOffset&)
extern void DateTimeUtils_TryParseDateTimeOffsetExact_m54E38E85E5D1E5C458B0254966A5CC6C7546321D ();
// 0x0000024D System.Boolean Newtonsoft.Json.Utilities.DateTimeUtils::TryReadOffset(Newtonsoft.Json.Utilities.StringReference,System.Int32,System.TimeSpan&)
extern void DateTimeUtils_TryReadOffset_m8D756B894BE25ED820CF16E6076905713524EE2D ();
// 0x0000024E System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.IO.TextWriter,System.DateTime,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern void DateTimeUtils_WriteDateTimeString_mCE460CFD698DD920E27891EA6E3B03836AE5A691 ();
// 0x0000024F System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeString(System.Char[],System.Int32,System.DateTime,System.Nullable`1<System.TimeSpan>,System.DateTimeKind,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeString_m0861A9992AE400F8FA19D00D12A68FA66C943DA6 ();
// 0x00000250 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDefaultIsoDate(System.Char[],System.Int32,System.DateTime)
extern void DateTimeUtils_WriteDefaultIsoDate_m6FEE0E7DBBC38DF88078C6C972C165A8D8B11C11 ();
// 0x00000251 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::CopyIntToCharArray(System.Char[],System.Int32,System.Int32,System.Int32)
extern void DateTimeUtils_CopyIntToCharArray_m721DB60D3F654BDED253D5DA660602F8B0FA6C82 ();
// 0x00000252 System.Int32 Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffset(System.Char[],System.Int32,System.TimeSpan,Newtonsoft.Json.DateFormatHandling)
extern void DateTimeUtils_WriteDateTimeOffset_m455B4552E7E44C9821A598709DCA7D5B238413E1 ();
// 0x00000253 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::WriteDateTimeOffsetString(System.IO.TextWriter,System.DateTimeOffset,Newtonsoft.Json.DateFormatHandling,System.String,System.Globalization.CultureInfo)
extern void DateTimeUtils_WriteDateTimeOffsetString_m6347D67D0DBCAE73D3475F9794293D77F06E4AB1 ();
// 0x00000254 System.Void Newtonsoft.Json.Utilities.DateTimeUtils::GetDateValues(System.DateTime,System.Int32&,System.Int32&,System.Int32&)
extern void DateTimeUtils_GetDateValues_mD1B89FE5E95556B9DE6B4912CA17D817E00081EC ();
// 0x00000255 System.Object Newtonsoft.Json.Utilities.IWrappedDictionary::get_UnderlyingDictionary()
// 0x00000256 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::.ctor(System.Collections.IDictionary)
// 0x00000257 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000258 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::.ctor(System.Collections.Generic.IReadOnlyDictionary`2<TKey,TValue>)
// 0x00000259 System.Collections.Generic.IDictionary`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_GenericDictionary()
// 0x0000025A System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Add(TKey,TValue)
// 0x0000025B System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::ContainsKey(TKey)
// 0x0000025C System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(TKey)
// 0x0000025D System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::TryGetValue(TKey,TValue&)
// 0x0000025E System.Collections.Generic.ICollection`1<TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Values()
// 0x0000025F TValue Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Item(TKey)
// 0x00000260 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::set_Item(TKey,TValue)
// 0x00000261 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000262 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Clear()
// 0x00000263 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000264 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000265 System.Int32 Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_Count()
// 0x00000266 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_IsReadOnly()
// 0x00000267 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000268 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Newtonsoft.Json.Utilities.DictionaryWrapper`2::GetEnumerator()
// 0x00000269 System.Collections.IEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000026A System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.Add(System.Object,System.Object)
// 0x0000026B System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.get_Item(System.Object)
// 0x0000026C System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.set_Item(System.Object,System.Object)
// 0x0000026D System.Collections.IDictionaryEnumerator Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.GetEnumerator()
// 0x0000026E System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.Contains(System.Object)
// 0x0000026F System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::Remove(System.Object)
// 0x00000270 System.Collections.ICollection Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.IDictionary.get_Values()
// 0x00000271 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
// 0x00000272 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::System.Collections.ICollection.get_SyncRoot()
// 0x00000273 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2::get_UnderlyingDictionary()
// 0x00000274 System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::.ctor(System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TEnumeratorKey,TEnumeratorValue>>)
// 0x00000275 System.Collections.DictionaryEntry Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Entry()
// 0x00000276 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Key()
// 0x00000277 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Value()
// 0x00000278 System.Object Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::get_Current()
// 0x00000279 System.Boolean Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::MoveNext()
// 0x0000027A System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_DictionaryEnumerator`2::Reset()
// 0x0000027B System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::.cctor()
// 0x0000027C System.Void Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::.ctor()
// 0x0000027D System.Collections.Generic.KeyValuePair`2<TKey,TValue> Newtonsoft.Json.Utilities.DictionaryWrapper`2_<>c::<GetEnumerator>b__29_0(System.Collections.DictionaryEntry)
// 0x0000027E System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.Utilities.DynamicProxy`1::GetDynamicMemberNames(T)
// 0x0000027F System.Void Newtonsoft.Json.Utilities.DynamicProxy`1::.ctor()
// 0x00000280 System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::.ctor(System.Linq.Expressions.Expression,T,Newtonsoft.Json.Utilities.DynamicProxy`1<T>)
// 0x00000281 System.Boolean Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::IsOverridden(System.String)
// 0x00000282 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::BindGetMember(System.Dynamic.GetMemberBinder)
// 0x00000283 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::BindSetMember(System.Dynamic.SetMemberBinder,System.Dynamic.DynamicMetaObject)
// 0x00000284 System.Linq.Expressions.Expression[] Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::get_NoArgs()
// 0x00000285 System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.Expression> Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::GetArgs(System.Dynamic.DynamicMetaObject[])
// 0x00000286 System.Linq.Expressions.ConstantExpression Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::Constant(System.Dynamic.DynamicMetaObjectBinder)
// 0x00000287 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::CallMethodWithResult(System.String,System.Dynamic.DynamicMetaObjectBinder,System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.Expression>,Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback<T>,Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback<T>)
// 0x00000288 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::BuildCallMethodWithResult(System.String,System.Dynamic.DynamicMetaObjectBinder,System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.Expression>,System.Dynamic.DynamicMetaObject,Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback<T>)
// 0x00000289 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::CallMethodReturnLast(System.String,System.Dynamic.DynamicMetaObjectBinder,System.Collections.Generic.IEnumerable`1<System.Linq.Expressions.Expression>,Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback<T>)
// 0x0000028A System.Dynamic.BindingRestrictions Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::GetRestrictions()
// 0x0000028B System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1::GetDynamicMemberNames()
// 0x0000028C System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback::.ctor(System.Object,System.IntPtr)
// 0x0000028D System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback::Invoke(System.Dynamic.DynamicMetaObject)
// 0x0000028E System.IAsyncResult Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback::BeginInvoke(System.Dynamic.DynamicMetaObject,System.AsyncCallback,System.Object)
// 0x0000028F System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_Fallback::EndInvoke(System.IAsyncResult)
// 0x00000290 System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c__DisplayClass3_0::.ctor()
// 0x00000291 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c__DisplayClass3_0::<BindGetMember>b__0(System.Dynamic.DynamicMetaObject)
// 0x00000292 System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c__DisplayClass4_0::.ctor()
// 0x00000293 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c__DisplayClass4_0::<BindSetMember>b__0(System.Dynamic.DynamicMetaObject)
// 0x00000294 System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c::.cctor()
// 0x00000295 System.Void Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c::.ctor()
// 0x00000296 System.Linq.Expressions.Expression Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1_<>c::<GetArgs>b__18_0(System.Dynamic.DynamicMetaObject)
// 0x00000297 System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.Utilities.DynamicUtils::GetDynamicMemberNames(System.Dynamic.IDynamicMetaObjectProvider)
extern void DynamicUtils_GetDynamicMemberNames_m9EE2F157494A945FC80F31D0249C228C841B76E3 ();
// 0x00000298 System.Void Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper::Init()
extern void BinderWrapper_Init_m5D70E736A7401CE42908D1A50EBD983402AA7625 ();
// 0x00000299 System.Object Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper::CreateSharpArgumentInfoArray(System.Int32[])
extern void BinderWrapper_CreateSharpArgumentInfoArray_m97B6B6D1C063B27560D0E8B597618AD3E17B4A0D ();
// 0x0000029A System.Void Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper::CreateMemberCalls()
extern void BinderWrapper_CreateMemberCalls_m93EAFFBFFF25B09B4CB75A274F3D01060D72DDC8 ();
// 0x0000029B System.Runtime.CompilerServices.CallSiteBinder Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper::GetMember(System.String,System.Type)
extern void BinderWrapper_GetMember_m4DC3FB0DBE56DEACA425757C67794BCF9D311A18 ();
// 0x0000029C System.Runtime.CompilerServices.CallSiteBinder Newtonsoft.Json.Utilities.DynamicUtils_BinderWrapper::SetMember(System.String,System.Type)
extern void BinderWrapper_SetMember_mFA51D571E767B4DA53DF06C8E26D3D40D2D6B503 ();
// 0x0000029D System.Void Newtonsoft.Json.Utilities.NoThrowGetBinderMember::.ctor(System.Dynamic.GetMemberBinder)
extern void NoThrowGetBinderMember__ctor_mDA0E964CCDDCAFB8675267DE0E3BF18D79E7C87C ();
// 0x0000029E System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.NoThrowGetBinderMember::FallbackGetMember(System.Dynamic.DynamicMetaObject,System.Dynamic.DynamicMetaObject)
extern void NoThrowGetBinderMember_FallbackGetMember_m9406BCB973D668AD9B17BEBABED58E8BE05E50E3 ();
// 0x0000029F System.Void Newtonsoft.Json.Utilities.NoThrowSetBinderMember::.ctor(System.Dynamic.SetMemberBinder)
extern void NoThrowSetBinderMember__ctor_m197C874AA0F759C90764FCF673E58C02440372BC ();
// 0x000002A0 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Utilities.NoThrowSetBinderMember::FallbackSetMember(System.Dynamic.DynamicMetaObject,System.Dynamic.DynamicMetaObject,System.Dynamic.DynamicMetaObject)
extern void NoThrowSetBinderMember_FallbackSetMember_m365BF1EE4BB822C5E691FB911C839D3C6C63EA94 ();
// 0x000002A1 System.Linq.Expressions.Expression Newtonsoft.Json.Utilities.NoThrowExpressionVisitor::VisitConditional(System.Linq.Expressions.ConditionalExpression)
extern void NoThrowExpressionVisitor_VisitConditional_mA2B2AE2CC6CD366385CADAF75D93773F8E17D91C ();
// 0x000002A2 System.Void Newtonsoft.Json.Utilities.NoThrowExpressionVisitor::.ctor()
extern void NoThrowExpressionVisitor__ctor_m9A6090E84B5651F3168C4A509D4C53C3D217FA08 ();
// 0x000002A3 System.Void Newtonsoft.Json.Utilities.NoThrowExpressionVisitor::.cctor()
extern void NoThrowExpressionVisitor__cctor_m1465172B3FD841337FDA664EE062E3A49ABBF1DD ();
// 0x000002A4 System.Void Newtonsoft.Json.Utilities.EnumInfo::.ctor(System.Boolean,System.UInt64[],System.String[],System.String[])
extern void EnumInfo__ctor_mDE19ED14790B4609B59AC8437C493C1DB3BFA68F ();
// 0x000002A5 Newtonsoft.Json.Utilities.EnumInfo Newtonsoft.Json.Utilities.EnumUtils::InitializeValuesAndNames(Newtonsoft.Json.Utilities.StructMultiKey`2<System.Type,Newtonsoft.Json.Serialization.NamingStrategy>)
extern void EnumUtils_InitializeValuesAndNames_mE11956DB6693D0DF70384549F81C596C83A883D6 ();
// 0x000002A6 System.Boolean Newtonsoft.Json.Utilities.EnumUtils::TryToString(System.Type,System.Object,Newtonsoft.Json.Serialization.NamingStrategy,System.String&)
extern void EnumUtils_TryToString_m842CF119FC5659E95CB09570659652D8753E999C ();
// 0x000002A7 System.String Newtonsoft.Json.Utilities.EnumUtils::InternalFlagsFormat(Newtonsoft.Json.Utilities.EnumInfo,System.UInt64)
extern void EnumUtils_InternalFlagsFormat_m611CFAF7C1381E1EE653D2492EDA433243777A27 ();
// 0x000002A8 Newtonsoft.Json.Utilities.EnumInfo Newtonsoft.Json.Utilities.EnumUtils::GetEnumValuesAndNames(System.Type)
extern void EnumUtils_GetEnumValuesAndNames_mC993C4AFE95D075CA64A695AE7469F07C2661140 ();
// 0x000002A9 System.UInt64 Newtonsoft.Json.Utilities.EnumUtils::ToUInt64(System.Object)
extern void EnumUtils_ToUInt64_m0D86B45B51747B2B191968070B82D1A724D17312 ();
// 0x000002AA System.Object Newtonsoft.Json.Utilities.EnumUtils::ParseEnum(System.Type,Newtonsoft.Json.Serialization.NamingStrategy,System.String,System.Boolean)
extern void EnumUtils_ParseEnum_m2C7528F536FA0AF803C3D81510618CE8F70E6DAB ();
// 0x000002AB System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.EnumUtils::MatchName(System.String,System.String[],System.String[],System.Int32,System.Int32,System.StringComparison)
extern void EnumUtils_MatchName_mBE379E853CC70B30B2A920F1B26F3370634CAEB3 ();
// 0x000002AC System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.EnumUtils::FindIndexByName(System.String[],System.String,System.Int32,System.Int32,System.StringComparison)
extern void EnumUtils_FindIndexByName_m462FDB3453FBC8601F9B7EE996D8875319E7C5AB ();
// 0x000002AD System.Void Newtonsoft.Json.Utilities.EnumUtils::.cctor()
extern void EnumUtils__cctor_m993FDCCE8DFFD4304E949C5DB00FF38D38D2070C ();
// 0x000002AE System.Void Newtonsoft.Json.Utilities.EnumUtils_<>c::.cctor()
extern void U3CU3Ec__cctor_mA4B587CE9C3DE218984EF1EF7EE6F5B25C99313C ();
// 0x000002AF System.Void Newtonsoft.Json.Utilities.EnumUtils_<>c::.ctor()
extern void U3CU3Ec__ctor_mD3C7C2B6449BEDB3FDD3EBB0808E4F466FCAFFDD ();
// 0x000002B0 System.String Newtonsoft.Json.Utilities.EnumUtils_<>c::<InitializeValuesAndNames>b__3_0(System.Runtime.Serialization.EnumMemberAttribute)
extern void U3CU3Ec_U3CInitializeValuesAndNamesU3Eb__3_0_m723957A29FF2263B146696158EC8B178C8D82041 ();
// 0x000002B1 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::get_Instance()
extern void ExpressionReflectionDelegateFactory_get_Instance_m831320943E9978D6A8D4F116D0B359F7735DE6BB ();
// 0x000002B2 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern void ExpressionReflectionDelegateFactory_CreateParameterizedConstructor_m7C08C0B36DA4BE047265DE4459D381D786BA5CBF ();
// 0x000002B3 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x000002B4 System.Linq.Expressions.Expression Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::BuildMethodCall(System.Reflection.MethodBase,System.Type,System.Linq.Expressions.ParameterExpression,System.Linq.Expressions.ParameterExpression)
extern void ExpressionReflectionDelegateFactory_BuildMethodCall_m0011EE22249F2B135F93C83AB42D20E4E2F6D1E1 ();
// 0x000002B5 System.Func`1<T> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x000002B6 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x000002B7 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x000002B8 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x000002B9 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x000002BA System.Linq.Expressions.Expression Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::EnsureCastExpression(System.Linq.Expressions.Expression,System.Type,System.Boolean)
extern void ExpressionReflectionDelegateFactory_EnsureCastExpression_mA0F3FC1518ED2CE86538D9DA0F4EF47564B936DB ();
// 0x000002BB System.Void Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::.ctor()
extern void ExpressionReflectionDelegateFactory__ctor_m1CD7A86504310E44B839178A088382C0190785D8 ();
// 0x000002BC System.Void Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory::.cctor()
extern void ExpressionReflectionDelegateFactory__cctor_mB710CB6081B4F4C8F806583B3BD00A3F9A24BBE8 ();
// 0x000002BD System.Void Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_ByRefParameter::.ctor(System.Linq.Expressions.Expression,System.Linq.Expressions.ParameterExpression,System.Boolean)
extern void ByRefParameter__ctor_m4718F8B823D095A1ECEB66CC6B1397B364154094 ();
// 0x000002BE System.Void Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_<>c__DisplayClass7_0`1::.ctor()
// 0x000002BF T Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_<>c__DisplayClass7_0`1::<CreateDefaultConstructor>b__0()
// 0x000002C0 T Newtonsoft.Json.Utilities.ExpressionReflectionDelegateFactory_<>c__DisplayClass7_0`1::<CreateDefaultConstructor>b__1()
// 0x000002C1 System.Void Newtonsoft.Json.Utilities.FSharpFunction::.ctor(System.Object,Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpFunction__ctor_m414E2BFD2D608C99E7D3B9AE10E799E83CABE729 ();
// 0x000002C2 System.Object Newtonsoft.Json.Utilities.FSharpFunction::Invoke(System.Object[])
extern void FSharpFunction_Invoke_mAD1C1BE3517ED288E69C2C63037A3DF0C8155AA3 ();
// 0x000002C3 System.Void Newtonsoft.Json.Utilities.FSharpUtils::.ctor(System.Reflection.Assembly)
extern void FSharpUtils__ctor_mBF998D082E661BD0BFBDE67059F18D1424CCDFB0 ();
// 0x000002C4 Newtonsoft.Json.Utilities.FSharpUtils Newtonsoft.Json.Utilities.FSharpUtils::get_Instance()
extern void FSharpUtils_get_Instance_m486791D0D96FC27FF4D4B0F1C7DC6B45BAF76EF6 ();
// 0x000002C5 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_FSharpCoreAssembly(System.Reflection.Assembly)
extern void FSharpUtils_set_FSharpCoreAssembly_m86E2DC83E7348FC4C937C2FA35E4138B76FD0FC1 ();
// 0x000002C6 Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_IsUnion()
extern void FSharpUtils_get_IsUnion_mE9B81C84480DAC67BB35CBCFAED1988C808CCAB3 ();
// 0x000002C7 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_IsUnion(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_IsUnion_mD06CB47EFF251198C95CB1B535BFB4C0174D5E3F ();
// 0x000002C8 Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_GetUnionCases()
extern void FSharpUtils_get_GetUnionCases_m9C137C2BFAAAA384B6ABBD3173D365EB88112CF0 ();
// 0x000002C9 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_GetUnionCases(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_GetUnionCases_mF1FB052695915469AD2271F73920B5972F8D4FF5 ();
// 0x000002CA Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_PreComputeUnionTagReader()
extern void FSharpUtils_get_PreComputeUnionTagReader_m48B81101C73D1D8672C2781AF4C275814E0A2F9E ();
// 0x000002CB System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_PreComputeUnionTagReader(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_PreComputeUnionTagReader_mA4DD07A716374BD63D048A8C23A97CB3F3D9CAC5 ();
// 0x000002CC Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_PreComputeUnionReader()
extern void FSharpUtils_get_PreComputeUnionReader_m99851918B1AA672F86F5BF4496DA20F1589F9A44 ();
// 0x000002CD System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_PreComputeUnionReader(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_PreComputeUnionReader_m1189A0892E94886BC90C05F9596E3A43F6208A24 ();
// 0x000002CE Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_PreComputeUnionConstructor()
extern void FSharpUtils_get_PreComputeUnionConstructor_m02303FADD834CBCBDFA4E3A521FC9DC4FAD16B26 ();
// 0x000002CF System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_PreComputeUnionConstructor(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_PreComputeUnionConstructor_mFA6894426E806E7DF01E568D431D25314C637E4D ();
// 0x000002D0 System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_GetUnionCaseInfoDeclaringType()
extern void FSharpUtils_get_GetUnionCaseInfoDeclaringType_m65C8F5023475861194551D2FE4B3D194D98BED37 ();
// 0x000002D1 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_GetUnionCaseInfoDeclaringType(System.Func`2<System.Object,System.Object>)
extern void FSharpUtils_set_GetUnionCaseInfoDeclaringType_m1440904750D8A46F40491D563AAA487E17462742 ();
// 0x000002D2 System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_GetUnionCaseInfoName()
extern void FSharpUtils_get_GetUnionCaseInfoName_m0DC562BAFC40E1E5A1E3A8DAD45283B53DC75BA9 ();
// 0x000002D3 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_GetUnionCaseInfoName(System.Func`2<System.Object,System.Object>)
extern void FSharpUtils_set_GetUnionCaseInfoName_m84B84F80973021E1C4634094D40C9912B9FA19F7 ();
// 0x000002D4 System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_GetUnionCaseInfoTag()
extern void FSharpUtils_get_GetUnionCaseInfoTag_m56F65C78314427E8E860CFD2749B6C76F2456448 ();
// 0x000002D5 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_GetUnionCaseInfoTag(System.Func`2<System.Object,System.Object>)
extern void FSharpUtils_set_GetUnionCaseInfoTag_mC24E81FC03E96BD4646314EAF97520789F615108 ();
// 0x000002D6 Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::get_GetUnionCaseInfoFields()
extern void FSharpUtils_get_GetUnionCaseInfoFields_mE7A8E0725168B71E5D6A3ED8E18C07D919F71C02 ();
// 0x000002D7 System.Void Newtonsoft.Json.Utilities.FSharpUtils::set_GetUnionCaseInfoFields(Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>)
extern void FSharpUtils_set_GetUnionCaseInfoFields_mE5FE7298B7EE311641F84F92B18BD4108BB79BE0 ();
// 0x000002D8 System.Void Newtonsoft.Json.Utilities.FSharpUtils::EnsureInitialized(System.Reflection.Assembly)
extern void FSharpUtils_EnsureInitialized_mC086249BFF6B0BF729EFAD8A62B5F0A8746CF16F ();
// 0x000002D9 System.Reflection.MethodInfo Newtonsoft.Json.Utilities.FSharpUtils::GetMethodWithNonPublicFallback(System.Type,System.String,System.Reflection.BindingFlags)
extern void FSharpUtils_GetMethodWithNonPublicFallback_mC24F68C005762BC3B08F44413866120454854840 ();
// 0x000002DA Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Utilities.FSharpUtils::CreateFSharpFuncCall(System.Type,System.String)
extern void FSharpUtils_CreateFSharpFuncCall_m614708A6E248F2168276ACBEB1CCA716323C5025 ();
// 0x000002DB Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.FSharpUtils::CreateSeq(System.Type)
extern void FSharpUtils_CreateSeq_m888C3AF7529A3DC2DC33CC14FAC7D86DE5CFB622 ();
// 0x000002DC Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.FSharpUtils::CreateMap(System.Type,System.Type)
extern void FSharpUtils_CreateMap_m560B7BC17043E7899636DA14AEE556D3367899CC ();
// 0x000002DD Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.FSharpUtils::BuildMapCreator()
// 0x000002DE System.Void Newtonsoft.Json.Utilities.FSharpUtils::.cctor()
extern void FSharpUtils__cctor_m47748D78452BED39FF42305B84662F8478505D4C ();
// 0x000002DF System.Void Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m8F4C9A0A218E7E963FDF77F570707C41446EC293 ();
// 0x000002E0 System.Object Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass52_0::<CreateFSharpFuncCall>b__0(System.Object,System.Object[])
extern void U3CU3Ec__DisplayClass52_0_U3CCreateFSharpFuncCallU3Eb__0_mD4B79F06BAF047EFFBF16835AC71CB512AC80FA3 ();
// 0x000002E1 System.Void Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass55_0`2::.ctor()
// 0x000002E2 System.Object Newtonsoft.Json.Utilities.FSharpUtils_<>c__DisplayClass55_0`2::<BuildMapCreator>b__0(System.Object[])
// 0x000002E3 System.Void Newtonsoft.Json.Utilities.FSharpUtils_<>c__55`2::.cctor()
// 0x000002E4 System.Void Newtonsoft.Json.Utilities.FSharpUtils_<>c__55`2::.ctor()
// 0x000002E5 System.Tuple`2<TKey,TValue> Newtonsoft.Json.Utilities.FSharpUtils_<>c__55`2::<BuildMapCreator>b__55_1(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x000002E6 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::TryBuildImmutableForArrayContract(System.Type,System.Type,System.Type&,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>&)
extern void ImmutableCollectionsUtils_TryBuildImmutableForArrayContract_m615DD967E76C1EA2D75C1D32A77BA4A170B71927 ();
// 0x000002E7 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::TryBuildImmutableForDictionaryContract(System.Type,System.Type,System.Type,System.Type&,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>&)
extern void ImmutableCollectionsUtils_TryBuildImmutableForDictionaryContract_m9CD19B9631AC687A7BE76419F6569B0881FF4BA9 ();
// 0x000002E8 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils::.cctor()
extern void ImmutableCollectionsUtils__cctor_m93A5F710C736CE1DFEB3C4836C61E3FEE1E204DF ();
// 0x000002E9 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::.ctor(System.String,System.String,System.String)
extern void ImmutableCollectionTypeInfo__ctor_m380C79692588CE83860FD6ACEBF1F6C1CDA52F94 ();
// 0x000002EA System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_ContractTypeName()
extern void ImmutableCollectionTypeInfo_get_ContractTypeName_mC528E485DDD9527CA2A164A610AD216127598DB3 ();
// 0x000002EB System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_ContractTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_ContractTypeName_m043381BA6B90967C18A2A91A038CE1AD9EBFEB22 ();
// 0x000002EC System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_CreatedTypeName()
extern void ImmutableCollectionTypeInfo_get_CreatedTypeName_m46F8F292D4423EA6C1036F23AD87A57A5523B8F8 ();
// 0x000002ED System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_CreatedTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_CreatedTypeName_m0BBE3B6543C7AACEB1DD02441C190010A5691D17 ();
// 0x000002EE System.String Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::get_BuilderTypeName()
extern void ImmutableCollectionTypeInfo_get_BuilderTypeName_m1F99E1EBCC1E1BAACE37AB1C2059AC55AE7932A0 ();
// 0x000002EF System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo::set_BuilderTypeName(System.String)
extern void ImmutableCollectionTypeInfo_set_BuilderTypeName_mE05CDA8DECD5212A21F8364CF59D768FCD1C79A4 ();
// 0x000002F0 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mF099F2BE48E8CDDD9B561BEC04A9000331F3ED45 ();
// 0x000002F1 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass24_0::<TryBuildImmutableForArrayContract>b__0(Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo)
extern void U3CU3Ec__DisplayClass24_0_U3CTryBuildImmutableForArrayContractU3Eb__0_m3AC76513531ABE2C3CEAD0596DE221F1077C6B9D ();
// 0x000002F2 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::.cctor()
extern void U3CU3Ec__cctor_m2114B2EA54D263FDE85268A87BEF71B5167923B4 ();
// 0x000002F3 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::.ctor()
extern void U3CU3Ec__ctor_mED0DD564DD1BF14B6A32F5C37C193BB62AD1B6F6 ();
// 0x000002F4 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<TryBuildImmutableForArrayContract>b__24_1(System.Reflection.MethodInfo)
extern void U3CU3Ec_U3CTryBuildImmutableForArrayContractU3Eb__24_1_m8D13553E20529D7AF62FB583BC378870214C39A0 ();
// 0x000002F5 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c::<TryBuildImmutableForDictionaryContract>b__25_1(System.Reflection.MethodInfo)
extern void U3CU3Ec_U3CTryBuildImmutableForDictionaryContractU3Eb__25_1_m0952E998E9EAC984D43F9DA81129D70A11DF8890 ();
// 0x000002F6 System.Void Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mB3D5FDDD44A30B5FE01E76250A834CFFDC89DFEF ();
// 0x000002F7 System.Boolean Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_<>c__DisplayClass25_0::<TryBuildImmutableForDictionaryContract>b__0(Newtonsoft.Json.Utilities.ImmutableCollectionsUtils_ImmutableCollectionTypeInfo)
extern void U3CU3Ec__DisplayClass25_0_U3CTryBuildImmutableForDictionaryContractU3Eb__0_mBA9BAEAF7F3D5DF221304C324F20EFF72897D914 ();
// 0x000002F8 System.Char[] Newtonsoft.Json.Utilities.BufferUtils::RentBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void BufferUtils_RentBuffer_mD5A4DC34563041EAF58CA9856A34325BA2EF4B46 ();
// 0x000002F9 System.Void Newtonsoft.Json.Utilities.BufferUtils::ReturnBuffer(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[])
extern void BufferUtils_ReturnBuffer_mD23E0F30843D0613C5C23C598EA48944E6481C73 ();
// 0x000002FA System.Char[] Newtonsoft.Json.Utilities.BufferUtils::EnsureBufferSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32,System.Char[])
extern void BufferUtils_EnsureBufferSize_mD6D3E409713EC1C9CBF11AFCA938117CF1F0F5D9 ();
// 0x000002FB System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::.cctor()
extern void JavaScriptUtils__cctor_mFEA98829B065FFA69BB276691A83FBFADA27C70A ();
// 0x000002FC System.Boolean[] Newtonsoft.Json.Utilities.JavaScriptUtils::GetCharEscapeFlags(Newtonsoft.Json.StringEscapeHandling,System.Char)
extern void JavaScriptUtils_GetCharEscapeFlags_m97FB6B18815E2740357EF8892B0A36DD73A4D2AE ();
// 0x000002FD System.Boolean Newtonsoft.Json.Utilities.JavaScriptUtils::ShouldEscapeJavaScriptString(System.String,System.Boolean[])
extern void JavaScriptUtils_ShouldEscapeJavaScriptString_mE390817FB23DD5307E1500239870AD1EFAF4D9AC ();
// 0x000002FE System.Void Newtonsoft.Json.Utilities.JavaScriptUtils::WriteEscapedJavaScriptString(System.IO.TextWriter,System.String,System.Char,System.Boolean,System.Boolean[],Newtonsoft.Json.StringEscapeHandling,Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[]&)
extern void JavaScriptUtils_WriteEscapedJavaScriptString_m8D385039955CB5ADC931E18073F29A90A859F871 ();
// 0x000002FF System.String Newtonsoft.Json.Utilities.JavaScriptUtils::ToEscapedJavaScriptString(System.String,System.Char,System.Boolean,Newtonsoft.Json.StringEscapeHandling)
extern void JavaScriptUtils_ToEscapedJavaScriptString_m2874A73B04CDACB7BD5092C7D8BEC9A3146AC09F ();
// 0x00000300 System.Int32 Newtonsoft.Json.Utilities.JavaScriptUtils::FirstCharToEscape(System.String,System.Boolean[],Newtonsoft.Json.StringEscapeHandling)
extern void JavaScriptUtils_FirstCharToEscape_mA6D67E3CCBF45CA2277E52C7EC1D3C99847A7240 ();
// 0x00000301 System.Boolean Newtonsoft.Json.Utilities.JavaScriptUtils::TryGetDateFromConstructorJson(Newtonsoft.Json.JsonReader,System.DateTime&,System.String&)
extern void JavaScriptUtils_TryGetDateFromConstructorJson_m7F38C0BA2AFD1C9AA887FB9499135D419CAB294C ();
// 0x00000302 System.Boolean Newtonsoft.Json.Utilities.JavaScriptUtils::TryGetDateConstructorValue(Newtonsoft.Json.JsonReader,System.Nullable`1<System.Int64>&,System.String&)
extern void JavaScriptUtils_TryGetDateConstructorValue_mFE2781B4E31177148E612DCC0EE69830BE6AFAF9 ();
// 0x00000303 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsEndToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsEndToken_mB84045847A9B44D14B001F29A92719B28D105DDF ();
// 0x00000304 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsStartToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsStartToken_m2735210D81474849D62B19250B0A27980E7B75A2 ();
// 0x00000305 System.Boolean Newtonsoft.Json.Utilities.JsonTokenUtils::IsPrimitiveToken(Newtonsoft.Json.JsonToken)
extern void JsonTokenUtils_IsPrimitiveToken_m124F7EA7B7D39A36699293C0A971AAE26A02F78A ();
// 0x00000306 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::get_Instance()
extern void LateBoundReflectionDelegateFactory_get_Instance_m4755DCFBF8E5C50D8F934D54F7F05BA3DB8DFCCD ();
// 0x00000307 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
extern void LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m556CF237D21F5F52DB41DEBE111AEE5589891006 ();
// 0x00000308 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x00000309 System.Func`1<T> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x0000030A System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x0000030B System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x0000030C System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x0000030D System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x0000030E System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.ctor()
extern void LateBoundReflectionDelegateFactory__ctor_mB1B201FFE8A4F7A4F6BAB610503B14BC59F5AC9A ();
// 0x0000030F System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.cctor()
extern void LateBoundReflectionDelegateFactory__cctor_mFA58E688133C3D0E6924A84EF136875BC0154321 ();
// 0x00000310 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB6D599DADDC2AF6F584E4468AEA7959F81BC537F ();
// 0x00000311 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m2DCDBEAEFEC11A15A42FBF50DE525BBF874863A4 ();
// 0x00000312 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass3_0::<CreateParameterizedConstructor>b__1(System.Object[])
extern void U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_m867B457418C43B80173C34AE08C7F75BBA3FB618 ();
// 0x00000313 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::.ctor()
// 0x00000314 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::<CreateMethodCall>b__0(T,System.Object[])
// 0x00000315 System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass4_0`1::<CreateMethodCall>b__1(T,System.Object[])
// 0x00000316 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::.ctor()
// 0x00000317 T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__0()
// 0x00000318 T Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass5_0`1::<CreateDefaultConstructor>b__1()
// 0x00000319 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass6_0`1::.ctor()
// 0x0000031A System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass6_0`1::<CreateGet>b__0(T)
// 0x0000031B System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass7_0`1::.ctor()
// 0x0000031C System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass7_0`1::<CreateGet>b__0(T)
// 0x0000031D System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass8_0`1::.ctor()
// 0x0000031E System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass8_0`1::<CreateSet>b__0(T,System.Object)
// 0x0000031F System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass9_0`1::.ctor()
// 0x00000320 System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory_<>c__DisplayClass9_0`1::<CreateSet>b__0(T,System.Object)
// 0x00000321 System.Int32 Newtonsoft.Json.Utilities.MathUtils::IntLength(System.UInt64)
extern void MathUtils_IntLength_m763650DE40D1E26C73EDED34ECB5AC4040B4C380 ();
// 0x00000322 System.Char Newtonsoft.Json.Utilities.MathUtils::IntToHex(System.Int32)
extern void MathUtils_IntToHex_m1FC55F6E4DC6AD83E9D67B5F7567C29396C311C0 ();
// 0x00000323 System.Boolean Newtonsoft.Json.Utilities.MathUtils::ApproxEquals(System.Double,System.Double)
extern void MathUtils_ApproxEquals_mBE0405CC42BAABB5E73BAFF38DF042305E4F6531 ();
// 0x00000324 System.Void Newtonsoft.Json.Utilities.MethodCall`2::.ctor(System.Object,System.IntPtr)
// 0x00000325 TResult Newtonsoft.Json.Utilities.MethodCall`2::Invoke(T,System.Object[])
// 0x00000326 System.IAsyncResult Newtonsoft.Json.Utilities.MethodCall`2::BeginInvoke(T,System.Object[],System.AsyncCallback,System.Object)
// 0x00000327 TResult Newtonsoft.Json.Utilities.MethodCall`2::EndInvoke(System.IAsyncResult)
// 0x00000328 System.Boolean Newtonsoft.Json.Utilities.MiscellaneousUtils::ValueEquals(System.Object,System.Object)
extern void MiscellaneousUtils_ValueEquals_mECB672BCBAEA002D27D7B7F496CE685CC07C9EC1 ();
// 0x00000329 System.ArgumentOutOfRangeException Newtonsoft.Json.Utilities.MiscellaneousUtils::CreateArgumentOutOfRangeException(System.String,System.Object,System.String)
extern void MiscellaneousUtils_CreateArgumentOutOfRangeException_mBF0CC2C7BD3B17559AF2E2D6B7F8DF8A31721248 ();
// 0x0000032A System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::ToString(System.Object)
extern void MiscellaneousUtils_ToString_m3D4BCB1F1205454E136CCBC4D17321A00E5D2CED ();
// 0x0000032B System.Int32 Newtonsoft.Json.Utilities.MiscellaneousUtils::ByteArrayCompare(System.Byte[],System.Byte[])
extern void MiscellaneousUtils_ByteArrayCompare_m194BBA180A5EBF6B190A17BAA9D610C1A5589DFF ();
// 0x0000032C System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::GetPrefix(System.String)
extern void MiscellaneousUtils_GetPrefix_m45C66A058790A154139F969F85DAA92B6ED8C2C2 ();
// 0x0000032D System.String Newtonsoft.Json.Utilities.MiscellaneousUtils::GetLocalName(System.String)
extern void MiscellaneousUtils_GetLocalName_m5C36FD76824F2E26D4B38C9B3CFC0879B1947843 ();
// 0x0000032E System.Void Newtonsoft.Json.Utilities.MiscellaneousUtils::GetQualifiedNameParts(System.String,System.String&,System.String&)
extern void MiscellaneousUtils_GetQualifiedNameParts_m03391FF5C5AE3BDB797286C3B0FB80A4397F56C0 ();
// 0x0000032F System.Text.RegularExpressions.RegexOptions Newtonsoft.Json.Utilities.MiscellaneousUtils::GetRegexOptions(System.String)
extern void MiscellaneousUtils_GetRegexOptions_m88971A02045D20AFD899F0790B9CCFBD359DC46C ();
// 0x00000330 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.MemberInfo)
// 0x00000331 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.MemberInfo)
// 0x00000332 Newtonsoft.Json.Utilities.MethodCall`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateMethodCall(System.Reflection.MethodBase)
// 0x00000333 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateParameterizedConstructor(System.Reflection.MethodBase)
// 0x00000334 System.Func`1<T> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateDefaultConstructor(System.Type)
// 0x00000335 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.PropertyInfo)
// 0x00000336 System.Func`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateGet(System.Reflection.FieldInfo)
// 0x00000337 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.FieldInfo)
// 0x00000338 System.Action`2<T,System.Object> Newtonsoft.Json.Utilities.ReflectionDelegateFactory::CreateSet(System.Reflection.PropertyInfo)
// 0x00000339 System.Void Newtonsoft.Json.Utilities.ReflectionDelegateFactory::.ctor()
extern void ReflectionDelegateFactory__ctor_m249ABB5C4DEC395A3741D22C46A0898F7E375C80 ();
// 0x0000033A System.Type Newtonsoft.Json.Utilities.ReflectionMember::get_MemberType()
extern void ReflectionMember_get_MemberType_m2FB1FB99AFE05485008EA486A47799727781C5EF ();
// 0x0000033B System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_MemberType(System.Type)
extern void ReflectionMember_set_MemberType_mEECF7DEA17AEE29166E28ED6A1EF67F1D70872E2 ();
// 0x0000033C System.Func`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::get_Getter()
extern void ReflectionMember_get_Getter_m1D3DD86B27D129BA2540F290ECDABC3AEDA68A8D ();
// 0x0000033D System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Getter(System.Func`2<System.Object,System.Object>)
extern void ReflectionMember_set_Getter_m56175EF5AE1EA3BE74049D66E920E6EAE252280E ();
// 0x0000033E System.Action`2<System.Object,System.Object> Newtonsoft.Json.Utilities.ReflectionMember::get_Setter()
extern void ReflectionMember_get_Setter_mE66CFA1687DC319AD43D279A73729827EB89E98E ();
// 0x0000033F System.Void Newtonsoft.Json.Utilities.ReflectionMember::set_Setter(System.Action`2<System.Object,System.Object>)
extern void ReflectionMember_set_Setter_m8F0B22DCCD00C4C6C9638B811E28422AC1F0233D ();
// 0x00000340 System.Void Newtonsoft.Json.Utilities.ReflectionMember::.ctor()
extern void ReflectionMember__ctor_m0BE4C4861DD0BEB72E03A2A1B608189746B5690A ();
// 0x00000341 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Utilities.ReflectionObject::get_Creator()
extern void ReflectionObject_get_Creator_m7E021F79D8C332AE3B0148786F1E831283054A7E ();
// 0x00000342 System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Utilities.ReflectionMember> Newtonsoft.Json.Utilities.ReflectionObject::get_Members()
extern void ReflectionObject_get_Members_mCB55B637679F287142CE51BC3955339CA50BC888 ();
// 0x00000343 System.Void Newtonsoft.Json.Utilities.ReflectionObject::.ctor(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void ReflectionObject__ctor_mB38E0F5C4530BEDEFBB9002CD82B2D84AB8F4BBB ();
// 0x00000344 System.Object Newtonsoft.Json.Utilities.ReflectionObject::GetValue(System.Object,System.String)
extern void ReflectionObject_GetValue_m90BE6B90640449C3340135617FC7D91C6A5A45FF ();
// 0x00000345 System.Void Newtonsoft.Json.Utilities.ReflectionObject::SetValue(System.Object,System.String,System.Object)
extern void ReflectionObject_SetValue_m7BA7A797EE7C9A7BF0D8A2DDB35D1453EEB416DD ();
// 0x00000346 System.Type Newtonsoft.Json.Utilities.ReflectionObject::GetType(System.String)
extern void ReflectionObject_GetType_mB8BA51631BFABD3BFB6D93A6B57982BCF135F911 ();
// 0x00000347 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.String[])
extern void ReflectionObject_Create_m31BBD22C0D51E671F5FF8D24DD3D9780DC883FFB ();
// 0x00000348 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Utilities.ReflectionObject::Create(System.Type,System.Reflection.MethodBase,System.String[])
extern void ReflectionObject_Create_mDDFAD2B0FA0DC5E3DC92E14E846AE4D194D351B1 ();
// 0x00000349 System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mCA910CCEA019B4A13B4F7225476D0D978C8009F5 ();
// 0x0000034A System.Object Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_0::<Create>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass11_0_U3CCreateU3Eb__0_m706A90DC257A712482CB841E7271BDC131889F11 ();
// 0x0000034B System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1::.ctor()
extern void U3CU3Ec__DisplayClass11_1__ctor_m3D4D3E4EE078DA0BF09127EC585A3430921B87F8 ();
// 0x0000034C System.Object Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_1::<Create>b__1(System.Object)
extern void U3CU3Ec__DisplayClass11_1_U3CCreateU3Eb__1_m4E5358FA5B992F2553B5AAEFF1964610342B5DDA ();
// 0x0000034D System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2::.ctor()
extern void U3CU3Ec__DisplayClass11_2__ctor_mCFD9155B5C6FB481BFE7901D8DA6D7C1F715091B ();
// 0x0000034E System.Void Newtonsoft.Json.Utilities.ReflectionObject_<>c__DisplayClass11_2::<Create>b__2(System.Object,System.Object)
extern void U3CU3Ec__DisplayClass11_2_U3CCreateU3Eb__2_mF77E5D911106CF0A2B2230068927A84D17069700 ();
// 0x0000034F System.Void Newtonsoft.Json.Utilities.ReflectionUtils::.cctor()
extern void ReflectionUtils__cctor_m141D35BC34CF7AA7FDE575E1447702DDE0812A86 ();
// 0x00000350 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsVirtual(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsVirtual_mA538A8B4A8C65D38448F5BD39CD05BD2C8CBB0D1 ();
// 0x00000351 System.Reflection.MethodInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetBaseDefinition(System.Reflection.PropertyInfo)
extern void ReflectionUtils_GetBaseDefinition_m3EE73CC7B521561A8E111356C5BE2AD1357B331A ();
// 0x00000352 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsPublic(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsPublic_m0F68EF7047D5DA214B7EE1B312AF0F2385CE1EE3 ();
// 0x00000353 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetObjectType(System.Object)
extern void ReflectionUtils_GetObjectType_m60C54E9B884211A4D3CBB9AC5E03D896E6BF0040 ();
// 0x00000354 System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetTypeName(System.Type,Newtonsoft.Json.TypeNameAssemblyFormatHandling,Newtonsoft.Json.Serialization.ISerializationBinder)
extern void ReflectionUtils_GetTypeName_mAAC5361EAE1AA98F0195D6D76266967A9CB848D9 ();
// 0x00000355 System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetFullyQualifiedTypeName(System.Type,Newtonsoft.Json.Serialization.ISerializationBinder)
extern void ReflectionUtils_GetFullyQualifiedTypeName_m41AE5258E8811FFF43EB821C785C237535E05BA5 ();
// 0x00000356 System.String Newtonsoft.Json.Utilities.ReflectionUtils::RemoveAssemblyDetails(System.String)
extern void ReflectionUtils_RemoveAssemblyDetails_m329AF4FBF44D518B59C8A9B41BEBD9FD30AC478D ();
// 0x00000357 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::HasDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_HasDefaultConstructor_m7AA2D83D5A4FEFF347FDB88C3AA57C2E49E3EF21 ();
// 0x00000358 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type)
extern void ReflectionUtils_GetDefaultConstructor_mF7DE07E23F2D6083F442B325618568E50A4196B7 ();
// 0x00000359 System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type,System.Boolean)
extern void ReflectionUtils_GetDefaultConstructor_m6E910D186A317CF4AC6527C3BDEA8E6FC539F804 ();
// 0x0000035A System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullable(System.Type)
extern void ReflectionUtils_IsNullable_m64CB43DBC25491FF5EC8855B1701B623176AA4EF ();
// 0x0000035B System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullableType(System.Type)
extern void ReflectionUtils_IsNullableType_m3A1A7823AF44D84487CD8D807286E42DD943BDC6 ();
// 0x0000035C System.Type Newtonsoft.Json.Utilities.ReflectionUtils::EnsureNotNullableType(System.Type)
extern void ReflectionUtils_EnsureNotNullableType_m5980E94E818D68F2439F14BDF8E944390C5685BA ();
// 0x0000035D System.Type Newtonsoft.Json.Utilities.ReflectionUtils::EnsureNotByRefType(System.Type)
extern void ReflectionUtils_EnsureNotByRefType_m98F3C76F5C2CCA42B321EA40AC442AD1C5D4395C ();
// 0x0000035E System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_IsGenericDefinition_m4A63A7D851BBD246C86C2ADEE1C8CBD44A2F8B0A ();
// 0x0000035F System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_ImplementsGenericDefinition_m5FA208833E2FB8C3F1F2FDDD7DB73EC9BF794E0B ();
// 0x00000360 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_ImplementsGenericDefinition_m971FDE4108C508C04B47CBCBEE00350C23FF40D7 ();
// 0x00000361 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type)
extern void ReflectionUtils_InheritsGenericDefinition_m8FF1C61B29FA9DA10B9F24FA1A3334559FD9D620 ();
// 0x00000362 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinition_m8E5BE681FC821447243C58D8EB1C7C879E97440B ();
// 0x00000363 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinitionInternal(System.Type,System.Type,System.Type&)
extern void ReflectionUtils_InheritsGenericDefinitionInternal_mAB6D807C8A07B022567347B93D74CDCF7AEDC6DD ();
// 0x00000364 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetCollectionItemType(System.Type)
extern void ReflectionUtils_GetCollectionItemType_m8707A39821344D59FF92CC038775BD49EB9A4710 ();
// 0x00000365 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryKeyValueTypes(System.Type,System.Type&,System.Type&)
extern void ReflectionUtils_GetDictionaryKeyValueTypes_mF6DB2A211AD51652C52C280F34A49F06130C878D ();
// 0x00000366 System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberUnderlyingType_m500350397C4D69A486064370A126CDC5D364D940 ();
// 0x00000367 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsByRefLikeType(System.Type)
extern void ReflectionUtils_IsByRefLikeType_m7B6BB9E870965F2031DBB2CA165D6D52A59BE382 ();
// 0x00000368 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.PropertyInfo)
extern void ReflectionUtils_IsIndexedProperty_m41C39B3318D5484BE6B701EE6BD8F00CD99A5ABF ();
// 0x00000369 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo,System.Boolean)
extern void ReflectionUtils_CanReadMemberValue_m09AC7DC90C5EADFB599FD0CC2F70ACCA4CCC5771 ();
// 0x0000036A System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo,System.Boolean,System.Boolean)
extern void ReflectionUtils_CanSetMemberValue_m94AFEA2E18E73F677AA63383DD54C95634CE9FF7 ();
// 0x0000036B System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFieldsAndProperties_mE327D2BB3CB07E0A245A24FBE7EA879EFA4EEBCD ();
// 0x0000036C System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsOverridenGenericMember(System.Reflection.MemberInfo,System.Reflection.BindingFlags)
extern void ReflectionUtils_IsOverridenGenericMember_m93A60DFD50FFCF5B2212B3D33E53E8EA16A3F02E ();
// 0x0000036D T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object)
// 0x0000036E T Newtonsoft.Json.Utilities.ReflectionUtils::GetAttribute(System.Object,System.Boolean)
// 0x0000036F T[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Boolean)
// 0x00000370 System.Attribute[] Newtonsoft.Json.Utilities.ReflectionUtils::GetAttributes(System.Object,System.Type,System.Boolean)
extern void ReflectionUtils_GetAttributes_mA9CDBD4D932B2FD8D7BCACEDA51B93631AB7E671 ();
// 0x00000371 Newtonsoft.Json.Utilities.StructMultiKey`2<System.String,System.String> Newtonsoft.Json.Utilities.ReflectionUtils::SplitFullyQualifiedTypeName(System.String)
extern void ReflectionUtils_SplitFullyQualifiedTypeName_m6A3EE9181E6889C5A44129B7A106B844EADBFD88 ();
// 0x00000372 System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.ReflectionUtils::GetAssemblyDelimiterIndex(System.String)
extern void ReflectionUtils_GetAssemblyDelimiterIndex_mE3A7B76A25E13ACBB3A5D700D35100A848681F89 ();
// 0x00000373 System.Reflection.MemberInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberInfoFromType(System.Type,System.Reflection.MemberInfo)
extern void ReflectionUtils_GetMemberInfoFromType_mADFBA62B34D382182EFC64E44E913F9BCAAE35B3 ();
// 0x00000374 System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFields(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetFields_m40A33C70A81AEAF5F1DC17DA0119903524CD8624 ();
// 0x00000375 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateFields(System.Collections.Generic.IList`1<System.Reflection.MemberInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateFields_m01B89F241EFCCD5BFF84FFCF4E3AD629A55AA323 ();
// 0x00000376 System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetProperties(System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetProperties_m53AA061FE94BA47A5D652A2C835F59F95CF40F3B ();
// 0x00000377 System.Reflection.BindingFlags Newtonsoft.Json.Utilities.ReflectionUtils::RemoveFlag(System.Reflection.BindingFlags,System.Reflection.BindingFlags)
extern void ReflectionUtils_RemoveFlag_mFE398FD30B419043A6A186AEAE26273D120CDCA9 ();
// 0x00000378 System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateProperties(System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>,System.Type,System.Reflection.BindingFlags)
extern void ReflectionUtils_GetChildPrivateProperties_m9066AAD0EBBA2CEB0532D7EC9E8E34861DDBE5CA ();
// 0x00000379 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsMethodOverridden(System.Type,System.Type,System.String)
extern void ReflectionUtils_IsMethodOverridden_m6283FCF8AF077955E0C7A9B77245AD0F2A877C5F ();
// 0x0000037A System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultValue(System.Type)
extern void ReflectionUtils_GetDefaultValue_m4108A8BF683F277BE923AFD01807442C370156B5 ();
// 0x0000037B System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c::.cctor()
extern void U3CU3Ec__cctor_mB7D547DD9FE8CA18E4EEC98E4EFF859314812F02 ();
// 0x0000037C System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c::.ctor()
extern void U3CU3Ec__ctor_mF38069D565143CDA23DEB4D8C99D3492CFF7B2AB ();
// 0x0000037D System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetDefaultConstructor>b__11_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetDefaultConstructorU3Eb__11_0_mAC2A19F7D1D9BD408085354779DC1B378A54983B ();
// 0x0000037E System.String Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetFieldsAndProperties>b__31_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__31_0_m8593F693E81487AC48355DB19125C639AF9080BD ();
// 0x0000037F System.Type Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetMemberInfoFromType>b__39_0(System.Reflection.ParameterInfo)
extern void U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__39_0_m1EB8B449DF74D0923D52D93E268FD21260953885 ();
// 0x00000380 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c::<GetChildPrivateFields>b__41_0(System.Reflection.FieldInfo)
extern void U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__41_0_mA5E1602F2958D13F9D38611DF12B75326D16BC4E ();
// 0x00000381 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m22D408A7A566AE5EDFC10FB5AF11ACB21D4DFA56 ();
// 0x00000382 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass31_0::<GetFieldsAndProperties>b__1(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass31_0_U3CGetFieldsAndPropertiesU3Eb__1_m4BCC524D52F430A510E8D5BCB7EDE999C94AAD30 ();
// 0x00000383 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mC63424D2E56D8BEA64E9EBF883111549A68AE764 ();
// 0x00000384 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass44_0::<GetChildPrivateProperties>b__0(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass44_0_U3CGetChildPrivatePropertiesU3Eb__0_mA27C7B364B7D72D971AC533933B8717FEF752E33 ();
// 0x00000385 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass44_0::<GetChildPrivateProperties>b__1(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass44_0_U3CGetChildPrivatePropertiesU3Eb__1_m72FC23A9AF1BE1FB5BB0FA53197B0A3C737FB0F8 ();
// 0x00000386 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass44_1::.ctor()
extern void U3CU3Ec__DisplayClass44_1__ctor_mB2F40E9D2DEC82C7EB0B50C82BDBCBBE7791C048 ();
// 0x00000387 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass44_1::<GetChildPrivateProperties>b__2(System.Reflection.PropertyInfo)
extern void U3CU3Ec__DisplayClass44_1_U3CGetChildPrivatePropertiesU3Eb__2_mB59113D93F7231C1B12FF99EEEA8A8EACF8844D6 ();
// 0x00000388 System.Void Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mA55114503AE0ACE0EC70A09F2950809AA350C56B ();
// 0x00000389 System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils_<>c__DisplayClass45_0::<IsMethodOverridden>b__0(System.Reflection.MethodInfo)
extern void U3CU3Ec__DisplayClass45_0_U3CIsMethodOverriddenU3Eb__0_mB829FE3CA5C61ADBD41F3439838940E4598E72BB ();
// 0x0000038A System.Int32 Newtonsoft.Json.Utilities.StringBuffer::get_Position()
extern void StringBuffer_get_Position_m83F723C2AF17EF7D1C375E47524FA7AB88171B7E_AdjustorThunk ();
// 0x0000038B System.Void Newtonsoft.Json.Utilities.StringBuffer::set_Position(System.Int32)
extern void StringBuffer_set_Position_m2BFD7C5B2352AF33D9AA3C824923133989E7805F_AdjustorThunk ();
// 0x0000038C System.Boolean Newtonsoft.Json.Utilities.StringBuffer::get_IsEmpty()
extern void StringBuffer_get_IsEmpty_m3A209BFC219A27F107A45E3FF97C4F77B882A557_AdjustorThunk ();
// 0x0000038D System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer__ctor_mEF7BDBA7FB252D738CA1248D5F93B8B4805CEA88_AdjustorThunk ();
// 0x0000038E System.Void Newtonsoft.Json.Utilities.StringBuffer::.ctor(System.Char[])
extern void StringBuffer__ctor_m69AF5866650F170CF733B000E178531B0267ED01_AdjustorThunk ();
// 0x0000038F System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char)
extern void StringBuffer_Append_m160730AFEB21E3CD5A8B1D597D653E424A4E16C6_AdjustorThunk ();
// 0x00000390 System.Void Newtonsoft.Json.Utilities.StringBuffer::Append(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Char[],System.Int32,System.Int32)
extern void StringBuffer_Append_mC5ED80F3F3847B8B9DF502E89BB51DEA9D13B49B_AdjustorThunk ();
// 0x00000391 System.Void Newtonsoft.Json.Utilities.StringBuffer::Clear(Newtonsoft.Json.IArrayPool`1<System.Char>)
extern void StringBuffer_Clear_mD5F44587D8FB55BE8C93F2FB7EED70E699712BD3_AdjustorThunk ();
// 0x00000392 System.Void Newtonsoft.Json.Utilities.StringBuffer::EnsureSize(Newtonsoft.Json.IArrayPool`1<System.Char>,System.Int32)
extern void StringBuffer_EnsureSize_mEB9C44AB1E16068A4BC4429E552ADC07D347A0F3_AdjustorThunk ();
// 0x00000393 System.String Newtonsoft.Json.Utilities.StringBuffer::ToString()
extern void StringBuffer_ToString_m57E05B88A083A828ACD1FCAE20928B77B811EC9A_AdjustorThunk ();
// 0x00000394 System.String Newtonsoft.Json.Utilities.StringBuffer::ToString(System.Int32,System.Int32)
extern void StringBuffer_ToString_m61E1C3FDB3DC0CDCCCB6192B581779F99559174B_AdjustorThunk ();
// 0x00000395 System.Char[] Newtonsoft.Json.Utilities.StringBuffer::get_InternalBuffer()
extern void StringBuffer_get_InternalBuffer_mB3BF2E89A05E07D7E80528801009C9389B7009DB_AdjustorThunk ();
// 0x00000396 System.Char Newtonsoft.Json.Utilities.StringReference::get_Item(System.Int32)
extern void StringReference_get_Item_mEE1E8EF598FEF02A9CB4C5605DC9CFB385529D82_AdjustorThunk ();
// 0x00000397 System.Char[] Newtonsoft.Json.Utilities.StringReference::get_Chars()
extern void StringReference_get_Chars_mB940C355A3E54CF70A0BFD8E0F057845C74EA565_AdjustorThunk ();
// 0x00000398 System.Int32 Newtonsoft.Json.Utilities.StringReference::get_StartIndex()
extern void StringReference_get_StartIndex_m937057682C248341C618E1591F2DB8490DF00087_AdjustorThunk ();
// 0x00000399 System.Int32 Newtonsoft.Json.Utilities.StringReference::get_Length()
extern void StringReference_get_Length_m820A37B0476E248C88EE1C1248D87D1D3CCCEACC_AdjustorThunk ();
// 0x0000039A System.Void Newtonsoft.Json.Utilities.StringReference::.ctor(System.Char[],System.Int32,System.Int32)
extern void StringReference__ctor_mF6FCA9412B7D5EE743F083798D09476263B4B9B2_AdjustorThunk ();
// 0x0000039B System.String Newtonsoft.Json.Utilities.StringReference::ToString()
extern void StringReference_ToString_m5900C8D292A447D1E52DC57DC7E38C20FA9A687B_AdjustorThunk ();
// 0x0000039C System.Int32 Newtonsoft.Json.Utilities.StringReferenceExtensions::IndexOf(Newtonsoft.Json.Utilities.StringReference,System.Char,System.Int32,System.Int32)
extern void StringReferenceExtensions_IndexOf_mED906B77321DE3BAA9EDDE334171965E019898CB ();
// 0x0000039D System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::StartsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_StartsWith_mA192ED52E608ED33CCC781FB4648C2545365B8A1 ();
// 0x0000039E System.Boolean Newtonsoft.Json.Utilities.StringReferenceExtensions::EndsWith(Newtonsoft.Json.Utilities.StringReference,System.String)
extern void StringReferenceExtensions_EndsWith_mE5037AE2181E864E853859016143C44FFA09CD33 ();
// 0x0000039F System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsNullOrEmpty(System.String)
extern void StringUtils_IsNullOrEmpty_mB58C32CFC344EB6F80BB592F45E136DA99C40E37 ();
// 0x000003A0 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object)
extern void StringUtils_FormatWith_m2D67274D9DA1C6E2E96118B205A0F7EC50506DDA ();
// 0x000003A1 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object)
extern void StringUtils_FormatWith_mE72B9659176DE4E52E28A5E3BC5F0826BF7642B5 ();
// 0x000003A2 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_m13F02CC8660679187AFACB877D6A9E1FDCF76841 ();
// 0x000003A3 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object,System.Object,System.Object,System.Object)
extern void StringUtils_FormatWith_m61F5B5F89CB62CF25E2163918DF2CBF5C1AC7918 ();
// 0x000003A4 System.String Newtonsoft.Json.Utilities.StringUtils::FormatWith(System.String,System.IFormatProvider,System.Object[])
extern void StringUtils_FormatWith_mABDDFF33E7972C10F36A72C8DCE0685DB7B5CB13 ();
// 0x000003A5 System.IO.StringWriter Newtonsoft.Json.Utilities.StringUtils::CreateStringWriter(System.Int32)
extern void StringUtils_CreateStringWriter_mDB3A91DB84B0A08D80B321CC8B568C11C800E302 ();
// 0x000003A6 System.Void Newtonsoft.Json.Utilities.StringUtils::ToCharAsUnicode(System.Char,System.Char[])
extern void StringUtils_ToCharAsUnicode_mCC37911576DD31F842C29294DF8C7F1C1081C668 ();
// 0x000003A7 TSource Newtonsoft.Json.Utilities.StringUtils::ForgivingCaseSensitiveFind(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.String>,System.String)
// 0x000003A8 System.String Newtonsoft.Json.Utilities.StringUtils::ToCamelCase(System.String)
extern void StringUtils_ToCamelCase_m43B382C941A5F19EF05F16D0A722AD02763FBA30 ();
// 0x000003A9 System.Char Newtonsoft.Json.Utilities.StringUtils::ToLower(System.Char)
extern void StringUtils_ToLower_m769207EF0A7BBA0B60C04A27CA63C77998C95BB8 ();
// 0x000003AA System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsHighSurrogate(System.Char)
extern void StringUtils_IsHighSurrogate_m6179B135893122AF29A06C037B51175E9B8C633D ();
// 0x000003AB System.Boolean Newtonsoft.Json.Utilities.StringUtils::IsLowSurrogate(System.Char)
extern void StringUtils_IsLowSurrogate_mF7D22770A1FECFE3F6A8BCB767BE0AD7CA6CC098 ();
// 0x000003AC System.Boolean Newtonsoft.Json.Utilities.StringUtils::StartsWith(System.String,System.Char)
extern void StringUtils_StartsWith_m3F00634D0FE8E40155E75AB46E17151560F6EC71 ();
// 0x000003AD System.Boolean Newtonsoft.Json.Utilities.StringUtils::EndsWith(System.String,System.Char)
extern void StringUtils_EndsWith_m51572DC9F29F5D3E0F53C0011788803A1C4C7DA0 ();
// 0x000003AE System.String Newtonsoft.Json.Utilities.StringUtils::Trim(System.String,System.Int32,System.Int32)
extern void StringUtils_Trim_m6CC1277CA2FCD3B1865A3BC50EBB03EE67E15D1C ();
// 0x000003AF System.Void Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass14_0`1::.ctor()
// 0x000003B0 System.Boolean Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass14_0`1::<ForgivingCaseSensitiveFind>b__0(TSource)
// 0x000003B1 System.Boolean Newtonsoft.Json.Utilities.StringUtils_<>c__DisplayClass14_0`1::<ForgivingCaseSensitiveFind>b__1(TSource)
// 0x000003B2 System.Void Newtonsoft.Json.Utilities.StructMultiKey`2::.ctor(T1,T2)
// 0x000003B3 System.Int32 Newtonsoft.Json.Utilities.StructMultiKey`2::GetHashCode()
// 0x000003B4 System.Boolean Newtonsoft.Json.Utilities.StructMultiKey`2::Equals(System.Object)
// 0x000003B5 System.Boolean Newtonsoft.Json.Utilities.StructMultiKey`2::Equals(Newtonsoft.Json.Utilities.StructMultiKey`2<T1,T2>)
// 0x000003B6 System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2::.ctor(System.Func`2<TKey,TValue>)
// 0x000003B7 TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2::Get(TKey)
// 0x000003B8 System.Reflection.MemberTypes Newtonsoft.Json.Utilities.TypeExtensions::MemberType(System.Reflection.MemberInfo)
extern void TypeExtensions_MemberType_m4C01999F78A2E00448DEA2EFF8A07E751E82D974 ();
// 0x000003B9 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ContainsGenericParameters(System.Type)
extern void TypeExtensions_ContainsGenericParameters_mD1381A549AE755AAEBDFC7EA7F14A1BF969A668E ();
// 0x000003BA System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsInterface(System.Type)
extern void TypeExtensions_IsInterface_mF90F1C446936F97CE69CA11E0B5352EF9D4902E4 ();
// 0x000003BB System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericType(System.Type)
extern void TypeExtensions_IsGenericType_mF8FA3DA2153AF6FA2738B61BC4FDE19BD54D0539 ();
// 0x000003BC System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsGenericTypeDefinition(System.Type)
extern void TypeExtensions_IsGenericTypeDefinition_mB6618E157D2E9A9C47DAFB4D33A44D9C8CDC1DE0 ();
// 0x000003BD System.Type Newtonsoft.Json.Utilities.TypeExtensions::BaseType(System.Type)
extern void TypeExtensions_BaseType_mCF843FF6E543AA3A42BA832968905E2AA2C27969 ();
// 0x000003BE System.Reflection.Assembly Newtonsoft.Json.Utilities.TypeExtensions::Assembly(System.Type)
extern void TypeExtensions_Assembly_mF83E7EC9A6CF5895A8ED903B5222C0FF49F12C6D ();
// 0x000003BF System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsEnum(System.Type)
extern void TypeExtensions_IsEnum_m86C76615ECEC5C1FD385DF3EE1AFB273213F897A ();
// 0x000003C0 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsClass(System.Type)
extern void TypeExtensions_IsClass_m5505387F00584AA3E1F4E1984F5C8E900750434E ();
// 0x000003C1 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsSealed(System.Type)
extern void TypeExtensions_IsSealed_mFBA15084B6383583EE26520B9727FA67B5156646 ();
// 0x000003C2 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsAbstract(System.Type)
extern void TypeExtensions_IsAbstract_mD5B7437406F44CFB14BAC3D8900C16B67258FA21 ();
// 0x000003C3 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsVisible(System.Type)
extern void TypeExtensions_IsVisible_m468C89574A86C94ABFFC5D3137A4BB15E3C93952 ();
// 0x000003C4 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsValueType(System.Type)
extern void TypeExtensions_IsValueType_m69BF09F047B3D8E303A2856ADBB1FCD7DDC2FB1B ();
// 0x000003C5 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::IsPrimitive(System.Type)
extern void TypeExtensions_IsPrimitive_mE0AB728968073C7BEA002B71A49672502C210FA7 ();
// 0x000003C6 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String,System.Boolean,System.Type&)
extern void TypeExtensions_AssignableToTypeName_mDC42F639FB2F8F876EC548F608795DBB6F153214 ();
// 0x000003C7 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::AssignableToTypeName(System.Type,System.String,System.Boolean)
extern void TypeExtensions_AssignableToTypeName_m8D9CB15EDD79A2DF4E1CCF5E33EA3584F0E70639 ();
// 0x000003C8 System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::ImplementInterface(System.Type,System.Type)
extern void TypeExtensions_ImplementInterface_mEFCE6A62B07FA6F10B8A7FC816FAFBB8DF53977D ();
// 0x000003C9 System.Void Newtonsoft.Json.Utilities.ValidationUtils::ArgumentNotNull(System.Object,System.String)
extern void ValidationUtils_ArgumentNotNull_mC5F3D8E2D73FC0F7A45A428EF878E9B8B59C59D0 ();
// 0x000003CA T Newtonsoft.Json.Serialization.CachedAttributeGetter`1::GetAttribute(System.Object)
// 0x000003CB System.Void Newtonsoft.Json.Serialization.CachedAttributeGetter`1::.cctor()
// 0x000003CC System.Void Newtonsoft.Json.Serialization.CamelCaseNamingStrategy::.ctor()
extern void CamelCaseNamingStrategy__ctor_m89BCD9FDF6BD285829A6F1EB37BD1B45DB135F03 ();
// 0x000003CD System.String Newtonsoft.Json.Serialization.CamelCaseNamingStrategy::ResolvePropertyName(System.String)
extern void CamelCaseNamingStrategy_ResolvePropertyName_m88E5CB044B18FA4355EEA66C256C15B378F10469 ();
// 0x000003CE Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::get_Instance()
extern void DefaultContractResolver_get_Instance_m79001EE9FDA11D9969EA252074830FC118E4389D ();
// 0x000003CF System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::get_DefaultMembersSearchFlags()
extern void DefaultContractResolver_get_DefaultMembersSearchFlags_m12FD36351239622699BE5343E3DC59BB9B393169 ();
// 0x000003D0 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_DefaultMembersSearchFlags(System.Reflection.BindingFlags)
extern void DefaultContractResolver_set_DefaultMembersSearchFlags_m08146C8A73EB011E85CB6312FE6DE49CC2E7EEAD ();
// 0x000003D1 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_SerializeCompilerGeneratedMembers()
extern void DefaultContractResolver_get_SerializeCompilerGeneratedMembers_m78AFD3ADFD68C3FD34D99DAED756EC787C5B9E03 ();
// 0x000003D2 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableInterface()
extern void DefaultContractResolver_get_IgnoreSerializableInterface_mF8630105A6B09519C1B4755CB812803299F65F74 ();
// 0x000003D3 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreSerializableAttribute()
extern void DefaultContractResolver_get_IgnoreSerializableAttribute_mD55E08E2FD8BD0BB7A4230DA5BB5011E3540D056 ();
// 0x000003D4 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_IgnoreSerializableAttribute(System.Boolean)
extern void DefaultContractResolver_set_IgnoreSerializableAttribute_m4CE0B5099EA76A21F9AE603A669AAF88F05A3129 ();
// 0x000003D5 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreIsSpecifiedMembers()
extern void DefaultContractResolver_get_IgnoreIsSpecifiedMembers_mE6E81B7AEBBB2EC324EA1BF2451CF65E4FF4F974 ();
// 0x000003D6 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_IgnoreShouldSerializeMembers()
extern void DefaultContractResolver_get_IgnoreShouldSerializeMembers_mF309E6A38AFB542467A41C0BDAFBAF344B1FE917 ();
// 0x000003D7 Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver::get_NamingStrategy()
extern void DefaultContractResolver_get_NamingStrategy_mA312DF5B0ED1B3D7E47B01F0481DD497AA3F3402 ();
// 0x000003D8 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor()
extern void DefaultContractResolver__ctor_m6D9934090BC1016112BD095CF4A74AAC54B1DABE ();
// 0x000003D9 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContract(System.Type)
extern void DefaultContractResolver_ResolveContract_m340256C4421D2AA299EF22073B3D37839B663794 ();
// 0x000003DA System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::FilterMembers(System.Reflection.MemberInfo)
extern void DefaultContractResolver_FilterMembers_mFB3977C8169BC0E1AA5AFBCCEE2E6EBE3197C8AD ();
// 0x000003DB System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::GetSerializableMembers(System.Type)
extern void DefaultContractResolver_GetSerializableMembers_m0773ED45174599D1DE355F2CB9082D0E313792C8 ();
// 0x000003DC System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSerializeEntityMember(System.Reflection.MemberInfo)
extern void DefaultContractResolver_ShouldSerializeEntityMember_mB2785F834AA160FE4E4A991C612CBBC56BC7F91C ();
// 0x000003DD Newtonsoft.Json.Serialization.JsonObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateObjectContract(System.Type)
extern void DefaultContractResolver_CreateObjectContract_mF7084C9A7823AD7B4C064278159C2EAAFCF2C14E ();
// 0x000003DE System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ThrowUnableToSerializeError(System.Object,System.Runtime.Serialization.StreamingContext)
extern void DefaultContractResolver_ThrowUnableToSerializeError_m3169C87B1B938B09E670A8DFF37CDF0D08A6F658 ();
// 0x000003DF System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetExtensionDataMemberForType(System.Type)
extern void DefaultContractResolver_GetExtensionDataMemberForType_m4EF1893B50CF8DA4251F57C5699DBD4259E1944F ();
// 0x000003E0 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetExtensionDataDelegates(Newtonsoft.Json.Serialization.JsonObjectContract,System.Reflection.MemberInfo)
extern void DefaultContractResolver_SetExtensionDataDelegates_m8B786C651E53FE50810644F15C03E855EE57953F ();
// 0x000003E1 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetAttributeConstructor(System.Type)
extern void DefaultContractResolver_GetAttributeConstructor_m42B7C359258ABFB146142FA1E674446426EB7E8F ();
// 0x000003E2 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetImmutableConstructor(System.Type,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void DefaultContractResolver_GetImmutableConstructor_m26009C0F25BC0C9AD5036F82418DBB340A464A63 ();
// 0x000003E3 System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetParameterizedConstructor(System.Type)
extern void DefaultContractResolver_GetParameterizedConstructor_m150BA2A3CA8F495B1948D8B0B717C526E53FD6AC ();
// 0x000003E4 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateConstructorParameters(System.Reflection.ConstructorInfo,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern void DefaultContractResolver_CreateConstructorParameters_m433C63293A41D93CB8AD11B4CDD2FC3FAAFCF3AE ();
// 0x000003E5 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::MatchProperty(Newtonsoft.Json.Serialization.JsonPropertyCollection,System.String,System.Type)
extern void DefaultContractResolver_MatchProperty_m80097FB0A4EEAD32B0574A3CB7E2D2BD1564C307 ();
// 0x000003E6 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePropertyFromConstructorParameter(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ParameterInfo)
extern void DefaultContractResolver_CreatePropertyFromConstructorParameter_m97555C234C57331CFA17332902721711A999BC43 ();
// 0x000003E7 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContractConverter(System.Type)
extern void DefaultContractResolver_ResolveContractConverter_mF3EBD8169ABFEF08A418F369637006F4CF4BF6D5 ();
// 0x000003E8 System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::GetDefaultCreator(System.Type)
extern void DefaultContractResolver_GetDefaultCreator_m959DBE4C1291F84AE4369F6348E0AFFB66C8F09B ();
// 0x000003E9 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::InitializeContract(Newtonsoft.Json.Serialization.JsonContract)
extern void DefaultContractResolver_InitializeContract_m1BCF68B795E9644F0A985B8E3EB1C2224825A583 ();
// 0x000003EA System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveCallbackMethods(Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void DefaultContractResolver_ResolveCallbackMethods_m306ECF212B1DF74639989D1FA363A30ADA9533A8 ();
// 0x000003EB System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::GetCallbackMethodsForType(System.Type,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>&,System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>&)
extern void DefaultContractResolver_GetCallbackMethodsForType_m4787DE539841B684F694B743F1A5799CA491B13B ();
// 0x000003EC System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsConcurrentOrObservableCollection(System.Type)
extern void DefaultContractResolver_IsConcurrentOrObservableCollection_m9A00B3B3E34002280D66DBAB0D5C869C6EE39D01 ();
// 0x000003ED System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipDeserialized(System.Type)
extern void DefaultContractResolver_ShouldSkipDeserialized_mBA93B215D74D6840714146F90EB9585C73C16814 ();
// 0x000003EE System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSkipSerializing(System.Type)
extern void DefaultContractResolver_ShouldSkipSerializing_m9F236D5943C722D3D372857836897CCDE25CAC2C ();
// 0x000003EF System.Collections.Generic.List`1<System.Type> Newtonsoft.Json.Serialization.DefaultContractResolver::GetClassHierarchyForType(System.Type)
extern void DefaultContractResolver_GetClassHierarchyForType_mF58899EAE6D11DCAB0457C83C1A281BEBE58D4F8 ();
// 0x000003F0 Newtonsoft.Json.Serialization.JsonDictionaryContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDictionaryContract(System.Type)
extern void DefaultContractResolver_CreateDictionaryContract_m5E5B0500D9C84CF069381CEB27EC290528C27FDD ();
// 0x000003F1 Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateArrayContract(System.Type)
extern void DefaultContractResolver_CreateArrayContract_m205E5B2ADB22D252879EA0351C0AFE6B26BA8C42 ();
// 0x000003F2 Newtonsoft.Json.Serialization.JsonPrimitiveContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePrimitiveContract(System.Type)
extern void DefaultContractResolver_CreatePrimitiveContract_mAA8B3A37089C28834A512436E568B12292C1B244 ();
// 0x000003F3 Newtonsoft.Json.Serialization.JsonLinqContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateLinqContract(System.Type)
extern void DefaultContractResolver_CreateLinqContract_m7DB1965B5AC9AAA038EDB9151B4001EF9BF2DD81 ();
// 0x000003F4 Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateISerializableContract(System.Type)
extern void DefaultContractResolver_CreateISerializableContract_m1F0E6292B3524B3CBD4FDF5CCA6960774A01B7B9 ();
// 0x000003F5 Newtonsoft.Json.Serialization.JsonDynamicContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDynamicContract(System.Type)
extern void DefaultContractResolver_CreateDynamicContract_mEBA8720682A7FD46383A45DCDC348D7C1B85125A ();
// 0x000003F6 Newtonsoft.Json.Serialization.JsonStringContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateStringContract(System.Type)
extern void DefaultContractResolver_CreateStringContract_mD69DB0A3074708A5A3FD351F47069ABCEC153D99 ();
// 0x000003F7 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateContract(System.Type)
extern void DefaultContractResolver_CreateContract_m5A635C8A137ED89C1A6AD9E6BD6683715BF3931D ();
// 0x000003F8 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsJsonPrimitiveType(System.Type)
extern void DefaultContractResolver_IsJsonPrimitiveType_m0AEB5082C4B302220550FD7D3D0766F10D39BF52 ();
// 0x000003F9 System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsIConvertible(System.Type)
extern void DefaultContractResolver_IsIConvertible_mBD698B298824FC7D4887C033E60AC48307BA72AE ();
// 0x000003FA System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::CanConvertToString(System.Type)
extern void DefaultContractResolver_CanConvertToString_m7B2F093B6D9FCCB30568DEA39BFA12488D84A9A5 ();
// 0x000003FB System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsValidCallback(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern void DefaultContractResolver_IsValidCallback_mBAFA737F05CCC59079BA8DA20E4159670B6FD43F ();
// 0x000003FC System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetClrTypeFullName(System.Type)
extern void DefaultContractResolver_GetClrTypeFullName_mFB5C7722F194142C89C5A9FCEFDBF5E7D948E27A ();
// 0x000003FD System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperties(System.Type,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperties_mED79EB6E286A2DC2BB87F8FF88D1FAD4747CFB46 ();
// 0x000003FE Newtonsoft.Json.DefaultJsonNameTable Newtonsoft.Json.Serialization.DefaultContractResolver::GetNameTable()
extern void DefaultContractResolver_GetNameTable_m7BC5A4ECE137DA1588829877559561E7DABF3F4B ();
// 0x000003FF Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.DefaultContractResolver::CreateMemberValueProvider(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateMemberValueProvider_m96E53340EED32A927490C456A16CD9880E6DD716 ();
// 0x00000400 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern void DefaultContractResolver_CreateProperty_mB99480D68319B57652D4993E3D278B5E98635661 ();
// 0x00000401 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetPropertySettingsFromAttributes(Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String,System.Type,Newtonsoft.Json.MemberSerialization,System.Boolean&)
extern void DefaultContractResolver_SetPropertySettingsFromAttributes_mCAF13F63A2E3E7A9B825BEE350E5327FC583BC04 ();
// 0x00000402 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateShouldSerializeTest(System.Reflection.MemberInfo)
extern void DefaultContractResolver_CreateShouldSerializeTest_mC2FB1258E395521D961A40A0DB65126B97B0D66D ();
// 0x00000403 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetIsSpecifiedActions(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.MemberInfo,System.Boolean)
extern void DefaultContractResolver_SetIsSpecifiedActions_m1A2A5A6DC4F537C3024820FC87C9E3042D168024 ();
// 0x00000404 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolvePropertyName(System.String)
extern void DefaultContractResolver_ResolvePropertyName_mFAD612C9823F2082FC306862847FEB5500E70C03 ();
// 0x00000405 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveExtensionDataName(System.String)
extern void DefaultContractResolver_ResolveExtensionDataName_m45C12721003E5C3F8F856EC871EBF3081EF30AE8 ();
// 0x00000406 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveDictionaryKey(System.String)
extern void DefaultContractResolver_ResolveDictionaryKey_mC939787DE1DAA19C2DE69EF4F5B8F097911F482E ();
// 0x00000407 System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetResolvedPropertyName(System.String)
extern void DefaultContractResolver_GetResolvedPropertyName_m99EFABF89F54490915E819AF9CB293D062788D56 ();
// 0x00000408 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.cctor()
extern void DefaultContractResolver__cctor_m6ADC69873C07383B8C325516C1EDB50FA8EF8BFB ();
// 0x00000409 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::.cctor()
extern void U3CU3Ec__cctor_m7DEDD9FCD2C7E0D14213A2C33FC8DF08DE23770C ();
// 0x0000040A System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::.ctor()
extern void U3CU3Ec__ctor_m02E469D3B5D1F808558BB2AD5B76DA1500FAFA22 ();
// 0x0000040B System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetSerializableMembers>b__40_0(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__40_0_m8C00AEDC96550CB8D360003E87AA08F31D2EC166 ();
// 0x0000040C System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetSerializableMembers>b__40_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetSerializableMembersU3Eb__40_1_m3350E78A8C8CFAB4A5A41BCC32E710CB7D930767 ();
// 0x0000040D System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetExtensionDataMemberForType>b__44_0(System.Type)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__44_0_mE88454F8CBFA88535D5BA907C8B7E2C8C90D0D44 ();
// 0x0000040E System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetExtensionDataMemberForType>b__44_1(System.Reflection.MemberInfo)
extern void U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__44_1_mC5417DF13600622548B432AEC1489A3F48D40144 ();
// 0x0000040F System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<GetAttributeConstructor>b__47_0(System.Reflection.ConstructorInfo)
extern void U3CU3Ec_U3CGetAttributeConstructorU3Eb__47_0_mB50A4D48AEDDE76E68BDD9CA98D8FC52A9DEDCAC ();
// 0x00000410 System.Int32 Newtonsoft.Json.Serialization.DefaultContractResolver_<>c::<CreateProperties>b__75_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreatePropertiesU3Eb__75_0_m5B2F1926F21A9BDDC4FAA3A26941C6AB93C590C1 ();
// 0x00000411 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m3CB0C0F0A700D978D835AC46CD703B03FE212F88 ();
// 0x00000412 System.String Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass42_0::<CreateObjectContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass42_0_U3CCreateObjectContractU3Eb__0_m22A928C10C1D20B8AAD017CEDB4E4A8698B02680 ();
// 0x00000413 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass45_0::.ctor()
extern void U3CU3Ec__DisplayClass45_0__ctor_mFED4764FFEC1CFCD9058D18F7AB2C21F3D45692D ();
// 0x00000414 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass45_1::.ctor()
extern void U3CU3Ec__DisplayClass45_1__ctor_mF64010AEF5C499A325EECF4FD01DC3854AC01969 ();
// 0x00000415 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass45_1::<SetExtensionDataDelegates>b__0(System.Object,System.String,System.Object)
extern void U3CU3Ec__DisplayClass45_1_U3CSetExtensionDataDelegatesU3Eb__0_m19B2E04FA40B44731659D1F9CE8E8025507B08C2 ();
// 0x00000416 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass45_2::.ctor()
extern void U3CU3Ec__DisplayClass45_2__ctor_mB04B1C51382BB8014E3532E5D4FE625D0F238902 ();
// 0x00000417 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass45_2::<SetExtensionDataDelegates>b__1(System.Object)
extern void U3CU3Ec__DisplayClass45_2_U3CSetExtensionDataDelegatesU3Eb__1_m2E825E67A2A84155B87600F4AB07A76AF0986E6B ();
// 0x00000418 System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m961A3BC222344FD0DD50AAB42E59EEA7A81636BB ();
// 0x00000419 System.String Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass62_0::<CreateDictionaryContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass62_0_U3CCreateDictionaryContractU3Eb__0_m3D6C0C2A966FF510D9C0A39E7133B53F9A366DC4 ();
// 0x0000041A System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_mADC323863980F480209A1F2C23534B385FB0998E ();
// 0x0000041B System.String Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass67_0::<CreateDynamicContract>b__0(System.String)
extern void U3CU3Ec__DisplayClass67_0_U3CCreateDynamicContractU3Eb__0_m8F230208FB7AC5FA0C7A4BA557FD6C41913F6170 ();
// 0x0000041C System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass80_0::.ctor()
extern void U3CU3Ec__DisplayClass80_0__ctor_mA03446B68D7B88BB8B0F511C66D9B0DE7DACBDCB ();
// 0x0000041D System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass80_0::<CreateShouldSerializeTest>b__0(System.Object)
extern void U3CU3Ec__DisplayClass80_0_U3CCreateShouldSerializeTestU3Eb__0_m434977ED3A2F858E76C76A6ECCE06AA5097292AB ();
// 0x0000041E System.Void Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass81_0::.ctor()
extern void U3CU3Ec__DisplayClass81_0__ctor_m1F6E5111D3EEA16612D40BBBB8166A1C33983DB2 ();
// 0x0000041F System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver_<>c__DisplayClass81_0::<SetIsSpecifiedActions>b__0(System.Object)
extern void U3CU3Ec__DisplayClass81_0_U3CSetIsSpecifiedActionsU3Eb__0_mD549EE3068EE64CE4D8DEE935585E0EA1651C403 ();
// 0x00000420 Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetMappings(System.Object)
extern void DefaultReferenceResolver_GetMappings_m6D3752FC05DDA88554AE83721CECE7974313569B ();
// 0x00000421 System.Object Newtonsoft.Json.Serialization.DefaultReferenceResolver::ResolveReference(System.Object,System.String)
extern void DefaultReferenceResolver_ResolveReference_m6FCAA26BE7AD255D5FE85DFEE3C5D263577D195F ();
// 0x00000422 System.String Newtonsoft.Json.Serialization.DefaultReferenceResolver::GetReference(System.Object,System.Object)
extern void DefaultReferenceResolver_GetReference_m6FF3B975D1CAD18BCFDC38E7FB632C0DBD01B38B ();
// 0x00000423 System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::AddReference(System.Object,System.String,System.Object)
extern void DefaultReferenceResolver_AddReference_m55C433E5E722B5A4A61FD9B14449A9B3A5FBC071 ();
// 0x00000424 System.Boolean Newtonsoft.Json.Serialization.DefaultReferenceResolver::IsReferenced(System.Object,System.Object)
extern void DefaultReferenceResolver_IsReferenced_mEC95757504746F0A9EB07DC2FC42A6DD19CBFA12 ();
// 0x00000425 System.Void Newtonsoft.Json.Serialization.DefaultReferenceResolver::.ctor()
extern void DefaultReferenceResolver__ctor_m4D96F31AA081E6D3A1D820526AED6FFBEFDB88AF ();
// 0x00000426 System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.ctor()
extern void DefaultSerializationBinder__ctor_m7B09BDB8485B92C513D89A6F14C3F83722C3143C ();
// 0x00000427 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeFromTypeNameKey(Newtonsoft.Json.Utilities.StructMultiKey`2<System.String,System.String>)
extern void DefaultSerializationBinder_GetTypeFromTypeNameKey_m24BBD4FB8D3F906870CA2D52802CC6067B9726C3 ();
// 0x00000428 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetGenericTypeFromTypeName(System.String,System.Reflection.Assembly)
extern void DefaultSerializationBinder_GetGenericTypeFromTypeName_m1219EE0283B3FCA78FFAD850A9C5FFE2B8AC70E0 ();
// 0x00000429 System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::GetTypeByName(Newtonsoft.Json.Utilities.StructMultiKey`2<System.String,System.String>)
extern void DefaultSerializationBinder_GetTypeByName_m73822B5075ABBB0DF24E087FFF3AC6C3C10875C4 ();
// 0x0000042A System.Type Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToType(System.String,System.String)
extern void DefaultSerializationBinder_BindToType_mD38C3E39741A2A3668AA775A88387B7BEE21F514 ();
// 0x0000042B System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::BindToName(System.Type,System.String&,System.String&)
extern void DefaultSerializationBinder_BindToName_mB15F8F0A861528066046AE15771A2989498F686B ();
// 0x0000042C System.Void Newtonsoft.Json.Serialization.DefaultSerializationBinder::.cctor()
extern void DefaultSerializationBinder__cctor_m2E728B81A695D72B1E949591C7FD7F00F621C5FF ();
// 0x0000042D System.Void Newtonsoft.Json.Serialization.ErrorContext::.ctor(System.Object,System.Object,System.String,System.Exception)
extern void ErrorContext__ctor_mE2D8DBE046E712B93A1461E2C4FFF06256365F60 ();
// 0x0000042E System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Traced()
extern void ErrorContext_get_Traced_m682D4F4552FBF85D07DE3336B4BBAE665FDE8337 ();
// 0x0000042F System.Void Newtonsoft.Json.Serialization.ErrorContext::set_Traced(System.Boolean)
extern void ErrorContext_set_Traced_mDBDA49897B21212E952C5F3DFCC5806CB25F2DB9 ();
// 0x00000430 System.Exception Newtonsoft.Json.Serialization.ErrorContext::get_Error()
extern void ErrorContext_get_Error_m2ACA5AF2B81548FCFD3811554A089D0D0926D1EB ();
// 0x00000431 System.Boolean Newtonsoft.Json.Serialization.ErrorContext::get_Handled()
extern void ErrorContext_get_Handled_m8F517FF8F9F5E8B8EF88E79BE6D73E247762CD47 ();
// 0x00000432 System.Void Newtonsoft.Json.Serialization.ErrorEventArgs::.ctor(System.Object,Newtonsoft.Json.Serialization.ErrorContext)
extern void ErrorEventArgs__ctor_m19D11B561E17BB068BC0AAFB0A4235339625494B ();
// 0x00000433 System.Void Newtonsoft.Json.Serialization.ExpressionValueProvider::.ctor(System.Reflection.MemberInfo)
extern void ExpressionValueProvider__ctor_mD982048A8C7DD002C0EAD56CAF6795000436680E ();
// 0x00000434 System.Void Newtonsoft.Json.Serialization.ExpressionValueProvider::SetValue(System.Object,System.Object)
extern void ExpressionValueProvider_SetValue_mC3B6F6A202024B5C6F4E98CF0B9E0CB2FD6DEED0 ();
// 0x00000435 System.Object Newtonsoft.Json.Serialization.ExpressionValueProvider::GetValue(System.Object)
extern void ExpressionValueProvider_GetValue_mB043C3CDB8B8ED124DDEE76B83A813CAC6BF28EA ();
// 0x00000436 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.IContractResolver::ResolveContract(System.Type)
// 0x00000437 System.Object Newtonsoft.Json.Serialization.IReferenceResolver::ResolveReference(System.Object,System.String)
// 0x00000438 System.String Newtonsoft.Json.Serialization.IReferenceResolver::GetReference(System.Object,System.Object)
// 0x00000439 System.Boolean Newtonsoft.Json.Serialization.IReferenceResolver::IsReferenced(System.Object,System.Object)
// 0x0000043A System.Void Newtonsoft.Json.Serialization.IReferenceResolver::AddReference(System.Object,System.String,System.Object)
// 0x0000043B System.Type Newtonsoft.Json.Serialization.ISerializationBinder::BindToType(System.String,System.String)
// 0x0000043C System.Void Newtonsoft.Json.Serialization.ISerializationBinder::BindToName(System.Type,System.String&,System.String&)
// 0x0000043D System.Diagnostics.TraceLevel Newtonsoft.Json.Serialization.ITraceWriter::get_LevelFilter()
// 0x0000043E System.Void Newtonsoft.Json.Serialization.ITraceWriter::Trace(System.Diagnostics.TraceLevel,System.String,System.Exception)
// 0x0000043F System.Void Newtonsoft.Json.Serialization.IValueProvider::SetValue(System.Object,System.Object)
// 0x00000440 System.Object Newtonsoft.Json.Serialization.IValueProvider::GetValue(System.Object)
// 0x00000441 System.Type Newtonsoft.Json.Serialization.JsonArrayContract::get_CollectionItemType()
extern void JsonArrayContract_get_CollectionItemType_mEAFE112AEAE56103CB92E06253C5C2CE705F6758 ();
// 0x00000442 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsMultidimensionalArray()
extern void JsonArrayContract_get_IsMultidimensionalArray_mE912AABFE131E546E0AC4B325709C082ECB144C2 ();
// 0x00000443 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_IsArray()
extern void JsonArrayContract_get_IsArray_m24137A4AA58764A1DBAEC396986D2D11B3ED2A4D ();
// 0x00000444 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_ShouldCreateWrapper()
extern void JsonArrayContract_get_ShouldCreateWrapper_mF4D684BF4B87EF2903BCD47E80E3A8C5E4C37522 ();
// 0x00000445 System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_CanDeserialize()
extern void JsonArrayContract_get_CanDeserialize_mF9ED7EF3A1D19223A465C5FCCD624D725E8CEE4C ();
// 0x00000446 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_CanDeserialize(System.Boolean)
extern void JsonArrayContract_set_CanDeserialize_m36D5A45CF5EA0715BD3A9800BD065F083EFD8693 ();
// 0x00000447 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_ParameterizedCreator()
extern void JsonArrayContract_get_ParameterizedCreator_mB9127B30338B5F350AF6E607FD860A8FC98AC919 ();
// 0x00000448 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::get_OverrideCreator()
extern void JsonArrayContract_get_OverrideCreator_mC25F0E9DCCEF7D2B0DADFCEA11527AC07707C2CE ();
// 0x00000449 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonArrayContract_set_OverrideCreator_m8A98E265A7E73413A61E9227540074D2D31BADFE ();
// 0x0000044A System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreator()
extern void JsonArrayContract_get_HasParameterizedCreator_mAF1B29B54276FAFC7107D47FB891A62A031F0B82 ();
// 0x0000044B System.Void Newtonsoft.Json.Serialization.JsonArrayContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonArrayContract_set_HasParameterizedCreator_m53E4A947C9306EABC0C505F836FE44A753076D53 ();
// 0x0000044C System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::get_HasParameterizedCreatorInternal()
extern void JsonArrayContract_get_HasParameterizedCreatorInternal_m97B73D1E2740B034BF3169DB82DDC82D3CBBA2DB ();
// 0x0000044D System.Void Newtonsoft.Json.Serialization.JsonArrayContract::.ctor(System.Type)
extern void JsonArrayContract__ctor_mF6D3D64944FE7112CACF3E9830E2336A34D4F8E1 ();
// 0x0000044E Newtonsoft.Json.Utilities.IWrappedCollection Newtonsoft.Json.Serialization.JsonArrayContract::CreateWrapper(System.Object)
extern void JsonArrayContract_CreateWrapper_m98284EF26E40CB5DF0CA9DA68579A350BF7E6F49 ();
// 0x0000044F System.Collections.IList Newtonsoft.Json.Serialization.JsonArrayContract::CreateTemporaryCollection()
extern void JsonArrayContract_CreateTemporaryCollection_mBEB54BFE6AF247507A980A01CAD4B2FE32D4C506 ();
// 0x00000450 System.Void Newtonsoft.Json.Serialization.JsonArrayContract::StoreFSharpListCreatorIfNecessary(System.Type)
extern void JsonArrayContract_StoreFSharpListCreatorIfNecessary_mC7377ADE8DF2ABCC0FE01A355A2E53D5F9A8BB57 ();
// 0x00000451 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemContract()
extern void JsonContainerContract_get_ItemContract_m6F77C60E3CA205AB20B11B8C5883E10C8CF44D45 ();
// 0x00000452 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonContainerContract_set_ItemContract_m575EE3C0BD44A6B2B7B09CB53D93D3E9C87F6E57 ();
// 0x00000453 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::get_FinalItemContract()
extern void JsonContainerContract_get_FinalItemContract_m8E7F3DE58928CE86634FA11C12608B1AF41E615E ();
// 0x00000454 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemConverter()
extern void JsonContainerContract_get_ItemConverter_m65BD3A1A619BB764E67C28340CF982742000A87C ();
// 0x00000455 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContainerContract_set_ItemConverter_m5275B3AF49CE6BFB487A93C3EFC217D145EE9F0C ();
// 0x00000456 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemIsReference()
extern void JsonContainerContract_get_ItemIsReference_m0CD76DDEE8A706AEE13AB4C3070239B67D4306D3 ();
// 0x00000457 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonContainerContract_set_ItemIsReference_m2DF2B6D8775CB547609987D0EEEDA4D8FFFA4445 ();
// 0x00000458 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemReferenceLoopHandling()
extern void JsonContainerContract_get_ItemReferenceLoopHandling_mBBE2464C486B2EAC9249040AF56C63EADF86F331 ();
// 0x00000459 System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonContainerContract_set_ItemReferenceLoopHandling_mE763F64A84746F6981DFE84A314FA874E6030FAE ();
// 0x0000045A System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::get_ItemTypeNameHandling()
extern void JsonContainerContract_get_ItemTypeNameHandling_m3066802530448D89BE24AB7672140194EAE96160 ();
// 0x0000045B System.Void Newtonsoft.Json.Serialization.JsonContainerContract::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonContainerContract_set_ItemTypeNameHandling_mA708CEE01B307BD5D9D665A6D756A33588FEC4E8 ();
// 0x0000045C System.Void Newtonsoft.Json.Serialization.JsonContainerContract::.ctor(System.Type)
extern void JsonContainerContract__ctor_mCF9664476A7B4CDDA8D24832D07D104F5AE36DE8 ();
// 0x0000045D System.Void Newtonsoft.Json.Serialization.SerializationCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationCallback__ctor_m034A339E0CFACB331C28002C5E1F75E11AF8F04A ();
// 0x0000045E System.Void Newtonsoft.Json.Serialization.SerializationCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext)
extern void SerializationCallback_Invoke_mF4530F2447A1391F14F00AE8F4BF8723B950E081 ();
// 0x0000045F System.IAsyncResult Newtonsoft.Json.Serialization.SerializationCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,System.AsyncCallback,System.Object)
extern void SerializationCallback_BeginInvoke_mB0EC258E7BE8A25960CE0744395A5012A3C4E6F0 ();
// 0x00000460 System.Void Newtonsoft.Json.Serialization.SerializationCallback::EndInvoke(System.IAsyncResult)
extern void SerializationCallback_EndInvoke_m209AA3DBB77201449CBA5FCC36E244B26D495DDA ();
// 0x00000461 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::.ctor(System.Object,System.IntPtr)
extern void SerializationErrorCallback__ctor_m970746DB3663107008676F9B7C860ADFF5C6EF25 ();
// 0x00000462 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::Invoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void SerializationErrorCallback_Invoke_m2241C9CBE86AF12E7F0DC0678B77541B323CAD9F ();
// 0x00000463 System.IAsyncResult Newtonsoft.Json.Serialization.SerializationErrorCallback::BeginInvoke(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext,System.AsyncCallback,System.Object)
extern void SerializationErrorCallback_BeginInvoke_mADD81828292595908BEEC78918CB7EED1404FD87 ();
// 0x00000464 System.Void Newtonsoft.Json.Serialization.SerializationErrorCallback::EndInvoke(System.IAsyncResult)
extern void SerializationErrorCallback_EndInvoke_m9873DDA24FF394525888948F492BD49E5C31DF20 ();
// 0x00000465 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataSetter__ctor_m7976E945D2C0C193B00D8CF8EE747FB25EF252A4 ();
// 0x00000466 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::Invoke(System.Object,System.String,System.Object)
extern void ExtensionDataSetter_Invoke_mA4C6C271DB78FB5F150FBDC5F5827FAD2F1F3FB0 ();
// 0x00000467 System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataSetter::BeginInvoke(System.Object,System.String,System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataSetter_BeginInvoke_m63E2139E4439FC5DCB58D94FF3F713124F507350 ();
// 0x00000468 System.Void Newtonsoft.Json.Serialization.ExtensionDataSetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataSetter_EndInvoke_mAC33DC5FF694FC86CA521B9D50C6C47C9027D184 ();
// 0x00000469 System.Void Newtonsoft.Json.Serialization.ExtensionDataGetter::.ctor(System.Object,System.IntPtr)
extern void ExtensionDataGetter__ctor_m102446EE01B71BE0D9737453284FA95C1D3E160F ();
// 0x0000046A System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::Invoke(System.Object)
extern void ExtensionDataGetter_Invoke_m91E608874E43EA88B04B61D77758BEFA3813014A ();
// 0x0000046B System.IAsyncResult Newtonsoft.Json.Serialization.ExtensionDataGetter::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern void ExtensionDataGetter_BeginInvoke_m0ACFADAD0B6F9C718391C213DDAA294FF41125CD ();
// 0x0000046C System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>> Newtonsoft.Json.Serialization.ExtensionDataGetter::EndInvoke(System.IAsyncResult)
extern void ExtensionDataGetter_EndInvoke_mA7637A1583892B4177D6080932F8F0E79A5AD3D2 ();
// 0x0000046D System.Type Newtonsoft.Json.Serialization.JsonContract::get_UnderlyingType()
extern void JsonContract_get_UnderlyingType_mE0084E5B73A9526ABAC59E4EF1E105251484C88A ();
// 0x0000046E System.Type Newtonsoft.Json.Serialization.JsonContract::get_CreatedType()
extern void JsonContract_get_CreatedType_m442AF165A70728A21B3AD09BD6478A425D8AE502 ();
// 0x0000046F System.Void Newtonsoft.Json.Serialization.JsonContract::set_CreatedType(System.Type)
extern void JsonContract_set_CreatedType_m98965FF7A972A2039FF951189730D63F01382B90 ();
// 0x00000470 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::get_IsReference()
extern void JsonContract_get_IsReference_m11C5F106339EB17EB2E245E7D8789C84CBE9D5D2 ();
// 0x00000471 System.Void Newtonsoft.Json.Serialization.JsonContract::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonContract_set_IsReference_m6DDC8BA4E61B6D26629BAEA75AB630C96A08E249 ();
// 0x00000472 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_Converter()
extern void JsonContract_get_Converter_mFEAD9B249CF7BE7FC31A02F55E6A9CDB2CF877E1 ();
// 0x00000473 System.Void Newtonsoft.Json.Serialization.JsonContract::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_Converter_m1C84ACD36B89C7943358D26519FB00E82847E47E ();
// 0x00000474 Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::get_InternalConverter()
extern void JsonContract_get_InternalConverter_m57F161FE7233C862A8CD74722CFE211940A062A7 ();
// 0x00000475 System.Void Newtonsoft.Json.Serialization.JsonContract::set_InternalConverter(Newtonsoft.Json.JsonConverter)
extern void JsonContract_set_InternalConverter_m290298D2C6F6DC9AA26D26B1700614C5A322958A ();
// 0x00000476 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializedCallbacks()
extern void JsonContract_get_OnDeserializedCallbacks_m19EFB524A1F05C133F64D3367800C1FD6D0E651A ();
// 0x00000477 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnDeserializingCallbacks()
extern void JsonContract_get_OnDeserializingCallbacks_m73E7E2BF768EF40630A212378C92FAB0649DE2EE ();
// 0x00000478 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializedCallbacks()
extern void JsonContract_get_OnSerializedCallbacks_m2185A0545CEFFB953819A5325F15B502DC07AF46 ();
// 0x00000479 System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnSerializingCallbacks()
extern void JsonContract_get_OnSerializingCallbacks_mA08247B7610507E69986A10C6BA22CBC9633C811 ();
// 0x0000047A System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::get_OnErrorCallbacks()
extern void JsonContract_get_OnErrorCallbacks_m25AC02640934B2F555DD7023A648940B43315A99 ();
// 0x0000047B System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreator()
extern void JsonContract_get_DefaultCreator_mB7579F859D4492CC933B5F5622D3C003B2BDA1AA ();
// 0x0000047C System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreator(System.Func`1<System.Object>)
extern void JsonContract_set_DefaultCreator_m75B74EBBD288130AC93A26A446DA97B76E3CB812 ();
// 0x0000047D System.Boolean Newtonsoft.Json.Serialization.JsonContract::get_DefaultCreatorNonPublic()
extern void JsonContract_get_DefaultCreatorNonPublic_mF37271A60F600E6D71B9B130BC9C88F1F9053D69 ();
// 0x0000047E System.Void Newtonsoft.Json.Serialization.JsonContract::set_DefaultCreatorNonPublic(System.Boolean)
extern void JsonContract_set_DefaultCreatorNonPublic_mB2080A71D3002F22F425D33688BD54D84E044003 ();
// 0x0000047F System.Void Newtonsoft.Json.Serialization.JsonContract::.ctor(System.Type)
extern void JsonContract__ctor_mBB28B39C7A2001CDB4EFE81F0C0E31CF6628C90B ();
// 0x00000480 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerializing_m5DF16DA6841C7114894C2424CA674B3529D3F511 ();
// 0x00000481 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnSerialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnSerialized_mCBE1D586F783169AB1A11653FD740BCAC2F68CAF ();
// 0x00000482 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserializing(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserializing_mAB3C96F578936312DB1B92705DD690E142AE3313 ();
// 0x00000483 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnDeserialized(System.Object,System.Runtime.Serialization.StreamingContext)
extern void JsonContract_InvokeOnDeserialized_mFF44B5AECCD00F539D4DE46AB77D8F2F9286C775 ();
// 0x00000484 System.Void Newtonsoft.Json.Serialization.JsonContract::InvokeOnError(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void JsonContract_InvokeOnError_m5E55591506C09AE37333EDE63E4908D3560E9034 ();
// 0x00000485 Newtonsoft.Json.Serialization.SerializationCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationCallback_m1D1C04D219134B7E1971D61178F13C6C3E6388C1 ();
// 0x00000486 Newtonsoft.Json.Serialization.SerializationErrorCallback Newtonsoft.Json.Serialization.JsonContract::CreateSerializationErrorCallback(System.Reflection.MethodInfo)
extern void JsonContract_CreateSerializationErrorCallback_m8BE3098020ACA1A98B57974F640C33A38EC1CC35 ();
// 0x00000487 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_mA6AC153231123C37D38F11ED56A6025BEEF490F8 ();
// 0x00000488 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass57_0::<CreateSerializationCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext)
extern void U3CU3Ec__DisplayClass57_0_U3CCreateSerializationCallbackU3Eb__0_m5EF8D51FAA219EA795071B55B5CFBBBFEA985FCB ();
// 0x00000489 System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0::.ctor()
extern void U3CU3Ec__DisplayClass58_0__ctor_m468293CFBB821B032C41538226AC5EFD01887A89 ();
// 0x0000048A System.Void Newtonsoft.Json.Serialization.JsonContract_<>c__DisplayClass58_0::<CreateSerializationErrorCallback>b__0(System.Object,System.Runtime.Serialization.StreamingContext,Newtonsoft.Json.Serialization.ErrorContext)
extern void U3CU3Ec__DisplayClass58_0_U3CCreateSerializationErrorCallbackU3Eb__0_m5B00E6278CC33833DF5325875FD8172117ADF85C ();
// 0x0000048B System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyResolver()
extern void JsonDictionaryContract_get_DictionaryKeyResolver_m694F96281481FC1A07DD22B75F3CC0F2409F8AD8 ();
// 0x0000048C System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_DictionaryKeyResolver(System.Func`2<System.String,System.String>)
extern void JsonDictionaryContract_set_DictionaryKeyResolver_m7730DDF4BF17728862C25FC540F1BD7F52183B41 ();
// 0x0000048D System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryKeyType()
extern void JsonDictionaryContract_get_DictionaryKeyType_mDD654476C058C529DE631E77513ADB23C5F5C22D ();
// 0x0000048E System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::get_DictionaryValueType()
extern void JsonDictionaryContract_get_DictionaryValueType_m8BB2006AAE979645FD1E679BFF3D50BC60E159CC ();
// 0x0000048F Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::get_KeyContract()
extern void JsonDictionaryContract_get_KeyContract_m4BE3BE029727F9591B58C341749E2BAFA5EC91D3 ();
// 0x00000490 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_KeyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonDictionaryContract_set_KeyContract_m87B65AD616AD5BAE49DD786CE996BFDAA07E25AD ();
// 0x00000491 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ShouldCreateWrapper()
extern void JsonDictionaryContract_get_ShouldCreateWrapper_m0221A20F9AFEB94681E1132D1DF939ADCD0B0430 ();
// 0x00000492 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_ParameterizedCreator()
extern void JsonDictionaryContract_get_ParameterizedCreator_m4437DE8F37E2317090A58F06BD6B49722BF5A688 ();
// 0x00000493 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::get_OverrideCreator()
extern void JsonDictionaryContract_get_OverrideCreator_m7C77ECB0C98EA6E06061391D01532304ADD6E645 ();
// 0x00000494 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonDictionaryContract_set_OverrideCreator_mD75F873EE4F0278A4A60FF679C70DE539546F146 ();
// 0x00000495 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreator()
extern void JsonDictionaryContract_get_HasParameterizedCreator_m2E6DDAEAC2A39AA2DD9B6410A01A3986978C8B0C ();
// 0x00000496 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::set_HasParameterizedCreator(System.Boolean)
extern void JsonDictionaryContract_set_HasParameterizedCreator_mB5449625C488CE09A1D8F838B985696CEA4B2108 ();
// 0x00000497 System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::get_HasParameterizedCreatorInternal()
extern void JsonDictionaryContract_get_HasParameterizedCreatorInternal_mEB1A5240D5E7B2EC3A0306B63B4A23B031CE7190 ();
// 0x00000498 System.Void Newtonsoft.Json.Serialization.JsonDictionaryContract::.ctor(System.Type)
extern void JsonDictionaryContract__ctor_m3FA1679AFF7E73CB7A5824297DA17FA8C55D37FB ();
// 0x00000499 Newtonsoft.Json.Utilities.IWrappedDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateWrapper(System.Object)
extern void JsonDictionaryContract_CreateWrapper_m4E8E5F47E583FD314A2E93D68340933734DE4B69 ();
// 0x0000049A System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonDictionaryContract::CreateTemporaryDictionary()
extern void JsonDictionaryContract_CreateTemporaryDictionary_mB6FFBD7923CE5DA789D7DE4597CC2BFCB28A315B ();
// 0x0000049B Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonDynamicContract::get_Properties()
extern void JsonDynamicContract_get_Properties_mD840527AC93C463C3C53978CE8D0FB5724764A94 ();
// 0x0000049C System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDynamicContract::get_PropertyNameResolver()
extern void JsonDynamicContract_get_PropertyNameResolver_m55A060B6A7B13C610EDE9B983207FD388A848ADB ();
// 0x0000049D System.Void Newtonsoft.Json.Serialization.JsonDynamicContract::set_PropertyNameResolver(System.Func`2<System.String,System.String>)
extern void JsonDynamicContract_set_PropertyNameResolver_m79A00D8C0C2C162452428A44F255225A6C123450 ();
// 0x0000049E System.Runtime.CompilerServices.CallSite`1<System.Func`3<System.Runtime.CompilerServices.CallSite,System.Object,System.Object>> Newtonsoft.Json.Serialization.JsonDynamicContract::CreateCallSiteGetter(System.String)
extern void JsonDynamicContract_CreateCallSiteGetter_m9308DB4AA72A61554337A84D9BE23C5973976DD5 ();
// 0x0000049F System.Runtime.CompilerServices.CallSite`1<System.Func`4<System.Runtime.CompilerServices.CallSite,System.Object,System.Object,System.Object>> Newtonsoft.Json.Serialization.JsonDynamicContract::CreateCallSiteSetter(System.String)
extern void JsonDynamicContract_CreateCallSiteSetter_m2E4F4A7DA48FF2F841C7DD77F64165AC9A08B1BE ();
// 0x000004A0 System.Void Newtonsoft.Json.Serialization.JsonDynamicContract::.ctor(System.Type)
extern void JsonDynamicContract__ctor_m856C0589DD9DD0C61A000421242FE7443CFB72C8 ();
// 0x000004A1 System.Boolean Newtonsoft.Json.Serialization.JsonDynamicContract::TryGetMember(System.Dynamic.IDynamicMetaObjectProvider,System.String,System.Object&)
extern void JsonDynamicContract_TryGetMember_m0176BA2038F5A142ED428D3E19D7981767B42C0B ();
// 0x000004A2 System.Boolean Newtonsoft.Json.Serialization.JsonDynamicContract::TrySetMember(System.Dynamic.IDynamicMetaObjectProvider,System.String,System.Object)
extern void JsonDynamicContract_TrySetMember_m025FC0AD054CECBCB64316C9D0A36D972DA2644A ();
// 0x000004A3 System.Void Newtonsoft.Json.Serialization.JsonFormatterConverter::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonFormatterConverter__ctor_mD09B510D858E55E25445F526A67883A305D6C20A ();
// 0x000004A4 T Newtonsoft.Json.Serialization.JsonFormatterConverter::GetTokenValue(System.Object)
// 0x000004A5 System.Object Newtonsoft.Json.Serialization.JsonFormatterConverter::Convert(System.Object,System.Type)
extern void JsonFormatterConverter_Convert_m3C31BFAD90DA212608D78A5E290715BA70F3A31C ();
// 0x000004A6 System.Boolean Newtonsoft.Json.Serialization.JsonFormatterConverter::ToBoolean(System.Object)
extern void JsonFormatterConverter_ToBoolean_m31C9665F91E643F5F6A7322908C0CB79FC9B2CCD ();
// 0x000004A7 System.Int32 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt32(System.Object)
extern void JsonFormatterConverter_ToInt32_m08BD69A88EB2D8B5B5C00413C96E657BE3705903 ();
// 0x000004A8 System.Int64 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt64(System.Object)
extern void JsonFormatterConverter_ToInt64_m4A2804D7E5FBF0A92FEB3E5A347A5DBC928B0F06 ();
// 0x000004A9 System.Single Newtonsoft.Json.Serialization.JsonFormatterConverter::ToSingle(System.Object)
extern void JsonFormatterConverter_ToSingle_mACB682B302D2D99FDECDDE0B631207DFB76BB602 ();
// 0x000004AA System.String Newtonsoft.Json.Serialization.JsonFormatterConverter::ToString(System.Object)
extern void JsonFormatterConverter_ToString_m67BFE5AA97BCE700D1DE3C7FA36801D7E21C92C3 ();
// 0x000004AB Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::get_ISerializableCreator()
extern void JsonISerializableContract_get_ISerializableCreator_mA8116BF441C7D2F3D59F4BC213DAD397D28BC7B9 ();
// 0x000004AC System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::set_ISerializableCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonISerializableContract_set_ISerializableCreator_mB4EC114FDB78FB8F6A6A508E42D634C936A8FC75 ();
// 0x000004AD System.Void Newtonsoft.Json.Serialization.JsonISerializableContract::.ctor(System.Type)
extern void JsonISerializableContract__ctor_m3A8193BCE1B2BF2AD003DE22086F51F640E4FD16 ();
// 0x000004AE System.Void Newtonsoft.Json.Serialization.JsonLinqContract::.ctor(System.Type)
extern void JsonLinqContract__ctor_m290A5E599B83FA984E6F36C13EBA1666E312DD1D ();
// 0x000004AF Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::get_MemberSerialization()
extern void JsonObjectContract_get_MemberSerialization_m1D215E92E7385199BEEA6BF22BDAF70DFB5E4360 ();
// 0x000004B0 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern void JsonObjectContract_set_MemberSerialization_m7215647FDBABE568FB08A8C35D38653C016DB9D1 ();
// 0x000004B1 System.Nullable`1<Newtonsoft.Json.MissingMemberHandling> Newtonsoft.Json.Serialization.JsonObjectContract::get_MissingMemberHandling()
extern void JsonObjectContract_get_MissingMemberHandling_m065105E5B82D2618A399B37307108E3EF80B543A ();
// 0x000004B2 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_MissingMemberHandling(System.Nullable`1<Newtonsoft.Json.MissingMemberHandling>)
extern void JsonObjectContract_set_MissingMemberHandling_m764058C5197A263266C7ABA4F6D35364FD24E03F ();
// 0x000004B3 System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemRequired()
extern void JsonObjectContract_get_ItemRequired_mE98D1B218CF9227FCA14A138E90044DEB9B74936 ();
// 0x000004B4 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemRequired(System.Nullable`1<Newtonsoft.Json.Required>)
extern void JsonObjectContract_set_ItemRequired_m84FBCEBD2B84A5B1B18050A29A90A72384EF93A9 ();
// 0x000004B5 System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonObjectContract::get_ItemNullValueHandling()
extern void JsonObjectContract_get_ItemNullValueHandling_mF5A4C7D1B229F2478DFF3307AD8A33459DFBD05A ();
// 0x000004B6 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ItemNullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern void JsonObjectContract_set_ItemNullValueHandling_mA077DF7CC913461524E454FE362472B3020EBF7C ();
// 0x000004B7 Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_Properties()
extern void JsonObjectContract_get_Properties_m1E3AD6BD850BCA3FD49032377CCFBC2876026AA7 ();
// 0x000004B8 Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::get_CreatorParameters()
extern void JsonObjectContract_get_CreatorParameters_m103A4D96616ADA884DC916D60C3EB55E777ECF18 ();
// 0x000004B9 Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_OverrideCreator()
extern void JsonObjectContract_get_OverrideCreator_m120DBCAD83B6DA281A437582E7AB78D6847E1432 ();
// 0x000004BA System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_OverrideCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonObjectContract_set_OverrideCreator_mD6145D3DDA2DC1CF56E2BF18A63F2C6FCE5CC514 ();
// 0x000004BB Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::get_ParameterizedCreator()
extern void JsonObjectContract_get_ParameterizedCreator_mC7557E0587CD28329688516C8BC5E1C52A5BE1E6 ();
// 0x000004BC System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ParameterizedCreator(Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>)
extern void JsonObjectContract_set_ParameterizedCreator_m3E5D339F9FA060B6983F003528A130353F816109 ();
// 0x000004BD Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataSetter()
extern void JsonObjectContract_get_ExtensionDataSetter_m57E41B46FFEBEB00902ABFCC921276520A10DDE1 ();
// 0x000004BE System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataSetter(Newtonsoft.Json.Serialization.ExtensionDataSetter)
extern void JsonObjectContract_set_ExtensionDataSetter_mE962EC0B83961774CD386559DBD5BE7A06055F37 ();
// 0x000004BF Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataGetter()
extern void JsonObjectContract_get_ExtensionDataGetter_m19E3FFC4BBDF5DCF9C3EA2E41567023BFA83D21F ();
// 0x000004C0 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataGetter(Newtonsoft.Json.Serialization.ExtensionDataGetter)
extern void JsonObjectContract_set_ExtensionDataGetter_m5B03F8C525C2808AAD5600A1DFCCD39CE88D7E85 ();
// 0x000004C1 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataValueType(System.Type)
extern void JsonObjectContract_set_ExtensionDataValueType_m909D2DBEBC8DB862DF88C4BB08DEC6AE64C27D93 ();
// 0x000004C2 System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonObjectContract::get_ExtensionDataNameResolver()
extern void JsonObjectContract_get_ExtensionDataNameResolver_m11FC4F958AFEC204E57287C7B7E0F57354A8C834 ();
// 0x000004C3 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::set_ExtensionDataNameResolver(System.Func`2<System.String,System.String>)
extern void JsonObjectContract_set_ExtensionDataNameResolver_mA8FDF2E3B17BE6BE93C34ED3CD3A5E515419A78B ();
// 0x000004C4 System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::get_HasRequiredOrDefaultValueProperties()
extern void JsonObjectContract_get_HasRequiredOrDefaultValueProperties_m505B2BCA9D9BCCCB61B5949E74D13127568C761C ();
// 0x000004C5 System.Void Newtonsoft.Json.Serialization.JsonObjectContract::.ctor(System.Type)
extern void JsonObjectContract__ctor_m3747070511B934D9AF1C9F5559E4BAEF491D2461 ();
// 0x000004C6 System.Object Newtonsoft.Json.Serialization.JsonObjectContract::GetUninitializedObject()
extern void JsonObjectContract_GetUninitializedObject_m640B1C14884E35C920F871DDB3E2C9E4305DFD52 ();
// 0x000004C7 Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::get_TypeCode()
extern void JsonPrimitiveContract_get_TypeCode_m04B173C5426475AF94126ADEDFD230E317F930EE ();
// 0x000004C8 System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::set_TypeCode(Newtonsoft.Json.Utilities.PrimitiveTypeCode)
extern void JsonPrimitiveContract_set_TypeCode_mD3EE2E2BDC2EF286A72DEA9FCCDC23200C245C85 ();
// 0x000004C9 System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.ctor(System.Type)
extern void JsonPrimitiveContract__ctor_mFF896C487ECD98D4102904D3F6A51B1F7F522E86 ();
// 0x000004CA System.Void Newtonsoft.Json.Serialization.JsonPrimitiveContract::.cctor()
extern void JsonPrimitiveContract__cctor_m1E287FB2EB109FCA1D6192854C9F89243C3254E9 ();
// 0x000004CB Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::get_PropertyContract()
extern void JsonProperty_get_PropertyContract_m0B88E8230316FBC8A5D2074FBF1D4DADA2F8E7E7 ();
// 0x000004CC System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyContract(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonProperty_set_PropertyContract_m2B2D17492F6B92DA4316BB0B4D02457EAD25D26A ();
// 0x000004CD System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern void JsonProperty_get_PropertyName_mC4B6B67B094AAD67D122971133C599F6E891C32B ();
// 0x000004CE System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern void JsonProperty_set_PropertyName_m3B8680BBC5F40C0CC5CBB6172CF7F460D886C033 ();
// 0x000004CF System.Type Newtonsoft.Json.Serialization.JsonProperty::get_DeclaringType()
extern void JsonProperty_get_DeclaringType_mAC6337F71FFBBB71C77944D34D55E871ADF98AA4 ();
// 0x000004D0 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DeclaringType(System.Type)
extern void JsonProperty_set_DeclaringType_m212950DFF2F651F9240FD5843A328C30D298F336 ();
// 0x000004D1 System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern void JsonProperty_get_Order_mB082C04FEA20F7B444D5DF7F1758D4CE9C98AC38 ();
// 0x000004D2 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern void JsonProperty_set_Order_mBB0ECB9C5633188BE63CFC7CCB578F6BF5543327 ();
// 0x000004D3 System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern void JsonProperty_get_UnderlyingName_mB4EA21466053D86D317B7F45EDCBC44056539D55 ();
// 0x000004D4 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern void JsonProperty_set_UnderlyingName_mE6333333CF67219BC8ACF280079CADF688509A2C ();
// 0x000004D5 Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern void JsonProperty_get_ValueProvider_mD8BC092B52E89F58C3228A9BE9527A95E73AF52C ();
// 0x000004D6 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern void JsonProperty_set_ValueProvider_m211EF301287FBABDDE3272271FC8EC0B0A10254D ();
// 0x000004D7 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_AttributeProvider(Newtonsoft.Json.Serialization.IAttributeProvider)
extern void JsonProperty_set_AttributeProvider_mD25FBBBD604F49319E4AB1949169F3AEA3DA5E6C ();
// 0x000004D8 System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern void JsonProperty_get_PropertyType_m6B3AE9E3A110A5800E84300F7CBD9ADF13B275D1 ();
// 0x000004D9 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern void JsonProperty_set_PropertyType_mD915B2D96EFD441149EFC7D6D67F961C1A4D4DCA ();
// 0x000004DA Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern void JsonProperty_get_Converter_mB354BB5CF3FF375D46F11D13C9F91FE517569178 ();
// 0x000004DB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_Converter_mC0879D94A0B2C303C6EEAFBE1C877E3E321CAD68 ();
// 0x000004DC System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern void JsonProperty_get_Ignored_mCD40902CE424EEE05A9C98A67FE9F131AE6F0F18 ();
// 0x000004DD System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern void JsonProperty_set_Ignored_m2D1671501F476D2F4D89B5FDA653A8F98361367B ();
// 0x000004DE System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern void JsonProperty_get_Readable_m5D6996D3F73BAF6C6DC3BFA7C9554983D632EA88 ();
// 0x000004DF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern void JsonProperty_set_Readable_m805FD592FD9A49944FAC183C37D1D686791208A4 ();
// 0x000004E0 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern void JsonProperty_get_Writable_m603494F6C7D90E2DF69612F660EB77DFC00817F0 ();
// 0x000004E1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern void JsonProperty_set_Writable_m9668B31D602D93E3A2EF5E86DB831D3B20D935FC ();
// 0x000004E2 System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_HasMemberAttribute()
extern void JsonProperty_get_HasMemberAttribute_m8410E984D65A4A0D8CC6F425941FCEEAE33BCC4B ();
// 0x000004E3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_HasMemberAttribute(System.Boolean)
extern void JsonProperty_set_HasMemberAttribute_mFE0B334F0F966CD90DB5366FB16296EEBEDA1D6C ();
// 0x000004E4 System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern void JsonProperty_get_DefaultValue_m9F7C4702DD2C81CAE956B9FE06A21F2BF6D73AC3 ();
// 0x000004E5 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern void JsonProperty_set_DefaultValue_m5687BFE228338FF5A7A5FAFEFE99FFB88D54BA21 ();
// 0x000004E6 System.Object Newtonsoft.Json.Serialization.JsonProperty::GetResolvedDefaultValue()
extern void JsonProperty_GetResolvedDefaultValue_m0F79FBC4EE28042935C27E84CA7B6A27EA70F020 ();
// 0x000004E7 Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern void JsonProperty_get_Required_m560C3B1D45DE3778B6409DEF15A68EB34DE051A6 ();
// 0x000004E8 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern void JsonProperty_get_IsReference_m6D4D3E09456EAE5DACE7A8BBC6D3E7326EFB5FD1 ();
// 0x000004E9 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_IsReference_mDBA7EC3CF7F3B5677F788081ABC039D3FAC1D1C7 ();
// 0x000004EA System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern void JsonProperty_get_NullValueHandling_m20C37276D59A8FFE4FA83234BB72AA2B2BB111AF ();
// 0x000004EB System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern void JsonProperty_set_NullValueHandling_m2CB4BA8F08E32FD21E0C967C597F478B36EEDCB0 ();
// 0x000004EC System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern void JsonProperty_get_DefaultValueHandling_m8FDCF1FA86330D7398C6AA93BDCCA81362362441 ();
// 0x000004ED System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern void JsonProperty_set_DefaultValueHandling_mEBF1DDAD284ABB620EECF47B67A007B15075F844 ();
// 0x000004EE System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern void JsonProperty_get_ReferenceLoopHandling_mDAC3521C25B91CD78EF3A3F8A08575CB557AC210 ();
// 0x000004EF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ReferenceLoopHandling_m8C509109EBFCF4258424B3B95A7CBD66C5D35775 ();
// 0x000004F0 System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern void JsonProperty_get_ObjectCreationHandling_m69CBFA45BDB11AC5B37F2BC21FBEF3DCF2627035 ();
// 0x000004F1 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern void JsonProperty_set_ObjectCreationHandling_m9F6060963F582F6B5E2AB1FF9ECD554B9B5CCA44 ();
// 0x000004F2 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern void JsonProperty_get_TypeNameHandling_m6708C116A993419FF69BFA6D5ADDB4C3B42DAAC1 ();
// 0x000004F3 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_TypeNameHandling_m9E28CAE6750DF03C9A8BE28AED8DD355BCA34987 ();
// 0x000004F4 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern void JsonProperty_get_ShouldSerialize_m7286940398E365E6FE05BCC66A17DA17674D2ECF ();
// 0x000004F5 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern void JsonProperty_set_ShouldSerialize_mD9E5DDDB79E6F6926558E93C29BCEBA37D1703A7 ();
// 0x000004F6 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldDeserialize()
extern void JsonProperty_get_ShouldDeserialize_mE12E10AD5F0ABA90A22BFD7F84F2508FF04A582D ();
// 0x000004F7 System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern void JsonProperty_get_GetIsSpecified_mBE29FC0A2D499F9E0EDF24EECF5CDE7CAF9369A8 ();
// 0x000004F8 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern void JsonProperty_set_GetIsSpecified_m36131877DE9B1A9AE8C93745FA0339F6560EFEDA ();
// 0x000004F9 System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern void JsonProperty_get_SetIsSpecified_mD5F8A991E4B2B64BB92BBC40D95B99DE8B09DF91 ();
// 0x000004FA System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(System.Action`2<System.Object,System.Object>)
extern void JsonProperty_set_SetIsSpecified_mF91E426C2D42632DC25AED3D51AA0F788758D616 ();
// 0x000004FB System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern void JsonProperty_ToString_mDEA1DAD3B4B80CF614B2D2749E973C734943655F ();
// 0x000004FC Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_ItemConverter()
extern void JsonProperty_get_ItemConverter_mA22671473B62F6DD761DC7253806D61C149E0457 ();
// 0x000004FD System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemConverter(Newtonsoft.Json.JsonConverter)
extern void JsonProperty_set_ItemConverter_m0C448E942E4991A66E0CABACD251943CF8A1CD2A ();
// 0x000004FE System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_ItemIsReference()
extern void JsonProperty_get_ItemIsReference_m89DD6D113A51AFFDFAFB6EA9BA84528ABE65F989 ();
// 0x000004FF System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemIsReference(System.Nullable`1<System.Boolean>)
extern void JsonProperty_set_ItemIsReference_mB817D311FCE34A65A42C096427637F95161D4162 ();
// 0x00000500 System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemTypeNameHandling()
extern void JsonProperty_get_ItemTypeNameHandling_m4DB1BFC69030FF7797FD8DF728C31B4B2E331877 ();
// 0x00000501 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemTypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern void JsonProperty_set_ItemTypeNameHandling_mBE904EF9267CF68EB2ECEC8FE9A6C6E44C76BAE6 ();
// 0x00000502 System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ItemReferenceLoopHandling()
extern void JsonProperty_get_ItemReferenceLoopHandling_mB25B842882A9708B06A096AADCBEFF39332A252A ();
// 0x00000503 System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ItemReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern void JsonProperty_set_ItemReferenceLoopHandling_m1E858AEB84D30F7B24FA71ECC6695EBF6DCC4726 ();
// 0x00000504 System.Void Newtonsoft.Json.Serialization.JsonProperty::WritePropertyName(Newtonsoft.Json.JsonWriter)
extern void JsonProperty_WritePropertyName_mFEA67C79463450A73C2D0C635F9D5814125D75B3 ();
// 0x00000505 System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern void JsonProperty__ctor_mABFD1A376901EED3B95C62A299286EAA4C5D60E8 ();
// 0x00000506 System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::.ctor(System.Type)
extern void JsonPropertyCollection__ctor_m2E403FE3F91492837D473554B1299CBEDB36C4DA ();
// 0x00000507 System.String Newtonsoft.Json.Serialization.JsonPropertyCollection::GetKeyForItem(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_GetKeyForItem_m5BE0DFD498BFF694D854BF776D559B111DB1205A ();
// 0x00000508 System.Void Newtonsoft.Json.Serialization.JsonPropertyCollection::AddProperty(Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonPropertyCollection_AddProperty_m4044DC5A15B7FB6B750A93C3C20F65DA40587EBC ();
// 0x00000509 Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetClosestMatchProperty(System.String)
extern void JsonPropertyCollection_GetClosestMatchProperty_m1AD3CBA8CAFF3CADEE5FACACCD5DFB6B4D05E6E8 ();
// 0x0000050A System.Boolean Newtonsoft.Json.Serialization.JsonPropertyCollection::TryGetValue(System.String,Newtonsoft.Json.Serialization.JsonProperty&)
extern void JsonPropertyCollection_TryGetValue_m5E757A7EE6699CE60CFF77BEED6FE9C8A87A994C ();
// 0x0000050B Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonPropertyCollection::GetProperty(System.String,System.StringComparison)
extern void JsonPropertyCollection_GetProperty_m057B2EC613A6B27C543E7BAB3C2FD5DB4DFD736D ();
// 0x0000050C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalBase__ctor_m9924B594FBBB7CF6D608BE28919E9D521A682FCE ();
// 0x0000050D Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::get_DefaultReferenceMappings()
extern void JsonSerializerInternalBase_get_DefaultReferenceMappings_mF07E4387AF858013980340E70C14F8981C258ADF ();
// 0x0000050E Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ResolvedNullValueHandling(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalBase_ResolvedNullValueHandling_m59CB67F6BBD682B9C9376E981A69A8A53D2B5F93 ();
// 0x0000050F Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::GetErrorContext(System.Object,System.Object,System.String,System.Exception)
extern void JsonSerializerInternalBase_GetErrorContext_mF10726F0352994E000C1ECD9EBFDFAFF92C6F402 ();
// 0x00000510 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase::ClearErrorContext()
extern void JsonSerializerInternalBase_ClearErrorContext_mC300E0725B338BBCFFE263633171865D0F4B694F ();
// 0x00000511 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase::IsErrorHandled(System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,Newtonsoft.Json.IJsonLineInfo,System.String,System.Exception)
extern void JsonSerializerInternalBase_IsErrorHandled_m70C5C217285CF3B33F86F7D3527701EE072E1605 ();
// 0x00000512 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.Equals(System.Object,System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_mA3CA65464B17BE559BC81A3F8FB8E865A2C3A4E3 ();
// 0x00000513 System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::System.Collections.Generic.IEqualityComparer<System.Object>.GetHashCode(System.Object)
extern void ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m34BFDEB7B6EB04CD7D23C6A53E627862155C5A25 ();
// 0x00000514 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalBase_ReferenceEqualsEqualityComparer::.ctor()
extern void ReferenceEqualsEqualityComparer__ctor_mEB61B4C5A39FA645DDAF54B10C91ED41D8ED0097 ();
// 0x00000515 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalReader__ctor_m84B910B18B4A333391D48F90DF6E005D87AECFDA ();
// 0x00000516 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type)
extern void JsonSerializerInternalReader_GetContractSafe_m38EF535F3F6AE2698416A6AB04A8F49ACA5079ED ();
// 0x00000517 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContract(System.Type)
extern void JsonSerializerInternalReader_GetContract_m417881EC192F8E6463A01F615F532F3D776CAE5A ();
// 0x00000518 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Deserialize(Newtonsoft.Json.JsonReader,System.Type,System.Boolean)
extern void JsonSerializerInternalReader_Deserialize_mBADEC4E0B606D2D25E9DB0F814EE349F74294364 ();
// 0x00000519 Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetInternalSerializer()
extern void JsonSerializerInternalReader_GetInternalSerializer_m74A94813F85F4A842ADB1966F06AF985940BC707 ();
// 0x0000051A Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJToken(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_CreateJToken_m392836D279982DF443FB754CF783D8DC2FC48E33 ();
// 0x0000051B Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJObject(Newtonsoft.Json.JsonReader)
extern void JsonSerializerInternalReader_CreateJObject_mA11DF73964BA4BA03A13110F4789F8BE53EB2FE9 ();
// 0x0000051C System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueInternal(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateValueInternal_m2143D4244958F80AB3D7A2CE4EB532E51BD46E41 ();
// 0x0000051D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CoerceEmptyStringToNull(System.Type,Newtonsoft.Json.Serialization.JsonContract,System.String)
extern void JsonSerializerInternalReader_CoerceEmptyStringToNull_m82CD37FDFA6FBE9A21D7EBED14850D6368B6B164 ();
// 0x0000051E System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetExpectedDescription(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_GetExpectedDescription_mB7B241939E1352F9BD4ED8542BCE9AEC74014910 ();
// 0x0000051F Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetConverter(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalReader_GetConverter_mDA6C98DF1A63778799D3E7ACA2BB512B45A823ED ();
// 0x00000520 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObject(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_CreateObject_mC442CD0B382ADC9E1CACB60C76DB80DAE239795E ();
// 0x00000521 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataPropertiesToken(Newtonsoft.Json.Linq.JTokenReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern void JsonSerializerInternalReader_ReadMetadataPropertiesToken_mE7EFBE1AA260787A616AFA9A7F39BED1C2B4465E ();
// 0x00000522 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadMetadataProperties(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Object&,System.String&)
extern void JsonSerializerInternalReader_ReadMetadataProperties_m0EEA84F640A074425430733364C72A1B61CDAF8D ();
// 0x00000523 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolveTypeName(Newtonsoft.Json.JsonReader,System.Type&,Newtonsoft.Json.Serialization.JsonContract&,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_ResolveTypeName_m45FFCF99B4E199728915DB902C47CDF8B97D5348 ();
// 0x00000524 Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureArrayContract(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_EnsureArrayContract_m6300A06BED91530127768F19F2F8A46995AC7F2C ();
// 0x00000525 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateList(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern void JsonSerializerInternalReader_CreateList_m8F91654B57A26686A907278DEFA86DBB75532821 ();
// 0x00000526 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasNoDefinedType(Newtonsoft.Json.Serialization.JsonContract)
extern void JsonSerializerInternalReader_HasNoDefinedType_mC13B8E91C19C981F5D75A3D223184BDF794CEBFA ();
// 0x00000527 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureType(Newtonsoft.Json.JsonReader,System.Object,System.Globalization.CultureInfo,Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern void JsonSerializerInternalReader_EnsureType_mF55F1BD9D0540AF9A0D58F08D6B6AA0F6AA78166 ();
// 0x00000528 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern void JsonSerializerInternalReader_SetPropertyValue_mE604DBB1E2133A32DA492C18BC7C5227ED220277 ();
// 0x00000529 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CalculatePropertyDetails(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter&,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object,System.Boolean&,System.Object&,Newtonsoft.Json.Serialization.JsonContract&,System.Boolean&,System.Boolean&)
extern void JsonSerializerInternalReader_CalculatePropertyDetails_mC3750E84FE2D0A374888840DC8E8EC53DF333870 ();
// 0x0000052A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::AddReference(Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_AddReference_m408136601F7A55FAF2DCE8A795FF62750D46FC00 ();
// 0x0000052B System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalReader_HasFlag_m5B915C81BB1A77886B67A2F003A2A04944A2BEB8 ();
// 0x0000052C System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldSetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonObjectContract,System.Object)
extern void JsonSerializerInternalReader_ShouldSetPropertyValue_m9CA1330FAE1539772C23690804E9ED4500261C4F ();
// 0x0000052D System.Collections.IList Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewList(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewList_m2D1B33586CC7C408B619F0167B4FC64AB0E0D9FE ();
// 0x0000052E System.Collections.IDictionary Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewDictionary(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewDictionary_mFAD6A679C07C8DE6337219EDCC4AD5B554C6BC34 ();
// 0x0000052F System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserializing(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserializing_m268BA36974DA49878FE4F7B550DBBDEF0517D8E8 ();
// 0x00000530 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::OnDeserialized(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalReader_OnDeserialized_m0083FA8B79FC36B7E74A67FF54C0D4E9326CF4D3 ();
// 0x00000531 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateDictionary(System.Collections.IDictionary,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateDictionary_m7F33C2CB98A3BEE15E45B463F9FD78D7D4263AB9 ();
// 0x00000532 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateMultidimensionalArray(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateMultidimensionalArray_mBF5F6B366BBF5BC1E35EA8FFDC165A9B84C8CF0C ();
// 0x00000533 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ThrowUnexpectedEndException(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.String)
extern void JsonSerializerInternalReader_ThrowUnexpectedEndException_m101EFD78937CCF2DC3ABABB95227949806059955 ();
// 0x00000534 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateList(System.Collections.IList,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateList_m8A9B2EF70BDDD2097CBEFADF4F61968FA6A498A7 ();
// 0x00000535 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializable(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_CreateISerializable_mF2D90D10426A9ECEEA037565ABD4ABF110BDF8D7 ();
// 0x00000536 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializableItem(Newtonsoft.Json.Linq.JToken,System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalReader_CreateISerializableItem_m5F10D375448F2308DD5E18E2440A6CBE9556DA6E ();
// 0x00000537 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateDynamic(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDynamicContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_CreateDynamic_m24C9A857ED8B3896286A728101115B207188F762 ();
// 0x00000538 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObjectUsingCreatorWithParameters(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>,System.String)
extern void JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_mDB395A12FD6087B7ED7AB294B039FCD645C06D75 ();
// 0x00000539 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::DeserializeConvertable(Newtonsoft.Json.JsonConverter,Newtonsoft.Json.JsonReader,System.Type,System.Object)
extern void JsonSerializerInternalReader_DeserializeConvertable_m3038C49BB7226A43F035742300541E6716B8C305 ();
// 0x0000053A System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolvePropertyAndCreatorValues(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_m50795BC70D9B5E777FCDB62BEBBD9C5E1D357D1C ();
// 0x0000053B System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateNewObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,System.String,System.Boolean&)
extern void JsonSerializerInternalReader_CreateNewObject_mD2AAE7099669E5FDF50FD5071D62B0E71DF38F2D ();
// 0x0000053C System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateObject(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,System.String)
extern void JsonSerializerInternalReader_PopulateObject_m54CC247DA1F185408A10836A77456397FE273BD8 ();
// 0x0000053D System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldDeserialize(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalReader_ShouldDeserialize_m6904DF089934441900B22374416545F7553D7CAF ();
// 0x0000053E System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CheckPropertyName(Newtonsoft.Json.JsonReader,System.String)
extern void JsonSerializerInternalReader_CheckPropertyName_mBAE2B3F98D8F326C3F6B919025F441597A7B078C ();
// 0x0000053F System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetExtensionData(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.String,System.Object)
extern void JsonSerializerInternalReader_SetExtensionData_mD00249CF9B85AB62AB055A422D897C18113D3C5E ();
// 0x00000540 System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadExtensionDataValue(Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader)
extern void JsonSerializerInternalReader_ReadExtensionDataValue_mB627B7B92BEF9C7ADEDE29172A9793A7FE65CF1F ();
// 0x00000541 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EndProcessProperty(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonObjectContract,System.Int32,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence,System.Boolean)
extern void JsonSerializerInternalReader_EndProcessProperty_m0A115FD2984F8FBFD1EA281E05C5B358E1014A79 ();
// 0x00000542 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyPresence(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence>)
extern void JsonSerializerInternalReader_SetPropertyPresence_mE1FE421F01D285CE4F6E0E5BE06F1C79E52BBE56 ();
// 0x00000543 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HandleError(Newtonsoft.Json.JsonReader,System.Boolean,System.Int32)
extern void JsonSerializerInternalReader_HandleError_mB3C42C7EDC61BB938C94133DE2EEC0EC9FE5B94F ();
// 0x00000544 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext::.ctor(System.String)
extern void CreatorPropertyContext__ctor_mF743F3DBE3DE03A4079C503DBD0B2E4EAD035CC7 ();
// 0x00000545 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_mF0EB3DE96FACD9AD3FD9F3DF2EA616B1D5E02EFA ();
// 0x00000546 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c__DisplayClass38_0::<CreateObjectUsingCreatorWithParameters>b__1(Newtonsoft.Json.Serialization.JsonSerializerInternalReader_CreatorPropertyContext)
extern void U3CU3Ec__DisplayClass38_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_mD7DF82CDA2D357542E67243C7DF7E6711FA1FBFE ();
// 0x00000547 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::.cctor()
extern void U3CU3Ec__cctor_mCDFABC3C0AC5181A60BC55CCE535F428906E8A8E ();
// 0x00000548 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::.ctor()
extern void U3CU3Ec__ctor_mCC15938D9283F207914384F3C723590548E77EDA ();
// 0x00000549 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<CreateObjectUsingCreatorWithParameters>b__38_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__38_0_m3B46B0CDC0AAC57F21D183E89C16FEB1D9C332CD ();
// 0x0000054A System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<CreateObjectUsingCreatorWithParameters>b__38_2(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__38_2_m178470408855EF773A5835D565EA26AEC4843620 ();
// 0x0000054B Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<PopulateObject>b__42_0(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__42_0_m3CCB0D8AB2ABE4328681549F15DE05D9E1FB943D ();
// 0x0000054C Newtonsoft.Json.Serialization.JsonSerializerInternalReader_PropertyPresence Newtonsoft.Json.Serialization.JsonSerializerInternalReader_<>c::<PopulateObject>b__42_1(Newtonsoft.Json.Serialization.JsonProperty)
extern void U3CU3Ec_U3CPopulateObjectU3Eb__42_1_m57B62ACC149660BA895F211C5B01943DA239C634 ();
// 0x0000054D System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern void JsonSerializerInternalWriter__ctor_m33A513551DFD57A754CBC292875D8647D9C90354 ();
// 0x0000054E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerInternalWriter_Serialize_m707CFA0C0B62B161BF402C87255DEA7BDA1F1BF0 ();
// 0x0000054F Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern void JsonSerializerInternalWriter_GetInternalSerializer_m659C0D3DCF0610F80DE4FDF64A1B146145C49CD5 ();
// 0x00000550 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern void JsonSerializerInternalWriter_GetContractSafe_m21516BE389D4587D5DDF20710A4E3BD578FF0B98 ();
// 0x00000551 Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContract(System.Object)
extern void JsonSerializerInternalWriter_GetContract_m35B22C5C5607A598BA1034AF4C036CEF8B941641 ();
// 0x00000552 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializePrimitive_m1F9F8F0F26D50DC1473475D1BB2444D125B06337 ();
// 0x00000553 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeValue_mD61EB3DBE3D0C93FE456328AE34B1BFFA0551449 ();
// 0x00000554 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ResolveIsReference(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ResolveIsReference_mEA802A7DBEE556E52CDA196B6FFE605488F56318 ();
// 0x00000555 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteReference_mC3A021ADB209FD105A1F0A12E1A1C7CEDDCC7F56 ();
// 0x00000556 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteProperty(System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteProperty_m5E6672F648A0E7F5250372A0E08AA458426DDD35 ();
// 0x00000557 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_CheckForCircularReference_mDE39910B958FE1D68FB8D59F66351EEB703378CA ();
// 0x00000558 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_WriteReference_mC605E2E5553627A5770962B8822A5C66E1F44D0E ();
// 0x00000559 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetReference(Newtonsoft.Json.JsonWriter,System.Object)
extern void JsonSerializerInternalWriter_GetReference_m3FA8F945FB8855C1E8A6E03BD761A9C38A38B6C5 ();
// 0x0000055A System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern void JsonSerializerInternalWriter_TryConvertToString_m74B307586429D228216FE93B9113C4A65C944371 ();
// 0x0000055B System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern void JsonSerializerInternalWriter_SerializeString_m60887799FFDBE7A94349AB1525734D7AAD382B58 ();
// 0x0000055C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerializing(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerializing_mA38E0CFBB65D91E3CACF7886BF8902D671F51DD4 ();
// 0x0000055D System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::OnSerialized(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonContract,System.Object)
extern void JsonSerializerInternalWriter_OnSerialized_m04503F54C55F4EEE8E669C5DBEE2FC4C7F0FF439 ();
// 0x0000055E System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeObject_mF0EA7E8A7D0A9E843CDE5E83CE18B7E03D8F25A4 ();
// 0x0000055F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CalculatePropertyValues(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract&,System.Object&)
extern void JsonSerializerInternalWriter_CalculatePropertyValues_m17CC13E541FAD756B2544ACEF50E85D567A51AEA ();
// 0x00000560 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteObjectStart(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteObjectStart_m869B63137B88D12E4B0A32BC32ADEEDC659564AC ();
// 0x00000561 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasCreatorParameter(Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_HasCreatorParameter_mDD45720C7BC7F0DB26B95E1B99C73B7740685AE9 ();
// 0x00000562 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReferenceIdProperty(Newtonsoft.Json.JsonWriter,System.Type,System.Object)
extern void JsonSerializerInternalWriter_WriteReferenceIdProperty_mA5E2DE1A9D72D6777CBEAE39D18DA4352F91810C ();
// 0x00000563 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern void JsonSerializerInternalWriter_WriteTypeProperty_mB38CA27C4E202C93794B676DF635DBE33E4562EC ();
// 0x00000564 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerInternalWriter_HasFlag_m3C84D0037FEBADD9D50BFB0BEAE24841226ABCFF ();
// 0x00000565 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerInternalWriter_HasFlag_m1F0DFCC3FDC84C3E821AF042621BFF7D21CF3267 ();
// 0x00000566 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerInternalWriter_HasFlag_m343A3BF98DC5BFFB21FF727BF3EAEC63E4D929B3 ();
// 0x00000567 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeConvertable_m96E202C8E55EB3201FAD7FAA308D70F726ECA20E ();
// 0x00000568 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,System.Collections.IEnumerable,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeList_mE4C06D471B8E6746D52382143F881A0B40981CB5 ();
// 0x00000569 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_m4674A1CF208920B4CB6F0405449F892E4E8CF946 ();
// 0x0000056A System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeMultidimensionalArray(Newtonsoft.Json.JsonWriter,System.Array,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,System.Int32,System.Int32[])
extern void JsonSerializerInternalWriter_SerializeMultidimensionalArray_mCCF2F792B68A6E8AB97EE7C4E505D73BA421775F ();
// 0x0000056B System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteStartArray(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_WriteStartArray_m2CE2821C2BEF10DBD14244AE46E5FB2F17D1CCCB ();
// 0x0000056C System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeISerializable(Newtonsoft.Json.JsonWriter,System.Runtime.Serialization.ISerializable,Newtonsoft.Json.Serialization.JsonISerializableContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeISerializable_mAD90C6519F6590F8A9915C70BDC7B42F375DB833 ();
// 0x0000056D System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDynamic(Newtonsoft.Json.JsonWriter,System.Dynamic.IDynamicMetaObjectProvider,Newtonsoft.Json.Serialization.JsonDynamicContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeDynamic_m7DDB3A53FFE50ABCF123763AE0E8C3502C576119 ();
// 0x0000056E System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteDynamicProperty(System.Object)
extern void JsonSerializerInternalWriter_ShouldWriteDynamicProperty_m4822FB4F8FE4D5E4164003FDC6F4CD983485000F ();
// 0x0000056F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_ShouldWriteType_m99E52666597835534A7A33D1A135DBAA29C1E49C ();
// 0x00000570 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,System.Collections.IDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContainerContract,Newtonsoft.Json.Serialization.JsonProperty)
extern void JsonSerializerInternalWriter_SerializeDictionary_m43AA3F60EB93F947A63344DC382D9744F69EFE24 ();
// 0x00000571 System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Boolean&)
extern void JsonSerializerInternalWriter_GetPropertyName_mF037E280338FBF288BCDD0155E5380D6C61EF855 ();
// 0x00000572 System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern void JsonSerializerInternalWriter_HandleError_m9C8E83D03C30410C95766BB1BE8E31A46D0C44E4 ();
// 0x00000573 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_ShouldSerialize_m234E3479C305C5E38D143C37375D757CB8E0C107 ();
// 0x00000574 System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern void JsonSerializerInternalWriter_IsSpecified_mC0E4F1C42F45907DAA98C832C1DB0C19E22422B4 ();
// 0x00000575 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::add_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_add_Error_mF2EF3D15D9A9094523E7C26CDFEDA4D48D066E13 ();
// 0x00000576 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::remove_Error(System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>)
extern void JsonSerializerProxy_remove_Error_m1F1D074344139E88826E27D52FBC9FDF6B901EB5 ();
// 0x00000577 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceResolver(Newtonsoft.Json.Serialization.IReferenceResolver)
extern void JsonSerializerProxy_set_ReferenceResolver_m9B55AEC432FFEB6F6608EEA7774C7E4AAEBC8FEE ();
// 0x00000578 Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::get_TraceWriter()
extern void JsonSerializerProxy_get_TraceWriter_mDE7236F88BF24B2A80D57705DC4FC1F90058D309 ();
// 0x00000579 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TraceWriter(Newtonsoft.Json.Serialization.ITraceWriter)
extern void JsonSerializerProxy_set_TraceWriter_m2E7CEDF9C8A6918CB29B5B5CC5FA912220DC31D1 ();
// 0x0000057A System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_EqualityComparer(System.Collections.IEqualityComparer)
extern void JsonSerializerProxy_set_EqualityComparer_m0D3427DD852FA45D7812EA0878ABC025F063404B ();
// 0x0000057B Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Converters()
extern void JsonSerializerProxy_get_Converters_m7D0DA28309E628F03F2C5E2F046DD0C539A412F6 ();
// 0x0000057C System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_DefaultValueHandling(Newtonsoft.Json.DefaultValueHandling)
extern void JsonSerializerProxy_set_DefaultValueHandling_m59E356F0BA097FB5D0933F278E5613BAA4E58294 ();
// 0x0000057D Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerProxy::get_ContractResolver()
extern void JsonSerializerProxy_get_ContractResolver_mBEF012F2144CF88BB5D09E147D01FCD248B4237C ();
// 0x0000057E System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern void JsonSerializerProxy_set_ContractResolver_m5075C86F641EC82329F06F140EF6F2B0C5EBCB78 ();
// 0x0000057F System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MissingMemberHandling(Newtonsoft.Json.MissingMemberHandling)
extern void JsonSerializerProxy_set_MissingMemberHandling_m2E83B36F2336E4B7414FBA581F8954FC3B519FF7 ();
// 0x00000580 Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_NullValueHandling()
extern void JsonSerializerProxy_get_NullValueHandling_mEC7FF21CE82CD5F19329A449ADCB5CD252C605F5 ();
// 0x00000581 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_NullValueHandling(Newtonsoft.Json.NullValueHandling)
extern void JsonSerializerProxy_set_NullValueHandling_m2B79A43EE8DC7EFEE9B6FB16BFB9C1C8749231DA ();
// 0x00000582 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ObjectCreationHandling(Newtonsoft.Json.ObjectCreationHandling)
extern void JsonSerializerProxy_set_ObjectCreationHandling_m5340EC76209BE9FD9196E8A384CF24E841F55B49 ();
// 0x00000583 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ReferenceLoopHandling(Newtonsoft.Json.ReferenceLoopHandling)
extern void JsonSerializerProxy_set_ReferenceLoopHandling_m60633FF1711BC421F597D9C1018C8AA9CC041883 ();
// 0x00000584 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_PreserveReferencesHandling(Newtonsoft.Json.PreserveReferencesHandling)
extern void JsonSerializerProxy_set_PreserveReferencesHandling_mFD00F4958D60F0E3AA94B747B0211AD14B15358F ();
// 0x00000585 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameHandling(Newtonsoft.Json.TypeNameHandling)
extern void JsonSerializerProxy_set_TypeNameHandling_m3FBD9D4AAB50D80487A73ABEBB6CE28D7F4BDD42 ();
// 0x00000586 Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.Serialization.JsonSerializerProxy::get_MetadataPropertyHandling()
extern void JsonSerializerProxy_get_MetadataPropertyHandling_mC25F1DC18C01CD262F29D4E7382038197894A97D ();
// 0x00000587 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_MetadataPropertyHandling(Newtonsoft.Json.MetadataPropertyHandling)
extern void JsonSerializerProxy_set_MetadataPropertyHandling_m2C01F1F046E2355B91FF3801B2E6FF2A248C9344 ();
// 0x00000588 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_TypeNameAssemblyFormatHandling(Newtonsoft.Json.TypeNameAssemblyFormatHandling)
extern void JsonSerializerProxy_set_TypeNameAssemblyFormatHandling_mDAA8E97C1392F37D558BFDFE14F89A3491ECFAF4 ();
// 0x00000589 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_ConstructorHandling(Newtonsoft.Json.ConstructorHandling)
extern void JsonSerializerProxy_set_ConstructorHandling_m2F984C8CDFBAC765FD17B892B6FA4DBCD8A42B1F ();
// 0x0000058A System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_SerializationBinder(Newtonsoft.Json.Serialization.ISerializationBinder)
extern void JsonSerializerProxy_set_SerializationBinder_m257111821814BD400BB5F523190D1AD2CDD617D0 ();
// 0x0000058B System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Context()
extern void JsonSerializerProxy_get_Context_mAE9E38CD49020552815E1DA093246E21122D38BC ();
// 0x0000058C System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Context(System.Runtime.Serialization.StreamingContext)
extern void JsonSerializerProxy_set_Context_m87D66D48B7487972C2A5316BFEC386B80DFC5B2E ();
// 0x0000058D Newtonsoft.Json.Formatting Newtonsoft.Json.Serialization.JsonSerializerProxy::get_Formatting()
extern void JsonSerializerProxy_get_Formatting_mD75120CBDF3E4146DD6A2ACA871FA4396EBDB338 ();
// 0x0000058E System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_Formatting(Newtonsoft.Json.Formatting)
extern void JsonSerializerProxy_set_Formatting_mD9D8F5CAC54632DB441BF455F021F1C9214AEAFE ();
// 0x0000058F System.Boolean Newtonsoft.Json.Serialization.JsonSerializerProxy::get_CheckAdditionalContent()
extern void JsonSerializerProxy_get_CheckAdditionalContent_mBC0C0743A0B731DA2C1F60AFA75E0EFBA62CF165 ();
// 0x00000590 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::set_CheckAdditionalContent(System.Boolean)
extern void JsonSerializerProxy_set_CheckAdditionalContent_m51B6F02E06D463137B396E428C06F3777F800A6A ();
// 0x00000591 Newtonsoft.Json.Serialization.JsonSerializerInternalBase Newtonsoft.Json.Serialization.JsonSerializerProxy::GetInternalSerializer()
extern void JsonSerializerProxy_GetInternalSerializer_m7E88F076E26CBCB287598ED57AE332056C60F598 ();
// 0x00000592 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern void JsonSerializerProxy__ctor_m450813A5C9BFE4ACBB1374D30255055E836F819D ();
// 0x00000593 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::.ctor(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern void JsonSerializerProxy__ctor_m48ABC76757CD3D9BB7C72B0474704FA98798D609 ();
// 0x00000594 System.Object Newtonsoft.Json.Serialization.JsonSerializerProxy::DeserializeInternal(Newtonsoft.Json.JsonReader,System.Type)
extern void JsonSerializerProxy_DeserializeInternal_mF48D6DE08B58D4791BA52237374F45ACB637685E ();
// 0x00000595 System.Void Newtonsoft.Json.Serialization.JsonSerializerProxy::SerializeInternal(Newtonsoft.Json.JsonWriter,System.Object,System.Type)
extern void JsonSerializerProxy_SerializeInternal_mA4A87ADA184432F52798DF72C92911FD5340CFA7 ();
// 0x00000596 System.Void Newtonsoft.Json.Serialization.JsonStringContract::.ctor(System.Type)
extern void JsonStringContract__ctor_m29822C0D0E361FF18D429DD6D8ED87CE1A4F130B ();
// 0x00000597 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetCachedAttribute(System.Object)
// 0x00000598 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::CanTypeDescriptorConvertString(System.Type,System.ComponentModel.TypeConverter&)
extern void JsonTypeReflector_CanTypeDescriptorConvertString_m84300AF4F2D012CFA7D7EEBF7F83FA5570C31625 ();
// 0x00000599 System.Runtime.Serialization.DataContractAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataContractAttribute(System.Type)
extern void JsonTypeReflector_GetDataContractAttribute_mF22093A92DFAC8FEF02DBA94CDCF0729B5BCB224 ();
// 0x0000059A System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.JsonTypeReflector::GetDataMemberAttribute(System.Reflection.MemberInfo)
extern void JsonTypeReflector_GetDataMemberAttribute_m0C7037E9C4357869EF1ACD388E2483AE58F67EE4 ();
// 0x0000059B Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonTypeReflector::GetObjectMemberSerialization(System.Type,System.Boolean)
extern void JsonTypeReflector_GetObjectMemberSerialization_m594BFFB1DCF35067A078329FBF558277EEBC856C ();
// 0x0000059C Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::GetJsonConverter(System.Object)
extern void JsonTypeReflector_GetJsonConverter_m5F6290D480CD2D77A78F478B8839DBB1C363690B ();
// 0x0000059D Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonTypeReflector::CreateJsonConverterInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateJsonConverterInstance_m301154F582E0F7D691056BFBBBCF009BB3B83C5D ();
// 0x0000059E Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::CreateNamingStrategyInstance(System.Type,System.Object[])
extern void JsonTypeReflector_CreateNamingStrategyInstance_mF696C077FA0BE88B523B33C8BECD5EEF09BA74BB ();
// 0x0000059F Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.JsonTypeReflector::GetContainerNamingStrategy(Newtonsoft.Json.JsonContainerAttribute)
extern void JsonTypeReflector_GetContainerNamingStrategy_m3600547A4E20616F67AA956D1F1E25EA393235F2 ();
// 0x000005A0 System.Func`2<System.Object[],System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector::GetCreator(System.Type)
extern void JsonTypeReflector_GetCreator_m474B3070ABB1AE4562702580108F30C561DD31EE ();
// 0x000005A1 System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociatedMetadataType(System.Type)
extern void JsonTypeReflector_GetAssociatedMetadataType_m53B88A5BECF179C9C647C7B3C8DE8C858F385AAC ();
// 0x000005A2 System.Type Newtonsoft.Json.Serialization.JsonTypeReflector::GetAssociateMetadataTypeFromAttribute(System.Type)
extern void JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_m8F97BC6B3C0377088FAA96BF06C1EAE0627B2A2C ();
// 0x000005A3 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Type)
// 0x000005A4 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Reflection.MemberInfo)
// 0x000005A5 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::IsNonSerializable(System.Object)
extern void JsonTypeReflector_IsNonSerializable_m22341E5E343D2C5C7B3D9DC92E3CBD09D1F61418 ();
// 0x000005A6 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::IsSerializable(System.Object)
extern void JsonTypeReflector_IsSerializable_m5008F24D0786D5C3290EF5A486C61E1DB6F5FAA2 ();
// 0x000005A7 T Newtonsoft.Json.Serialization.JsonTypeReflector::GetAttribute(System.Object)
// 0x000005A8 System.Boolean Newtonsoft.Json.Serialization.JsonTypeReflector::get_FullyTrusted()
extern void JsonTypeReflector_get_FullyTrusted_m8B3C3C180728D881ADD8BE997D5C854F60BB985E ();
// 0x000005A9 Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.JsonTypeReflector::get_ReflectionDelegateFactory()
extern void JsonTypeReflector_get_ReflectionDelegateFactory_m1B84C786AA4BDF710B3D8687E4349845B1D52ECA ();
// 0x000005AA System.Void Newtonsoft.Json.Serialization.JsonTypeReflector::.cctor()
extern void JsonTypeReflector__cctor_m9E98E9D497A5D8CF4CAD32C54F633BAD38DB57AD ();
// 0x000005AB System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m45AEC28F89F0F9830013E3AC574EF508001E03CE ();
// 0x000005AC System.Object Newtonsoft.Json.Serialization.JsonTypeReflector_<>c__DisplayClass22_0::<GetCreator>b__0(System.Object[])
extern void U3CU3Ec__DisplayClass22_0_U3CGetCreatorU3Eb__0_m11E5A1D76268AB0E28B4507A4184C0A29354A469 ();
// 0x000005AD System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::.cctor()
extern void U3CU3Ec__cctor_mBF2DFA692E52FBFC78909AA00DF4164E9E7E480F ();
// 0x000005AE System.Void Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::.ctor()
extern void U3CU3Ec__ctor_m41345F77121364B62BD6235689A94FCB6EAD11D2 ();
// 0x000005AF System.Type Newtonsoft.Json.Serialization.JsonTypeReflector_<>c::<GetCreator>b__22_1(System.Object)
extern void U3CU3Ec_U3CGetCreatorU3Eb__22_1_m5FC35D391C707A03D4E090418381CA55A75E7DCD ();
// 0x000005B0 System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_ProcessDictionaryKeys()
extern void NamingStrategy_get_ProcessDictionaryKeys_mA0BB47718E74A21AA9691A3ABBE7BD839124F9C5 ();
// 0x000005B1 System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_ProcessExtensionDataNames()
extern void NamingStrategy_get_ProcessExtensionDataNames_mF481764166B3218F7511EDE9541511CAA281A304 ();
// 0x000005B2 System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::get_OverrideSpecifiedNames()
extern void NamingStrategy_get_OverrideSpecifiedNames_m36F4D8839E9F2352DDCD7CE2260F544C760087CD ();
// 0x000005B3 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetPropertyName(System.String,System.Boolean)
extern void NamingStrategy_GetPropertyName_mB04ED92612E21C0E2D1DF5F78B116109769EB8D7 ();
// 0x000005B4 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetExtensionDataName(System.String)
extern void NamingStrategy_GetExtensionDataName_mAC2B2314D2E11F9630A5FAE10FF32E0626E3B63A ();
// 0x000005B5 System.String Newtonsoft.Json.Serialization.NamingStrategy::GetDictionaryKey(System.String)
extern void NamingStrategy_GetDictionaryKey_m3483DBD7B7066C82C12DBF56C8E5F786B137C537 ();
// 0x000005B6 System.String Newtonsoft.Json.Serialization.NamingStrategy::ResolvePropertyName(System.String)
// 0x000005B7 System.Int32 Newtonsoft.Json.Serialization.NamingStrategy::GetHashCode()
extern void NamingStrategy_GetHashCode_m6CC23DF05988865D6D8B8B2B4F7067496C2FD6FA ();
// 0x000005B8 System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::Equals(System.Object)
extern void NamingStrategy_Equals_m5DD4B5BBB758D1F570BDC58AB5BB34CC76D854E5 ();
// 0x000005B9 System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::Equals(Newtonsoft.Json.Serialization.NamingStrategy)
extern void NamingStrategy_Equals_mE5BD95F018AC817E98B42D11383D26572909538E ();
// 0x000005BA System.Void Newtonsoft.Json.Serialization.NamingStrategy::.ctor()
extern void NamingStrategy__ctor_m6E3F1A8B7B18168CF71DAC024F6696DA72F30FDC ();
// 0x000005BB System.Void Newtonsoft.Json.Serialization.ObjectConstructor`1::.ctor(System.Object,System.IntPtr)
// 0x000005BC System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::Invoke(System.Object[])
// 0x000005BD System.IAsyncResult Newtonsoft.Json.Serialization.ObjectConstructor`1::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
// 0x000005BE System.Object Newtonsoft.Json.Serialization.ObjectConstructor`1::EndInvoke(System.IAsyncResult)
// 0x000005BF System.Void Newtonsoft.Json.Serialization.ReflectionAttributeProvider::.ctor(System.Object)
extern void ReflectionAttributeProvider__ctor_mDFA04D6DB3CD1085D5C4A10CC31013E66D92FD98 ();
// 0x000005C0 System.Void Newtonsoft.Json.Serialization.TraceJsonReader::.ctor(Newtonsoft.Json.JsonReader)
extern void TraceJsonReader__ctor_mDFF4B48518BFE3985F239509546CEF7F43B431DD ();
// 0x000005C1 System.String Newtonsoft.Json.Serialization.TraceJsonReader::GetDeserializedJsonMessage()
extern void TraceJsonReader_GetDeserializedJsonMessage_m2EC51A79AB178A896A4022BAC28EB05ACEDB9518 ();
// 0x000005C2 System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Read()
extern void TraceJsonReader_Read_m317908643ABB850535339E49B4536B3E61FC5903 ();
// 0x000005C3 System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsInt32()
extern void TraceJsonReader_ReadAsInt32_mA6D12424BEC5C9B434B78937D0B063273CF631C3 ();
// 0x000005C4 System.String Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsString()
extern void TraceJsonReader_ReadAsString_m768F8D8BB0E7D858A9189FC74F906C201DC8961F ();
// 0x000005C5 System.Byte[] Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBytes()
extern void TraceJsonReader_ReadAsBytes_mAC1F756870E1F1BA5B93AAF3120E732DFAFECF7C ();
// 0x000005C6 System.Nullable`1<System.Decimal> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDecimal()
extern void TraceJsonReader_ReadAsDecimal_mAE12C2F15289C4C26F7AA3B569680F2185552F9E ();
// 0x000005C7 System.Nullable`1<System.Double> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDouble()
extern void TraceJsonReader_ReadAsDouble_m639DA1C7ACD3A5F52330F513692D18374F988711 ();
// 0x000005C8 System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsBoolean()
extern void TraceJsonReader_ReadAsBoolean_mC302CB7BF9394BF4F58391A7F8DF55C2E8115DF1 ();
// 0x000005C9 System.Nullable`1<System.DateTime> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTime()
extern void TraceJsonReader_ReadAsDateTime_mF1EB174F229872E7144C97673A7D069A465D2F22 ();
// 0x000005CA System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Serialization.TraceJsonReader::ReadAsDateTimeOffset()
extern void TraceJsonReader_ReadAsDateTimeOffset_m3A2D657D7E820F2284B0C92662CD18FD7666FB57 ();
// 0x000005CB System.Void Newtonsoft.Json.Serialization.TraceJsonReader::WriteCurrentToken()
extern void TraceJsonReader_WriteCurrentToken_m495829D4A70DAA81B0273B22827CE971F8956F43 ();
// 0x000005CC System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::get_Depth()
extern void TraceJsonReader_get_Depth_m39A35E3212D361F9C2715FB51C833411460F1FD9 ();
// 0x000005CD System.String Newtonsoft.Json.Serialization.TraceJsonReader::get_Path()
extern void TraceJsonReader_get_Path_m1165599BDE969148D6847B563AA25174BD265D79 ();
// 0x000005CE Newtonsoft.Json.JsonToken Newtonsoft.Json.Serialization.TraceJsonReader::get_TokenType()
extern void TraceJsonReader_get_TokenType_m44102BF5C38E3F80D9A94EF874E7C8B4050A2994 ();
// 0x000005CF System.Object Newtonsoft.Json.Serialization.TraceJsonReader::get_Value()
extern void TraceJsonReader_get_Value_m642F6E3510027EA14D0326DE650BE34638B987DD ();
// 0x000005D0 System.Type Newtonsoft.Json.Serialization.TraceJsonReader::get_ValueType()
extern void TraceJsonReader_get_ValueType_mC956D916766C35819F8C8DC759AD75F446DA5A0E ();
// 0x000005D1 System.Void Newtonsoft.Json.Serialization.TraceJsonReader::Close()
extern void TraceJsonReader_Close_mE6F26C26EE18476812A49804E49236071FC1621F ();
// 0x000005D2 System.Boolean Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mA888E5A94A9A801FACC3A4F7CB3BA993D690CEA2 ();
// 0x000005D3 System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m06E0A5A23FB4FAC4CAD5F27D410BD9A0AC9EC9F1 ();
// 0x000005D4 System.Int32 Newtonsoft.Json.Serialization.TraceJsonReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m8FE95795CA790CF4918820C50888F216662091C5 ();
// 0x000005D5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::.ctor(Newtonsoft.Json.JsonWriter)
extern void TraceJsonWriter__ctor_mCB8E954714DBDB57E48E7011F884DFDF6A11BB75 ();
// 0x000005D6 System.String Newtonsoft.Json.Serialization.TraceJsonWriter::GetSerializedJsonMessage()
extern void TraceJsonWriter_GetSerializedJsonMessage_mCCBCAC2AAB27DA3A6BD7B3F134E8CC541BD04C23 ();
// 0x000005D7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Decimal)
extern void TraceJsonWriter_WriteValue_m090F44A298FDD947C98342724E700BCAF2B56EAE ();
// 0x000005D8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Decimal>)
extern void TraceJsonWriter_WriteValue_m6DB6EDEBCB63883905D5E3F4BE7C512ACF72E21C ();
// 0x000005D9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Boolean)
extern void TraceJsonWriter_WriteValue_mE98BAD306FFE01F7616FD7070EB707AF4DA60444 ();
// 0x000005DA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Boolean>)
extern void TraceJsonWriter_WriteValue_m571FBAF27728796D14CE3C09796839B228D75ED4 ();
// 0x000005DB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte)
extern void TraceJsonWriter_WriteValue_m8E65D9583B2B0B44A253FA278AFF8FED0FEE882F ();
// 0x000005DC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Byte>)
extern void TraceJsonWriter_WriteValue_m635BDB293E4321010C4A8B8F9CFBA285752EBC47 ();
// 0x000005DD System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Char)
extern void TraceJsonWriter_WriteValue_m262AF5C637DC3400C4E8FDCC44BE881526E7C03F ();
// 0x000005DE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Char>)
extern void TraceJsonWriter_WriteValue_mB7E21DF98E786326BDAD98EF3296A9465882394C ();
// 0x000005DF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Byte[])
extern void TraceJsonWriter_WriteValue_mFB13F476D1CEC7EC22A0FBF7868ECC55607A8DA8 ();
// 0x000005E0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.DateTime)
extern void TraceJsonWriter_WriteValue_mED823B8BDD21F94FF946DAD7456201F50F822985 ();
// 0x000005E1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.DateTime>)
extern void TraceJsonWriter_WriteValue_m523A5988B1AE5779B0695CF310135830378F9709 ();
// 0x000005E2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.DateTimeOffset)
extern void TraceJsonWriter_WriteValue_m705F6723792F3317FF0904CC07F559129E6368A8 ();
// 0x000005E3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.DateTimeOffset>)
extern void TraceJsonWriter_WriteValue_m306400AB203E4EA3C37E43D065DC7481AB18C231 ();
// 0x000005E4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Double)
extern void TraceJsonWriter_WriteValue_m73B0F7FBC37FEA6CC1FA4E6B3DCFC5A731786DC8 ();
// 0x000005E5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Double>)
extern void TraceJsonWriter_WriteValue_mA042AFB9ABDD5502861B82BA4E6902C7E59D7496 ();
// 0x000005E6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteUndefined()
extern void TraceJsonWriter_WriteUndefined_m4C20713F57208CDEC39BDE4D5B09B96822079374 ();
// 0x000005E7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteNull()
extern void TraceJsonWriter_WriteNull_mA5994CB415761756E03A4F4B465ACB3951BD2FE8 ();
// 0x000005E8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Single)
extern void TraceJsonWriter_WriteValue_m316AFF2E10D5EDBAC9AEF7692BD520DC8FBE2DBB ();
// 0x000005E9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Single>)
extern void TraceJsonWriter_WriteValue_m534123650001D1224B88E4027D5DF422A7A2200D ();
// 0x000005EA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Guid)
extern void TraceJsonWriter_WriteValue_m70956A7E128D9BB3BF3B8C71A571D6C7501B1B54 ();
// 0x000005EB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Guid>)
extern void TraceJsonWriter_WriteValue_m29229E50900E8ED284BD416CC534D88085DB9275 ();
// 0x000005EC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int32)
extern void TraceJsonWriter_WriteValue_mAF79997D38316EA5EB58DEEF44F6244F1EBA0E68 ();
// 0x000005ED System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int32>)
extern void TraceJsonWriter_WriteValue_m9A1C52D69533A9FDF2F646B37034FACDBDBE0BA4 ();
// 0x000005EE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int64)
extern void TraceJsonWriter_WriteValue_mFAFB8D90B902F747BFFB52F28AF330C24D331C65 ();
// 0x000005EF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int64>)
extern void TraceJsonWriter_WriteValue_mAFD3E8C98EC4795834EDB92F48EA43617FE8B472 ();
// 0x000005F0 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Object)
extern void TraceJsonWriter_WriteValue_m338E12BB33ABDA8881DC6409D526A65C406C79EC ();
// 0x000005F1 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.SByte)
extern void TraceJsonWriter_WriteValue_m9D8B92242951E588B56CDFD1C0077F9738E2495E ();
// 0x000005F2 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.SByte>)
extern void TraceJsonWriter_WriteValue_m56486D1D7BE620E964AB5A73A37736FD6B955B56 ();
// 0x000005F3 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Int16)
extern void TraceJsonWriter_WriteValue_mF12DBB84449438111F17639C647FCCE453F7C9BA ();
// 0x000005F4 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.Int16>)
extern void TraceJsonWriter_WriteValue_mA1DBC1BCF410930ECAE10F20A97D71D8BB713986 ();
// 0x000005F5 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.String)
extern void TraceJsonWriter_WriteValue_mA2902F2932109772FB3CAD6D96A5E63ADD9E5B02 ();
// 0x000005F6 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.TimeSpan)
extern void TraceJsonWriter_WriteValue_mFEB5B06854076E071FDFA290441CE6E0ECEA6373 ();
// 0x000005F7 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.TimeSpan>)
extern void TraceJsonWriter_WriteValue_mD0F5333E20F732BCE89A54F03A882182D2E55AA4 ();
// 0x000005F8 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt32)
extern void TraceJsonWriter_WriteValue_m90D42BBEA5F7C1869EC3556A1B4287B59D1F2DCA ();
// 0x000005F9 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt32>)
extern void TraceJsonWriter_WriteValue_m9070033F23876070AAA8458A7C7E382EFD4DED31 ();
// 0x000005FA System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt64)
extern void TraceJsonWriter_WriteValue_mE71AE7EDA7396C6DDEC7BA1A1998C9EC535AA3E1 ();
// 0x000005FB System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt64>)
extern void TraceJsonWriter_WriteValue_mDF8EF24DF5BCD05716C94AF5A62C3665A18DCDC7 ();
// 0x000005FC System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Uri)
extern void TraceJsonWriter_WriteValue_m9CCF2DE479431156D6E3A005C0C999B7CFC36351 ();
// 0x000005FD System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.UInt16)
extern void TraceJsonWriter_WriteValue_m0C93FB423C4A5528DBF64E0432B8AB495D1532EE ();
// 0x000005FE System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteValue(System.Nullable`1<System.UInt16>)
extern void TraceJsonWriter_WriteValue_m55941F317A98B0ACA6E39D62637FDAB461AC016F ();
// 0x000005FF System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteComment(System.String)
extern void TraceJsonWriter_WriteComment_mFF534D10C6A1730E8D9FD2676448B1BC03BFA7B4 ();
// 0x00000600 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartArray()
extern void TraceJsonWriter_WriteStartArray_m96B34217073043222888E650456126A2BB20DFCA ();
// 0x00000601 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndArray()
extern void TraceJsonWriter_WriteEndArray_m2FCE8425119B65792C43CD37986DEBD60B66D7B1 ();
// 0x00000602 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartConstructor(System.String)
extern void TraceJsonWriter_WriteStartConstructor_m710B24C732EA4757E3C2BCD55CFD20A16A0DC77E ();
// 0x00000603 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndConstructor()
extern void TraceJsonWriter_WriteEndConstructor_m159BABC2EBABE6D4DC1683FA18A4A9D2D2928AD5 ();
// 0x00000604 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String)
extern void TraceJsonWriter_WritePropertyName_mFAE312535CAF8837C14668C6931964B02841057D ();
// 0x00000605 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WritePropertyName(System.String,System.Boolean)
extern void TraceJsonWriter_WritePropertyName_m4766486E8ED9537C22434AEFF189A60D10E57F16 ();
// 0x00000606 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteStartObject()
extern void TraceJsonWriter_WriteStartObject_mE97856C576ADF3CF5EF40A8F9A0B0C8C4FA43B21 ();
// 0x00000607 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteEndObject()
extern void TraceJsonWriter_WriteEndObject_mC91B5E978AF28AB8F4FD9E26F23599C63FF69019 ();
// 0x00000608 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRawValue(System.String)
extern void TraceJsonWriter_WriteRawValue_m119558ED3D6970C3F01196F27C25F3793CF51CC0 ();
// 0x00000609 System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::WriteRaw(System.String)
extern void TraceJsonWriter_WriteRaw_m7B890E7615BEF9D440DF1ED14E46819E389EC8BB ();
// 0x0000060A System.Void Newtonsoft.Json.Serialization.TraceJsonWriter::Close()
extern void TraceJsonWriter_Close_m747E78F25F9F7DAAEF501885A4BFBEE995C7A932 ();
// 0x0000060B System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::get_ChildrenTokens()
extern void JArray_get_ChildrenTokens_mA1DEC06AEA095FC146C961608990188CB3EB33AE ();
// 0x0000060C Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JArray::get_Type()
extern void JArray_get_Type_mA33AF3317CDA9DD046B7ABB8106A8D98C876C952 ();
// 0x0000060D System.Void Newtonsoft.Json.Linq.JArray::.ctor()
extern void JArray__ctor_mBEEE73B6A0AC3D3FCA28E3B6B896453A4308920B ();
// 0x0000060E System.Void Newtonsoft.Json.Linq.JArray::.ctor(Newtonsoft.Json.Linq.JArray)
extern void JArray__ctor_m70FA99428E8D9BC5320C5FC8ED3C0D82B9D5959B ();
// 0x0000060F System.Void Newtonsoft.Json.Linq.JArray::.ctor(System.Object)
extern void JArray__ctor_mDC6B2631D37AF77521585303A4FEA6CC7260C8EB ();
// 0x00000610 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::CloneToken()
extern void JArray_CloneToken_mBD9DA3EE5F599169F7386BA54DA2E07893B385D3 ();
// 0x00000611 Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JArray::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JArray_Load_m0CD5F55F08391C0649C95B7C42EE24040300524E ();
// 0x00000612 System.Void Newtonsoft.Json.Linq.JArray::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JArray_WriteTo_mBEE123D499E5AB74F56EF55B3F1AA9A72B37FD7C ();
// 0x00000613 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JArray::get_Item(System.Int32)
extern void JArray_get_Item_m88D1B9E2605132D4701B9C88EA6C4A3A3C340A89 ();
// 0x00000614 System.Void Newtonsoft.Json.Linq.JArray::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JArray_set_Item_mA02ED51F0DDD32C5468560C513868BFE31B1ADE0 ();
// 0x00000615 System.Int32 Newtonsoft.Json.Linq.JArray::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JArray_IndexOfItem_m88E68E9F11F4E80E2AD5B6B9B8D2C2029C28671E ();
// 0x00000616 System.Int32 Newtonsoft.Json.Linq.JArray::IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JArray_IndexOf_m7C1584468BD22DF5E36B174DC2D7EEDE1AED69F7 ();
// 0x00000617 System.Void Newtonsoft.Json.Linq.JArray::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JArray_Insert_m7C82622CF143C377A7F1ACFF2AEAC9C24A262771 ();
// 0x00000618 System.Void Newtonsoft.Json.Linq.JArray::RemoveAt(System.Int32)
extern void JArray_RemoveAt_mCDEC5B28E071AF9AF6D641448153A59AA88E6642 ();
// 0x00000619 System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::GetEnumerator()
extern void JArray_GetEnumerator_mA52FC13BD541B196C067B10F75EE218611100582 ();
// 0x0000061A System.Void Newtonsoft.Json.Linq.JArray::Add(Newtonsoft.Json.Linq.JToken)
extern void JArray_Add_m7867C056B0DFEC1765A3BEA7250BF2FD701EFD55 ();
// 0x0000061B System.Void Newtonsoft.Json.Linq.JArray::Clear()
extern void JArray_Clear_m23A03D37333E6429D2F914F2870B56C20C564F51 ();
// 0x0000061C System.Boolean Newtonsoft.Json.Linq.JArray::Contains(Newtonsoft.Json.Linq.JToken)
extern void JArray_Contains_m4A9353DD9C3A8BE8881E22F80197AEF67FD6D25F ();
// 0x0000061D System.Void Newtonsoft.Json.Linq.JArray::CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JArray_CopyTo_m38EBA986D37D4B6C71F0B17DDDD919C693ABF0FC ();
// 0x0000061E System.Boolean Newtonsoft.Json.Linq.JArray::get_IsReadOnly()
extern void JArray_get_IsReadOnly_m68282E1344A5F4A0893F2B3FED274169EECD6E9B ();
// 0x0000061F System.Boolean Newtonsoft.Json.Linq.JArray::Remove(Newtonsoft.Json.Linq.JToken)
extern void JArray_Remove_m01697C0702ADE16AEFE131806240E4F37EC47317 ();
// 0x00000620 System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::get_ChildrenTokens()
extern void JConstructor_get_ChildrenTokens_m47FDDE57DA0B3ACAD231077B8BB1BE194E11782D ();
// 0x00000621 System.Int32 Newtonsoft.Json.Linq.JConstructor::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JConstructor_IndexOfItem_m7FD5AF6CCEBA0E2EDB7244D7417433F849B7433F ();
// 0x00000622 System.String Newtonsoft.Json.Linq.JConstructor::get_Name()
extern void JConstructor_get_Name_m857ED71B4469321BE4DCFCC3042E2D761B45811D ();
// 0x00000623 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JConstructor::get_Type()
extern void JConstructor_get_Type_m1B04D60143327306F8CAAC0E5FC2F18E4E679BDD ();
// 0x00000624 System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(Newtonsoft.Json.Linq.JConstructor)
extern void JConstructor__ctor_m632EE02A4470EB8A076F62B69AA3A40A41DF0E9F ();
// 0x00000625 System.Void Newtonsoft.Json.Linq.JConstructor::.ctor(System.String)
extern void JConstructor__ctor_m15C66D05844E5C74B5EA60A28C2968F9EC5E8665 ();
// 0x00000626 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JConstructor::CloneToken()
extern void JConstructor_CloneToken_mA219AB5B8ECB9CA0F5A0AF731166EF6065B0C04F ();
// 0x00000627 System.Void Newtonsoft.Json.Linq.JConstructor::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JConstructor_WriteTo_mB85D481DAFA14C0C5C4B172454CB21B0F9E715B1 ();
// 0x00000628 Newtonsoft.Json.Linq.JConstructor Newtonsoft.Json.Linq.JConstructor::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JConstructor_Load_m0B45878BFC83EEFA9DC69BD92D7B40797AA4BCEB ();
// 0x00000629 System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::get_ChildrenTokens()
// 0x0000062A System.Void Newtonsoft.Json.Linq.JContainer::.ctor()
extern void JContainer__ctor_m0CE22FBD81CF2B70971DBCE4580E1F95310DD5EA ();
// 0x0000062B System.Void Newtonsoft.Json.Linq.JContainer::.ctor(Newtonsoft.Json.Linq.JContainer)
extern void JContainer__ctor_m0902CE016622EC4AB064E0FC17D8E735AC819439 ();
// 0x0000062C System.Void Newtonsoft.Json.Linq.JContainer::CheckReentrancy()
extern void JContainer_CheckReentrancy_m97FFF3B6866D08E86DA13149D3216226946A7308 ();
// 0x0000062D System.Void Newtonsoft.Json.Linq.JContainer::OnListChanged(System.ComponentModel.ListChangedEventArgs)
extern void JContainer_OnListChanged_m43B8FA4297F83C78AF7A397DE6FFD74EFF3DCE9B ();
// 0x0000062E System.Void Newtonsoft.Json.Linq.JContainer::OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs)
extern void JContainer_OnCollectionChanged_mDBC9B5E334A2B0E055D06CB590055EB35E687BA2 ();
// 0x0000062F System.Boolean Newtonsoft.Json.Linq.JContainer::get_HasValues()
extern void JContainer_get_HasValues_m7239BD55357E09ADD283116D7CE365FA200A86CC ();
// 0x00000630 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_First()
extern void JContainer_get_First_m36D3338561F4D3D0E499648AE5E7FD9523BED6E5 ();
// 0x00000631 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::get_Last()
extern void JContainer_get_Last_m0BB4890F883946993F80DEE7F7E7B0FE8A0951B0 ();
// 0x00000632 Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer::Children()
extern void JContainer_Children_m4D17F48661D82595E64CAFB5B806EF9E000DA4FD ();
// 0x00000633 System.Boolean Newtonsoft.Json.Linq.JContainer::IsMultiContent(System.Object)
extern void JContainer_IsMultiContent_m542E4D877230B7E397956A48D020FDBF9D408196 ();
// 0x00000634 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureParentToken(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JContainer_EnsureParentToken_m0200054AC023E2480589356054EBCC9A22D2B431 ();
// 0x00000635 System.Int32 Newtonsoft.Json.Linq.JContainer::IndexOfItem(Newtonsoft.Json.Linq.JToken)
// 0x00000636 System.Void Newtonsoft.Json.Linq.JContainer::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JContainer_InsertItem_mE2FA60A9793A5707E3C8CC4F8E566FE452FFC5B7 ();
// 0x00000637 System.Void Newtonsoft.Json.Linq.JContainer::RemoveItemAt(System.Int32)
extern void JContainer_RemoveItemAt_m9695A4D6FBDA2153904170723B8C5B498DD07B52 ();
// 0x00000638 System.Boolean Newtonsoft.Json.Linq.JContainer::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern void JContainer_RemoveItem_mC31969653040EA3EB1B6A241469B56C4AE84D72C ();
// 0x00000639 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::GetItem(System.Int32)
extern void JContainer_GetItem_m67248AB90E307B18D7B543A531930F6C58800DF7 ();
// 0x0000063A System.Void Newtonsoft.Json.Linq.JContainer::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_SetItem_m21806067E0FC24F0A744D3AC9EA1C95A0DD8A8DF ();
// 0x0000063B System.Void Newtonsoft.Json.Linq.JContainer::ClearItems()
extern void JContainer_ClearItems_m60C68B6A3BE52D5B7A3E2A9246D070495D47F0A8 ();
// 0x0000063C System.Void Newtonsoft.Json.Linq.JContainer::ReplaceItem(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_ReplaceItem_mF49492E29F1DB2D449B8E1AE138D025D1BD2260C ();
// 0x0000063D System.Boolean Newtonsoft.Json.Linq.JContainer::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern void JContainer_ContainsItem_mD1CF3EFCD467598A0651A3F964DF1D0544C1EC22 ();
// 0x0000063E System.Void Newtonsoft.Json.Linq.JContainer::CopyItemsTo(System.Array,System.Int32)
extern void JContainer_CopyItemsTo_mF586911EB401EB7EFE65DCDC3F69F34BA9162FC6 ();
// 0x0000063F System.Boolean Newtonsoft.Json.Linq.JContainer::IsTokenUnchanged(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_IsTokenUnchanged_mA560534C1BEFB1FD3A74392B7FCC5B1D87151166 ();
// 0x00000640 System.Void Newtonsoft.Json.Linq.JContainer::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JContainer_ValidateToken_m3844C3AC8376D0EE7A80AF730954128054E8054B ();
// 0x00000641 System.Void Newtonsoft.Json.Linq.JContainer::Add(System.Object)
extern void JContainer_Add_mB92DF57916610ACC06BFDA6BFBE6193B9A992120 ();
// 0x00000642 System.Void Newtonsoft.Json.Linq.JContainer::AddAndSkipParentCheck(Newtonsoft.Json.Linq.JToken)
extern void JContainer_AddAndSkipParentCheck_m33E204AA42AC6FC8474C2FB6D4668147A97C22AA ();
// 0x00000643 System.Void Newtonsoft.Json.Linq.JContainer::AddInternal(System.Int32,System.Object,System.Boolean)
extern void JContainer_AddInternal_m3E51EBAF6DE85EEB5ED40A8F9B61732F3ED5B1E3 ();
// 0x00000644 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::CreateFromContent(System.Object)
extern void JContainer_CreateFromContent_m7AFCFE69DC6E3334F1EE5E9331E0F03BCAC8881B ();
// 0x00000645 System.Void Newtonsoft.Json.Linq.JContainer::RemoveAll()
extern void JContainer_RemoveAll_m8B271F31E5A44D1BF2C63322DADA656108B8619B ();
// 0x00000646 System.Void Newtonsoft.Json.Linq.JContainer::ReadTokenFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JContainer_ReadTokenFrom_mA9CC3A9D820D91A0D2C5EB59FBCF61D11051B72A ();
// 0x00000647 System.Void Newtonsoft.Json.Linq.JContainer::ReadContentFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JContainer_ReadContentFrom_mE6AECC267DB4D3D355D7FEB2D9AA5329A7227181 ();
// 0x00000648 Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JContainer::ReadProperty(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings,Newtonsoft.Json.IJsonLineInfo,Newtonsoft.Json.Linq.JContainer)
extern void JContainer_ReadProperty_m439B5DC6F3092419CBC32A44AEAF4D0194B15CD8 ();
// 0x00000649 System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_IndexOf_mEB0089063FAE4C81F47D25C0B1B176AA6A9031B6 ();
// 0x0000064A System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_Insert_m7312E90BD69267AE047819700DB955784664BBE1 ();
// 0x0000064B System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.RemoveAt(System.Int32)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_RemoveAt_m28849367105D64650CD353E873C5834331016B6D ();
// 0x0000064C Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.get_Item(System.Int32)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m1B5B440A3A5F8264A8E6AC629AA0FA338BC0AD22 ();
// 0x0000064D System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.IList<Newtonsoft.Json.Linq.JToken>.set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_set_Item_m01E4E879831C8F4D1CD2FCF08B16AB4E49A64300 ();
// 0x0000064E System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Add(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Add_m19BA521ADED9B5360DF7D554CE0EB55D75F9D741 ();
// 0x0000064F System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Clear()
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Clear_m4F940A1BC8E661763B627D3B26FDCDD5F1DE3630 ();
// 0x00000650 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Contains(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Contains_m900941D95D8B8BDF1C497C75947983CA3C73BEA7 ();
// 0x00000651 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_mFCE0DD0B2C56D0FCCAB3D4038D4F27B4A850DCBA ();
// 0x00000652 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.get_IsReadOnly()
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m6607FC73F08DFD533548A2173B28EC7CF125FC63 ();
// 0x00000653 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.Generic.ICollection<Newtonsoft.Json.Linq.JToken>.Remove(Newtonsoft.Json.Linq.JToken)
extern void JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Remove_m7181FC9E077FCEB86358DAD26E7D7B825E36F737 ();
// 0x00000654 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer::EnsureValue(System.Object)
extern void JContainer_EnsureValue_mE2A06BCF0E4A2ABCA5FA7E649AFAB3A8163C0383 ();
// 0x00000655 System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Add(System.Object)
extern void JContainer_System_Collections_IList_Add_m3F811C8EA6AAE7F25433FB0E793F0E2C0ACC2323 ();
// 0x00000656 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Clear()
extern void JContainer_System_Collections_IList_Clear_m671790420FB27D56C2A704E6E0B7BEAC274C2E3B ();
// 0x00000657 System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Contains(System.Object)
extern void JContainer_System_Collections_IList_Contains_mFE433F2BF5269EFCE2E2EAA5AB1FC7906E47B749 ();
// 0x00000658 System.Int32 Newtonsoft.Json.Linq.JContainer::System.Collections.IList.IndexOf(System.Object)
extern void JContainer_System_Collections_IList_IndexOf_m2DBECA578BFC86E12228DA99AC06FCB777FA8195 ();
// 0x00000659 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Insert(System.Int32,System.Object)
extern void JContainer_System_Collections_IList_Insert_mF51E09186B620E85EF25DA2452E8226FF947820F ();
// 0x0000065A System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsFixedSize()
extern void JContainer_System_Collections_IList_get_IsFixedSize_m59C782EF836BA52F736FB1281305AED90F8A4C93 ();
// 0x0000065B System.Boolean Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_IsReadOnly()
extern void JContainer_System_Collections_IList_get_IsReadOnly_m47F2BB96C1E31D45252910739F0935498BD01051 ();
// 0x0000065C System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.Remove(System.Object)
extern void JContainer_System_Collections_IList_Remove_mF6A47B5BD7F1A91AE538020DAFDA6DC649920BE4 ();
// 0x0000065D System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.RemoveAt(System.Int32)
extern void JContainer_System_Collections_IList_RemoveAt_m836BEB978B58245BD1C9088EF6C0954A3D87F99C ();
// 0x0000065E System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.IList.get_Item(System.Int32)
extern void JContainer_System_Collections_IList_get_Item_mAADD5FB1DBF4364008F9D7DB6460ED8945467581 ();
// 0x0000065F System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.IList.set_Item(System.Int32,System.Object)
extern void JContainer_System_Collections_IList_set_Item_m369E563AB174C21AC6E1B545AC6CC17C27A276F2 ();
// 0x00000660 System.Void Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern void JContainer_System_Collections_ICollection_CopyTo_mEA6BEAB8787C814724F977F0F3A9EE5C2F5DF401 ();
// 0x00000661 System.Int32 Newtonsoft.Json.Linq.JContainer::get_Count()
extern void JContainer_get_Count_m1C92C14323C92FF6E2AF2B8FB75B6726F67A85D6 ();
// 0x00000662 System.Object Newtonsoft.Json.Linq.JContainer::System.Collections.ICollection.get_SyncRoot()
extern void JContainer_System_Collections_ICollection_get_SyncRoot_m7D1C5F603B08522258C4AB46C538415199730FDE ();
// 0x00000663 System.Void Newtonsoft.Json.Linq.JEnumerable`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000664 System.Collections.Generic.IEnumerator`1<T> Newtonsoft.Json.Linq.JEnumerable`1::GetEnumerator()
// 0x00000665 System.Collections.IEnumerator Newtonsoft.Json.Linq.JEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000666 System.Boolean Newtonsoft.Json.Linq.JEnumerable`1::Equals(Newtonsoft.Json.Linq.JEnumerable`1<T>)
// 0x00000667 System.Boolean Newtonsoft.Json.Linq.JEnumerable`1::Equals(System.Object)
// 0x00000668 System.Int32 Newtonsoft.Json.Linq.JEnumerable`1::GetHashCode()
// 0x00000669 System.Void Newtonsoft.Json.Linq.JEnumerable`1::.cctor()
// 0x0000066A System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::get_ChildrenTokens()
extern void JObject_get_ChildrenTokens_m266186AFB3740EF26E4C04E68C75DBB3F5363B10 ();
// 0x0000066B System.Void Newtonsoft.Json.Linq.JObject::.ctor()
extern void JObject__ctor_m4ABF1922B4DECD1DA5CD12F6DC6DBDE833D17AE2 ();
// 0x0000066C System.Void Newtonsoft.Json.Linq.JObject::.ctor(Newtonsoft.Json.Linq.JObject)
extern void JObject__ctor_m1C4DD67B48A523218211C0FB069FE2229041F4CD ();
// 0x0000066D System.Int32 Newtonsoft.Json.Linq.JObject::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JObject_IndexOfItem_m24476C5FB8F85466112322D19619734CE020EE7C ();
// 0x0000066E System.Void Newtonsoft.Json.Linq.JObject::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JObject_InsertItem_m6A199B37C8324D0F3EE15C8BBD3D0DE976407E48 ();
// 0x0000066F System.Void Newtonsoft.Json.Linq.JObject::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern void JObject_ValidateToken_mCB3DDA384F247047F319CCF532E56FB87225A16D ();
// 0x00000670 System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanged(Newtonsoft.Json.Linq.JProperty)
extern void JObject_InternalPropertyChanged_m900E8C192C5C9A6523B8BD410049193C6AE77A61 ();
// 0x00000671 System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanging(Newtonsoft.Json.Linq.JProperty)
extern void JObject_InternalPropertyChanging_m5ABEEC0280B5A9234AD2C1395762CBD2885330A9 ();
// 0x00000672 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::CloneToken()
extern void JObject_CloneToken_m1333CE1C1B472305128B7439A56B9C0A24079FD8 ();
// 0x00000673 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JObject::get_Type()
extern void JObject_get_Type_mFDE5DED25A4C9FAE856EC444EEB2A49040868AE0 ();
// 0x00000674 System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.JObject::Properties()
extern void JObject_Properties_m19E8627EFB77572308C4366A5232B2A80F65C986 ();
// 0x00000675 Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject::Property(System.String,System.StringComparison)
extern void JObject_Property_m6A261A9EEE36DE60ABEFD6E65F547B6BE8994AF3 ();
// 0x00000676 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.String)
extern void JObject_get_Item_mED171D088059A4217F5BAFE7BE0F97A7CF8299CB ();
// 0x00000677 System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.String,Newtonsoft.Json.Linq.JToken)
extern void JObject_set_Item_m3A5EDDA204F4B59D144BA775168C381DD48E94EA ();
// 0x00000678 Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JObject_Load_mC67D559144E3D219A1ECB2880EBBCD84118C065C ();
// 0x00000679 Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Parse(System.String)
extern void JObject_Parse_m6EDB41DC32CFAD94BBE7DE418C7E25D02E7FB295 ();
// 0x0000067A Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Parse(System.String,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JObject_Parse_m4219C3D3A06F8084023D4FDC5CF7CB131E93C5B3 ();
// 0x0000067B System.Void Newtonsoft.Json.Linq.JObject::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JObject_WriteTo_m7AFA1E1DDEB761DEAC4CED6308FEC1C98ADD14C7 ();
// 0x0000067C System.Void Newtonsoft.Json.Linq.JObject::Add(System.String,Newtonsoft.Json.Linq.JToken)
extern void JObject_Add_m41C7BBD1E5187334172C1414ECA9C4094E42BE09 ();
// 0x0000067D System.Boolean Newtonsoft.Json.Linq.JObject::ContainsKey(System.String)
extern void JObject_ContainsKey_m45AA5603727F2847CA6D0038A3BE7B0D99DC88F5 ();
// 0x0000067E System.Boolean Newtonsoft.Json.Linq.JObject::Remove(System.String)
extern void JObject_Remove_mEB10588027E8797E1CE1A5D8170823E591E81780 ();
// 0x0000067F System.Boolean Newtonsoft.Json.Linq.JObject::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern void JObject_TryGetValue_m5C001A77D8CAC5221A6DEC0148744977C8DD254A ();
// 0x00000680 System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<System.String,Newtonsoft.Json.Linq.JToken>.get_Values()
extern void JObject_System_Collections_Generic_IDictionaryU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m11A812701B46674AF8B3C00142062EE847FCA8B0 ();
// 0x00000681 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Add(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_mBE02B2795E6BA6ECB7A7DC71EDDB1C44A8687DDB ();
// 0x00000682 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Clear()
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m7F527CD11BA1BB912D6A7504F06A334EB8ED7868 ();
// 0x00000683 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Contains(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_m50E7E5F1B63B315D19BD60A2816A30F0D0E60448 ();
// 0x00000684 System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[],System.Int32)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_mF201C14DA4208AABE4816512C0FB92726E556B56 ();
// 0x00000685 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.get_IsReadOnly()
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_m9078C5ECFC01EFDF7EA0E049EF839DC96C4E13FD ();
// 0x00000686 System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.Remove(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_m325901E55F60FF4E3742814AD8B117E20F8D9CDD ();
// 0x00000687 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JObject::GetEnumerator()
extern void JObject_GetEnumerator_m2DE962AE58C6A538CD4F0692785B71C1EB7BA944 ();
// 0x00000688 System.Void Newtonsoft.Json.Linq.JObject::OnPropertyChanged(System.String)
extern void JObject_OnPropertyChanged_m1208B15BCD54990795050C8BFB2694716F945EFC ();
// 0x00000689 System.Void Newtonsoft.Json.Linq.JObject::OnPropertyChanging(System.String)
extern void JObject_OnPropertyChanging_m3019CA4A7A5C8C5968F1DC56496F4BEEF750BA2D ();
// 0x0000068A System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m996584BF140C41116295AF81F8801A7018251796 ();
// 0x0000068B System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties(System.Attribute[])
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m7DE7DA3A5378699AE704E75C784C77FF2C4F18B5 ();
// 0x0000068C System.ComponentModel.AttributeCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetAttributes()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m280DA57DB5D19912C1266C150099E12B16C97818 ();
// 0x0000068D System.ComponentModel.TypeConverter Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetConverter()
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m5411AF02E7B42DD2A936A3B8800F9E3F7709F8D8 ();
// 0x0000068E System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetPropertyOwner(System.ComponentModel.PropertyDescriptor)
extern void JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_m7BD8F359AC1C2ADAACEB21233163A4271622243D ();
// 0x0000068F System.Dynamic.DynamicMetaObject Newtonsoft.Json.Linq.JObject::GetMetaObject(System.Linq.Expressions.Expression)
extern void JObject_GetMetaObject_m5EF46BD88344C931557E16098710C711F2C4AB52 ();
// 0x00000690 System.Collections.Generic.IEnumerable`1<System.String> Newtonsoft.Json.Linq.JObject_JObjectDynamicProxy::GetDynamicMemberNames(Newtonsoft.Json.Linq.JObject)
extern void JObjectDynamicProxy_GetDynamicMemberNames_mDB00214DE4AD76808BBFDB21898CC3BFE98F2994 ();
// 0x00000691 System.Void Newtonsoft.Json.Linq.JObject_JObjectDynamicProxy::.ctor()
extern void JObjectDynamicProxy__ctor_m741866551A0F8E9BEB331494268858FDCC185E99 ();
// 0x00000692 System.Void Newtonsoft.Json.Linq.JObject_JObjectDynamicProxy_<>c::.cctor()
extern void U3CU3Ec__cctor_m54A99D4EF53878B83E83E1337C924267C3E5CD39 ();
// 0x00000693 System.Void Newtonsoft.Json.Linq.JObject_JObjectDynamicProxy_<>c::.ctor()
extern void U3CU3Ec__ctor_m28C53F0D4555A7A0D8D078D91F734D98AD78665E ();
// 0x00000694 System.String Newtonsoft.Json.Linq.JObject_JObjectDynamicProxy_<>c::<GetDynamicMemberNames>b__2_0(Newtonsoft.Json.Linq.JProperty)
extern void U3CU3Ec_U3CGetDynamicMemberNamesU3Eb__2_0_m70A91B5055F6F7B7C4E9DFA7060D9E275EAC8B2B ();
// 0x00000695 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__63__ctor_m16597C9D193B2D7D6450BE5CEE4BF2D2D5A51F54 ();
// 0x00000696 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__63_System_IDisposable_Dispose_mE185B4358E2352B249C0EA846C9725EA3DAD82D7 ();
// 0x00000697 System.Boolean Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::MoveNext()
extern void U3CGetEnumeratorU3Ed__63_MoveNext_mD34979B2F123B5B4447C22849A233B784769342F ();
// 0x00000698 System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__63_U3CU3Em__Finally1_m6EF73DFD3776ACDF31C920388830F948FE402FA6 ();
// 0x00000699 System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,Newtonsoft.Json.Linq.JToken>>.get_Current()
extern void U3CGetEnumeratorU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_mBC037F4FAE0A23758A485E73BF980EEC0FB34413 ();
// 0x0000069A System.Void Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__63_System_Collections_IEnumerator_Reset_mCE6A51B34B51DA53D97CD220660A6F0E580AE771 ();
// 0x0000069B System.Object Newtonsoft.Json.Linq.JObject_<GetEnumerator>d__63::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__63_System_Collections_IEnumerator_get_Current_m259473560C00DF3477CBF1AD41EEE65D9403D4F5 ();
// 0x0000069C System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty::get_ChildrenTokens()
extern void JProperty_get_ChildrenTokens_m09D87A90E4B77D3F37B09A4ECB6E73D37B704E52 ();
// 0x0000069D System.String Newtonsoft.Json.Linq.JProperty::get_Name()
extern void JProperty_get_Name_mACECEEA2BC913EC667635FA76AD99BE53E7C89D7 ();
// 0x0000069E Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::get_Value()
extern void JProperty_get_Value_m42F9FEB52576F3E8A6057DA6E0047D5873F344F2 ();
// 0x0000069F System.Void Newtonsoft.Json.Linq.JProperty::set_Value(Newtonsoft.Json.Linq.JToken)
extern void JProperty_set_Value_mD1149BD291F227DBBF2B2C1CA03FB0B439499553 ();
// 0x000006A0 System.Void Newtonsoft.Json.Linq.JProperty::.ctor(Newtonsoft.Json.Linq.JProperty)
extern void JProperty__ctor_mD75DC416674FB0F2BD2809D24B46A5CEE2173867 ();
// 0x000006A1 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::GetItem(System.Int32)
extern void JProperty_GetItem_m77448BBBC7DBB0EB9556116220BAB1E72F260F2A ();
// 0x000006A2 System.Void Newtonsoft.Json.Linq.JProperty::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JProperty_SetItem_m781458D8F5B9B733A0CE1A00A786BB15A2EB88D6 ();
// 0x000006A3 System.Boolean Newtonsoft.Json.Linq.JProperty::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_RemoveItem_m4880CB8BBF529B2D4288326EE6A71FE61C2FF526 ();
// 0x000006A4 System.Void Newtonsoft.Json.Linq.JProperty::RemoveItemAt(System.Int32)
extern void JProperty_RemoveItemAt_mC13E7C708B0C32AA0B22343A3B8CCFC9FB47B7D9 ();
// 0x000006A5 System.Int32 Newtonsoft.Json.Linq.JProperty::IndexOfItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_IndexOfItem_mFF1C2D3AD09ADC3B75CB794022A705CB127868CD ();
// 0x000006A6 System.Void Newtonsoft.Json.Linq.JProperty::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken,System.Boolean)
extern void JProperty_InsertItem_mC38DE258B92B87EC85624A05EE8677BE0B362199 ();
// 0x000006A7 System.Boolean Newtonsoft.Json.Linq.JProperty::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern void JProperty_ContainsItem_m1310C45BAA88436FEC056BFE511D3599D0578C39 ();
// 0x000006A8 System.Void Newtonsoft.Json.Linq.JProperty::ClearItems()
extern void JProperty_ClearItems_mC9ECF0A83296CBEF945E11E91B8C521606D0E79B ();
// 0x000006A9 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::CloneToken()
extern void JProperty_CloneToken_m943C0D6E83D1EA96A41A82C323DA9D1C5974A56E ();
// 0x000006AA Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JProperty::get_Type()
extern void JProperty_get_Type_m956D15F01DF6162C8E27BB57A381816A1CBC5971 ();
// 0x000006AB System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String)
extern void JProperty__ctor_m5123C40415A79F32C520448416192E80E77E7D35 ();
// 0x000006AC System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String,System.Object)
extern void JProperty__ctor_m6F9A418D5CB916B387F62E91962571ABF148DB55 ();
// 0x000006AD System.Void Newtonsoft.Json.Linq.JProperty::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JProperty_WriteTo_mF13D103C02D0E60DE09797572A4D22E3A1521B84 ();
// 0x000006AE Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JProperty::Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JProperty_Load_m1F3D60673ED18836DCA4124160F8989ABE3F30E0 ();
// 0x000006AF System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty_JPropertyList::GetEnumerator()
extern void JPropertyList_GetEnumerator_m570686A9CB55EEE12CE3921F71DA4A5DAFF95654 ();
// 0x000006B0 System.Collections.IEnumerator Newtonsoft.Json.Linq.JProperty_JPropertyList::System.Collections.IEnumerable.GetEnumerator()
extern void JPropertyList_System_Collections_IEnumerable_GetEnumerator_m905FBBF8E644EC168CB0620DE1F821A2E54EF518 ();
// 0x000006B1 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Add(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Add_mE6880E09207E96382298CB55DE5C4B24F4501BA7 ();
// 0x000006B2 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Clear()
extern void JPropertyList_Clear_m53EB4CBDE6FC6BB8698C2D3052FA506836A5C2B4 ();
// 0x000006B3 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::Contains(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Contains_mFB58417165DEBD95DCF9E0F316083674C72AF1A1 ();
// 0x000006B4 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::CopyTo(Newtonsoft.Json.Linq.JToken[],System.Int32)
extern void JPropertyList_CopyTo_m757B8A194601BB3BBE26F5CA38ECA43ACCEEE662 ();
// 0x000006B5 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::Remove(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Remove_m781F2E250E34956BF834B5FF9013C4765A27884E ();
// 0x000006B6 System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList::get_Count()
extern void JPropertyList_get_Count_m6905FC2CA1E71EDB0A159F379FB246F5765FB0CF ();
// 0x000006B7 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList::get_IsReadOnly()
extern void JPropertyList_get_IsReadOnly_mFDCA32EAB8BC6F1C1803D7443720E10B91CE9149 ();
// 0x000006B8 System.Int32 Newtonsoft.Json.Linq.JProperty_JPropertyList::IndexOf(Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_IndexOf_m63D22F6AADE0900D7E7E1B1FE6D2EBADD4BFB022 ();
// 0x000006B9 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::Insert(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_Insert_mF51B149BD53A382D451D4BF5EFB8D26DE83385BC ();
// 0x000006BA System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::RemoveAt(System.Int32)
extern void JPropertyList_RemoveAt_m594E723AB7AB66AF7F1DFB789D88579B007DC720 ();
// 0x000006BB Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList::get_Item(System.Int32)
extern void JPropertyList_get_Item_m9CCDE18F8356D5FD9D42F5BEE477AFCF4A69AB2F ();
// 0x000006BC System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::set_Item(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyList_set_Item_m4D409C29FB271164BD89832B26E35BE5C869A0DB ();
// 0x000006BD System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList::.ctor()
extern void JPropertyList__ctor_m28C210433E968471FCD8BAC19B6C16DAB92D6119 ();
// 0x000006BE System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__1__ctor_m51B36DE071D6342B19695BB9803650F004966078 ();
// 0x000006BF System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__1_System_IDisposable_Dispose_m5E84033520F6941F60D25510D2F116B00C26ACFB ();
// 0x000006C0 System.Boolean Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::MoveNext()
extern void U3CGetEnumeratorU3Ed__1_MoveNext_mA7CDA7CF81C222940582519BCEAF72560A4A540F ();
// 0x000006C1 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m6062F82EA393CEA956EADC0CF10D3AFFD4EFCB9C ();
// 0x000006C2 System.Void Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_Reset_mC28B482E8836E43B79799E11F2EF4F6C5998B634 ();
// 0x000006C3 System.Object Newtonsoft.Json.Linq.JProperty_JPropertyList_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_get_Current_m48F23FBCB7E67DA86C62BF936AC1038357AC218A ();
// 0x000006C4 System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::.ctor(System.String)
extern void JPropertyDescriptor__ctor_m200C5553371BF93A3044D03EDA7AA405A666286A ();
// 0x000006C5 System.Object Newtonsoft.Json.Linq.JPropertyDescriptor::GetValue(System.Object)
extern void JPropertyDescriptor_GetValue_mFA7D468738FFE36F17D678728BE954003A01F1C8 ();
// 0x000006C6 System.Void Newtonsoft.Json.Linq.JPropertyDescriptor::SetValue(System.Object,System.Object)
extern void JPropertyDescriptor_SetValue_mC97EDC59D9C4EBD2A2D3A5769F4B848BE7169D41 ();
// 0x000006C7 System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::ShouldSerializeValue(System.Object)
extern void JPropertyDescriptor_ShouldSerializeValue_m14F03DE65139E02F6353DFF24830A1F5CF0BAF22 ();
// 0x000006C8 System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_ComponentType()
extern void JPropertyDescriptor_get_ComponentType_mEDC30D60BADC626D0C2A5F8FB617D344A49E931B ();
// 0x000006C9 System.Boolean Newtonsoft.Json.Linq.JPropertyDescriptor::get_IsReadOnly()
extern void JPropertyDescriptor_get_IsReadOnly_m9B815BCFB243D6D4244975339EF72BF331869354 ();
// 0x000006CA System.Type Newtonsoft.Json.Linq.JPropertyDescriptor::get_PropertyType()
extern void JPropertyDescriptor_get_PropertyType_m8E39AE752F53555EE5608DDF104E7AEA64E3DA71 ();
// 0x000006CB System.Int32 Newtonsoft.Json.Linq.JPropertyDescriptor::get_NameHashCode()
extern void JPropertyDescriptor_get_NameHashCode_mFD890FE80DF1DC46050503D9394EDCB85FE93853 ();
// 0x000006CC System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.ctor()
extern void JPropertyKeyedCollection__ctor_mD6CED5A5546C1C35ECF614D75D3597F6F94649B0 ();
// 0x000006CD System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::AddKey(System.String,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_AddKey_m4633882595DF3769B82CB864943699C5F266894C ();
// 0x000006CE System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::ClearItems()
extern void JPropertyKeyedCollection_ClearItems_mCCFBFEB59BC203D7646A62DC1D49AD90313C3A5B ();
// 0x000006CF System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::Contains(System.String)
extern void JPropertyKeyedCollection_Contains_mCE8E9846CC2365EE3760982EC977EC8FB319FCFC ();
// 0x000006D0 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::EnsureDictionary()
extern void JPropertyKeyedCollection_EnsureDictionary_m91B20B6E667B97A8098071FF4EB9A4A2D2ED4C59 ();
// 0x000006D1 System.String Newtonsoft.Json.Linq.JPropertyKeyedCollection::GetKeyForItem(Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_GetKeyForItem_mD178C29110AB250E23DF06F22F52D4E4891AA00D ();
// 0x000006D2 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_InsertItem_m317E2662E890BC477A95B036CA357790EE7DD509 ();
// 0x000006D3 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveItem(System.Int32)
extern void JPropertyKeyedCollection_RemoveItem_m7E4949267296B4C0C65F539C7CCCAC3FE961F5FA ();
// 0x000006D4 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::RemoveKey(System.String)
extern void JPropertyKeyedCollection_RemoveKey_mE81CDA6E4B9808D6C5A15BA71D71296EC30F5D54 ();
// 0x000006D5 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_SetItem_m20E39BE28BEC60EA0FE562B2C99D0E3EF30381D0 ();
// 0x000006D6 System.Boolean Newtonsoft.Json.Linq.JPropertyKeyedCollection::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern void JPropertyKeyedCollection_TryGetValue_m9AA5D00E2C98623E762FFC82621D59A50C317E1A ();
// 0x000006D7 System.Int32 Newtonsoft.Json.Linq.JPropertyKeyedCollection::IndexOfReference(Newtonsoft.Json.Linq.JToken)
extern void JPropertyKeyedCollection_IndexOfReference_m0B011F09010122922F2A39D1C7DDD5C5005E75A6 ();
// 0x000006D8 System.Void Newtonsoft.Json.Linq.JPropertyKeyedCollection::.cctor()
extern void JPropertyKeyedCollection__cctor_m91AF5A1BBB872088C6DE704392BD466AF09CBE8C ();
// 0x000006D9 System.Void Newtonsoft.Json.Linq.JRaw::.ctor(Newtonsoft.Json.Linq.JRaw)
extern void JRaw__ctor_mE1E29D14D63E627A4ADDC148BF47576AD6E0700C ();
// 0x000006DA System.Void Newtonsoft.Json.Linq.JRaw::.ctor(System.Object)
extern void JRaw__ctor_m2D4BD8F289A8380150173AC6CF605E3040BC7B4A ();
// 0x000006DB Newtonsoft.Json.Linq.JRaw Newtonsoft.Json.Linq.JRaw::Create(Newtonsoft.Json.JsonReader)
extern void JRaw_Create_mA3636B03EBB9ED5CB860AABABC79C3EDAC916391 ();
// 0x000006DC Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JRaw::CloneToken()
extern void JRaw_CloneToken_m1DCB2AD0423B5439D3892916590D82020E005D05 ();
// 0x000006DD Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_CommentHandling()
extern void JsonLoadSettings_get_CommentHandling_mA34D24EF3ACA88F8E09FFDA51481CADBA08C9C19 ();
// 0x000006DE Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_LineInfoHandling()
extern void JsonLoadSettings_get_LineInfoHandling_m2AD316E98970C5A7D36D677FBE4682EF32DB0C50 ();
// 0x000006DF Newtonsoft.Json.Linq.DuplicatePropertyNameHandling Newtonsoft.Json.Linq.JsonLoadSettings::get_DuplicatePropertyNameHandling()
extern void JsonLoadSettings_get_DuplicatePropertyNameHandling_m938013262617A16532A60AEA3AFBA7BA22CD4ADC ();
// 0x000006E0 Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::get_Parent()
extern void JToken_get_Parent_m063166BECB700D927D31F6A78F60F8F8B7F90D41 ();
// 0x000006E1 System.Void Newtonsoft.Json.Linq.JToken::set_Parent(Newtonsoft.Json.Linq.JContainer)
extern void JToken_set_Parent_m7BE843F90087F2D4216BAFB488101E22C8602775 ();
// 0x000006E2 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Root()
extern void JToken_get_Root_m324F6E6BB5AC879F57661922B5FEA26FB539EDB0 ();
// 0x000006E3 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::CloneToken()
// 0x000006E4 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JToken::get_Type()
// 0x000006E5 System.Boolean Newtonsoft.Json.Linq.JToken::get_HasValues()
// 0x000006E6 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Next()
extern void JToken_get_Next_m9D46389AC24A9F0C3D3B71366BEABA1C19AA9C2D ();
// 0x000006E7 System.Void Newtonsoft.Json.Linq.JToken::set_Next(Newtonsoft.Json.Linq.JToken)
extern void JToken_set_Next_mD79E073265DF73A5B6438B28E7362F8FE696817F ();
// 0x000006E8 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Previous()
extern void JToken_get_Previous_m67169B2FAE14AA628820DDFE5424C68555356A35 ();
// 0x000006E9 System.Void Newtonsoft.Json.Linq.JToken::set_Previous(Newtonsoft.Json.Linq.JToken)
extern void JToken_set_Previous_mA91AB456AC1F169F2A1D7F13842583DE4386280F ();
// 0x000006EA System.String Newtonsoft.Json.Linq.JToken::get_Path()
extern void JToken_get_Path_mD320131161693E7BEFCC4BF7E305CF3FC351EF45 ();
// 0x000006EB System.Void Newtonsoft.Json.Linq.JToken::.ctor()
extern void JToken__ctor_m9403EA4776AE598E3FF68C450C1917D3A48CE470 ();
// 0x000006EC Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_First()
extern void JToken_get_First_m14E10D2D1FA762A3FED76D4E83F7F453043026ED ();
// 0x000006ED Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Last()
extern void JToken_get_Last_m11E9366AC0C3B9D3D861767F97B21C43AEA91B90 ();
// 0x000006EE Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Children()
extern void JToken_Children_m0906256C207729201CAF8E84A7B211CA30CB948B ();
// 0x000006EF System.Void Newtonsoft.Json.Linq.JToken::Remove()
extern void JToken_Remove_mD5F9FBFAD849743FDC4F03514B707BF68ECABEED ();
// 0x000006F0 System.Void Newtonsoft.Json.Linq.JToken::Replace(Newtonsoft.Json.Linq.JToken)
extern void JToken_Replace_m67B8D9199B5F3D976FD1A7C833C4309FB0AFA408 ();
// 0x000006F1 System.Void Newtonsoft.Json.Linq.JToken::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
// 0x000006F2 System.String Newtonsoft.Json.Linq.JToken::ToString()
extern void JToken_ToString_m65C4DC88F1476746199E28B80D9B2CC720383AB5 ();
// 0x000006F3 System.String Newtonsoft.Json.Linq.JToken::ToString(Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern void JToken_ToString_m9A15F7553907AB7AF47A02D43B3F426A168162B2 ();
// 0x000006F4 Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JToken::EnsureValue(Newtonsoft.Json.Linq.JToken)
extern void JToken_EnsureValue_m40DF80803817C5D89FBCC9C59B3A97A0F9972890 ();
// 0x000006F5 System.String Newtonsoft.Json.Linq.JToken::GetType(Newtonsoft.Json.Linq.JToken)
extern void JToken_GetType_m43DA114C98D6E97AAAE4567C7EDEA8CADC838221 ();
// 0x000006F6 System.Boolean Newtonsoft.Json.Linq.JToken::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JTokenType[],System.Boolean)
extern void JToken_ValidateToken_m7CC9ED2EA3268AC496616301024BF998E67F1344 ();
// 0x000006F7 System.Boolean Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m5CB54CD44D187CAC6796C6B46094F5DA1BC7AF80 ();
// 0x000006F8 System.DateTimeOffset Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m366F02B0C9B052E9DAA91EDB904B6F9118AFFFE4 ();
// 0x000006F9 System.Nullable`1<System.Boolean> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m8109DD61856AD55360B6F2F6CABAB5DC11EACD11 ();
// 0x000006FA System.Int64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m930D65D1BDEA9B6BF39D29F0E85B43A34BD1A3B1 ();
// 0x000006FB System.Nullable`1<System.DateTime> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mECA899B405FAAB4ABBE9955C61C114A39CB8CCA8 ();
// 0x000006FC System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m20997FE5D10871851493D4E81B6DF2E69DCE2761 ();
// 0x000006FD System.Nullable`1<System.Decimal> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m87A5B257B1E369111A04D700476A80BD01961C56 ();
// 0x000006FE System.Nullable`1<System.Double> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m0F727614AF70EEDF2C237EA58B999DA29C4A8D55 ();
// 0x000006FF System.Nullable`1<System.Char> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mD02895C8CEA53E5155644CEAFCE1B809736A7863 ();
// 0x00000700 System.Int32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m46AC6C59EA789613D16025132C0437910A8B1D16 ();
// 0x00000701 System.Int16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m63641BCC1997D440114F23ECADD75482854E3439 ();
// 0x00000702 System.UInt16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mBBC97DEF7FD1AA961E72ABCC0619F6C6E8C30A95 ();
// 0x00000703 System.Char Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mA30A1EAF36028DE4E91935D9ECC7215A5954B5D7 ();
// 0x00000704 System.Byte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m7C0FB26C58AF3A305E455116822B829255FD88EB ();
// 0x00000705 System.SByte Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mAC324B94621EBF11E88D304029B4F80F9ABB3D80 ();
// 0x00000706 System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m017E4DD2890CB408CB373BF50B801C0A1746D128 ();
// 0x00000707 System.Nullable`1<System.Int16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mD72FF1B8EA3D2852FB159B18A06109433DA14ADD ();
// 0x00000708 System.Nullable`1<System.UInt16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mA902AA9D06E7AE2067E663CAA9CD3BC4CB9A6910 ();
// 0x00000709 System.Nullable`1<System.Byte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m694195C301821E99A5B019D7B7924A557BCA287A ();
// 0x0000070A System.Nullable`1<System.SByte> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m01615A483BAB4B8A8A155308FA3D1BC33F1072A7 ();
// 0x0000070B System.DateTime Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m2277C9449E8DEDE78F5A5D06AB5A681C2F71D968 ();
// 0x0000070C System.Nullable`1<System.Int64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mEAE5AA13B34BE32813B1B9AAEFD6D27CA3EF6FC9 ();
// 0x0000070D System.Nullable`1<System.Single> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mCB30AD2C793CEB5DBB143248E9FB65B2B1BCCEA8 ();
// 0x0000070E System.Decimal Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mC94107C01B3BFFB52E4B0F241D2831CA5C1FE989 ();
// 0x0000070F System.Nullable`1<System.UInt32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mEA356E24459E72FD243B0F220E585415B238ACA2 ();
// 0x00000710 System.Nullable`1<System.UInt64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m7C3D358D3A768DD6FE7E840C2BF7303CF4E7170F ();
// 0x00000711 System.Double Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mAB8B832E5DE1BC6A2076AEF3752C09D42F7E4871 ();
// 0x00000712 System.Single Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mAA344ADAE9F1A5D5569F92349C5014716B8B2DBC ();
// 0x00000713 System.String Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mDAD31C3695EC07CBBD5E983F41FB450D6E7ED2FA ();
// 0x00000714 System.UInt32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mB863015F27A343485C8199920D53B0DF4997615C ();
// 0x00000715 System.UInt64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mEF1BF2DCAE7984256D03A933626ABF996632A24F ();
// 0x00000716 System.Guid Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_mFB3DF0C0FAC44053CB7D9EF8AD2A2502E69E956E ();
// 0x00000717 System.Nullable`1<System.Guid> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m89B1E77388D7E74FD707E37B919A51D0EA37A344 ();
// 0x00000718 System.TimeSpan Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m9BD04FE9C15DB138F15D514A5524FB2119D6862A ();
// 0x00000719 System.Nullable`1<System.TimeSpan> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m9BCB18850374D7EE2C6999BD71FE4FA98A860838 ();
// 0x0000071A System.Uri Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern void JToken_op_Explicit_m97404E29535DCF8C507010342CB88468ACC19ADA ();
// 0x0000071B System.Numerics.BigInteger Newtonsoft.Json.Linq.JToken::ToBigInteger(Newtonsoft.Json.Linq.JToken)
extern void JToken_ToBigInteger_mA20791DC013DB05777DA7A81B94D649839852FBC ();
// 0x0000071C System.Nullable`1<System.Numerics.BigInteger> Newtonsoft.Json.Linq.JToken::ToBigIntegerNullable(Newtonsoft.Json.Linq.JToken)
extern void JToken_ToBigIntegerNullable_m02E6BF8C2B6A7980C44FE05A9354AC29AA11C047 ();
// 0x0000071D System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken::System.Collections.IEnumerable.GetEnumerator()
extern void JToken_System_Collections_IEnumerable_GetEnumerator_m2BC4C41C026BAFE012BD7D5206D3446FCF9312A5 ();
// 0x0000071E System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern void JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_mB6FD49260ACE2507F2263B3EE43ACDB4F5CD09AA ();
// 0x0000071F Newtonsoft.Json.JsonReader Newtonsoft.Json.Linq.JToken::CreateReader()
extern void JToken_CreateReader_mF5E2D2604521FA794AD783710F3C7FE48ADF5E24 ();
// 0x00000720 System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type)
extern void JToken_ToObject_mD0513627B512DAE9B80A3E75F654AA29E8DF9A34 ();
// 0x00000721 System.Object Newtonsoft.Json.Linq.JToken::ToObject(System.Type,Newtonsoft.Json.JsonSerializer)
extern void JToken_ToObject_m0559DA0AB5DD6E95C434B70CFCC99383AD174344 ();
// 0x00000722 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader)
extern void JToken_ReadFrom_m2A0A1874821741EAC0C587028591A8657290ADDC ();
// 0x00000723 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JToken_ReadFrom_mB8D8177BFE01FC0692070171D79BC9389F8BABD4 ();
// 0x00000724 System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(Newtonsoft.Json.IJsonLineInfo,Newtonsoft.Json.Linq.JsonLoadSettings)
extern void JToken_SetLineInfo_mBA60584A609A591D4FF6F0A56631EE447F7DB6C0 ();
// 0x00000725 System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(System.Int32,System.Int32)
extern void JToken_SetLineInfo_m81271DAA0C8D4E0C6C3FD0EFBF00BCB10EA2BE16 ();
// 0x00000726 System.Boolean Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mA238457EAAF4973A3DFA8FCDF8998C4BCD1618AF ();
// 0x00000727 System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m04944C7797AC592DD4D579848AA955951EC43C4B ();
// 0x00000728 System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m7DFD8260A8102C3C51D795E263B9E9F2D4B1D9A3 ();
// 0x00000729 System.Dynamic.DynamicMetaObject Newtonsoft.Json.Linq.JToken::GetMetaObject(System.Linq.Expressions.Expression)
extern void JToken_GetMetaObject_m3E1F0054A769B223FA721A48D7A4C0F2CC713683 ();
// 0x0000072A System.Dynamic.DynamicMetaObject Newtonsoft.Json.Linq.JToken::System.Dynamic.IDynamicMetaObjectProvider.GetMetaObject(System.Linq.Expressions.Expression)
extern void JToken_System_Dynamic_IDynamicMetaObjectProvider_GetMetaObject_m11CF558F49FD96B6479650225EA134BCC44039AB ();
// 0x0000072B System.Object Newtonsoft.Json.Linq.JToken::System.ICloneable.Clone()
extern void JToken_System_ICloneable_Clone_m9F5B8EED736A0AA6AB066268C8E249BC4FB5A877 ();
// 0x0000072C Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::DeepClone()
extern void JToken_DeepClone_m2EAC0C9B86BE2DD02DAB06B05677E99C2C72EECA ();
// 0x0000072D System.Void Newtonsoft.Json.Linq.JToken::AddAnnotation(System.Object)
extern void JToken_AddAnnotation_m6D27445CACC7FFA8223956207997DBCDC8D13321 ();
// 0x0000072E T Newtonsoft.Json.Linq.JToken::Annotation()
// 0x0000072F System.Void Newtonsoft.Json.Linq.JToken::.cctor()
extern void JToken__cctor_m7C212A58CB853C24BF505C888BEF330267E233C5 ();
// 0x00000730 System.Void Newtonsoft.Json.Linq.JToken_LineInfoAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoAnnotation__ctor_m145919F96CF15FAE402041EC35504B71A72CAD81 ();
// 0x00000731 Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::get_CurrentToken()
extern void JTokenReader_get_CurrentToken_m7A9ECD6E7ED51C9C138755D48C132EA7D923AEBA ();
// 0x00000732 System.Void Newtonsoft.Json.Linq.JTokenReader::.ctor(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader__ctor_mC79ED5F26C7EB260A81A781EFE747FE0429A66BC ();
// 0x00000733 System.Boolean Newtonsoft.Json.Linq.JTokenReader::Read()
extern void JTokenReader_Read_m13B825CA699C4269C24E128E70867207C3A1A471 ();
// 0x00000734 System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadOver(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader_ReadOver_m1FAC2900B7357F6B76A22A45B97826176F8FCB20 ();
// 0x00000735 System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadToEnd()
extern void JTokenReader_ReadToEnd_m680FD1C7618F0F2242B0B62D5CE5B7FDD438AE54 ();
// 0x00000736 System.Nullable`1<Newtonsoft.Json.JsonToken> Newtonsoft.Json.Linq.JTokenReader::GetEndToken(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_GetEndToken_mCBAAA95284EAD4529680668714CAC5B6D2F18DC7 ();
// 0x00000737 System.Boolean Newtonsoft.Json.Linq.JTokenReader::ReadInto(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_ReadInto_mF45C69C1C9B3F157CB6B8267007C4C479FEC28E4 ();
// 0x00000738 System.Boolean Newtonsoft.Json.Linq.JTokenReader::SetEnd(Newtonsoft.Json.Linq.JContainer)
extern void JTokenReader_SetEnd_m1DD15D1D2F364BE07648B908F384C6B8716CB9CC ();
// 0x00000739 System.Void Newtonsoft.Json.Linq.JTokenReader::SetToken(Newtonsoft.Json.Linq.JToken)
extern void JTokenReader_SetToken_m286990EC9E759D4F040C486FB1FF8D5931DD1FF4 ();
// 0x0000073A System.String Newtonsoft.Json.Linq.JTokenReader::SafeToString(System.Object)
extern void JTokenReader_SafeToString_mEF4CB8D66CCA84EC6FF30B299DA51499F15296B9 ();
// 0x0000073B System.Boolean Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mF4DDA65277F7DC3D365EFC6108B06124DE53056B ();
// 0x0000073C System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m7771C266081518DBD3EE5091E19211E8C861DD93 ();
// 0x0000073D System.Int32 Newtonsoft.Json.Linq.JTokenReader::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern void JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m9B4511CBE4BED33FD35C129BE3A73EF0A8F894E9 ();
// 0x0000073E System.String Newtonsoft.Json.Linq.JTokenReader::get_Path()
extern void JTokenReader_get_Path_m603F641AF06490A280060CB10BCD442CB39C4E6B ();
// 0x0000073F Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::get_Token()
extern void JTokenWriter_get_Token_m0595D2D38CD2872664C84C1B30FDC8C98CD406DF ();
// 0x00000740 System.Void Newtonsoft.Json.Linq.JTokenWriter::.ctor()
extern void JTokenWriter__ctor_m045384937D8A1410E45006A1805C51E8C90FA5BA ();
// 0x00000741 System.Void Newtonsoft.Json.Linq.JTokenWriter::Close()
extern void JTokenWriter_Close_mA2DE46AB7DDA9689956A9949BC9D1A73192B9A55 ();
// 0x00000742 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartObject()
extern void JTokenWriter_WriteStartObject_m844D589B418130ACB879FB57C8F795F1BFC14513 ();
// 0x00000743 System.Void Newtonsoft.Json.Linq.JTokenWriter::AddParent(Newtonsoft.Json.Linq.JContainer)
extern void JTokenWriter_AddParent_mE14EF88D72C8E89698ACB462C135AE18CE4B49F3 ();
// 0x00000744 System.Void Newtonsoft.Json.Linq.JTokenWriter::RemoveParent()
extern void JTokenWriter_RemoveParent_mE07C63592E7D0A84FFADF78D9E57362D27519B39 ();
// 0x00000745 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartArray()
extern void JTokenWriter_WriteStartArray_mFFDBE68C3F1CCAABA9DD9603B87A05CB94AD40E5 ();
// 0x00000746 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteStartConstructor(System.String)
extern void JTokenWriter_WriteStartConstructor_mA8738134CD98535FD180CF628A4D354ECDE8614A ();
// 0x00000747 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteEnd(Newtonsoft.Json.JsonToken)
extern void JTokenWriter_WriteEnd_mD38539ABE993D2F3D6DEE0BB831C81F8901CE121 ();
// 0x00000748 System.Void Newtonsoft.Json.Linq.JTokenWriter::WritePropertyName(System.String)
extern void JTokenWriter_WritePropertyName_mF11C52C6BF1BD6BFE058882DDDC86A0C2872B752 ();
// 0x00000749 System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(System.Object,Newtonsoft.Json.JsonToken)
extern void JTokenWriter_AddValue_m8BCA668F22D86503D27D98FAEF74C1373B8C526D ();
// 0x0000074A System.Void Newtonsoft.Json.Linq.JTokenWriter::AddValue(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.JsonToken)
extern void JTokenWriter_AddValue_mFF1435E6EF1D3278E213D601D6AE00F54D6C1BBF ();
// 0x0000074B System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Object)
extern void JTokenWriter_WriteValue_mE842786471A1CBC363F58E995432FF95A0E6F92A ();
// 0x0000074C System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteNull()
extern void JTokenWriter_WriteNull_m6248EE783616A554E0EB79424E7D795CF0B65E06 ();
// 0x0000074D System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteUndefined()
extern void JTokenWriter_WriteUndefined_m10A46B5E18D8AD5D49409FF2B7EABFF9C8562327 ();
// 0x0000074E System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteRaw(System.String)
extern void JTokenWriter_WriteRaw_m3EDA379DC0C7E7D088521C6790E0FFA89362AFEF ();
// 0x0000074F System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteComment(System.String)
extern void JTokenWriter_WriteComment_m5541A1C1F6001C10F2DE23965993601F72E2E309 ();
// 0x00000750 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.String)
extern void JTokenWriter_WriteValue_m0F6AEC99F10EA6CDEA0D34635E9BBC568FF288F3 ();
// 0x00000751 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int32)
extern void JTokenWriter_WriteValue_m818C778F26340E550814507E36207F65DDDA1CC9 ();
// 0x00000752 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt32)
extern void JTokenWriter_WriteValue_mDC294D4566485D708F26B2AEB02F281A33308CA3 ();
// 0x00000753 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int64)
extern void JTokenWriter_WriteValue_mC558E2C21A46ACF57A9EA3D393CD0F9E376D0633 ();
// 0x00000754 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt64)
extern void JTokenWriter_WriteValue_mB65910925D4C6608998A1D728C19C326AF103C9C ();
// 0x00000755 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Single)
extern void JTokenWriter_WriteValue_mFA4F52EEEBD7523076F3FE7A1FC74F8CDDEF6A81 ();
// 0x00000756 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Double)
extern void JTokenWriter_WriteValue_mE058D1D3D39CE72E334F7872FF880B06773821C4 ();
// 0x00000757 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Boolean)
extern void JTokenWriter_WriteValue_m262F998BA6D8CD4F8472EBC6006AE5B2F5FAD153 ();
// 0x00000758 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Int16)
extern void JTokenWriter_WriteValue_m4325EA3D208AB4D15F57AA4C1397FBAB5D22AB4A ();
// 0x00000759 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.UInt16)
extern void JTokenWriter_WriteValue_mA3023B66ADFD019AB022493529C44642CCC37517 ();
// 0x0000075A System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Char)
extern void JTokenWriter_WriteValue_mDB2D6C1C875F27A855B14AE4C9136D4BE7E67277 ();
// 0x0000075B System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte)
extern void JTokenWriter_WriteValue_m36F301C60E6F7600F5524CB2A2124E98F4CF4456 ();
// 0x0000075C System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.SByte)
extern void JTokenWriter_WriteValue_m6ADB79611A15AB8ECF865AFF5F7CAF8CA77649A0 ();
// 0x0000075D System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Decimal)
extern void JTokenWriter_WriteValue_mCB5364B7DED79821AD8F5AB1D073DDEFCFA8098C ();
// 0x0000075E System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTime)
extern void JTokenWriter_WriteValue_m7159AF0551A51895651946FDA37A677E88E61F28 ();
// 0x0000075F System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.DateTimeOffset)
extern void JTokenWriter_WriteValue_mD4C8B642325090765E261FC36176D7BBD30F2EEF ();
// 0x00000760 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Byte[])
extern void JTokenWriter_WriteValue_m5ECAA5DDB7967032C1ABB8EF3A11AD56E42E34A6 ();
// 0x00000761 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.TimeSpan)
extern void JTokenWriter_WriteValue_m21AC556B849CEE72A6F3D0B62FD8879221858A57 ();
// 0x00000762 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Guid)
extern void JTokenWriter_WriteValue_m4A8A33E56DD2D2A33C22C5F5808722A90CF33EBE ();
// 0x00000763 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteValue(System.Uri)
extern void JTokenWriter_WriteValue_mB366A01142928039DE56A73A758F4BB4C79C70E4 ();
// 0x00000764 System.Void Newtonsoft.Json.Linq.JTokenWriter::WriteToken(Newtonsoft.Json.JsonReader,System.Boolean,System.Boolean,System.Boolean)
extern void JTokenWriter_WriteToken_mFB95B232D2B03CA7B750C82223D067285A5172D0 ();
// 0x00000765 System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object,Newtonsoft.Json.Linq.JTokenType)
extern void JValue__ctor_m8007B375443A338098CC8864A47D0B6B1CF75F0D ();
// 0x00000766 System.Void Newtonsoft.Json.Linq.JValue::.ctor(Newtonsoft.Json.Linq.JValue)
extern void JValue__ctor_m6EAFA8228885869EA5B375A1932316BC7FF4DD4E ();
// 0x00000767 System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object)
extern void JValue__ctor_m5A507F5548216C06142A53841A9823944817E295 ();
// 0x00000768 System.Boolean Newtonsoft.Json.Linq.JValue::get_HasValues()
extern void JValue_get_HasValues_m5D34C1DB0F79DCBC720E2699A71CCC02C2745687 ();
// 0x00000769 System.Int32 Newtonsoft.Json.Linq.JValue::CompareBigInteger(System.Numerics.BigInteger,System.Object)
extern void JValue_CompareBigInteger_mD808C213154C477942A94C0DF3A146B7AE79D1D9 ();
// 0x0000076A System.Int32 Newtonsoft.Json.Linq.JValue::Compare(Newtonsoft.Json.Linq.JTokenType,System.Object,System.Object)
extern void JValue_Compare_m1237219402299717A0A4A72C017D9ECEB1F8CFE8 ();
// 0x0000076B System.Int32 Newtonsoft.Json.Linq.JValue::CompareFloat(System.Object,System.Object)
extern void JValue_CompareFloat_m8E99A0B189B65DDB2DB29A0EF80C746D97116925 ();
// 0x0000076C Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JValue::CloneToken()
extern void JValue_CloneToken_m2278A8BF115960CE65B4DE48713B8B14F8C2DA68 ();
// 0x0000076D Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateComment(System.String)
extern void JValue_CreateComment_m4E96B9D2D9E9BB03B7F1F1A1F2A09CE15850E04E ();
// 0x0000076E Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateNull()
extern void JValue_CreateNull_m7946A1C3AAC6B695B010883B4DA5A20252BE90D2 ();
// 0x0000076F Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateUndefined()
extern void JValue_CreateUndefined_mC97FAFB0E146D858B42CF708C19EE6E74C55D0A9 ();
// 0x00000770 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>,System.Object)
extern void JValue_GetValueType_m113E2D72866AA4B3E4A2D6996EE0ABABD2C828BE ();
// 0x00000771 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetStringValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>)
extern void JValue_GetStringValueType_mF60A5C1B5C6EF5493D98D0DDC2F69521588E3025 ();
// 0x00000772 Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::get_Type()
extern void JValue_get_Type_mABF6475D3326E13103F2E18DC3D886BE04297795 ();
// 0x00000773 System.Object Newtonsoft.Json.Linq.JValue::get_Value()
extern void JValue_get_Value_mCBF2B0ABEE8EAB2605E7E035114875A1FDF14A2C ();
// 0x00000774 System.Void Newtonsoft.Json.Linq.JValue::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern void JValue_WriteTo_m2B6FDACB3570278849002223FCF6C377C4C7C1D6 ();
// 0x00000775 System.Boolean Newtonsoft.Json.Linq.JValue::ValuesEquals(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.Linq.JValue)
extern void JValue_ValuesEquals_m9A992ED7E337788D1952B91F3390D206F3F8AC1A ();
// 0x00000776 System.Boolean Newtonsoft.Json.Linq.JValue::Equals(Newtonsoft.Json.Linq.JValue)
extern void JValue_Equals_mCE10C9076BA72D10635EDF5071FAF5D7FD47C7E1 ();
// 0x00000777 System.Boolean Newtonsoft.Json.Linq.JValue::Equals(System.Object)
extern void JValue_Equals_m83626462DDEC2166569220F304A2A752F15CE942 ();
// 0x00000778 System.Int32 Newtonsoft.Json.Linq.JValue::GetHashCode()
extern void JValue_GetHashCode_m1CAC729387B43AEDAA2D0907D0503ED6F981F2BA ();
// 0x00000779 System.String Newtonsoft.Json.Linq.JValue::ToString()
extern void JValue_ToString_m3896D883303028D76C5841C368B2D0365B3B95EC ();
// 0x0000077A System.String Newtonsoft.Json.Linq.JValue::ToString(System.IFormatProvider)
extern void JValue_ToString_m7CFC016396D6601002AFE7FB7C16CF82A546A3A5 ();
// 0x0000077B System.String Newtonsoft.Json.Linq.JValue::ToString(System.String,System.IFormatProvider)
extern void JValue_ToString_mF7AD4373341218788213830E69F740D0679F3969 ();
// 0x0000077C System.Dynamic.DynamicMetaObject Newtonsoft.Json.Linq.JValue::GetMetaObject(System.Linq.Expressions.Expression)
extern void JValue_GetMetaObject_m40EAB6EF513B04E25695FBB8DFA8FA5493AC66AA ();
// 0x0000077D System.Int32 Newtonsoft.Json.Linq.JValue::System.IComparable.CompareTo(System.Object)
extern void JValue_System_IComparable_CompareTo_m9DBAEB3154195AC829C354449BDC5036D2161509 ();
// 0x0000077E System.Int32 Newtonsoft.Json.Linq.JValue::CompareTo(Newtonsoft.Json.Linq.JValue)
extern void JValue_CompareTo_m976EA46743620E9FBA671C79D9148A55AFC5B17C ();
// 0x0000077F System.TypeCode Newtonsoft.Json.Linq.JValue::System.IConvertible.GetTypeCode()
extern void JValue_System_IConvertible_GetTypeCode_m887796C18EBCF0428E82C15E9E9D4EE7E738FC07 ();
// 0x00000780 System.Boolean Newtonsoft.Json.Linq.JValue::System.IConvertible.ToBoolean(System.IFormatProvider)
extern void JValue_System_IConvertible_ToBoolean_m1B389C8084B13D948582F7DCCB756E0A9EB04A12 ();
// 0x00000781 System.Char Newtonsoft.Json.Linq.JValue::System.IConvertible.ToChar(System.IFormatProvider)
extern void JValue_System_IConvertible_ToChar_m88C9AA3F35775A0BCAAEEBA617899166C6E22BE1 ();
// 0x00000782 System.SByte Newtonsoft.Json.Linq.JValue::System.IConvertible.ToSByte(System.IFormatProvider)
extern void JValue_System_IConvertible_ToSByte_m459F451FD1A1B217A5FB6CACABD38F52176F3932 ();
// 0x00000783 System.Byte Newtonsoft.Json.Linq.JValue::System.IConvertible.ToByte(System.IFormatProvider)
extern void JValue_System_IConvertible_ToByte_mE312081EBD0C6BC6F3EEE28D5FB1C0465066A4D2 ();
// 0x00000784 System.Int16 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt16(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt16_m57E642BBFE858D9A676E68FB7105171263F018D0 ();
// 0x00000785 System.UInt16 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt16(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt16_m0EAEA48C942D2886D5A901BAE4A2CD7059792090 ();
// 0x00000786 System.Int32 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt32(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt32_mD62FE733ABD4504D633F479AEE3BEFA6A12608B2 ();
// 0x00000787 System.UInt32 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt32(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt32_mE6209F3A7D0BEAAAC37D92A3652D6F5CC4974997 ();
// 0x00000788 System.Int64 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToInt64(System.IFormatProvider)
extern void JValue_System_IConvertible_ToInt64_mBCD9FC80F23BE666D5648D20225DC3268547FFA2 ();
// 0x00000789 System.UInt64 Newtonsoft.Json.Linq.JValue::System.IConvertible.ToUInt64(System.IFormatProvider)
extern void JValue_System_IConvertible_ToUInt64_mE48A6AA9C3BD5FE13F22C427539425AF10A6A211 ();
// 0x0000078A System.Single Newtonsoft.Json.Linq.JValue::System.IConvertible.ToSingle(System.IFormatProvider)
extern void JValue_System_IConvertible_ToSingle_mD320FDFEBFBCD1B6122173BA43CBF557094A7857 ();
// 0x0000078B System.Double Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDouble(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDouble_mFCE10AF219790E54315A6734C7EB267193D10BB6 ();
// 0x0000078C System.Decimal Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDecimal(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDecimal_mF214F40F9909FC476F53336B000E9AFA1A1C6AA0 ();
// 0x0000078D System.DateTime Newtonsoft.Json.Linq.JValue::System.IConvertible.ToDateTime(System.IFormatProvider)
extern void JValue_System_IConvertible_ToDateTime_m37A998314995CFD14BD6C26111F85FCAEB1F3FE7 ();
// 0x0000078E System.Object Newtonsoft.Json.Linq.JValue::System.IConvertible.ToType(System.Type,System.IFormatProvider)
extern void JValue_System_IConvertible_ToType_m9B10B361696D641B4321B2CFBBFC6D089676021E ();
// 0x0000078F System.Void Newtonsoft.Json.Linq.JValue_JValueDynamicProxy::.ctor()
extern void JValueDynamicProxy__ctor_m238373055C135315DA9F1BD625A68C2FF3C87A7C ();
// 0x00000790 System.Void Newtonsoft.Json.Converters.BinaryConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_WriteJson_m96D53B374E75504FE23BB90B198C2384357C70E0 ();
// 0x00000791 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::GetByteArray(System.Object)
extern void BinaryConverter_GetByteArray_m40B78C5DC2EF4A6776B452B89EBDA82CB413DD43 ();
// 0x00000792 System.Void Newtonsoft.Json.Converters.BinaryConverter::EnsureReflectionObject(System.Type)
extern void BinaryConverter_EnsureReflectionObject_mD7E2802E108C70073A6ED836E7F8C67A23CD7520 ();
// 0x00000793 System.Object Newtonsoft.Json.Converters.BinaryConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BinaryConverter_ReadJson_m52DC3119DF13E8970288536E4687955CC82E695C ();
// 0x00000794 System.Byte[] Newtonsoft.Json.Converters.BinaryConverter::ReadByteArray(Newtonsoft.Json.JsonReader)
extern void BinaryConverter_ReadByteArray_mC6AC543CA4348F7B319D5142FC58E6C2A4811D21 ();
// 0x00000795 System.Boolean Newtonsoft.Json.Converters.BinaryConverter::CanConvert(System.Type)
extern void BinaryConverter_CanConvert_mE4044330144C80BA45CBFA3F9180E0F31F679C97 ();
// 0x00000796 System.Void Newtonsoft.Json.Converters.BinaryConverter::.ctor()
extern void BinaryConverter__ctor_m3A7F6F6DE299919309EC69F91E2726471CD8D388 ();
// 0x00000797 System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BsonObjectIdConverter_WriteJson_m88E58F971EACB92570EDDB004C4C516D8F3FFEA9 ();
// 0x00000798 System.Object Newtonsoft.Json.Converters.BsonObjectIdConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void BsonObjectIdConverter_ReadJson_m5B77B0AD68060923A99A30770B698673A473B956 ();
// 0x00000799 System.Boolean Newtonsoft.Json.Converters.BsonObjectIdConverter::CanConvert(System.Type)
extern void BsonObjectIdConverter_CanConvert_m1046BCCF1CA60D89029F9AEFAEAA9BD318729135 ();
// 0x0000079A System.Void Newtonsoft.Json.Converters.BsonObjectIdConverter::.ctor()
extern void BsonObjectIdConverter__ctor_m3E94339F463D9EF1D0B9B0F2BAC1E8DEA14C84AC ();
// 0x0000079B System.Void Newtonsoft.Json.Converters.DataSetConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataSetConverter_WriteJson_m07EDC41B3391B7C969D776870A3EEC8473A7A72E ();
// 0x0000079C System.Object Newtonsoft.Json.Converters.DataSetConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataSetConverter_ReadJson_m10CD5F381F0460B81DC532BCBC05A98802B02569 ();
// 0x0000079D System.Boolean Newtonsoft.Json.Converters.DataSetConverter::CanConvert(System.Type)
extern void DataSetConverter_CanConvert_m3F55E4B7948CDB10F845772B18CCF7A82EA3306B ();
// 0x0000079E System.Void Newtonsoft.Json.Converters.DataSetConverter::.ctor()
extern void DataSetConverter__ctor_m3110CEB8B8FDA3DF51D31E84BAB23C5DB6784E57 ();
// 0x0000079F System.Void Newtonsoft.Json.Converters.DataTableConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_WriteJson_m64B51B7CE0956A7BADD5B9FBB1A0049C0337E04F ();
// 0x000007A0 System.Object Newtonsoft.Json.Converters.DataTableConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_ReadJson_mDC27E7FD57CFEB5FF6D741D53251D53D16D88CAA ();
// 0x000007A1 System.Void Newtonsoft.Json.Converters.DataTableConverter::CreateRow(Newtonsoft.Json.JsonReader,System.Data.DataTable,Newtonsoft.Json.JsonSerializer)
extern void DataTableConverter_CreateRow_m0E0331C44F177771FEB4036D02116C69A7E6A458 ();
// 0x000007A2 System.Type Newtonsoft.Json.Converters.DataTableConverter::GetColumnDataType(Newtonsoft.Json.JsonReader)
extern void DataTableConverter_GetColumnDataType_mB11B7E47B858A84A93A67A3EC7053676D313161A ();
// 0x000007A3 System.Boolean Newtonsoft.Json.Converters.DataTableConverter::CanConvert(System.Type)
extern void DataTableConverter_CanConvert_m5F49A7656F227654BF2B45FE3753AB5DA6AC7352 ();
// 0x000007A4 System.Void Newtonsoft.Json.Converters.DataTableConverter::.ctor()
extern void DataTableConverter__ctor_m7BBD33D0536A71B4EEC258000FF42C069D0D6127 ();
// 0x000007A5 System.Type Newtonsoft.Json.Converters.DiscriminatedUnionConverter::CreateUnionTypeLookup(System.Type)
extern void DiscriminatedUnionConverter_CreateUnionTypeLookup_m01FE5176A4BAFFFD53B21C8EDA7377F61DB2EF5E ();
// 0x000007A6 Newtonsoft.Json.Converters.DiscriminatedUnionConverter_Union Newtonsoft.Json.Converters.DiscriminatedUnionConverter::CreateUnion(System.Type)
extern void DiscriminatedUnionConverter_CreateUnion_m4A4FB0851D1E2E85CDE08495A926055E98B0C574 ();
// 0x000007A7 System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DiscriminatedUnionConverter_WriteJson_mFB032F950A72B80E2F48F5DCF7B41C737370B57A ();
// 0x000007A8 System.Object Newtonsoft.Json.Converters.DiscriminatedUnionConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void DiscriminatedUnionConverter_ReadJson_m82C3F7B22CF5F829F775FA284332FB96D10AA48B ();
// 0x000007A9 System.Boolean Newtonsoft.Json.Converters.DiscriminatedUnionConverter::CanConvert(System.Type)
extern void DiscriminatedUnionConverter_CanConvert_m0C65C9145C42CA4F73AF20AB6FD1463FC3D95069 ();
// 0x000007AA System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter::.ctor()
extern void DiscriminatedUnionConverter__ctor_m04CA04F2F08B535E99AD01D274E720264B88F700 ();
// 0x000007AB System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter::.cctor()
extern void DiscriminatedUnionConverter__cctor_m6636CE0D46B4C0E2F446C86A2FA3F0E698DA50B0 ();
// 0x000007AC System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter_Union::.ctor(Newtonsoft.Json.Utilities.FSharpFunction,System.Collections.Generic.List`1<Newtonsoft.Json.Converters.DiscriminatedUnionConverter_UnionCase>)
extern void Union__ctor_mB5C7E2E3BC1FB55007A4921DB8158820E3695486 ();
// 0x000007AD System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter_UnionCase::.ctor(System.Int32,System.String,System.Reflection.PropertyInfo[],Newtonsoft.Json.Utilities.FSharpFunction,Newtonsoft.Json.Utilities.FSharpFunction)
extern void UnionCase__ctor_m58A7EACC5C89C2C568A5F82B740B78F87C0299BF ();
// 0x000007AE System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m355E0384D5F20FDFD86C4C8B0C61F0CBCCD72591 ();
// 0x000007AF System.Boolean Newtonsoft.Json.Converters.DiscriminatedUnionConverter_<>c__DisplayClass8_0::<WriteJson>b__0(Newtonsoft.Json.Converters.DiscriminatedUnionConverter_UnionCase)
extern void U3CU3Ec__DisplayClass8_0_U3CWriteJsonU3Eb__0_mF336A4857B509750C6687767019BD5BD991668C0 ();
// 0x000007B0 System.Void Newtonsoft.Json.Converters.DiscriminatedUnionConverter_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mCED67CFDFE4DD725377FFB7A9AB3987CF91D852E ();
// 0x000007B1 System.Boolean Newtonsoft.Json.Converters.DiscriminatedUnionConverter_<>c__DisplayClass9_0::<ReadJson>b__0(Newtonsoft.Json.Converters.DiscriminatedUnionConverter_UnionCase)
extern void U3CU3Ec__DisplayClass9_0_U3CReadJsonU3Eb__0_mC95972DA98D50540757E5A11E9678E6CA6E3B383 ();
// 0x000007B2 System.Void Newtonsoft.Json.Converters.EntityKeyMemberConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void EntityKeyMemberConverter_WriteJson_mB15BD4945525B4F185DD1D899E7446FCDCA73278 ();
// 0x000007B3 System.Void Newtonsoft.Json.Converters.EntityKeyMemberConverter::ReadAndAssertProperty(Newtonsoft.Json.JsonReader,System.String)
extern void EntityKeyMemberConverter_ReadAndAssertProperty_mC0F3CED5D745008AE2836FBA5EE9F78F0F2E9BD5 ();
// 0x000007B4 System.Object Newtonsoft.Json.Converters.EntityKeyMemberConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void EntityKeyMemberConverter_ReadJson_m37B626017F05971AC4F3D9DED61764FFE8A06C64 ();
// 0x000007B5 System.Void Newtonsoft.Json.Converters.EntityKeyMemberConverter::EnsureReflectionObject(System.Type)
extern void EntityKeyMemberConverter_EnsureReflectionObject_m77BCE52B285964B4C08226A11029789CB6C4FD48 ();
// 0x000007B6 System.Boolean Newtonsoft.Json.Converters.EntityKeyMemberConverter::CanConvert(System.Type)
extern void EntityKeyMemberConverter_CanConvert_m6A19BCF9D35326A1030F049BAF5CA75AFF255110 ();
// 0x000007B7 System.Void Newtonsoft.Json.Converters.EntityKeyMemberConverter::.ctor()
extern void EntityKeyMemberConverter__ctor_mAE71F62BD056308A33ECD1DA938EE8FF4812963F ();
// 0x000007B8 System.Void Newtonsoft.Json.Converters.ExpandoObjectConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ExpandoObjectConverter_WriteJson_m1FC6FE51B4FF58F116CF03689AA6D0389D0F8A6F ();
// 0x000007B9 System.Object Newtonsoft.Json.Converters.ExpandoObjectConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void ExpandoObjectConverter_ReadJson_m2D65EFF3EDE736B0C286D1B1B7AAC246AC090E1D ();
// 0x000007BA System.Object Newtonsoft.Json.Converters.ExpandoObjectConverter::ReadValue(Newtonsoft.Json.JsonReader)
extern void ExpandoObjectConverter_ReadValue_m5226D79D83DB64E215C5187926B4D7A4E2DABBE8 ();
// 0x000007BB System.Object Newtonsoft.Json.Converters.ExpandoObjectConverter::ReadList(Newtonsoft.Json.JsonReader)
extern void ExpandoObjectConverter_ReadList_m08B57D48D091B3F985D56E2A2D74FDF0F23B1297 ();
// 0x000007BC System.Object Newtonsoft.Json.Converters.ExpandoObjectConverter::ReadObject(Newtonsoft.Json.JsonReader)
extern void ExpandoObjectConverter_ReadObject_m410F1681824221BB27A3904A052B2BF5F066B8D5 ();
// 0x000007BD System.Boolean Newtonsoft.Json.Converters.ExpandoObjectConverter::CanConvert(System.Type)
extern void ExpandoObjectConverter_CanConvert_m268D59B965D2800581FE08AE4EDA82A9BFDDEC50 ();
// 0x000007BE System.Boolean Newtonsoft.Json.Converters.ExpandoObjectConverter::get_CanWrite()
extern void ExpandoObjectConverter_get_CanWrite_mEF962CA95A656ACF3E6142D5D42C6F6B7B32E2CB ();
// 0x000007BF System.Void Newtonsoft.Json.Converters.ExpandoObjectConverter::.ctor()
extern void ExpandoObjectConverter__ctor_m3783B25FDF7D61DD5F827BDD9359353A2E4F86D6 ();
// 0x000007C0 Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.KeyValuePairConverter::InitializeReflectionObject(System.Type)
extern void KeyValuePairConverter_InitializeReflectionObject_m1F651E5178CD6B92D15A9058E66E1664F1444D1D ();
// 0x000007C1 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_WriteJson_m7D7E1F090459FB36DAD6173D31FE63020EF5B6FC ();
// 0x000007C2 System.Object Newtonsoft.Json.Converters.KeyValuePairConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void KeyValuePairConverter_ReadJson_m3CEA2CF8478C5FEF57A10216D9474670164D2C49 ();
// 0x000007C3 System.Boolean Newtonsoft.Json.Converters.KeyValuePairConverter::CanConvert(System.Type)
extern void KeyValuePairConverter_CanConvert_mA7E89DDA708CC2C5E37191FCB9A85A1365F7D129 ();
// 0x000007C4 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.ctor()
extern void KeyValuePairConverter__ctor_mB1119AC0D1B112476028E000D91E6BBBB5E3E860 ();
// 0x000007C5 System.Void Newtonsoft.Json.Converters.KeyValuePairConverter::.cctor()
extern void KeyValuePairConverter__cctor_m5CF0FDF70C7296D1BCB2BF9CD8B50FA1D04CAE7B ();
// 0x000007C6 System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_m27BE000DE0DCD75479A19B2A7550E84ADCF27290 ();
// 0x000007C7 System.Boolean Newtonsoft.Json.Converters.RegexConverter::HasFlag(System.Text.RegularExpressions.RegexOptions,System.Text.RegularExpressions.RegexOptions)
extern void RegexConverter_HasFlag_m811EB1C06C8B6C133996C5D3BFDD02292DC9B235 ();
// 0x000007C8 System.Void Newtonsoft.Json.Converters.RegexConverter::WriteBson(Newtonsoft.Json.Bson.BsonWriter,System.Text.RegularExpressions.Regex)
extern void RegexConverter_WriteBson_m55C1DA46CFA8A8394F13E4D6FC1F89A42F4A5106 ();
// 0x000007C9 System.Void Newtonsoft.Json.Converters.RegexConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Text.RegularExpressions.Regex,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_WriteJson_m5FE8015641B8AE79CF053644C9F1497FAC07E94D ();
// 0x000007CA System.Object Newtonsoft.Json.Converters.RegexConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadJson_mEE820DC0D2C1F217548394569DE78C0ECBC158EE ();
// 0x000007CB System.Object Newtonsoft.Json.Converters.RegexConverter::ReadRegexString(Newtonsoft.Json.JsonReader)
extern void RegexConverter_ReadRegexString_mC68A0CB377B3E9DB895E67650B08E3126DF6D2C4 ();
// 0x000007CC System.Text.RegularExpressions.Regex Newtonsoft.Json.Converters.RegexConverter::ReadRegexObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.JsonSerializer)
extern void RegexConverter_ReadRegexObject_m45715078A419E0BA314E27EB782A4FB4E87C74C2 ();
// 0x000007CD System.Boolean Newtonsoft.Json.Converters.RegexConverter::CanConvert(System.Type)
extern void RegexConverter_CanConvert_mE03B0EEC81088385512C579A2BBAFEEBE7743C2C ();
// 0x000007CE System.Boolean Newtonsoft.Json.Converters.RegexConverter::IsRegex(System.Type)
extern void RegexConverter_IsRegex_m3624371E914A9467399D2C660FCA83C39E1AAE7D ();
// 0x000007CF System.Void Newtonsoft.Json.Converters.RegexConverter::.ctor()
extern void RegexConverter__ctor_m294DEABFC3BCFBFE0A6AE0E85B670F76863AFCD2 ();
// 0x000007D0 Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Converters.StringEnumConverter::get_NamingStrategy()
extern void StringEnumConverter_get_NamingStrategy_m6118F88740E250FD7E1FA57988F6937893FEACB6 ();
// 0x000007D1 System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::get_AllowIntegerValues()
extern void StringEnumConverter_get_AllowIntegerValues_mC7F41257DF961EDCDBF27AD1FC863633BC6BC478 ();
// 0x000007D2 System.Void Newtonsoft.Json.Converters.StringEnumConverter::.ctor()
extern void StringEnumConverter__ctor_mD8BA43C37DE510A97BD0FFFD90E0513BA914D770 ();
// 0x000007D3 System.Void Newtonsoft.Json.Converters.StringEnumConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void StringEnumConverter_WriteJson_m8133B9C1DEFB29D70717657BA12D20ED5B27A4EA ();
// 0x000007D4 System.Object Newtonsoft.Json.Converters.StringEnumConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void StringEnumConverter_ReadJson_m4468C614021E94C19F8952FBFC75DEA3E3D72A67 ();
// 0x000007D5 System.Boolean Newtonsoft.Json.Converters.StringEnumConverter::CanConvert(System.Type)
extern void StringEnumConverter_CanConvert_m80381ED5F69192580DBB56DE38561902F12447C1 ();
// 0x000007D6 System.Void Newtonsoft.Json.Converters.XmlDocumentWrapper::.ctor(System.Xml.XmlDocument)
extern void XmlDocumentWrapper__ctor_mCBFF932E5F24B1632175A9A1FE8D147632DED570 ();
// 0x000007D7 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateComment(System.String)
extern void XmlDocumentWrapper_CreateComment_m00DB173A6E090B2CC1D473CE2E49687D08BACBAC ();
// 0x000007D8 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateTextNode(System.String)
extern void XmlDocumentWrapper_CreateTextNode_m3C4DDB13BA7DE00ED2029CEE011A948E71BF8A3E ();
// 0x000007D9 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateCDataSection(System.String)
extern void XmlDocumentWrapper_CreateCDataSection_mCFF2248CE45B03416F10C02E2B34EC4FF39A7A30 ();
// 0x000007DA Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateWhitespace(System.String)
extern void XmlDocumentWrapper_CreateWhitespace_mC43CE41B76799228987EFC3C50CCF0F13A65EF1D ();
// 0x000007DB Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateSignificantWhitespace(System.String)
extern void XmlDocumentWrapper_CreateSignificantWhitespace_m31F61254E88E3B1FCDE8D602032A05B3EBBC96F5 ();
// 0x000007DC Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateXmlDeclaration(System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateXmlDeclaration_mC254925B75A1665F94FA3912AEEC366C0C07B785 ();
// 0x000007DD Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateXmlDocumentType(System.String,System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateXmlDocumentType_m22A29E0B51AA2D6B6F4A6240C4F7775C24445629 ();
// 0x000007DE Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateProcessingInstruction(System.String,System.String)
extern void XmlDocumentWrapper_CreateProcessingInstruction_m6631D66D87B464AD48676D00FB4BF0CF49EB6FBF ();
// 0x000007DF Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String)
extern void XmlDocumentWrapper_CreateElement_mFE5B1066954439DAA29D833502A69BA4348555D6 ();
// 0x000007E0 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateElement(System.String,System.String)
extern void XmlDocumentWrapper_CreateElement_m62FF4ADD074839190AA6CEE46CC39FB6E5E0C579 ();
// 0x000007E1 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String)
extern void XmlDocumentWrapper_CreateAttribute_mE4DC3536A32A874F8C3E8B2BEDC2F18CA12E2290 ();
// 0x000007E2 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlDocumentWrapper::CreateAttribute(System.String,System.String,System.String)
extern void XmlDocumentWrapper_CreateAttribute_mB43374B0E6FDA9710D1DCF4BBF643AAA10407C46 ();
// 0x000007E3 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlDocumentWrapper::get_DocumentElement()
extern void XmlDocumentWrapper_get_DocumentElement_m6DFC5B13D5C7D95201FB1A5E846BC6E91168FE70 ();
// 0x000007E4 System.Void Newtonsoft.Json.Converters.XmlElementWrapper::.ctor(System.Xml.XmlElement)
extern void XmlElementWrapper__ctor_mC938DA690DCA1FBAB95E675762B1BC88E922A898 ();
// 0x000007E5 System.Void Newtonsoft.Json.Converters.XmlElementWrapper::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlElementWrapper_SetAttributeNode_m88EDD195DDC9F54ECA37246F2DCF95C67B63F87E ();
// 0x000007E6 System.String Newtonsoft.Json.Converters.XmlElementWrapper::GetPrefixOfNamespace(System.String)
extern void XmlElementWrapper_GetPrefixOfNamespace_m2BDC572D276BA29DB810CB957C2FCAA381BCFA97 ();
// 0x000007E7 System.Boolean Newtonsoft.Json.Converters.XmlElementWrapper::get_IsEmpty()
extern void XmlElementWrapper_get_IsEmpty_m3BB47AE5D0F8D5B69659202C32D58131B5EC0961 ();
// 0x000007E8 System.Void Newtonsoft.Json.Converters.XmlDeclarationWrapper::.ctor(System.Xml.XmlDeclaration)
extern void XmlDeclarationWrapper__ctor_m0DE6BCCC51085CAFBEA0C83E086DA7A7F9B1A5F5 ();
// 0x000007E9 System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Version()
extern void XmlDeclarationWrapper_get_Version_m66F69FAFAE7391561F102F829DA006CE658AF4B4 ();
// 0x000007EA System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Encoding()
extern void XmlDeclarationWrapper_get_Encoding_m196878FE3723E7FCDBD60AA5227C5C659792C405 ();
// 0x000007EB System.String Newtonsoft.Json.Converters.XmlDeclarationWrapper::get_Standalone()
extern void XmlDeclarationWrapper_get_Standalone_m4C990F42B856D1887B22CA448A150A9A30706E9C ();
// 0x000007EC System.Void Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::.ctor(System.Xml.XmlDocumentType)
extern void XmlDocumentTypeWrapper__ctor_m79C3E16EB8A73722D3ECE0073A01329EB8173CE3 ();
// 0x000007ED System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_Name()
extern void XmlDocumentTypeWrapper_get_Name_m49EA5B9702C3BA1A2C779F12BD6528C758C603DD ();
// 0x000007EE System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_System()
extern void XmlDocumentTypeWrapper_get_System_mCF3F92E9C8781B3F27B350E6FC37961F7F60D51C ();
// 0x000007EF System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_Public()
extern void XmlDocumentTypeWrapper_get_Public_mB0A321FA9F9D771CA2AC6648BA03CA4CA96A9CCC ();
// 0x000007F0 System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_InternalSubset()
extern void XmlDocumentTypeWrapper_get_InternalSubset_mD2334CC4FADF87EA863653E69062F3304A0CEF7C ();
// 0x000007F1 System.String Newtonsoft.Json.Converters.XmlDocumentTypeWrapper::get_LocalName()
extern void XmlDocumentTypeWrapper_get_LocalName_m04AD4A05C90918316C68AF717D3FCBB4D84ADAF6 ();
// 0x000007F2 System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::.ctor(System.Xml.XmlNode)
extern void XmlNodeWrapper__ctor_m56955E78398F89D5C7FA1C452DA3703F9AC64783 ();
// 0x000007F3 System.Object Newtonsoft.Json.Converters.XmlNodeWrapper::get_WrappedNode()
extern void XmlNodeWrapper_get_WrappedNode_mEA9197640A19B4BBC990E45CBB7FE9EBEB140452 ();
// 0x000007F4 System.Xml.XmlNodeType Newtonsoft.Json.Converters.XmlNodeWrapper::get_NodeType()
extern void XmlNodeWrapper_get_NodeType_m2F8DBF13A830888104A4F52039C2722B9062F904 ();
// 0x000007F5 System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_LocalName()
extern void XmlNodeWrapper_get_LocalName_m0CB9646D438188E9E5EF06B3902DB8938E1377BE ();
// 0x000007F6 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_ChildNodes()
extern void XmlNodeWrapper_get_ChildNodes_mE5A10C3BA574ED7BB55FE2D5B5E0729C93981312 ();
// 0x000007F7 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::WrapNode(System.Xml.XmlNode)
extern void XmlNodeWrapper_WrapNode_mF48B11E19BF30A5C3475DA1C7DE6C96AA8DCA21D ();
// 0x000007F8 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_Attributes()
extern void XmlNodeWrapper_get_Attributes_m97A169FD65946347942F7618A1B3F53F28030DE4 ();
// 0x000007F9 System.Boolean Newtonsoft.Json.Converters.XmlNodeWrapper::get_HasAttributes()
extern void XmlNodeWrapper_get_HasAttributes_m561B4D0F55A889813329C42B8439ED9033BFB242 ();
// 0x000007FA Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::get_ParentNode()
extern void XmlNodeWrapper_get_ParentNode_m99E27E9F05C18CA5812289698BC89DB4476070AA ();
// 0x000007FB System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Value()
extern void XmlNodeWrapper_get_Value_mF2428B1A129335F77FA818245CC94F4FCF1EF035 ();
// 0x000007FC System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::set_Value(System.String)
extern void XmlNodeWrapper_set_Value_m74D3957FB01C792FD6B02C9ADB2E2B7629A85667 ();
// 0x000007FD Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeWrapper_AppendChild_m527F431399ACBB712FA45F4F38D601D2F5604F8F ();
// 0x000007FE System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_NamespaceUri()
extern void XmlNodeWrapper_get_NamespaceUri_m3F0B866BB9E36A278C950F4BBE795044C3796BAA ();
// 0x000007FF Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateComment(System.String)
// 0x00000800 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateTextNode(System.String)
// 0x00000801 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateCDataSection(System.String)
// 0x00000802 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateWhitespace(System.String)
// 0x00000803 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateSignificantWhitespace(System.String)
// 0x00000804 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateXmlDeclaration(System.String,System.String,System.String)
// 0x00000805 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateXmlDocumentType(System.String,System.String,System.String,System.String)
// 0x00000806 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateProcessingInstruction(System.String,System.String)
// 0x00000807 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::CreateElement(System.String)
// 0x00000808 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::CreateElement(System.String,System.String)
// 0x00000809 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateAttribute(System.String,System.String)
// 0x0000080A Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlDocument::CreateAttribute(System.String,System.String,System.String)
// 0x0000080B Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.IXmlDocument::get_DocumentElement()
// 0x0000080C System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Version()
// 0x0000080D System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Encoding()
// 0x0000080E System.String Newtonsoft.Json.Converters.IXmlDeclaration::get_Standalone()
// 0x0000080F System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_Name()
// 0x00000810 System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_System()
// 0x00000811 System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_Public()
// 0x00000812 System.String Newtonsoft.Json.Converters.IXmlDocumentType::get_InternalSubset()
// 0x00000813 System.Void Newtonsoft.Json.Converters.IXmlElement::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
// 0x00000814 System.String Newtonsoft.Json.Converters.IXmlElement::GetPrefixOfNamespace(System.String)
// 0x00000815 System.Boolean Newtonsoft.Json.Converters.IXmlElement::get_IsEmpty()
// 0x00000816 System.Xml.XmlNodeType Newtonsoft.Json.Converters.IXmlNode::get_NodeType()
// 0x00000817 System.String Newtonsoft.Json.Converters.IXmlNode::get_LocalName()
// 0x00000818 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.IXmlNode::get_ChildNodes()
// 0x00000819 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.IXmlNode::get_Attributes()
// 0x0000081A Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlNode::get_ParentNode()
// 0x0000081B System.String Newtonsoft.Json.Converters.IXmlNode::get_Value()
// 0x0000081C Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.IXmlNode::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
// 0x0000081D System.String Newtonsoft.Json.Converters.IXmlNode::get_NamespaceUri()
// 0x0000081E System.Object Newtonsoft.Json.Converters.IXmlNode::get_WrappedNode()
// 0x0000081F System.Xml.Linq.XDeclaration Newtonsoft.Json.Converters.XDeclarationWrapper::get_Declaration()
extern void XDeclarationWrapper_get_Declaration_m6B71AC50AA501740922CBB3A07AC6B57BADFF3E6 ();
// 0x00000820 System.Void Newtonsoft.Json.Converters.XDeclarationWrapper::.ctor(System.Xml.Linq.XDeclaration)
extern void XDeclarationWrapper__ctor_m92E156FBCF6479C70F243E596B4E32FF4F04D2C2 ();
// 0x00000821 System.Xml.XmlNodeType Newtonsoft.Json.Converters.XDeclarationWrapper::get_NodeType()
extern void XDeclarationWrapper_get_NodeType_m77B6B12924DB0CE56FDB5122BDD8A7E24F21ED3F ();
// 0x00000822 System.String Newtonsoft.Json.Converters.XDeclarationWrapper::get_Version()
extern void XDeclarationWrapper_get_Version_mE63D0CE8066442111E8057EC3F6B9D1C716980EC ();
// 0x00000823 System.String Newtonsoft.Json.Converters.XDeclarationWrapper::get_Encoding()
extern void XDeclarationWrapper_get_Encoding_mA50767247C5D51B57FAB9DC85A8F6B1F784BAF12 ();
// 0x00000824 System.String Newtonsoft.Json.Converters.XDeclarationWrapper::get_Standalone()
extern void XDeclarationWrapper_get_Standalone_m7F5FBA9A1F7AEFF28E67062815A091C5C02EEF99 ();
// 0x00000825 System.Void Newtonsoft.Json.Converters.XDocumentTypeWrapper::.ctor(System.Xml.Linq.XDocumentType)
extern void XDocumentTypeWrapper__ctor_m26D46820553E4DC831D2CD2E3C3403B8957DEB8D ();
// 0x00000826 System.String Newtonsoft.Json.Converters.XDocumentTypeWrapper::get_Name()
extern void XDocumentTypeWrapper_get_Name_m5568DB1E64425419B89053AC0A297CF57C84E050 ();
// 0x00000827 System.String Newtonsoft.Json.Converters.XDocumentTypeWrapper::get_System()
extern void XDocumentTypeWrapper_get_System_m3FDD057A8114D05BE4CA0B569A184D25E8C1EB00 ();
// 0x00000828 System.String Newtonsoft.Json.Converters.XDocumentTypeWrapper::get_Public()
extern void XDocumentTypeWrapper_get_Public_m8568FEE7E79D219E245E66E00C06B4B2B6A69AD9 ();
// 0x00000829 System.String Newtonsoft.Json.Converters.XDocumentTypeWrapper::get_InternalSubset()
extern void XDocumentTypeWrapper_get_InternalSubset_mC900B6161241721AD3B9AB5D9AE9A8F9D12BACDD ();
// 0x0000082A System.String Newtonsoft.Json.Converters.XDocumentTypeWrapper::get_LocalName()
extern void XDocumentTypeWrapper_get_LocalName_m91C0BAFA290E7B2A258A240A38D02BF0FC6C0915 ();
// 0x0000082B System.Xml.Linq.XDocument Newtonsoft.Json.Converters.XDocumentWrapper::get_Document()
extern void XDocumentWrapper_get_Document_mF3AA048CF86753719932305066EBE3D71CA5D263 ();
// 0x0000082C System.Void Newtonsoft.Json.Converters.XDocumentWrapper::.ctor(System.Xml.Linq.XDocument)
extern void XDocumentWrapper__ctor_m6E109AAC5C9CE19EB647D45E0442AC0E28DBFDA4 ();
// 0x0000082D System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XDocumentWrapper::get_ChildNodes()
extern void XDocumentWrapper_get_ChildNodes_m66DD385359184B30F5402547BA0B3B79AA1359B2 ();
// 0x0000082E System.Boolean Newtonsoft.Json.Converters.XDocumentWrapper::get_HasChildNodes()
extern void XDocumentWrapper_get_HasChildNodes_m0BCB6C97C6A80035F1F658993F7DE128DC791652 ();
// 0x0000082F Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateComment(System.String)
extern void XDocumentWrapper_CreateComment_mBE2C9FA27EB94BC06DE1C07DF75BF038049D565E ();
// 0x00000830 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateTextNode(System.String)
extern void XDocumentWrapper_CreateTextNode_m2022E291A8081A037E25B6DC3BDD20C0DD9F4844 ();
// 0x00000831 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateCDataSection(System.String)
extern void XDocumentWrapper_CreateCDataSection_mEBDD3E9B0E029DF106BF83C264BA93449FBAE995 ();
// 0x00000832 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateWhitespace(System.String)
extern void XDocumentWrapper_CreateWhitespace_m6FEF8BA816EA0EBC1D13168AB59EA0845E244A0A ();
// 0x00000833 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateSignificantWhitespace(System.String)
extern void XDocumentWrapper_CreateSignificantWhitespace_m89CFF8550D28094FBC9F32ECEE6CE22340F20312 ();
// 0x00000834 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateXmlDeclaration(System.String,System.String,System.String)
extern void XDocumentWrapper_CreateXmlDeclaration_m583FD9D82D593A0924CCC99C06C054427DAB2E16 ();
// 0x00000835 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateXmlDocumentType(System.String,System.String,System.String,System.String)
extern void XDocumentWrapper_CreateXmlDocumentType_m35E9388BBD3E485B7D7AD4DC1F3DBE776E353302 ();
// 0x00000836 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateProcessingInstruction(System.String,System.String)
extern void XDocumentWrapper_CreateProcessingInstruction_m403FC41B218B872A2A39D6391C50A2C91F9C06D7 ();
// 0x00000837 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XDocumentWrapper::CreateElement(System.String)
extern void XDocumentWrapper_CreateElement_mDB804223D4E9C024B78771C1ED702DF71B6281FE ();
// 0x00000838 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XDocumentWrapper::CreateElement(System.String,System.String)
extern void XDocumentWrapper_CreateElement_mAFDDEE9303F1EF746A9AECE4B17F95667BAF7E9C ();
// 0x00000839 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateAttribute(System.String,System.String)
extern void XDocumentWrapper_CreateAttribute_m470B1D78632FFE1C2CCDF2660F76CD7DD47E8327 ();
// 0x0000083A Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::CreateAttribute(System.String,System.String,System.String)
extern void XDocumentWrapper_CreateAttribute_mE71E4CFE30BAA95448BA9A7B4AFC446E1D9B47FF ();
// 0x0000083B Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XDocumentWrapper::get_DocumentElement()
extern void XDocumentWrapper_get_DocumentElement_m1257C318BA84CAE2C9DC76197DEB299F1B70D04B ();
// 0x0000083C Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XDocumentWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XDocumentWrapper_AppendChild_m4D3ADB19375159452D02049D5BF4D9639E969962 ();
// 0x0000083D System.Xml.Linq.XText Newtonsoft.Json.Converters.XTextWrapper::get_Text()
extern void XTextWrapper_get_Text_m9E0C69C1AFB2C4D9949D33D681565192C6BE2E36 ();
// 0x0000083E System.Void Newtonsoft.Json.Converters.XTextWrapper::.ctor(System.Xml.Linq.XText)
extern void XTextWrapper__ctor_mD7EF2546868D97F3A931CBC294CBA6DF1B8E2238 ();
// 0x0000083F System.String Newtonsoft.Json.Converters.XTextWrapper::get_Value()
extern void XTextWrapper_get_Value_m231219A7A8BC8D30BCE90E02FAEAF79CC9E74D63 ();
// 0x00000840 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XTextWrapper::get_ParentNode()
extern void XTextWrapper_get_ParentNode_m09DA419C7181FC1E4CDEC5CD17AC6E0466A25EC3 ();
// 0x00000841 System.Xml.Linq.XComment Newtonsoft.Json.Converters.XCommentWrapper::get_Text()
extern void XCommentWrapper_get_Text_m713EB632E04CB6A89E3E28D71D50548AD9929877 ();
// 0x00000842 System.Void Newtonsoft.Json.Converters.XCommentWrapper::.ctor(System.Xml.Linq.XComment)
extern void XCommentWrapper__ctor_m45F66B921B66D080093A633B6251E79D161C4637 ();
// 0x00000843 System.String Newtonsoft.Json.Converters.XCommentWrapper::get_Value()
extern void XCommentWrapper_get_Value_mCCABEBCC945CDA5FC4698AB05CC370549CBE7E57 ();
// 0x00000844 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XCommentWrapper::get_ParentNode()
extern void XCommentWrapper_get_ParentNode_mF2C8AC269A82C7ADFDD0C3A72E6A0A4254F22221 ();
// 0x00000845 System.Xml.Linq.XProcessingInstruction Newtonsoft.Json.Converters.XProcessingInstructionWrapper::get_ProcessingInstruction()
extern void XProcessingInstructionWrapper_get_ProcessingInstruction_mA2C743A6FC6BA660AE553D05E6364F6CD63AE29B ();
// 0x00000846 System.Void Newtonsoft.Json.Converters.XProcessingInstructionWrapper::.ctor(System.Xml.Linq.XProcessingInstruction)
extern void XProcessingInstructionWrapper__ctor_m76A4A0100E0428790AFC5D23A1736CF7BC1EC38C ();
// 0x00000847 System.String Newtonsoft.Json.Converters.XProcessingInstructionWrapper::get_LocalName()
extern void XProcessingInstructionWrapper_get_LocalName_m85DF0F00AED7099621CE59B80CE1BB697841DC76 ();
// 0x00000848 System.String Newtonsoft.Json.Converters.XProcessingInstructionWrapper::get_Value()
extern void XProcessingInstructionWrapper_get_Value_mA78A2672B3E518B9AB902379C48CA8E0D96D51F0 ();
// 0x00000849 System.Xml.Linq.XContainer Newtonsoft.Json.Converters.XContainerWrapper::get_Container()
extern void XContainerWrapper_get_Container_m9848D428F04F497D8F95758A77D252F10653B1B1 ();
// 0x0000084A System.Void Newtonsoft.Json.Converters.XContainerWrapper::.ctor(System.Xml.Linq.XContainer)
extern void XContainerWrapper__ctor_m5FA678CF4F3D1EA0B812106AA8DA1F19B38CD0FB ();
// 0x0000084B System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XContainerWrapper::get_ChildNodes()
extern void XContainerWrapper_get_ChildNodes_mCBEE68DF2B446879DF0C8A0E40AE1220266BD3EA ();
// 0x0000084C System.Boolean Newtonsoft.Json.Converters.XContainerWrapper::get_HasChildNodes()
extern void XContainerWrapper_get_HasChildNodes_m9959C7C278BC5F9BA65D57994346139E53B6D966 ();
// 0x0000084D Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XContainerWrapper::get_ParentNode()
extern void XContainerWrapper_get_ParentNode_mECAEFE1CE9D421432EB614AB7B656173029EE624 ();
// 0x0000084E Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XContainerWrapper::WrapNode(System.Xml.Linq.XObject)
extern void XContainerWrapper_WrapNode_m9368FD5E4AEA85FFFAB8250000B9203450735C4E ();
// 0x0000084F Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XContainerWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XContainerWrapper_AppendChild_mB1AE254C5ACF5001FD013EC06670932D77C765A9 ();
// 0x00000850 System.Void Newtonsoft.Json.Converters.XObjectWrapper::.ctor(System.Xml.Linq.XObject)
extern void XObjectWrapper__ctor_mD99AA05A08081D3E7EBE932E30865B556C325AF7 ();
// 0x00000851 System.Object Newtonsoft.Json.Converters.XObjectWrapper::get_WrappedNode()
extern void XObjectWrapper_get_WrappedNode_m25279F86A39559906A8F7E282916953CB3E289B5 ();
// 0x00000852 System.Xml.XmlNodeType Newtonsoft.Json.Converters.XObjectWrapper::get_NodeType()
extern void XObjectWrapper_get_NodeType_m20D34D93E4DF66392E4238C95DAD668AC1EC2851 ();
// 0x00000853 System.String Newtonsoft.Json.Converters.XObjectWrapper::get_LocalName()
extern void XObjectWrapper_get_LocalName_mE12B4A83E99F2B9E05EBB8C7A2138389EAF7F573 ();
// 0x00000854 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::get_ChildNodes()
extern void XObjectWrapper_get_ChildNodes_m05E6FBC7605057788F6722E4A3A876D988993851 ();
// 0x00000855 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XObjectWrapper::get_Attributes()
extern void XObjectWrapper_get_Attributes_mB983361EC4F96CDE9464098F20EEAE792ABE5B62 ();
// 0x00000856 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XObjectWrapper::get_ParentNode()
extern void XObjectWrapper_get_ParentNode_m34E1467029A86E6724158CEBF19CB8746312C80D ();
// 0x00000857 System.String Newtonsoft.Json.Converters.XObjectWrapper::get_Value()
extern void XObjectWrapper_get_Value_m72EBE002953C13B37195FD4D75F59344160BD0E1 ();
// 0x00000858 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XObjectWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XObjectWrapper_AppendChild_m16B735232AFF1AA6F5FB5656D3C3D38B8C2E94F3 ();
// 0x00000859 System.String Newtonsoft.Json.Converters.XObjectWrapper::get_NamespaceUri()
extern void XObjectWrapper_get_NamespaceUri_m6FA98699A5D2B0121B77CB00170A629A60B82796 ();
// 0x0000085A System.Xml.Linq.XAttribute Newtonsoft.Json.Converters.XAttributeWrapper::get_Attribute()
extern void XAttributeWrapper_get_Attribute_m179DD5BFD35ED40CF3CA56678F087EEAB0DCF1BA ();
// 0x0000085B System.Void Newtonsoft.Json.Converters.XAttributeWrapper::.ctor(System.Xml.Linq.XAttribute)
extern void XAttributeWrapper__ctor_mE75DDF01CF6098B48F689B166E84C10B9709A2A5 ();
// 0x0000085C System.String Newtonsoft.Json.Converters.XAttributeWrapper::get_Value()
extern void XAttributeWrapper_get_Value_mC08C7A8B6EF1E875A699EBDAB69BC6F334B88720 ();
// 0x0000085D System.String Newtonsoft.Json.Converters.XAttributeWrapper::get_LocalName()
extern void XAttributeWrapper_get_LocalName_mDC625E5526AD0F76A7B90572AF173AE46184B22E ();
// 0x0000085E System.String Newtonsoft.Json.Converters.XAttributeWrapper::get_NamespaceUri()
extern void XAttributeWrapper_get_NamespaceUri_m594B0AF334B7BCC38858668178CB12162DFB5F8D ();
// 0x0000085F Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XAttributeWrapper::get_ParentNode()
extern void XAttributeWrapper_get_ParentNode_m09DE8AD07A4824ED0DE8D56E4FFADA8413D8CB8B ();
// 0x00000860 System.Xml.Linq.XElement Newtonsoft.Json.Converters.XElementWrapper::get_Element()
extern void XElementWrapper_get_Element_m50FD04D503C7376106D75712C2C4935FF18B17FF ();
// 0x00000861 System.Void Newtonsoft.Json.Converters.XElementWrapper::.ctor(System.Xml.Linq.XElement)
extern void XElementWrapper__ctor_m937078F4FCE5230E84B8249A7C1ACDD8696E33E1 ();
// 0x00000862 System.Void Newtonsoft.Json.Converters.XElementWrapper::SetAttributeNode(Newtonsoft.Json.Converters.IXmlNode)
extern void XElementWrapper_SetAttributeNode_mBB7A2289297DCCBD84EEBFE1A8C2673EDAB5E8E5 ();
// 0x00000863 System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XElementWrapper::get_Attributes()
extern void XElementWrapper_get_Attributes_m5C95307FB17FC43EB43E0B87C25B1944F31AEE53 ();
// 0x00000864 System.Boolean Newtonsoft.Json.Converters.XElementWrapper::HasImplicitNamespaceAttribute(System.String)
extern void XElementWrapper_HasImplicitNamespaceAttribute_mBEC9B9459FE78FE3FF189ADE2A16B315EC728A93 ();
// 0x00000865 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XElementWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern void XElementWrapper_AppendChild_mE5AC5DA4C44C25197B621DC2C76C9B191BDE3460 ();
// 0x00000866 System.String Newtonsoft.Json.Converters.XElementWrapper::get_Value()
extern void XElementWrapper_get_Value_m9CEC8A9B253BE025CA0B32E7904B179FFE70B9ED ();
// 0x00000867 System.String Newtonsoft.Json.Converters.XElementWrapper::get_LocalName()
extern void XElementWrapper_get_LocalName_m0E8C6261303727A53C3D7467ADBC13756ED2F63F ();
// 0x00000868 System.String Newtonsoft.Json.Converters.XElementWrapper::get_NamespaceUri()
extern void XElementWrapper_get_NamespaceUri_m7D3D958E51C5E32D3EF8B95E62700FBCF4011E41 ();
// 0x00000869 System.String Newtonsoft.Json.Converters.XElementWrapper::GetPrefixOfNamespace(System.String)
extern void XElementWrapper_GetPrefixOfNamespace_m2DD2A82049E3B736B94DE0772A684D7673307BC1 ();
// 0x0000086A System.Boolean Newtonsoft.Json.Converters.XElementWrapper::get_IsEmpty()
extern void XElementWrapper_get_IsEmpty_m4047C0B631DA5F3C724D6C2DD107204B2CB24B1A ();
// 0x0000086B System.String Newtonsoft.Json.Converters.XmlNodeConverter::get_DeserializeRootElementName()
extern void XmlNodeConverter_get_DeserializeRootElementName_m63B070694296090C009BB14D77461E84B8EC82B2 ();
// 0x0000086C System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_WriteArrayAttribute()
extern void XmlNodeConverter_get_WriteArrayAttribute_m3A6016D30C98406D4C30D6DCCC53C9B039CF5DBA ();
// 0x0000086D System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_OmitRootObject()
extern void XmlNodeConverter_get_OmitRootObject_m0931E1452D03534EA2CCB661765C4038D098A3EE ();
// 0x0000086E System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::get_EncodeSpecialCharacters()
extern void XmlNodeConverter_get_EncodeSpecialCharacters_mAD2AE708CFAED00AA0D7F2C2E1FC6CCB50B9FA79 ();
// 0x0000086F System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void XmlNodeConverter_WriteJson_m0946D3AF2831BD718BF92D1FA87933C1AC09D846 ();
// 0x00000870 Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeConverter::WrapXml(System.Object)
extern void XmlNodeConverter_WrapXml_m2A524C3E5650139862E747A1FB51444DC0691D98 ();
// 0x00000871 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::PushParentNamespaces(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_PushParentNamespaces_mCEAC6475A4AB3538685682D81B65F9477FBDF9DB ();
// 0x00000872 System.String Newtonsoft.Json.Converters.XmlNodeConverter::ResolveFullName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ResolveFullName_m259FDBB8D1479FFE9378DCCE6537EE2E604F37FE ();
// 0x00000873 System.String Newtonsoft.Json.Converters.XmlNodeConverter::GetPropertyName(Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_GetPropertyName_m43BFF71FC7CAB9C34E413496D2BFCC06ABD3161F ();
// 0x00000874 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsArray(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_IsArray_m4C93CF7D8927D8072535D76A2850660CC26F93B8 ();
// 0x00000875 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeGroupedNodes(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern void XmlNodeConverter_SerializeGroupedNodes_m1D2519ACD384ED228B1D39ACFF2A027B3FDF29A6 ();
// 0x00000876 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteGroupedNodes(Newtonsoft.Json.JsonWriter,System.Xml.XmlNamespaceManager,System.Boolean,System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>,System.String)
extern void XmlNodeConverter_WriteGroupedNodes_m325D7AA9F38ACB652ED5055F05BCA0252BB2658C ();
// 0x00000877 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::WriteGroupedNodes(Newtonsoft.Json.JsonWriter,System.Xml.XmlNamespaceManager,System.Boolean,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern void XmlNodeConverter_WriteGroupedNodes_mD5EC88187CDE6764B27BDA3245FB880E80CAEDC9 ();
// 0x00000878 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::SerializeNode(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager,System.Boolean)
extern void XmlNodeConverter_SerializeNode_m6F75281C7AC1CAE074AD9A5BAA0D5796B5680329 ();
// 0x00000879 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::AllSameName(Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_AllSameName_mF3F3CF125BBF97209F28859C8EE130591E3C3A5A ();
// 0x0000087A System.Object Newtonsoft.Json.Converters.XmlNodeConverter::ReadJson(Newtonsoft.Json.JsonReader,System.Type,System.Object,Newtonsoft.Json.JsonSerializer)
extern void XmlNodeConverter_ReadJson_mB9F9B1F1EA83A04D8FA715A6929CB75729071888 ();
// 0x0000087B System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeValue(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,System.String,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_DeserializeValue_mD0C2CD230EC4C24F3AB8BF7000C292DE97F7B3EA ();
// 0x0000087C System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadElement_m52CD62E8182CA2BE93162AC15FA241783CF03BD2 ();
// 0x0000087D System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.Xml.XmlNamespaceManager,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void XmlNodeConverter_CreateElement_m6D148559F8C186402B04056DC7901C3CD6BE20CC ();
// 0x0000087E System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddAttribute(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String,System.String,System.Xml.XmlNamespaceManager,System.String)
extern void XmlNodeConverter_AddAttribute_mAF18A72C9662976EED20145A3CC342BE678ED5CD ();
// 0x0000087F System.String Newtonsoft.Json.Converters.XmlNodeConverter::ConvertTokenToXmlValue(Newtonsoft.Json.JsonReader)
extern void XmlNodeConverter_ConvertTokenToXmlValue_mB31E099F93AF2CB0F558B2626B370C7729C2A24F ();
// 0x00000880 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::ReadArrayElements(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.String,Newtonsoft.Json.Converters.IXmlNode,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadArrayElements_mE71ABD583A9D9F47490FE5A4DC24E8527FE4EAAD ();
// 0x00000881 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::AddJsonArrayAttribute(Newtonsoft.Json.Converters.IXmlElement,Newtonsoft.Json.Converters.IXmlDocument)
extern void XmlNodeConverter_AddJsonArrayAttribute_mEBA3F44A2C09045A787751B637EBF15674777626 ();
// 0x00000882 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ShouldReadInto(Newtonsoft.Json.JsonReader)
extern void XmlNodeConverter_ShouldReadInto_m5BBBB7B44C7F6565BCE9A854EB60AFBEA919EDA5 ();
// 0x00000883 System.Collections.Generic.Dictionary`2<System.String,System.String> Newtonsoft.Json.Converters.XmlNodeConverter::ReadAttributeElements(Newtonsoft.Json.JsonReader,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_ReadAttributeElements_m95E2F93312CD8472E8F3F88B47228B4288902002 ();
// 0x00000884 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateInstruction(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode,System.String)
extern void XmlNodeConverter_CreateInstruction_mC317B6EF18F64415F38475CC2558A81B7C8EBF75 ();
// 0x00000885 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::CreateDocumentType(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_CreateDocumentType_mA1F554B82D72F49DD49223BAAB9D577675362FA3 ();
// 0x00000886 Newtonsoft.Json.Converters.IXmlElement Newtonsoft.Json.Converters.XmlNodeConverter::CreateElement(System.String,Newtonsoft.Json.Converters.IXmlDocument,System.String,System.Xml.XmlNamespaceManager)
extern void XmlNodeConverter_CreateElement_m575B9ACF2636770F7D929D8AAFA94F80D8C29ADE ();
// 0x00000887 System.Void Newtonsoft.Json.Converters.XmlNodeConverter::DeserializeNode(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Converters.IXmlDocument,System.Xml.XmlNamespaceManager,Newtonsoft.Json.Converters.IXmlNode)
extern void XmlNodeConverter_DeserializeNode_m1F303681104FF534B2D33CAEF062DFFC124317B7 ();
// 0x00000888 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsNamespaceAttribute(System.String,System.String&)
extern void XmlNodeConverter_IsNamespaceAttribute_m3F83132E95A31835A3CB1546480EF6E780CBD2C6 ();
// 0x00000889 System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::ValueAttributes(System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>)
extern void XmlNodeConverter_ValueAttributes_m296E7D53C4450778E79DF956F184A1172120B3A8 ();
// 0x0000088A System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::CanConvert(System.Type)
extern void XmlNodeConverter_CanConvert_m5CF84182FFB1194C0D83705F6F605533349A8B48 ();
// 0x0000088B System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsXObject(System.Type)
extern void XmlNodeConverter_IsXObject_m0FEE07E42C97ED3DA2EE42C4D4E910F432CE7E0A ();
// 0x0000088C System.Boolean Newtonsoft.Json.Converters.XmlNodeConverter::IsXmlNode(System.Type)
extern void XmlNodeConverter_IsXmlNode_m596F5B1168356D5BC69075E91CDC16929EDEB36F ();
// 0x0000088D System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.ctor()
extern void XmlNodeConverter__ctor_mDADEAA8CDDB5EA5CF654F3768B09001BFCBFBFE2 ();
// 0x0000088E System.Void Newtonsoft.Json.Converters.XmlNodeConverter::.cctor()
extern void XmlNodeConverter__cctor_mA139CD3692E1F9C6C60BD681967209F4BECD56B8 ();
// 0x0000088F System.Byte[] Newtonsoft.Json.Bson.BsonObjectId::get_Value()
extern void BsonObjectId_get_Value_mB2BB4F836020BAEC4CF65BBACAD07926A053946B ();
// 0x00000890 System.Void Newtonsoft.Json.Bson.BsonObjectId::.ctor(System.Byte[])
extern void BsonObjectId__ctor_m3D9121A541D6276C14091527447EFFFCFA343D85 ();
// 0x00000891 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonToken::get_Type()
// 0x00000892 System.Void Newtonsoft.Json.Bson.BsonToken::set_Parent(Newtonsoft.Json.Bson.BsonToken)
extern void BsonToken_set_Parent_mD4F3136F36730C20DA04344824246DE7AD54E902 ();
// 0x00000893 System.Void Newtonsoft.Json.Bson.BsonToken::.ctor()
extern void BsonToken__ctor_mF5AEF2BE7F64AD78CF515419271E6855EA6A84E2 ();
// 0x00000894 System.Void Newtonsoft.Json.Bson.BsonObject::Add(System.String,Newtonsoft.Json.Bson.BsonToken)
extern void BsonObject_Add_mCB9F536AD0F214CCB2A305D5F8F0611D7D24228E ();
// 0x00000895 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonObject::get_Type()
extern void BsonObject_get_Type_m7FF064E7D3528E7CEACA5D4C4CF85D8C47CDE1A6 ();
// 0x00000896 System.Void Newtonsoft.Json.Bson.BsonArray::Add(Newtonsoft.Json.Bson.BsonToken)
extern void BsonArray_Add_mA93BAD1A4704568872344EE8786000186271FEE0 ();
// 0x00000897 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonArray::get_Type()
extern void BsonArray_get_Type_mC29ECECD16CE0301F4B17C16A93ED223D15919FD ();
// 0x00000898 System.Void Newtonsoft.Json.Bson.BsonValue::.ctor(System.Object,Newtonsoft.Json.Bson.BsonType)
extern void BsonValue__ctor_m64B7C66C8F1A61BF525807FB9FFD9563E6E384F0 ();
// 0x00000899 Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonValue::get_Type()
extern void BsonValue_get_Type_m6DDEA9908FF18D962F3C1CFC495D5CACCCD71AED ();
// 0x0000089A System.Void Newtonsoft.Json.Bson.BsonString::.ctor(System.Object,System.Boolean)
extern void BsonString__ctor_mF94A16E54777C681EA000D01EA35A5F7EE07B212 ();
// 0x0000089B System.Void Newtonsoft.Json.Bson.BsonRegex::set_Pattern(Newtonsoft.Json.Bson.BsonString)
extern void BsonRegex_set_Pattern_m21DD5B5D3BBFE8B16E935CA9DEFC52DFDBB05DBF ();
// 0x0000089C System.Void Newtonsoft.Json.Bson.BsonRegex::set_Options(Newtonsoft.Json.Bson.BsonString)
extern void BsonRegex_set_Options_m39332EE8F58BB3E3748772227D76C0FDF7A49504 ();
// 0x0000089D System.Void Newtonsoft.Json.Bson.BsonRegex::.ctor(System.String,System.String)
extern void BsonRegex__ctor_mC868086A71FF8F56B9F526E485A919FB81383865 ();
// 0x0000089E Newtonsoft.Json.Bson.BsonType Newtonsoft.Json.Bson.BsonRegex::get_Type()
extern void BsonRegex_get_Type_m82E5509487A124B7839E98563CA5025FF958D276 ();
// 0x0000089F System.Void Newtonsoft.Json.Bson.BsonProperty::set_Name(Newtonsoft.Json.Bson.BsonString)
extern void BsonProperty_set_Name_m864ECCC2EAAC4A23CDA4256056147D5E460A482E ();
// 0x000008A0 System.Void Newtonsoft.Json.Bson.BsonProperty::set_Value(Newtonsoft.Json.Bson.BsonToken)
extern void BsonProperty_set_Value_m919CA8D72C3328C9AB044DBCF989C404462B4D06 ();
// 0x000008A1 System.Void Newtonsoft.Json.Bson.BsonProperty::.ctor()
extern void BsonProperty__ctor_m65E91143CF6A7EBC37D3BC7C7D51CDC13677E8B0 ();
// 0x000008A2 System.Void Newtonsoft.Json.Bson.BsonWriter::AddValue(System.Object,Newtonsoft.Json.Bson.BsonType)
extern void BsonWriter_AddValue_m278DB787BF4E90BDF46069697DA9F9F7BE417CC2 ();
// 0x000008A3 System.Void Newtonsoft.Json.Bson.BsonWriter::AddToken(Newtonsoft.Json.Bson.BsonToken)
extern void BsonWriter_AddToken_m7E1E7D939DB6128455533817DAE3B5FD960C7609 ();
// 0x000008A4 System.Void Newtonsoft.Json.Bson.BsonWriter::WriteObjectId(System.Byte[])
extern void BsonWriter_WriteObjectId_mACB166CF782B2E0A55D2BDE73F56F72D570254CB ();
// 0x000008A5 System.Void Newtonsoft.Json.Bson.BsonWriter::WriteRegex(System.String,System.String)
extern void BsonWriter_WriteRegex_m6353BD2E68125E81E64F1723476DC319A3871682 ();
static Il2CppMethodPointer s_methodPointers[2213] = 
{
	EmbeddedAttribute__ctor_mB273193DD52B5C9A336427F024738D94C58BAD7A,
	IsReadOnlyAttribute__ctor_m0ED71E75DDB669B49D1D31F5F5BAD18A7090A124,
	NullableAttribute__ctor_m7DD70DAAA69617120883F56611434D0D0D42E3EC,
	NullableAttribute__ctor_mBE190C8A1BFD024F749C6314C354F44885600014,
	NullableContextAttribute__ctor_mD8DE65331926EC11CBC38222484F6A7EF7D70F8B,
	NotNullAttribute__ctor_m79B49921B0928C20183F7133F04B89748935A463,
	NotNullWhenAttribute__ctor_mD92C70B72B528AB3AF64F167537A59AEFDA946FC,
	MaybeNullAttribute__ctor_mEEB759014C4ACC6EC19C244D77F4953D138E1A20,
	AllowNullAttribute__ctor_mB0FA9F7895722BC30947A41FE32C2EF687130115,
	DefaultJsonNameTable__cctor_m777387907B745619C20DA84466262DB733D1F329,
	DefaultJsonNameTable__ctor_m5A0440F494B10AB256DAA10052F8BB07D701588E,
	DefaultJsonNameTable_Get_m48891B9470C709211443C20664764BFE4A105182,
	DefaultJsonNameTable_Add_mB2D7C9B2427728AFA31D6F63F1FC75002125068A,
	DefaultJsonNameTable_AddEntry_mB67424E4B4EA9B81CED3EE08FF6E35B5FD1DEDF6,
	DefaultJsonNameTable_Grow_m087C859E722E13F86FD6F0C25BD7DB50364AA8A0,
	DefaultJsonNameTable_TextEquals_m9A4E903024CE7531B5778AB50A13FE38BCD17AF8,
	Entry__ctor_mD93E768008E8A5BBCD25F1115F6970D9BEF672DC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonContainerAttribute_get_ItemConverterType_m5D96D2834844FF0B7C16B3D48B7F79ABD1A0EB83,
	JsonContainerAttribute_get_ItemConverterParameters_m520C0C8C690384C3C197C5D712FA9E2F800D2BE3,
	JsonContainerAttribute_get_NamingStrategyType_m81A64DEDC6DAAD8779ACBBD9F642F92A7DD64155,
	JsonContainerAttribute_get_NamingStrategyParameters_m530DCA6B890697E0DB6F9A7D47D43417789148F0,
	JsonContainerAttribute_get_NamingStrategyInstance_m15380E8ABFA6198F97B2FC8614BABA0EEF4DA7E2,
	JsonContainerAttribute_set_NamingStrategyInstance_mE4363D98B671605F556978755D570249F5D907C4,
	JsonConvert_get_DefaultSettings_m0A5829E16356A29B346272512A584D9D26307A70,
	JsonConvert_ToString_m5539D9C3EB7CE1EBBB1FAECE58392F9AA9B7D078,
	JsonConvert_ToString_m04EAC879D3ACDC77DF51A996FA5E65EA22F61AC8,
	JsonConvert_ToString_mE6AE849A1CDEEF9BCAE53266B72BA24095EF9C98,
	JsonConvert_EnsureFloatFormat_m124C426E6715F9834E03B83C4A59D8EE5FDE0493,
	JsonConvert_ToString_m60389DAA11D9D7A863D0A05667510C78B7E7F985,
	JsonConvert_EnsureDecimalPlace_m4F4F7BC16AF4D73DB62DCB9E66A53166945B6D98,
	JsonConvert_EnsureDecimalPlace_mF1744F05E62345548DC2357DB3D6FE509F336809,
	JsonConvert_ToString_mD0A25EA590A53D43045C3650CFF72B8A772CDEBD,
	JsonConvert_ToString_mBD0E940E54C81DB74188774D2E69EA954A9F8F10,
	JsonConvert_ToString_m3AA6E9FF7A6738C7E330EB39CAADDB9DA407A06B,
	JsonConvert_ToString_m8F2A67B6DD842552AA9B96F5003DE119A8D22357,
	JsonConvert_SerializeObject_m062FD300E3D2077562830DD0932BEAAB287E60F7,
	JsonConvert_SerializeObject_m8104C62BC781FB4C99515DADA67C044A0734A3F3,
	JsonConvert_SerializeObject_mE6A5323F38C9CC4E06F9C554A7A30E152314CFDC,
	JsonConvert_SerializeObject_m6243D043AB7C228CE5DCB69026DDF04570197E5D,
	JsonConvert_SerializeObject_mC903A1A78DA571EEA1D7FF6D2C29E2AA41AF35A1,
	JsonConvert_SerializeObject_m698382F545F441FF31EED69370F4B3D66B39471A,
	JsonConvert_SerializeObjectInternal_m11EC9E5E449E36BEE41CFEC7B09E6E242B5958D9,
	JsonConvert_DeserializeObject_m3BBFF9DA6FB0DC26B9CE39AF351B97F6A2313732,
	JsonConvert_DeserializeObject_mDA0C11B56769B7A57E66BB464676BE6B7B7EFCAF,
	JsonConvert__cctor_m7CC8FB7137895A45DFEC72EBC9D247F764420A72,
	NULL,
	NULL,
	NULL,
	JsonConverter_get_CanRead_m9000CD8CB08FB4937904FD3C4DE248EC81C7CF26,
	JsonConverter_get_CanWrite_m92AD594A45F983BBB6564660A66C320B6D33F8CE,
	JsonConverter__ctor_m8FF84C02C0A9A2CCCD194D214D1C167A4FD4EF1F,
	JsonConverterAttribute_get_ConverterType_m48DC4328902ACEE7B13BFC3612683C11E175C14B,
	JsonConverterAttribute_get_ConverterParameters_m9D92AE344C1B162A9ED2D61FC89E18AC872697DD,
	JsonConverterCollection__ctor_mA5111AF227AE8E59AFFE6ED324C6851207890972,
	JsonException__ctor_m3F80CF7E07C8D0FA7F3F002872553725110ADCFC,
	JsonException__ctor_mE7035AB3F35D9599B2E322778D6FE6CBDF264E47,
	JsonException__ctor_mD0F2E724723681D9A58F3005CCEF101CF601F8B1,
	JsonException__ctor_m3C49AC07DDC28568B0FFE8A6EF27074D21FE262F,
	JsonExtensionDataAttribute_get_WriteData_m65FEF9E9BA6439913E87CBE3FFC41A14DE14C0F3,
	JsonExtensionDataAttribute_get_ReadData_mBFF7CD91D4A3544891ABE93C24FCB6CF2964A5A3,
	NULL,
	JsonNameTable__ctor_m2E6B4870C328D678E80DEF2A9EC0EB259977FED2,
	JsonObjectAttribute_get_MemberSerialization_m1A6E56883ACE6421B24BA6FA3C73B48B25B82701,
	JsonPosition__ctor_m4BA076CE024C3FF82069500255938E4AA21A0218_AdjustorThunk,
	JsonPosition_CalculateLength_m539687D796EBA5ED3178CAAEB315D74682D3151F_AdjustorThunk,
	JsonPosition_WriteTo_mD76A0378E52911640E329D0DCFCDDF4281AFA772_AdjustorThunk,
	JsonPosition_TypeHasIndex_m043EC579B05748A4926CF9530F2416756C45BF41,
	JsonPosition_BuildPath_mE563730C31113CF0AAEC5608455AC547494D7CA7,
	JsonPosition_FormatMessage_m1E221745622F3A5A217A03CC6C99A51DFC015921,
	JsonPosition__cctor_mE5BBF78165A85742910DCB97C7A378039B243AD3,
	JsonPropertyAttribute_get_ItemConverterType_m4B6B27AC08330039C98EEC8CD2ED6E0594100A80,
	JsonPropertyAttribute_get_ItemConverterParameters_m7541013D32024590A01A74A974E59C1E175D218F,
	JsonPropertyAttribute_get_NamingStrategyType_m7E0DECD0FC26319048A77974A04FE992F789C484,
	JsonPropertyAttribute_get_NamingStrategyParameters_mE3FE6F503EE89057BA2A8250714852382D9F753A,
	JsonPropertyAttribute_get_PropertyName_mF1066F5ABA5DB30B47C643523CC7A5B1320D99BC,
	JsonReader_get_CurrentState_mE9257C8DE44E2E2FFC91CFE6CC38B4645926B87C,
	JsonReader_get_CloseInput_m6B7725C8D9DC8A2B3402A81344A63498F0C7D724,
	JsonReader_set_CloseInput_m16D053F85327E2A97A0B7677D994A2665921FF52,
	JsonReader_get_SupportMultipleContent_mC50C66DE453AD3A0F510C2A7D6AC28BA62A1698C,
	JsonReader_set_SupportMultipleContent_mABCCAD5810A129EE7F9CE71218BB3D71909EA6FA,
	JsonReader_get_DateTimeZoneHandling_mB1E72523A4BC2644910C4E9855091456F32C05AA,
	JsonReader_set_DateTimeZoneHandling_m58DAFDBFA2B8851F83C8B4B82C7106314FAE151F,
	JsonReader_get_DateParseHandling_mA75F45CBF7CD7A1B9C4DAA770867F9D597CE61E9,
	JsonReader_set_DateParseHandling_m27B77AFE1688593BC0E8910293E33416481542E1,
	JsonReader_get_FloatParseHandling_mE9263A993BE6C823745205847EAA59561E86034A,
	JsonReader_set_FloatParseHandling_mAC1FB4A0A6D47F54DB72DDB167D4A12618A48E0F,
	JsonReader_get_DateFormatString_m2012D03E952DE4EAF3F24925DBD857CBCA11C8D2,
	JsonReader_set_DateFormatString_m7072A352582E68458BCE43FF3CABA6F0A8FE4E5D,
	JsonReader_get_MaxDepth_mD820201F2F7D55EE7BEF65F5FA988922730FAA37,
	JsonReader_set_MaxDepth_mA79BC9CBAB51AF7B187A77FD37D08A40CABAA198,
	JsonReader_get_TokenType_m9D40984F9628FF1F4516F0192038EAAA0B08982A,
	JsonReader_get_Value_m5BB85A7BFB0978CF205098C18D7DB85178A519A5,
	JsonReader_get_ValueType_mDA60FBE0FAC4390C33A0FAF15FEA236D0D8823DC,
	JsonReader_get_Depth_mBF4A523E7448C8F3BA76E4F8771BC18B07395406,
	JsonReader_get_Path_m737454190C235173BD176363A79B3E928E5B0A37,
	JsonReader_get_Culture_m86CB2F35EDA466949BB203F7BD6106C859FB4088,
	JsonReader_set_Culture_m649FF7F5DC53B0C9389F20E07566BE7960D63AE7,
	JsonReader_GetPosition_mEC14FC2503F4EEF172EAAD966FE03A2EDBDC447E,
	JsonReader__ctor_m6435D6519810700AA3F206AD3C1F611EC27D3D78,
	JsonReader_Push_m2CE8921ACE143040888CA7DDDBBC444BD6FB4295,
	JsonReader_Pop_m31AD4B3BDF093712E8B0BA0C6799B95603556EDE,
	JsonReader_Peek_mE5A6870F9310C854FCA8DBA769F3336950EC4C77,
	NULL,
	JsonReader_ReadAsInt32_m9D143221536A9C552B78F03C18AB6257A6ACFD59,
	JsonReader_ReadInt32String_mFD4A4A7D68D9F14EFF26DEB20EB4A3EA8DC12C81,
	JsonReader_ReadAsString_m674CE8AE02859F88AFC75C3F9CE888C609B9830A,
	JsonReader_ReadAsBytes_m9E20E94F6A91714519BEFE62718FCD834AAEC698,
	JsonReader_ReadArrayIntoByteArray_mDDB2A8F383AEE45F2DCAF57A2817EBADA18F82C6,
	JsonReader_ReadArrayElementIntoByteArrayReportDone_m0495F5A1876A85F57188430C3AD5EC9495FCA289,
	JsonReader_ReadAsDouble_m31358D0C45601C032F809A54BA174A2347D2DA73,
	JsonReader_ReadDoubleString_mD84724DBCD98CF499B10E87087EF74AAEF397F30,
	JsonReader_ReadAsBoolean_m57B718A704F15C0E180EAAC1DDD96850AD205B30,
	JsonReader_ReadBooleanString_mE50CB9A6F524C2CD091571AC9C98F529958DEFCE,
	JsonReader_ReadAsDecimal_m8EDF5399CFEAFE9968B320B78B63DF86C3DBB87D,
	JsonReader_ReadDecimalString_m4613D50A65F43F73D59DBCA51BAB57A4EF305C87,
	JsonReader_ReadAsDateTime_mE7FAACD30ACC184CCC01A7FE09AE64AD64E97586,
	JsonReader_ReadDateTimeString_mAEDFCE1DDAD8E07B9A071298CCE710AAC7E85F14,
	JsonReader_ReadAsDateTimeOffset_m26D64892F6994A956A39ED5A5D9F308F2B406B96,
	JsonReader_ReadDateTimeOffsetString_mFEE9A470585E5E121B7E11D60FC4F01485A6546D,
	JsonReader_ReaderReadAndAssert_mADBD0FDC473BA90557EECE8C5736BFD1C8D2083B,
	JsonReader_CreateUnexpectedEndException_m62AA28C49F7CCEAC1A71C0C59427EEF0E2621BC1,
	JsonReader_ReadIntoWrappedTypeObject_m94C238BD063EA6B635748029F49DDAA99973A0C2,
	JsonReader_Skip_m9BC7ACC43FC591D25DB584EDEE48E8565E81C7A2,
	JsonReader_SetToken_mF760E05707D8BBB9BAFE7F881EDCDCC5F041FDB9,
	JsonReader_SetToken_m25B74EA504701DD5727894D38E21127DA43EFF71,
	JsonReader_SetToken_m03EFB6B1B50834851EE1000E0F930625DE3065BA,
	JsonReader_SetPostValueState_m84FF7954F1945C5AA9F3C12E7AC5DE7D82B14EC0,
	JsonReader_UpdateScopeWithFinishedValue_m4EF50B861EA117072A615EA48FA64A1FEFEBF064,
	JsonReader_ValidateEnd_m95DCF892763FD53CC04B998713E7DACD7B133970,
	JsonReader_SetStateBasedOnCurrent_m287B22E68E1AD9A58596E549D67B96032398F119,
	JsonReader_SetFinished_m161F3CE2916D0B28CBAF7D7EB9483B8EB81D498C,
	JsonReader_GetTypeForCloseToken_mEC31AA4B686704C087546D2DB97DE4F5CAC1FCA5,
	JsonReader_System_IDisposable_Dispose_m25183F5F0B76E01282CFE99189415B2A8F8AF338,
	JsonReader_Dispose_mE2A0BF3EB0E0C47A8544D206CF4291460BAFE194,
	JsonReader_Close_mA4BD0D0E0F0BF1053F6E71D704690558AE4B3CEA,
	JsonReader_ReadAndAssert_m60377A4E982731616E2DD35DC0C776E555BD2403,
	JsonReader_ReadForTypeAndAssert_mFC6BE5BBA870DDB1F58898805BA3C9148BF271D8,
	JsonReader_ReadForType_mCA99B3BA3FED0DF53C6300F473E98D51BF90147B,
	JsonReader_ReadAndMoveToContent_mC9B4355733551D774CF9BBB87CE54FC4BD8D55E9,
	JsonReader_MoveToContent_m572417EEF0425D17E05D87991B6CC73345304D3E,
	JsonReader_GetContentToken_m18467A9E395500A24458B5071383A21746AFB267,
	JsonReaderException__ctor_m17709A483BE5AFC82C9CD4C1392A1B482EA02612,
	JsonReaderException__ctor_mE599F48E0D998BA4D43BA76F6B09FFD5FD974199,
	JsonReaderException__ctor_m3F643A49E0E96AAD530F9DCE7041164D49D2B011,
	JsonReaderException_Create_mBFCDB362520B63E24F19F7F668A7D1A26D244393,
	JsonReaderException_Create_mCA27B10DAC0A21E578CB9A737471CA8CC8857ECC,
	JsonReaderException_Create_m1C6DF4EDA32CB02CD7EF0935DF9F923A200476BC,
	JsonSerializationException__ctor_m634DBDBB07F3DD2925EE7471AF309BC81AFFA46F,
	JsonSerializationException__ctor_m362C8866E990FE51924BD56A23183697D88120B6,
	JsonSerializationException__ctor_mC0D8B49862C150AB65EF289D9F9AEE4DD6FFD00D,
	JsonSerializationException__ctor_m8CCDC62941FAD083CE6596C4847D56F6A30C63F8,
	JsonSerializationException__ctor_mFC075421E04C9719868FE9892B672169E978972E,
	JsonSerializationException_Create_mBD96D2D781F3F0136C9C7178922E6C132DCA463A,
	JsonSerializationException_Create_mC7AC79438307341E50881B959F24FE18E002FA6E,
	JsonSerializationException_Create_m9513A22FA51B54FF18C132C275080310B0FDA5CF,
	JsonSerializer_add_Error_mB0BCA4E7A163D55B673D8F4EBDA37D01C836F77C,
	JsonSerializer_remove_Error_m8B2DEDCADD72698B1980AF73482C589B88AA2A8F,
	JsonSerializer_set_ReferenceResolver_mF8FC1A12F8D2D2216FD728747A541A03D2C85168,
	JsonSerializer_set_SerializationBinder_mD31607A4C5768610E17CF68DE5777F0679DE963C,
	JsonSerializer_get_TraceWriter_m1E6DE87F57E994741B993145B1759C3238047AE1,
	JsonSerializer_set_TraceWriter_m1E9B2458F21E89A819B06AF56B42F9AFD65A0DDB,
	JsonSerializer_set_EqualityComparer_mB9463E38FEDF3D4C8EAD0B6A75C448BF619FB29B,
	JsonSerializer_set_TypeNameHandling_m5BFFBC69CCA954FE7114D213E2D9CC36342D9ACF,
	JsonSerializer_set_TypeNameAssemblyFormatHandling_mDDAD0FE4E0E32119A1BB825905713396ECA2DE21,
	JsonSerializer_set_PreserveReferencesHandling_m40B87C043D190DCDF2B8235061FADF119886F731,
	JsonSerializer_set_ReferenceLoopHandling_m4A23E49D3E379B1388F59CE7D9934F05E3966F27,
	JsonSerializer_set_MissingMemberHandling_mDED52F36DE2DE2A69A03CBA1FB1A377E56A35503,
	JsonSerializer_get_NullValueHandling_mE0CE90ACD6A1D02EF50C278AF30AF75F54CC6681,
	JsonSerializer_set_NullValueHandling_m3AFA3FE7E0901A608245E5D66BF87418F8FFA324,
	JsonSerializer_set_DefaultValueHandling_mDA03DA259E4C3ECAAE2CEBA61FB576A2FF05965A,
	JsonSerializer_set_ObjectCreationHandling_m91A704DEC7B648681D14B0A143702F03B02C4B41,
	JsonSerializer_set_ConstructorHandling_mAB93A88F957789A5137AF24DAF561D12E702A583,
	JsonSerializer_get_MetadataPropertyHandling_m3BF51DB8C02F4B8BD2DBC1817482EBF4F915FD67,
	JsonSerializer_set_MetadataPropertyHandling_m2163D5C589945BB28D6685E4F3D5DDFBC94D378D,
	JsonSerializer_get_Converters_m7E3D7FD78BAA52920392FECC1D162BACBB307A06,
	JsonSerializer_get_ContractResolver_m9DCE120E6F5FEED68FD45C9744F09D3DC3BF0DEE,
	JsonSerializer_set_ContractResolver_m23E7AC4F5B365A1433D65CC6520023A8FB7CCB2A,
	JsonSerializer_get_Context_m3CCAF37318CD5D9B45D01F7446C78205CDBC2EF3,
	JsonSerializer_set_Context_mC05152A1D91767ED3CB229CEE539E9824588F52B,
	JsonSerializer_get_Formatting_m526158D742DE52EF751C9B5F7CD3CBF2F81771DC,
	JsonSerializer_set_Formatting_m3E116ED355352780B0E2B666C1FC66B60CD08018,
	JsonSerializer_get_CheckAdditionalContent_mFAFF37D6C1A793D156EEF98EB265296F68618BA4,
	JsonSerializer_set_CheckAdditionalContent_m32C104748C50C14A22C25E465B6C51ADEE7406A8,
	JsonSerializer_IsCheckAdditionalContentSet_m0ECE9513F8BCFC08C239F228D5363E07FA0D5A6F,
	JsonSerializer__ctor_mAD92BF954F1F4AE0307D54EC58195F80C62584F6,
	JsonSerializer_Create_m38AE1D6F6AC13F2B1886F944837888AAD480BBBB,
	JsonSerializer_Create_mD5BACE2C41A3569A68146E9ADBC2E16AE11BC534,
	JsonSerializer_CreateDefault_mFEA7B08199B331CF80CE4B7E1051E362B117DDDD,
	JsonSerializer_CreateDefault_m8982D86A2D017A4AB3AD8F8F65CCDDD8A65C649E,
	JsonSerializer_ApplySerializerSettings_m68F831B611204D7823B12684ED97AF46CAEAC6F2,
	NULL,
	JsonSerializer_Deserialize_mF21BAE6FBB07736E98FE3E568C97A789BC69F741,
	JsonSerializer_DeserializeInternal_m4AA70E53989355DDCC5AA2C830504A08E0B3F7DB,
	JsonSerializer_SetupReader_m9BF978B3F5E872B4CAAD0DF7FB834A162564DD7A,
	JsonSerializer_ResetReader_mFFAC8C34425014083E41FCB09FF5B7CE32201118,
	JsonSerializer_Serialize_m47CE5177BE3A0035C88BD9E194CEE0EB06315190,
	JsonSerializer_Serialize_m4D67925906BBB7B57B032021F0E27854800C7660,
	JsonSerializer_CreateTraceJsonReader_m0622574AFAE5E8EB1F714FD1CF649DD92F78F19B,
	JsonSerializer_SerializeInternal_m35515DF8607422337C334DAE42EDF9832A7F9BF6,
	JsonSerializer_GetReferenceResolver_m90EA406FF935E63E4F0502760993BBB529E8CB7A,
	JsonSerializer_GetMatchingConverter_m18934A32207B9B0BE108A745033DC6DDC8B324C2,
	JsonSerializer_GetMatchingConverter_m1353F204DEC337892BDA3D6EE9FFDDF5EF6525C5,
	JsonSerializer_OnError_m289078F0F035DB054768AE2F12C4A84E4FE5A3FA,
	JsonSerializerSettings_get_ReferenceLoopHandling_mAFBACB55BFAA3628BF251BA6093398F8E4FABF09,
	JsonSerializerSettings_get_MissingMemberHandling_m38ACF479C56819F31A97FA1F699C4BF9509DFCEE,
	JsonSerializerSettings_get_ObjectCreationHandling_m2E22656E2D4167EF9E800F449CB150DDF8E9B8DF,
	JsonSerializerSettings_get_NullValueHandling_m9B348EE414738F88AEE2B1177736BA48409D70D3,
	JsonSerializerSettings_get_DefaultValueHandling_mDF585B9DA1BFCB513677851151EB6FDE7D844032,
	JsonSerializerSettings_get_Converters_m2082CB93FEE2556D1DD20EE22FA96E91C91AD254,
	JsonSerializerSettings_set_Converters_mE404F2FFA8927D289E012E487733071355E03582,
	JsonSerializerSettings_get_PreserveReferencesHandling_m3218CAD106363185DF0345F68ADA6F818E280BF3,
	JsonSerializerSettings_get_TypeNameHandling_mF037D0B9A9BD973B3B489697FD61CFFCE2FB9071,
	JsonSerializerSettings_get_MetadataPropertyHandling_mEB517B5FFC03BF2DDD812B93BC515E02B9FAEF8B,
	JsonSerializerSettings_get_TypeNameAssemblyFormatHandling_mABA42D2E33149757F1873B2CCBD45B81AD9F2923,
	JsonSerializerSettings_get_ConstructorHandling_mBB0D4BCACC023AA9A26B86584245804EB7284694,
	JsonSerializerSettings_get_ContractResolver_mC8D819BE1B3693E1662B298A4C0F148D9CD5121F,
	JsonSerializerSettings_get_EqualityComparer_m3D32A0754A35A1C75268260645EAB0CC9FA1EFDC,
	JsonSerializerSettings_get_ReferenceResolverProvider_m500B89B016F7144AC35F639C70A9BB1A81F87616,
	JsonSerializerSettings_get_TraceWriter_m2554972134F06B7A10FF17897569E0817C9907B2,
	JsonSerializerSettings_get_SerializationBinder_m3FE5BC66686F5A035E6BF30B7308FAAC7F92C327,
	JsonSerializerSettings_get_Error_m132FFF7E6DAA4BA2910806152F01DC4EC06C9147,
	JsonSerializerSettings_get_Context_m2217C6A8A494E2CD2DA054E1D8305BD3BD269686,
	JsonSerializerSettings__cctor_mF16A8C54E3C80878C036C68EE9BD0C28518AEC48,
	JsonSerializerSettings__ctor_m843CA829AC2170EBC1F50AA7A487380A6D16D3AF,
	JsonTextReader__ctor_mD757FE66102CF9BDD13810CA37A3983C6B0495C6,
	JsonTextReader_get_PropertyNameTable_mCD88FDBD5C8CC99817BECAED46DC2AEACA54C96A,
	JsonTextReader_set_PropertyNameTable_m3DCC659AF803257DA259127CDC8981E2F13879FC,
	JsonTextReader_EnsureBufferNotEmpty_m513F7E24DC100EB1BEE528ADC0CE37E420F220F6,
	JsonTextReader_SetNewLine_mC677256B86304E019294AAC4063D23D67B5350EA,
	JsonTextReader_OnNewLine_m6B048A456F5BC924D60961806C51602E4AE79CE8,
	JsonTextReader_ParseString_m7AD39F57DFD2AE50822189F23D76D720FA39A132,
	JsonTextReader_ParseReadString_m75B563FB0DD5D0B814F26118DF905223E5568EBA,
	JsonTextReader_BlockCopyChars_m24FDD693272A89733F0287385CE27F79D5807B93,
	JsonTextReader_ShiftBufferIfNeeded_m8E175EB836A7AC8CEA617EFDD3629E8242629B62,
	JsonTextReader_ReadData_m87C1A07DD90BF68CE245BED20ABA4997FF930535,
	JsonTextReader_PrepareBufferForReadData_m7092C65BF7691B6896327A21FFC7EF475705AC45,
	JsonTextReader_ReadData_m9F47BF6E23F177F044F02108CD0C843F8A2C327F,
	JsonTextReader_EnsureChars_mD53AB0A9160D2526564C88CC63BF6BEF6AB4CA81,
	JsonTextReader_ReadChars_m9B138130964FE4738AB1C49878D2AF1CCB07F703,
	JsonTextReader_Read_mEE8C16634337970B5F1C65F5EC359F992CD3CD2F,
	JsonTextReader_ReadAsInt32_m2469B2B1E5A50D5D6FFF56839D74ACA81B48F038,
	JsonTextReader_ReadAsDateTime_mF001194EA3CB46A1960BED74CEF20AB0352FB9E9,
	JsonTextReader_ReadAsString_mED6FB834CC6F852E68B8E23C41B033DC5C9F374C,
	JsonTextReader_ReadAsBytes_m22C65609230391B135110B140A25CEA6612F6C28,
	JsonTextReader_ReadStringValue_mFE19FD5EAF58D0D307C00B21EB0B450DC97A9413,
	JsonTextReader_FinishReadQuotedStringValue_m47A54F9C73474D1D4797AA751B81642F38E128AB,
	JsonTextReader_CreateUnexpectedCharacterException_m0B919A9C1EFD7EB9F36815B8D19249B559DA5590,
	JsonTextReader_ReadAsBoolean_m94AC14A953F4912D5477F1B3ADF6EB4EF0A62A4B,
	JsonTextReader_ProcessValueComma_mB8631556021FCB76F1B8555DE168A523CE1E55C9,
	JsonTextReader_ReadNumberValue_mD32A235B467A325B973FF183AA7B14344C347DEA,
	JsonTextReader_FinishReadQuotedNumber_m50221D0C91C3AED53B94F98B44EE99E3DD13935E,
	JsonTextReader_ReadAsDateTimeOffset_m325F1FFC2497EC35DE57C2BB736F88CD810D3B95,
	JsonTextReader_ReadAsDecimal_m8C890DFD8722407E1D6B48AFCC9A74C71616DC8C,
	JsonTextReader_ReadAsDouble_m60DAD9A7FF93E0BABBC303769BE48AC5DC5D77E2,
	JsonTextReader_HandleNull_m6373EA41EA55987A3A9946CF7D218EF35F434B24,
	JsonTextReader_ReadFinished_m9AF6D1992F4416B85F0850D3AC59F32BDBE7E2DB,
	JsonTextReader_ReadNullChar_mC05A3E61BFA83892FE671635D25F437CDAFF522E,
	JsonTextReader_EnsureBuffer_m2089B726D8BD8CCBF2B02A6D92839ED3A50C26F1,
	JsonTextReader_ReadStringIntoBuffer_mFD2D9918238BE262788BA3432D0956A538C8A96E,
	JsonTextReader_FinishReadStringIntoBuffer_m6027917EB06E9D5A86C5ADD88B08A442682A0504,
	JsonTextReader_WriteCharToBuffer_m90E413DFB9121F57D1AB5A464DEFED08FC79AE00,
	JsonTextReader_ConvertUnicode_m39CD283E5DE3D0AE1F21B813966FADBE3D862B7E,
	JsonTextReader_ParseUnicode_m3A9955A2F54E01141600B4F9535D0B9D74814BEC,
	JsonTextReader_ReadNumberIntoBuffer_m6CD1125FC52E738F1B782E3A25DF5E2C3FAC1D81,
	JsonTextReader_ReadNumberCharIntoBuffer_m5B05D56492B99BC40CAF9CE2C008342B2DD54ACB,
	JsonTextReader_ClearRecentString_mEBA79BC7ED7D388F4D213743300990C9896D8581,
	JsonTextReader_ParsePostValue_mABCAF889C3AAE744A4ED551AFAD67AD680826AC6,
	JsonTextReader_ParseObject_mAC24C4AA2D70E220DA36B86237293157911D9E26,
	JsonTextReader_ParseProperty_m652D117928358983ACB9D4BD43B89829E2FD2587,
	JsonTextReader_ValidIdentifierChar_m8091FEC53B3583B0EAB8D67EA1E42C4EEEDFEBED,
	JsonTextReader_ParseUnquotedProperty_mD0915F072E723B0B7B47D52E4118EF710A765472,
	JsonTextReader_ReadUnquotedPropertyReportIfDone_mC002DD09CE9C0FFE2D0FB2069439F735A8F6E582,
	JsonTextReader_ParseValue_mD2F36D571A869D97A8F6217B8B0288A8A0498B9D,
	JsonTextReader_ProcessLineFeed_m448B054CE009287F09571D9B28F3564B283BA3EA,
	JsonTextReader_ProcessCarriageReturn_m275932A53EA9CA67A70D2FCA5173680A971DEDE1,
	JsonTextReader_EatWhitespace_m66536C21721CC37BE7F02995ABD04F41C5A98196,
	JsonTextReader_ParseConstructor_m3C123404371DFED28F04A86EB1248438390BC03F,
	JsonTextReader_ParseNumber_m874FD99EC524E03531A299F64EFD9EE6A01915E8,
	JsonTextReader_ParseReadNumber_m6C29BF3BD841F7E5FA48043ADCB483E1B4FC4B84,
	JsonTextReader_ThrowReaderError_m45EAB30E55AFDCE3AACC56471EA3866F1FC7A778,
	JsonTextReader_BigIntegerParse_m70F36467D855F38C88BEF9994EDD909827292C5B,
	JsonTextReader_ParseComment_m1D2B00F2BDE8919D8B1227C07AA28AADFD5806F8,
	JsonTextReader_EndComment_mCC46197E5124182D7D6DA4F88F1BD6164F97693E,
	JsonTextReader_MatchValue_m1935A7A541C6CCC3E5F5A357FD1E315A3244B973,
	JsonTextReader_MatchValue_mC3BD53BF3DF0C54C2C992E2AB371F3BA63405266,
	JsonTextReader_MatchValueWithTrailingSeparator_mE11D3741D39DF8D6B38FAD8822E509BA4086734A,
	JsonTextReader_IsSeparator_mF5989F18CD1654E53DCD1FE2DD23D3B5BCF674B2,
	JsonTextReader_ParseTrue_m1A10CCF29E57F73C0A73ADF2ED1EAD4773E8A290,
	JsonTextReader_ParseNull_m7AA2765C38C516AF2C00F61A733CC858602B7044,
	JsonTextReader_ParseUndefined_mDF9F2DD5D7FBC845840EFD632CF795200A6DEED0,
	JsonTextReader_ParseFalse_mB7784D257AE3B7CDB89E3644D07CEE2B08E5D10E,
	JsonTextReader_ParseNumberNegativeInfinity_mD9C1D4A929B849432287A1327A7A52B1F2F27D76,
	JsonTextReader_ParseNumberNegativeInfinity_m7962F2C985883617C0DBA6ED10F3B80F628B7763,
	JsonTextReader_ParseNumberPositiveInfinity_m3055DEA9060BAFE180CDF0625E2A737F4F56110B,
	JsonTextReader_ParseNumberPositiveInfinity_mBC768D585BD1832D28B9EABEC5743D918135F0CE,
	JsonTextReader_ParseNumberNaN_m581DCE13BE84E85D021143EADB3D4468351A6217,
	JsonTextReader_ParseNumberNaN_m344AF9F28AF46B53873F374BEAB69230A976FC25,
	JsonTextReader_Close_m7D3AF83A69FDABDC83B3B117F7A51D503B504C7D,
	JsonTextReader_HasLineInfo_m8932706F6A3EAA4D2C38A5256B3C934E75D8529B,
	JsonTextReader_get_LineNumber_m2C75B372D6352EA272C5768E3BB15A2A0A9536C4,
	JsonTextReader_get_LinePosition_m3711496BA16996996E34EA89D202C41405889D02,
	JsonTextWriter_get_Base64Encoder_m9916FB0D93EDCAEB9BABDD34A4C717201A193127,
	JsonTextWriter_get_QuoteChar_mE8C6518BDF2FB09C6958EFB2EC09C7226310210E,
	JsonTextWriter__ctor_mEB186DB56607F358929B1337AE02E17E1B6E9AB4,
	JsonTextWriter_Close_mFD4B3222BC5D7882EF56E3370BF2E6891FB01ECA,
	JsonTextWriter_CloseBufferAndWriter_m6ECC0059C657EF0CAD776CFA08922476F42FAF12,
	JsonTextWriter_WriteStartObject_m7C36C434D9FDAE13C7205FB04FFFCC06B60D1CE2,
	JsonTextWriter_WriteStartArray_mE0274CDEADFB8619324E45FAD9663A3A8D9176C5,
	JsonTextWriter_WriteStartConstructor_m959AF4B291975C89E053DFF288FF6DC1C619388A,
	JsonTextWriter_WriteEnd_mE2CB8CAB0080DEEEBD560A905C3DAB233AD3F848,
	JsonTextWriter_WritePropertyName_m7EE962BBB56396D017F0FDF93288616CA17D01CD,
	JsonTextWriter_WritePropertyName_m76AB324E7BBE746DDAA78019A5C1E6301A868173,
	JsonTextWriter_OnStringEscapeHandlingChanged_mF766E70B02C137B011568406F68F080B03ACE65B,
	JsonTextWriter_UpdateCharEscapeFlags_mB3CE53DE1620FFAF5A16F97282EC76D20BD03788,
	JsonTextWriter_WriteIndent_m9181A051C2CF29CB20A07D00629CFF6FD06EC1E3,
	JsonTextWriter_SetIndentChars_m2245B846F8FF5DAD6EF49EB5DBB5D858B865F937,
	JsonTextWriter_WriteValueDelimiter_m32651A5CF35F02B1CE5F386C91D14AA818C8170C,
	JsonTextWriter_WriteIndentSpace_mA19A3749FEFFFA3110AA81B3CAB25637F3E882B1,
	JsonTextWriter_WriteValueInternal_mE72D8571B70C94CC3B39580AC907AC0BE0CDE30D,
	JsonTextWriter_WriteValue_m936DFF76CD5F7BF8FC8FFB1744981F4F259509AD,
	JsonTextWriter_WriteNull_mC368D44D4B1F71828B087A543A5768F6656A2D5C,
	JsonTextWriter_WriteUndefined_m8FE318E3DCC52C57F4A41B8A12137DC3EA060144,
	JsonTextWriter_WriteRaw_m826731AE5302BFD1A81DF25A0919BC2DD86AA2F7,
	JsonTextWriter_WriteValue_m9B2A4778399C3A08C95790D4856DA6A2ED4B2C7B,
	JsonTextWriter_WriteEscapedString_mB0719DBFB6D26A9479A0DA874975B5F21556C885,
	JsonTextWriter_WriteValue_m299FAED87D3F68CE9C088F1DB0FD2BC1EC54C81A,
	JsonTextWriter_WriteValue_m5ECF2F8B9AD0FD5AF5ACE14B9BC874281D5E5F70,
	JsonTextWriter_WriteValue_m7DA7DA5711CDFB8DE2B92F624221F64F7CCADA43,
	JsonTextWriter_WriteValue_mA86B7915D9E8F5E97283DD32E84FC8A7A527D2E3,
	JsonTextWriter_WriteValue_m2F6A5344A24B8D97E08801625537A4F452695DFD,
	JsonTextWriter_WriteValue_m2084CC543850E22C4F15F4F336AEECBE87C86067,
	JsonTextWriter_WriteValue_mEBD8C1DD1151526E950599F58750DF20CCEF9EF8,
	JsonTextWriter_WriteValue_m10DF1AB911D0EE6221ED8A10DDDE43BAC465FBBB,
	JsonTextWriter_WriteValue_mF8A1B627CBE8CDD02C681197AD334D9C68BF1CA4,
	JsonTextWriter_WriteValue_m3229AFB5B29A399DF8C3B35EF0C2200F811DC0FC,
	JsonTextWriter_WriteValue_m770F089CE64BD6D2513641F9FD788415B261EEEC,
	JsonTextWriter_WriteValue_m23296089CA7E0A28102308106FA3F8E4DE9E3237,
	JsonTextWriter_WriteValue_m10DDF98FAAFDFC317F6EBDC4F79C908B2A90DD5B,
	JsonTextWriter_WriteValue_m43236B80B922F4AEB866DDB31A80CE8F05BF3740,
	JsonTextWriter_WriteValue_m84497D2E47F08E8B2710F42781D8BDFFEA6EBDFC,
	JsonTextWriter_WriteValue_mDA794DB59E9A287DBA36E4517A42720336AC3B6E,
	JsonTextWriter_WriteValueToBuffer_m82C026B4817F510251DB11FDC59349C7F7E815CE,
	JsonTextWriter_WriteValue_m90C8C538315261FFA4A3A625581DAA8164578AFF,
	JsonTextWriter_WriteValue_m5446770BD0943EC4472B55B84E50AC6E840BF306,
	JsonTextWriter_WriteValueToBuffer_mD7B0FA27C9F0ADA8D7F22BFAE505D4CB486167D8,
	JsonTextWriter_WriteValue_m80C96737E42BB9CBACD50D613BB0FEC0825AB442,
	JsonTextWriter_WriteValue_m0AFA1921B23FC347893FB2F17F7ED82F973A244E,
	JsonTextWriter_WriteValue_m5758ECAE2DDB277A82B65ED9A97A9FFED0992FE5,
	JsonTextWriter_WriteComment_mA639FC5D7FDC2BDE6701EEA8D4E9F10298D740B3,
	JsonTextWriter_EnsureWriteBuffer_m3FD2D3051076873D10AB8D935CBCFAD255CA68B3,
	JsonTextWriter_WriteIntegerValue_mEFE39F09FE5158AF12FFEC321E08C97CAD94AE66,
	JsonTextWriter_WriteIntegerValue_m9EDB3D5AD2DBB26EF55DB66A5B76A2B813739DCA,
	JsonTextWriter_WriteNumberToBuffer_m91A3362A091E4722B1CC92C3043CFC1C67FF146B,
	JsonTextWriter_WriteIntegerValue_m15E1116CE9EECE48EB9E92F99FEFC299A6605F9B,
	JsonTextWriter_WriteIntegerValue_m26963CDCA8631FAF88B3DFF733A20AC973115CF8,
	JsonTextWriter_WriteNumberToBuffer_mBB58E0DE048A6531F83F54E49CE93988E5702F44,
	JsonWriter_BuildStateArray_mB4029DF81934CCC7012A74F314D5F4EC4DC6FC69,
	JsonWriter__cctor_m84C6852A1CC9B573989805AD4A8E0ABDC8553ECD,
	JsonWriter_get_CloseOutput_m5FF0E3B6BB76B31A6B0308D4F66D65A33F8D93FC,
	JsonWriter_set_CloseOutput_mC8CA7E44AA2074EC2BE542F8D14B27A592D6940B,
	JsonWriter_get_AutoCompleteOnClose_m622193FA7480DC2E94FF4CA30014D1FA51F17065,
	JsonWriter_set_AutoCompleteOnClose_m9F3B204A96239ABDFBC1C0298BC4A3810C413D5C,
	JsonWriter_get_Top_m2354A453B0DD0528B5A462112696F96682FA7248,
	JsonWriter_get_WriteState_m017AC175CF914096FD4AD0FF4807A89BFB97E26E,
	JsonWriter_get_ContainerPath_m03E4F990ECB42B167BBB8E897F2ADC0E3204A39D,
	JsonWriter_get_Path_mC3E57CCCA1D0C5B869FD19671230F7CC636F73B5,
	JsonWriter_get_Formatting_m64705DF9A1D91C9491580597585197A3B303E5A4,
	JsonWriter_set_Formatting_m5DB6CD74DFDD9B8D14587DD95B933035F65489FA,
	JsonWriter_get_DateFormatHandling_m083B2D33EEA4E87F670B2F892484089DFC8BEF27,
	JsonWriter_set_DateFormatHandling_m2FDED018732705C95CA2FB7A78F92DAC7418CE72,
	JsonWriter_get_DateTimeZoneHandling_m93B5E389843B9BCA59F5DF0EF6CEB54000071E75,
	JsonWriter_set_DateTimeZoneHandling_mDA4E11034DC93715091D58173EDF791A8E290FA4,
	JsonWriter_get_StringEscapeHandling_m712D7D3A01F8FB07140D147094B14BC322EA5CD9,
	JsonWriter_set_StringEscapeHandling_m9E210B17B4858882560226E668706C097BC37972,
	JsonWriter_OnStringEscapeHandlingChanged_m1A437C6B83EEF74B13CC68FC652CDD0815828C5F,
	JsonWriter_get_FloatFormatHandling_m2B9D45FA45DAC4256D52E99F512C6AD1163ACB67,
	JsonWriter_set_FloatFormatHandling_m579A35628D92FE58E6D342A621C889ABA8684829,
	JsonWriter_get_DateFormatString_m9CB1F8206E0FDD6CB2538DCA5A1762478453B9B9,
	JsonWriter_set_DateFormatString_m907BEAE6972D801EFC845C344F39AB900890AB10,
	JsonWriter_get_Culture_m43859002F83B5F3A286FF4CFC6DAC7C55BC9E23B,
	JsonWriter_set_Culture_m026FA089CF142BE15CF21A6C95DA59B8C18AEA22,
	JsonWriter__ctor_m66A258C60B929D8BD124F92C63348F8C4731E90E,
	JsonWriter_UpdateScopeWithFinishedValue_m53DF3F5D1797C278B22FBFBE0435A6E2922B766A,
	JsonWriter_Push_m6CEFC5AE6C8554750AE111E3D70A2F0E8BA14C5D,
	JsonWriter_Pop_m8410D60A45BDA750D2CC7CF171B59771E70AAE41,
	JsonWriter_Peek_mC209157E349F1FDF5EA189E19EB4B673501D24F3,
	JsonWriter_Close_m6FBCF9BF49FC7530B4BD4DA2FE5AD92681525436,
	JsonWriter_WriteStartObject_m3D06C9E26B6056E8ECE2369656BD9AC5BE0ED2C5,
	JsonWriter_WriteEndObject_mE0DBD023E46A8E33E7EF19C0D0B8618521CC48A1,
	JsonWriter_WriteStartArray_mFAA44AB50BDC729500551CF697896B3EF8A96014,
	JsonWriter_WriteEndArray_mC83C124C0145E60569E14BF5207DCA47FFF52D14,
	JsonWriter_WriteStartConstructor_mA969BDFE136EC5FC185DDC748D6943725C2410B4,
	JsonWriter_WriteEndConstructor_m9951DD1CEB792CBF9FE360B24C2A03052032179F,
	JsonWriter_WritePropertyName_mD397F092D5C206547C215174EAAEF8264E22650B,
	JsonWriter_WritePropertyName_m264E6F18C94285D9C7876865DE493938EB0C9546,
	JsonWriter_WriteEnd_mB41CC40E8D86D8A57CD7142ACEC315EC7B62FC81,
	JsonWriter_WriteToken_mDB5563BBD03F1B1A148112246AD516AE0E8EFA52,
	JsonWriter_WriteToken_m3B7E8026BD68E5F27DE19E912E4738B02B6A737D,
	JsonWriter_WriteToken_m5A89C041AD8A697864BE3D0467171FFCF4AABD21,
	JsonWriter_WriteToken_mE72503A890E625DCAAD1B20E7A3FCD4558FFB36A,
	JsonWriter_IsWriteTokenIncomplete_m60EC01D0CE713BF95D33B9A2C34132FC1A202BA1,
	JsonWriter_CalculateWriteTokenInitialDepth_m363C2E8CB31589DBF514241530BA1AFC70A55A61,
	JsonWriter_CalculateWriteTokenFinalDepth_m832F054479AAD477DFB90059431476B1D97C3B5A,
	JsonWriter_WriteConstructorDate_mCF788E169C72B7EF6C6BB8BBDDBE9EBC1A6DB96B,
	JsonWriter_WriteEnd_mA0F3BC719E8DF7264D0110970169B59491AD6CBD,
	JsonWriter_AutoCompleteAll_m29E90D16C7F4B0CA8313B7B84A993E3BC0D1A7CC,
	JsonWriter_GetCloseTokenForType_mB22E5F1F10D778E43C58711859522FE59D172B3B,
	JsonWriter_AutoCompleteClose_m5C458AF9ED22C93B9EA0D17005986322B97C16B4,
	JsonWriter_CalculateLevelsToComplete_mE52143CBF753EC86289FB73CAC879D50E164CD7E,
	JsonWriter_UpdateCurrentState_m0C204307995D802552DDF52683F9B7E701ADB1CB,
	JsonWriter_WriteEnd_m859D5A13D90D3709504DD2D69C41F5377DCF059C,
	JsonWriter_WriteIndent_m34F017E8045B45DE46ACC0264B9BA8B9D8612CA6,
	JsonWriter_WriteValueDelimiter_mEEEDD00E60D8C42DFC9F61FADEA7FB595A2ED694,
	JsonWriter_WriteIndentSpace_mDC8FFB2FD18AD9AA4DC0CEF462478AAF4C458C66,
	JsonWriter_AutoComplete_mEDC0587330A9C4B3D23143E7CCC4781A204059CE,
	JsonWriter_WriteNull_m552D7BC51FEE5DB8AAD3F335F4F8E452ACE4A3D2,
	JsonWriter_WriteUndefined_mBE8A092DE095039F8075C7BE0EAD17A392939983,
	JsonWriter_WriteRaw_mFF3946B43D5A76824F32C87BE18F1F3987A04EF6,
	JsonWriter_WriteRawValue_m145C8AFAA99A98312F883F3FA6CD38429AFB2632,
	JsonWriter_WriteValue_m777AE165C2E70C0C5C81EE7BBD269853272109E1,
	JsonWriter_WriteValue_mCD008395307C860CF687FB7D2BAB861659A716DC,
	JsonWriter_WriteValue_mBB737FA5C808CAE31BCAEE10BEE06F6DABA0CD50,
	JsonWriter_WriteValue_m52F0AC08B76373382BD0292082A00A5A8F3A27B5,
	JsonWriter_WriteValue_m8020C6BD0E8C8868339401489703EBFAECF085E7,
	JsonWriter_WriteValue_m7FC0CF4A0258F72CED4F8BD91D0261BE048A50CB,
	JsonWriter_WriteValue_mCAF862AEC66BFE336D9899BFAB4BD1E16EFB0078,
	JsonWriter_WriteValue_m14CF041566177F456CBB8674A5CEC3CB6C8A30C3,
	JsonWriter_WriteValue_m3D818BA3F94F43D247E8A11C2B53186646207E4F,
	JsonWriter_WriteValue_mCF7BEA7ECF73067857A06EE5D56764F3950E984D,
	JsonWriter_WriteValue_mD26835A94141F61E65C995EB6DEFF5C71929C2A0,
	JsonWriter_WriteValue_mF49FAE8699F53462FEEB6E0841707289C22C08A5,
	JsonWriter_WriteValue_m06F820D0E98FAB1C995D971350D5707FAC990D36,
	JsonWriter_WriteValue_m793F9858AD678612B614CAEF45474B3FFDE60B07,
	JsonWriter_WriteValue_m7FD998131F74873AE1100ACE78972207F4B51531,
	JsonWriter_WriteValue_mD9629C3F618774552AD17075645AC3CCF61A594A,
	JsonWriter_WriteValue_mE4992CE60A42487D0564D675E2FF2A5373917B5F,
	JsonWriter_WriteValue_mCDEFDC530CFCBD00A215958F7869CC7A0E8197A0,
	JsonWriter_WriteValue_m6B4A111D549F6A348A8B435A4138E307B1CD98C7,
	JsonWriter_WriteValue_m30641DD12FA3D89E4F74E3F07747C01491184102,
	JsonWriter_WriteValue_mCB7878AE73DD921638D3485628B4FB16D35A7745,
	JsonWriter_WriteValue_m809D95F4898B71DB8ADA09171E8249C4300989ED,
	JsonWriter_WriteValue_m35F418C494DAEB11DAB4B6BD47D742245760D2A1,
	JsonWriter_WriteValue_m15BB97F0DD73344057BC854AADD111375E384AB8,
	JsonWriter_WriteValue_mAA02BBF0FE3D01F558082573B2C9804C785F0A9B,
	JsonWriter_WriteValue_m61CCD71C3585F45DED94EB0D93CFBC2A304E992F,
	JsonWriter_WriteValue_m0D2AFB7EFA6A9853E18F9014CEB964F3C715F6B2,
	JsonWriter_WriteValue_m61A87E8ADB473B2B3C20BCDC88FF65F0518BF182,
	JsonWriter_WriteValue_m22324F70BD305201ADD6D7FCABF7A15DAF29C805,
	JsonWriter_WriteValue_mADA04F2D6A16CC432037347DA69DBDB6621899E5,
	JsonWriter_WriteValue_m83D12F21167C1D6756F452D5512674FF930D49BD,
	JsonWriter_WriteValue_mC4FE7A1CA1A8F951BDABDA01774461DA577BEFA2,
	JsonWriter_WriteValue_m6E0F2CD366ADFA5F7FC23DA5B7589B796385639C,
	JsonWriter_WriteValue_m1BFAF47F81C721C377418567826EECD8A2AF0223,
	JsonWriter_WriteValue_mC99378DC7CE8C049523DC71BD646A2FBC305539C,
	JsonWriter_WriteValue_m47876EA0CA86D0484525C60B52D3095B68BC1D5A,
	JsonWriter_WriteValue_mB39AE2C0D705E4CA2E5332B7E5CEBD41DC529FDB,
	JsonWriter_WriteValue_m16403749D7AF4C994657AB3B6718A6F162A67F33,
	JsonWriter_WriteComment_mCF1E16F4E31982BF2EA150A7634EEA1D23A39782,
	JsonWriter_System_IDisposable_Dispose_m7A0CBF67068D4E0CFF85F56D2F3262075BC27BCF,
	JsonWriter_Dispose_mEC2E7AE9B5F5789033A2505B764837C05F4FD0AF,
	JsonWriter_WriteValue_m3878E2A3F7645D06F0EC55819C340161B76C1665,
	JsonWriter_ResolveConvertibleValue_m3895705B1775E01F4C23C80ECC7675FD6DB79999,
	JsonWriter_CreateUnsupportedTypeException_m86B2D476DFA392F9D04D90B20B75FCEA5E5B7DCD,
	JsonWriter_SetWriteState_m0A34DF0F4A4F65BC87F5D1149C6435B536EC53E2,
	JsonWriter_InternalWriteEnd_m5372D41D8643B46072A4FA66A6D28B13FDF84C5A,
	JsonWriter_InternalWritePropertyName_m2F31FD7BAA9621AAE842E822AACE0CE6BC3FFB64,
	JsonWriter_InternalWriteRaw_m223C778B3D873B446104D8313C7A6DD306248B82,
	JsonWriter_InternalWriteStart_mC8B0B74D09E5AEEF030C9BA5EA1FAFF13E8265B3,
	JsonWriter_InternalWriteValue_m99C4297885F09BC33721461E7B5B52EF3F939785,
	JsonWriter_InternalWriteComment_mEFE34AC1697C516BE19E2A16B8A6D3FE0361044D,
	JsonWriterException__ctor_m2C69D350D91879C4494B958A6E823EBA7EFBEA28,
	JsonWriterException__ctor_m1DADCE5C7A2141DFBC403D632181630126A962A6,
	JsonWriterException__ctor_m1F9D74B17C17FC1425C67FE0AB5874E23A89645A,
	JsonWriterException_Create_m5825DFE38AEB983B3A597F18661916702AD976E9,
	JsonWriterException_Create_mCDDC9024ACDB1460E49AF35A3502022C68DA30A1,
	Base64Encoder__ctor_m982CBB72C173A975E585F57B63D7AD5415533DAC,
	Base64Encoder_ValidateEncode_m8308A330730304551CAB18E05F986105ECBF4F7B,
	Base64Encoder_Encode_m27FFDF2EDE9DFC40DB04FCE17B50C4B803377D14,
	Base64Encoder_StoreLeftOverBytes_m6F384D73FB57B9B9DD948F41F2124DCC00F5130F,
	Base64Encoder_FulfillFromLeftover_m27B705EFA17278FC1B2DF4B1A10E2BCF837F7D8F,
	Base64Encoder_Flush_m800E9F9994B0196447F754E52A3427911AD3342C,
	Base64Encoder_WriteChars_m308B0DB7A2788256714998251D41C5D9B78BD68D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CollectionUtils_IsDictionaryType_mBB7E6392C2B3D65BF209A555F653C91826EE3DD5,
	CollectionUtils_ResolveEnumerableCollectionConstructor_mD08643BA723A89B494E122145D3375BA6CA3F6B7,
	CollectionUtils_ResolveEnumerableCollectionConstructor_mBF195DB8E5EF6F11867DA8E7894DBD00D59C37B4,
	NULL,
	NULL,
	NULL,
	NULL,
	CollectionUtils_GetDimensions_mAB57BCD2D9BA63A2158ABFF46F965097DF6AF47C,
	CollectionUtils_CopyFromJaggedToMultidimensionalArray_m484084C058991DC9DB43FBADE418CA043CA7B35D,
	CollectionUtils_JaggedArrayGetValue_mFD3099263FDCDB4707CAA843ABA307E228A6D55F,
	CollectionUtils_ToMultidimensionalArray_m3089FD29439160A238C9BC17215697DB4FAC3C68,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeInformation_get_Type_mA706AD5CBD89E6285DD2560F770295B5A7531F5D,
	TypeInformation_get_TypeCode_m8AB63E952423E39EA536D0B9E6BB3FCC4D2C9E7C,
	TypeInformation__ctor_m07FA50E1AF5C91D8721CB072C70FE9C53B1ADE86,
	ConvertUtils_GetTypeCode_mB4A5A95F8664AD1F0B3748E95E923C31B642FC6E,
	ConvertUtils_GetTypeCode_m699C5A721CB1EACC55CCF6B3C5890E6955A3EC81,
	ConvertUtils_GetTypeInformation_mD536E4E084E45A00FD456508CF6B9F6F04B7298E,
	ConvertUtils_IsConvertible_mCD4DD5F8C1E2BEE7DE97E874424EA85DC27AF0B8,
	ConvertUtils_ParseTimeSpan_m172717B02C98E4CB3A0BCD91EA37C9DC2E2588AF,
	ConvertUtils_CreateCastConverter_m27A7B131089FEC17727AF43267B66E17E3A4CED5,
	ConvertUtils_ToBigInteger_mFF5B7E8EF9FC2BB3FAE33B1FAD0A849849FA7298,
	ConvertUtils_FromBigInteger_m50624DDBAA5E23938CF330E81511276CBDA8A059,
	ConvertUtils_TryConvert_m0FC1DF180180D199C32D2B547C31E927F3F2A527,
	ConvertUtils_TryConvertInternal_m683671B6926DFBB7890804B010D6D075A5BD5A45,
	ConvertUtils_ConvertOrCast_m00E1E1315ED0FDEAF662854041FEF18DA7065198,
	ConvertUtils_EnsureTypeAssignable_mC88A1375B57428FA087FABDA244A2FB271194198,
	ConvertUtils_VersionTryParse_mB8A3CB61AAC8120693E11841566AFEBF4FCA30CC,
	ConvertUtils_IsInteger_m1E8095C7D169840870AF7F158EDC07D2E3F57E37,
	ConvertUtils_Int32TryParse_mB81CE59DBD850DF031DAFFB2F0DDEB48A345BEF6,
	ConvertUtils_Int64TryParse_mE4F4963695B52AF1320D3EAA5F8263301C2EB69E,
	ConvertUtils_DecimalTryParse_m635343424FB64EC5169E97D45DD076C75E3AD41C,
	ConvertUtils_TryConvertGuid_m4D8F89945C283C3499C33A8DEF956BD19E0B0B1E,
	ConvertUtils_TryHexTextToInt_mE03A65879D470EFCAA7AE02A2F7502C144F510F2,
	ConvertUtils__cctor_mD36D5B3BC15AF6F53BCD3FFB1EBD935BBF2B7D0C,
	U3CU3Ec__DisplayClass8_0__ctor_m7B6D25E211D4871A7CFEA6DC9C8DA57FA6263B47,
	U3CU3Ec__DisplayClass8_0_U3CCreateCastConverterU3Eb__0_m061CFEA2BA887591FDA47F65300203C0862BA9F5,
	DateTimeParser__cctor_mEA064906F2088A75549E50B47C17B7035DDFAB9F,
	DateTimeParser_Parse_m43B3FC0FA8161DDF4BD65D8BA4FDC932ECB5975A_AdjustorThunk,
	DateTimeParser_ParseDate_mEE029E37D7888F1F1F565C62D2B1AD8A24C196B0_AdjustorThunk,
	DateTimeParser_ParseTimeAndZoneAndWhitespace_m8D86188478104927B08DCC91471CA7F91D5D6DA1_AdjustorThunk,
	DateTimeParser_ParseTime_mEAF9A7B597931847CD06F3E63D9E89F8BDD8DDF5_AdjustorThunk,
	DateTimeParser_ParseZone_mE7CBCC4DC51A530D5DBF1E68ADDA5171A977048A_AdjustorThunk,
	DateTimeParser_Parse4Digit_m9155B9CDCD558218F7F08302E8613152CAD89829_AdjustorThunk,
	DateTimeParser_Parse2Digit_mEE0C6006FE2D42510E2B0BCD19B365201AB04D57_AdjustorThunk,
	DateTimeParser_ParseChar_m2BBAFB1B1FE2859BC8072E8423A57E6D04533437_AdjustorThunk,
	DateTimeUtils__cctor_m648A889F08E03B5A42AD69EDD8CF48C0E2F6F212,
	DateTimeUtils_GetUtcOffset_m9F7AD533125BAED1CDBE01DFB144795B1D1B15AC,
	DateTimeUtils_ToSerializationMode_m4659F4806C6CBD8545C502E88084B146241F2E89,
	DateTimeUtils_EnsureDateTime_m325F398FBDD29DBBEADEA941A77566F982286792,
	DateTimeUtils_SwitchToLocalTime_m0D45766632CF27ECC5EC90E7DE50EB7A9694CCC5,
	DateTimeUtils_SwitchToUtcTime_mC8AF1BFBF807E1DA3FA4B9405340902752D671B2,
	DateTimeUtils_ToUniversalTicks_mAE971477C4D0745AB6DE0AEDC5062631725B6FEA,
	DateTimeUtils_ConvertDateTimeToJavaScriptTicks_m2485A1AD9F2B52CFD01A4BE3AD97F9C37107BC3C,
	DateTimeUtils_UniversialTicksToJavaScriptTicks_m771D7B6833225843D320529D9AA6C8DEFB18F8E2,
	DateTimeUtils_ConvertJavaScriptTicksToDateTime_m5FAB48CFDDA9607DFB1917A23A1CCC1D73F91C43,
	DateTimeUtils_TryParseDateTimeIso_m7E06FF64EC2BA50B75E4A2732C1EF00CE11AD771,
	DateTimeUtils_TryParseDateTimeOffsetIso_m913E3D8DC898481197A0F98F5B6A850F1BA027A2,
	DateTimeUtils_CreateDateTime_m0FC2FC875264027406C3BF8AD6E42302683E0720,
	DateTimeUtils_TryParseDateTime_m41E39A9DFBD97C2AE6AC56C4B504F42E993851FF,
	DateTimeUtils_TryParseDateTime_mD2262844C1A4B464337775148F3E4E54F6D7E74A,
	DateTimeUtils_TryParseDateTimeOffset_m9B5B3485A46D66E27457822CC1EF6E0D17ADB5CC,
	DateTimeUtils_TryParseDateTimeOffset_m254A3FDDABDDF979844C0ADF79201B6BB462FDD9,
	DateTimeUtils_TryParseMicrosoftDate_mC6EDFDD1538000260211463563578A2E41580E54,
	DateTimeUtils_TryParseDateTimeMicrosoft_m69ACCE201D332547C54BA5280AC22E01DE675420,
	DateTimeUtils_TryParseDateTimeExact_m07E1106E3A362924998703F599A31181561CB2E3,
	DateTimeUtils_TryParseDateTimeOffsetMicrosoft_m605D9E1887EB40D7439F50ED2FB82E48C8CE7563,
	DateTimeUtils_TryParseDateTimeOffsetExact_m54E38E85E5D1E5C458B0254966A5CC6C7546321D,
	DateTimeUtils_TryReadOffset_m8D756B894BE25ED820CF16E6076905713524EE2D,
	DateTimeUtils_WriteDateTimeString_mCE460CFD698DD920E27891EA6E3B03836AE5A691,
	DateTimeUtils_WriteDateTimeString_m0861A9992AE400F8FA19D00D12A68FA66C943DA6,
	DateTimeUtils_WriteDefaultIsoDate_m6FEE0E7DBBC38DF88078C6C972C165A8D8B11C11,
	DateTimeUtils_CopyIntToCharArray_m721DB60D3F654BDED253D5DA660602F8B0FA6C82,
	DateTimeUtils_WriteDateTimeOffset_m455B4552E7E44C9821A598709DCA7D5B238413E1,
	DateTimeUtils_WriteDateTimeOffsetString_m6347D67D0DBCAE73D3475F9794293D77F06E4AB1,
	DateTimeUtils_GetDateValues_mD1B89FE5E95556B9DE6B4912CA17D817E00081EC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DynamicUtils_GetDynamicMemberNames_m9EE2F157494A945FC80F31D0249C228C841B76E3,
	BinderWrapper_Init_m5D70E736A7401CE42908D1A50EBD983402AA7625,
	BinderWrapper_CreateSharpArgumentInfoArray_m97B6B6D1C063B27560D0E8B597618AD3E17B4A0D,
	BinderWrapper_CreateMemberCalls_m93EAFFBFFF25B09B4CB75A274F3D01060D72DDC8,
	BinderWrapper_GetMember_m4DC3FB0DBE56DEACA425757C67794BCF9D311A18,
	BinderWrapper_SetMember_mFA51D571E767B4DA53DF06C8E26D3D40D2D6B503,
	NoThrowGetBinderMember__ctor_mDA0E964CCDDCAFB8675267DE0E3BF18D79E7C87C,
	NoThrowGetBinderMember_FallbackGetMember_m9406BCB973D668AD9B17BEBABED58E8BE05E50E3,
	NoThrowSetBinderMember__ctor_m197C874AA0F759C90764FCF673E58C02440372BC,
	NoThrowSetBinderMember_FallbackSetMember_m365BF1EE4BB822C5E691FB911C839D3C6C63EA94,
	NoThrowExpressionVisitor_VisitConditional_mA2B2AE2CC6CD366385CADAF75D93773F8E17D91C,
	NoThrowExpressionVisitor__ctor_m9A6090E84B5651F3168C4A509D4C53C3D217FA08,
	NoThrowExpressionVisitor__cctor_m1465172B3FD841337FDA664EE062E3A49ABBF1DD,
	EnumInfo__ctor_mDE19ED14790B4609B59AC8437C493C1DB3BFA68F,
	EnumUtils_InitializeValuesAndNames_mE11956DB6693D0DF70384549F81C596C83A883D6,
	EnumUtils_TryToString_m842CF119FC5659E95CB09570659652D8753E999C,
	EnumUtils_InternalFlagsFormat_m611CFAF7C1381E1EE653D2492EDA433243777A27,
	EnumUtils_GetEnumValuesAndNames_mC993C4AFE95D075CA64A695AE7469F07C2661140,
	EnumUtils_ToUInt64_m0D86B45B51747B2B191968070B82D1A724D17312,
	EnumUtils_ParseEnum_m2C7528F536FA0AF803C3D81510618CE8F70E6DAB,
	EnumUtils_MatchName_mBE379E853CC70B30B2A920F1B26F3370634CAEB3,
	EnumUtils_FindIndexByName_m462FDB3453FBC8601F9B7EE996D8875319E7C5AB,
	EnumUtils__cctor_m993FDCCE8DFFD4304E949C5DB00FF38D38D2070C,
	U3CU3Ec__cctor_mA4B587CE9C3DE218984EF1EF7EE6F5B25C99313C,
	U3CU3Ec__ctor_mD3C7C2B6449BEDB3FDD3EBB0808E4F466FCAFFDD,
	U3CU3Ec_U3CInitializeValuesAndNamesU3Eb__3_0_m723957A29FF2263B146696158EC8B178C8D82041,
	ExpressionReflectionDelegateFactory_get_Instance_m831320943E9978D6A8D4F116D0B359F7735DE6BB,
	ExpressionReflectionDelegateFactory_CreateParameterizedConstructor_m7C08C0B36DA4BE047265DE4459D381D786BA5CBF,
	NULL,
	ExpressionReflectionDelegateFactory_BuildMethodCall_m0011EE22249F2B135F93C83AB42D20E4E2F6D1E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExpressionReflectionDelegateFactory_EnsureCastExpression_mA0F3FC1518ED2CE86538D9DA0F4EF47564B936DB,
	ExpressionReflectionDelegateFactory__ctor_m1CD7A86504310E44B839178A088382C0190785D8,
	ExpressionReflectionDelegateFactory__cctor_mB710CB6081B4F4C8F806583B3BD00A3F9A24BBE8,
	ByRefParameter__ctor_m4718F8B823D095A1ECEB66CC6B1397B364154094,
	NULL,
	NULL,
	NULL,
	FSharpFunction__ctor_m414E2BFD2D608C99E7D3B9AE10E799E83CABE729,
	FSharpFunction_Invoke_mAD1C1BE3517ED288E69C2C63037A3DF0C8155AA3,
	FSharpUtils__ctor_mBF998D082E661BD0BFBDE67059F18D1424CCDFB0,
	FSharpUtils_get_Instance_m486791D0D96FC27FF4D4B0F1C7DC6B45BAF76EF6,
	FSharpUtils_set_FSharpCoreAssembly_m86E2DC83E7348FC4C937C2FA35E4138B76FD0FC1,
	FSharpUtils_get_IsUnion_mE9B81C84480DAC67BB35CBCFAED1988C808CCAB3,
	FSharpUtils_set_IsUnion_mD06CB47EFF251198C95CB1B535BFB4C0174D5E3F,
	FSharpUtils_get_GetUnionCases_m9C137C2BFAAAA384B6ABBD3173D365EB88112CF0,
	FSharpUtils_set_GetUnionCases_mF1FB052695915469AD2271F73920B5972F8D4FF5,
	FSharpUtils_get_PreComputeUnionTagReader_m48B81101C73D1D8672C2781AF4C275814E0A2F9E,
	FSharpUtils_set_PreComputeUnionTagReader_mA4DD07A716374BD63D048A8C23A97CB3F3D9CAC5,
	FSharpUtils_get_PreComputeUnionReader_m99851918B1AA672F86F5BF4496DA20F1589F9A44,
	FSharpUtils_set_PreComputeUnionReader_m1189A0892E94886BC90C05F9596E3A43F6208A24,
	FSharpUtils_get_PreComputeUnionConstructor_m02303FADD834CBCBDFA4E3A521FC9DC4FAD16B26,
	FSharpUtils_set_PreComputeUnionConstructor_mFA6894426E806E7DF01E568D431D25314C637E4D,
	FSharpUtils_get_GetUnionCaseInfoDeclaringType_m65C8F5023475861194551D2FE4B3D194D98BED37,
	FSharpUtils_set_GetUnionCaseInfoDeclaringType_m1440904750D8A46F40491D563AAA487E17462742,
	FSharpUtils_get_GetUnionCaseInfoName_m0DC562BAFC40E1E5A1E3A8DAD45283B53DC75BA9,
	FSharpUtils_set_GetUnionCaseInfoName_m84B84F80973021E1C4634094D40C9912B9FA19F7,
	FSharpUtils_get_GetUnionCaseInfoTag_m56F65C78314427E8E860CFD2749B6C76F2456448,
	FSharpUtils_set_GetUnionCaseInfoTag_mC24E81FC03E96BD4646314EAF97520789F615108,
	FSharpUtils_get_GetUnionCaseInfoFields_mE7A8E0725168B71E5D6A3ED8E18C07D919F71C02,
	FSharpUtils_set_GetUnionCaseInfoFields_mE5FE7298B7EE311641F84F92B18BD4108BB79BE0,
	FSharpUtils_EnsureInitialized_mC086249BFF6B0BF729EFAD8A62B5F0A8746CF16F,
	FSharpUtils_GetMethodWithNonPublicFallback_mC24F68C005762BC3B08F44413866120454854840,
	FSharpUtils_CreateFSharpFuncCall_m614708A6E248F2168276ACBEB1CCA716323C5025,
	FSharpUtils_CreateSeq_m888C3AF7529A3DC2DC33CC14FAC7D86DE5CFB622,
	FSharpUtils_CreateMap_m560B7BC17043E7899636DA14AEE556D3367899CC,
	NULL,
	FSharpUtils__cctor_m47748D78452BED39FF42305B84662F8478505D4C,
	U3CU3Ec__DisplayClass52_0__ctor_m8F4C9A0A218E7E963FDF77F570707C41446EC293,
	U3CU3Ec__DisplayClass52_0_U3CCreateFSharpFuncCallU3Eb__0_mD4B79F06BAF047EFFBF16835AC71CB512AC80FA3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ImmutableCollectionsUtils_TryBuildImmutableForArrayContract_m615DD967E76C1EA2D75C1D32A77BA4A170B71927,
	ImmutableCollectionsUtils_TryBuildImmutableForDictionaryContract_m9CD19B9631AC687A7BE76419F6569B0881FF4BA9,
	ImmutableCollectionsUtils__cctor_m93A5F710C736CE1DFEB3C4836C61E3FEE1E204DF,
	ImmutableCollectionTypeInfo__ctor_m380C79692588CE83860FD6ACEBF1F6C1CDA52F94,
	ImmutableCollectionTypeInfo_get_ContractTypeName_mC528E485DDD9527CA2A164A610AD216127598DB3,
	ImmutableCollectionTypeInfo_set_ContractTypeName_m043381BA6B90967C18A2A91A038CE1AD9EBFEB22,
	ImmutableCollectionTypeInfo_get_CreatedTypeName_m46F8F292D4423EA6C1036F23AD87A57A5523B8F8,
	ImmutableCollectionTypeInfo_set_CreatedTypeName_m0BBE3B6543C7AACEB1DD02441C190010A5691D17,
	ImmutableCollectionTypeInfo_get_BuilderTypeName_m1F99E1EBCC1E1BAACE37AB1C2059AC55AE7932A0,
	ImmutableCollectionTypeInfo_set_BuilderTypeName_mE05CDA8DECD5212A21F8364CF59D768FCD1C79A4,
	U3CU3Ec__DisplayClass24_0__ctor_mF099F2BE48E8CDDD9B561BEC04A9000331F3ED45,
	U3CU3Ec__DisplayClass24_0_U3CTryBuildImmutableForArrayContractU3Eb__0_m3AC76513531ABE2C3CEAD0596DE221F1077C6B9D,
	U3CU3Ec__cctor_m2114B2EA54D263FDE85268A87BEF71B5167923B4,
	U3CU3Ec__ctor_mED0DD564DD1BF14B6A32F5C37C193BB62AD1B6F6,
	U3CU3Ec_U3CTryBuildImmutableForArrayContractU3Eb__24_1_m8D13553E20529D7AF62FB583BC378870214C39A0,
	U3CU3Ec_U3CTryBuildImmutableForDictionaryContractU3Eb__25_1_m0952E998E9EAC984D43F9DA81129D70A11DF8890,
	U3CU3Ec__DisplayClass25_0__ctor_mB3D5FDDD44A30B5FE01E76250A834CFFDC89DFEF,
	U3CU3Ec__DisplayClass25_0_U3CTryBuildImmutableForDictionaryContractU3Eb__0_mBA9BAEAF7F3D5DF221304C324F20EFF72897D914,
	BufferUtils_RentBuffer_mD5A4DC34563041EAF58CA9856A34325BA2EF4B46,
	BufferUtils_ReturnBuffer_mD23E0F30843D0613C5C23C598EA48944E6481C73,
	BufferUtils_EnsureBufferSize_mD6D3E409713EC1C9CBF11AFCA938117CF1F0F5D9,
	JavaScriptUtils__cctor_mFEA98829B065FFA69BB276691A83FBFADA27C70A,
	JavaScriptUtils_GetCharEscapeFlags_m97FB6B18815E2740357EF8892B0A36DD73A4D2AE,
	JavaScriptUtils_ShouldEscapeJavaScriptString_mE390817FB23DD5307E1500239870AD1EFAF4D9AC,
	JavaScriptUtils_WriteEscapedJavaScriptString_m8D385039955CB5ADC931E18073F29A90A859F871,
	JavaScriptUtils_ToEscapedJavaScriptString_m2874A73B04CDACB7BD5092C7D8BEC9A3146AC09F,
	JavaScriptUtils_FirstCharToEscape_mA6D67E3CCBF45CA2277E52C7EC1D3C99847A7240,
	JavaScriptUtils_TryGetDateFromConstructorJson_m7F38C0BA2AFD1C9AA887FB9499135D419CAB294C,
	JavaScriptUtils_TryGetDateConstructorValue_mFE2781B4E31177148E612DCC0EE69830BE6AFAF9,
	JsonTokenUtils_IsEndToken_mB84045847A9B44D14B001F29A92719B28D105DDF,
	JsonTokenUtils_IsStartToken_m2735210D81474849D62B19250B0A27980E7B75A2,
	JsonTokenUtils_IsPrimitiveToken_m124F7EA7B7D39A36699293C0A971AAE26A02F78A,
	LateBoundReflectionDelegateFactory_get_Instance_m4755DCFBF8E5C50D8F934D54F7F05BA3DB8DFCCD,
	LateBoundReflectionDelegateFactory_CreateParameterizedConstructor_m556CF237D21F5F52DB41DEBE111AEE5589891006,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LateBoundReflectionDelegateFactory__ctor_mB1B201FFE8A4F7A4F6BAB610503B14BC59F5AC9A,
	LateBoundReflectionDelegateFactory__cctor_mFA58E688133C3D0E6924A84EF136875BC0154321,
	U3CU3Ec__DisplayClass3_0__ctor_mB6D599DADDC2AF6F584E4468AEA7959F81BC537F,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__0_m2DCDBEAEFEC11A15A42FBF50DE525BBF874863A4,
	U3CU3Ec__DisplayClass3_0_U3CCreateParameterizedConstructorU3Eb__1_m867B457418C43B80173C34AE08C7F75BBA3FB618,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MathUtils_IntLength_m763650DE40D1E26C73EDED34ECB5AC4040B4C380,
	MathUtils_IntToHex_m1FC55F6E4DC6AD83E9D67B5F7567C29396C311C0,
	MathUtils_ApproxEquals_mBE0405CC42BAABB5E73BAFF38DF042305E4F6531,
	NULL,
	NULL,
	NULL,
	NULL,
	MiscellaneousUtils_ValueEquals_mECB672BCBAEA002D27D7B7F496CE685CC07C9EC1,
	MiscellaneousUtils_CreateArgumentOutOfRangeException_mBF0CC2C7BD3B17559AF2E2D6B7F8DF8A31721248,
	MiscellaneousUtils_ToString_m3D4BCB1F1205454E136CCBC4D17321A00E5D2CED,
	MiscellaneousUtils_ByteArrayCompare_m194BBA180A5EBF6B190A17BAA9D610C1A5589DFF,
	MiscellaneousUtils_GetPrefix_m45C66A058790A154139F969F85DAA92B6ED8C2C2,
	MiscellaneousUtils_GetLocalName_m5C36FD76824F2E26D4B38C9B3CFC0879B1947843,
	MiscellaneousUtils_GetQualifiedNameParts_m03391FF5C5AE3BDB797286C3B0FB80A4397F56C0,
	MiscellaneousUtils_GetRegexOptions_m88971A02045D20AFD899F0790B9CCFBD359DC46C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionDelegateFactory__ctor_m249ABB5C4DEC395A3741D22C46A0898F7E375C80,
	ReflectionMember_get_MemberType_m2FB1FB99AFE05485008EA486A47799727781C5EF,
	ReflectionMember_set_MemberType_mEECF7DEA17AEE29166E28ED6A1EF67F1D70872E2,
	ReflectionMember_get_Getter_m1D3DD86B27D129BA2540F290ECDABC3AEDA68A8D,
	ReflectionMember_set_Getter_m56175EF5AE1EA3BE74049D66E920E6EAE252280E,
	ReflectionMember_get_Setter_mE66CFA1687DC319AD43D279A73729827EB89E98E,
	ReflectionMember_set_Setter_m8F0B22DCCD00C4C6C9638B811E28422AC1F0233D,
	ReflectionMember__ctor_m0BE4C4861DD0BEB72E03A2A1B608189746B5690A,
	ReflectionObject_get_Creator_m7E021F79D8C332AE3B0148786F1E831283054A7E,
	ReflectionObject_get_Members_mCB55B637679F287142CE51BC3955339CA50BC888,
	ReflectionObject__ctor_mB38E0F5C4530BEDEFBB9002CD82B2D84AB8F4BBB,
	ReflectionObject_GetValue_m90BE6B90640449C3340135617FC7D91C6A5A45FF,
	ReflectionObject_SetValue_m7BA7A797EE7C9A7BF0D8A2DDB35D1453EEB416DD,
	ReflectionObject_GetType_mB8BA51631BFABD3BFB6D93A6B57982BCF135F911,
	ReflectionObject_Create_m31BBD22C0D51E671F5FF8D24DD3D9780DC883FFB,
	ReflectionObject_Create_mDDFAD2B0FA0DC5E3DC92E14E846AE4D194D351B1,
	U3CU3Ec__DisplayClass11_0__ctor_mCA910CCEA019B4A13B4F7225476D0D978C8009F5,
	U3CU3Ec__DisplayClass11_0_U3CCreateU3Eb__0_m706A90DC257A712482CB841E7271BDC131889F11,
	U3CU3Ec__DisplayClass11_1__ctor_m3D4D3E4EE078DA0BF09127EC585A3430921B87F8,
	U3CU3Ec__DisplayClass11_1_U3CCreateU3Eb__1_m4E5358FA5B992F2553B5AAEFF1964610342B5DDA,
	U3CU3Ec__DisplayClass11_2__ctor_mCFD9155B5C6FB481BFE7901D8DA6D7C1F715091B,
	U3CU3Ec__DisplayClass11_2_U3CCreateU3Eb__2_mF77E5D911106CF0A2B2230068927A84D17069700,
	ReflectionUtils__cctor_m141D35BC34CF7AA7FDE575E1447702DDE0812A86,
	ReflectionUtils_IsVirtual_mA538A8B4A8C65D38448F5BD39CD05BD2C8CBB0D1,
	ReflectionUtils_GetBaseDefinition_m3EE73CC7B521561A8E111356C5BE2AD1357B331A,
	ReflectionUtils_IsPublic_m0F68EF7047D5DA214B7EE1B312AF0F2385CE1EE3,
	ReflectionUtils_GetObjectType_m60C54E9B884211A4D3CBB9AC5E03D896E6BF0040,
	ReflectionUtils_GetTypeName_mAAC5361EAE1AA98F0195D6D76266967A9CB848D9,
	ReflectionUtils_GetFullyQualifiedTypeName_m41AE5258E8811FFF43EB821C785C237535E05BA5,
	ReflectionUtils_RemoveAssemblyDetails_m329AF4FBF44D518B59C8A9B41BEBD9FD30AC478D,
	ReflectionUtils_HasDefaultConstructor_m7AA2D83D5A4FEFF347FDB88C3AA57C2E49E3EF21,
	ReflectionUtils_GetDefaultConstructor_mF7DE07E23F2D6083F442B325618568E50A4196B7,
	ReflectionUtils_GetDefaultConstructor_m6E910D186A317CF4AC6527C3BDEA8E6FC539F804,
	ReflectionUtils_IsNullable_m64CB43DBC25491FF5EC8855B1701B623176AA4EF,
	ReflectionUtils_IsNullableType_m3A1A7823AF44D84487CD8D807286E42DD943BDC6,
	ReflectionUtils_EnsureNotNullableType_m5980E94E818D68F2439F14BDF8E944390C5685BA,
	ReflectionUtils_EnsureNotByRefType_m98F3C76F5C2CCA42B321EA40AC442AD1C5D4395C,
	ReflectionUtils_IsGenericDefinition_m4A63A7D851BBD246C86C2ADEE1C8CBD44A2F8B0A,
	ReflectionUtils_ImplementsGenericDefinition_m5FA208833E2FB8C3F1F2FDDD7DB73EC9BF794E0B,
	ReflectionUtils_ImplementsGenericDefinition_m971FDE4108C508C04B47CBCBEE00350C23FF40D7,
	ReflectionUtils_InheritsGenericDefinition_m8FF1C61B29FA9DA10B9F24FA1A3334559FD9D620,
	ReflectionUtils_InheritsGenericDefinition_m8E5BE681FC821447243C58D8EB1C7C879E97440B,
	ReflectionUtils_InheritsGenericDefinitionInternal_mAB6D807C8A07B022567347B93D74CDCF7AEDC6DD,
	ReflectionUtils_GetCollectionItemType_m8707A39821344D59FF92CC038775BD49EB9A4710,
	ReflectionUtils_GetDictionaryKeyValueTypes_mF6DB2A211AD51652C52C280F34A49F06130C878D,
	ReflectionUtils_GetMemberUnderlyingType_m500350397C4D69A486064370A126CDC5D364D940,
	ReflectionUtils_IsByRefLikeType_m7B6BB9E870965F2031DBB2CA165D6D52A59BE382,
	ReflectionUtils_IsIndexedProperty_m41C39B3318D5484BE6B701EE6BD8F00CD99A5ABF,
	ReflectionUtils_CanReadMemberValue_m09AC7DC90C5EADFB599FD0CC2F70ACCA4CCC5771,
	ReflectionUtils_CanSetMemberValue_m94AFEA2E18E73F677AA63383DD54C95634CE9FF7,
	ReflectionUtils_GetFieldsAndProperties_mE327D2BB3CB07E0A245A24FBE7EA879EFA4EEBCD,
	ReflectionUtils_IsOverridenGenericMember_m93A60DFD50FFCF5B2212B3D33E53E8EA16A3F02E,
	NULL,
	NULL,
	NULL,
	ReflectionUtils_GetAttributes_mA9CDBD4D932B2FD8D7BCACEDA51B93631AB7E671,
	ReflectionUtils_SplitFullyQualifiedTypeName_m6A3EE9181E6889C5A44129B7A106B844EADBFD88,
	ReflectionUtils_GetAssemblyDelimiterIndex_mE3A7B76A25E13ACBB3A5D700D35100A848681F89,
	ReflectionUtils_GetMemberInfoFromType_mADFBA62B34D382182EFC64E44E913F9BCAAE35B3,
	ReflectionUtils_GetFields_m40A33C70A81AEAF5F1DC17DA0119903524CD8624,
	ReflectionUtils_GetChildPrivateFields_m01B89F241EFCCD5BFF84FFCF4E3AD629A55AA323,
	ReflectionUtils_GetProperties_m53AA061FE94BA47A5D652A2C835F59F95CF40F3B,
	ReflectionUtils_RemoveFlag_mFE398FD30B419043A6A186AEAE26273D120CDCA9,
	ReflectionUtils_GetChildPrivateProperties_m9066AAD0EBBA2CEB0532D7EC9E8E34861DDBE5CA,
	ReflectionUtils_IsMethodOverridden_m6283FCF8AF077955E0C7A9B77245AD0F2A877C5F,
	ReflectionUtils_GetDefaultValue_m4108A8BF683F277BE923AFD01807442C370156B5,
	U3CU3Ec__cctor_mB7D547DD9FE8CA18E4EEC98E4EFF859314812F02,
	U3CU3Ec__ctor_mF38069D565143CDA23DEB4D8C99D3492CFF7B2AB,
	U3CU3Ec_U3CGetDefaultConstructorU3Eb__11_0_mAC2A19F7D1D9BD408085354779DC1B378A54983B,
	U3CU3Ec_U3CGetFieldsAndPropertiesU3Eb__31_0_m8593F693E81487AC48355DB19125C639AF9080BD,
	U3CU3Ec_U3CGetMemberInfoFromTypeU3Eb__39_0_m1EB8B449DF74D0923D52D93E268FD21260953885,
	U3CU3Ec_U3CGetChildPrivateFieldsU3Eb__41_0_mA5E1602F2958D13F9D38611DF12B75326D16BC4E,
	U3CU3Ec__DisplayClass31_0__ctor_m22D408A7A566AE5EDFC10FB5AF11ACB21D4DFA56,
	U3CU3Ec__DisplayClass31_0_U3CGetFieldsAndPropertiesU3Eb__1_m4BCC524D52F430A510E8D5BCB7EDE999C94AAD30,
	U3CU3Ec__DisplayClass44_0__ctor_mC63424D2E56D8BEA64E9EBF883111549A68AE764,
	U3CU3Ec__DisplayClass44_0_U3CGetChildPrivatePropertiesU3Eb__0_mA27C7B364B7D72D971AC533933B8717FEF752E33,
	U3CU3Ec__DisplayClass44_0_U3CGetChildPrivatePropertiesU3Eb__1_m72FC23A9AF1BE1FB5BB0FA53197B0A3C737FB0F8,
	U3CU3Ec__DisplayClass44_1__ctor_mB2F40E9D2DEC82C7EB0B50C82BDBCBBE7791C048,
	U3CU3Ec__DisplayClass44_1_U3CGetChildPrivatePropertiesU3Eb__2_mB59113D93F7231C1B12FF99EEEA8A8EACF8844D6,
	U3CU3Ec__DisplayClass45_0__ctor_mA55114503AE0ACE0EC70A09F2950809AA350C56B,
	U3CU3Ec__DisplayClass45_0_U3CIsMethodOverriddenU3Eb__0_mB829FE3CA5C61ADBD41F3439838940E4598E72BB,
	StringBuffer_get_Position_m83F723C2AF17EF7D1C375E47524FA7AB88171B7E_AdjustorThunk,
	StringBuffer_set_Position_m2BFD7C5B2352AF33D9AA3C824923133989E7805F_AdjustorThunk,
	StringBuffer_get_IsEmpty_m3A209BFC219A27F107A45E3FF97C4F77B882A557_AdjustorThunk,
	StringBuffer__ctor_mEF7BDBA7FB252D738CA1248D5F93B8B4805CEA88_AdjustorThunk,
	StringBuffer__ctor_m69AF5866650F170CF733B000E178531B0267ED01_AdjustorThunk,
	StringBuffer_Append_m160730AFEB21E3CD5A8B1D597D653E424A4E16C6_AdjustorThunk,
	StringBuffer_Append_mC5ED80F3F3847B8B9DF502E89BB51DEA9D13B49B_AdjustorThunk,
	StringBuffer_Clear_mD5F44587D8FB55BE8C93F2FB7EED70E699712BD3_AdjustorThunk,
	StringBuffer_EnsureSize_mEB9C44AB1E16068A4BC4429E552ADC07D347A0F3_AdjustorThunk,
	StringBuffer_ToString_m57E05B88A083A828ACD1FCAE20928B77B811EC9A_AdjustorThunk,
	StringBuffer_ToString_m61E1C3FDB3DC0CDCCCB6192B581779F99559174B_AdjustorThunk,
	StringBuffer_get_InternalBuffer_mB3BF2E89A05E07D7E80528801009C9389B7009DB_AdjustorThunk,
	StringReference_get_Item_mEE1E8EF598FEF02A9CB4C5605DC9CFB385529D82_AdjustorThunk,
	StringReference_get_Chars_mB940C355A3E54CF70A0BFD8E0F057845C74EA565_AdjustorThunk,
	StringReference_get_StartIndex_m937057682C248341C618E1591F2DB8490DF00087_AdjustorThunk,
	StringReference_get_Length_m820A37B0476E248C88EE1C1248D87D1D3CCCEACC_AdjustorThunk,
	StringReference__ctor_mF6FCA9412B7D5EE743F083798D09476263B4B9B2_AdjustorThunk,
	StringReference_ToString_m5900C8D292A447D1E52DC57DC7E38C20FA9A687B_AdjustorThunk,
	StringReferenceExtensions_IndexOf_mED906B77321DE3BAA9EDDE334171965E019898CB,
	StringReferenceExtensions_StartsWith_mA192ED52E608ED33CCC781FB4648C2545365B8A1,
	StringReferenceExtensions_EndsWith_mE5037AE2181E864E853859016143C44FFA09CD33,
	StringUtils_IsNullOrEmpty_mB58C32CFC344EB6F80BB592F45E136DA99C40E37,
	StringUtils_FormatWith_m2D67274D9DA1C6E2E96118B205A0F7EC50506DDA,
	StringUtils_FormatWith_mE72B9659176DE4E52E28A5E3BC5F0826BF7642B5,
	StringUtils_FormatWith_m13F02CC8660679187AFACB877D6A9E1FDCF76841,
	StringUtils_FormatWith_m61F5B5F89CB62CF25E2163918DF2CBF5C1AC7918,
	StringUtils_FormatWith_mABDDFF33E7972C10F36A72C8DCE0685DB7B5CB13,
	StringUtils_CreateStringWriter_mDB3A91DB84B0A08D80B321CC8B568C11C800E302,
	StringUtils_ToCharAsUnicode_mCC37911576DD31F842C29294DF8C7F1C1081C668,
	NULL,
	StringUtils_ToCamelCase_m43B382C941A5F19EF05F16D0A722AD02763FBA30,
	StringUtils_ToLower_m769207EF0A7BBA0B60C04A27CA63C77998C95BB8,
	StringUtils_IsHighSurrogate_m6179B135893122AF29A06C037B51175E9B8C633D,
	StringUtils_IsLowSurrogate_mF7D22770A1FECFE3F6A8BCB767BE0AD7CA6CC098,
	StringUtils_StartsWith_m3F00634D0FE8E40155E75AB46E17151560F6EC71,
	StringUtils_EndsWith_m51572DC9F29F5D3E0F53C0011788803A1C4C7DA0,
	StringUtils_Trim_m6CC1277CA2FCD3B1865A3BC50EBB03EE67E15D1C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TypeExtensions_MemberType_m4C01999F78A2E00448DEA2EFF8A07E751E82D974,
	TypeExtensions_ContainsGenericParameters_mD1381A549AE755AAEBDFC7EA7F14A1BF969A668E,
	TypeExtensions_IsInterface_mF90F1C446936F97CE69CA11E0B5352EF9D4902E4,
	TypeExtensions_IsGenericType_mF8FA3DA2153AF6FA2738B61BC4FDE19BD54D0539,
	TypeExtensions_IsGenericTypeDefinition_mB6618E157D2E9A9C47DAFB4D33A44D9C8CDC1DE0,
	TypeExtensions_BaseType_mCF843FF6E543AA3A42BA832968905E2AA2C27969,
	TypeExtensions_Assembly_mF83E7EC9A6CF5895A8ED903B5222C0FF49F12C6D,
	TypeExtensions_IsEnum_m86C76615ECEC5C1FD385DF3EE1AFB273213F897A,
	TypeExtensions_IsClass_m5505387F00584AA3E1F4E1984F5C8E900750434E,
	TypeExtensions_IsSealed_mFBA15084B6383583EE26520B9727FA67B5156646,
	TypeExtensions_IsAbstract_mD5B7437406F44CFB14BAC3D8900C16B67258FA21,
	TypeExtensions_IsVisible_m468C89574A86C94ABFFC5D3137A4BB15E3C93952,
	TypeExtensions_IsValueType_m69BF09F047B3D8E303A2856ADBB1FCD7DDC2FB1B,
	TypeExtensions_IsPrimitive_mE0AB728968073C7BEA002B71A49672502C210FA7,
	TypeExtensions_AssignableToTypeName_mDC42F639FB2F8F876EC548F608795DBB6F153214,
	TypeExtensions_AssignableToTypeName_m8D9CB15EDD79A2DF4E1CCF5E33EA3584F0E70639,
	TypeExtensions_ImplementInterface_mEFCE6A62B07FA6F10B8A7FC816FAFBB8DF53977D,
	ValidationUtils_ArgumentNotNull_mC5F3D8E2D73FC0F7A45A428EF878E9B8B59C59D0,
	NULL,
	NULL,
	CamelCaseNamingStrategy__ctor_m89BCD9FDF6BD285829A6F1EB37BD1B45DB135F03,
	CamelCaseNamingStrategy_ResolvePropertyName_m88E5CB044B18FA4355EEA66C256C15B378F10469,
	DefaultContractResolver_get_Instance_m79001EE9FDA11D9969EA252074830FC118E4389D,
	DefaultContractResolver_get_DefaultMembersSearchFlags_m12FD36351239622699BE5343E3DC59BB9B393169,
	DefaultContractResolver_set_DefaultMembersSearchFlags_m08146C8A73EB011E85CB6312FE6DE49CC2E7EEAD,
	DefaultContractResolver_get_SerializeCompilerGeneratedMembers_m78AFD3ADFD68C3FD34D99DAED756EC787C5B9E03,
	DefaultContractResolver_get_IgnoreSerializableInterface_mF8630105A6B09519C1B4755CB812803299F65F74,
	DefaultContractResolver_get_IgnoreSerializableAttribute_mD55E08E2FD8BD0BB7A4230DA5BB5011E3540D056,
	DefaultContractResolver_set_IgnoreSerializableAttribute_m4CE0B5099EA76A21F9AE603A669AAF88F05A3129,
	DefaultContractResolver_get_IgnoreIsSpecifiedMembers_mE6E81B7AEBBB2EC324EA1BF2451CF65E4FF4F974,
	DefaultContractResolver_get_IgnoreShouldSerializeMembers_mF309E6A38AFB542467A41C0BDAFBAF344B1FE917,
	DefaultContractResolver_get_NamingStrategy_mA312DF5B0ED1B3D7E47B01F0481DD497AA3F3402,
	DefaultContractResolver__ctor_m6D9934090BC1016112BD095CF4A74AAC54B1DABE,
	DefaultContractResolver_ResolveContract_m340256C4421D2AA299EF22073B3D37839B663794,
	DefaultContractResolver_FilterMembers_mFB3977C8169BC0E1AA5AFBCCEE2E6EBE3197C8AD,
	DefaultContractResolver_GetSerializableMembers_m0773ED45174599D1DE355F2CB9082D0E313792C8,
	DefaultContractResolver_ShouldSerializeEntityMember_mB2785F834AA160FE4E4A991C612CBBC56BC7F91C,
	DefaultContractResolver_CreateObjectContract_mF7084C9A7823AD7B4C064278159C2EAAFCF2C14E,
	DefaultContractResolver_ThrowUnableToSerializeError_m3169C87B1B938B09E670A8DFF37CDF0D08A6F658,
	DefaultContractResolver_GetExtensionDataMemberForType_m4EF1893B50CF8DA4251F57C5699DBD4259E1944F,
	DefaultContractResolver_SetExtensionDataDelegates_m8B786C651E53FE50810644F15C03E855EE57953F,
	DefaultContractResolver_GetAttributeConstructor_m42B7C359258ABFB146142FA1E674446426EB7E8F,
	DefaultContractResolver_GetImmutableConstructor_m26009C0F25BC0C9AD5036F82418DBB340A464A63,
	DefaultContractResolver_GetParameterizedConstructor_m150BA2A3CA8F495B1948D8B0B717C526E53FD6AC,
	DefaultContractResolver_CreateConstructorParameters_m433C63293A41D93CB8AD11B4CDD2FC3FAAFCF3AE,
	DefaultContractResolver_MatchProperty_m80097FB0A4EEAD32B0574A3CB7E2D2BD1564C307,
	DefaultContractResolver_CreatePropertyFromConstructorParameter_m97555C234C57331CFA17332902721711A999BC43,
	DefaultContractResolver_ResolveContractConverter_mF3EBD8169ABFEF08A418F369637006F4CF4BF6D5,
	DefaultContractResolver_GetDefaultCreator_m959DBE4C1291F84AE4369F6348E0AFFB66C8F09B,
	DefaultContractResolver_InitializeContract_m1BCF68B795E9644F0A985B8E3EB1C2224825A583,
	DefaultContractResolver_ResolveCallbackMethods_m306ECF212B1DF74639989D1FA363A30ADA9533A8,
	DefaultContractResolver_GetCallbackMethodsForType_m4787DE539841B684F694B743F1A5799CA491B13B,
	DefaultContractResolver_IsConcurrentOrObservableCollection_m9A00B3B3E34002280D66DBAB0D5C869C6EE39D01,
	DefaultContractResolver_ShouldSkipDeserialized_mBA93B215D74D6840714146F90EB9585C73C16814,
	DefaultContractResolver_ShouldSkipSerializing_m9F236D5943C722D3D372857836897CCDE25CAC2C,
	DefaultContractResolver_GetClassHierarchyForType_mF58899EAE6D11DCAB0457C83C1A281BEBE58D4F8,
	DefaultContractResolver_CreateDictionaryContract_m5E5B0500D9C84CF069381CEB27EC290528C27FDD,
	DefaultContractResolver_CreateArrayContract_m205E5B2ADB22D252879EA0351C0AFE6B26BA8C42,
	DefaultContractResolver_CreatePrimitiveContract_mAA8B3A37089C28834A512436E568B12292C1B244,
	DefaultContractResolver_CreateLinqContract_m7DB1965B5AC9AAA038EDB9151B4001EF9BF2DD81,
	DefaultContractResolver_CreateISerializableContract_m1F0E6292B3524B3CBD4FDF5CCA6960774A01B7B9,
	DefaultContractResolver_CreateDynamicContract_mEBA8720682A7FD46383A45DCDC348D7C1B85125A,
	DefaultContractResolver_CreateStringContract_mD69DB0A3074708A5A3FD351F47069ABCEC153D99,
	DefaultContractResolver_CreateContract_m5A635C8A137ED89C1A6AD9E6BD6683715BF3931D,
	DefaultContractResolver_IsJsonPrimitiveType_m0AEB5082C4B302220550FD7D3D0766F10D39BF52,
	DefaultContractResolver_IsIConvertible_mBD698B298824FC7D4887C033E60AC48307BA72AE,
	DefaultContractResolver_CanConvertToString_m7B2F093B6D9FCCB30568DEA39BFA12488D84A9A5,
	DefaultContractResolver_IsValidCallback_mBAFA737F05CCC59079BA8DA20E4159670B6FD43F,
	DefaultContractResolver_GetClrTypeFullName_mFB5C7722F194142C89C5A9FCEFDBF5E7D948E27A,
	DefaultContractResolver_CreateProperties_mED79EB6E286A2DC2BB87F8FF88D1FAD4747CFB46,
	DefaultContractResolver_GetNameTable_m7BC5A4ECE137DA1588829877559561E7DABF3F4B,
	DefaultContractResolver_CreateMemberValueProvider_m96E53340EED32A927490C456A16CD9880E6DD716,
	DefaultContractResolver_CreateProperty_mB99480D68319B57652D4993E3D278B5E98635661,
	DefaultContractResolver_SetPropertySettingsFromAttributes_mCAF13F63A2E3E7A9B825BEE350E5327FC583BC04,
	DefaultContractResolver_CreateShouldSerializeTest_mC2FB1258E395521D961A40A0DB65126B97B0D66D,
	DefaultContractResolver_SetIsSpecifiedActions_m1A2A5A6DC4F537C3024820FC87C9E3042D168024,
	DefaultContractResolver_ResolvePropertyName_mFAD612C9823F2082FC306862847FEB5500E70C03,
	DefaultContractResolver_ResolveExtensionDataName_m45C12721003E5C3F8F856EC871EBF3081EF30AE8,
	DefaultContractResolver_ResolveDictionaryKey_mC939787DE1DAA19C2DE69EF4F5B8F097911F482E,
	DefaultContractResolver_GetResolvedPropertyName_m99EFABF89F54490915E819AF9CB293D062788D56,
	DefaultContractResolver__cctor_m6ADC69873C07383B8C325516C1EDB50FA8EF8BFB,
	U3CU3Ec__cctor_m7DEDD9FCD2C7E0D14213A2C33FC8DF08DE23770C,
	U3CU3Ec__ctor_m02E469D3B5D1F808558BB2AD5B76DA1500FAFA22,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__40_0_m8C00AEDC96550CB8D360003E87AA08F31D2EC166,
	U3CU3Ec_U3CGetSerializableMembersU3Eb__40_1_m3350E78A8C8CFAB4A5A41BCC32E710CB7D930767,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__44_0_mE88454F8CBFA88535D5BA907C8B7E2C8C90D0D44,
	U3CU3Ec_U3CGetExtensionDataMemberForTypeU3Eb__44_1_mC5417DF13600622548B432AEC1489A3F48D40144,
	U3CU3Ec_U3CGetAttributeConstructorU3Eb__47_0_mB50A4D48AEDDE76E68BDD9CA98D8FC52A9DEDCAC,
	U3CU3Ec_U3CCreatePropertiesU3Eb__75_0_m5B2F1926F21A9BDDC4FAA3A26941C6AB93C590C1,
	U3CU3Ec__DisplayClass42_0__ctor_m3CB0C0F0A700D978D835AC46CD703B03FE212F88,
	U3CU3Ec__DisplayClass42_0_U3CCreateObjectContractU3Eb__0_m22A928C10C1D20B8AAD017CEDB4E4A8698B02680,
	U3CU3Ec__DisplayClass45_0__ctor_mFED4764FFEC1CFCD9058D18F7AB2C21F3D45692D,
	U3CU3Ec__DisplayClass45_1__ctor_mF64010AEF5C499A325EECF4FD01DC3854AC01969,
	U3CU3Ec__DisplayClass45_1_U3CSetExtensionDataDelegatesU3Eb__0_m19B2E04FA40B44731659D1F9CE8E8025507B08C2,
	U3CU3Ec__DisplayClass45_2__ctor_mB04B1C51382BB8014E3532E5D4FE625D0F238902,
	U3CU3Ec__DisplayClass45_2_U3CSetExtensionDataDelegatesU3Eb__1_m2E825E67A2A84155B87600F4AB07A76AF0986E6B,
	U3CU3Ec__DisplayClass62_0__ctor_m961A3BC222344FD0DD50AAB42E59EEA7A81636BB,
	U3CU3Ec__DisplayClass62_0_U3CCreateDictionaryContractU3Eb__0_m3D6C0C2A966FF510D9C0A39E7133B53F9A366DC4,
	U3CU3Ec__DisplayClass67_0__ctor_mADC323863980F480209A1F2C23534B385FB0998E,
	U3CU3Ec__DisplayClass67_0_U3CCreateDynamicContractU3Eb__0_m8F230208FB7AC5FA0C7A4BA557FD6C41913F6170,
	U3CU3Ec__DisplayClass80_0__ctor_mA03446B68D7B88BB8B0F511C66D9B0DE7DACBDCB,
	U3CU3Ec__DisplayClass80_0_U3CCreateShouldSerializeTestU3Eb__0_m434977ED3A2F858E76C76A6ECCE06AA5097292AB,
	U3CU3Ec__DisplayClass81_0__ctor_m1F6E5111D3EEA16612D40BBBB8166A1C33983DB2,
	U3CU3Ec__DisplayClass81_0_U3CSetIsSpecifiedActionsU3Eb__0_mD549EE3068EE64CE4D8DEE935585E0EA1651C403,
	DefaultReferenceResolver_GetMappings_m6D3752FC05DDA88554AE83721CECE7974313569B,
	DefaultReferenceResolver_ResolveReference_m6FCAA26BE7AD255D5FE85DFEE3C5D263577D195F,
	DefaultReferenceResolver_GetReference_m6FF3B975D1CAD18BCFDC38E7FB632C0DBD01B38B,
	DefaultReferenceResolver_AddReference_m55C433E5E722B5A4A61FD9B14449A9B3A5FBC071,
	DefaultReferenceResolver_IsReferenced_mEC95757504746F0A9EB07DC2FC42A6DD19CBFA12,
	DefaultReferenceResolver__ctor_m4D96F31AA081E6D3A1D820526AED6FFBEFDB88AF,
	DefaultSerializationBinder__ctor_m7B09BDB8485B92C513D89A6F14C3F83722C3143C,
	DefaultSerializationBinder_GetTypeFromTypeNameKey_m24BBD4FB8D3F906870CA2D52802CC6067B9726C3,
	DefaultSerializationBinder_GetGenericTypeFromTypeName_m1219EE0283B3FCA78FFAD850A9C5FFE2B8AC70E0,
	DefaultSerializationBinder_GetTypeByName_m73822B5075ABBB0DF24E087FFF3AC6C3C10875C4,
	DefaultSerializationBinder_BindToType_mD38C3E39741A2A3668AA775A88387B7BEE21F514,
	DefaultSerializationBinder_BindToName_mB15F8F0A861528066046AE15771A2989498F686B,
	DefaultSerializationBinder__cctor_m2E728B81A695D72B1E949591C7FD7F00F621C5FF,
	ErrorContext__ctor_mE2D8DBE046E712B93A1461E2C4FFF06256365F60,
	ErrorContext_get_Traced_m682D4F4552FBF85D07DE3336B4BBAE665FDE8337,
	ErrorContext_set_Traced_mDBDA49897B21212E952C5F3DFCC5806CB25F2DB9,
	ErrorContext_get_Error_m2ACA5AF2B81548FCFD3811554A089D0D0926D1EB,
	ErrorContext_get_Handled_m8F517FF8F9F5E8B8EF88E79BE6D73E247762CD47,
	ErrorEventArgs__ctor_m19D11B561E17BB068BC0AAFB0A4235339625494B,
	ExpressionValueProvider__ctor_mD982048A8C7DD002C0EAD56CAF6795000436680E,
	ExpressionValueProvider_SetValue_mC3B6F6A202024B5C6F4E98CF0B9E0CB2FD6DEED0,
	ExpressionValueProvider_GetValue_mB043C3CDB8B8ED124DDEE76B83A813CAC6BF28EA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JsonArrayContract_get_CollectionItemType_mEAFE112AEAE56103CB92E06253C5C2CE705F6758,
	JsonArrayContract_get_IsMultidimensionalArray_mE912AABFE131E546E0AC4B325709C082ECB144C2,
	JsonArrayContract_get_IsArray_m24137A4AA58764A1DBAEC396986D2D11B3ED2A4D,
	JsonArrayContract_get_ShouldCreateWrapper_mF4D684BF4B87EF2903BCD47E80E3A8C5E4C37522,
	JsonArrayContract_get_CanDeserialize_mF9ED7EF3A1D19223A465C5FCCD624D725E8CEE4C,
	JsonArrayContract_set_CanDeserialize_m36D5A45CF5EA0715BD3A9800BD065F083EFD8693,
	JsonArrayContract_get_ParameterizedCreator_mB9127B30338B5F350AF6E607FD860A8FC98AC919,
	JsonArrayContract_get_OverrideCreator_mC25F0E9DCCEF7D2B0DADFCEA11527AC07707C2CE,
	JsonArrayContract_set_OverrideCreator_m8A98E265A7E73413A61E9227540074D2D31BADFE,
	JsonArrayContract_get_HasParameterizedCreator_mAF1B29B54276FAFC7107D47FB891A62A031F0B82,
	JsonArrayContract_set_HasParameterizedCreator_m53E4A947C9306EABC0C505F836FE44A753076D53,
	JsonArrayContract_get_HasParameterizedCreatorInternal_m97B73D1E2740B034BF3169DB82DDC82D3CBBA2DB,
	JsonArrayContract__ctor_mF6D3D64944FE7112CACF3E9830E2336A34D4F8E1,
	JsonArrayContract_CreateWrapper_m98284EF26E40CB5DF0CA9DA68579A350BF7E6F49,
	JsonArrayContract_CreateTemporaryCollection_mBEB54BFE6AF247507A980A01CAD4B2FE32D4C506,
	JsonArrayContract_StoreFSharpListCreatorIfNecessary_mC7377ADE8DF2ABCC0FE01A355A2E53D5F9A8BB57,
	JsonContainerContract_get_ItemContract_m6F77C60E3CA205AB20B11B8C5883E10C8CF44D45,
	JsonContainerContract_set_ItemContract_m575EE3C0BD44A6B2B7B09CB53D93D3E9C87F6E57,
	JsonContainerContract_get_FinalItemContract_m8E7F3DE58928CE86634FA11C12608B1AF41E615E,
	JsonContainerContract_get_ItemConverter_m65BD3A1A619BB764E67C28340CF982742000A87C,
	JsonContainerContract_set_ItemConverter_m5275B3AF49CE6BFB487A93C3EFC217D145EE9F0C,
	JsonContainerContract_get_ItemIsReference_m0CD76DDEE8A706AEE13AB4C3070239B67D4306D3,
	JsonContainerContract_set_ItemIsReference_m2DF2B6D8775CB547609987D0EEEDA4D8FFFA4445,
	JsonContainerContract_get_ItemReferenceLoopHandling_mBBE2464C486B2EAC9249040AF56C63EADF86F331,
	JsonContainerContract_set_ItemReferenceLoopHandling_mE763F64A84746F6981DFE84A314FA874E6030FAE,
	JsonContainerContract_get_ItemTypeNameHandling_m3066802530448D89BE24AB7672140194EAE96160,
	JsonContainerContract_set_ItemTypeNameHandling_mA708CEE01B307BD5D9D665A6D756A33588FEC4E8,
	JsonContainerContract__ctor_mCF9664476A7B4CDDA8D24832D07D104F5AE36DE8,
	SerializationCallback__ctor_m034A339E0CFACB331C28002C5E1F75E11AF8F04A,
	SerializationCallback_Invoke_mF4530F2447A1391F14F00AE8F4BF8723B950E081,
	SerializationCallback_BeginInvoke_mB0EC258E7BE8A25960CE0744395A5012A3C4E6F0,
	SerializationCallback_EndInvoke_m209AA3DBB77201449CBA5FCC36E244B26D495DDA,
	SerializationErrorCallback__ctor_m970746DB3663107008676F9B7C860ADFF5C6EF25,
	SerializationErrorCallback_Invoke_m2241C9CBE86AF12E7F0DC0678B77541B323CAD9F,
	SerializationErrorCallback_BeginInvoke_mADD81828292595908BEEC78918CB7EED1404FD87,
	SerializationErrorCallback_EndInvoke_m9873DDA24FF394525888948F492BD49E5C31DF20,
	ExtensionDataSetter__ctor_m7976E945D2C0C193B00D8CF8EE747FB25EF252A4,
	ExtensionDataSetter_Invoke_mA4C6C271DB78FB5F150FBDC5F5827FAD2F1F3FB0,
	ExtensionDataSetter_BeginInvoke_m63E2139E4439FC5DCB58D94FF3F713124F507350,
	ExtensionDataSetter_EndInvoke_mAC33DC5FF694FC86CA521B9D50C6C47C9027D184,
	ExtensionDataGetter__ctor_m102446EE01B71BE0D9737453284FA95C1D3E160F,
	ExtensionDataGetter_Invoke_m91E608874E43EA88B04B61D77758BEFA3813014A,
	ExtensionDataGetter_BeginInvoke_m0ACFADAD0B6F9C718391C213DDAA294FF41125CD,
	ExtensionDataGetter_EndInvoke_mA7637A1583892B4177D6080932F8F0E79A5AD3D2,
	JsonContract_get_UnderlyingType_mE0084E5B73A9526ABAC59E4EF1E105251484C88A,
	JsonContract_get_CreatedType_m442AF165A70728A21B3AD09BD6478A425D8AE502,
	JsonContract_set_CreatedType_m98965FF7A972A2039FF951189730D63F01382B90,
	JsonContract_get_IsReference_m11C5F106339EB17EB2E245E7D8789C84CBE9D5D2,
	JsonContract_set_IsReference_m6DDC8BA4E61B6D26629BAEA75AB630C96A08E249,
	JsonContract_get_Converter_mFEAD9B249CF7BE7FC31A02F55E6A9CDB2CF877E1,
	JsonContract_set_Converter_m1C84ACD36B89C7943358D26519FB00E82847E47E,
	JsonContract_get_InternalConverter_m57F161FE7233C862A8CD74722CFE211940A062A7,
	JsonContract_set_InternalConverter_m290298D2C6F6DC9AA26D26B1700614C5A322958A,
	JsonContract_get_OnDeserializedCallbacks_m19EFB524A1F05C133F64D3367800C1FD6D0E651A,
	JsonContract_get_OnDeserializingCallbacks_m73E7E2BF768EF40630A212378C92FAB0649DE2EE,
	JsonContract_get_OnSerializedCallbacks_m2185A0545CEFFB953819A5325F15B502DC07AF46,
	JsonContract_get_OnSerializingCallbacks_mA08247B7610507E69986A10C6BA22CBC9633C811,
	JsonContract_get_OnErrorCallbacks_m25AC02640934B2F555DD7023A648940B43315A99,
	JsonContract_get_DefaultCreator_mB7579F859D4492CC933B5F5622D3C003B2BDA1AA,
	JsonContract_set_DefaultCreator_m75B74EBBD288130AC93A26A446DA97B76E3CB812,
	JsonContract_get_DefaultCreatorNonPublic_mF37271A60F600E6D71B9B130BC9C88F1F9053D69,
	JsonContract_set_DefaultCreatorNonPublic_mB2080A71D3002F22F425D33688BD54D84E044003,
	JsonContract__ctor_mBB28B39C7A2001CDB4EFE81F0C0E31CF6628C90B,
	JsonContract_InvokeOnSerializing_m5DF16DA6841C7114894C2424CA674B3529D3F511,
	JsonContract_InvokeOnSerialized_mCBE1D586F783169AB1A11653FD740BCAC2F68CAF,
	JsonContract_InvokeOnDeserializing_mAB3C96F578936312DB1B92705DD690E142AE3313,
	JsonContract_InvokeOnDeserialized_mFF44B5AECCD00F539D4DE46AB77D8F2F9286C775,
	JsonContract_InvokeOnError_m5E55591506C09AE37333EDE63E4908D3560E9034,
	JsonContract_CreateSerializationCallback_m1D1C04D219134B7E1971D61178F13C6C3E6388C1,
	JsonContract_CreateSerializationErrorCallback_m8BE3098020ACA1A98B57974F640C33A38EC1CC35,
	U3CU3Ec__DisplayClass57_0__ctor_mA6AC153231123C37D38F11ED56A6025BEEF490F8,
	U3CU3Ec__DisplayClass57_0_U3CCreateSerializationCallbackU3Eb__0_m5EF8D51FAA219EA795071B55B5CFBBBFEA985FCB,
	U3CU3Ec__DisplayClass58_0__ctor_m468293CFBB821B032C41538226AC5EFD01887A89,
	U3CU3Ec__DisplayClass58_0_U3CCreateSerializationErrorCallbackU3Eb__0_m5B00E6278CC33833DF5325875FD8172117ADF85C,
	JsonDictionaryContract_get_DictionaryKeyResolver_m694F96281481FC1A07DD22B75F3CC0F2409F8AD8,
	JsonDictionaryContract_set_DictionaryKeyResolver_m7730DDF4BF17728862C25FC540F1BD7F52183B41,
	JsonDictionaryContract_get_DictionaryKeyType_mDD654476C058C529DE631E77513ADB23C5F5C22D,
	JsonDictionaryContract_get_DictionaryValueType_m8BB2006AAE979645FD1E679BFF3D50BC60E159CC,
	JsonDictionaryContract_get_KeyContract_m4BE3BE029727F9591B58C341749E2BAFA5EC91D3,
	JsonDictionaryContract_set_KeyContract_m87B65AD616AD5BAE49DD786CE996BFDAA07E25AD,
	JsonDictionaryContract_get_ShouldCreateWrapper_m0221A20F9AFEB94681E1132D1DF939ADCD0B0430,
	JsonDictionaryContract_get_ParameterizedCreator_m4437DE8F37E2317090A58F06BD6B49722BF5A688,
	JsonDictionaryContract_get_OverrideCreator_m7C77ECB0C98EA6E06061391D01532304ADD6E645,
	JsonDictionaryContract_set_OverrideCreator_mD75F873EE4F0278A4A60FF679C70DE539546F146,
	JsonDictionaryContract_get_HasParameterizedCreator_m2E6DDAEAC2A39AA2DD9B6410A01A3986978C8B0C,
	JsonDictionaryContract_set_HasParameterizedCreator_mB5449625C488CE09A1D8F838B985696CEA4B2108,
	JsonDictionaryContract_get_HasParameterizedCreatorInternal_mEB1A5240D5E7B2EC3A0306B63B4A23B031CE7190,
	JsonDictionaryContract__ctor_m3FA1679AFF7E73CB7A5824297DA17FA8C55D37FB,
	JsonDictionaryContract_CreateWrapper_m4E8E5F47E583FD314A2E93D68340933734DE4B69,
	JsonDictionaryContract_CreateTemporaryDictionary_mB6FFBD7923CE5DA789D7DE4597CC2BFCB28A315B,
	JsonDynamicContract_get_Properties_mD840527AC93C463C3C53978CE8D0FB5724764A94,
	JsonDynamicContract_get_PropertyNameResolver_m55A060B6A7B13C610EDE9B983207FD388A848ADB,
	JsonDynamicContract_set_PropertyNameResolver_m79A00D8C0C2C162452428A44F255225A6C123450,
	JsonDynamicContract_CreateCallSiteGetter_m9308DB4AA72A61554337A84D9BE23C5973976DD5,
	JsonDynamicContract_CreateCallSiteSetter_m2E4F4A7DA48FF2F841C7DD77F64165AC9A08B1BE,
	JsonDynamicContract__ctor_m856C0589DD9DD0C61A000421242FE7443CFB72C8,
	JsonDynamicContract_TryGetMember_m0176BA2038F5A142ED428D3E19D7981767B42C0B,
	JsonDynamicContract_TrySetMember_m025FC0AD054CECBCB64316C9D0A36D972DA2644A,
	JsonFormatterConverter__ctor_mD09B510D858E55E25445F526A67883A305D6C20A,
	NULL,
	JsonFormatterConverter_Convert_m3C31BFAD90DA212608D78A5E290715BA70F3A31C,
	JsonFormatterConverter_ToBoolean_m31C9665F91E643F5F6A7322908C0CB79FC9B2CCD,
	JsonFormatterConverter_ToInt32_m08BD69A88EB2D8B5B5C00413C96E657BE3705903,
	JsonFormatterConverter_ToInt64_m4A2804D7E5FBF0A92FEB3E5A347A5DBC928B0F06,
	JsonFormatterConverter_ToSingle_mACB682B302D2D99FDECDDE0B631207DFB76BB602,
	JsonFormatterConverter_ToString_m67BFE5AA97BCE700D1DE3C7FA36801D7E21C92C3,
	JsonISerializableContract_get_ISerializableCreator_mA8116BF441C7D2F3D59F4BC213DAD397D28BC7B9,
	JsonISerializableContract_set_ISerializableCreator_mB4EC114FDB78FB8F6A6A508E42D634C936A8FC75,
	JsonISerializableContract__ctor_m3A8193BCE1B2BF2AD003DE22086F51F640E4FD16,
	JsonLinqContract__ctor_m290A5E599B83FA984E6F36C13EBA1666E312DD1D,
	JsonObjectContract_get_MemberSerialization_m1D215E92E7385199BEEA6BF22BDAF70DFB5E4360,
	JsonObjectContract_set_MemberSerialization_m7215647FDBABE568FB08A8C35D38653C016DB9D1,
	JsonObjectContract_get_MissingMemberHandling_m065105E5B82D2618A399B37307108E3EF80B543A,
	JsonObjectContract_set_MissingMemberHandling_m764058C5197A263266C7ABA4F6D35364FD24E03F,
	JsonObjectContract_get_ItemRequired_mE98D1B218CF9227FCA14A138E90044DEB9B74936,
	JsonObjectContract_set_ItemRequired_m84FBCEBD2B84A5B1B18050A29A90A72384EF93A9,
	JsonObjectContract_get_ItemNullValueHandling_mF5A4C7D1B229F2478DFF3307AD8A33459DFBD05A,
	JsonObjectContract_set_ItemNullValueHandling_mA077DF7CC913461524E454FE362472B3020EBF7C,
	JsonObjectContract_get_Properties_m1E3AD6BD850BCA3FD49032377CCFBC2876026AA7,
	JsonObjectContract_get_CreatorParameters_m103A4D96616ADA884DC916D60C3EB55E777ECF18,
	JsonObjectContract_get_OverrideCreator_m120DBCAD83B6DA281A437582E7AB78D6847E1432,
	JsonObjectContract_set_OverrideCreator_mD6145D3DDA2DC1CF56E2BF18A63F2C6FCE5CC514,
	JsonObjectContract_get_ParameterizedCreator_mC7557E0587CD28329688516C8BC5E1C52A5BE1E6,
	JsonObjectContract_set_ParameterizedCreator_m3E5D339F9FA060B6983F003528A130353F816109,
	JsonObjectContract_get_ExtensionDataSetter_m57E41B46FFEBEB00902ABFCC921276520A10DDE1,
	JsonObjectContract_set_ExtensionDataSetter_mE962EC0B83961774CD386559DBD5BE7A06055F37,
	JsonObjectContract_get_ExtensionDataGetter_m19E3FFC4BBDF5DCF9C3EA2E41567023BFA83D21F,
	JsonObjectContract_set_ExtensionDataGetter_m5B03F8C525C2808AAD5600A1DFCCD39CE88D7E85,
	JsonObjectContract_set_ExtensionDataValueType_m909D2DBEBC8DB862DF88C4BB08DEC6AE64C27D93,
	JsonObjectContract_get_ExtensionDataNameResolver_m11FC4F958AFEC204E57287C7B7E0F57354A8C834,
	JsonObjectContract_set_ExtensionDataNameResolver_mA8FDF2E3B17BE6BE93C34ED3CD3A5E515419A78B,
	JsonObjectContract_get_HasRequiredOrDefaultValueProperties_m505B2BCA9D9BCCCB61B5949E74D13127568C761C,
	JsonObjectContract__ctor_m3747070511B934D9AF1C9F5559E4BAEF491D2461,
	JsonObjectContract_GetUninitializedObject_m640B1C14884E35C920F871DDB3E2C9E4305DFD52,
	JsonPrimitiveContract_get_TypeCode_m04B173C5426475AF94126ADEDFD230E317F930EE,
	JsonPrimitiveContract_set_TypeCode_mD3EE2E2BDC2EF286A72DEA9FCCDC23200C245C85,
	JsonPrimitiveContract__ctor_mFF896C487ECD98D4102904D3F6A51B1F7F522E86,
	JsonPrimitiveContract__cctor_m1E287FB2EB109FCA1D6192854C9F89243C3254E9,
	JsonProperty_get_PropertyContract_m0B88E8230316FBC8A5D2074FBF1D4DADA2F8E7E7,
	JsonProperty_set_PropertyContract_m2B2D17492F6B92DA4316BB0B4D02457EAD25D26A,
	JsonProperty_get_PropertyName_mC4B6B67B094AAD67D122971133C599F6E891C32B,
	JsonProperty_set_PropertyName_m3B8680BBC5F40C0CC5CBB6172CF7F460D886C033,
	JsonProperty_get_DeclaringType_mAC6337F71FFBBB71C77944D34D55E871ADF98AA4,
	JsonProperty_set_DeclaringType_m212950DFF2F651F9240FD5843A328C30D298F336,
	JsonProperty_get_Order_mB082C04FEA20F7B444D5DF7F1758D4CE9C98AC38,
	JsonProperty_set_Order_mBB0ECB9C5633188BE63CFC7CCB578F6BF5543327,
	JsonProperty_get_UnderlyingName_mB4EA21466053D86D317B7F45EDCBC44056539D55,
	JsonProperty_set_UnderlyingName_mE6333333CF67219BC8ACF280079CADF688509A2C,
	JsonProperty_get_ValueProvider_mD8BC092B52E89F58C3228A9BE9527A95E73AF52C,
	JsonProperty_set_ValueProvider_m211EF301287FBABDDE3272271FC8EC0B0A10254D,
	JsonProperty_set_AttributeProvider_mD25FBBBD604F49319E4AB1949169F3AEA3DA5E6C,
	JsonProperty_get_PropertyType_m6B3AE9E3A110A5800E84300F7CBD9ADF13B275D1,
	JsonProperty_set_PropertyType_mD915B2D96EFD441149EFC7D6D67F961C1A4D4DCA,
	JsonProperty_get_Converter_mB354BB5CF3FF375D46F11D13C9F91FE517569178,
	JsonProperty_set_Converter_mC0879D94A0B2C303C6EEAFBE1C877E3E321CAD68,
	JsonProperty_get_Ignored_mCD40902CE424EEE05A9C98A67FE9F131AE6F0F18,
	JsonProperty_set_Ignored_m2D1671501F476D2F4D89B5FDA653A8F98361367B,
	JsonProperty_get_Readable_m5D6996D3F73BAF6C6DC3BFA7C9554983D632EA88,
	JsonProperty_set_Readable_m805FD592FD9A49944FAC183C37D1D686791208A4,
	JsonProperty_get_Writable_m603494F6C7D90E2DF69612F660EB77DFC00817F0,
	JsonProperty_set_Writable_m9668B31D602D93E3A2EF5E86DB831D3B20D935FC,
	JsonProperty_get_HasMemberAttribute_m8410E984D65A4A0D8CC6F425941FCEEAE33BCC4B,
	JsonProperty_set_HasMemberAttribute_mFE0B334F0F966CD90DB5366FB16296EEBEDA1D6C,
	JsonProperty_get_DefaultValue_m9F7C4702DD2C81CAE956B9FE06A21F2BF6D73AC3,
	JsonProperty_set_DefaultValue_m5687BFE228338FF5A7A5FAFEFE99FFB88D54BA21,
	JsonProperty_GetResolvedDefaultValue_m0F79FBC4EE28042935C27E84CA7B6A27EA70F020,
	JsonProperty_get_Required_m560C3B1D45DE3778B6409DEF15A68EB34DE051A6,
	JsonProperty_get_IsReference_m6D4D3E09456EAE5DACE7A8BBC6D3E7326EFB5FD1,
	JsonProperty_set_IsReference_mDBA7EC3CF7F3B5677F788081ABC039D3FAC1D1C7,
	JsonProperty_get_NullValueHandling_m20C37276D59A8FFE4FA83234BB72AA2B2BB111AF,
	JsonProperty_set_NullValueHandling_m2CB4BA8F08E32FD21E0C967C597F478B36EEDCB0,
	JsonProperty_get_DefaultValueHandling_m8FDCF1FA86330D7398C6AA93BDCCA81362362441,
	JsonProperty_set_DefaultValueHandling_mEBF1DDAD284ABB620EECF47B67A007B15075F844,
	JsonProperty_get_ReferenceLoopHandling_mDAC3521C25B91CD78EF3A3F8A08575CB557AC210,
	JsonProperty_set_ReferenceLoopHandling_m8C509109EBFCF4258424B3B95A7CBD66C5D35775,
	JsonProperty_get_ObjectCreationHandling_m69CBFA45BDB11AC5B37F2BC21FBEF3DCF2627035,
	JsonProperty_set_ObjectCreationHandling_m9F6060963F582F6B5E2AB1FF9ECD554B9B5CCA44,
	JsonProperty_get_TypeNameHandling_m6708C116A993419FF69BFA6D5ADDB4C3B42DAAC1,
	JsonProperty_set_TypeNameHandling_m9E28CAE6750DF03C9A8BE28AED8DD355BCA34987,
	JsonProperty_get_ShouldSerialize_m7286940398E365E6FE05BCC66A17DA17674D2ECF,
	JsonProperty_set_ShouldSerialize_mD9E5DDDB79E6F6926558E93C29BCEBA37D1703A7,
	JsonProperty_get_ShouldDeserialize_mE12E10AD5F0ABA90A22BFD7F84F2508FF04A582D,
	JsonProperty_get_GetIsSpecified_mBE29FC0A2D499F9E0EDF24EECF5CDE7CAF9369A8,
	JsonProperty_set_GetIsSpecified_m36131877DE9B1A9AE8C93745FA0339F6560EFEDA,
	JsonProperty_get_SetIsSpecified_mD5F8A991E4B2B64BB92BBC40D95B99DE8B09DF91,
	JsonProperty_set_SetIsSpecified_mF91E426C2D42632DC25AED3D51AA0F788758D616,
	JsonProperty_ToString_mDEA1DAD3B4B80CF614B2D2749E973C734943655F,
	JsonProperty_get_ItemConverter_mA22671473B62F6DD761DC7253806D61C149E0457,
	JsonProperty_set_ItemConverter_m0C448E942E4991A66E0CABACD251943CF8A1CD2A,
	JsonProperty_get_ItemIsReference_m89DD6D113A51AFFDFAFB6EA9BA84528ABE65F989,
	JsonProperty_set_ItemIsReference_mB817D311FCE34A65A42C096427637F95161D4162,
	JsonProperty_get_ItemTypeNameHandling_m4DB1BFC69030FF7797FD8DF728C31B4B2E331877,
	JsonProperty_set_ItemTypeNameHandling_mBE904EF9267CF68EB2ECEC8FE9A6C6E44C76BAE6,
	JsonProperty_get_ItemReferenceLoopHandling_mB25B842882A9708B06A096AADCBEFF39332A252A,
	JsonProperty_set_ItemReferenceLoopHandling_m1E858AEB84D30F7B24FA71ECC6695EBF6DCC4726,
	JsonProperty_WritePropertyName_mFEA67C79463450A73C2D0C635F9D5814125D75B3,
	JsonProperty__ctor_mABFD1A376901EED3B95C62A299286EAA4C5D60E8,
	JsonPropertyCollection__ctor_m2E403FE3F91492837D473554B1299CBEDB36C4DA,
	JsonPropertyCollection_GetKeyForItem_m5BE0DFD498BFF694D854BF776D559B111DB1205A,
	JsonPropertyCollection_AddProperty_m4044DC5A15B7FB6B750A93C3C20F65DA40587EBC,
	JsonPropertyCollection_GetClosestMatchProperty_m1AD3CBA8CAFF3CADEE5FACACCD5DFB6B4D05E6E8,
	JsonPropertyCollection_TryGetValue_m5E757A7EE6699CE60CFF77BEED6FE9C8A87A994C,
	JsonPropertyCollection_GetProperty_m057B2EC613A6B27C543E7BAB3C2FD5DB4DFD736D,
	JsonSerializerInternalBase__ctor_m9924B594FBBB7CF6D608BE28919E9D521A682FCE,
	JsonSerializerInternalBase_get_DefaultReferenceMappings_mF07E4387AF858013980340E70C14F8981C258ADF,
	JsonSerializerInternalBase_ResolvedNullValueHandling_m59CB67F6BBD682B9C9376E981A69A8A53D2B5F93,
	JsonSerializerInternalBase_GetErrorContext_mF10726F0352994E000C1ECD9EBFDFAFF92C6F402,
	JsonSerializerInternalBase_ClearErrorContext_mC300E0725B338BBCFFE263633171865D0F4B694F,
	JsonSerializerInternalBase_IsErrorHandled_m70C5C217285CF3B33F86F7D3527701EE072E1605,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_mA3CA65464B17BE559BC81A3F8FB8E865A2C3A4E3,
	ReferenceEqualsEqualityComparer_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m34BFDEB7B6EB04CD7D23C6A53E627862155C5A25,
	ReferenceEqualsEqualityComparer__ctor_mEB61B4C5A39FA645DDAF54B10C91ED41D8ED0097,
	JsonSerializerInternalReader__ctor_m84B910B18B4A333391D48F90DF6E005D87AECFDA,
	JsonSerializerInternalReader_GetContractSafe_m38EF535F3F6AE2698416A6AB04A8F49ACA5079ED,
	JsonSerializerInternalReader_GetContract_m417881EC192F8E6463A01F615F532F3D776CAE5A,
	JsonSerializerInternalReader_Deserialize_mBADEC4E0B606D2D25E9DB0F814EE349F74294364,
	JsonSerializerInternalReader_GetInternalSerializer_m74A94813F85F4A842ADB1966F06AF985940BC707,
	JsonSerializerInternalReader_CreateJToken_m392836D279982DF443FB754CF783D8DC2FC48E33,
	JsonSerializerInternalReader_CreateJObject_mA11DF73964BA4BA03A13110F4789F8BE53EB2FE9,
	JsonSerializerInternalReader_CreateValueInternal_m2143D4244958F80AB3D7A2CE4EB532E51BD46E41,
	JsonSerializerInternalReader_CoerceEmptyStringToNull_m82CD37FDFA6FBE9A21D7EBED14850D6368B6B164,
	JsonSerializerInternalReader_GetExpectedDescription_mB7B241939E1352F9BD4ED8542BCE9AEC74014910,
	JsonSerializerInternalReader_GetConverter_mDA6C98DF1A63778799D3E7ACA2BB512B45A823ED,
	JsonSerializerInternalReader_CreateObject_mC442CD0B382ADC9E1CACB60C76DB80DAE239795E,
	JsonSerializerInternalReader_ReadMetadataPropertiesToken_mE7EFBE1AA260787A616AFA9A7F39BED1C2B4465E,
	JsonSerializerInternalReader_ReadMetadataProperties_m0EEA84F640A074425430733364C72A1B61CDAF8D,
	JsonSerializerInternalReader_ResolveTypeName_m45FFCF99B4E199728915DB902C47CDF8B97D5348,
	JsonSerializerInternalReader_EnsureArrayContract_m6300A06BED91530127768F19F2F8A46995AC7F2C,
	JsonSerializerInternalReader_CreateList_m8F91654B57A26686A907278DEFA86DBB75532821,
	JsonSerializerInternalReader_HasNoDefinedType_mC13B8E91C19C981F5D75A3D223184BDF794CEBFA,
	JsonSerializerInternalReader_EnsureType_mF55F1BD9D0540AF9A0D58F08D6B6AA0F6AA78166,
	JsonSerializerInternalReader_SetPropertyValue_mE604DBB1E2133A32DA492C18BC7C5227ED220277,
	JsonSerializerInternalReader_CalculatePropertyDetails_mC3750E84FE2D0A374888840DC8E8EC53DF333870,
	JsonSerializerInternalReader_AddReference_m408136601F7A55FAF2DCE8A795FF62750D46FC00,
	JsonSerializerInternalReader_HasFlag_m5B915C81BB1A77886B67A2F003A2A04944A2BEB8,
	JsonSerializerInternalReader_ShouldSetPropertyValue_m9CA1330FAE1539772C23690804E9ED4500261C4F,
	JsonSerializerInternalReader_CreateNewList_m2D1B33586CC7C408B619F0167B4FC64AB0E0D9FE,
	JsonSerializerInternalReader_CreateNewDictionary_mFAD6A679C07C8DE6337219EDCC4AD5B554C6BC34,
	JsonSerializerInternalReader_OnDeserializing_m268BA36974DA49878FE4F7B550DBBDEF0517D8E8,
	JsonSerializerInternalReader_OnDeserialized_m0083FA8B79FC36B7E74A67FF54C0D4E9326CF4D3,
	JsonSerializerInternalReader_PopulateDictionary_m7F33C2CB98A3BEE15E45B463F9FD78D7D4263AB9,
	JsonSerializerInternalReader_PopulateMultidimensionalArray_mBF5F6B366BBF5BC1E35EA8FFDC165A9B84C8CF0C,
	JsonSerializerInternalReader_ThrowUnexpectedEndException_m101EFD78937CCF2DC3ABABB95227949806059955,
	JsonSerializerInternalReader_PopulateList_m8A9B2EF70BDDD2097CBEFADF4F61968FA6A498A7,
	JsonSerializerInternalReader_CreateISerializable_mF2D90D10426A9ECEEA037565ABD4ABF110BDF8D7,
	JsonSerializerInternalReader_CreateISerializableItem_m5F10D375448F2308DD5E18E2440A6CBE9556DA6E,
	JsonSerializerInternalReader_CreateDynamic_m24C9A857ED8B3896286A728101115B207188F762,
	JsonSerializerInternalReader_CreateObjectUsingCreatorWithParameters_mDB395A12FD6087B7ED7AB294B039FCD645C06D75,
	JsonSerializerInternalReader_DeserializeConvertable_m3038C49BB7226A43F035742300541E6716B8C305,
	JsonSerializerInternalReader_ResolvePropertyAndCreatorValues_m50795BC70D9B5E777FCDB62BEBBD9C5E1D357D1C,
	JsonSerializerInternalReader_CreateNewObject_mD2AAE7099669E5FDF50FD5071D62B0E71DF38F2D,
	JsonSerializerInternalReader_PopulateObject_m54CC247DA1F185408A10836A77456397FE273BD8,
	JsonSerializerInternalReader_ShouldDeserialize_m6904DF089934441900B22374416545F7553D7CAF,
	JsonSerializerInternalReader_CheckPropertyName_mBAE2B3F98D8F326C3F6B919025F441597A7B078C,
	JsonSerializerInternalReader_SetExtensionData_mD00249CF9B85AB62AB055A422D897C18113D3C5E,
	JsonSerializerInternalReader_ReadExtensionDataValue_mB627B7B92BEF9C7ADEDE29172A9793A7FE65CF1F,
	JsonSerializerInternalReader_EndProcessProperty_m0A115FD2984F8FBFD1EA281E05C5B358E1014A79,
	JsonSerializerInternalReader_SetPropertyPresence_mE1FE421F01D285CE4F6E0E5BE06F1C79E52BBE56,
	JsonSerializerInternalReader_HandleError_mB3C42C7EDC61BB938C94133DE2EEC0EC9FE5B94F,
	CreatorPropertyContext__ctor_mF743F3DBE3DE03A4079C503DBD0B2E4EAD035CC7,
	U3CU3Ec__DisplayClass38_0__ctor_mF0EB3DE96FACD9AD3FD9F3DF2EA616B1D5E02EFA,
	U3CU3Ec__DisplayClass38_0_U3CCreateObjectUsingCreatorWithParametersU3Eb__1_mD7DF82CDA2D357542E67243C7DF7E6711FA1FBFE,
	U3CU3Ec__cctor_mCDFABC3C0AC5181A60BC55CCE535F428906E8A8E,
	U3CU3Ec__ctor_mCC15938D9283F207914384F3C723590548E77EDA,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__38_0_m3B46B0CDC0AAC57F21D183E89C16FEB1D9C332CD,
	U3CU3Ec_U3CCreateObjectUsingCreatorWithParametersU3Eb__38_2_m178470408855EF773A5835D565EA26AEC4843620,
	U3CU3Ec_U3CPopulateObjectU3Eb__42_0_m3CCB0D8AB2ABE4328681549F15DE05D9E1FB943D,
	U3CU3Ec_U3CPopulateObjectU3Eb__42_1_m57B62ACC149660BA895F211C5B01943DA239C634,
	JsonSerializerInternalWriter__ctor_m33A513551DFD57A754CBC292875D8647D9C90354,
	JsonSerializerInternalWriter_Serialize_m707CFA0C0B62B161BF402C87255DEA7BDA1F1BF0,
	JsonSerializerInternalWriter_GetInternalSerializer_m659C0D3DCF0610F80DE4FDF64A1B146145C49CD5,
	JsonSerializerInternalWriter_GetContractSafe_m21516BE389D4587D5DDF20710A4E3BD578FF0B98,
	JsonSerializerInternalWriter_GetContract_m35B22C5C5607A598BA1034AF4C036CEF8B941641,
	JsonSerializerInternalWriter_SerializePrimitive_m1F9F8F0F26D50DC1473475D1BB2444D125B06337,
	JsonSerializerInternalWriter_SerializeValue_mD61EB3DBE3D0C93FE456328AE34B1BFFA0551449,
	JsonSerializerInternalWriter_ResolveIsReference_mEA802A7DBEE556E52CDA196B6FFE605488F56318,
	JsonSerializerInternalWriter_ShouldWriteReference_mC3A021ADB209FD105A1F0A12E1A1C7CEDDCC7F56,
	JsonSerializerInternalWriter_ShouldWriteProperty_m5E6672F648A0E7F5250372A0E08AA458426DDD35,
	JsonSerializerInternalWriter_CheckForCircularReference_mDE39910B958FE1D68FB8D59F66351EEB703378CA,
	JsonSerializerInternalWriter_WriteReference_mC605E2E5553627A5770962B8822A5C66E1F44D0E,
	JsonSerializerInternalWriter_GetReference_m3FA8F945FB8855C1E8A6E03BD761A9C38A38B6C5,
	JsonSerializerInternalWriter_TryConvertToString_m74B307586429D228216FE93B9113C4A65C944371,
	JsonSerializerInternalWriter_SerializeString_m60887799FFDBE7A94349AB1525734D7AAD382B58,
	JsonSerializerInternalWriter_OnSerializing_mA38E0CFBB65D91E3CACF7886BF8902D671F51DD4,
	JsonSerializerInternalWriter_OnSerialized_m04503F54C55F4EEE8E669C5DBEE2FC4C7F0FF439,
	JsonSerializerInternalWriter_SerializeObject_mF0EA7E8A7D0A9E843CDE5E83CE18B7E03D8F25A4,
	JsonSerializerInternalWriter_CalculatePropertyValues_m17CC13E541FAD756B2544ACEF50E85D567A51AEA,
	JsonSerializerInternalWriter_WriteObjectStart_m869B63137B88D12E4B0A32BC32ADEEDC659564AC,
	JsonSerializerInternalWriter_HasCreatorParameter_mDD45720C7BC7F0DB26B95E1B99C73B7740685AE9,
	JsonSerializerInternalWriter_WriteReferenceIdProperty_mA5E2DE1A9D72D6777CBEAE39D18DA4352F91810C,
	JsonSerializerInternalWriter_WriteTypeProperty_mB38CA27C4E202C93794B676DF635DBE33E4562EC,
	JsonSerializerInternalWriter_HasFlag_m3C84D0037FEBADD9D50BFB0BEAE24841226ABCFF,
	JsonSerializerInternalWriter_HasFlag_m1F0DFCC3FDC84C3E821AF042621BFF7D21CF3267,
	JsonSerializerInternalWriter_HasFlag_m343A3BF98DC5BFFB21FF727BF3EAEC63E4D929B3,
	JsonSerializerInternalWriter_SerializeConvertable_m96E202C8E55EB3201FAD7FAA308D70F726ECA20E,
	JsonSerializerInternalWriter_SerializeList_mE4C06D471B8E6746D52382143F881A0B40981CB5,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_m4674A1CF208920B4CB6F0405449F892E4E8CF946,
	JsonSerializerInternalWriter_SerializeMultidimensionalArray_mCCF2F792B68A6E8AB97EE7C4E505D73BA421775F,
	JsonSerializerInternalWriter_WriteStartArray_m2CE2821C2BEF10DBD14244AE46E5FB2F17D1CCCB,
	JsonSerializerInternalWriter_SerializeISerializable_mAD90C6519F6590F8A9915C70BDC7B42F375DB833,
	JsonSerializerInternalWriter_SerializeDynamic_m7DDB3A53FFE50ABCF123763AE0E8C3502C576119,
	JsonSerializerInternalWriter_ShouldWriteDynamicProperty_m4822FB4F8FE4D5E4164003FDC6F4CD983485000F,
	JsonSerializerInternalWriter_ShouldWriteType_m99E52666597835534A7A33D1A135DBAA29C1E49C,
	JsonSerializerInternalWriter_SerializeDictionary_m43AA3F60EB93F947A63344DC382D9744F69EFE24,
	JsonSerializerInternalWriter_GetPropertyName_mF037E280338FBF288BCDD0155E5380D6C61EF855,
	JsonSerializerInternalWriter_HandleError_m9C8E83D03C30410C95766BB1BE8E31A46D0C44E4,
	JsonSerializerInternalWriter_ShouldSerialize_m234E3479C305C5E38D143C37375D757CB8E0C107,
	JsonSerializerInternalWriter_IsSpecified_mC0E4F1C42F45907DAA98C832C1DB0C19E22422B4,
	JsonSerializerProxy_add_Error_mF2EF3D15D9A9094523E7C26CDFEDA4D48D066E13,
	JsonSerializerProxy_remove_Error_m1F1D074344139E88826E27D52FBC9FDF6B901EB5,
	JsonSerializerProxy_set_ReferenceResolver_m9B55AEC432FFEB6F6608EEA7774C7E4AAEBC8FEE,
	JsonSerializerProxy_get_TraceWriter_mDE7236F88BF24B2A80D57705DC4FC1F90058D309,
	JsonSerializerProxy_set_TraceWriter_m2E7CEDF9C8A6918CB29B5B5CC5FA912220DC31D1,
	JsonSerializerProxy_set_EqualityComparer_m0D3427DD852FA45D7812EA0878ABC025F063404B,
	JsonSerializerProxy_get_Converters_m7D0DA28309E628F03F2C5E2F046DD0C539A412F6,
	JsonSerializerProxy_set_DefaultValueHandling_m59E356F0BA097FB5D0933F278E5613BAA4E58294,
	JsonSerializerProxy_get_ContractResolver_mBEF012F2144CF88BB5D09E147D01FCD248B4237C,
	JsonSerializerProxy_set_ContractResolver_m5075C86F641EC82329F06F140EF6F2B0C5EBCB78,
	JsonSerializerProxy_set_MissingMemberHandling_m2E83B36F2336E4B7414FBA581F8954FC3B519FF7,
	JsonSerializerProxy_get_NullValueHandling_mEC7FF21CE82CD5F19329A449ADCB5CD252C605F5,
	JsonSerializerProxy_set_NullValueHandling_m2B79A43EE8DC7EFEE9B6FB16BFB9C1C8749231DA,
	JsonSerializerProxy_set_ObjectCreationHandling_m5340EC76209BE9FD9196E8A384CF24E841F55B49,
	JsonSerializerProxy_set_ReferenceLoopHandling_m60633FF1711BC421F597D9C1018C8AA9CC041883,
	JsonSerializerProxy_set_PreserveReferencesHandling_mFD00F4958D60F0E3AA94B747B0211AD14B15358F,
	JsonSerializerProxy_set_TypeNameHandling_m3FBD9D4AAB50D80487A73ABEBB6CE28D7F4BDD42,
	JsonSerializerProxy_get_MetadataPropertyHandling_mC25F1DC18C01CD262F29D4E7382038197894A97D,
	JsonSerializerProxy_set_MetadataPropertyHandling_m2C01F1F046E2355B91FF3801B2E6FF2A248C9344,
	JsonSerializerProxy_set_TypeNameAssemblyFormatHandling_mDAA8E97C1392F37D558BFDFE14F89A3491ECFAF4,
	JsonSerializerProxy_set_ConstructorHandling_m2F984C8CDFBAC765FD17B892B6FA4DBCD8A42B1F,
	JsonSerializerProxy_set_SerializationBinder_m257111821814BD400BB5F523190D1AD2CDD617D0,
	JsonSerializerProxy_get_Context_mAE9E38CD49020552815E1DA093246E21122D38BC,
	JsonSerializerProxy_set_Context_m87D66D48B7487972C2A5316BFEC386B80DFC5B2E,
	JsonSerializerProxy_get_Formatting_mD75120CBDF3E4146DD6A2ACA871FA4396EBDB338,
	JsonSerializerProxy_set_Formatting_mD9D8F5CAC54632DB441BF455F021F1C9214AEAFE,
	JsonSerializerProxy_get_CheckAdditionalContent_mBC0C0743A0B731DA2C1F60AFA75E0EFBA62CF165,
	JsonSerializerProxy_set_CheckAdditionalContent_m51B6F02E06D463137B396E428C06F3777F800A6A,
	JsonSerializerProxy_GetInternalSerializer_m7E88F076E26CBCB287598ED57AE332056C60F598,
	JsonSerializerProxy__ctor_m450813A5C9BFE4ACBB1374D30255055E836F819D,
	JsonSerializerProxy__ctor_m48ABC76757CD3D9BB7C72B0474704FA98798D609,
	JsonSerializerProxy_DeserializeInternal_mF48D6DE08B58D4791BA52237374F45ACB637685E,
	JsonSerializerProxy_SerializeInternal_mA4A87ADA184432F52798DF72C92911FD5340CFA7,
	JsonStringContract__ctor_m29822C0D0E361FF18D429DD6D8ED87CE1A4F130B,
	NULL,
	JsonTypeReflector_CanTypeDescriptorConvertString_m84300AF4F2D012CFA7D7EEBF7F83FA5570C31625,
	JsonTypeReflector_GetDataContractAttribute_mF22093A92DFAC8FEF02DBA94CDCF0729B5BCB224,
	JsonTypeReflector_GetDataMemberAttribute_m0C7037E9C4357869EF1ACD388E2483AE58F67EE4,
	JsonTypeReflector_GetObjectMemberSerialization_m594BFFB1DCF35067A078329FBF558277EEBC856C,
	JsonTypeReflector_GetJsonConverter_m5F6290D480CD2D77A78F478B8839DBB1C363690B,
	JsonTypeReflector_CreateJsonConverterInstance_m301154F582E0F7D691056BFBBBCF009BB3B83C5D,
	JsonTypeReflector_CreateNamingStrategyInstance_mF696C077FA0BE88B523B33C8BECD5EEF09BA74BB,
	JsonTypeReflector_GetContainerNamingStrategy_m3600547A4E20616F67AA956D1F1E25EA393235F2,
	JsonTypeReflector_GetCreator_m474B3070ABB1AE4562702580108F30C561DD31EE,
	JsonTypeReflector_GetAssociatedMetadataType_m53B88A5BECF179C9C647C7B3C8DE8C858F385AAC,
	JsonTypeReflector_GetAssociateMetadataTypeFromAttribute_m8F97BC6B3C0377088FAA96BF06C1EAE0627B2A2C,
	NULL,
	NULL,
	JsonTypeReflector_IsNonSerializable_m22341E5E343D2C5C7B3D9DC92E3CBD09D1F61418,
	JsonTypeReflector_IsSerializable_m5008F24D0786D5C3290EF5A486C61E1DB6F5FAA2,
	NULL,
	JsonTypeReflector_get_FullyTrusted_m8B3C3C180728D881ADD8BE997D5C854F60BB985E,
	JsonTypeReflector_get_ReflectionDelegateFactory_m1B84C786AA4BDF710B3D8687E4349845B1D52ECA,
	JsonTypeReflector__cctor_m9E98E9D497A5D8CF4CAD32C54F633BAD38DB57AD,
	U3CU3Ec__DisplayClass22_0__ctor_m45AEC28F89F0F9830013E3AC574EF508001E03CE,
	U3CU3Ec__DisplayClass22_0_U3CGetCreatorU3Eb__0_m11E5A1D76268AB0E28B4507A4184C0A29354A469,
	U3CU3Ec__cctor_mBF2DFA692E52FBFC78909AA00DF4164E9E7E480F,
	U3CU3Ec__ctor_m41345F77121364B62BD6235689A94FCB6EAD11D2,
	U3CU3Ec_U3CGetCreatorU3Eb__22_1_m5FC35D391C707A03D4E090418381CA55A75E7DCD,
	NamingStrategy_get_ProcessDictionaryKeys_mA0BB47718E74A21AA9691A3ABBE7BD839124F9C5,
	NamingStrategy_get_ProcessExtensionDataNames_mF481764166B3218F7511EDE9541511CAA281A304,
	NamingStrategy_get_OverrideSpecifiedNames_m36F4D8839E9F2352DDCD7CE2260F544C760087CD,
	NamingStrategy_GetPropertyName_mB04ED92612E21C0E2D1DF5F78B116109769EB8D7,
	NamingStrategy_GetExtensionDataName_mAC2B2314D2E11F9630A5FAE10FF32E0626E3B63A,
	NamingStrategy_GetDictionaryKey_m3483DBD7B7066C82C12DBF56C8E5F786B137C537,
	NULL,
	NamingStrategy_GetHashCode_m6CC23DF05988865D6D8B8B2B4F7067496C2FD6FA,
	NamingStrategy_Equals_m5DD4B5BBB758D1F570BDC58AB5BB34CC76D854E5,
	NamingStrategy_Equals_mE5BD95F018AC817E98B42D11383D26572909538E,
	NamingStrategy__ctor_m6E3F1A8B7B18168CF71DAC024F6696DA72F30FDC,
	NULL,
	NULL,
	NULL,
	NULL,
	ReflectionAttributeProvider__ctor_mDFA04D6DB3CD1085D5C4A10CC31013E66D92FD98,
	TraceJsonReader__ctor_mDFF4B48518BFE3985F239509546CEF7F43B431DD,
	TraceJsonReader_GetDeserializedJsonMessage_m2EC51A79AB178A896A4022BAC28EB05ACEDB9518,
	TraceJsonReader_Read_m317908643ABB850535339E49B4536B3E61FC5903,
	TraceJsonReader_ReadAsInt32_mA6D12424BEC5C9B434B78937D0B063273CF631C3,
	TraceJsonReader_ReadAsString_m768F8D8BB0E7D858A9189FC74F906C201DC8961F,
	TraceJsonReader_ReadAsBytes_mAC1F756870E1F1BA5B93AAF3120E732DFAFECF7C,
	TraceJsonReader_ReadAsDecimal_mAE12C2F15289C4C26F7AA3B569680F2185552F9E,
	TraceJsonReader_ReadAsDouble_m639DA1C7ACD3A5F52330F513692D18374F988711,
	TraceJsonReader_ReadAsBoolean_mC302CB7BF9394BF4F58391A7F8DF55C2E8115DF1,
	TraceJsonReader_ReadAsDateTime_mF1EB174F229872E7144C97673A7D069A465D2F22,
	TraceJsonReader_ReadAsDateTimeOffset_m3A2D657D7E820F2284B0C92662CD18FD7666FB57,
	TraceJsonReader_WriteCurrentToken_m495829D4A70DAA81B0273B22827CE971F8956F43,
	TraceJsonReader_get_Depth_m39A35E3212D361F9C2715FB51C833411460F1FD9,
	TraceJsonReader_get_Path_m1165599BDE969148D6847B563AA25174BD265D79,
	TraceJsonReader_get_TokenType_m44102BF5C38E3F80D9A94EF874E7C8B4050A2994,
	TraceJsonReader_get_Value_m642F6E3510027EA14D0326DE650BE34638B987DD,
	TraceJsonReader_get_ValueType_mC956D916766C35819F8C8DC759AD75F446DA5A0E,
	TraceJsonReader_Close_mE6F26C26EE18476812A49804E49236071FC1621F,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mA888E5A94A9A801FACC3A4F7CB3BA993D690CEA2,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m06E0A5A23FB4FAC4CAD5F27D410BD9A0AC9EC9F1,
	TraceJsonReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m8FE95795CA790CF4918820C50888F216662091C5,
	TraceJsonWriter__ctor_mCB8E954714DBDB57E48E7011F884DFDF6A11BB75,
	TraceJsonWriter_GetSerializedJsonMessage_mCCBCAC2AAB27DA3A6BD7B3F134E8CC541BD04C23,
	TraceJsonWriter_WriteValue_m090F44A298FDD947C98342724E700BCAF2B56EAE,
	TraceJsonWriter_WriteValue_m6DB6EDEBCB63883905D5E3F4BE7C512ACF72E21C,
	TraceJsonWriter_WriteValue_mE98BAD306FFE01F7616FD7070EB707AF4DA60444,
	TraceJsonWriter_WriteValue_m571FBAF27728796D14CE3C09796839B228D75ED4,
	TraceJsonWriter_WriteValue_m8E65D9583B2B0B44A253FA278AFF8FED0FEE882F,
	TraceJsonWriter_WriteValue_m635BDB293E4321010C4A8B8F9CFBA285752EBC47,
	TraceJsonWriter_WriteValue_m262AF5C637DC3400C4E8FDCC44BE881526E7C03F,
	TraceJsonWriter_WriteValue_mB7E21DF98E786326BDAD98EF3296A9465882394C,
	TraceJsonWriter_WriteValue_mFB13F476D1CEC7EC22A0FBF7868ECC55607A8DA8,
	TraceJsonWriter_WriteValue_mED823B8BDD21F94FF946DAD7456201F50F822985,
	TraceJsonWriter_WriteValue_m523A5988B1AE5779B0695CF310135830378F9709,
	TraceJsonWriter_WriteValue_m705F6723792F3317FF0904CC07F559129E6368A8,
	TraceJsonWriter_WriteValue_m306400AB203E4EA3C37E43D065DC7481AB18C231,
	TraceJsonWriter_WriteValue_m73B0F7FBC37FEA6CC1FA4E6B3DCFC5A731786DC8,
	TraceJsonWriter_WriteValue_mA042AFB9ABDD5502861B82BA4E6902C7E59D7496,
	TraceJsonWriter_WriteUndefined_m4C20713F57208CDEC39BDE4D5B09B96822079374,
	TraceJsonWriter_WriteNull_mA5994CB415761756E03A4F4B465ACB3951BD2FE8,
	TraceJsonWriter_WriteValue_m316AFF2E10D5EDBAC9AEF7692BD520DC8FBE2DBB,
	TraceJsonWriter_WriteValue_m534123650001D1224B88E4027D5DF422A7A2200D,
	TraceJsonWriter_WriteValue_m70956A7E128D9BB3BF3B8C71A571D6C7501B1B54,
	TraceJsonWriter_WriteValue_m29229E50900E8ED284BD416CC534D88085DB9275,
	TraceJsonWriter_WriteValue_mAF79997D38316EA5EB58DEEF44F6244F1EBA0E68,
	TraceJsonWriter_WriteValue_m9A1C52D69533A9FDF2F646B37034FACDBDBE0BA4,
	TraceJsonWriter_WriteValue_mFAFB8D90B902F747BFFB52F28AF330C24D331C65,
	TraceJsonWriter_WriteValue_mAFD3E8C98EC4795834EDB92F48EA43617FE8B472,
	TraceJsonWriter_WriteValue_m338E12BB33ABDA8881DC6409D526A65C406C79EC,
	TraceJsonWriter_WriteValue_m9D8B92242951E588B56CDFD1C0077F9738E2495E,
	TraceJsonWriter_WriteValue_m56486D1D7BE620E964AB5A73A37736FD6B955B56,
	TraceJsonWriter_WriteValue_mF12DBB84449438111F17639C647FCCE453F7C9BA,
	TraceJsonWriter_WriteValue_mA1DBC1BCF410930ECAE10F20A97D71D8BB713986,
	TraceJsonWriter_WriteValue_mA2902F2932109772FB3CAD6D96A5E63ADD9E5B02,
	TraceJsonWriter_WriteValue_mFEB5B06854076E071FDFA290441CE6E0ECEA6373,
	TraceJsonWriter_WriteValue_mD0F5333E20F732BCE89A54F03A882182D2E55AA4,
	TraceJsonWriter_WriteValue_m90D42BBEA5F7C1869EC3556A1B4287B59D1F2DCA,
	TraceJsonWriter_WriteValue_m9070033F23876070AAA8458A7C7E382EFD4DED31,
	TraceJsonWriter_WriteValue_mE71AE7EDA7396C6DDEC7BA1A1998C9EC535AA3E1,
	TraceJsonWriter_WriteValue_mDF8EF24DF5BCD05716C94AF5A62C3665A18DCDC7,
	TraceJsonWriter_WriteValue_m9CCF2DE479431156D6E3A005C0C999B7CFC36351,
	TraceJsonWriter_WriteValue_m0C93FB423C4A5528DBF64E0432B8AB495D1532EE,
	TraceJsonWriter_WriteValue_m55941F317A98B0ACA6E39D62637FDAB461AC016F,
	TraceJsonWriter_WriteComment_mFF534D10C6A1730E8D9FD2676448B1BC03BFA7B4,
	TraceJsonWriter_WriteStartArray_m96B34217073043222888E650456126A2BB20DFCA,
	TraceJsonWriter_WriteEndArray_m2FCE8425119B65792C43CD37986DEBD60B66D7B1,
	TraceJsonWriter_WriteStartConstructor_m710B24C732EA4757E3C2BCD55CFD20A16A0DC77E,
	TraceJsonWriter_WriteEndConstructor_m159BABC2EBABE6D4DC1683FA18A4A9D2D2928AD5,
	TraceJsonWriter_WritePropertyName_mFAE312535CAF8837C14668C6931964B02841057D,
	TraceJsonWriter_WritePropertyName_m4766486E8ED9537C22434AEFF189A60D10E57F16,
	TraceJsonWriter_WriteStartObject_mE97856C576ADF3CF5EF40A8F9A0B0C8C4FA43B21,
	TraceJsonWriter_WriteEndObject_mC91B5E978AF28AB8F4FD9E26F23599C63FF69019,
	TraceJsonWriter_WriteRawValue_m119558ED3D6970C3F01196F27C25F3793CF51CC0,
	TraceJsonWriter_WriteRaw_m7B890E7615BEF9D440DF1ED14E46819E389EC8BB,
	TraceJsonWriter_Close_m747E78F25F9F7DAAEF501885A4BFBEE995C7A932,
	JArray_get_ChildrenTokens_mA1DEC06AEA095FC146C961608990188CB3EB33AE,
	JArray_get_Type_mA33AF3317CDA9DD046B7ABB8106A8D98C876C952,
	JArray__ctor_mBEEE73B6A0AC3D3FCA28E3B6B896453A4308920B,
	JArray__ctor_m70FA99428E8D9BC5320C5FC8ED3C0D82B9D5959B,
	JArray__ctor_mDC6B2631D37AF77521585303A4FEA6CC7260C8EB,
	JArray_CloneToken_mBD9DA3EE5F599169F7386BA54DA2E07893B385D3,
	JArray_Load_m0CD5F55F08391C0649C95B7C42EE24040300524E,
	JArray_WriteTo_mBEE123D499E5AB74F56EF55B3F1AA9A72B37FD7C,
	JArray_get_Item_m88D1B9E2605132D4701B9C88EA6C4A3A3C340A89,
	JArray_set_Item_mA02ED51F0DDD32C5468560C513868BFE31B1ADE0,
	JArray_IndexOfItem_m88E68E9F11F4E80E2AD5B6B9B8D2C2029C28671E,
	JArray_IndexOf_m7C1584468BD22DF5E36B174DC2D7EEDE1AED69F7,
	JArray_Insert_m7C82622CF143C377A7F1ACFF2AEAC9C24A262771,
	JArray_RemoveAt_mCDEC5B28E071AF9AF6D641448153A59AA88E6642,
	JArray_GetEnumerator_mA52FC13BD541B196C067B10F75EE218611100582,
	JArray_Add_m7867C056B0DFEC1765A3BEA7250BF2FD701EFD55,
	JArray_Clear_m23A03D37333E6429D2F914F2870B56C20C564F51,
	JArray_Contains_m4A9353DD9C3A8BE8881E22F80197AEF67FD6D25F,
	JArray_CopyTo_m38EBA986D37D4B6C71F0B17DDDD919C693ABF0FC,
	JArray_get_IsReadOnly_m68282E1344A5F4A0893F2B3FED274169EECD6E9B,
	JArray_Remove_m01697C0702ADE16AEFE131806240E4F37EC47317,
	JConstructor_get_ChildrenTokens_m47FDDE57DA0B3ACAD231077B8BB1BE194E11782D,
	JConstructor_IndexOfItem_m7FD5AF6CCEBA0E2EDB7244D7417433F849B7433F,
	JConstructor_get_Name_m857ED71B4469321BE4DCFCC3042E2D761B45811D,
	JConstructor_get_Type_m1B04D60143327306F8CAAC0E5FC2F18E4E679BDD,
	JConstructor__ctor_m632EE02A4470EB8A076F62B69AA3A40A41DF0E9F,
	JConstructor__ctor_m15C66D05844E5C74B5EA60A28C2968F9EC5E8665,
	JConstructor_CloneToken_mA219AB5B8ECB9CA0F5A0AF731166EF6065B0C04F,
	JConstructor_WriteTo_mB85D481DAFA14C0C5C4B172454CB21B0F9E715B1,
	JConstructor_Load_m0B45878BFC83EEFA9DC69BD92D7B40797AA4BCEB,
	NULL,
	JContainer__ctor_m0CE22FBD81CF2B70971DBCE4580E1F95310DD5EA,
	JContainer__ctor_m0902CE016622EC4AB064E0FC17D8E735AC819439,
	JContainer_CheckReentrancy_m97FFF3B6866D08E86DA13149D3216226946A7308,
	JContainer_OnListChanged_m43B8FA4297F83C78AF7A397DE6FFD74EFF3DCE9B,
	JContainer_OnCollectionChanged_mDBC9B5E334A2B0E055D06CB590055EB35E687BA2,
	JContainer_get_HasValues_m7239BD55357E09ADD283116D7CE365FA200A86CC,
	JContainer_get_First_m36D3338561F4D3D0E499648AE5E7FD9523BED6E5,
	JContainer_get_Last_m0BB4890F883946993F80DEE7F7E7B0FE8A0951B0,
	JContainer_Children_m4D17F48661D82595E64CAFB5B806EF9E000DA4FD,
	JContainer_IsMultiContent_m542E4D877230B7E397956A48D020FDBF9D408196,
	JContainer_EnsureParentToken_m0200054AC023E2480589356054EBCC9A22D2B431,
	NULL,
	JContainer_InsertItem_mE2FA60A9793A5707E3C8CC4F8E566FE452FFC5B7,
	JContainer_RemoveItemAt_m9695A4D6FBDA2153904170723B8C5B498DD07B52,
	JContainer_RemoveItem_mC31969653040EA3EB1B6A241469B56C4AE84D72C,
	JContainer_GetItem_m67248AB90E307B18D7B543A531930F6C58800DF7,
	JContainer_SetItem_m21806067E0FC24F0A744D3AC9EA1C95A0DD8A8DF,
	JContainer_ClearItems_m60C68B6A3BE52D5B7A3E2A9246D070495D47F0A8,
	JContainer_ReplaceItem_mF49492E29F1DB2D449B8E1AE138D025D1BD2260C,
	JContainer_ContainsItem_mD1CF3EFCD467598A0651A3F964DF1D0544C1EC22,
	JContainer_CopyItemsTo_mF586911EB401EB7EFE65DCDC3F69F34BA9162FC6,
	JContainer_IsTokenUnchanged_mA560534C1BEFB1FD3A74392B7FCC5B1D87151166,
	JContainer_ValidateToken_m3844C3AC8376D0EE7A80AF730954128054E8054B,
	JContainer_Add_mB92DF57916610ACC06BFDA6BFBE6193B9A992120,
	JContainer_AddAndSkipParentCheck_m33E204AA42AC6FC8474C2FB6D4668147A97C22AA,
	JContainer_AddInternal_m3E51EBAF6DE85EEB5ED40A8F9B61732F3ED5B1E3,
	JContainer_CreateFromContent_m7AFCFE69DC6E3334F1EE5E9331E0F03BCAC8881B,
	JContainer_RemoveAll_m8B271F31E5A44D1BF2C63322DADA656108B8619B,
	JContainer_ReadTokenFrom_mA9CC3A9D820D91A0D2C5EB59FBCF61D11051B72A,
	JContainer_ReadContentFrom_mE6AECC267DB4D3D355D7FEB2D9AA5329A7227181,
	JContainer_ReadProperty_m439B5DC6F3092419CBC32A44AEAF4D0194B15CD8,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_IndexOf_mEB0089063FAE4C81F47D25C0B1B176AA6A9031B6,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_Insert_m7312E90BD69267AE047819700DB955784664BBE1,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_RemoveAt_m28849367105D64650CD353E873C5834331016B6D,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m1B5B440A3A5F8264A8E6AC629AA0FA338BC0AD22,
	JContainer_System_Collections_Generic_IListU3CNewtonsoft_Json_Linq_JTokenU3E_set_Item_m01E4E879831C8F4D1CD2FCF08B16AB4E49A64300,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Add_m19BA521ADED9B5360DF7D554CE0EB55D75F9D741,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Clear_m4F940A1BC8E661763B627D3B26FDCDD5F1DE3630,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Contains_m900941D95D8B8BDF1C497C75947983CA3C73BEA7,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_CopyTo_mFCE0DD0B2C56D0FCCAB3D4038D4F27B4A850DCBA,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_get_IsReadOnly_m6607FC73F08DFD533548A2173B28EC7CF125FC63,
	JContainer_System_Collections_Generic_ICollectionU3CNewtonsoft_Json_Linq_JTokenU3E_Remove_m7181FC9E077FCEB86358DAD26E7D7B825E36F737,
	JContainer_EnsureValue_mE2A06BCF0E4A2ABCA5FA7E649AFAB3A8163C0383,
	JContainer_System_Collections_IList_Add_m3F811C8EA6AAE7F25433FB0E793F0E2C0ACC2323,
	JContainer_System_Collections_IList_Clear_m671790420FB27D56C2A704E6E0B7BEAC274C2E3B,
	JContainer_System_Collections_IList_Contains_mFE433F2BF5269EFCE2E2EAA5AB1FC7906E47B749,
	JContainer_System_Collections_IList_IndexOf_m2DBECA578BFC86E12228DA99AC06FCB777FA8195,
	JContainer_System_Collections_IList_Insert_mF51E09186B620E85EF25DA2452E8226FF947820F,
	JContainer_System_Collections_IList_get_IsFixedSize_m59C782EF836BA52F736FB1281305AED90F8A4C93,
	JContainer_System_Collections_IList_get_IsReadOnly_m47F2BB96C1E31D45252910739F0935498BD01051,
	JContainer_System_Collections_IList_Remove_mF6A47B5BD7F1A91AE538020DAFDA6DC649920BE4,
	JContainer_System_Collections_IList_RemoveAt_m836BEB978B58245BD1C9088EF6C0954A3D87F99C,
	JContainer_System_Collections_IList_get_Item_mAADD5FB1DBF4364008F9D7DB6460ED8945467581,
	JContainer_System_Collections_IList_set_Item_m369E563AB174C21AC6E1B545AC6CC17C27A276F2,
	JContainer_System_Collections_ICollection_CopyTo_mEA6BEAB8787C814724F977F0F3A9EE5C2F5DF401,
	JContainer_get_Count_m1C92C14323C92FF6E2AF2B8FB75B6726F67A85D6,
	JContainer_System_Collections_ICollection_get_SyncRoot_m7D1C5F603B08522258C4AB46C538415199730FDE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JObject_get_ChildrenTokens_m266186AFB3740EF26E4C04E68C75DBB3F5363B10,
	JObject__ctor_m4ABF1922B4DECD1DA5CD12F6DC6DBDE833D17AE2,
	JObject__ctor_m1C4DD67B48A523218211C0FB069FE2229041F4CD,
	JObject_IndexOfItem_m24476C5FB8F85466112322D19619734CE020EE7C,
	JObject_InsertItem_m6A199B37C8324D0F3EE15C8BBD3D0DE976407E48,
	JObject_ValidateToken_mCB3DDA384F247047F319CCF532E56FB87225A16D,
	JObject_InternalPropertyChanged_m900E8C192C5C9A6523B8BD410049193C6AE77A61,
	JObject_InternalPropertyChanging_m5ABEEC0280B5A9234AD2C1395762CBD2885330A9,
	JObject_CloneToken_m1333CE1C1B472305128B7439A56B9C0A24079FD8,
	JObject_get_Type_mFDE5DED25A4C9FAE856EC444EEB2A49040868AE0,
	JObject_Properties_m19E8627EFB77572308C4366A5232B2A80F65C986,
	JObject_Property_m6A261A9EEE36DE60ABEFD6E65F547B6BE8994AF3,
	JObject_get_Item_mED171D088059A4217F5BAFE7BE0F97A7CF8299CB,
	JObject_set_Item_m3A5EDDA204F4B59D144BA775168C381DD48E94EA,
	JObject_Load_mC67D559144E3D219A1ECB2880EBBCD84118C065C,
	JObject_Parse_m6EDB41DC32CFAD94BBE7DE418C7E25D02E7FB295,
	JObject_Parse_m4219C3D3A06F8084023D4FDC5CF7CB131E93C5B3,
	JObject_WriteTo_m7AFA1E1DDEB761DEAC4CED6308FEC1C98ADD14C7,
	JObject_Add_m41C7BBD1E5187334172C1414ECA9C4094E42BE09,
	JObject_ContainsKey_m45AA5603727F2847CA6D0038A3BE7B0D99DC88F5,
	JObject_Remove_mEB10588027E8797E1CE1A5D8170823E591E81780,
	JObject_TryGetValue_m5C001A77D8CAC5221A6DEC0148744977C8DD254A,
	JObject_System_Collections_Generic_IDictionaryU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m11A812701B46674AF8B3C00142062EE847FCA8B0,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_mBE02B2795E6BA6ECB7A7DC71EDDB1C44A8687DDB,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m7F527CD11BA1BB912D6A7504F06A334EB8ED7868,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_m50E7E5F1B63B315D19BD60A2816A30F0D0E60448,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_mF201C14DA4208AABE4816512C0FB92726E556B56,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_m9078C5ECFC01EFDF7EA0E049EF839DC96C4E13FD,
	JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_m325901E55F60FF4E3742814AD8B117E20F8D9CDD,
	JObject_GetEnumerator_m2DE962AE58C6A538CD4F0692785B71C1EB7BA944,
	JObject_OnPropertyChanged_m1208B15BCD54990795050C8BFB2694716F945EFC,
	JObject_OnPropertyChanging_m3019CA4A7A5C8C5968F1DC56496F4BEEF750BA2D,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m996584BF140C41116295AF81F8801A7018251796,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m7DE7DA3A5378699AE704E75C784C77FF2C4F18B5,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m280DA57DB5D19912C1266C150099E12B16C97818,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m5411AF02E7B42DD2A936A3B8800F9E3F7709F8D8,
	JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_m7BD8F359AC1C2ADAACEB21233163A4271622243D,
	JObject_GetMetaObject_m5EF46BD88344C931557E16098710C711F2C4AB52,
	JObjectDynamicProxy_GetDynamicMemberNames_mDB00214DE4AD76808BBFDB21898CC3BFE98F2994,
	JObjectDynamicProxy__ctor_m741866551A0F8E9BEB331494268858FDCC185E99,
	U3CU3Ec__cctor_m54A99D4EF53878B83E83E1337C924267C3E5CD39,
	U3CU3Ec__ctor_m28C53F0D4555A7A0D8D078D91F734D98AD78665E,
	U3CU3Ec_U3CGetDynamicMemberNamesU3Eb__2_0_m70A91B5055F6F7B7C4E9DFA7060D9E275EAC8B2B,
	U3CGetEnumeratorU3Ed__63__ctor_m16597C9D193B2D7D6450BE5CEE4BF2D2D5A51F54,
	U3CGetEnumeratorU3Ed__63_System_IDisposable_Dispose_mE185B4358E2352B249C0EA846C9725EA3DAD82D7,
	U3CGetEnumeratorU3Ed__63_MoveNext_mD34979B2F123B5B4447C22849A233B784769342F,
	U3CGetEnumeratorU3Ed__63_U3CU3Em__Finally1_m6EF73DFD3776ACDF31C920388830F948FE402FA6,
	U3CGetEnumeratorU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_Current_mBC037F4FAE0A23758A485E73BF980EEC0FB34413,
	U3CGetEnumeratorU3Ed__63_System_Collections_IEnumerator_Reset_mCE6A51B34B51DA53D97CD220660A6F0E580AE771,
	U3CGetEnumeratorU3Ed__63_System_Collections_IEnumerator_get_Current_m259473560C00DF3477CBF1AD41EEE65D9403D4F5,
	JProperty_get_ChildrenTokens_m09D87A90E4B77D3F37B09A4ECB6E73D37B704E52,
	JProperty_get_Name_mACECEEA2BC913EC667635FA76AD99BE53E7C89D7,
	JProperty_get_Value_m42F9FEB52576F3E8A6057DA6E0047D5873F344F2,
	JProperty_set_Value_mD1149BD291F227DBBF2B2C1CA03FB0B439499553,
	JProperty__ctor_mD75DC416674FB0F2BD2809D24B46A5CEE2173867,
	JProperty_GetItem_m77448BBBC7DBB0EB9556116220BAB1E72F260F2A,
	JProperty_SetItem_m781458D8F5B9B733A0CE1A00A786BB15A2EB88D6,
	JProperty_RemoveItem_m4880CB8BBF529B2D4288326EE6A71FE61C2FF526,
	JProperty_RemoveItemAt_mC13E7C708B0C32AA0B22343A3B8CCFC9FB47B7D9,
	JProperty_IndexOfItem_mFF1C2D3AD09ADC3B75CB794022A705CB127868CD,
	JProperty_InsertItem_mC38DE258B92B87EC85624A05EE8677BE0B362199,
	JProperty_ContainsItem_m1310C45BAA88436FEC056BFE511D3599D0578C39,
	JProperty_ClearItems_mC9ECF0A83296CBEF945E11E91B8C521606D0E79B,
	JProperty_CloneToken_m943C0D6E83D1EA96A41A82C323DA9D1C5974A56E,
	JProperty_get_Type_m956D15F01DF6162C8E27BB57A381816A1CBC5971,
	JProperty__ctor_m5123C40415A79F32C520448416192E80E77E7D35,
	JProperty__ctor_m6F9A418D5CB916B387F62E91962571ABF148DB55,
	JProperty_WriteTo_mF13D103C02D0E60DE09797572A4D22E3A1521B84,
	JProperty_Load_m1F3D60673ED18836DCA4124160F8989ABE3F30E0,
	JPropertyList_GetEnumerator_m570686A9CB55EEE12CE3921F71DA4A5DAFF95654,
	JPropertyList_System_Collections_IEnumerable_GetEnumerator_m905FBBF8E644EC168CB0620DE1F821A2E54EF518,
	JPropertyList_Add_mE6880E09207E96382298CB55DE5C4B24F4501BA7,
	JPropertyList_Clear_m53EB4CBDE6FC6BB8698C2D3052FA506836A5C2B4,
	JPropertyList_Contains_mFB58417165DEBD95DCF9E0F316083674C72AF1A1,
	JPropertyList_CopyTo_m757B8A194601BB3BBE26F5CA38ECA43ACCEEE662,
	JPropertyList_Remove_m781F2E250E34956BF834B5FF9013C4765A27884E,
	JPropertyList_get_Count_m6905FC2CA1E71EDB0A159F379FB246F5765FB0CF,
	JPropertyList_get_IsReadOnly_mFDCA32EAB8BC6F1C1803D7443720E10B91CE9149,
	JPropertyList_IndexOf_m63D22F6AADE0900D7E7E1B1FE6D2EBADD4BFB022,
	JPropertyList_Insert_mF51B149BD53A382D451D4BF5EFB8D26DE83385BC,
	JPropertyList_RemoveAt_m594E723AB7AB66AF7F1DFB789D88579B007DC720,
	JPropertyList_get_Item_m9CCDE18F8356D5FD9D42F5BEE477AFCF4A69AB2F,
	JPropertyList_set_Item_m4D409C29FB271164BD89832B26E35BE5C869A0DB,
	JPropertyList__ctor_m28C210433E968471FCD8BAC19B6C16DAB92D6119,
	U3CGetEnumeratorU3Ed__1__ctor_m51B36DE071D6342B19695BB9803650F004966078,
	U3CGetEnumeratorU3Ed__1_System_IDisposable_Dispose_m5E84033520F6941F60D25510D2F116B00C26ACFB,
	U3CGetEnumeratorU3Ed__1_MoveNext_mA7CDA7CF81C222940582519BCEAF72560A4A540F,
	U3CGetEnumeratorU3Ed__1_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m6062F82EA393CEA956EADC0CF10D3AFFD4EFCB9C,
	U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_Reset_mC28B482E8836E43B79799E11F2EF4F6C5998B634,
	U3CGetEnumeratorU3Ed__1_System_Collections_IEnumerator_get_Current_m48F23FBCB7E67DA86C62BF936AC1038357AC218A,
	JPropertyDescriptor__ctor_m200C5553371BF93A3044D03EDA7AA405A666286A,
	JPropertyDescriptor_GetValue_mFA7D468738FFE36F17D678728BE954003A01F1C8,
	JPropertyDescriptor_SetValue_mC97EDC59D9C4EBD2A2D3A5769F4B848BE7169D41,
	JPropertyDescriptor_ShouldSerializeValue_m14F03DE65139E02F6353DFF24830A1F5CF0BAF22,
	JPropertyDescriptor_get_ComponentType_mEDC30D60BADC626D0C2A5F8FB617D344A49E931B,
	JPropertyDescriptor_get_IsReadOnly_m9B815BCFB243D6D4244975339EF72BF331869354,
	JPropertyDescriptor_get_PropertyType_m8E39AE752F53555EE5608DDF104E7AEA64E3DA71,
	JPropertyDescriptor_get_NameHashCode_mFD890FE80DF1DC46050503D9394EDCB85FE93853,
	JPropertyKeyedCollection__ctor_mD6CED5A5546C1C35ECF614D75D3597F6F94649B0,
	JPropertyKeyedCollection_AddKey_m4633882595DF3769B82CB864943699C5F266894C,
	JPropertyKeyedCollection_ClearItems_mCCFBFEB59BC203D7646A62DC1D49AD90313C3A5B,
	JPropertyKeyedCollection_Contains_mCE8E9846CC2365EE3760982EC977EC8FB319FCFC,
	JPropertyKeyedCollection_EnsureDictionary_m91B20B6E667B97A8098071FF4EB9A4A2D2ED4C59,
	JPropertyKeyedCollection_GetKeyForItem_mD178C29110AB250E23DF06F22F52D4E4891AA00D,
	JPropertyKeyedCollection_InsertItem_m317E2662E890BC477A95B036CA357790EE7DD509,
	JPropertyKeyedCollection_RemoveItem_m7E4949267296B4C0C65F539C7CCCAC3FE961F5FA,
	JPropertyKeyedCollection_RemoveKey_mE81CDA6E4B9808D6C5A15BA71D71296EC30F5D54,
	JPropertyKeyedCollection_SetItem_m20E39BE28BEC60EA0FE562B2C99D0E3EF30381D0,
	JPropertyKeyedCollection_TryGetValue_m9AA5D00E2C98623E762FFC82621D59A50C317E1A,
	JPropertyKeyedCollection_IndexOfReference_m0B011F09010122922F2A39D1C7DDD5C5005E75A6,
	JPropertyKeyedCollection__cctor_m91AF5A1BBB872088C6DE704392BD466AF09CBE8C,
	JRaw__ctor_mE1E29D14D63E627A4ADDC148BF47576AD6E0700C,
	JRaw__ctor_m2D4BD8F289A8380150173AC6CF605E3040BC7B4A,
	JRaw_Create_mA3636B03EBB9ED5CB860AABABC79C3EDAC916391,
	JRaw_CloneToken_m1DCB2AD0423B5439D3892916590D82020E005D05,
	JsonLoadSettings_get_CommentHandling_mA34D24EF3ACA88F8E09FFDA51481CADBA08C9C19,
	JsonLoadSettings_get_LineInfoHandling_m2AD316E98970C5A7D36D677FBE4682EF32DB0C50,
	JsonLoadSettings_get_DuplicatePropertyNameHandling_m938013262617A16532A60AEA3AFBA7BA22CD4ADC,
	JToken_get_Parent_m063166BECB700D927D31F6A78F60F8F8B7F90D41,
	JToken_set_Parent_m7BE843F90087F2D4216BAFB488101E22C8602775,
	JToken_get_Root_m324F6E6BB5AC879F57661922B5FEA26FB539EDB0,
	NULL,
	NULL,
	NULL,
	JToken_get_Next_m9D46389AC24A9F0C3D3B71366BEABA1C19AA9C2D,
	JToken_set_Next_mD79E073265DF73A5B6438B28E7362F8FE696817F,
	JToken_get_Previous_m67169B2FAE14AA628820DDFE5424C68555356A35,
	JToken_set_Previous_mA91AB456AC1F169F2A1D7F13842583DE4386280F,
	JToken_get_Path_mD320131161693E7BEFCC4BF7E305CF3FC351EF45,
	JToken__ctor_m9403EA4776AE598E3FF68C450C1917D3A48CE470,
	JToken_get_First_m14E10D2D1FA762A3FED76D4E83F7F453043026ED,
	JToken_get_Last_m11E9366AC0C3B9D3D861767F97B21C43AEA91B90,
	JToken_Children_m0906256C207729201CAF8E84A7B211CA30CB948B,
	JToken_Remove_mD5F9FBFAD849743FDC4F03514B707BF68ECABEED,
	JToken_Replace_m67B8D9199B5F3D976FD1A7C833C4309FB0AFA408,
	NULL,
	JToken_ToString_m65C4DC88F1476746199E28B80D9B2CC720383AB5,
	JToken_ToString_m9A15F7553907AB7AF47A02D43B3F426A168162B2,
	JToken_EnsureValue_m40DF80803817C5D89FBCC9C59B3A97A0F9972890,
	JToken_GetType_m43DA114C98D6E97AAAE4567C7EDEA8CADC838221,
	JToken_ValidateToken_m7CC9ED2EA3268AC496616301024BF998E67F1344,
	JToken_op_Explicit_m5CB54CD44D187CAC6796C6B46094F5DA1BC7AF80,
	JToken_op_Explicit_m366F02B0C9B052E9DAA91EDB904B6F9118AFFFE4,
	JToken_op_Explicit_m8109DD61856AD55360B6F2F6CABAB5DC11EACD11,
	JToken_op_Explicit_m930D65D1BDEA9B6BF39D29F0E85B43A34BD1A3B1,
	JToken_op_Explicit_mECA899B405FAAB4ABBE9955C61C114A39CB8CCA8,
	JToken_op_Explicit_m20997FE5D10871851493D4E81B6DF2E69DCE2761,
	JToken_op_Explicit_m87A5B257B1E369111A04D700476A80BD01961C56,
	JToken_op_Explicit_m0F727614AF70EEDF2C237EA58B999DA29C4A8D55,
	JToken_op_Explicit_mD02895C8CEA53E5155644CEAFCE1B809736A7863,
	JToken_op_Explicit_m46AC6C59EA789613D16025132C0437910A8B1D16,
	JToken_op_Explicit_m63641BCC1997D440114F23ECADD75482854E3439,
	JToken_op_Explicit_mBBC97DEF7FD1AA961E72ABCC0619F6C6E8C30A95,
	JToken_op_Explicit_mA30A1EAF36028DE4E91935D9ECC7215A5954B5D7,
	JToken_op_Explicit_m7C0FB26C58AF3A305E455116822B829255FD88EB,
	JToken_op_Explicit_mAC324B94621EBF11E88D304029B4F80F9ABB3D80,
	JToken_op_Explicit_m017E4DD2890CB408CB373BF50B801C0A1746D128,
	JToken_op_Explicit_mD72FF1B8EA3D2852FB159B18A06109433DA14ADD,
	JToken_op_Explicit_mA902AA9D06E7AE2067E663CAA9CD3BC4CB9A6910,
	JToken_op_Explicit_m694195C301821E99A5B019D7B7924A557BCA287A,
	JToken_op_Explicit_m01615A483BAB4B8A8A155308FA3D1BC33F1072A7,
	JToken_op_Explicit_m2277C9449E8DEDE78F5A5D06AB5A681C2F71D968,
	JToken_op_Explicit_mEAE5AA13B34BE32813B1B9AAEFD6D27CA3EF6FC9,
	JToken_op_Explicit_mCB30AD2C793CEB5DBB143248E9FB65B2B1BCCEA8,
	JToken_op_Explicit_mC94107C01B3BFFB52E4B0F241D2831CA5C1FE989,
	JToken_op_Explicit_mEA356E24459E72FD243B0F220E585415B238ACA2,
	JToken_op_Explicit_m7C3D358D3A768DD6FE7E840C2BF7303CF4E7170F,
	JToken_op_Explicit_mAB8B832E5DE1BC6A2076AEF3752C09D42F7E4871,
	JToken_op_Explicit_mAA344ADAE9F1A5D5569F92349C5014716B8B2DBC,
	JToken_op_Explicit_mDAD31C3695EC07CBBD5E983F41FB450D6E7ED2FA,
	JToken_op_Explicit_mB863015F27A343485C8199920D53B0DF4997615C,
	JToken_op_Explicit_mEF1BF2DCAE7984256D03A933626ABF996632A24F,
	JToken_op_Explicit_mFB3DF0C0FAC44053CB7D9EF8AD2A2502E69E956E,
	JToken_op_Explicit_m89B1E77388D7E74FD707E37B919A51D0EA37A344,
	JToken_op_Explicit_m9BD04FE9C15DB138F15D514A5524FB2119D6862A,
	JToken_op_Explicit_m9BCB18850374D7EE2C6999BD71FE4FA98A860838,
	JToken_op_Explicit_m97404E29535DCF8C507010342CB88468ACC19ADA,
	JToken_ToBigInteger_mA20791DC013DB05777DA7A81B94D649839852FBC,
	JToken_ToBigIntegerNullable_m02E6BF8C2B6A7980C44FE05A9354AC29AA11C047,
	JToken_System_Collections_IEnumerable_GetEnumerator_m2BC4C41C026BAFE012BD7D5206D3446FCF9312A5,
	JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_mB6FD49260ACE2507F2263B3EE43ACDB4F5CD09AA,
	JToken_CreateReader_mF5E2D2604521FA794AD783710F3C7FE48ADF5E24,
	JToken_ToObject_mD0513627B512DAE9B80A3E75F654AA29E8DF9A34,
	JToken_ToObject_m0559DA0AB5DD6E95C434B70CFCC99383AD174344,
	JToken_ReadFrom_m2A0A1874821741EAC0C587028591A8657290ADDC,
	JToken_ReadFrom_mB8D8177BFE01FC0692070171D79BC9389F8BABD4,
	JToken_SetLineInfo_mBA60584A609A591D4FF6F0A56631EE447F7DB6C0,
	JToken_SetLineInfo_m81271DAA0C8D4E0C6C3FD0EFBF00BCB10EA2BE16,
	JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mA238457EAAF4973A3DFA8FCDF8998C4BCD1618AF,
	JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m04944C7797AC592DD4D579848AA955951EC43C4B,
	JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m7DFD8260A8102C3C51D795E263B9E9F2D4B1D9A3,
	JToken_GetMetaObject_m3E1F0054A769B223FA721A48D7A4C0F2CC713683,
	JToken_System_Dynamic_IDynamicMetaObjectProvider_GetMetaObject_m11CF558F49FD96B6479650225EA134BCC44039AB,
	JToken_System_ICloneable_Clone_m9F5B8EED736A0AA6AB066268C8E249BC4FB5A877,
	JToken_DeepClone_m2EAC0C9B86BE2DD02DAB06B05677E99C2C72EECA,
	JToken_AddAnnotation_m6D27445CACC7FFA8223956207997DBCDC8D13321,
	NULL,
	JToken__cctor_m7C212A58CB853C24BF505C888BEF330267E233C5,
	LineInfoAnnotation__ctor_m145919F96CF15FAE402041EC35504B71A72CAD81,
	JTokenReader_get_CurrentToken_m7A9ECD6E7ED51C9C138755D48C132EA7D923AEBA,
	JTokenReader__ctor_mC79ED5F26C7EB260A81A781EFE747FE0429A66BC,
	JTokenReader_Read_m13B825CA699C4269C24E128E70867207C3A1A471,
	JTokenReader_ReadOver_m1FAC2900B7357F6B76A22A45B97826176F8FCB20,
	JTokenReader_ReadToEnd_m680FD1C7618F0F2242B0B62D5CE5B7FDD438AE54,
	JTokenReader_GetEndToken_mCBAAA95284EAD4529680668714CAC5B6D2F18DC7,
	JTokenReader_ReadInto_mF45C69C1C9B3F157CB6B8267007C4C479FEC28E4,
	JTokenReader_SetEnd_m1DD15D1D2F364BE07648B908F384C6B8716CB9CC,
	JTokenReader_SetToken_m286990EC9E759D4F040C486FB1FF8D5931DD1FF4,
	JTokenReader_SafeToString_mEF4CB8D66CCA84EC6FF30B299DA51499F15296B9,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_mF4DDA65277F7DC3D365EFC6108B06124DE53056B,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m7771C266081518DBD3EE5091E19211E8C861DD93,
	JTokenReader_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m9B4511CBE4BED33FD35C129BE3A73EF0A8F894E9,
	JTokenReader_get_Path_m603F641AF06490A280060CB10BCD442CB39C4E6B,
	JTokenWriter_get_Token_m0595D2D38CD2872664C84C1B30FDC8C98CD406DF,
	JTokenWriter__ctor_m045384937D8A1410E45006A1805C51E8C90FA5BA,
	JTokenWriter_Close_mA2DE46AB7DDA9689956A9949BC9D1A73192B9A55,
	JTokenWriter_WriteStartObject_m844D589B418130ACB879FB57C8F795F1BFC14513,
	JTokenWriter_AddParent_mE14EF88D72C8E89698ACB462C135AE18CE4B49F3,
	JTokenWriter_RemoveParent_mE07C63592E7D0A84FFADF78D9E57362D27519B39,
	JTokenWriter_WriteStartArray_mFFDBE68C3F1CCAABA9DD9603B87A05CB94AD40E5,
	JTokenWriter_WriteStartConstructor_mA8738134CD98535FD180CF628A4D354ECDE8614A,
	JTokenWriter_WriteEnd_mD38539ABE993D2F3D6DEE0BB831C81F8901CE121,
	JTokenWriter_WritePropertyName_mF11C52C6BF1BD6BFE058882DDDC86A0C2872B752,
	JTokenWriter_AddValue_m8BCA668F22D86503D27D98FAEF74C1373B8C526D,
	JTokenWriter_AddValue_mFF1435E6EF1D3278E213D601D6AE00F54D6C1BBF,
	JTokenWriter_WriteValue_mE842786471A1CBC363F58E995432FF95A0E6F92A,
	JTokenWriter_WriteNull_m6248EE783616A554E0EB79424E7D795CF0B65E06,
	JTokenWriter_WriteUndefined_m10A46B5E18D8AD5D49409FF2B7EABFF9C8562327,
	JTokenWriter_WriteRaw_m3EDA379DC0C7E7D088521C6790E0FFA89362AFEF,
	JTokenWriter_WriteComment_m5541A1C1F6001C10F2DE23965993601F72E2E309,
	JTokenWriter_WriteValue_m0F6AEC99F10EA6CDEA0D34635E9BBC568FF288F3,
	JTokenWriter_WriteValue_m818C778F26340E550814507E36207F65DDDA1CC9,
	JTokenWriter_WriteValue_mDC294D4566485D708F26B2AEB02F281A33308CA3,
	JTokenWriter_WriteValue_mC558E2C21A46ACF57A9EA3D393CD0F9E376D0633,
	JTokenWriter_WriteValue_mB65910925D4C6608998A1D728C19C326AF103C9C,
	JTokenWriter_WriteValue_mFA4F52EEEBD7523076F3FE7A1FC74F8CDDEF6A81,
	JTokenWriter_WriteValue_mE058D1D3D39CE72E334F7872FF880B06773821C4,
	JTokenWriter_WriteValue_m262F998BA6D8CD4F8472EBC6006AE5B2F5FAD153,
	JTokenWriter_WriteValue_m4325EA3D208AB4D15F57AA4C1397FBAB5D22AB4A,
	JTokenWriter_WriteValue_mA3023B66ADFD019AB022493529C44642CCC37517,
	JTokenWriter_WriteValue_mDB2D6C1C875F27A855B14AE4C9136D4BE7E67277,
	JTokenWriter_WriteValue_m36F301C60E6F7600F5524CB2A2124E98F4CF4456,
	JTokenWriter_WriteValue_m6ADB79611A15AB8ECF865AFF5F7CAF8CA77649A0,
	JTokenWriter_WriteValue_mCB5364B7DED79821AD8F5AB1D073DDEFCFA8098C,
	JTokenWriter_WriteValue_m7159AF0551A51895651946FDA37A677E88E61F28,
	JTokenWriter_WriteValue_mD4C8B642325090765E261FC36176D7BBD30F2EEF,
	JTokenWriter_WriteValue_m5ECAA5DDB7967032C1ABB8EF3A11AD56E42E34A6,
	JTokenWriter_WriteValue_m21AC556B849CEE72A6F3D0B62FD8879221858A57,
	JTokenWriter_WriteValue_m4A8A33E56DD2D2A33C22C5F5808722A90CF33EBE,
	JTokenWriter_WriteValue_mB366A01142928039DE56A73A758F4BB4C79C70E4,
	JTokenWriter_WriteToken_mFB95B232D2B03CA7B750C82223D067285A5172D0,
	JValue__ctor_m8007B375443A338098CC8864A47D0B6B1CF75F0D,
	JValue__ctor_m6EAFA8228885869EA5B375A1932316BC7FF4DD4E,
	JValue__ctor_m5A507F5548216C06142A53841A9823944817E295,
	JValue_get_HasValues_m5D34C1DB0F79DCBC720E2699A71CCC02C2745687,
	JValue_CompareBigInteger_mD808C213154C477942A94C0DF3A146B7AE79D1D9,
	JValue_Compare_m1237219402299717A0A4A72C017D9ECEB1F8CFE8,
	JValue_CompareFloat_m8E99A0B189B65DDB2DB29A0EF80C746D97116925,
	JValue_CloneToken_m2278A8BF115960CE65B4DE48713B8B14F8C2DA68,
	JValue_CreateComment_m4E96B9D2D9E9BB03B7F1F1A1F2A09CE15850E04E,
	JValue_CreateNull_m7946A1C3AAC6B695B010883B4DA5A20252BE90D2,
	JValue_CreateUndefined_mC97FAFB0E146D858B42CF708C19EE6E74C55D0A9,
	JValue_GetValueType_m113E2D72866AA4B3E4A2D6996EE0ABABD2C828BE,
	JValue_GetStringValueType_mF60A5C1B5C6EF5493D98D0DDC2F69521588E3025,
	JValue_get_Type_mABF6475D3326E13103F2E18DC3D886BE04297795,
	JValue_get_Value_mCBF2B0ABEE8EAB2605E7E035114875A1FDF14A2C,
	JValue_WriteTo_m2B6FDACB3570278849002223FCF6C377C4C7C1D6,
	JValue_ValuesEquals_m9A992ED7E337788D1952B91F3390D206F3F8AC1A,
	JValue_Equals_mCE10C9076BA72D10635EDF5071FAF5D7FD47C7E1,
	JValue_Equals_m83626462DDEC2166569220F304A2A752F15CE942,
	JValue_GetHashCode_m1CAC729387B43AEDAA2D0907D0503ED6F981F2BA,
	JValue_ToString_m3896D883303028D76C5841C368B2D0365B3B95EC,
	JValue_ToString_m7CFC016396D6601002AFE7FB7C16CF82A546A3A5,
	JValue_ToString_mF7AD4373341218788213830E69F740D0679F3969,
	JValue_GetMetaObject_m40EAB6EF513B04E25695FBB8DFA8FA5493AC66AA,
	JValue_System_IComparable_CompareTo_m9DBAEB3154195AC829C354449BDC5036D2161509,
	JValue_CompareTo_m976EA46743620E9FBA671C79D9148A55AFC5B17C,
	JValue_System_IConvertible_GetTypeCode_m887796C18EBCF0428E82C15E9E9D4EE7E738FC07,
	JValue_System_IConvertible_ToBoolean_m1B389C8084B13D948582F7DCCB756E0A9EB04A12,
	JValue_System_IConvertible_ToChar_m88C9AA3F35775A0BCAAEEBA617899166C6E22BE1,
	JValue_System_IConvertible_ToSByte_m459F451FD1A1B217A5FB6CACABD38F52176F3932,
	JValue_System_IConvertible_ToByte_mE312081EBD0C6BC6F3EEE28D5FB1C0465066A4D2,
	JValue_System_IConvertible_ToInt16_m57E642BBFE858D9A676E68FB7105171263F018D0,
	JValue_System_IConvertible_ToUInt16_m0EAEA48C942D2886D5A901BAE4A2CD7059792090,
	JValue_System_IConvertible_ToInt32_mD62FE733ABD4504D633F479AEE3BEFA6A12608B2,
	JValue_System_IConvertible_ToUInt32_mE6209F3A7D0BEAAAC37D92A3652D6F5CC4974997,
	JValue_System_IConvertible_ToInt64_mBCD9FC80F23BE666D5648D20225DC3268547FFA2,
	JValue_System_IConvertible_ToUInt64_mE48A6AA9C3BD5FE13F22C427539425AF10A6A211,
	JValue_System_IConvertible_ToSingle_mD320FDFEBFBCD1B6122173BA43CBF557094A7857,
	JValue_System_IConvertible_ToDouble_mFCE10AF219790E54315A6734C7EB267193D10BB6,
	JValue_System_IConvertible_ToDecimal_mF214F40F9909FC476F53336B000E9AFA1A1C6AA0,
	JValue_System_IConvertible_ToDateTime_m37A998314995CFD14BD6C26111F85FCAEB1F3FE7,
	JValue_System_IConvertible_ToType_m9B10B361696D641B4321B2CFBBFC6D089676021E,
	JValueDynamicProxy__ctor_m238373055C135315DA9F1BD625A68C2FF3C87A7C,
	BinaryConverter_WriteJson_m96D53B374E75504FE23BB90B198C2384357C70E0,
	BinaryConverter_GetByteArray_m40B78C5DC2EF4A6776B452B89EBDA82CB413DD43,
	BinaryConverter_EnsureReflectionObject_mD7E2802E108C70073A6ED836E7F8C67A23CD7520,
	BinaryConverter_ReadJson_m52DC3119DF13E8970288536E4687955CC82E695C,
	BinaryConverter_ReadByteArray_mC6AC543CA4348F7B319D5142FC58E6C2A4811D21,
	BinaryConverter_CanConvert_mE4044330144C80BA45CBFA3F9180E0F31F679C97,
	BinaryConverter__ctor_m3A7F6F6DE299919309EC69F91E2726471CD8D388,
	BsonObjectIdConverter_WriteJson_m88E58F971EACB92570EDDB004C4C516D8F3FFEA9,
	BsonObjectIdConverter_ReadJson_m5B77B0AD68060923A99A30770B698673A473B956,
	BsonObjectIdConverter_CanConvert_m1046BCCF1CA60D89029F9AEFAEAA9BD318729135,
	BsonObjectIdConverter__ctor_m3E94339F463D9EF1D0B9B0F2BAC1E8DEA14C84AC,
	DataSetConverter_WriteJson_m07EDC41B3391B7C969D776870A3EEC8473A7A72E,
	DataSetConverter_ReadJson_m10CD5F381F0460B81DC532BCBC05A98802B02569,
	DataSetConverter_CanConvert_m3F55E4B7948CDB10F845772B18CCF7A82EA3306B,
	DataSetConverter__ctor_m3110CEB8B8FDA3DF51D31E84BAB23C5DB6784E57,
	DataTableConverter_WriteJson_m64B51B7CE0956A7BADD5B9FBB1A0049C0337E04F,
	DataTableConverter_ReadJson_mDC27E7FD57CFEB5FF6D741D53251D53D16D88CAA,
	DataTableConverter_CreateRow_m0E0331C44F177771FEB4036D02116C69A7E6A458,
	DataTableConverter_GetColumnDataType_mB11B7E47B858A84A93A67A3EC7053676D313161A,
	DataTableConverter_CanConvert_m5F49A7656F227654BF2B45FE3753AB5DA6AC7352,
	DataTableConverter__ctor_m7BBD33D0536A71B4EEC258000FF42C069D0D6127,
	DiscriminatedUnionConverter_CreateUnionTypeLookup_m01FE5176A4BAFFFD53B21C8EDA7377F61DB2EF5E,
	DiscriminatedUnionConverter_CreateUnion_m4A4FB0851D1E2E85CDE08495A926055E98B0C574,
	DiscriminatedUnionConverter_WriteJson_mFB032F950A72B80E2F48F5DCF7B41C737370B57A,
	DiscriminatedUnionConverter_ReadJson_m82C3F7B22CF5F829F775FA284332FB96D10AA48B,
	DiscriminatedUnionConverter_CanConvert_m0C65C9145C42CA4F73AF20AB6FD1463FC3D95069,
	DiscriminatedUnionConverter__ctor_m04CA04F2F08B535E99AD01D274E720264B88F700,
	DiscriminatedUnionConverter__cctor_m6636CE0D46B4C0E2F446C86A2FA3F0E698DA50B0,
	Union__ctor_mB5C7E2E3BC1FB55007A4921DB8158820E3695486,
	UnionCase__ctor_m58A7EACC5C89C2C568A5F82B740B78F87C0299BF,
	U3CU3Ec__DisplayClass8_0__ctor_m355E0384D5F20FDFD86C4C8B0C61F0CBCCD72591,
	U3CU3Ec__DisplayClass8_0_U3CWriteJsonU3Eb__0_mF336A4857B509750C6687767019BD5BD991668C0,
	U3CU3Ec__DisplayClass9_0__ctor_mCED67CFDFE4DD725377FFB7A9AB3987CF91D852E,
	U3CU3Ec__DisplayClass9_0_U3CReadJsonU3Eb__0_mC95972DA98D50540757E5A11E9678E6CA6E3B383,
	EntityKeyMemberConverter_WriteJson_mB15BD4945525B4F185DD1D899E7446FCDCA73278,
	EntityKeyMemberConverter_ReadAndAssertProperty_mC0F3CED5D745008AE2836FBA5EE9F78F0F2E9BD5,
	EntityKeyMemberConverter_ReadJson_m37B626017F05971AC4F3D9DED61764FFE8A06C64,
	EntityKeyMemberConverter_EnsureReflectionObject_m77BCE52B285964B4C08226A11029789CB6C4FD48,
	EntityKeyMemberConverter_CanConvert_m6A19BCF9D35326A1030F049BAF5CA75AFF255110,
	EntityKeyMemberConverter__ctor_mAE71F62BD056308A33ECD1DA938EE8FF4812963F,
	ExpandoObjectConverter_WriteJson_m1FC6FE51B4FF58F116CF03689AA6D0389D0F8A6F,
	ExpandoObjectConverter_ReadJson_m2D65EFF3EDE736B0C286D1B1B7AAC246AC090E1D,
	ExpandoObjectConverter_ReadValue_m5226D79D83DB64E215C5187926B4D7A4E2DABBE8,
	ExpandoObjectConverter_ReadList_m08B57D48D091B3F985D56E2A2D74FDF0F23B1297,
	ExpandoObjectConverter_ReadObject_m410F1681824221BB27A3904A052B2BF5F066B8D5,
	ExpandoObjectConverter_CanConvert_m268D59B965D2800581FE08AE4EDA82A9BFDDEC50,
	ExpandoObjectConverter_get_CanWrite_mEF962CA95A656ACF3E6142D5D42C6F6B7B32E2CB,
	ExpandoObjectConverter__ctor_m3783B25FDF7D61DD5F827BDD9359353A2E4F86D6,
	KeyValuePairConverter_InitializeReflectionObject_m1F651E5178CD6B92D15A9058E66E1664F1444D1D,
	KeyValuePairConverter_WriteJson_m7D7E1F090459FB36DAD6173D31FE63020EF5B6FC,
	KeyValuePairConverter_ReadJson_m3CEA2CF8478C5FEF57A10216D9474670164D2C49,
	KeyValuePairConverter_CanConvert_mA7E89DDA708CC2C5E37191FCB9A85A1365F7D129,
	KeyValuePairConverter__ctor_mB1119AC0D1B112476028E000D91E6BBBB5E3E860,
	KeyValuePairConverter__cctor_m5CF0FDF70C7296D1BCB2BF9CD8B50FA1D04CAE7B,
	RegexConverter_WriteJson_m27BE000DE0DCD75479A19B2A7550E84ADCF27290,
	RegexConverter_HasFlag_m811EB1C06C8B6C133996C5D3BFDD02292DC9B235,
	RegexConverter_WriteBson_m55C1DA46CFA8A8394F13E4D6FC1F89A42F4A5106,
	RegexConverter_WriteJson_m5FE8015641B8AE79CF053644C9F1497FAC07E94D,
	RegexConverter_ReadJson_mEE820DC0D2C1F217548394569DE78C0ECBC158EE,
	RegexConverter_ReadRegexString_mC68A0CB377B3E9DB895E67650B08E3126DF6D2C4,
	RegexConverter_ReadRegexObject_m45715078A419E0BA314E27EB782A4FB4E87C74C2,
	RegexConverter_CanConvert_mE03B0EEC81088385512C579A2BBAFEEBE7743C2C,
	RegexConverter_IsRegex_m3624371E914A9467399D2C660FCA83C39E1AAE7D,
	RegexConverter__ctor_m294DEABFC3BCFBFE0A6AE0E85B670F76863AFCD2,
	StringEnumConverter_get_NamingStrategy_m6118F88740E250FD7E1FA57988F6937893FEACB6,
	StringEnumConverter_get_AllowIntegerValues_mC7F41257DF961EDCDBF27AD1FC863633BC6BC478,
	StringEnumConverter__ctor_mD8BA43C37DE510A97BD0FFFD90E0513BA914D770,
	StringEnumConverter_WriteJson_m8133B9C1DEFB29D70717657BA12D20ED5B27A4EA,
	StringEnumConverter_ReadJson_m4468C614021E94C19F8952FBFC75DEA3E3D72A67,
	StringEnumConverter_CanConvert_m80381ED5F69192580DBB56DE38561902F12447C1,
	XmlDocumentWrapper__ctor_mCBFF932E5F24B1632175A9A1FE8D147632DED570,
	XmlDocumentWrapper_CreateComment_m00DB173A6E090B2CC1D473CE2E49687D08BACBAC,
	XmlDocumentWrapper_CreateTextNode_m3C4DDB13BA7DE00ED2029CEE011A948E71BF8A3E,
	XmlDocumentWrapper_CreateCDataSection_mCFF2248CE45B03416F10C02E2B34EC4FF39A7A30,
	XmlDocumentWrapper_CreateWhitespace_mC43CE41B76799228987EFC3C50CCF0F13A65EF1D,
	XmlDocumentWrapper_CreateSignificantWhitespace_m31F61254E88E3B1FCDE8D602032A05B3EBBC96F5,
	XmlDocumentWrapper_CreateXmlDeclaration_mC254925B75A1665F94FA3912AEEC366C0C07B785,
	XmlDocumentWrapper_CreateXmlDocumentType_m22A29E0B51AA2D6B6F4A6240C4F7775C24445629,
	XmlDocumentWrapper_CreateProcessingInstruction_m6631D66D87B464AD48676D00FB4BF0CF49EB6FBF,
	XmlDocumentWrapper_CreateElement_mFE5B1066954439DAA29D833502A69BA4348555D6,
	XmlDocumentWrapper_CreateElement_m62FF4ADD074839190AA6CEE46CC39FB6E5E0C579,
	XmlDocumentWrapper_CreateAttribute_mE4DC3536A32A874F8C3E8B2BEDC2F18CA12E2290,
	XmlDocumentWrapper_CreateAttribute_mB43374B0E6FDA9710D1DCF4BBF643AAA10407C46,
	XmlDocumentWrapper_get_DocumentElement_m6DFC5B13D5C7D95201FB1A5E846BC6E91168FE70,
	XmlElementWrapper__ctor_mC938DA690DCA1FBAB95E675762B1BC88E922A898,
	XmlElementWrapper_SetAttributeNode_m88EDD195DDC9F54ECA37246F2DCF95C67B63F87E,
	XmlElementWrapper_GetPrefixOfNamespace_m2BDC572D276BA29DB810CB957C2FCAA381BCFA97,
	XmlElementWrapper_get_IsEmpty_m3BB47AE5D0F8D5B69659202C32D58131B5EC0961,
	XmlDeclarationWrapper__ctor_m0DE6BCCC51085CAFBEA0C83E086DA7A7F9B1A5F5,
	XmlDeclarationWrapper_get_Version_m66F69FAFAE7391561F102F829DA006CE658AF4B4,
	XmlDeclarationWrapper_get_Encoding_m196878FE3723E7FCDBD60AA5227C5C659792C405,
	XmlDeclarationWrapper_get_Standalone_m4C990F42B856D1887B22CA448A150A9A30706E9C,
	XmlDocumentTypeWrapper__ctor_m79C3E16EB8A73722D3ECE0073A01329EB8173CE3,
	XmlDocumentTypeWrapper_get_Name_m49EA5B9702C3BA1A2C779F12BD6528C758C603DD,
	XmlDocumentTypeWrapper_get_System_mCF3F92E9C8781B3F27B350E6FC37961F7F60D51C,
	XmlDocumentTypeWrapper_get_Public_mB0A321FA9F9D771CA2AC6648BA03CA4CA96A9CCC,
	XmlDocumentTypeWrapper_get_InternalSubset_mD2334CC4FADF87EA863653E69062F3304A0CEF7C,
	XmlDocumentTypeWrapper_get_LocalName_m04AD4A05C90918316C68AF717D3FCBB4D84ADAF6,
	XmlNodeWrapper__ctor_m56955E78398F89D5C7FA1C452DA3703F9AC64783,
	XmlNodeWrapper_get_WrappedNode_mEA9197640A19B4BBC990E45CBB7FE9EBEB140452,
	XmlNodeWrapper_get_NodeType_m2F8DBF13A830888104A4F52039C2722B9062F904,
	XmlNodeWrapper_get_LocalName_m0CB9646D438188E9E5EF06B3902DB8938E1377BE,
	XmlNodeWrapper_get_ChildNodes_mE5A10C3BA574ED7BB55FE2D5B5E0729C93981312,
	XmlNodeWrapper_WrapNode_mF48B11E19BF30A5C3475DA1C7DE6C96AA8DCA21D,
	XmlNodeWrapper_get_Attributes_m97A169FD65946347942F7618A1B3F53F28030DE4,
	XmlNodeWrapper_get_HasAttributes_m561B4D0F55A889813329C42B8439ED9033BFB242,
	XmlNodeWrapper_get_ParentNode_m99E27E9F05C18CA5812289698BC89DB4476070AA,
	XmlNodeWrapper_get_Value_mF2428B1A129335F77FA818245CC94F4FCF1EF035,
	XmlNodeWrapper_set_Value_m74D3957FB01C792FD6B02C9ADB2E2B7629A85667,
	XmlNodeWrapper_AppendChild_m527F431399ACBB712FA45F4F38D601D2F5604F8F,
	XmlNodeWrapper_get_NamespaceUri_m3F0B866BB9E36A278C950F4BBE795044C3796BAA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XDeclarationWrapper_get_Declaration_m6B71AC50AA501740922CBB3A07AC6B57BADFF3E6,
	XDeclarationWrapper__ctor_m92E156FBCF6479C70F243E596B4E32FF4F04D2C2,
	XDeclarationWrapper_get_NodeType_m77B6B12924DB0CE56FDB5122BDD8A7E24F21ED3F,
	XDeclarationWrapper_get_Version_mE63D0CE8066442111E8057EC3F6B9D1C716980EC,
	XDeclarationWrapper_get_Encoding_mA50767247C5D51B57FAB9DC85A8F6B1F784BAF12,
	XDeclarationWrapper_get_Standalone_m7F5FBA9A1F7AEFF28E67062815A091C5C02EEF99,
	XDocumentTypeWrapper__ctor_m26D46820553E4DC831D2CD2E3C3403B8957DEB8D,
	XDocumentTypeWrapper_get_Name_m5568DB1E64425419B89053AC0A297CF57C84E050,
	XDocumentTypeWrapper_get_System_m3FDD057A8114D05BE4CA0B569A184D25E8C1EB00,
	XDocumentTypeWrapper_get_Public_m8568FEE7E79D219E245E66E00C06B4B2B6A69AD9,
	XDocumentTypeWrapper_get_InternalSubset_mC900B6161241721AD3B9AB5D9AE9A8F9D12BACDD,
	XDocumentTypeWrapper_get_LocalName_m91C0BAFA290E7B2A258A240A38D02BF0FC6C0915,
	XDocumentWrapper_get_Document_mF3AA048CF86753719932305066EBE3D71CA5D263,
	XDocumentWrapper__ctor_m6E109AAC5C9CE19EB647D45E0442AC0E28DBFDA4,
	XDocumentWrapper_get_ChildNodes_m66DD385359184B30F5402547BA0B3B79AA1359B2,
	XDocumentWrapper_get_HasChildNodes_m0BCB6C97C6A80035F1F658993F7DE128DC791652,
	XDocumentWrapper_CreateComment_mBE2C9FA27EB94BC06DE1C07DF75BF038049D565E,
	XDocumentWrapper_CreateTextNode_m2022E291A8081A037E25B6DC3BDD20C0DD9F4844,
	XDocumentWrapper_CreateCDataSection_mEBDD3E9B0E029DF106BF83C264BA93449FBAE995,
	XDocumentWrapper_CreateWhitespace_m6FEF8BA816EA0EBC1D13168AB59EA0845E244A0A,
	XDocumentWrapper_CreateSignificantWhitespace_m89CFF8550D28094FBC9F32ECEE6CE22340F20312,
	XDocumentWrapper_CreateXmlDeclaration_m583FD9D82D593A0924CCC99C06C054427DAB2E16,
	XDocumentWrapper_CreateXmlDocumentType_m35E9388BBD3E485B7D7AD4DC1F3DBE776E353302,
	XDocumentWrapper_CreateProcessingInstruction_m403FC41B218B872A2A39D6391C50A2C91F9C06D7,
	XDocumentWrapper_CreateElement_mDB804223D4E9C024B78771C1ED702DF71B6281FE,
	XDocumentWrapper_CreateElement_mAFDDEE9303F1EF746A9AECE4B17F95667BAF7E9C,
	XDocumentWrapper_CreateAttribute_m470B1D78632FFE1C2CCDF2660F76CD7DD47E8327,
	XDocumentWrapper_CreateAttribute_mE71E4CFE30BAA95448BA9A7B4AFC446E1D9B47FF,
	XDocumentWrapper_get_DocumentElement_m1257C318BA84CAE2C9DC76197DEB299F1B70D04B,
	XDocumentWrapper_AppendChild_m4D3ADB19375159452D02049D5BF4D9639E969962,
	XTextWrapper_get_Text_m9E0C69C1AFB2C4D9949D33D681565192C6BE2E36,
	XTextWrapper__ctor_mD7EF2546868D97F3A931CBC294CBA6DF1B8E2238,
	XTextWrapper_get_Value_m231219A7A8BC8D30BCE90E02FAEAF79CC9E74D63,
	XTextWrapper_get_ParentNode_m09DA419C7181FC1E4CDEC5CD17AC6E0466A25EC3,
	XCommentWrapper_get_Text_m713EB632E04CB6A89E3E28D71D50548AD9929877,
	XCommentWrapper__ctor_m45F66B921B66D080093A633B6251E79D161C4637,
	XCommentWrapper_get_Value_mCCABEBCC945CDA5FC4698AB05CC370549CBE7E57,
	XCommentWrapper_get_ParentNode_mF2C8AC269A82C7ADFDD0C3A72E6A0A4254F22221,
	XProcessingInstructionWrapper_get_ProcessingInstruction_mA2C743A6FC6BA660AE553D05E6364F6CD63AE29B,
	XProcessingInstructionWrapper__ctor_m76A4A0100E0428790AFC5D23A1736CF7BC1EC38C,
	XProcessingInstructionWrapper_get_LocalName_m85DF0F00AED7099621CE59B80CE1BB697841DC76,
	XProcessingInstructionWrapper_get_Value_mA78A2672B3E518B9AB902379C48CA8E0D96D51F0,
	XContainerWrapper_get_Container_m9848D428F04F497D8F95758A77D252F10653B1B1,
	XContainerWrapper__ctor_m5FA678CF4F3D1EA0B812106AA8DA1F19B38CD0FB,
	XContainerWrapper_get_ChildNodes_mCBEE68DF2B446879DF0C8A0E40AE1220266BD3EA,
	XContainerWrapper_get_HasChildNodes_m9959C7C278BC5F9BA65D57994346139E53B6D966,
	XContainerWrapper_get_ParentNode_mECAEFE1CE9D421432EB614AB7B656173029EE624,
	XContainerWrapper_WrapNode_m9368FD5E4AEA85FFFAB8250000B9203450735C4E,
	XContainerWrapper_AppendChild_mB1AE254C5ACF5001FD013EC06670932D77C765A9,
	XObjectWrapper__ctor_mD99AA05A08081D3E7EBE932E30865B556C325AF7,
	XObjectWrapper_get_WrappedNode_m25279F86A39559906A8F7E282916953CB3E289B5,
	XObjectWrapper_get_NodeType_m20D34D93E4DF66392E4238C95DAD668AC1EC2851,
	XObjectWrapper_get_LocalName_mE12B4A83E99F2B9E05EBB8C7A2138389EAF7F573,
	XObjectWrapper_get_ChildNodes_m05E6FBC7605057788F6722E4A3A876D988993851,
	XObjectWrapper_get_Attributes_mB983361EC4F96CDE9464098F20EEAE792ABE5B62,
	XObjectWrapper_get_ParentNode_m34E1467029A86E6724158CEBF19CB8746312C80D,
	XObjectWrapper_get_Value_m72EBE002953C13B37195FD4D75F59344160BD0E1,
	XObjectWrapper_AppendChild_m16B735232AFF1AA6F5FB5656D3C3D38B8C2E94F3,
	XObjectWrapper_get_NamespaceUri_m6FA98699A5D2B0121B77CB00170A629A60B82796,
	XAttributeWrapper_get_Attribute_m179DD5BFD35ED40CF3CA56678F087EEAB0DCF1BA,
	XAttributeWrapper__ctor_mE75DDF01CF6098B48F689B166E84C10B9709A2A5,
	XAttributeWrapper_get_Value_mC08C7A8B6EF1E875A699EBDAB69BC6F334B88720,
	XAttributeWrapper_get_LocalName_mDC625E5526AD0F76A7B90572AF173AE46184B22E,
	XAttributeWrapper_get_NamespaceUri_m594B0AF334B7BCC38858668178CB12162DFB5F8D,
	XAttributeWrapper_get_ParentNode_m09DE8AD07A4824ED0DE8D56E4FFADA8413D8CB8B,
	XElementWrapper_get_Element_m50FD04D503C7376106D75712C2C4935FF18B17FF,
	XElementWrapper__ctor_m937078F4FCE5230E84B8249A7C1ACDD8696E33E1,
	XElementWrapper_SetAttributeNode_mBB7A2289297DCCBD84EEBFE1A8C2673EDAB5E8E5,
	XElementWrapper_get_Attributes_m5C95307FB17FC43EB43E0B87C25B1944F31AEE53,
	XElementWrapper_HasImplicitNamespaceAttribute_mBEC9B9459FE78FE3FF189ADE2A16B315EC728A93,
	XElementWrapper_AppendChild_mE5AC5DA4C44C25197B621DC2C76C9B191BDE3460,
	XElementWrapper_get_Value_m9CEC8A9B253BE025CA0B32E7904B179FFE70B9ED,
	XElementWrapper_get_LocalName_m0E8C6261303727A53C3D7467ADBC13756ED2F63F,
	XElementWrapper_get_NamespaceUri_m7D3D958E51C5E32D3EF8B95E62700FBCF4011E41,
	XElementWrapper_GetPrefixOfNamespace_m2DD2A82049E3B736B94DE0772A684D7673307BC1,
	XElementWrapper_get_IsEmpty_m4047C0B631DA5F3C724D6C2DD107204B2CB24B1A,
	XmlNodeConverter_get_DeserializeRootElementName_m63B070694296090C009BB14D77461E84B8EC82B2,
	XmlNodeConverter_get_WriteArrayAttribute_m3A6016D30C98406D4C30D6DCCC53C9B039CF5DBA,
	XmlNodeConverter_get_OmitRootObject_m0931E1452D03534EA2CCB661765C4038D098A3EE,
	XmlNodeConverter_get_EncodeSpecialCharacters_mAD2AE708CFAED00AA0D7F2C2E1FC6CCB50B9FA79,
	XmlNodeConverter_WriteJson_m0946D3AF2831BD718BF92D1FA87933C1AC09D846,
	XmlNodeConverter_WrapXml_m2A524C3E5650139862E747A1FB51444DC0691D98,
	XmlNodeConverter_PushParentNamespaces_mCEAC6475A4AB3538685682D81B65F9477FBDF9DB,
	XmlNodeConverter_ResolveFullName_m259FDBB8D1479FFE9378DCCE6537EE2E604F37FE,
	XmlNodeConverter_GetPropertyName_m43BFF71FC7CAB9C34E413496D2BFCC06ABD3161F,
	XmlNodeConverter_IsArray_m4C93CF7D8927D8072535D76A2850660CC26F93B8,
	XmlNodeConverter_SerializeGroupedNodes_m1D2519ACD384ED228B1D39ACFF2A027B3FDF29A6,
	XmlNodeConverter_WriteGroupedNodes_m325D7AA9F38ACB652ED5055F05BCA0252BB2658C,
	XmlNodeConverter_WriteGroupedNodes_mD5EC88187CDE6764B27BDA3245FB880E80CAEDC9,
	XmlNodeConverter_SerializeNode_m6F75281C7AC1CAE074AD9A5BAA0D5796B5680329,
	XmlNodeConverter_AllSameName_mF3F3CF125BBF97209F28859C8EE130591E3C3A5A,
	XmlNodeConverter_ReadJson_mB9F9B1F1EA83A04D8FA715A6929CB75729071888,
	XmlNodeConverter_DeserializeValue_mD0C2CD230EC4C24F3AB8BF7000C292DE97F7B3EA,
	XmlNodeConverter_ReadElement_m52CD62E8182CA2BE93162AC15FA241783CF03BD2,
	XmlNodeConverter_CreateElement_m6D148559F8C186402B04056DC7901C3CD6BE20CC,
	XmlNodeConverter_AddAttribute_mAF18A72C9662976EED20145A3CC342BE678ED5CD,
	XmlNodeConverter_ConvertTokenToXmlValue_mB31E099F93AF2CB0F558B2626B370C7729C2A24F,
	XmlNodeConverter_ReadArrayElements_mE71ABD583A9D9F47490FE5A4DC24E8527FE4EAAD,
	XmlNodeConverter_AddJsonArrayAttribute_mEBA3F44A2C09045A787751B637EBF15674777626,
	XmlNodeConverter_ShouldReadInto_m5BBBB7B44C7F6565BCE9A854EB60AFBEA919EDA5,
	XmlNodeConverter_ReadAttributeElements_m95E2F93312CD8472E8F3F88B47228B4288902002,
	XmlNodeConverter_CreateInstruction_mC317B6EF18F64415F38475CC2558A81B7C8EBF75,
	XmlNodeConverter_CreateDocumentType_mA1F554B82D72F49DD49223BAAB9D577675362FA3,
	XmlNodeConverter_CreateElement_m575B9ACF2636770F7D929D8AAFA94F80D8C29ADE,
	XmlNodeConverter_DeserializeNode_m1F303681104FF534B2D33CAEF062DFFC124317B7,
	XmlNodeConverter_IsNamespaceAttribute_m3F83132E95A31835A3CB1546480EF6E780CBD2C6,
	XmlNodeConverter_ValueAttributes_m296E7D53C4450778E79DF956F184A1172120B3A8,
	XmlNodeConverter_CanConvert_m5CF84182FFB1194C0D83705F6F605533349A8B48,
	XmlNodeConverter_IsXObject_m0FEE07E42C97ED3DA2EE42C4D4E910F432CE7E0A,
	XmlNodeConverter_IsXmlNode_m596F5B1168356D5BC69075E91CDC16929EDEB36F,
	XmlNodeConverter__ctor_mDADEAA8CDDB5EA5CF654F3768B09001BFCBFBFE2,
	XmlNodeConverter__cctor_mA139CD3692E1F9C6C60BD681967209F4BECD56B8,
	BsonObjectId_get_Value_mB2BB4F836020BAEC4CF65BBACAD07926A053946B,
	BsonObjectId__ctor_m3D9121A541D6276C14091527447EFFFCFA343D85,
	NULL,
	BsonToken_set_Parent_mD4F3136F36730C20DA04344824246DE7AD54E902,
	BsonToken__ctor_mF5AEF2BE7F64AD78CF515419271E6855EA6A84E2,
	BsonObject_Add_mCB9F536AD0F214CCB2A305D5F8F0611D7D24228E,
	BsonObject_get_Type_m7FF064E7D3528E7CEACA5D4C4CF85D8C47CDE1A6,
	BsonArray_Add_mA93BAD1A4704568872344EE8786000186271FEE0,
	BsonArray_get_Type_mC29ECECD16CE0301F4B17C16A93ED223D15919FD,
	BsonValue__ctor_m64B7C66C8F1A61BF525807FB9FFD9563E6E384F0,
	BsonValue_get_Type_m6DDEA9908FF18D962F3C1CFC495D5CACCCD71AED,
	BsonString__ctor_mF94A16E54777C681EA000D01EA35A5F7EE07B212,
	BsonRegex_set_Pattern_m21DD5B5D3BBFE8B16E935CA9DEFC52DFDBB05DBF,
	BsonRegex_set_Options_m39332EE8F58BB3E3748772227D76C0FDF7A49504,
	BsonRegex__ctor_mC868086A71FF8F56B9F526E485A919FB81383865,
	BsonRegex_get_Type_m82E5509487A124B7839E98563CA5025FF958D276,
	BsonProperty_set_Name_m864ECCC2EAAC4A23CDA4256056147D5E460A482E,
	BsonProperty_set_Value_m919CA8D72C3328C9AB044DBCF989C404462B4D06,
	BsonProperty__ctor_m65E91143CF6A7EBC37D3BC7C7D51CDC13677E8B0,
	BsonWriter_AddValue_m278DB787BF4E90BDF46069697DA9F9F7BE417CC2,
	BsonWriter_AddToken_m7E1E7D939DB6128455533817DAE3B5FD960C7609,
	BsonWriter_WriteObjectId_mACB166CF782B2E0A55D2BDE73F56F72D570254CB,
	BsonWriter_WriteRegex_m6353BD2E68125E81E64F1723476DC319A3871682,
};
static const int32_t s_InvokerIndices[2213] = 
{
	23,
	23,
	31,
	26,
	31,
	23,
	31,
	23,
	23,
	3,
	23,
	54,
	28,
	58,
	23,
	1115,
	107,
	-1,
	-1,
	89,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	26,
	4,
	208,
	209,
	2239,
	2240,
	2241,
	2242,
	0,
	350,
	0,
	369,
	2243,
	0,
	119,
	1,
	2,
	243,
	1842,
	2,
	0,
	2,
	3,
	197,
	125,
	9,
	89,
	89,
	23,
	14,
	14,
	23,
	23,
	26,
	27,
	111,
	89,
	89,
	54,
	23,
	10,
	32,
	10,
	845,
	46,
	2244,
	2,
	3,
	14,
	14,
	14,
	14,
	14,
	10,
	89,
	31,
	89,
	31,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	26,
	778,
	2245,
	10,
	14,
	14,
	10,
	14,
	14,
	26,
	2246,
	23,
	32,
	10,
	10,
	89,
	778,
	2247,
	14,
	14,
	14,
	9,
	2248,
	2249,
	1036,
	2250,
	2251,
	2252,
	1038,
	2253,
	2254,
	2255,
	23,
	14,
	23,
	23,
	32,
	62,
	1091,
	31,
	23,
	32,
	23,
	23,
	37,
	23,
	31,
	23,
	23,
	460,
	483,
	89,
	89,
	10,
	23,
	111,
	1132,
	1,
	2,
	359,
	23,
	26,
	27,
	111,
	1132,
	1,
	2,
	359,
	26,
	26,
	26,
	26,
	14,
	26,
	26,
	32,
	32,
	32,
	32,
	32,
	10,
	32,
	32,
	32,
	32,
	10,
	32,
	14,
	14,
	26,
	2256,
	339,
	10,
	32,
	89,
	31,
	89,
	23,
	4,
	0,
	4,
	0,
	137,
	-1,
	105,
	105,
	2257,
	2258,
	197,
	27,
	28,
	197,
	14,
	28,
	1,
	26,
	10,
	10,
	10,
	10,
	10,
	14,
	26,
	10,
	10,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	2256,
	3,
	23,
	26,
	14,
	26,
	23,
	31,
	32,
	504,
	504,
	196,
	23,
	217,
	825,
	2259,
	855,
	855,
	89,
	778,
	1038,
	14,
	14,
	34,
	34,
	672,
	1036,
	23,
	34,
	34,
	2254,
	2251,
	2248,
	23,
	23,
	89,
	23,
	614,
	38,
	2260,
	2261,
	240,
	23,
	660,
	23,
	216,
	89,
	89,
	233,
	23,
	660,
	89,
	23,
	31,
	23,
	23,
	32,
	2262,
	105,
	1,
	31,
	1394,
	9,
	2263,
	9,
	233,
	23,
	23,
	23,
	23,
	34,
	93,
	34,
	93,
	34,
	93,
	23,
	89,
	10,
	10,
	14,
	240,
	26,
	23,
	23,
	23,
	23,
	26,
	32,
	26,
	460,
	23,
	23,
	23,
	10,
	23,
	23,
	130,
	26,
	23,
	23,
	26,
	26,
	460,
	32,
	32,
	200,
	200,
	336,
	2264,
	337,
	2265,
	31,
	614,
	614,
	614,
	31,
	31,
	955,
	324,
	306,
	26,
	1225,
	328,
	1679,
	956,
	26,
	26,
	23,
	200,
	1104,
	2266,
	32,
	133,
	116,
	4,
	3,
	89,
	31,
	89,
	31,
	10,
	10,
	14,
	14,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	23,
	32,
	10,
	10,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	26,
	460,
	23,
	26,
	460,
	62,
	1526,
	2267,
	112,
	112,
	26,
	32,
	23,
	37,
	32,
	37,
	23,
	32,
	23,
	23,
	23,
	32,
	23,
	23,
	26,
	26,
	26,
	32,
	32,
	200,
	200,
	336,
	337,
	31,
	614,
	614,
	614,
	31,
	31,
	955,
	324,
	1225,
	1679,
	956,
	2245,
	2268,
	2269,
	2270,
	2264,
	2265,
	1037,
	2271,
	2272,
	2273,
	2274,
	2275,
	2276,
	1039,
	2277,
	2278,
	2279,
	26,
	26,
	26,
	26,
	23,
	31,
	500,
	466,
	1,
	62,
	32,
	26,
	23,
	129,
	32,
	23,
	23,
	111,
	197,
	2,
	2,
	26,
	35,
	35,
	2280,
	2281,
	23,
	35,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	114,
	1,
	2,
	-1,
	-1,
	-1,
	-1,
	119,
	186,
	1,
	561,
	-1,
	-1,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	10,
	130,
	94,
	646,
	0,
	114,
	531,
	2282,
	2283,
	2284,
	2285,
	2286,
	2,
	2,
	218,
	114,
	592,
	592,
	592,
	218,
	1443,
	3,
	23,
	28,
	3,
	1315,
	30,
	30,
	846,
	30,
	1190,
	1190,
	1229,
	3,
	2287,
	21,
	310,
	1126,
	1126,
	2288,
	2288,
	263,
	309,
	2289,
	2290,
	2291,
	2292,
	2293,
	2294,
	2285,
	2295,
	2289,
	2293,
	2290,
	2285,
	2289,
	2296,
	2297,
	2298,
	1050,
	2299,
	2300,
	2301,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	0,
	3,
	0,
	3,
	1,
	1,
	26,
	105,
	26,
	205,
	28,
	23,
	3,
	1376,
	2302,
	2285,
	367,
	0,
	151,
	576,
	2303,
	2304,
	3,
	3,
	23,
	28,
	4,
	28,
	-1,
	125,
	-1,
	-1,
	-1,
	-1,
	-1,
	143,
	23,
	3,
	147,
	-1,
	-1,
	-1,
	27,
	28,
	26,
	4,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	154,
	561,
	1,
	28,
	105,
	-1,
	3,
	23,
	105,
	-1,
	-1,
	-1,
	-1,
	-1,
	587,
	2305,
	3,
	197,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	9,
	3,
	23,
	9,
	9,
	23,
	9,
	119,
	137,
	243,
	3,
	2306,
	135,
	2307,
	2308,
	180,
	366,
	366,
	46,
	46,
	46,
	4,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	3,
	23,
	28,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	255,
	213,
	2309,
	-1,
	-1,
	-1,
	-1,
	135,
	2,
	0,
	138,
	0,
	0,
	466,
	94,
	-1,
	-1,
	-1,
	28,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	14,
	14,
	26,
	105,
	197,
	28,
	1,
	2,
	23,
	28,
	23,
	28,
	23,
	27,
	3,
	114,
	0,
	114,
	0,
	243,
	1,
	0,
	629,
	0,
	153,
	114,
	114,
	0,
	0,
	135,
	135,
	390,
	135,
	390,
	390,
	0,
	466,
	0,
	114,
	114,
	629,
	2310,
	119,
	134,
	-1,
	-1,
	-1,
	206,
	2311,
	2312,
	1,
	119,
	195,
	119,
	168,
	195,
	361,
	0,
	3,
	23,
	9,
	28,
	28,
	9,
	23,
	9,
	23,
	9,
	9,
	23,
	9,
	23,
	9,
	10,
	32,
	89,
	130,
	26,
	937,
	36,
	26,
	130,
	14,
	190,
	14,
	421,
	14,
	10,
	10,
	35,
	14,
	2313,
	2314,
	2314,
	114,
	2,
	359,
	516,
	1511,
	2,
	43,
	2315,
	-1,
	0,
	237,
	48,
	48,
	2316,
	2316,
	193,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	94,
	114,
	114,
	114,
	114,
	0,
	0,
	114,
	114,
	114,
	114,
	114,
	114,
	114,
	368,
	207,
	135,
	137,
	-1,
	-1,
	23,
	28,
	4,
	10,
	32,
	89,
	89,
	89,
	31,
	89,
	89,
	14,
	23,
	28,
	114,
	28,
	9,
	28,
	2317,
	28,
	137,
	28,
	105,
	28,
	105,
	205,
	105,
	28,
	28,
	26,
	27,
	51,
	114,
	114,
	114,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	28,
	114,
	114,
	114,
	2318,
	0,
	58,
	14,
	28,
	58,
	2319,
	28,
	147,
	28,
	28,
	28,
	28,
	3,
	3,
	23,
	9,
	9,
	28,
	9,
	9,
	112,
	23,
	28,
	23,
	23,
	197,
	23,
	28,
	23,
	28,
	23,
	28,
	23,
	9,
	23,
	9,
	28,
	105,
	105,
	197,
	90,
	23,
	23,
	2320,
	105,
	2320,
	105,
	845,
	3,
	447,
	89,
	31,
	14,
	89,
	27,
	26,
	27,
	28,
	28,
	105,
	105,
	90,
	197,
	105,
	845,
	10,
	370,
	27,
	28,
	14,
	89,
	89,
	89,
	89,
	31,
	14,
	14,
	26,
	89,
	31,
	89,
	26,
	28,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	1036,
	1037,
	2321,
	2322,
	2323,
	2324,
	26,
	124,
	111,
	2325,
	26,
	124,
	2326,
	2327,
	26,
	124,
	197,
	674,
	26,
	124,
	28,
	205,
	28,
	14,
	14,
	26,
	1036,
	1037,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	89,
	31,
	26,
	111,
	111,
	111,
	111,
	2326,
	0,
	0,
	23,
	111,
	23,
	2326,
	14,
	26,
	14,
	14,
	14,
	26,
	89,
	14,
	14,
	26,
	89,
	31,
	89,
	26,
	28,
	14,
	14,
	14,
	26,
	0,
	0,
	26,
	1203,
	931,
	197,
	-1,
	105,
	9,
	112,
	220,
	221,
	28,
	14,
	26,
	26,
	26,
	10,
	32,
	2328,
	2329,
	2330,
	2331,
	2332,
	2333,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	89,
	26,
	14,
	10,
	32,
	26,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	778,
	2245,
	14,
	26,
	14,
	26,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	10,
	1036,
	1037,
	2332,
	2333,
	2334,
	2335,
	2321,
	2322,
	2336,
	2337,
	2323,
	2324,
	14,
	26,
	14,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	1036,
	1037,
	2323,
	2324,
	2321,
	2322,
	26,
	23,
	26,
	28,
	26,
	28,
	800,
	58,
	26,
	14,
	41,
	125,
	23,
	2338,
	90,
	112,
	23,
	26,
	28,
	28,
	143,
	14,
	105,
	28,
	2339,
	361,
	28,
	125,
	2339,
	2340,
	2340,
	2341,
	205,
	1033,
	9,
	674,
	2338,
	2342,
	197,
	52,
	931,
	725,
	725,
	197,
	197,
	674,
	674,
	447,
	674,
	125,
	125,
	125,
	674,
	125,
	125,
	2343,
	674,
	931,
	90,
	824,
	205,
	2344,
	197,
	1346,
	26,
	23,
	9,
	3,
	23,
	28,
	28,
	28,
	112,
	26,
	197,
	14,
	28,
	28,
	975,
	975,
	2345,
	2346,
	931,
	2338,
	27,
	105,
	390,
	197,
	197,
	197,
	975,
	2347,
	975,
	90,
	197,
	27,
	52,
	52,
	52,
	975,
	975,
	975,
	949,
	2338,
	975,
	975,
	9,
	2348,
	975,
	1171,
	130,
	931,
	931,
	26,
	26,
	26,
	14,
	26,
	26,
	14,
	32,
	14,
	26,
	32,
	10,
	32,
	32,
	32,
	32,
	32,
	10,
	32,
	32,
	32,
	26,
	2256,
	339,
	10,
	32,
	89,
	31,
	14,
	26,
	26,
	105,
	197,
	26,
	-1,
	218,
	0,
	0,
	2349,
	0,
	1,
	1,
	0,
	0,
	0,
	0,
	-1,
	-1,
	114,
	114,
	-1,
	49,
	4,
	3,
	23,
	28,
	3,
	23,
	28,
	89,
	89,
	89,
	148,
	28,
	28,
	28,
	10,
	9,
	9,
	23,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	14,
	89,
	778,
	14,
	14,
	2251,
	2248,
	1036,
	1038,
	2254,
	23,
	10,
	14,
	10,
	14,
	14,
	23,
	89,
	10,
	10,
	26,
	14,
	955,
	2276,
	31,
	1037,
	31,
	2274,
	614,
	2273,
	26,
	324,
	1039,
	1225,
	2277,
	337,
	2265,
	23,
	23,
	336,
	2264,
	1679,
	2278,
	32,
	2245,
	200,
	2269,
	26,
	31,
	2275,
	614,
	2271,
	26,
	956,
	2279,
	32,
	2268,
	200,
	2270,
	26,
	614,
	2272,
	26,
	23,
	23,
	26,
	23,
	26,
	460,
	23,
	23,
	26,
	26,
	23,
	14,
	10,
	23,
	26,
	26,
	14,
	1,
	27,
	34,
	62,
	112,
	112,
	62,
	32,
	14,
	26,
	23,
	9,
	130,
	89,
	9,
	14,
	112,
	14,
	10,
	26,
	26,
	14,
	27,
	1,
	14,
	23,
	26,
	23,
	26,
	26,
	89,
	14,
	14,
	2350,
	9,
	148,
	112,
	1091,
	32,
	9,
	34,
	62,
	23,
	27,
	9,
	130,
	135,
	27,
	26,
	26,
	1091,
	0,
	23,
	27,
	27,
	359,
	112,
	62,
	32,
	34,
	62,
	26,
	23,
	9,
	130,
	89,
	9,
	28,
	112,
	23,
	9,
	112,
	62,
	89,
	89,
	26,
	32,
	34,
	62,
	130,
	10,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	23,
	26,
	112,
	1091,
	27,
	26,
	26,
	14,
	10,
	14,
	58,
	28,
	27,
	1,
	0,
	1,
	27,
	27,
	9,
	9,
	800,
	14,
	2351,
	23,
	2352,
	130,
	89,
	2352,
	14,
	26,
	26,
	14,
	28,
	14,
	14,
	28,
	28,
	28,
	23,
	3,
	23,
	28,
	32,
	23,
	89,
	23,
	2353,
	23,
	14,
	14,
	14,
	14,
	26,
	26,
	34,
	62,
	9,
	32,
	112,
	1091,
	9,
	23,
	14,
	10,
	26,
	27,
	27,
	1,
	14,
	14,
	26,
	23,
	9,
	130,
	9,
	10,
	89,
	112,
	62,
	32,
	34,
	62,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	28,
	27,
	9,
	14,
	89,
	14,
	10,
	23,
	27,
	23,
	9,
	23,
	28,
	62,
	32,
	26,
	62,
	800,
	112,
	3,
	26,
	26,
	0,
	14,
	10,
	10,
	10,
	14,
	26,
	14,
	14,
	10,
	89,
	14,
	26,
	14,
	26,
	14,
	23,
	14,
	14,
	2350,
	23,
	26,
	27,
	14,
	136,
	0,
	0,
	207,
	114,
	1125,
	2354,
	151,
	2355,
	2356,
	2357,
	2358,
	2359,
	94,
	235,
	235,
	235,
	114,
	114,
	2312,
	2360,
	2361,
	2362,
	2363,
	95,
	2364,
	2365,
	351,
	2366,
	2367,
	274,
	266,
	0,
	94,
	151,
	423,
	2368,
	531,
	2369,
	0,
	2283,
	2370,
	14,
	14,
	14,
	28,
	105,
	0,
	1,
	27,
	129,
	89,
	10,
	10,
	28,
	28,
	14,
	14,
	26,
	-1,
	3,
	129,
	14,
	26,
	89,
	9,
	89,
	2371,
	9,
	9,
	26,
	28,
	89,
	10,
	10,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	32,
	26,
	130,
	130,
	26,
	23,
	23,
	26,
	26,
	26,
	32,
	32,
	200,
	200,
	336,
	337,
	31,
	614,
	614,
	614,
	31,
	31,
	955,
	324,
	1225,
	26,
	956,
	1679,
	26,
	1526,
	130,
	26,
	26,
	89,
	2372,
	2373,
	138,
	14,
	0,
	4,
	4,
	2374,
	2375,
	10,
	14,
	27,
	135,
	9,
	9,
	10,
	14,
	28,
	105,
	28,
	112,
	112,
	10,
	9,
	219,
	9,
	9,
	219,
	219,
	112,
	112,
	220,
	220,
	221,
	222,
	223,
	224,
	105,
	23,
	197,
	28,
	154,
	125,
	28,
	9,
	23,
	197,
	125,
	9,
	23,
	197,
	125,
	9,
	23,
	197,
	125,
	186,
	0,
	9,
	23,
	0,
	0,
	197,
	125,
	9,
	23,
	3,
	27,
	434,
	23,
	9,
	23,
	9,
	197,
	137,
	125,
	154,
	9,
	23,
	197,
	125,
	28,
	28,
	28,
	9,
	89,
	23,
	0,
	197,
	125,
	9,
	23,
	3,
	197,
	52,
	27,
	197,
	125,
	28,
	105,
	9,
	9,
	23,
	14,
	89,
	23,
	197,
	125,
	9,
	26,
	28,
	28,
	28,
	28,
	28,
	205,
	125,
	105,
	28,
	105,
	105,
	205,
	14,
	26,
	26,
	28,
	89,
	26,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	26,
	14,
	10,
	14,
	14,
	0,
	14,
	89,
	14,
	14,
	26,
	28,
	14,
	28,
	28,
	28,
	28,
	28,
	205,
	125,
	105,
	28,
	105,
	105,
	205,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	28,
	89,
	10,
	14,
	14,
	14,
	14,
	14,
	28,
	14,
	14,
	14,
	26,
	10,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	14,
	89,
	28,
	28,
	28,
	28,
	28,
	205,
	125,
	105,
	28,
	105,
	105,
	205,
	14,
	28,
	14,
	26,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	26,
	14,
	14,
	14,
	26,
	14,
	89,
	14,
	0,
	28,
	26,
	14,
	10,
	14,
	14,
	14,
	14,
	14,
	28,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	26,
	26,
	14,
	9,
	28,
	14,
	14,
	14,
	28,
	89,
	14,
	89,
	89,
	89,
	197,
	28,
	27,
	105,
	105,
	9,
	954,
	1139,
	1139,
	954,
	114,
	125,
	824,
	824,
	974,
	2376,
	0,
	824,
	27,
	9,
	105,
	447,
	197,
	125,
	447,
	800,
	9,
	9,
	9,
	9,
	23,
	3,
	14,
	26,
	89,
	26,
	23,
	27,
	89,
	26,
	89,
	460,
	89,
	460,
	26,
	26,
	27,
	89,
	26,
	26,
	23,
	460,
	26,
	26,
	27,
};
static const Il2CppTokenRangePair s_rgctxIndices[55] = 
{
	{ 0x02000041, { 2, 8 } },
	{ 0x02000043, { 27, 2 } },
	{ 0x02000045, { 29, 14 } },
	{ 0x02000050, { 43, 24 } },
	{ 0x02000051, { 67, 7 } },
	{ 0x02000052, { 74, 7 } },
	{ 0x02000054, { 81, 24 } },
	{ 0x02000058, { 105, 3 } },
	{ 0x02000063, { 132, 1 } },
	{ 0x02000067, { 139, 6 } },
	{ 0x02000068, { 145, 7 } },
	{ 0x02000073, { 184, 1 } },
	{ 0x02000074, { 185, 1 } },
	{ 0x02000075, { 186, 1 } },
	{ 0x02000076, { 187, 1 } },
	{ 0x02000077, { 188, 1 } },
	{ 0x02000078, { 189, 1 } },
	{ 0x0200008C, { 210, 1 } },
	{ 0x0200008D, { 211, 4 } },
	{ 0x0200008E, { 215, 3 } },
	{ 0x02000091, { 218, 7 } },
	{ 0x020000D4, { 236, 7 } },
	{ 0x060000C4, { 0, 2 } },
	{ 0x060001EC, { 10, 1 } },
	{ 0x060001ED, { 11, 3 } },
	{ 0x060001F1, { 14, 3 } },
	{ 0x060001F2, { 17, 3 } },
	{ 0x060001F3, { 20, 3 } },
	{ 0x060001F4, { 23, 3 } },
	{ 0x060001F9, { 26, 1 } },
	{ 0x060002B3, { 108, 2 } },
	{ 0x060002B5, { 110, 8 } },
	{ 0x060002B6, { 118, 3 } },
	{ 0x060002B7, { 121, 3 } },
	{ 0x060002B8, { 124, 4 } },
	{ 0x060002B9, { 128, 4 } },
	{ 0x060002DD, { 133, 6 } },
	{ 0x06000308, { 152, 6 } },
	{ 0x06000309, { 158, 6 } },
	{ 0x0600030A, { 164, 5 } },
	{ 0x0600030B, { 169, 5 } },
	{ 0x0600030C, { 174, 5 } },
	{ 0x0600030D, { 179, 5 } },
	{ 0x06000330, { 190, 2 } },
	{ 0x06000331, { 192, 2 } },
	{ 0x0600036D, { 194, 1 } },
	{ 0x0600036E, { 195, 2 } },
	{ 0x0600036F, { 197, 4 } },
	{ 0x060003A7, { 201, 9 } },
	{ 0x060004A4, { 225, 2 } },
	{ 0x06000597, { 227, 2 } },
	{ 0x060005A3, { 229, 2 } },
	{ 0x060005A4, { 231, 2 } },
	{ 0x060005A7, { 233, 3 } },
	{ 0x0600072E, { 243, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[244] = 
{
	{ (Il2CppRGCTXDataType)1, 30122 },
	{ (Il2CppRGCTXDataType)2, 30122 },
	{ (Il2CppRGCTXDataType)2, 34612 },
	{ (Il2CppRGCTXDataType)3, 23546 },
	{ (Il2CppRGCTXDataType)2, 34613 },
	{ (Il2CppRGCTXDataType)3, 23547 },
	{ (Il2CppRGCTXDataType)2, 34614 },
	{ (Il2CppRGCTXDataType)2, 30233 },
	{ (Il2CppRGCTXDataType)2, 30231 },
	{ (Il2CppRGCTXDataType)2, 34615 },
	{ (Il2CppRGCTXDataType)2, 30240 },
	{ (Il2CppRGCTXDataType)2, 30244 },
	{ (Il2CppRGCTXDataType)2, 34616 },
	{ (Il2CppRGCTXDataType)2, 34617 },
	{ (Il2CppRGCTXDataType)2, 30245 },
	{ (Il2CppRGCTXDataType)2, 34618 },
	{ (Il2CppRGCTXDataType)3, 23548 },
	{ (Il2CppRGCTXDataType)2, 30249 },
	{ (Il2CppRGCTXDataType)3, 23549 },
	{ (Il2CppRGCTXDataType)3, 23550 },
	{ (Il2CppRGCTXDataType)2, 30251 },
	{ (Il2CppRGCTXDataType)3, 23551 },
	{ (Il2CppRGCTXDataType)3, 23552 },
	{ (Il2CppRGCTXDataType)3, 23553 },
	{ (Il2CppRGCTXDataType)3, 23554 },
	{ (Il2CppRGCTXDataType)3, 23555 },
	{ (Il2CppRGCTXDataType)2, 34619 },
	{ (Il2CppRGCTXDataType)2, 34620 },
	{ (Il2CppRGCTXDataType)2, 34621 },
	{ (Il2CppRGCTXDataType)2, 30263 },
	{ (Il2CppRGCTXDataType)2, 30264 },
	{ (Il2CppRGCTXDataType)3, 23556 },
	{ (Il2CppRGCTXDataType)2, 34009 },
	{ (Il2CppRGCTXDataType)3, 23557 },
	{ (Il2CppRGCTXDataType)2, 34622 },
	{ (Il2CppRGCTXDataType)3, 23558 },
	{ (Il2CppRGCTXDataType)3, 23559 },
	{ (Il2CppRGCTXDataType)3, 23560 },
	{ (Il2CppRGCTXDataType)3, 23561 },
	{ (Il2CppRGCTXDataType)3, 23562 },
	{ (Il2CppRGCTXDataType)2, 30265 },
	{ (Il2CppRGCTXDataType)3, 23563 },
	{ (Il2CppRGCTXDataType)1, 30264 },
	{ (Il2CppRGCTXDataType)2, 30304 },
	{ (Il2CppRGCTXDataType)2, 30305 },
	{ (Il2CppRGCTXDataType)2, 30303 },
	{ (Il2CppRGCTXDataType)2, 30306 },
	{ (Il2CppRGCTXDataType)3, 23564 },
	{ (Il2CppRGCTXDataType)3, 23565 },
	{ (Il2CppRGCTXDataType)3, 23566 },
	{ (Il2CppRGCTXDataType)2, 30309 },
	{ (Il2CppRGCTXDataType)2, 34010 },
	{ (Il2CppRGCTXDataType)3, 23567 },
	{ (Il2CppRGCTXDataType)3, 23568 },
	{ (Il2CppRGCTXDataType)2, 34623 },
	{ (Il2CppRGCTXDataType)3, 23569 },
	{ (Il2CppRGCTXDataType)3, 23570 },
	{ (Il2CppRGCTXDataType)2, 34624 },
	{ (Il2CppRGCTXDataType)3, 23571 },
	{ (Il2CppRGCTXDataType)2, 34625 },
	{ (Il2CppRGCTXDataType)3, 23572 },
	{ (Il2CppRGCTXDataType)3, 23573 },
	{ (Il2CppRGCTXDataType)2, 34011 },
	{ (Il2CppRGCTXDataType)3, 23574 },
	{ (Il2CppRGCTXDataType)2, 34626 },
	{ (Il2CppRGCTXDataType)3, 23575 },
	{ (Il2CppRGCTXDataType)2, 30310 },
	{ (Il2CppRGCTXDataType)3, 23576 },
	{ (Il2CppRGCTXDataType)3, 23577 },
	{ (Il2CppRGCTXDataType)2, 30317 },
	{ (Il2CppRGCTXDataType)3, 23578 },
	{ (Il2CppRGCTXDataType)2, 30319 },
	{ (Il2CppRGCTXDataType)3, 23579 },
	{ (Il2CppRGCTXDataType)2, 30320 },
	{ (Il2CppRGCTXDataType)2, 34629 },
	{ (Il2CppRGCTXDataType)3, 23580 },
	{ (Il2CppRGCTXDataType)2, 34629 },
	{ (Il2CppRGCTXDataType)2, 30325 },
	{ (Il2CppRGCTXDataType)2, 30326 },
	{ (Il2CppRGCTXDataType)2, 30324 },
	{ (Il2CppRGCTXDataType)3, 23581 },
	{ (Il2CppRGCTXDataType)2, 30339 },
	{ (Il2CppRGCTXDataType)1, 30340 },
	{ (Il2CppRGCTXDataType)2, 34630 },
	{ (Il2CppRGCTXDataType)3, 23582 },
	{ (Il2CppRGCTXDataType)3, 23583 },
	{ (Il2CppRGCTXDataType)3, 23584 },
	{ (Il2CppRGCTXDataType)2, 34631 },
	{ (Il2CppRGCTXDataType)3, 23585 },
	{ (Il2CppRGCTXDataType)2, 30341 },
	{ (Il2CppRGCTXDataType)3, 23586 },
	{ (Il2CppRGCTXDataType)3, 23587 },
	{ (Il2CppRGCTXDataType)2, 34632 },
	{ (Il2CppRGCTXDataType)3, 23588 },
	{ (Il2CppRGCTXDataType)3, 23589 },
	{ (Il2CppRGCTXDataType)3, 23590 },
	{ (Il2CppRGCTXDataType)3, 23591 },
	{ (Il2CppRGCTXDataType)2, 34633 },
	{ (Il2CppRGCTXDataType)3, 23592 },
	{ (Il2CppRGCTXDataType)3, 23593 },
	{ (Il2CppRGCTXDataType)3, 23594 },
	{ (Il2CppRGCTXDataType)1, 30339 },
	{ (Il2CppRGCTXDataType)3, 23595 },
	{ (Il2CppRGCTXDataType)3, 23596 },
	{ (Il2CppRGCTXDataType)3, 23597 },
	{ (Il2CppRGCTXDataType)2, 34634 },
	{ (Il2CppRGCTXDataType)3, 23598 },
	{ (Il2CppRGCTXDataType)2, 34634 },
	{ (Il2CppRGCTXDataType)1, 30388 },
	{ (Il2CppRGCTXDataType)2, 30388 },
	{ (Il2CppRGCTXDataType)2, 34635 },
	{ (Il2CppRGCTXDataType)3, 23599 },
	{ (Il2CppRGCTXDataType)3, 23600 },
	{ (Il2CppRGCTXDataType)2, 30390 },
	{ (Il2CppRGCTXDataType)3, 23601 },
	{ (Il2CppRGCTXDataType)1, 30391 },
	{ (Il2CppRGCTXDataType)1, 30390 },
	{ (Il2CppRGCTXDataType)3, 23602 },
	{ (Il2CppRGCTXDataType)1, 30393 },
	{ (Il2CppRGCTXDataType)1, 30392 },
	{ (Il2CppRGCTXDataType)2, 30392 },
	{ (Il2CppRGCTXDataType)1, 30395 },
	{ (Il2CppRGCTXDataType)3, 23603 },
	{ (Il2CppRGCTXDataType)3, 23604 },
	{ (Il2CppRGCTXDataType)3, 23605 },
	{ (Il2CppRGCTXDataType)1, 30397 },
	{ (Il2CppRGCTXDataType)1, 30396 },
	{ (Il2CppRGCTXDataType)2, 30396 },
	{ (Il2CppRGCTXDataType)3, 23606 },
	{ (Il2CppRGCTXDataType)1, 30399 },
	{ (Il2CppRGCTXDataType)1, 30398 },
	{ (Il2CppRGCTXDataType)2, 30398 },
	{ (Il2CppRGCTXDataType)2, 30406 },
	{ (Il2CppRGCTXDataType)2, 34636 },
	{ (Il2CppRGCTXDataType)3, 23607 },
	{ (Il2CppRGCTXDataType)1, 34637 },
	{ (Il2CppRGCTXDataType)1, 34638 },
	{ (Il2CppRGCTXDataType)1, 34639 },
	{ (Il2CppRGCTXDataType)3, 23608 },
	{ (Il2CppRGCTXDataType)2, 34641 },
	{ (Il2CppRGCTXDataType)2, 34645 },
	{ (Il2CppRGCTXDataType)3, 23609 },
	{ (Il2CppRGCTXDataType)2, 34646 },
	{ (Il2CppRGCTXDataType)3, 23610 },
	{ (Il2CppRGCTXDataType)3, 23611 },
	{ (Il2CppRGCTXDataType)2, 34648 },
	{ (Il2CppRGCTXDataType)3, 23612 },
	{ (Il2CppRGCTXDataType)2, 34648 },
	{ (Il2CppRGCTXDataType)3, 23613 },
	{ (Il2CppRGCTXDataType)3, 23614 },
	{ (Il2CppRGCTXDataType)2, 30424 },
	{ (Il2CppRGCTXDataType)3, 23615 },
	{ (Il2CppRGCTXDataType)2, 34649 },
	{ (Il2CppRGCTXDataType)3, 23616 },
	{ (Il2CppRGCTXDataType)3, 23617 },
	{ (Il2CppRGCTXDataType)2, 30450 },
	{ (Il2CppRGCTXDataType)3, 23618 },
	{ (Il2CppRGCTXDataType)3, 23619 },
	{ (Il2CppRGCTXDataType)2, 34650 },
	{ (Il2CppRGCTXDataType)3, 23620 },
	{ (Il2CppRGCTXDataType)3, 23621 },
	{ (Il2CppRGCTXDataType)2, 30452 },
	{ (Il2CppRGCTXDataType)3, 23622 },
	{ (Il2CppRGCTXDataType)3, 23623 },
	{ (Il2CppRGCTXDataType)2, 34651 },
	{ (Il2CppRGCTXDataType)3, 23624 },
	{ (Il2CppRGCTXDataType)3, 23625 },
	{ (Il2CppRGCTXDataType)2, 30454 },
	{ (Il2CppRGCTXDataType)3, 23626 },
	{ (Il2CppRGCTXDataType)2, 34652 },
	{ (Il2CppRGCTXDataType)3, 23627 },
	{ (Il2CppRGCTXDataType)3, 23628 },
	{ (Il2CppRGCTXDataType)2, 30456 },
	{ (Il2CppRGCTXDataType)3, 23629 },
	{ (Il2CppRGCTXDataType)2, 34653 },
	{ (Il2CppRGCTXDataType)3, 23630 },
	{ (Il2CppRGCTXDataType)3, 23631 },
	{ (Il2CppRGCTXDataType)2, 30458 },
	{ (Il2CppRGCTXDataType)3, 23632 },
	{ (Il2CppRGCTXDataType)2, 34654 },
	{ (Il2CppRGCTXDataType)3, 23633 },
	{ (Il2CppRGCTXDataType)3, 23634 },
	{ (Il2CppRGCTXDataType)2, 30460 },
	{ (Il2CppRGCTXDataType)3, 23635 },
	{ (Il2CppRGCTXDataType)2, 30469 },
	{ (Il2CppRGCTXDataType)2, 30472 },
	{ (Il2CppRGCTXDataType)2, 30475 },
	{ (Il2CppRGCTXDataType)2, 30479 },
	{ (Il2CppRGCTXDataType)2, 30483 },
	{ (Il2CppRGCTXDataType)2, 30486 },
	{ (Il2CppRGCTXDataType)3, 23636 },
	{ (Il2CppRGCTXDataType)3, 23637 },
	{ (Il2CppRGCTXDataType)3, 23638 },
	{ (Il2CppRGCTXDataType)3, 23639 },
	{ (Il2CppRGCTXDataType)3, 23640 },
	{ (Il2CppRGCTXDataType)3, 23641 },
	{ (Il2CppRGCTXDataType)3, 23642 },
	{ (Il2CppRGCTXDataType)1, 30529 },
	{ (Il2CppRGCTXDataType)2, 30528 },
	{ (Il2CppRGCTXDataType)3, 23643 },
	{ (Il2CppRGCTXDataType)3, 23644 },
	{ (Il2CppRGCTXDataType)2, 34655 },
	{ (Il2CppRGCTXDataType)3, 23645 },
	{ (Il2CppRGCTXDataType)3, 23646 },
	{ (Il2CppRGCTXDataType)2, 34656 },
	{ (Il2CppRGCTXDataType)3, 23647 },
	{ (Il2CppRGCTXDataType)3, 23648 },
	{ (Il2CppRGCTXDataType)3, 23649 },
	{ (Il2CppRGCTXDataType)3, 23650 },
	{ (Il2CppRGCTXDataType)3, 23651 },
	{ (Il2CppRGCTXDataType)3, 23652 },
	{ (Il2CppRGCTXDataType)2, 30562 },
	{ (Il2CppRGCTXDataType)2, 30563 },
	{ (Il2CppRGCTXDataType)2, 30564 },
	{ (Il2CppRGCTXDataType)3, 23653 },
	{ (Il2CppRGCTXDataType)2, 34657 },
	{ (Il2CppRGCTXDataType)3, 23654 },
	{ (Il2CppRGCTXDataType)3, 23655 },
	{ (Il2CppRGCTXDataType)2, 34658 },
	{ (Il2CppRGCTXDataType)3, 23656 },
	{ (Il2CppRGCTXDataType)3, 23657 },
	{ (Il2CppRGCTXDataType)2, 34659 },
	{ (Il2CppRGCTXDataType)3, 23658 },
	{ (Il2CppRGCTXDataType)2, 34660 },
	{ (Il2CppRGCTXDataType)3, 23659 },
	{ (Il2CppRGCTXDataType)1, 30686 },
	{ (Il2CppRGCTXDataType)2, 30686 },
	{ (Il2CppRGCTXDataType)3, 23660 },
	{ (Il2CppRGCTXDataType)2, 34661 },
	{ (Il2CppRGCTXDataType)3, 23661 },
	{ (Il2CppRGCTXDataType)2, 30747 },
	{ (Il2CppRGCTXDataType)3, 23662 },
	{ (Il2CppRGCTXDataType)2, 30748 },
	{ (Il2CppRGCTXDataType)3, 23663 },
	{ (Il2CppRGCTXDataType)3, 23664 },
	{ (Il2CppRGCTXDataType)3, 23665 },
	{ (Il2CppRGCTXDataType)2, 30797 },
	{ (Il2CppRGCTXDataType)2, 30797 },
	{ (Il2CppRGCTXDataType)2, 30794 },
	{ (Il2CppRGCTXDataType)3, 23666 },
	{ (Il2CppRGCTXDataType)3, 23667 },
	{ (Il2CppRGCTXDataType)3, 23668 },
	{ (Il2CppRGCTXDataType)3, 23669 },
	{ (Il2CppRGCTXDataType)2, 30843 },
};
extern const Il2CppCodeGenModule g_Newtonsoft_JsonCodeGenModule;
const Il2CppCodeGenModule g_Newtonsoft_JsonCodeGenModule = 
{
	"Newtonsoft.Json.dll",
	2213,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	55,
	s_rgctxIndices,
	244,
	s_rgctxValues,
	NULL,
};
