﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange>
struct Action_2_tE42A0F669301F0398965DF529367035B65C55784;
// System.Action`2<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr>
struct Action_2_t75C0072ADD9B1BCA881C800496EBBC5AC7227606;
// System.Action`2<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr>[]
struct Action_2U5BU5D_t4CAE18049BF3B3D38B703F990124DDBA64FA133D;
// System.Action`2<UnityEngine.InputSystem.InputDevice,UnityEngine.InputSystem.InputDeviceChange>
struct Action_2_t6749D553543AAD815E5099A7E737C5B7589CBD9A;
// System.Action`2<UnityEngine.InputSystem.LowLevel.InputEventPtr,UnityEngine.InputSystem.InputDevice>
struct Action_2_tA4B1466FF5BDCCC806EB4E05EDAD6DF5FC9A5116;
// System.Action`3<UnityEngine.InputSystem.Users.InputUser,UnityEngine.InputSystem.Users.InputUserChange,UnityEngine.InputSystem.InputDevice>
struct Action_3_tAF3DEC985D65398AF51993F85E9354BF58E04352;
// System.Action`3<UnityEngine.InputSystem.Users.InputUser,UnityEngine.InputSystem.Users.InputUserChange,UnityEngine.InputSystem.InputDevice>[]
struct Action_3U5BU5D_tA51B433AB9DDA22B7DD8EA40646933831CF636B9;
// System.ArgumentNullException
struct ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<System.Object,System.Int32>[]
struct EntryU5BU5D_tE3A30635C5B794ABD7983F09075F9D4F740716D9;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int32>
struct KeyCollection_tEECFF3D52DBAFA05FAD1589D36F0A8EEF9E2670E;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t7B108E5949AABED7EBC85ED1016365781619DAB7;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_tDDB69E91697CCB64C7993B651487CEEC287DB7E8;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>
struct IEnumerator_1_t5C8510D5779F4C833F0E1DD58A0217FE19327338;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputBinding>
struct IEnumerator_1_tA852A679A11B51B8435568FA80A8C1F6EBE0E6EE;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>
struct IEnumerator_1_t2C2D5F5D2ABF3E7E18871B2BE1EB9310035731F1;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputControlScheme>
struct IEnumerator_1_t936EFC1FCE1EB984C5CEC6C8F2268CBE7FD8F94B;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct IEnumerator_1_t832D444044136DDB0A3CFF104EC06813866F5F51;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>
struct IEnumerator_1_tE90C5E231595E0617A6AE4AEE8FB4692163DB47E;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Users.InputUser>
struct IEnumerator_1_tE192D56E72B8796DBBDE0A3789175F0A345EC02C;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.InternedString>
struct IEnumerator_1_tEE5D61250C5890A75462839F4D7E88389A5E4D94;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct IEnumerator_1_t9500FD94AA2AAA2B7571A30A5D4B053162145A43;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct IEnumerator_1_tEC0226A873D5C6D9861985AD1E1F5C758BFB0CFA;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_tAE7A8756D8CF0882DD348DC328FB36FEE0FB7DD0;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>
struct Stack_1_tD844FBAA7E7F6C2AE4AB607F96FC1DB969C7B071;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Object>>
struct Stack_1_t7C250D04E2182CB96C1A0EDEF94ACC7B268BAEC6;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct Stack_1_tD3686B49BE87370A979ADAFA6DEAAE30B3FB6452;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct Stack_1_tDDB642ED18C289FF860C44FD24B801BAC507139A;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct Stack_1_t6CD8A23D0684D010CD60BE0EC39253502CB8D20D;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct Stack_1_tB88789DF6FEC373BE3216AC28D17A22FDB65D489;
// System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct Stack_1_tCFA2A645438950A02F8F2217C68D78686F8FDABB;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.IEnumerator
struct IEnumerator_t8789118187258CC88B77AFAC6315B5AF87D3E18A;
// System.Comparison`1<System.Object>
struct Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.NotImplementedException
struct NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Predicate`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>
struct Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB;
// System.Predicate`1<UnityEngine.InputSystem.InputBinding>
struct Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213;
// System.Predicate`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>
struct Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437;
// System.Predicate`1<UnityEngine.InputSystem.InputControlScheme>
struct Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7;
// System.Predicate`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>
struct Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9;
// System.Predicate`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>
struct Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2;
// System.Predicate`1<UnityEngine.InputSystem.Users.InputUser>
struct Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20;
// System.Predicate`1<UnityEngine.InputSystem.Utilities.InternedString>
struct Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC;
// System.Predicate`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04;
// System.Predicate`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Color32[]
struct Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA;
// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008;
// UnityEngine.Events.UnityAction`1<System.Object>
struct UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.ISubsystemDescriptor
struct ISubsystemDescriptor_t5BCD578E4BAD3A0C1DF6C5654720FE7D4420605B;
// UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage[]
struct HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93;
// UnityEngine.InputSystem.InputBinding[]
struct InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2;
// UnityEngine.InputSystem.InputControlScheme/DeviceRequirement[]
struct DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA;
// UnityEngine.InputSystem.InputControlScheme[]
struct InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D;
// UnityEngine.InputSystem.InputDevice
struct InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678;
// UnityEngine.InputSystem.InputDevice[]
struct InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3;
// UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem[]
struct ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE;
// UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo[]
struct DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A;
// UnityEngine.InputSystem.Users.InputUser/OngoingAccountSelection[]
struct OngoingAccountSelectionU5BU5D_t4EDE21B6668F00E14EBF5A732B8240113101214F;
// UnityEngine.InputSystem.Users.InputUser/UserData[]
struct UserDataU5BU5D_t75DF9B9AFCC647AE82A77C9757993356F20B3AA2;
// UnityEngine.InputSystem.Users.InputUser[]
struct InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D;
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79;
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7;
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE;
// UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026;
// UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA;
// UnityEngine.IntegratedSubsystemDescriptor`1<System.Object>
struct IntegratedSubsystemDescriptor_1_t26346DD8E0AD1C04F25B94E8AD18577ABA15EBCB;
// UnityEngine.IntegratedSubsystem`1<System.Object>
struct IntegratedSubsystem_1_tA39FA5C25840EB481167108B3E02F7F09E901583;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenCallback
struct ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0;
// UnityEngine.UI.CoroutineTween.FloatTween/FloatTweenCallback
struct FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB;
// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>
struct U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902;
// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338;
// UnityEngine.UI.ObjectPool`1<System.Object>
struct ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66;

IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC;
IL2CPP_EXTERN_C String_t* _stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF;
IL2CPP_EXTERN_C String_t* _stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9;
IL2CPP_EXTERN_C String_t* _stringLiteralDFADDFA065E9D92CFE8B8F06444DD5652DFC1D58;
IL2CPP_EXTERN_C String_t* _stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346;
IL2CPP_EXTERN_C const RuntimeMethod* IndexedSet_1_GetEnumerator_mC16C0BE13B73DB47882CA51DB581747C19C7128B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* IndexedSet_1_Insert_mB43CB3AA6D461E94217E51DE6DD4ED3F5E1607CD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4AE666FAF40BAF18CA8D1BCAFDB0D428DEA5DFF8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m605D45F96449D5CF6DDFACF60880651C85F29195_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t IndexedSet_1_GetEnumerator_mC16C0BE13B73DB47882CA51DB581747C19C7128B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t IndexedSet_1_Insert_mB43CB3AA6D461E94217E51DE6DD4ED3F5E1607CD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ObjectPool_1_Release_m11F10A7199F0544250785EAA41F819E03D65BC17_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TweenRunner_1_StartTween_m1C12D6475DFCA47D74844FD2395C1057AF1CA41D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TweenRunner_1_StartTween_m96867F74662CBAED655A9C7B3AF0036E5282F945_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__2_MoveNext_m072CCA02187FAAE6E23766279083847801807198_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__2_MoveNext_m1F9733709DEC4B352BBFD9939F2B27D0A01137AF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4AE666FAF40BAF18CA8D1BCAFDB0D428DEA5DFF8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m605D45F96449D5CF6DDFACF60880651C85F29195_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_com;
struct DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
struct HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93;
struct InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2;
struct DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA;
struct InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D;
struct ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE;
struct DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A;
struct InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D;
struct InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79;
struct NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7;
struct NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct  Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tE3A30635C5B794ABD7983F09075F9D4F740716D9* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tEECFF3D52DBAFA05FAD1589D36F0A8EEF9E2670E * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t7B108E5949AABED7EBC85ED1016365781619DAB7 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___entries_1)); }
	inline EntryU5BU5D_tE3A30635C5B794ABD7983F09075F9D4F740716D9* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tE3A30635C5B794ABD7983F09075F9D4F740716D9** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tE3A30635C5B794ABD7983F09075F9D4F740716D9* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___keys_7)); }
	inline KeyCollection_tEECFF3D52DBAFA05FAD1589D36F0A8EEF9E2670E * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tEECFF3D52DBAFA05FAD1589D36F0A8EEF9E2670E ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tEECFF3D52DBAFA05FAD1589D36F0A8EEF9E2670E * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ___values_8)); }
	inline ValueCollection_t7B108E5949AABED7EBC85ED1016365781619DAB7 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t7B108E5949AABED7EBC85ED1016365781619DAB7 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t7B108E5949AABED7EBC85ED1016365781619DAB7 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____items_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____items_1)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Color32>
struct  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____items_1)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__items_1() const { return ____items_1; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5_StaticFields, ____emptyArray_5)); }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Color32U5BU5D_tABFBCB467E6D1B791303A0D3A3AA1A482F620983* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____items_1)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554_StaticFields, ____emptyArray_5)); }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIVertexU5BU5D_tB560F9F9269864891FCE1677971F603A08AA857A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____items_1)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__items_1() const { return ____items_1; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB_StaticFields, ____emptyArray_5)); }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector2U5BU5D_tA065A07DFC060C1B8786BBAA5F3A6577CCEB27D6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____items_1)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__items_1() const { return ____items_1; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5_StaticFields, ____emptyArray_5)); }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector3U5BU5D_tB9EC3346CC4A0EA5447D968E84A9AC1F6F372C28* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____items_1)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__items_1() const { return ____items_1; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955_StaticFields, ____emptyArray_5)); }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Vector4U5BU5D_t51402C154FFFCF7217A9BEC4B834F0B726C10F66* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<System.Object>
struct  Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____array_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get__array_0() const { return ____array_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage,UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>
struct  Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201, ___m_Array_0)); }
	inline HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* get_m_Array_0() const { return ___m_Array_0; }
	inline HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputBinding>
struct  Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908, ___m_Array_0)); }
	inline InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* get_m_Array_0() const { return ___m_Array_0; }
	inline InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.InputControlScheme,UnityEngine.InputSystem.InputControlScheme>
struct  Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF, ___m_Array_0)); }
	inline InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* get_m_Array_0() const { return ___m_Array_0; }
	inline InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement,UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>
struct  Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D, ___m_Array_0)); }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* get_m_Array_0() const { return ___m_Array_0; }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem,UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>
struct  Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0, ___m_Array_0)); }
	inline ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* get_m_Array_0() const { return ___m_Array_0; }
	inline ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo,UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>
struct  Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142, ___m_Array_0)); }
	inline DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* get_m_Array_0() const { return ___m_Array_0; }
	inline DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.Users.InputUser,UnityEngine.InputSystem.Users.InputUser>
struct  Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4, ___m_Array_0)); }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* get_m_Array_0() const { return ___m_Array_0; }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.Utilities.InternedString,UnityEngine.InputSystem.Utilities.InternedString>
struct  Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C, ___m_Array_0)); }
	inline InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* get_m_Array_0() const { return ___m_Array_0; }
	inline InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.Utilities.NameAndParameters,UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A, ___m_Array_0)); }
	inline NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* get_m_Array_0() const { return ___m_Array_0; }
	inline NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1<UnityEngine.InputSystem.Utilities.NamedValue,UnityEngine.InputSystem.Utilities.NamedValue>
struct  Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008  : public RuntimeObject
{
public:
	// T[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Array
	NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexStart
	int32_t ___m_IndexStart_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_IndexEnd
	int32_t ___m_IndexEnd_2;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1_Enumerator`1::m_Index
	int32_t ___m_Index_3;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008, ___m_Array_0)); }
	inline NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_IndexStart_1() { return static_cast<int32_t>(offsetof(Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008, ___m_IndexStart_1)); }
	inline int32_t get_m_IndexStart_1() const { return ___m_IndexStart_1; }
	inline int32_t* get_address_of_m_IndexStart_1() { return &___m_IndexStart_1; }
	inline void set_m_IndexStart_1(int32_t value)
	{
		___m_IndexStart_1 = value;
	}

	inline static int32_t get_offset_of_m_IndexEnd_2() { return static_cast<int32_t>(offsetof(Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008, ___m_IndexEnd_2)); }
	inline int32_t get_m_IndexEnd_2() const { return ___m_IndexEnd_2; }
	inline int32_t* get_address_of_m_IndexEnd_2() { return &___m_IndexEnd_2; }
	inline void set_m_IndexEnd_2(int32_t value)
	{
		___m_IndexEnd_2 = value;
	}

	inline static int32_t get_offset_of_m_Index_3() { return static_cast<int32_t>(offsetof(Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008, ___m_Index_3)); }
	inline int32_t get_m_Index_3() const { return ___m_Index_3; }
	inline int32_t* get_address_of_m_Index_3() { return &___m_Index_3; }
	inline void set_m_Index_3(int32_t value)
	{
		___m_Index_3 = value;
	}
};


// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct  IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<T> UnityEngine.UI.Collections.IndexedSet`1::m_List
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___m_List_0;
	// System.Collections.Generic.Dictionary`2<T,System.Int32> UnityEngine.UI.Collections.IndexedSet`1::m_Dictionary
	Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * ___m_Dictionary_1;

public:
	inline static int32_t get_offset_of_m_List_0() { return static_cast<int32_t>(offsetof(IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C, ___m_List_0)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_m_List_0() const { return ___m_List_0; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_m_List_0() { return &___m_List_0; }
	inline void set_m_List_0(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___m_List_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_List_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Dictionary_1() { return static_cast<int32_t>(offsetof(IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C, ___m_Dictionary_1)); }
	inline Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * get_m_Dictionary_1() const { return ___m_Dictionary_1; }
	inline Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A ** get_address_of_m_Dictionary_1() { return &___m_Dictionary_1; }
	inline void set_m_Dictionary_1(Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * value)
	{
		___m_Dictionary_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Dictionary_1), (void*)value);
	}
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct  TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CoroutineContainer_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tween_1), (void*)value);
	}
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct  TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF  : public RuntimeObject
{
public:
	// UnityEngine.MonoBehaviour UnityEngine.UI.CoroutineTween.TweenRunner`1::m_CoroutineContainer
	MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___m_CoroutineContainer_0;
	// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1::m_Tween
	RuntimeObject* ___m_Tween_1;

public:
	inline static int32_t get_offset_of_m_CoroutineContainer_0() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_CoroutineContainer_0)); }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * get_m_CoroutineContainer_0() const { return ___m_CoroutineContainer_0; }
	inline MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 ** get_address_of_m_CoroutineContainer_0() { return &___m_CoroutineContainer_0; }
	inline void set_m_CoroutineContainer_0(MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * value)
	{
		___m_CoroutineContainer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CoroutineContainer_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tween_1() { return static_cast<int32_t>(offsetof(TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF, ___m_Tween_1)); }
	inline RuntimeObject* get_m_Tween_1() const { return ___m_Tween_1; }
	inline RuntimeObject** get_address_of_m_Tween_1() { return &___m_Tween_1; }
	inline void set_m_Tween_1(RuntimeObject* value)
	{
		___m_Tween_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tween_1), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<System.Int32>
struct  ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<System.Object>
struct  ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Color32>
struct  ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>
struct  ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector2>
struct  ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector3>
struct  ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ListPool`1<UnityEngine.Vector4>
struct  ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540  : public RuntimeObject
{
public:

public:
};

struct ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540_StaticFields
{
public:
	// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<T>> UnityEngine.UI.ListPool`1::s_ListPool
	ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * ___s_ListPool_0;

public:
	inline static int32_t get_offset_of_s_ListPool_0() { return static_cast<int32_t>(offsetof(ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540_StaticFields, ___s_ListPool_0)); }
	inline ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * get_s_ListPool_0() const { return ___s_ListPool_0; }
	inline ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 ** get_address_of_s_ListPool_0() { return &___s_ListPool_0; }
	inline void set_s_ListPool_0(ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * value)
	{
		___s_ListPool_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ListPool_0), (void*)value);
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Int32>>
struct  ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tD844FBAA7E7F6C2AE4AB607F96FC1DB969C7B071 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B, ___m_Stack_0)); }
	inline Stack_1_tD844FBAA7E7F6C2AE4AB607F96FC1DB969C7B071 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tD844FBAA7E7F6C2AE4AB607F96FC1DB969C7B071 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tD844FBAA7E7F6C2AE4AB607F96FC1DB969C7B071 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<System.Object>>
struct  ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t7C250D04E2182CB96C1A0EDEF94ACC7B268BAEC6 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE, ___m_Stack_0)); }
	inline Stack_1_t7C250D04E2182CB96C1A0EDEF94ACC7B268BAEC6 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t7C250D04E2182CB96C1A0EDEF94ACC7B268BAEC6 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t7C250D04E2182CB96C1A0EDEF94ACC7B268BAEC6 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tD3686B49BE87370A979ADAFA6DEAAE30B3FB6452 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8, ___m_Stack_0)); }
	inline Stack_1_tD3686B49BE87370A979ADAFA6DEAAE30B3FB6452 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tD3686B49BE87370A979ADAFA6DEAAE30B3FB6452 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tD3686B49BE87370A979ADAFA6DEAAE30B3FB6452 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tDDB642ED18C289FF860C44FD24B801BAC507139A * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062, ___m_Stack_0)); }
	inline Stack_1_tDDB642ED18C289FF860C44FD24B801BAC507139A * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tDDB642ED18C289FF860C44FD24B801BAC507139A ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tDDB642ED18C289FF860C44FD24B801BAC507139A * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t6CD8A23D0684D010CD60BE0EC39253502CB8D20D * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26, ___m_Stack_0)); }
	inline Stack_1_t6CD8A23D0684D010CD60BE0EC39253502CB8D20D * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t6CD8A23D0684D010CD60BE0EC39253502CB8D20D ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t6CD8A23D0684D010CD60BE0EC39253502CB8D20D * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tB88789DF6FEC373BE3216AC28D17A22FDB65D489 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902, ___m_Stack_0)); }
	inline Stack_1_tB88789DF6FEC373BE3216AC28D17A22FDB65D489 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tB88789DF6FEC373BE3216AC28D17A22FDB65D489 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tB88789DF6FEC373BE3216AC28D17A22FDB65D489 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902, ___m_ActionOnGet_1)); }
	inline UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_tCFA2A645438950A02F8F2217C68D78686F8FDABB * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338, ___m_Stack_0)); }
	inline Stack_1_tCFA2A645438950A02F8F2217C68D78686F8FDABB * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_tCFA2A645438950A02F8F2217C68D78686F8FDABB ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_tCFA2A645438950A02F8F2217C68D78686F8FDABB * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.UI.ObjectPool`1<System.Object>
struct  ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<T> UnityEngine.UI.ObjectPool`1::m_Stack
	Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * ___m_Stack_0;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnGet
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnGet_1;
	// UnityEngine.Events.UnityAction`1<T> UnityEngine.UI.ObjectPool`1::m_ActionOnRelease
	UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___m_ActionOnRelease_2;
	// System.Int32 UnityEngine.UI.ObjectPool`1::<countAll>k__BackingField
	int32_t ___U3CcountAllU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_m_Stack_0() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B, ___m_Stack_0)); }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * get_m_Stack_0() const { return ___m_Stack_0; }
	inline Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 ** get_address_of_m_Stack_0() { return &___m_Stack_0; }
	inline void set_m_Stack_0(Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * value)
	{
		___m_Stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Stack_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnGet_1() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B, ___m_ActionOnGet_1)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnGet_1() const { return ___m_ActionOnGet_1; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnGet_1() { return &___m_ActionOnGet_1; }
	inline void set_m_ActionOnGet_1(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnGet_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnGet_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionOnRelease_2() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B, ___m_ActionOnRelease_2)); }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * get_m_ActionOnRelease_2() const { return ___m_ActionOnRelease_2; }
	inline UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 ** get_address_of_m_ActionOnRelease_2() { return &___m_ActionOnRelease_2; }
	inline void set_m_ActionOnRelease_2(UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * value)
	{
		___m_ActionOnRelease_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionOnRelease_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcountAllU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B, ___U3CcountAllU3Ek__BackingField_3)); }
	inline int32_t get_U3CcountAllU3Ek__BackingField_3() const { return ___U3CcountAllU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CcountAllU3Ek__BackingField_3() { return &___U3CcountAllU3Ek__BackingField_3; }
	inline void set_U3CcountAllU3Ek__BackingField_3(int32_t value)
	{
		___U3CcountAllU3Ek__BackingField_3 = value;
	}
};


// UnityEngine.YieldInstruction
struct  YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
};

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_fastRng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____fastRng_14;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rngAccess_12), (void*)value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rng_13), (void*)value);
	}

	inline static int32_t get_offset_of__fastRng_14() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____fastRng_14)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__fastRng_14() const { return ____fastRng_14; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__fastRng_14() { return &____fastRng_14; }
	inline void set__fastRng_14(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____fastRng_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fastRng_14), (void*)value);
	}
};


// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.InputSystem.InputControlScheme
struct  InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431 
{
public:
	// System.String UnityEngine.InputSystem.InputControlScheme::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.InputSystem.InputControlScheme::m_BindingGroup
	String_t* ___m_BindingGroup_1;
	// UnityEngine.InputSystem.InputControlScheme_DeviceRequirement[] UnityEngine.InputSystem.InputControlScheme::m_DeviceRequirements
	DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___m_DeviceRequirements_2;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingGroup_1() { return static_cast<int32_t>(offsetof(InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431, ___m_BindingGroup_1)); }
	inline String_t* get_m_BindingGroup_1() const { return ___m_BindingGroup_1; }
	inline String_t** get_address_of_m_BindingGroup_1() { return &___m_BindingGroup_1; }
	inline void set_m_BindingGroup_1(String_t* value)
	{
		___m_BindingGroup_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BindingGroup_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeviceRequirements_2() { return static_cast<int32_t>(offsetof(InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431, ___m_DeviceRequirements_2)); }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* get_m_DeviceRequirements_2() const { return ___m_DeviceRequirements_2; }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA** get_address_of_m_DeviceRequirements_2() { return &___m_DeviceRequirements_2; }
	inline void set_m_DeviceRequirements_2(DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* value)
	{
		___m_DeviceRequirements_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceRequirements_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_BindingGroup_1;
	DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_pinvoke* ___m_DeviceRequirements_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_BindingGroup_1;
	DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_com* ___m_DeviceRequirements_2;
};

// UnityEngine.InputSystem.Users.InputUser_OngoingAccountSelection
struct  OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752 
{
public:
	// UnityEngine.InputSystem.InputDevice UnityEngine.InputSystem.Users.InputUser_OngoingAccountSelection::device
	InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 * ___device_0;
	// System.UInt32 UnityEngine.InputSystem.Users.InputUser_OngoingAccountSelection::userId
	uint32_t ___userId_1;

public:
	inline static int32_t get_offset_of_device_0() { return static_cast<int32_t>(offsetof(OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752, ___device_0)); }
	inline InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 * get_device_0() const { return ___device_0; }
	inline InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 ** get_address_of_device_0() { return &___device_0; }
	inline void set_device_0(InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 * value)
	{
		___device_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___device_0), (void*)value);
	}

	inline static int32_t get_offset_of_userId_1() { return static_cast<int32_t>(offsetof(OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752, ___userId_1)); }
	inline uint32_t get_userId_1() const { return ___userId_1; }
	inline uint32_t* get_address_of_userId_1() { return &___userId_1; }
	inline void set_userId_1(uint32_t value)
	{
		___userId_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Users.InputUser/OngoingAccountSelection
struct OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752_marshaled_pinvoke
{
	InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 * ___device_0;
	uint32_t ___userId_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Users.InputUser/OngoingAccountSelection
struct OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752_marshaled_com
{
	InputDevice_tA5CE5C65B4BC6BA9B6E9A5598EF60A95A828E678 * ___device_0;
	uint32_t ___userId_1;
};

// UnityEngine.InputSystem.Utilities.FourCC
struct  FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`2<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr>>
struct  InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_2_t75C0072ADD9B1BCA881C800496EBBC5AC7227606 * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_2U5BU5D_t4CAE18049BF3B3D38B703F990124DDBA64FA133D* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE, ___firstValue_1)); }
	inline Action_2_t75C0072ADD9B1BCA881C800496EBBC5AC7227606 * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_2_t75C0072ADD9B1BCA881C800496EBBC5AC7227606 ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_2_t75C0072ADD9B1BCA881C800496EBBC5AC7227606 * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE, ___additionalValues_2)); }
	inline Action_2U5BU5D_t4CAE18049BF3B3D38B703F990124DDBA64FA133D* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_2U5BU5D_t4CAE18049BF3B3D38B703F990124DDBA64FA133D** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_2U5BU5D_t4CAE18049BF3B3D38B703F990124DDBA64FA133D* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`3<UnityEngine.InputSystem.Users.InputUser,UnityEngine.InputSystem.Users.InputUserChange,UnityEngine.InputSystem.InputDevice>>
struct  InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_3_tAF3DEC985D65398AF51993F85E9354BF58E04352 * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_3U5BU5D_tA51B433AB9DDA22B7DD8EA40646933831CF636B9* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0, ___firstValue_1)); }
	inline Action_3_tAF3DEC985D65398AF51993F85E9354BF58E04352 * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_3_tAF3DEC985D65398AF51993F85E9354BF58E04352 ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_3_tAF3DEC985D65398AF51993F85E9354BF58E04352 * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0, ___additionalValues_2)); }
	inline Action_3U5BU5D_tA51B433AB9DDA22B7DD8EA40646933831CF636B9* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_3U5BU5D_tA51B433AB9DDA22B7DD8EA40646933831CF636B9** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_3U5BU5D_tA51B433AB9DDA22B7DD8EA40646933831CF636B9* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct  InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>
struct  ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8, ___m_Array_0)); }
	inline HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* get_m_Array_0() const { return ___m_Array_0; }
	inline HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>
struct  ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712, ___m_Array_0)); }
	inline InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* get_m_Array_0() const { return ___m_Array_0; }
	inline InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>
struct  ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF, ___m_Array_0)); }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* get_m_Array_0() const { return ___m_Array_0; }
	inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>
struct  ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999, ___m_Array_0)); }
	inline InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* get_m_Array_0() const { return ___m_Array_0; }
	inline InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>
struct  ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6, ___m_Array_0)); }
	inline ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* get_m_Array_0() const { return ___m_Array_0; }
	inline ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>
struct  ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD, ___m_Array_0)); }
	inline DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* get_m_Array_0() const { return ___m_Array_0; }
	inline DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>
struct  ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D, ___m_Array_0)); }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* get_m_Array_0() const { return ___m_Array_0; }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F, ___m_Array_0)); }
	inline InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* get_m_Array_0() const { return ___m_Array_0; }
	inline InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048, ___m_Array_0)); }
	inline NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* get_m_Array_0() const { return ___m_Array_0; }
	inline NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct  ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9, ___m_Array_0)); }
	inline NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* get_m_Array_0() const { return ___m_Array_0; }
	inline NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.UI.CoroutineTween.FloatTween
struct  FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A 
{
public:
	// UnityEngine.UI.CoroutineTween.FloatTween_FloatTweenCallback UnityEngine.UI.CoroutineTween.FloatTween::m_Target
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_StartValue
	float ___m_StartValue_1;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_TargetValue
	float ___m_TargetValue_2;
	// System.Single UnityEngine.UI.CoroutineTween.FloatTween::m_Duration
	float ___m_Duration_3;
	// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_4;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Target_0)); }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * get_m_Target_0() const { return ___m_Target_0; }
	inline FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartValue_1() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_StartValue_1)); }
	inline float get_m_StartValue_1() const { return ___m_StartValue_1; }
	inline float* get_address_of_m_StartValue_1() { return &___m_StartValue_1; }
	inline void set_m_StartValue_1(float value)
	{
		___m_StartValue_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetValue_2() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_TargetValue_2)); }
	inline float get_m_TargetValue_2() const { return ___m_TargetValue_2; }
	inline float* get_address_of_m_TargetValue_2() { return &___m_TargetValue_2; }
	inline void set_m_TargetValue_2(float value)
	{
		___m_TargetValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Duration_3() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_Duration_3)); }
	inline float get_m_Duration_3() const { return ___m_Duration_3; }
	inline float* get_address_of_m_Duration_3() { return &___m_Duration_3; }
	inline void set_m_Duration_3(float value)
	{
		___m_Duration_3 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_4() { return static_cast<int32_t>(offsetof(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A, ___m_IgnoreTimeScale_4)); }
	inline bool get_m_IgnoreTimeScale_4() const { return ___m_IgnoreTimeScale_4; }
	inline bool* get_address_of_m_IgnoreTimeScale_4() { return &___m_IgnoreTimeScale_4; }
	inline void set_m_IgnoreTimeScale_4(bool value)
	{
		___m_IgnoreTimeScale_4 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_pinvoke
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.FloatTween
struct FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A_marshaled_com
{
	FloatTweenCallback_t69056DA8AAB3BCDA97012834C1F1F265F7617502 * ___m_Target_0;
	float ___m_StartValue_1;
	float ___m_TargetValue_2;
	float ___m_Duration_3;
	int32_t ___m_IgnoreTimeScale_4;
};

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.TypeCode
struct  TypeCode_t03ED52F888000DAF40C550C434F29F39A23D61C6 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_t03ED52F888000DAF40C550C434F29F39A23D61C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC  : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_pinvoke : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC_marshaled_com : public YieldInstruction_t836035AC7BD07A3C7909F7AD2A5B42DE99D91C44_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.InputSystem.HID.HID_UsagePage
struct  UsagePage_t0998C5FFC45A2E17FC00B5300FAA0E3D1291BC4C 
{
public:
	// System.Int32 UnityEngine.InputSystem.HID.HID_UsagePage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UsagePage_t0998C5FFC45A2E17FC00B5300FAA0E3D1291BC4C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputBinding_Flags
struct  Flags_tE71E643C82C59093B86A764F24B2273DE3A69B3F 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputBinding_Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tE71E643C82C59093B86A764F24B2273DE3A69B3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputControlScheme_DeviceRequirement_Flags
struct  Flags_tB4F9DA9E86779AEED4EB381716BBEEDFCA330485 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputControlScheme_DeviceRequirement_Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tB4F9DA9E86779AEED4EB381716BBEEDFCA330485, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem_Flags
struct  Flags_tCCD992B18BB6B6EF069484214DF2D99FFED73E65 
{
public:
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem_Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tCCD992B18BB6B6EF069484214DF2D99FFED73E65, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo
struct  DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46 
{
public:
	// System.Int32 UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo::m_DeviceId
	int32_t ___m_DeviceId_0;
	// System.String UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo::m_Layout
	String_t* ___m_Layout_1;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo::m_StateFormat
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___m_StateFormat_2;
	// System.Int32 UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo::m_StateSizeInBytes
	int32_t ___m_StateSizeInBytes_3;
	// System.String UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo::m_FullLayoutJson
	String_t* ___m_FullLayoutJson_4;

public:
	inline static int32_t get_offset_of_m_DeviceId_0() { return static_cast<int32_t>(offsetof(DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46, ___m_DeviceId_0)); }
	inline int32_t get_m_DeviceId_0() const { return ___m_DeviceId_0; }
	inline int32_t* get_address_of_m_DeviceId_0() { return &___m_DeviceId_0; }
	inline void set_m_DeviceId_0(int32_t value)
	{
		___m_DeviceId_0 = value;
	}

	inline static int32_t get_offset_of_m_Layout_1() { return static_cast<int32_t>(offsetof(DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46, ___m_Layout_1)); }
	inline String_t* get_m_Layout_1() const { return ___m_Layout_1; }
	inline String_t** get_address_of_m_Layout_1() { return &___m_Layout_1; }
	inline void set_m_Layout_1(String_t* value)
	{
		___m_Layout_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Layout_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_StateFormat_2() { return static_cast<int32_t>(offsetof(DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46, ___m_StateFormat_2)); }
	inline FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  get_m_StateFormat_2() const { return ___m_StateFormat_2; }
	inline FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1 * get_address_of_m_StateFormat_2() { return &___m_StateFormat_2; }
	inline void set_m_StateFormat_2(FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  value)
	{
		___m_StateFormat_2 = value;
	}

	inline static int32_t get_offset_of_m_StateSizeInBytes_3() { return static_cast<int32_t>(offsetof(DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46, ___m_StateSizeInBytes_3)); }
	inline int32_t get_m_StateSizeInBytes_3() const { return ___m_StateSizeInBytes_3; }
	inline int32_t* get_address_of_m_StateSizeInBytes_3() { return &___m_StateSizeInBytes_3; }
	inline void set_m_StateSizeInBytes_3(int32_t value)
	{
		___m_StateSizeInBytes_3 = value;
	}

	inline static int32_t get_offset_of_m_FullLayoutJson_4() { return static_cast<int32_t>(offsetof(DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46, ___m_FullLayoutJson_4)); }
	inline String_t* get_m_FullLayoutJson_4() const { return ___m_FullLayoutJson_4; }
	inline String_t** get_address_of_m_FullLayoutJson_4() { return &___m_FullLayoutJson_4; }
	inline void set_m_FullLayoutJson_4(String_t* value)
	{
		___m_FullLayoutJson_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FullLayoutJson_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo
struct DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46_marshaled_pinvoke
{
	int32_t ___m_DeviceId_0;
	char* ___m_Layout_1;
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___m_StateFormat_2;
	int32_t ___m_StateSizeInBytes_3;
	char* ___m_FullLayoutJson_4;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo
struct DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46_marshaled_com
{
	int32_t ___m_DeviceId_0;
	Il2CppChar* ___m_Layout_1;
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___m_StateFormat_2;
	int32_t ___m_StateSizeInBytes_3;
	Il2CppChar* ___m_FullLayoutJson_4;
};

// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.Users.InputUser_OngoingAccountSelection>
struct  InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752  ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	OngoingAccountSelectionU5BU5D_t4EDE21B6668F00E14EBF5A732B8240113101214F* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC, ___firstValue_1)); }
	inline OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752  get_firstValue_1() const { return ___firstValue_1; }
	inline OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752 * get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(OngoingAccountSelection_t8C34027801F5CB192CB4D62CDE44649301F0F752  value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___firstValue_1))->___device_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC, ___additionalValues_2)); }
	inline OngoingAccountSelectionU5BU5D_t4EDE21B6668F00E14EBF5A732B8240113101214F* get_additionalValues_2() const { return ___additionalValues_2; }
	inline OngoingAccountSelectionU5BU5D_t4EDE21B6668F00E14EBF5A732B8240113101214F** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(OngoingAccountSelectionU5BU5D_t4EDE21B6668F00E14EBF5A732B8240113101214F* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.NameAndParameters
struct  NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NameAndParameters::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Utilities.NameAndParameters::<parameters>k__BackingField
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6, ___U3CparametersU3Ek__BackingField_1)); }
	inline ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  get_U3CparametersU3Ek__BackingField_1() const { return ___U3CparametersU3Ek__BackingField_1; }
	inline ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * get_address_of_U3CparametersU3Ek__BackingField_1() { return &___U3CparametersU3Ek__BackingField_1; }
	inline void set_U3CparametersU3Ek__BackingField_1(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  value)
	{
		___U3CparametersU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NameAndParameters
struct NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_1;
};

// UnityEngine.IntegratedSubsystem
struct  IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.IntegratedSubsystem::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.ISubsystemDescriptor UnityEngine.IntegratedSubsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_subsystemDescriptor_1() { return static_cast<int32_t>(offsetof(IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026, ___m_subsystemDescriptor_1)); }
	inline RuntimeObject* get_m_subsystemDescriptor_1() const { return ___m_subsystemDescriptor_1; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_1() { return &___m_subsystemDescriptor_1; }
	inline void set_m_subsystemDescriptor_1(RuntimeObject* value)
	{
		___m_subsystemDescriptor_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_subsystemDescriptor_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystem
struct IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026_marshaled_com
{
	intptr_t ___m_Ptr_0;
	RuntimeObject* ___m_subsystemDescriptor_1;
};

// UnityEngine.IntegratedSubsystemDescriptor
struct  IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.IntegratedSubsystemDescriptor::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor
struct IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode
struct  ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F 
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorTweenMode_tDCE018D37330F576ACCD00D16CAF91AE55315F2F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>
struct  U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::tweenInfo
	FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo_2;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_tweenInfo_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E, ___tweenInfo_2)); }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  get_tweenInfo_2() const { return ___tweenInfo_2; }
	inline FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * get_address_of_tweenInfo_2() { return &___tweenInfo_2; }
	inline void set_tweenInfo_2(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  value)
	{
		___tweenInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___tweenInfo_2))->___m_Target_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage
struct  HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9 
{
public:
	// UnityEngine.InputSystem.HID.HID_UsagePage UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage::page
	int32_t ___page_0;
	// System.Int32 UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage::usage
	int32_t ___usage_1;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9, ___page_0)); }
	inline int32_t get_page_0() const { return ___page_0; }
	inline int32_t* get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(int32_t value)
	{
		___page_0 = value;
	}

	inline static int32_t get_offset_of_usage_1() { return static_cast<int32_t>(offsetof(HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9, ___usage_1)); }
	inline int32_t get_usage_1() const { return ___usage_1; }
	inline int32_t* get_address_of_usage_1() { return &___usage_1; }
	inline void set_usage_1(int32_t value)
	{
		___usage_1 = value;
	}
};


// UnityEngine.InputSystem.InputBinding
struct  InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687 
{
public:
	// System.String UnityEngine.InputSystem.InputBinding::m_Name
	String_t* ___m_Name_2;
	// System.String UnityEngine.InputSystem.InputBinding::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputBinding::m_Path
	String_t* ___m_Path_4;
	// System.String UnityEngine.InputSystem.InputBinding::m_Interactions
	String_t* ___m_Interactions_5;
	// System.String UnityEngine.InputSystem.InputBinding::m_Processors
	String_t* ___m_Processors_6;
	// System.String UnityEngine.InputSystem.InputBinding::m_Groups
	String_t* ___m_Groups_7;
	// System.String UnityEngine.InputSystem.InputBinding::m_Action
	String_t* ___m_Action_8;
	// UnityEngine.InputSystem.InputBinding_Flags UnityEngine.InputSystem.InputBinding::m_Flags
	int32_t ___m_Flags_9;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverridePath
	String_t* ___m_OverridePath_10;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideInteractions
	String_t* ___m_OverrideInteractions_11;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideProcessors
	String_t* ___m_OverrideProcessors_12;
	// System.Guid UnityEngine.InputSystem.InputBinding::m_Guid
	Guid_t  ___m_Guid_13;

public:
	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Name_2)); }
	inline String_t* get_m_Name_2() const { return ___m_Name_2; }
	inline String_t** get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(String_t* value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Path_4() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Path_4)); }
	inline String_t* get_m_Path_4() const { return ___m_Path_4; }
	inline String_t** get_address_of_m_Path_4() { return &___m_Path_4; }
	inline void set_m_Path_4(String_t* value)
	{
		___m_Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_6() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Processors_6)); }
	inline String_t* get_m_Processors_6() const { return ___m_Processors_6; }
	inline String_t** get_address_of_m_Processors_6() { return &___m_Processors_6; }
	inline void set_m_Processors_6(String_t* value)
	{
		___m_Processors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Groups_7() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Groups_7)); }
	inline String_t* get_m_Groups_7() const { return ___m_Groups_7; }
	inline String_t** get_address_of_m_Groups_7() { return &___m_Groups_7; }
	inline void set_m_Groups_7(String_t* value)
	{
		___m_Groups_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Groups_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Action_8() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Action_8)); }
	inline String_t* get_m_Action_8() const { return ___m_Action_8; }
	inline String_t** get_address_of_m_Action_8() { return &___m_Action_8; }
	inline void set_m_Action_8(String_t* value)
	{
		___m_Action_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_9() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Flags_9)); }
	inline int32_t get_m_Flags_9() const { return ___m_Flags_9; }
	inline int32_t* get_address_of_m_Flags_9() { return &___m_Flags_9; }
	inline void set_m_Flags_9(int32_t value)
	{
		___m_Flags_9 = value;
	}

	inline static int32_t get_offset_of_m_OverridePath_10() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_OverridePath_10)); }
	inline String_t* get_m_OverridePath_10() const { return ___m_OverridePath_10; }
	inline String_t** get_address_of_m_OverridePath_10() { return &___m_OverridePath_10; }
	inline void set_m_OverridePath_10(String_t* value)
	{
		___m_OverridePath_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverridePath_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideInteractions_11() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_OverrideInteractions_11)); }
	inline String_t* get_m_OverrideInteractions_11() const { return ___m_OverrideInteractions_11; }
	inline String_t** get_address_of_m_OverrideInteractions_11() { return &___m_OverrideInteractions_11; }
	inline void set_m_OverrideInteractions_11(String_t* value)
	{
		___m_OverrideInteractions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideInteractions_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideProcessors_12() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_OverrideProcessors_12)); }
	inline String_t* get_m_OverrideProcessors_12() const { return ___m_OverrideProcessors_12; }
	inline String_t** get_address_of_m_OverrideProcessors_12() { return &___m_OverrideProcessors_12; }
	inline void set_m_OverrideProcessors_12(String_t* value)
	{
		___m_OverrideProcessors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideProcessors_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_Guid_13() { return static_cast<int32_t>(offsetof(InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687, ___m_Guid_13)); }
	inline Guid_t  get_m_Guid_13() const { return ___m_Guid_13; }
	inline Guid_t * get_address_of_m_Guid_13() { return &___m_Guid_13; }
	inline void set_m_Guid_13(Guid_t  value)
	{
		___m_Guid_13 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687_marshaled_pinvoke
{
	char* ___m_Name_2;
	char* ___m_Id_3;
	char* ___m_Path_4;
	char* ___m_Interactions_5;
	char* ___m_Processors_6;
	char* ___m_Groups_7;
	char* ___m_Action_8;
	int32_t ___m_Flags_9;
	char* ___m_OverridePath_10;
	char* ___m_OverrideInteractions_11;
	char* ___m_OverrideProcessors_12;
	Guid_t  ___m_Guid_13;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687_marshaled_com
{
	Il2CppChar* ___m_Name_2;
	Il2CppChar* ___m_Id_3;
	Il2CppChar* ___m_Path_4;
	Il2CppChar* ___m_Interactions_5;
	Il2CppChar* ___m_Processors_6;
	Il2CppChar* ___m_Groups_7;
	Il2CppChar* ___m_Action_8;
	int32_t ___m_Flags_9;
	Il2CppChar* ___m_OverridePath_10;
	Il2CppChar* ___m_OverrideInteractions_11;
	Il2CppChar* ___m_OverrideProcessors_12;
	Guid_t  ___m_Guid_13;
};

// UnityEngine.InputSystem.InputControlScheme_DeviceRequirement
struct  DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47 
{
public:
	// System.String UnityEngine.InputSystem.InputControlScheme_DeviceRequirement::m_ControlPath
	String_t* ___m_ControlPath_0;
	// UnityEngine.InputSystem.InputControlScheme_DeviceRequirement_Flags UnityEngine.InputSystem.InputControlScheme_DeviceRequirement::m_Flags
	int32_t ___m_Flags_1;

public:
	inline static int32_t get_offset_of_m_ControlPath_0() { return static_cast<int32_t>(offsetof(DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47, ___m_ControlPath_0)); }
	inline String_t* get_m_ControlPath_0() const { return ___m_ControlPath_0; }
	inline String_t** get_address_of_m_ControlPath_0() { return &___m_ControlPath_0; }
	inline void set_m_ControlPath_0(String_t* value)
	{
		___m_ControlPath_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ControlPath_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_1() { return static_cast<int32_t>(offsetof(DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47, ___m_Flags_1)); }
	inline int32_t get_m_Flags_1() const { return ___m_Flags_1; }
	inline int32_t* get_address_of_m_Flags_1() { return &___m_Flags_1; }
	inline void set_m_Flags_1(int32_t value)
	{
		___m_Flags_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputControlScheme/DeviceRequirement
struct DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_pinvoke
{
	char* ___m_ControlPath_0;
	int32_t ___m_Flags_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputControlScheme/DeviceRequirement
struct DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47_marshaled_com
{
	Il2CppChar* ___m_ControlPath_0;
	int32_t ___m_Flags_1;
};

// UnityEngine.InputSystem.Users.InputUser
struct  InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215 
{
public:
	// System.UInt32 UnityEngine.InputSystem.Users.InputUser::m_Id
	uint32_t ___m_Id_1;

public:
	inline static int32_t get_offset_of_m_Id_1() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215, ___m_Id_1)); }
	inline uint32_t get_m_Id_1() const { return ___m_Id_1; }
	inline uint32_t* get_address_of_m_Id_1() { return &___m_Id_1; }
	inline void set_m_Id_1(uint32_t value)
	{
		___m_Id_1 = value;
	}
};

struct InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields
{
public:
	// System.Int32 UnityEngine.InputSystem.Users.InputUser::s_PairingStateVersion
	int32_t ___s_PairingStateVersion_2;
	// System.UInt32 UnityEngine.InputSystem.Users.InputUser::s_LastUserId
	uint32_t ___s_LastUserId_3;
	// System.Int32 UnityEngine.InputSystem.Users.InputUser::s_AllUserCount
	int32_t ___s_AllUserCount_4;
	// System.Int32 UnityEngine.InputSystem.Users.InputUser::s_AllPairedDeviceCount
	int32_t ___s_AllPairedDeviceCount_5;
	// System.Int32 UnityEngine.InputSystem.Users.InputUser::s_AllLostDeviceCount
	int32_t ___s_AllLostDeviceCount_6;
	// UnityEngine.InputSystem.Users.InputUser[] UnityEngine.InputSystem.Users.InputUser::s_AllUsers
	InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___s_AllUsers_7;
	// UnityEngine.InputSystem.Users.InputUser_UserData[] UnityEngine.InputSystem.Users.InputUser::s_AllUserData
	UserDataU5BU5D_t75DF9B9AFCC647AE82A77C9757993356F20B3AA2* ___s_AllUserData_8;
	// UnityEngine.InputSystem.InputDevice[] UnityEngine.InputSystem.Users.InputUser::s_AllPairedDevices
	InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* ___s_AllPairedDevices_9;
	// UnityEngine.InputSystem.InputDevice[] UnityEngine.InputSystem.Users.InputUser::s_AllLostDevices
	InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* ___s_AllLostDevices_10;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.Users.InputUser_OngoingAccountSelection> UnityEngine.InputSystem.Users.InputUser::s_OngoingAccountSelections
	InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC  ___s_OngoingAccountSelections_11;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`3<UnityEngine.InputSystem.Users.InputUser,UnityEngine.InputSystem.Users.InputUserChange,UnityEngine.InputSystem.InputDevice>> UnityEngine.InputSystem.Users.InputUser::s_OnChange
	InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0  ___s_OnChange_12;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`2<UnityEngine.InputSystem.InputControl,UnityEngine.InputSystem.LowLevel.InputEventPtr>> UnityEngine.InputSystem.Users.InputUser::s_OnUnpairedDeviceUsed
	InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE  ___s_OnUnpairedDeviceUsed_13;
	// System.Action`2<System.Object,UnityEngine.InputSystem.InputActionChange> UnityEngine.InputSystem.Users.InputUser::s_ActionChangeDelegate
	Action_2_tE42A0F669301F0398965DF529367035B65C55784 * ___s_ActionChangeDelegate_14;
	// System.Action`2<UnityEngine.InputSystem.InputDevice,UnityEngine.InputSystem.InputDeviceChange> UnityEngine.InputSystem.Users.InputUser::s_OnDeviceChangeDelegate
	Action_2_t6749D553543AAD815E5099A7E737C5B7589CBD9A * ___s_OnDeviceChangeDelegate_15;
	// System.Action`2<UnityEngine.InputSystem.LowLevel.InputEventPtr,UnityEngine.InputSystem.InputDevice> UnityEngine.InputSystem.Users.InputUser::s_OnEventDelegate
	Action_2_tA4B1466FF5BDCCC806EB4E05EDAD6DF5FC9A5116 * ___s_OnEventDelegate_16;
	// System.Boolean UnityEngine.InputSystem.Users.InputUser::s_OnActionChangeHooked
	bool ___s_OnActionChangeHooked_17;
	// System.Boolean UnityEngine.InputSystem.Users.InputUser::s_OnDeviceChangeHooked
	bool ___s_OnDeviceChangeHooked_18;
	// System.Boolean UnityEngine.InputSystem.Users.InputUser::s_OnEventHooked
	bool ___s_OnEventHooked_19;
	// System.Int32 UnityEngine.InputSystem.Users.InputUser::s_ListenForUnpairedDeviceActivity
	int32_t ___s_ListenForUnpairedDeviceActivity_20;

public:
	inline static int32_t get_offset_of_s_PairingStateVersion_2() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_PairingStateVersion_2)); }
	inline int32_t get_s_PairingStateVersion_2() const { return ___s_PairingStateVersion_2; }
	inline int32_t* get_address_of_s_PairingStateVersion_2() { return &___s_PairingStateVersion_2; }
	inline void set_s_PairingStateVersion_2(int32_t value)
	{
		___s_PairingStateVersion_2 = value;
	}

	inline static int32_t get_offset_of_s_LastUserId_3() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_LastUserId_3)); }
	inline uint32_t get_s_LastUserId_3() const { return ___s_LastUserId_3; }
	inline uint32_t* get_address_of_s_LastUserId_3() { return &___s_LastUserId_3; }
	inline void set_s_LastUserId_3(uint32_t value)
	{
		___s_LastUserId_3 = value;
	}

	inline static int32_t get_offset_of_s_AllUserCount_4() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllUserCount_4)); }
	inline int32_t get_s_AllUserCount_4() const { return ___s_AllUserCount_4; }
	inline int32_t* get_address_of_s_AllUserCount_4() { return &___s_AllUserCount_4; }
	inline void set_s_AllUserCount_4(int32_t value)
	{
		___s_AllUserCount_4 = value;
	}

	inline static int32_t get_offset_of_s_AllPairedDeviceCount_5() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllPairedDeviceCount_5)); }
	inline int32_t get_s_AllPairedDeviceCount_5() const { return ___s_AllPairedDeviceCount_5; }
	inline int32_t* get_address_of_s_AllPairedDeviceCount_5() { return &___s_AllPairedDeviceCount_5; }
	inline void set_s_AllPairedDeviceCount_5(int32_t value)
	{
		___s_AllPairedDeviceCount_5 = value;
	}

	inline static int32_t get_offset_of_s_AllLostDeviceCount_6() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllLostDeviceCount_6)); }
	inline int32_t get_s_AllLostDeviceCount_6() const { return ___s_AllLostDeviceCount_6; }
	inline int32_t* get_address_of_s_AllLostDeviceCount_6() { return &___s_AllLostDeviceCount_6; }
	inline void set_s_AllLostDeviceCount_6(int32_t value)
	{
		___s_AllLostDeviceCount_6 = value;
	}

	inline static int32_t get_offset_of_s_AllUsers_7() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllUsers_7)); }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* get_s_AllUsers_7() const { return ___s_AllUsers_7; }
	inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D** get_address_of_s_AllUsers_7() { return &___s_AllUsers_7; }
	inline void set_s_AllUsers_7(InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* value)
	{
		___s_AllUsers_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AllUsers_7), (void*)value);
	}

	inline static int32_t get_offset_of_s_AllUserData_8() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllUserData_8)); }
	inline UserDataU5BU5D_t75DF9B9AFCC647AE82A77C9757993356F20B3AA2* get_s_AllUserData_8() const { return ___s_AllUserData_8; }
	inline UserDataU5BU5D_t75DF9B9AFCC647AE82A77C9757993356F20B3AA2** get_address_of_s_AllUserData_8() { return &___s_AllUserData_8; }
	inline void set_s_AllUserData_8(UserDataU5BU5D_t75DF9B9AFCC647AE82A77C9757993356F20B3AA2* value)
	{
		___s_AllUserData_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AllUserData_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_AllPairedDevices_9() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllPairedDevices_9)); }
	inline InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* get_s_AllPairedDevices_9() const { return ___s_AllPairedDevices_9; }
	inline InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3** get_address_of_s_AllPairedDevices_9() { return &___s_AllPairedDevices_9; }
	inline void set_s_AllPairedDevices_9(InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* value)
	{
		___s_AllPairedDevices_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AllPairedDevices_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_AllLostDevices_10() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_AllLostDevices_10)); }
	inline InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* get_s_AllLostDevices_10() const { return ___s_AllLostDevices_10; }
	inline InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3** get_address_of_s_AllLostDevices_10() { return &___s_AllLostDevices_10; }
	inline void set_s_AllLostDevices_10(InputDeviceU5BU5D_t76BD34EABDB58502864E1333BF6872F02BE520F3* value)
	{
		___s_AllLostDevices_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_AllLostDevices_10), (void*)value);
	}

	inline static int32_t get_offset_of_s_OngoingAccountSelections_11() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OngoingAccountSelections_11)); }
	inline InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC  get_s_OngoingAccountSelections_11() const { return ___s_OngoingAccountSelections_11; }
	inline InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC * get_address_of_s_OngoingAccountSelections_11() { return &___s_OngoingAccountSelections_11; }
	inline void set_s_OngoingAccountSelections_11(InlinedArray_1_t31495EE07DE310C11F4504D7E17A4C3D83CC96DC  value)
	{
		___s_OngoingAccountSelections_11 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___s_OngoingAccountSelections_11))->___firstValue_1))->___device_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OngoingAccountSelections_11))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_OnChange_12() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnChange_12)); }
	inline InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0  get_s_OnChange_12() const { return ___s_OnChange_12; }
	inline InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0 * get_address_of_s_OnChange_12() { return &___s_OnChange_12; }
	inline void set_s_OnChange_12(InlinedArray_1_t0B48058AD54DE55B59DB1B030327C3F38E481CA0  value)
	{
		___s_OnChange_12 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnChange_12))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnChange_12))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_OnUnpairedDeviceUsed_13() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnUnpairedDeviceUsed_13)); }
	inline InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE  get_s_OnUnpairedDeviceUsed_13() const { return ___s_OnUnpairedDeviceUsed_13; }
	inline InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE * get_address_of_s_OnUnpairedDeviceUsed_13() { return &___s_OnUnpairedDeviceUsed_13; }
	inline void set_s_OnUnpairedDeviceUsed_13(InlinedArray_1_tC77A6ED7B7D0D74B5EB2AEE8408A3769CDD570EE  value)
	{
		___s_OnUnpairedDeviceUsed_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnUnpairedDeviceUsed_13))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___s_OnUnpairedDeviceUsed_13))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_s_ActionChangeDelegate_14() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_ActionChangeDelegate_14)); }
	inline Action_2_tE42A0F669301F0398965DF529367035B65C55784 * get_s_ActionChangeDelegate_14() const { return ___s_ActionChangeDelegate_14; }
	inline Action_2_tE42A0F669301F0398965DF529367035B65C55784 ** get_address_of_s_ActionChangeDelegate_14() { return &___s_ActionChangeDelegate_14; }
	inline void set_s_ActionChangeDelegate_14(Action_2_tE42A0F669301F0398965DF529367035B65C55784 * value)
	{
		___s_ActionChangeDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ActionChangeDelegate_14), (void*)value);
	}

	inline static int32_t get_offset_of_s_OnDeviceChangeDelegate_15() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnDeviceChangeDelegate_15)); }
	inline Action_2_t6749D553543AAD815E5099A7E737C5B7589CBD9A * get_s_OnDeviceChangeDelegate_15() const { return ___s_OnDeviceChangeDelegate_15; }
	inline Action_2_t6749D553543AAD815E5099A7E737C5B7589CBD9A ** get_address_of_s_OnDeviceChangeDelegate_15() { return &___s_OnDeviceChangeDelegate_15; }
	inline void set_s_OnDeviceChangeDelegate_15(Action_2_t6749D553543AAD815E5099A7E737C5B7589CBD9A * value)
	{
		___s_OnDeviceChangeDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OnDeviceChangeDelegate_15), (void*)value);
	}

	inline static int32_t get_offset_of_s_OnEventDelegate_16() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnEventDelegate_16)); }
	inline Action_2_tA4B1466FF5BDCCC806EB4E05EDAD6DF5FC9A5116 * get_s_OnEventDelegate_16() const { return ___s_OnEventDelegate_16; }
	inline Action_2_tA4B1466FF5BDCCC806EB4E05EDAD6DF5FC9A5116 ** get_address_of_s_OnEventDelegate_16() { return &___s_OnEventDelegate_16; }
	inline void set_s_OnEventDelegate_16(Action_2_tA4B1466FF5BDCCC806EB4E05EDAD6DF5FC9A5116 * value)
	{
		___s_OnEventDelegate_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_OnEventDelegate_16), (void*)value);
	}

	inline static int32_t get_offset_of_s_OnActionChangeHooked_17() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnActionChangeHooked_17)); }
	inline bool get_s_OnActionChangeHooked_17() const { return ___s_OnActionChangeHooked_17; }
	inline bool* get_address_of_s_OnActionChangeHooked_17() { return &___s_OnActionChangeHooked_17; }
	inline void set_s_OnActionChangeHooked_17(bool value)
	{
		___s_OnActionChangeHooked_17 = value;
	}

	inline static int32_t get_offset_of_s_OnDeviceChangeHooked_18() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnDeviceChangeHooked_18)); }
	inline bool get_s_OnDeviceChangeHooked_18() const { return ___s_OnDeviceChangeHooked_18; }
	inline bool* get_address_of_s_OnDeviceChangeHooked_18() { return &___s_OnDeviceChangeHooked_18; }
	inline void set_s_OnDeviceChangeHooked_18(bool value)
	{
		___s_OnDeviceChangeHooked_18 = value;
	}

	inline static int32_t get_offset_of_s_OnEventHooked_19() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_OnEventHooked_19)); }
	inline bool get_s_OnEventHooked_19() const { return ___s_OnEventHooked_19; }
	inline bool* get_address_of_s_OnEventHooked_19() { return &___s_OnEventHooked_19; }
	inline void set_s_OnEventHooked_19(bool value)
	{
		___s_OnEventHooked_19 = value;
	}

	inline static int32_t get_offset_of_s_ListenForUnpairedDeviceActivity_20() { return static_cast<int32_t>(offsetof(InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215_StaticFields, ___s_ListenForUnpairedDeviceActivity_20)); }
	inline int32_t get_s_ListenForUnpairedDeviceActivity_20() const { return ___s_ListenForUnpairedDeviceActivity_20; }
	inline int32_t* get_address_of_s_ListenForUnpairedDeviceActivity_20() { return &___s_ListenForUnpairedDeviceActivity_20; }
	inline void set_s_ListenForUnpairedDeviceActivity_20(int32_t value)
	{
		___s_ListenForUnpairedDeviceActivity_20 = value;
	}
};


// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct  PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.IntegratedSubsystemDescriptor`1<System.Object>
struct  IntegratedSubsystemDescriptor_1_t26346DD8E0AD1C04F25B94E8AD18577ABA15EBCB  : public IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_pinvoke_define
#define IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_pinvoke_define
struct IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_pinvoke : public IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA_marshaled_pinvoke
{
};
#endif
// Native definition for COM marshalling of UnityEngine.IntegratedSubsystemDescriptor`1
#ifndef IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_com_define
#define IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_com_define
struct IntegratedSubsystemDescriptor_1_t6B91293AA752E7448406604457D5332168E7D867_marshaled_com : public IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA_marshaled_com
{
};
#endif

// UnityEngine.IntegratedSubsystem`1<System.Object>
struct  IntegratedSubsystem_1_tA39FA5C25840EB481167108B3E02F7F09E901583  : public IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026
{
public:

public:
};


// UnityEngine.UI.CoroutineTween.ColorTween
struct  ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 
{
public:
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenCallback UnityEngine.UI.CoroutineTween.ColorTween::m_Target
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_StartColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::m_TargetColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	// UnityEngine.UI.CoroutineTween.ColorTween_ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::m_TweenMode
	int32_t ___m_TweenMode_3;
	// System.Single UnityEngine.UI.CoroutineTween.ColorTween::m_Duration
	float ___m_Duration_4;
	// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_5;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Target_0)); }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * get_m_Target_0() const { return ___m_Target_0; }
	inline ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Target_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartColor_1() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_StartColor_1)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_StartColor_1() const { return ___m_StartColor_1; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_StartColor_1() { return &___m_StartColor_1; }
	inline void set_m_StartColor_1(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_StartColor_1 = value;
	}

	inline static int32_t get_offset_of_m_TargetColor_2() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TargetColor_2)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_m_TargetColor_2() const { return ___m_TargetColor_2; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_m_TargetColor_2() { return &___m_TargetColor_2; }
	inline void set_m_TargetColor_2(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___m_TargetColor_2 = value;
	}

	inline static int32_t get_offset_of_m_TweenMode_3() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_TweenMode_3)); }
	inline int32_t get_m_TweenMode_3() const { return ___m_TweenMode_3; }
	inline int32_t* get_address_of_m_TweenMode_3() { return &___m_TweenMode_3; }
	inline void set_m_TweenMode_3(int32_t value)
	{
		___m_TweenMode_3 = value;
	}

	inline static int32_t get_offset_of_m_Duration_4() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_Duration_4)); }
	inline float get_m_Duration_4() const { return ___m_Duration_4; }
	inline float* get_address_of_m_Duration_4() { return &___m_Duration_4; }
	inline void set_m_Duration_4(float value)
	{
		___m_Duration_4 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_5() { return static_cast<int32_t>(offsetof(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228, ___m_IgnoreTimeScale_5)); }
	inline bool get_m_IgnoreTimeScale_5() const { return ___m_IgnoreTimeScale_5; }
	inline bool* get_address_of_m_IgnoreTimeScale_5() { return &___m_IgnoreTimeScale_5; }
	inline void set_m_IgnoreTimeScale_5(bool value)
	{
		___m_IgnoreTimeScale_5 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_pinvoke
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};
// Native definition for COM marshalling of UnityEngine.UI.CoroutineTween.ColorTween
struct ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228_marshaled_com
{
	ColorTweenCallback_tA2357F5ECB0BB12F303C2D6EE5A628CFD14C91C0 * ___m_Target_0;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_StartColor_1;
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___m_TargetColor_2;
	int32_t ___m_TweenMode_3;
	float ___m_Duration_4;
	int32_t ___m_IgnoreTimeScale_5;
};

// System.ArgumentException
struct  ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.Comparison`1<System.Object>
struct  Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4  : public MulticastDelegate_t
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.NotImplementedException
struct  NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.Predicate`1<System.Object>
struct  Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>
struct  Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.InputBinding>
struct  Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>
struct  Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.InputControlScheme>
struct  Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>
struct  Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.Users.InputUser>
struct  Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.Utilities.InternedString>
struct  Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.Utilities.NameAndParameters>
struct  Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Int32>>
struct  UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<System.Object>>
struct  UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Color32>>
struct  UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>
struct  UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct  UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector3>>
struct  UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.Vector4>>
struct  UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Events.UnityAction`1<System.Object>
struct  UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem
struct  ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593 
{
public:
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<name>k__BackingField
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ___U3CnameU3Ek__BackingField_0;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<layout>k__BackingField
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ___U3ClayoutU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<variants>k__BackingField
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ___U3CvariantsU3Ek__BackingField_2;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<useStateFrom>k__BackingField
	String_t* ___U3CuseStateFromU3Ek__BackingField_3;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<displayName>k__BackingField
	String_t* ___U3CdisplayNameU3Ek__BackingField_4;
	// System.String UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<shortDisplayName>k__BackingField
	String_t* ___U3CshortDisplayNameU3Ek__BackingField_5;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<usages>k__BackingField
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CusagesU3Ek__BackingField_6;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString> UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<aliases>k__BackingField
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CaliasesU3Ek__BackingField_7;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue> UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<parameters>k__BackingField
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_8;
	// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters> UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<processors>k__BackingField
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  ___U3CprocessorsU3Ek__BackingField_9;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<offset>k__BackingField
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<bit>k__BackingField
	uint32_t ___U3CbitU3Ek__BackingField_11;
	// System.UInt32 UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<format>k__BackingField
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___U3CformatU3Ek__BackingField_13;
	// UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem_Flags UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<flags>k__BackingField
	int32_t ___U3CflagsU3Ek__BackingField_14;
	// System.Int32 UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<arraySize>k__BackingField
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<defaultState>k__BackingField
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  ___U3CdefaultStateU3Ek__BackingField_16;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<minValue>k__BackingField
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  ___U3CminValueU3Ek__BackingField_17;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem::<maxValue>k__BackingField
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  ___U3CmaxValueU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CnameU3Ek__BackingField_0)); }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 * get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3ClayoutU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3ClayoutU3Ek__BackingField_1)); }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  get_U3ClayoutU3Ek__BackingField_1() const { return ___U3ClayoutU3Ek__BackingField_1; }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 * get_address_of_U3ClayoutU3Ek__BackingField_1() { return &___U3ClayoutU3Ek__BackingField_1; }
	inline void set_U3ClayoutU3Ek__BackingField_1(InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  value)
	{
		___U3ClayoutU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CvariantsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CvariantsU3Ek__BackingField_2)); }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  get_U3CvariantsU3Ek__BackingField_2() const { return ___U3CvariantsU3Ek__BackingField_2; }
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 * get_address_of_U3CvariantsU3Ek__BackingField_2() { return &___U3CvariantsU3Ek__BackingField_2; }
	inline void set_U3CvariantsU3Ek__BackingField_2(InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  value)
	{
		___U3CvariantsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_U3CuseStateFromU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CuseStateFromU3Ek__BackingField_3)); }
	inline String_t* get_U3CuseStateFromU3Ek__BackingField_3() const { return ___U3CuseStateFromU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CuseStateFromU3Ek__BackingField_3() { return &___U3CuseStateFromU3Ek__BackingField_3; }
	inline void set_U3CuseStateFromU3Ek__BackingField_3(String_t* value)
	{
		___U3CuseStateFromU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CuseStateFromU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdisplayNameU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CdisplayNameU3Ek__BackingField_4)); }
	inline String_t* get_U3CdisplayNameU3Ek__BackingField_4() const { return ___U3CdisplayNameU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CdisplayNameU3Ek__BackingField_4() { return &___U3CdisplayNameU3Ek__BackingField_4; }
	inline void set_U3CdisplayNameU3Ek__BackingField_4(String_t* value)
	{
		___U3CdisplayNameU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdisplayNameU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CshortDisplayNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CshortDisplayNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CshortDisplayNameU3Ek__BackingField_5() const { return ___U3CshortDisplayNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CshortDisplayNameU3Ek__BackingField_5() { return &___U3CshortDisplayNameU3Ek__BackingField_5; }
	inline void set_U3CshortDisplayNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CshortDisplayNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CshortDisplayNameU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CusagesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CusagesU3Ek__BackingField_6)); }
	inline ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  get_U3CusagesU3Ek__BackingField_6() const { return ___U3CusagesU3Ek__BackingField_6; }
	inline ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * get_address_of_U3CusagesU3Ek__BackingField_6() { return &___U3CusagesU3Ek__BackingField_6; }
	inline void set_U3CusagesU3Ek__BackingField_6(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  value)
	{
		___U3CusagesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CaliasesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CaliasesU3Ek__BackingField_7)); }
	inline ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  get_U3CaliasesU3Ek__BackingField_7() const { return ___U3CaliasesU3Ek__BackingField_7; }
	inline ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * get_address_of_U3CaliasesU3Ek__BackingField_7() { return &___U3CaliasesU3Ek__BackingField_7; }
	inline void set_U3CaliasesU3Ek__BackingField_7(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  value)
	{
		___U3CaliasesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CparametersU3Ek__BackingField_8)); }
	inline ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  get_U3CparametersU3Ek__BackingField_8() const { return ___U3CparametersU3Ek__BackingField_8; }
	inline ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * get_address_of_U3CparametersU3Ek__BackingField_8() { return &___U3CparametersU3Ek__BackingField_8; }
	inline void set_U3CparametersU3Ek__BackingField_8(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  value)
	{
		___U3CparametersU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CprocessorsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CprocessorsU3Ek__BackingField_9)); }
	inline ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  get_U3CprocessorsU3Ek__BackingField_9() const { return ___U3CprocessorsU3Ek__BackingField_9; }
	inline ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * get_address_of_U3CprocessorsU3Ek__BackingField_9() { return &___U3CprocessorsU3Ek__BackingField_9; }
	inline void set_U3CprocessorsU3Ek__BackingField_9(ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  value)
	{
		___U3CprocessorsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CoffsetU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CoffsetU3Ek__BackingField_10)); }
	inline uint32_t get_U3CoffsetU3Ek__BackingField_10() const { return ___U3CoffsetU3Ek__BackingField_10; }
	inline uint32_t* get_address_of_U3CoffsetU3Ek__BackingField_10() { return &___U3CoffsetU3Ek__BackingField_10; }
	inline void set_U3CoffsetU3Ek__BackingField_10(uint32_t value)
	{
		___U3CoffsetU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CbitU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CbitU3Ek__BackingField_11)); }
	inline uint32_t get_U3CbitU3Ek__BackingField_11() const { return ___U3CbitU3Ek__BackingField_11; }
	inline uint32_t* get_address_of_U3CbitU3Ek__BackingField_11() { return &___U3CbitU3Ek__BackingField_11; }
	inline void set_U3CbitU3Ek__BackingField_11(uint32_t value)
	{
		___U3CbitU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CsizeInBitsU3Ek__BackingField_12)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_12() const { return ___U3CsizeInBitsU3Ek__BackingField_12; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_12() { return &___U3CsizeInBitsU3Ek__BackingField_12; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_12(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CformatU3Ek__BackingField_13)); }
	inline FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  get_U3CformatU3Ek__BackingField_13() const { return ___U3CformatU3Ek__BackingField_13; }
	inline FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1 * get_address_of_U3CformatU3Ek__BackingField_13() { return &___U3CformatU3Ek__BackingField_13; }
	inline void set_U3CformatU3Ek__BackingField_13(FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  value)
	{
		___U3CformatU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CflagsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CflagsU3Ek__BackingField_14)); }
	inline int32_t get_U3CflagsU3Ek__BackingField_14() const { return ___U3CflagsU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CflagsU3Ek__BackingField_14() { return &___U3CflagsU3Ek__BackingField_14; }
	inline void set_U3CflagsU3Ek__BackingField_14(int32_t value)
	{
		___U3CflagsU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CarraySizeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CarraySizeU3Ek__BackingField_15)); }
	inline int32_t get_U3CarraySizeU3Ek__BackingField_15() const { return ___U3CarraySizeU3Ek__BackingField_15; }
	inline int32_t* get_address_of_U3CarraySizeU3Ek__BackingField_15() { return &___U3CarraySizeU3Ek__BackingField_15; }
	inline void set_U3CarraySizeU3Ek__BackingField_15(int32_t value)
	{
		___U3CarraySizeU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CdefaultStateU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CdefaultStateU3Ek__BackingField_16)); }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  get_U3CdefaultStateU3Ek__BackingField_16() const { return ___U3CdefaultStateU3Ek__BackingField_16; }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4 * get_address_of_U3CdefaultStateU3Ek__BackingField_16() { return &___U3CdefaultStateU3Ek__BackingField_16; }
	inline void set_U3CdefaultStateU3Ek__BackingField_16(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  value)
	{
		___U3CdefaultStateU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CminValueU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CminValueU3Ek__BackingField_17)); }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  get_U3CminValueU3Ek__BackingField_17() const { return ___U3CminValueU3Ek__BackingField_17; }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4 * get_address_of_U3CminValueU3Ek__BackingField_17() { return &___U3CminValueU3Ek__BackingField_17; }
	inline void set_U3CminValueU3Ek__BackingField_17(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  value)
	{
		___U3CminValueU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CmaxValueU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593, ___U3CmaxValueU3Ek__BackingField_18)); }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  get_U3CmaxValueU3Ek__BackingField_18() const { return ___U3CmaxValueU3Ek__BackingField_18; }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4 * get_address_of_U3CmaxValueU3Ek__BackingField_18() { return &___U3CmaxValueU3Ek__BackingField_18; }
	inline void set_U3CmaxValueU3Ek__BackingField_18(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  value)
	{
		___U3CmaxValueU3Ek__BackingField_18 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem
struct ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593_marshaled_pinvoke
{
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_pinvoke ___U3CnameU3Ek__BackingField_0;
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_pinvoke ___U3ClayoutU3Ek__BackingField_1;
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_pinvoke ___U3CvariantsU3Ek__BackingField_2;
	char* ___U3CuseStateFromU3Ek__BackingField_3;
	char* ___U3CdisplayNameU3Ek__BackingField_4;
	char* ___U3CshortDisplayNameU3Ek__BackingField_5;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CusagesU3Ek__BackingField_6;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CaliasesU3Ek__BackingField_7;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_8;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  ___U3CprocessorsU3Ek__BackingField_9;
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	uint32_t ___U3CbitU3Ek__BackingField_11;
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___U3CformatU3Ek__BackingField_13;
	int32_t ___U3CflagsU3Ek__BackingField_14;
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_pinvoke ___U3CdefaultStateU3Ek__BackingField_16;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_pinvoke ___U3CminValueU3Ek__BackingField_17;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_pinvoke ___U3CmaxValueU3Ek__BackingField_18;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem
struct ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593_marshaled_com
{
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_com ___U3CnameU3Ek__BackingField_0;
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_com ___U3ClayoutU3Ek__BackingField_1;
	InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411_marshaled_com ___U3CvariantsU3Ek__BackingField_2;
	Il2CppChar* ___U3CuseStateFromU3Ek__BackingField_3;
	Il2CppChar* ___U3CdisplayNameU3Ek__BackingField_4;
	Il2CppChar* ___U3CshortDisplayNameU3Ek__BackingField_5;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CusagesU3Ek__BackingField_6;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ___U3CaliasesU3Ek__BackingField_7;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ___U3CparametersU3Ek__BackingField_8;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  ___U3CprocessorsU3Ek__BackingField_9;
	uint32_t ___U3CoffsetU3Ek__BackingField_10;
	uint32_t ___U3CbitU3Ek__BackingField_11;
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_12;
	FourCC_t733618C90CB68AF486F9D2F8565CE67EB4C0CCB1  ___U3CformatU3Ek__BackingField_13;
	int32_t ___U3CflagsU3Ek__BackingField_14;
	int32_t ___U3CarraySizeU3Ek__BackingField_15;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_com ___U3CdefaultStateU3Ek__BackingField_16;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_com ___U3CminValueU3Ek__BackingField_17;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_com ___U3CmaxValueU3Ek__BackingField_18;
};

// UnityEngine.InputSystem.Utilities.NamedValue
struct  NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.NamedValue::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_1;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.Utilities.NamedValue::<value>k__BackingField
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  ___U3CvalueU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2, ___U3CnameU3Ek__BackingField_1)); }
	inline String_t* get_U3CnameU3Ek__BackingField_1() const { return ___U3CnameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_1() { return &___U3CnameU3Ek__BackingField_1; }
	inline void set_U3CnameU3Ek__BackingField_1(String_t* value)
	{
		___U3CnameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CvalueU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2, ___U3CvalueU3Ek__BackingField_2)); }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  get_U3CvalueU3Ek__BackingField_2() const { return ___U3CvalueU3Ek__BackingField_2; }
	inline PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4 * get_address_of_U3CvalueU3Ek__BackingField_2() { return &___U3CvalueU3Ek__BackingField_2; }
	inline void set_U3CvalueU3Ek__BackingField_2(PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4  value)
	{
		___U3CvalueU3Ek__BackingField_2 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_pinvoke ___U3CvalueU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.NamedValue
struct NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_1;
	PrimitiveValue_tA68D6B5501EA1E70408173DED9A0EC334AEE11F4_marshaled_com ___U3CvalueU3Ek__BackingField_2;
};

// UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>
struct  U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB  : public RuntimeObject
{
public:
	// System.Int32 UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// T UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::tweenInfo
	ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo_2;
	// System.Single UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2::<elapsedTime>5__2
	float ___U3CelapsedTimeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_tweenInfo_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB, ___tweenInfo_2)); }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  get_tweenInfo_2() const { return ___tweenInfo_2; }
	inline ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * get_address_of_tweenInfo_2() { return &___tweenInfo_2; }
	inline void set_tweenInfo_2(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  value)
	{
		___tweenInfo_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___tweenInfo_2))->___m_Target_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_U3CelapsedTimeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB, ___U3CelapsedTimeU3E5__2_3)); }
	inline float get_U3CelapsedTimeU3E5__2_3() const { return ___U3CelapsedTimeU3E5__2_3; }
	inline float* get_address_of_U3CelapsedTimeU3E5__2_3() { return &___U3CelapsedTimeU3E5__2_3; }
	inline void set_U3CelapsedTimeU3E5__2_3(float value)
	{
		___U3CelapsedTimeU3E5__2_3 = value;
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct  ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.Predicate`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>
struct  Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9  : public MulticastDelegate_t
{
public:

public:
};


// System.Predicate`1<UnityEngine.InputSystem.Utilities.NamedValue>
struct  Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage[]
struct HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  m_Items[1];

public:
	inline HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.InputSystem.InputBinding[]
struct InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  m_Items[1];

public:
	inline InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}
	inline InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.InputControlScheme_DeviceRequirement[]
struct DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  m_Items[1];

public:
	inline DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_ControlPath_0), (void*)NULL);
	}
	inline DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_ControlPath_0), (void*)NULL);
	}
};
// UnityEngine.InputSystem.InputControlScheme[]
struct InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  m_Items[1];

public:
	inline InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_BindingGroup_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DeviceRequirements_2), (void*)NULL);
		#endif
	}
	inline InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_BindingGroup_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_DeviceRequirements_2), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem[]
struct ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  m_Items[1];

public:
	inline ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
	inline ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CnameU3Ek__BackingField_0))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3ClayoutU3Ek__BackingField_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringOriginalCase_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CvariantsU3Ek__BackingField_2))->___m_StringLowerCase_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CuseStateFromU3Ek__BackingField_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CdisplayNameU3Ek__BackingField_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CshortDisplayNameU3Ek__BackingField_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CusagesU3Ek__BackingField_6))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CaliasesU3Ek__BackingField_7))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_8))->___m_Array_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CprocessorsU3Ek__BackingField_9))->___m_Array_0), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo[]
struct DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  m_Items[1];

public:
	inline DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Layout_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_FullLayoutJson_4), (void*)NULL);
		#endif
	}
	inline DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_Layout_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_FullLayoutJson_4), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Users.InputUser[]
struct InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  m_Items[1];

public:
	inline InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.InputSystem.Utilities.InternedString[]
struct InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  m_Items[1];

public:
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.NameAndParameters[]
struct NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  m_Items[1];

public:
	inline NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
	inline NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((m_Items + index)->___U3CparametersU3Ek__BackingField_1))->___m_Array_0), (void*)NULL);
		#endif
	}
};
// UnityEngine.InputSystem.Utilities.NamedValue[]
struct NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  m_Items[1];

public:
	inline NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
	inline NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___U3CnameU3Ek__BackingField_1), (void*)NULL);
	}
};
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_gshared_inline (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_gshared_inline (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_gshared_inline (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_gshared_inline (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_gshared_inline (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_gshared_inline (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_gshared_inline (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_gshared_inline (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_gshared_inline (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * ___predicate0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Count()
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_gshared_inline (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, int32_t ___index0, const RuntimeMethod* method);

// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4 (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75 (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6 (RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method);
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::ToArray()
inline HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120 (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	return ((  HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120_gshared)(__this, method);
}
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_inline (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_gshared_inline)(__this, method);
}
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6 (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, const RuntimeMethod* method);
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport/HIDPageUsage>::get_Item(System.Int32)
inline HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  (*) (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790 (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81 (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::ToArray()
inline InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	return ((  InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_inline (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Item(System.Int32)
inline InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  (*) (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533 (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83 (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::ToArray()
inline DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	return ((  DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7 (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20 (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_inline (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme/DeviceRequirement>::get_Item(System.Int32)
inline DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226 (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  (*) (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*, const RuntimeMethod*))ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::ToArray()
inline InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	return ((  InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07 (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971 (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_inline (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Item(System.Int32)
inline InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  (*) (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0 (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*, const RuntimeMethod*))ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::ToArray()
inline ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302 (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	return ((  ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1 (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1 (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_inline (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout/ControlItem>::get_Item(System.Int32)
inline ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7 (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  (*) (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::ToArray()
inline DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1 (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	return ((  DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7 (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4 (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_inline (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace/DeviceInfo>::get_Item(System.Int32)
inline DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6 (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  (*) (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*, const RuntimeMethod*))ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::ToArray()
inline InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	return ((  InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_inline (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Item(System.Int32)
inline InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16 (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  (*) (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452 (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::ToArray()
inline InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9 (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	return ((  InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8 (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751 (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_inline (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Item(System.Int32)
inline InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  (*) (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*, const RuntimeMethod*))ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::ToArray()
inline NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	return ((  NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43 (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756 (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_inline (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Item(System.Int32)
inline NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  (*) (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[])
inline void ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3 (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*, const RuntimeMethod*))ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3_gshared)(__this, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[],System.Int32,System.Int32)
inline void ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371 (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	((  void (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*, int32_t, int32_t, const RuntimeMethod*))ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371_gshared)(__this, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::ToArray()
inline NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501 (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	return ((  NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, const RuntimeMethod*))ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::IndexOf(System.Predicate`1<TValue>)
inline int32_t ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * ___predicate0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 *, const RuntimeMethod*))ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_gshared)(__this, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, const RuntimeMethod*))ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD_gshared)(__this, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::System.Collections.IEnumerable.GetEnumerator()
inline RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, const RuntimeMethod*))ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE_gshared)(__this, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Count()
inline int32_t ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_inline (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, const RuntimeMethod*))ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_gshared_inline)(__this, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Item(System.Int32)
inline NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9 (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  (*) (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_gshared)(__this, ___index0, method);
}
// System.Void UnityEngine.IntegratedSubsystemDescriptor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntegratedSubsystemDescriptor__ctor_m1D87F86FF3A30C3ECCD95D1797802B34B9194039 (IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.IntegratedSubsystem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntegratedSubsystem__ctor_mDBF83DF7F1F0B6DB1C64DD2C585E8A0CC7EE0EF1 (IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026 * __this, const RuntimeMethod* method);
// System.Void System.NotImplementedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4 (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mD023A89A5C1F740F43F0A9CD6C49DC21230B3CEE (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ColorTween_ValidTarget_mCFF8428CFF8D45A85E4EE612263E751ED0B5987C (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool ColorTween_get_ignoreTimeScale_m6A06826E19314EFE9783E505C75CFC76E42E8F05_inline (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2 (const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ColorTween_get_duration_mC4A1E3C2EA46A5C657A2B9DA240C796F770ECC5F_inline (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B (float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ColorTween_TweenValue_m20FCBA50CE9328956973861A9CB0A1FD97265A58 (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33 (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::ValidTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool FloatTween_ValidTarget_m917EB0D30E72AC75D90D1D8F11B1D7EBBD00ECAE (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.UI.CoroutineTween.FloatTween::get_ignoreTimeScale()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FloatTween_get_ignoreTimeScale_mA7B69D72E1D52EF1890C89D5690D345B852C7239_inline (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Single UnityEngine.UI.CoroutineTween.FloatTween::get_duration()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FloatTween_get_duration_mBECFBEC57BDC30B54D0638873BA3313A8F7232F5_inline (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.CoroutineTween.FloatTween::TweenValue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FloatTween_TweenValue_m4E4418FB7FBDC7CBF96D95518DFACF25BCBE8EB3 (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, float ___floatPercentage0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_tAE7DB2FC70A0AE6477F896F852057CB0754F06EC * MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * G_B2_0 = NULL;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * G_B3_1 = NULL;
	{
		// m_Array = array;
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4_AdjustorThunk (RuntimeObject * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75_AdjustorThunk (RuntimeObject * __this, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m3B549EE815F7F7C2019450D62CE533C04F7FAE75(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_1 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)(HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_3 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m7529F1CA70B4D1F6B66F4F82B989F70A6CDF5120(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * L_2 = ___predicate0;
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_3 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB *, HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB *)L_2, (HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC_AdjustorThunk (RuntimeObject * __this, Predicate_1_t3928E9C83BA270E145EB7E3E5F6D8B2D3DEEABEB * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m507ECBDD14A8B2A0B091DDD2930BAE64735B26FC(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_0 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201 * L_3 = (Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_tA792BF020961A30C69C8C2A4819E73B36292C201 *, HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m154D3F5689B3AA8EE8971975E7DCE6620673EA3C((ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)(ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m29CA9F2E5AB8477FC9E0DFDC5CC561E8AD15F3EB(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8  ReadOnlyArray_1_op_Implicit_m02A1C178A3F76DAA85976BC5B261B0E4033C9A08_gshared (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_0 = ___array0;
		ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m1D67983CD552CD87C019063BE38CE2EDC4114FA4((&L_1), (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.HID.HIDSupport_HIDPageUsage>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_gshared (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_4 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93* L_6 = (HIDPageUsageU5BU5D_t3CAD2E433C621D9EB8BA2E941587592CACBFCE93*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  HIDPageUsage_t59B3E2363DE9D0713C5C7F42458EC53C441B60A9  ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_mCF90B88296AAB981C5B2C79452FAB5833BF1BEBE(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * G_B2_0 = NULL;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * G_B3_1 = NULL;
	{
		// m_Array = array;
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790_AdjustorThunk (RuntimeObject * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81_AdjustorThunk (RuntimeObject * __this, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m42FEC215E21437E79B26BE905FD154A6A339CD81(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_1 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)(InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_3 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m44344892C249C027419D19BE09F8C76C48AAB80E(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * L_2 = ___predicate0;
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_3 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 *, InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 *)L_2, (InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F_AdjustorThunk (RuntimeObject * __this, Predicate_1_t66B32FD352821F5FFE3D68C84C15EDA066608213 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m7A35EF285C59B14EB84CCC1B3C8BC781292BF50F(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_0 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908 * L_3 = (Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t863BA2B4E3063172A5588475CF49080DCD436908 *, InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m2E276036474355A33A43D693BD12EB61DF1FEF5D((ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)(ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mD42B2090FFC05AB6159594970F6EC4723873722A(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712  ReadOnlyArray_1_op_Implicit_mE98D6E976213D425ED2F1F9CB8C0A27A222304EF_gshared (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_0 = ___array0;
		ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m30ECE08996A9B0710CF11A20425D4659AE195790((&L_1), (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputBinding>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_gshared (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_4 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2* L_6 = (InputBindingU5BU5D_t65B6B33DB9C2C1C391D2AB4A68D3B29409AE9BF2*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  InputBinding_tD7A82C1BD9EB0CA7852114BB72DFF1C34525F687  ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m286482351565A145D72C46C0BE133E4932DF08CC(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * G_B2_0 = NULL;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * G_B3_1 = NULL;
	{
		// m_Array = array;
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533_AdjustorThunk (RuntimeObject * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83_AdjustorThunk (RuntimeObject * __this, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	ReadOnlyArray_1__ctor_m04A60BC9A74DC9A183CA7C3DB5C713BF104ABF83(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_1 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)(DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_3 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m8BE410A70EA27FDE0A7C0738EC6759EFA5DC9E3E(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * L_2 = ___predicate0;
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_3 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 *, DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 *)L_2, (DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7_AdjustorThunk (RuntimeObject * __this, Predicate_1_tC6545ED99C6C1B15764D949DA29163DCC95E3437 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m96D8D8A7F06548E82ECEBE6B66676B56EC053DA7(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_0 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D * L_3 = (Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t255E1EAB38A93FB425A1D2F153D6A146941BC26D *, DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m59D2872E092DFD5562A3451A71E76E02EC6C3F20((ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)(ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2D31FDDDDF9B55D1E51D0E46D1C528FAF23E7E4F(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF  ReadOnlyArray_1_op_Implicit_m451BEBD192162A2677A0C24D8115FBA20FA76729_gshared (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_0 = ___array0;
		ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m565A766AA6D0B086711F4332EA8E748E4C793533((&L_1), (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme_DeviceRequirement>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_gshared (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_4 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA* L_6 = (DeviceRequirementU5BU5D_t353BAB51DD4A2E83166636DA74FD92E67BC329FA*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  DeviceRequirement_t4AD8753789ED831847E156FD57066F4ED1960A47  ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m707FB938B9120819FBD137B59AF6F8580076C226(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * G_B2_0 = NULL;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * G_B3_1 = NULL;
	{
		// m_Array = array;
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B_AdjustorThunk (RuntimeObject * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA_AdjustorThunk (RuntimeObject * __this, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mF194A775C7EE7AE3A9F677AA90FCFD00F28E93DA(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_1 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)(InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_3 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m03273711040FD74A1AF303FD40F120097528D69F(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * L_2 = ___predicate0;
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_3 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 *, InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 *)L_2, (InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07_AdjustorThunk (RuntimeObject * __this, Predicate_1_t51781E6B6CD6E2BC4EAE010AF9C62826CE32F0A7 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m389EAF3415DB8CFE17BDA6B2EB467A96C051CD07(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_0 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF * L_3 = (Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t8A34F49BE44B92AF48CC1C259B74865F2F3D92FF *, InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m0A4A2DCBF050A58F8F2E7F30FEFF6A5DFC20109A((ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)(ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0DB01645A755A0F4535B216518DA64586AF14971(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999  ReadOnlyArray_1_op_Implicit_mC629994BB227E0958210CF9489DCC490CAA82D30_gshared (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_0 = ___array0;
		ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_mEB72B627EAAAAA3E99D1521B14E0ABE0B9C9E59B((&L_1), (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_gshared (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_4 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D* L_6 = (InputControlSchemeU5BU5D_t2F1701681727F2B535205DC1F7D694AF79DCB38D*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  InputControlScheme_t4D6BEBCAEBF57368080EACC53B4B2DB634D49431  ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_mEF97D5E63FB7A4E8E63DAAC68B0D876C31268C2C(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * G_B2_0 = NULL;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * G_B3_1 = NULL;
	{
		// m_Array = array;
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0_AdjustorThunk (RuntimeObject * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B_AdjustorThunk (RuntimeObject * __this, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mA095E6B46ACA80AA2352861B54E8252A3D946D4B(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_1 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)(ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_3 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_mA5019845C8C055087DA2DEB0DB22CBD8E5CD4302(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * L_2 = ___predicate0;
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_3 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 *, ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 *)L_2, (ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E_AdjustorThunk (RuntimeObject * __this, Predicate_1_t63405247A803CFAB05EB14B589E26174149D10B9 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m5938A7E2900B419FADB606289C399E6E8FC3803E(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_0 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0 * L_3 = (Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t9073091BD67C0472860C3422EFE5DE1CF7F01DE0 *, ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_mB1BC6272CA351F6D1D541CED9EBBF5D95AF55AB1((ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)(ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m0FC9BC6C90BAED7CCCA984CBAF6174328424D9C1(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6  ReadOnlyArray_1_op_Implicit_mD4E5CA3554C9A3C9D7BEFC60A14BA13DAE250139_gshared (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_0 = ___array0;
		ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_mEB33EC1265056A15F0B73486741B5EA103A8A6E0((&L_1), (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Layouts.InputControlLayout_ControlItem>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_gshared (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_4 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE* L_6 = (ControlItemU5BU5D_tDC20B5314479430812B30DE102E217F38E9A0EAE*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  ControlItem_t4E5AC7084EC5D6BEE484D26AEF7ED2656A85E593  ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m2D2F5549E9C4CEBE5B40CA90E88E9F043901D6D7(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * G_B2_0 = NULL;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * G_B3_1 = NULL;
	{
		// m_Array = array;
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD_AdjustorThunk (RuntimeObject * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C_AdjustorThunk (RuntimeObject * __this, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	ReadOnlyArray_1__ctor_m0C6E58647D418ED7B2C7C6C15606FA035E9B989C(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_1 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)(DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_3 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m43E0C11EDF1CF15F8F1A8478A04810B3C267ABE1(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * L_2 = ___predicate0;
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_3 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 *, DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 *)L_2, (DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7_AdjustorThunk (RuntimeObject * __this, Predicate_1_t73BD27BE5B3D203E591E10495DF42DC08AE98AA2 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m488AC247EE2EF31B520E98F2B6AB18F38B4B1FD7(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_0 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142 * L_3 = (Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t2F7E481068DDB252D136B51DED50FD9D6988E142 *, DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_mF211E846E4BAA44C42FDC866A28B157428FD08E4((ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)(ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mA9CFF117F0849B23A6B02F200CF315BA83A7028F(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD  ReadOnlyArray_1_op_Implicit_mBB72887939E425362F0172955B9CDCA0A5089058_gshared (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_0 = ___array0;
		ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m4BFB00351A873836E183864A89164E5AE8A949CD((&L_1), (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.LowLevel.InputEventTrace_DeviceInfo>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_gshared (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_4 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A* L_6 = (DeviceInfoU5BU5D_t1A30718BC3AE3C3E99845F1522402D7FC090AC3A*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  DeviceInfo_t29203AEABE1145D4F9C62B8E50777EF7289D6D46  ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_mD55FD89AAF014F302962F431FF95D76E481840F6(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * G_B2_0 = NULL;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * G_B3_1 = NULL;
	{
		// m_Array = array;
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978_AdjustorThunk (RuntimeObject * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62_AdjustorThunk (RuntimeObject * __this, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	ReadOnlyArray_1__ctor_m4005489F30C66BA8D589C58367D74030DDABED62(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_1 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)(InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_3 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_mCB5BDC7DC62E800764EF7A480AC2B7B254EA1358(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * L_2 = ___predicate0;
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_3 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 *, InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 *)L_2, (InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF_AdjustorThunk (RuntimeObject * __this, Predicate_1_t4D9B496168B96CA35BB3F0AC61442AB5A9E04C20 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m99F75C5EE18B1BEFF553CF848686270F62BCD1FF(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_0 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4 * L_3 = (Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_tA3359FD871526479EB2C1E2FA2E409285B7222A4 *, InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m7CB2AFB917EB57687C63BCAE2FD2D9E7982ADB94((ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)(ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mB03F7D3EEA8797C7C0512D98D486DFB42C24BFF4(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D  ReadOnlyArray_1_op_Implicit_m8F5F5EC56158EC54ED413624042040E1C6CBDE8F_gshared (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_0 = ___array0;
		ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_mC7AF93F20D20507C9636D72320DFA360A1DBF978((&L_1), (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Users.InputUser>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_gshared (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_4 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D* L_6 = (InputUserU5BU5D_t2CF99D4463379A317577A4E254983F39816E261D*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  InputUser_t1798DBDF8427D2F7E3E40343ABEF107D43810215  ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m6FD9171166B564CE089C66542A985D7DB3B65E16(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * G_B2_0 = NULL;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * G_B3_1 = NULL;
	{
		// m_Array = array;
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB_AdjustorThunk (RuntimeObject * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452_AdjustorThunk (RuntimeObject * __this, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	ReadOnlyArray_1__ctor_m420E415FA81F26B42D421B781838B82CB48B4452(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_1 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)(InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_3 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m8207BA5996DE74915C46B0B754FC960D712FBBD9(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * L_2 = ___predicate0;
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_3 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC *, InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC *)L_2, (InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8_AdjustorThunk (RuntimeObject * __this, Predicate_1_t6CA50813A0FD3BE19F8BF7CD63C172D72E39B6FC * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m4CD744D54320F4BCBCAC0BE4CE6AA2A4C25B01E8(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_0 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C * L_3 = (Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t0B3E868314D856E9C9B8441906022FC98CC4129C *, InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m0F541B0C5F68B1586862B360D07A44FC5A91DD9F((ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)(ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m41BBC998D4C9665E385DEAA8FC8CB9D9EF6DE751(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  ReadOnlyArray_1_op_Implicit_m43BEF47D6BB9F26DBC4D97FE6B680721241133C7_gshared (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_0 = ___array0;
		ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m922D635B840D8F093EBB4ECA927E9D3FBA8A32CB((&L_1), (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.InternedString>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_gshared (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_4 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79* L_6 = (InternedStringU5BU5D_t8F1FDF2C57B3120E7EF01CE792F826A8F2C86E79*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  InternedString_t91A97FDAF134BE01A8DE443C5C34C0605C8A4411  ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m933EC276D73FED1DB5A7730C3A78FC3DA4FE027B(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * G_B2_0 = NULL;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * G_B3_1 = NULL;
	{
		// m_Array = array;
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A_AdjustorThunk (RuntimeObject * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B_AdjustorThunk (RuntimeObject * __this, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	ReadOnlyArray_1__ctor_m9112115B15108B3418F3EF398521F5487B49340B(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_1 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)(NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_3 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_m57FC0CFA2787A3DC6CBF53C60899F91457BBA2DC(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * L_2 = ___predicate0;
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_3 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 *, NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 *)L_2, (NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F_AdjustorThunk (RuntimeObject * __this, Predicate_1_t5B6C4DDB8A6FCC5F2F0E0269AE4F57BF11440C04 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_m94E924AEF67CBE08356D391811A269377E9E4F4F(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_0 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A * L_3 = (Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t47DAC9FB5735BF92094F1051DA87FE04F5732B8A *, NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_mB4D48C84A68C770D8E2546EF5389959908226A43((ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)(ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_m2DC34C46B55011838237D4FCEE43C9B384312756(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  ReadOnlyArray_1_op_Implicit_m9427E88B4AA71C2A3AFFF8D7A600A964DAEA2086_gshared (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_0 = ___array0;
		ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_m79DEB572A8F3D59A3F324C422CB3B2F85F82A10A((&L_1), (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NameAndParameters>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_gshared (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_4 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7* L_6 = (NameAndParametersU5BU5D_t26338DFE7B19B6F9972DF04A284F3B18426DA0E7*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  NameAndParameters_t551621A9B5056A5698598F685ECFEA3C746B3CC6  ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m9A7A303B369B4AA068CF5F2A7CE3F7DD0911CBDA(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, const RuntimeMethod* method)
{
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * G_B2_0 = NULL;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * G_B3_1 = NULL;
	{
		// m_Array = array;
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = 0;
		__this->set_m_StartIndex_1(0);
		// m_Length = array?.Length ?? 0;
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_1 = ___array0;
		G_B1_0 = ((ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)(__this));
		if (L_1)
		{
			G_B2_0 = ((ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)(__this));
			goto IL_0015;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = ((ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)(G_B1_0));
		goto IL_0018;
	}

IL_0015:
	{
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_2 = ___array0;
		NullCheck(L_2);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length))));
		G_B3_1 = ((ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)(G_B2_0));
	}

IL_0018:
	{
		G_B3_1->set_m_Length_2(G_B3_0);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3_AdjustorThunk (RuntimeObject * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3(_thisAdjusted, ___array0, method);
}
// System.Void UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::.ctor(TValue[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	{
		// m_Array = array;
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_0 = ___array0;
		__this->set_m_Array_0(L_0);
		// m_StartIndex = index;
		int32_t L_1 = ___index1;
		__this->set_m_StartIndex_1(L_1);
		// m_Length = length;
		int32_t L_2 = ___length2;
		__this->set_m_Length_2(L_2);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371_AdjustorThunk (RuntimeObject * __this, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	ReadOnlyArray_1__ctor_mDC638619744FBBF8F3BFD46B57100CF349FCC371(_thisAdjusted, ___array0, ___index1, ___length2, method);
}
// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* V_0 = NULL;
	{
		// var result = new TValue[m_Length];
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_1 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)(NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)SZArrayNew(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0), (uint32_t)L_0);
		V_0 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)L_1;
		// if (m_Length > 0)
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// Array.Copy(m_Array, m_StartIndex, result, 0, m_Length);
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_3 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_5 = V_0;
		int32_t L_6 = (int32_t)__this->get_m_Length_2();
		Array_Copy_mA10D079DD8D9700CA44721A219A934A2397653F6((RuntimeArray *)(RuntimeArray *)L_3, (int32_t)L_4, (RuntimeArray *)(RuntimeArray *)L_5, (int32_t)0, (int32_t)L_6, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// return result;
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_7 = V_0;
		return L_7;
	}
}
IL2CPP_EXTERN_C  NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_ToArray_mF2239948D669260EA6C7408E9DFAD4643ED58501(_thisAdjusted, method);
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::IndexOf(System.Predicate`1<TValue>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * ___predicate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (predicate == null)
		Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * L_0 = ___predicate0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// throw new ArgumentNullException(nameof(predicate));
		ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD * L_1 = (ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD *)il2cpp_codegen_object_new(ArgumentNullException_t581DF992B1F3E0EC6EFB30CC5DC43519A79B27AD_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_mEE0C0D6FCB2D08CD7967DBB1329A0854BBED49ED(L_1, (String_t*)_stringLiteral04444310B8C9D216A6BC1D1CC9542ECC75BC02DF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_RuntimeMethod_var);
	}

IL_000e:
	{
		// for (var i = 0; i < m_Length; ++i)
		V_0 = (int32_t)0;
		goto IL_0033;
	}

IL_0012:
	{
		// if (predicate(m_Array[m_StartIndex + i]))
		Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * L_2 = ___predicate0;
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_3 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)__this->get_m_Array_0();
		int32_t L_4 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_5 = V_0;
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)L_5));
		NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		NullCheck((Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 *)L_2);
		bool L_8 = ((  bool (*) (Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 *, NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)((Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 *)L_2, (NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2 )L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		if (!L_8)
		{
			goto IL_002f;
		}
	}
	{
		// return i;
		int32_t L_9 = V_0;
		return L_9;
	}

IL_002f:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_10 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0033:
	{
		// for (var i = 0; i < m_Length; ++i)
		int32_t L_11 = V_0;
		int32_t L_12 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_11) < ((int32_t)L_12)))
		{
			goto IL_0012;
		}
	}
	{
		// return -1;
		return (-1);
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF_AdjustorThunk (RuntimeObject * __this, Predicate_1_tC6FDE3B0318647C8970225BA321F49A9295455D5 * ___predicate0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_IndexOf_mD710E32EC735171A34BE4B06998FF6EC46059CAF(_thisAdjusted, ___predicate0, method);
}
// System.Collections.Generic.IEnumerator`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	{
		// return new Enumerator<TValue>(m_Array, m_StartIndex, m_Length);
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_0 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)__this->get_m_Array_0();
		int32_t L_1 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008 * L_3 = (Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		((  void (*) (Enumerator_1_t147CA29C463418F2672609C940732F0CD8319008 *, NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)(L_3, (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		return L_3;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD(_thisAdjusted, method);
}
// System.Collections.IEnumerator UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0 = ReadOnlyArray_1_GetEnumerator_m81DAF920295EB3E6D8F3E731916F9E85319178CD((ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)(ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4));
		return L_0;
	}
}
IL2CPP_EXTERN_C  RuntimeObject* ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_System_Collections_IEnumerable_GetEnumerator_mBDEA9D0746150BA7AA0B6E324E2726ED42BF8DDE(_thisAdjusted, method);
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<TValue> UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::op_Implicit(TValue[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  ReadOnlyArray_1_op_Implicit_mCA46919D94C9C9D05BB9D21DF0F788A6C1D635D4_gshared (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* ___array0, const RuntimeMethod* method)
{
	{
		// return new ReadOnlyArray<TValue>(array);
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_0 = ___array0;
		ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9  L_1;
		memset((&L_1), 0, sizeof(L_1));
		ReadOnlyArray_1__ctor_mB85AC81B7580EEE14BD592100B3F4511BFD111E3((&L_1), (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		return L_1;
	}
}
// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C  int32_t ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_inline(_thisAdjusted, method);
}
// TValue UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.Utilities.NamedValue>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_gshared (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (index < 0 || index >= m_Length)
		int32_t L_0 = ___index0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = ___index0;
		int32_t L_2 = (int32_t)__this->get_m_Length_2();
		if ((((int32_t)L_1) < ((int32_t)L_2)))
		{
			goto IL_0018;
		}
	}

IL_000d:
	{
		// throw new ArgumentOutOfRangeException(nameof(index));
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_3 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_3, (String_t*)_stringLiteralE540CDD1328B2B21E29A95405C301B9313B7C346, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_RuntimeMethod_var);
	}

IL_0018:
	{
		// if (m_Array == null)
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_4 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)__this->get_m_Array_0();
		if (L_4)
		{
			goto IL_0026;
		}
	}
	{
		// throw new InvalidOperationException();
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_5 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1F94EA1226068BD1B7EAA1B836A59C99979F579E(L_5, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5, ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_RuntimeMethod_var);
	}

IL_0026:
	{
		// return m_Array[m_StartIndex + index];
		NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE* L_6 = (NamedValueU5BU5D_tB07A869DAC4AE9EAF1D4D2DE26F0044178B5DAFE*)__this->get_m_Array_0();
		int32_t L_7 = (int32_t)__this->get_m_StartIndex_1();
		int32_t L_8 = ___index0;
		NullCheck(L_6);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8));
		NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  L_10 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}
}
IL2CPP_EXTERN_C  NamedValue_t20B95A4ED92444C17BD88A350B19A8B1396C77D2  ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * _thisAdjusted = reinterpret_cast<ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 *>(__this + _offset);
	return ReadOnlyArray_1_get_Item_m9BAD0839C9110667ADD7920AF92431AB2620B4A9(_thisAdjusted, ___index0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.IntegratedSubsystemDescriptor`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntegratedSubsystemDescriptor_1__ctor_m3E14A32CB73A1C21C69CC9860EE2AE95F841876D_gshared (IntegratedSubsystemDescriptor_1_t26346DD8E0AD1C04F25B94E8AD18577ABA15EBCB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA *)__this);
		IntegratedSubsystemDescriptor__ctor_m1D87F86FF3A30C3ECCD95D1797802B34B9194039((IntegratedSubsystemDescriptor_t56BB69721C25889FFD6A9FE635ED05BB94D683DA *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.IntegratedSubsystem`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IntegratedSubsystem_1__ctor_m4F49ABCD91074575D743F3AA68170017E2A29B37_gshared (IntegratedSubsystem_1_tA39FA5C25840EB481167108B3E02F7F09E901583 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026 *)__this);
		IntegratedSubsystem__ctor_mDBF83DF7F1F0B6DB1C64DD2C585E8A0CC7EE0EF1((IntegratedSubsystem_tEFE71989A825ABA8955C1B1505C8F2405FA61026 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_Add_m4B9B1BB9F9CE9F3EA16E39E17D429BDE8F12781C_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		// m_List.Add(item);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		RuntimeObject * L_1 = ___item0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// m_Dictionary.Add(item, m_List.Count - 1);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_2 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_3 = ___item0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_4 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4);
		int32_t L_5 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_2);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_2, (RuntimeObject *)L_3, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// }
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::AddUnique(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IndexedSet_1_AddUnique_m1ECFE9938B5A00144BDA633A8D25A58316124761_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		// if (m_Dictionary.ContainsKey(item))
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_0 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		if (!L_2)
		{
			goto IL_0010;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0010:
	{
		// m_List.Add(item);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_3 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		RuntimeObject * L_4 = ___item0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		// m_Dictionary.Add(item, m_List.Count - 1);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_5 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_6 = ___item0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_7 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7);
		int32_t L_8 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_5);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_5, (RuntimeObject *)L_6, (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_8, (int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// return true;
		return (bool)1;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IndexedSet_1_Remove_m658717377F61EDE4C423765165C7C5FF30773E7B_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// int index = -1;
		V_0 = (int32_t)(-1);
		// if (!m_Dictionary.TryGetValue(item, out index))
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_0 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0, (RuntimeObject *)L_1, (int32_t*)(int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0014:
	{
		// RemoveAt(index);
		int32_t L_3 = V_0;
		NullCheck((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this);
		((  void (*) (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		// return true;
		return (bool)1;
	}
}
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* IndexedSet_1_GetEnumerator_mC16C0BE13B73DB47882CA51DB581747C19C7128B_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_GetEnumerator_mC16C0BE13B73DB47882CA51DB581747C19C7128B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// throw new System.NotImplementedException();
		NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 * L_0 = (NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4 *)il2cpp_codegen_object_new(NotImplementedException_t8AD6EBE5FEDB0AEBECEE0961CF73C35B372EFFA4_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m8BEA657E260FC05F0C6D2C43A6E9BC08040F59C4(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, IndexedSet_1_GetEnumerator_mC16C0BE13B73DB47882CA51DB581747C19C7128B_RuntimeMethod_var);
	}
}
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m81FDC80B01BFAD2E467F3F89BE79542567FC452C_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		NullCheck((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		return L_0;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_Clear_m0CE62BD950A168D1D01E16EE1DCBCDDC5C416F73_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	{
		// m_List.Clear();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		// m_Dictionary.Clear();
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_1 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_1);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		// }
		return;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IndexedSet_1_Contains_mB738528F8692F630112FD2539CE74D7B43AF4962_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	{
		// return m_Dictionary.ContainsKey(item);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_0 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0, (RuntimeObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_2;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_CopyTo_mD9F0ECDF20B494C1A493E939FE91D89AB3E91B84_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___array0, int32_t ___arrayIndex1, const RuntimeMethod* method)
{
	{
		// m_List.CopyTo(array, arrayIndex);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* L_1 = ___array0;
		int32_t L_2 = ___arrayIndex1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A*)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		// }
		return;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IndexedSet_1_get_Count_mEEA1184D1474B3B4B357EFC2C064908980921A1B_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	{
		// public int Count { get { return m_List.Count; } }
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		int32_t L_1 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return L_1;
	}
}
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IndexedSet_1_get_IsReadOnly_m29F668C4BDD834E41FEA84BBF4D1A1D6950D8BE7_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	{
		// public bool IsReadOnly { get { return false; } }
		return (bool)0;
	}
}
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IndexedSet_1_IndexOf_m89959972344E25AA8A0059BEE3F9F79A1D549C4E_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, RuntimeObject * ___item0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// int index = -1;
		V_0 = (int32_t)(-1);
		// if (m_Dictionary.TryGetValue(item, out index))
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_0 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_1 = ___item0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0);
		bool L_2 = ((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_0, (RuntimeObject *)L_1, (int32_t*)(int32_t*)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		if (!L_2)
		{
			goto IL_0014;
		}
	}
	{
		// return index;
		int32_t L_3 = V_0;
		return L_3;
	}

IL_0014:
	{
		// return -1;
		return (-1);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_Insert_mB43CB3AA6D461E94217E51DE6DD4ED3F5E1607CD_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, int32_t ___index0, RuntimeObject * ___item1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (IndexedSet_1_Insert_mB43CB3AA6D461E94217E51DE6DD4ED3F5E1607CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// throw new NotSupportedException("Random Insertion is semantically invalid, since this structure does not guarantee ordering.");
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mD023A89A5C1F740F43F0A9CD6C49DC21230B3CEE(L_0, (String_t*)_stringLiteralDFADDFA065E9D92CFE8B8F06444DD5652DFC1D58, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, IndexedSet_1_Insert_mB43CB3AA6D461E94217E51DE6DD4ED3F5E1607CD_RuntimeMethod_var);
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_RemoveAt_m5EA6CD03F4AEE2C8CDFA09F397C8510E23372D85_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, int32_t ___index0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	RuntimeObject * V_2 = NULL;
	{
		// T item = m_List[index];
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_0 = (RuntimeObject *)L_2;
		// m_Dictionary.Remove(item);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_3 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_4 = V_0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_3);
		((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// if (index == m_List.Count - 1)
		int32_t L_5 = ___index0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_6 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6);
		int32_t L_7 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		if ((!(((uint32_t)L_5) == ((uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_7, (int32_t)1))))))
		{
			goto IL_0037;
		}
	}
	{
		// m_List.RemoveAt(index);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_8 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_9 = ___index0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		return;
	}

IL_0037:
	{
		// int replaceItemIndex = m_List.Count - 1;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_1 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)1));
		// T replaceItem = m_List[replaceItemIndex];
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_12 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_13 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12);
		RuntimeObject * L_14 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_12, (int32_t)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_2 = (RuntimeObject *)L_14;
		// m_List[index] = replaceItem;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_15 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_16 = ___index0;
		RuntimeObject * L_17 = V_2;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_15, (int32_t)L_16, (RuntimeObject *)L_17, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		// m_Dictionary[replaceItem] = index;
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_18 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_19 = V_2;
		int32_t L_20 = ___index0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_18);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_18, (RuntimeObject *)L_19, (int32_t)L_20, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		// m_List.RemoveAt(replaceItemIndex);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_21 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_22 = V_1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_21);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_21, (int32_t)L_22, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		// }
		return;
	}
}
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * IndexedSet_1_get_Item_m8038D1362CB76F6CE791721319823B86D9DD2797_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		// get { return m_List[index]; }
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_2;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_set_Item_m382EAB9C2EBB858128026BE1AF5BD60A06A836A6_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, int32_t ___index0, RuntimeObject * ___value1, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		// T item = m_List[index];
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_1 = ___index0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_0 = (RuntimeObject *)L_2;
		// m_Dictionary.Remove(item);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_3 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_4 = V_0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_3);
		((  bool (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// m_List[index] = value;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_5 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_6 = ___index0;
		RuntimeObject * L_7 = ___value1;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_5, (int32_t)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		// m_Dictionary.Add(item, index);
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_8 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_9 = V_0;
		int32_t L_10 = ___index0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_8);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_8, (RuntimeObject *)L_9, (int32_t)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_RemoveAll_mE8AD13F9BC00551D303C9301BCA161E2E291D395_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___match0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		// int i = 0;
		V_0 = (int32_t)0;
		goto IL_0028;
	}

IL_0004:
	{
		// T item = m_List[i];
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_1 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_1 = (RuntimeObject *)L_2;
		// if (match(item))
		Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * L_3 = ___match0;
		RuntimeObject * L_4 = V_1;
		NullCheck((Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 *)L_3);
		bool L_5 = ((  bool (*) (Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)((Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 *)L_3, (RuntimeObject *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		if (!L_5)
		{
			goto IL_0024;
		}
	}
	{
		// Remove(item);
		RuntimeObject * L_6 = V_1;
		NullCheck((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this);
		((  bool (*) (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)->methodPointer)((IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C *)__this, (RuntimeObject *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16));
		goto IL_0028;
	}

IL_0024:
	{
		// i++;
		int32_t L_7 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_0028:
	{
		// while (i < m_List.Count)
		int32_t L_8 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_9 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_9);
		int32_t L_10 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		if ((((int32_t)L_8) < ((int32_t)L_10)))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1_Sort_mA748E0136CD04139BF95AF1EFE828776AA2FA9D4_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4 * ___sortLayoutFunction0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		// m_List.Sort(sortLayoutFunction);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4 * L_1 = ___sortLayoutFunction0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, (Comparison_1_tD9DBDF7B2E4774B4D35E113A76D75828A24641F4 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 17));
		// for (int i = 0; i < m_List.Count; ++i)
		V_0 = (int32_t)0;
		goto IL_002e;
	}

IL_0010:
	{
		// T item = m_List[i];
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_2 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		int32_t L_3 = V_0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2);
		RuntimeObject * L_4 = ((  RuntimeObject * (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_2, (int32_t)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		V_1 = (RuntimeObject *)L_4;
		// m_Dictionary[item] = i;
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_5 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)__this->get_m_Dictionary_1();
		RuntimeObject * L_6 = V_1;
		int32_t L_7 = V_0;
		NullCheck((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_5);
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14)->methodPointer)((Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)L_5, (RuntimeObject *)L_6, (int32_t)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 14));
		// for (int i = 0; i < m_List.Count; ++i)
		int32_t L_8 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_002e:
	{
		// for (int i = 0; i < m_List.Count; ++i)
		int32_t L_9 = V_0;
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_10 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)__this->get_m_List_0();
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10);
		int32_t L_11 = ((  int32_t (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		if ((((int32_t)L_9) < ((int32_t)L_11)))
		{
			goto IL_0010;
		}
	}
	{
		// }
		return;
	}
}
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IndexedSet_1__ctor_m78A15E3E0B20E8E78B6D3502F8CA7EE753AD0818_gshared (IndexedSet_1_t240606B62D2B3D9912AED248655C0479DD5BDE0C * __this, const RuntimeMethod* method)
{
	{
		// readonly List<T> m_List = new List<T>();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 18));
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		__this->set_m_List_0(L_0);
		// Dictionary<T, int> m_Dictionary = new Dictionary<T, int>();
		Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A * L_1 = (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 20));
		((  void (*) (Dictionary_2_t81923CE2A312318AE13F58085CCF7FA8D879B77A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		__this->set_m_Dictionary_1(L_1);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2__ctor_mF6B911DFD8723188732C99D4142571A0B6E851E3_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_IDisposable_Dispose_mA7325546FC25696F84C8ACD61A9D18859C95CEA6_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ed__2_MoveNext_m1F9733709DEC4B352BBFD9939F2B27D0A01137AF_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__2_MoveNext_m1F9733709DEC4B352BBFD9939F2B27D0A01137AF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float G_B8_0 = 0.0f;
	U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * G_B8_1 = NULL;
	float G_B7_0 = 0.0f;
	U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * G_B9_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00a8;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (!tweenInfo.ValidTarget())
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_3 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		bool L_4 = ColorTween_ValidTarget_mCFF8428CFF8D45A85E4EE612263E751ED0B5987C((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_002f:
	{
		// var elapsedTime = 0.0f;
		__this->set_U3CelapsedTimeU3E5__2_3((0.0f));
		goto IL_00af;
	}

IL_003c:
	{
		// elapsedTime += tweenInfo.ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
		float L_5 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_6 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		bool L_7 = ColorTween_get_ignoreTimeScale_m6A06826E19314EFE9783E505C75CFC76E42E8F05_inline((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_6, /*hidden argument*/NULL);
		G_B7_0 = L_5;
		G_B7_1 = ((U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)(__this));
		if (L_7)
		{
			G_B8_0 = L_5;
			G_B8_1 = ((U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)(__this));
			goto IL_005d;
		}
	}
	{
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		G_B9_0 = L_8;
		G_B9_1 = G_B7_0;
		G_B9_2 = ((U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)(G_B7_1));
		goto IL_0062;
	}

IL_005d:
	{
		float L_9 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		G_B9_0 = L_9;
		G_B9_1 = G_B8_0;
		G_B9_2 = ((U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)(G_B8_1));
	}

IL_0062:
	{
		NullCheck(G_B9_2);
		G_B9_2->set_U3CelapsedTimeU3E5__2_3(((float)il2cpp_codegen_add((float)G_B9_1, (float)G_B9_0)));
		// var percentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
		float L_10 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_11 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		float L_12 = ColorTween_get_duration_mC4A1E3C2EA46A5C657A2B9DA240C796F770ECC5F_inline((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_10/(float)L_12)), /*hidden argument*/NULL);
		V_1 = (float)L_13;
		// tweenInfo.TweenValue(percentage);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_14 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		float L_15 = V_1;
		ColorTween_TweenValue_m20FCBA50CE9328956973861A9CB0A1FD97265A58((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_14, (float)L_15, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00a8:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00af:
	{
		// while (elapsedTime < tweenInfo.duration)
		float L_16 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_17 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		float L_18 = ColorTween_get_duration_mC4A1E3C2EA46A5C657A2B9DA240C796F770ECC5F_inline((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_003c;
		}
	}
	{
		// tweenInfo.TweenValue(1.0f);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * L_19 = (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)__this->get_address_of_tweenInfo_2();
		ColorTween_TweenValue_m20FCBA50CE9328956973861A9CB0A1FD97265A58((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m761E2EC945A4EEC1CB3738E59CC7C953317FE4DB_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m605D45F96449D5CF6DDFACF60880651C85F29195_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m605D45F96449D5CF6DDFACF60880651C85F29195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m605D45F96449D5CF6DDFACF60880651C85F29195_RuntimeMethod_var);
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_m436A52941A440E22575D1CDBA63A3C6BE1B9903E_gshared (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2__ctor_m7F91B2018BD9A84A855E44C0F88566C748C6052C_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_IDisposable_Dispose_mF42D4A1DF8911EFB9E84343443F932F1FDA9F7D5_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CStartU3Ed__2_MoveNext_m072CCA02187FAAE6E23766279083847801807198_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__2_MoveNext_m072CCA02187FAAE6E23766279083847801807198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	float G_B8_0 = 0.0f;
	U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * G_B8_1 = NULL;
	float G_B7_0 = 0.0f;
	U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * G_B9_2 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_U3CU3E1__state_0();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_00a8;
		}
	}
	{
		return (bool)0;
	}

IL_0013:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (!tweenInfo.ValidTarget())
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_3 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		bool L_4 = FloatTween_ValidTarget_m917EB0D30E72AC75D90D1D8F11B1D7EBBD00ECAE((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002f;
		}
	}
	{
		// yield break;
		return (bool)0;
	}

IL_002f:
	{
		// var elapsedTime = 0.0f;
		__this->set_U3CelapsedTimeU3E5__2_3((0.0f));
		goto IL_00af;
	}

IL_003c:
	{
		// elapsedTime += tweenInfo.ignoreTimeScale ? Time.unscaledDeltaTime : Time.deltaTime;
		float L_5 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_6 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		bool L_7 = FloatTween_get_ignoreTimeScale_mA7B69D72E1D52EF1890C89D5690D345B852C7239_inline((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_6, /*hidden argument*/NULL);
		G_B7_0 = L_5;
		G_B7_1 = ((U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)(__this));
		if (L_7)
		{
			G_B8_0 = L_5;
			G_B8_1 = ((U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)(__this));
			goto IL_005d;
		}
	}
	{
		float L_8 = Time_get_deltaTime_m16F98FC9BA931581236008C288E3B25CBCB7C81E(/*hidden argument*/NULL);
		G_B9_0 = L_8;
		G_B9_1 = G_B7_0;
		G_B9_2 = ((U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)(G_B7_1));
		goto IL_0062;
	}

IL_005d:
	{
		float L_9 = Time_get_unscaledDeltaTime_mA0AE7A144C88AE8AABB42DF17B0F3F0714BA06B2(/*hidden argument*/NULL);
		G_B9_0 = L_9;
		G_B9_1 = G_B8_0;
		G_B9_2 = ((U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)(G_B8_1));
	}

IL_0062:
	{
		NullCheck(G_B9_2);
		G_B9_2->set_U3CelapsedTimeU3E5__2_3(((float)il2cpp_codegen_add((float)G_B9_1, (float)G_B9_0)));
		// var percentage = Mathf.Clamp01(elapsedTime / tweenInfo.duration);
		float L_10 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_11 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		float L_12 = FloatTween_get_duration_mBECFBEC57BDC30B54D0638873BA3313A8F7232F5_inline((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_tFBDE6467D269BFE410605C7D806FD9991D4A89CB_il2cpp_TypeInfo_var);
		float L_13 = Mathf_Clamp01_m1E5F736941A7E6DC4DBCA88A1E38FE9FBFE0C42B((float)((float)((float)L_10/(float)L_12)), /*hidden argument*/NULL);
		V_1 = (float)L_13;
		// tweenInfo.TweenValue(percentage);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_14 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		float L_15 = V_1;
		FloatTween_TweenValue_m4E4418FB7FBDC7CBF96D95518DFACF25BCBE8EB3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_14, (float)L_15, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00a8:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_00af:
	{
		// while (elapsedTime < tweenInfo.duration)
		float L_16 = (float)__this->get_U3CelapsedTimeU3E5__2_3();
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_17 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		float L_18 = FloatTween_get_duration_mBECFBEC57BDC30B54D0638873BA3313A8F7232F5_inline((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_17, /*hidden argument*/NULL);
		if ((((float)L_16) < ((float)L_18)))
		{
			goto IL_003c;
		}
	}
	{
		// tweenInfo.TweenValue(1.0f);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * L_19 = (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)__this->get_address_of_tweenInfo_2();
		FloatTween_TweenValue_m4E4418FB7FBDC7CBF96D95518DFACF25BCBE8EB3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)L_19, (float)(1.0f), /*hidden argument*/NULL);
		// }
		return (bool)0;
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D242745AC7E056D542481D3A98C6FDCF5EA5ACB_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4AE666FAF40BAF18CA8D1BCAFDB0D428DEA5DFF8_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4AE666FAF40BAF18CA8D1BCAFDB0D428DEA5DFF8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_0 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mA121DE1CAC8F25277DEB489DC7771209D91CAE33(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, U3CStartU3Ed__2_System_Collections_IEnumerator_Reset_m4AE666FAF40BAF18CA8D1BCAFDB0D428DEA5DFF8_RuntimeMethod_var);
	}
}
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1_<Start>d__2<UnityEngine.UI.CoroutineTween.FloatTween>::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CStartU3Ed__2_System_Collections_IEnumerator_get_Current_mB6E49CF1A10907385C7B0EDB922F3C7D1012CDC9_gshared (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Start(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m8B003840E8B2AD193FFC9B659EF4CB1460F2CEA1_gshared (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___tweenInfo0, const RuntimeMethod* method)
{
	{
		U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * L_0 = (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB * L_1 = (U3CStartU3Ed__2_t3117D5DE8A33C13E45D9874BB9E324E2102075BB *)L_0;
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_2(L_2);
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::Init(UnityEngine.MonoBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_Init_m8438EFCEA89C17CF6245208A3735D2B95BFB9647_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		// m_CoroutineContainer = coroutineContainer;
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StartTween(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_m96867F74662CBAED655A9C7B3AF0036E5282F945_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m96867F74662CBAED655A9C7B3AF0036E5282F945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_CoroutineContainer == null)
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogWarning("Coroutine container not configured... did you forget to call Init?");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0019:
	{
		// StopTween();
		NullCheck((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this);
		((  void (*) (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// if (!m_CoroutineContainer.gameObject.activeInHierarchy)
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		// info.TweenValue(1.0f);
		ColorTween_TweenValue_m20FCBA50CE9328956973861A9CB0A1FD97265A58((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0044:
	{
		// m_Tween = Start(info);
		ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		// m_CoroutineContainer.StartCoroutine(m_Tween);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::StopTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_m3E7518DEC0FF807C68619D211A352C7BF63D9572_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	{
		// if (m_Tween != null)
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// m_CoroutineContainer.StopCoroutine(m_Tween);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1);
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		// m_Tween = null;
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m354D2C09591A71CED72679E2056283296B7AC02B_gshared (TweenRunner_1_t56CEB168ADE3739A1BDDBF258FDC759DF8927172 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.IEnumerator UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Start(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* TweenRunner_1_Start_m0E35A54AAC11F13006A869FF0F3BA6FDDB2DF325_gshared (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___tweenInfo0, const RuntimeMethod* method)
{
	{
		U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * L_0 = (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E * L_1 = (U3CStartU3Ed__2_tF4BC79409CDBECA691561F2017756F22785A840E *)L_0;
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_2 = ___tweenInfo0;
		NullCheck(L_1);
		L_1->set_tweenInfo_2(L_2);
		return L_1;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::Init(UnityEngine.MonoBehaviour)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_Init_m17E5E1BE0EB8226EA16AA2C2AB572E1B225BECD2_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * ___coroutineContainer0, const RuntimeMethod* method)
{
	{
		// m_CoroutineContainer = coroutineContainer;
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = ___coroutineContainer0;
		__this->set_m_CoroutineContainer_0(L_0);
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StartTween(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StartTween_m1C12D6475DFCA47D74844FD2395C1057AF1CA41D_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  ___info0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TweenRunner_1_StartTween_m1C12D6475DFCA47D74844FD2395C1057AF1CA41D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_CoroutineContainer == null)
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_0 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C((Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// Debug.LogWarning("Coroutine container not configured... did you forget to call Init?");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogWarning_m37338644DC81F640CCDFEAE35A223F0E965F0568((RuntimeObject *)_stringLiteralA413973124713A2B7B3570CE8D97C7151C8628A9, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0019:
	{
		// StopTween();
		NullCheck((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this);
		((  void (*) (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		// if (!m_CoroutineContainer.gameObject.activeInHierarchy)
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_2 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		NullCheck((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_3 = Component_get_gameObject_m0B0570BA8DDD3CD78A9DB568EA18D7317686603C((Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *)L_2, /*hidden argument*/NULL);
		NullCheck((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3);
		bool L_4 = GameObject_get_activeInHierarchy_mDEE60F1B28281974BA9880EC448682F3DAABB1EF((GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F *)L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0044;
		}
	}
	{
		// info.TweenValue(1.0f);
		FloatTween_TweenValue_m4E4418FB7FBDC7CBF96D95518DFACF25BCBE8EB3((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A *)(&___info0), (float)(1.0f), /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0044:
	{
		// m_Tween = Start(info);
		FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A  L_5 = ___info0;
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A )L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_m_Tween_1(L_6);
		// m_CoroutineContainer.StartCoroutine(m_Tween);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_7 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_8 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7);
		MonoBehaviour_StartCoroutine_mBF8044CE06A35D76A69669ADD8977D05956616B7((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_7, (RuntimeObject*)L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::StopTween()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1_StopTween_mE648D0014C6DC19A996F304E657BA46278ABC420_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	{
		// if (m_Tween != null)
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_m_Tween_1();
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// m_CoroutineContainer.StopCoroutine(m_Tween);
		MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * L_1 = (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)__this->get_m_CoroutineContainer_0();
		RuntimeObject* L_2 = (RuntimeObject*)__this->get_m_Tween_1();
		NullCheck((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1);
		MonoBehaviour_StopCoroutine_m3CDD6C046CC660D4CD6583FCE97F88A9735FD5FA((MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 *)L_1, (RuntimeObject*)L_2, /*hidden argument*/NULL);
		// m_Tween = null;
		__this->set_m_Tween_1((RuntimeObject*)NULL);
	}

IL_0020:
	{
		// }
		return;
	}
}
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TweenRunner_1__ctor_m3BA236F3CC80F207C28B3A6B2D5C116C483AE872_gshared (TweenRunner_1_tA7C92F52BF30E9A20EDA2DD956E11A1493D098EF * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_mDB9FD3593823FE30F7CFAB286C1D3881990CECEE_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_0 = ___l0;
		NullCheck((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0);
		((  void (*) (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Int32>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ListPool_1_Get_mE7663F36254E817D56A40A8CDE5BF801AAF92542_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * L_0 = ((ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *)L_0);
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ((  List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * (*) (ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m11EA81E78327EFE2672086B352A5A68F653ADD1F_gshared (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * L_0 = ((ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *)L_0);
		((  void (*) (ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *, List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *)L_0, (List_1_tE1526161A558A17A39A8B69D8EEF3801393B6226 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Int32>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_mF2A2E1354E84E58BE5B600862D62C4ACE2E5994F_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A * L_0 = (UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B * L_1 = (ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tE40D9BEC5B067540DB2F9331C8AD1F3B071F974B *, UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *, UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *)NULL, (UnityAction_1_tCFF1D446AB859368D0E404A2D6738C98E10FCC6A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tCDBE9EE6C0FC882FB66BA0B6D36CA8E17652B15A_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m3765E01E233E7ADD214A7665B2988D16ABDDA472_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_0 = ___l0;
		NullCheck((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0);
		((  void (*) (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<System.Object>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ListPool_1_Get_mD763F64B9987BBE7DC6092E7C6014A51106D1362_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *)L_0);
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ((  List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * (*) (ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m377C6B9E4F57B5F932E838F423B6204E45FF45A5_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * L_0 = ((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *)L_0);
		((  void (*) (ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *, List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *)L_0, (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<System.Object>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m3496A882F0AD7E4968CE0CF81F33EB1FF21158F5_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 * L_0 = (UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE * L_1 = (ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t3A70BDB59315D0B41F26C2019575B584BE9816CE *, UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *, UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *)NULL, (UnityAction_1_tDEF40749C3309C99C0622A39D22422E683051C67 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_t66B0CA6885E680896C9747F9C0E28458D59743BE_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m8CA2445008A4E9AE47B9A12476C9137F7280548D_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_0 = ___l0;
		NullCheck((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0);
		((  void (*) (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ListPool_1_Get_mC0EBE105137C878FC9B9EF547C6A49850D26FAED_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * L_0 = ((ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *)L_0);
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ((  List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * (*) (ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m53CA5C7991B0F21FE9ADE77FF03A67882C7400E2_gshared (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * L_0 = ((ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *)L_0);
		((  void (*) (ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *, List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *)L_0, (List_1_t749ADA5233D9B421293A000DCB83608A24C3D5D5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Color32>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m8963863F58DA18195F28B03D3EB55AF3FCAD005D_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 * L_0 = (UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 * L_1 = (ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tA42695C1C422850E35146976898DA19272914AC8 *, UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *, UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *)NULL, (UnityAction_1_tBFC448984D861333BFB83317707C2F208D61E3D1 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tFB4F0894A1EF21042E633BD1514A9258601CB1F1_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m111FC0DF82D2C8A9F0A93D535F022BAD0B61E809_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_0 = ___l0;
		NullCheck((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0);
		((  void (*) (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ListPool_1_Get_mAC3BD476854BD242EF7B65950D86C4A12761F461_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * L_0 = ((ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *)L_0);
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ((  List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * (*) (ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m8B379A7621039BF3ABE82280D6ABD4606F3459F9_gshared (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * L_0 = ((ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *)L_0);
		((  void (*) (ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *, List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *)L_0, (List_1_t4CE16E1B496C9FE941554BB47727DFDD7C3D9554 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m09F39A9E0242FE6E549173DE14DDC8C41AB542B2_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A * L_0 = (UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 * L_1 = (ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t8C6F590942FFC5DFF0DF79569711056BE7883062 *, UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *, UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *)NULL, (UnityAction_1_t6CAED93F6613F01731695AAC7F8201ABB209CC0A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tC140413ABBDAF87428D43AA5EFDD842394341CF6_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m48D5FAC919131FCD61A7B62577B6C4C5043FB6C7_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_0 = ___l0;
		NullCheck((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0);
		((  void (*) (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ListPool_1_Get_m4095D588F45966ACF43C1C5D1390D7E3F6E388C7_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * L_0 = ((ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *)L_0);
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ((  List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * (*) (ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_m696D12DB4E7D73886B746B87B7491B200ED0F8F0_gshared (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * L_0 = ((ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *)L_0);
		((  void (*) (ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *, List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *)L_0, (List_1_t0737D51EB43DAAA1BDC9C2B83B393A4B9B9BE8EB *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m5A0004898A4F7854EA5B0E9394EE822C3B2275DF_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC * L_0 = (UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 * L_1 = (ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t592C52579DFFCDED9A816AE87CA33C2A992F7E26 *, UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *, UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *)NULL, (UnityAction_1_tAF537B97BFBDAFB0D27DBBE2DC414D654B6C41EC *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tEC2CD2FE52DC3A8B1F3E2BF498E60420B6ABDDF8_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m362B6B736C6D546692C23E8A44C47F56C3B236F9_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_0 = ___l0;
		NullCheck((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0);
		((  void (*) (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ListPool_1_Get_m8A0AAD805F41A1FD12A20327638FB81947677A4A_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * L_0 = ((ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *)L_0);
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ((  List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * (*) (ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_mE909A7A9F124C64BF0014554CF1679F93F7F9544_gshared (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * L_0 = ((ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *)L_0);
		((  void (*) (ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *, List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *)L_0, (List_1_tFCCBEDAA56D8F7598520FB136A9F8D713033D6B5 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector3>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m27499BE3966E501959E8957FE81F00639E383216_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA * L_0 = (UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 * L_1 = (ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_t559B14380295E744507B0BF258F6EB60B6D3B902 *, UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *, UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *)NULL, (UnityAction_1_tFFEAFE33FC07D89E336EC0E8863403BAC10853CA *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_t5E8D36B177BEE61E319DF7927E463DFBAC58E09D_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Clear(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Clear_m67D074308DE6FFFBA1B99BDE569FFDACE5CE4243_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___l0, const RuntimeMethod* method)
{
	{
		// static void Clear(List<T> l) { l.Clear(); }
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_0 = ___l0;
		NullCheck((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0);
		((  void (*) (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0)->methodPointer)((List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		// static void Clear(List<T> l) { l.Clear(); }
		return;
	}
}
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ListPool_1_Get_mF32AE75B27464EE9535E8C1AE7D14B948B2A7281_gshared (const RuntimeMethod* method)
{
	{
		// return s_ListPool.Get();
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * L_0 = ((ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		NullCheck((ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *)L_0);
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ((  List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * (*) (ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2)->methodPointer)((ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::Release(System.Collections.Generic.List`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1_Release_mF52D201E89E59068809EE4F76B2B69086F6EBC6E_gshared (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * ___toRelease0, const RuntimeMethod* method)
{
	{
		// s_ListPool.Release(toRelease);
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * L_0 = ((ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->get_s_ListPool_0();
		List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 * L_1 = ___toRelease0;
		NullCheck((ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *)L_0);
		((  void (*) (ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *, List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3)->methodPointer)((ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *)L_0, (List_1_tFF4005B40E5BA433006DA11C56DB086B1E2FC955 *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 3));
		// }
		return;
	}
}
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector4>::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ListPool_1__cctor_m322FE00775BB7CD39B3E2B828D85FB50BB75CFE5_gshared (const RuntimeMethod* method)
{
	{
		// private static readonly ObjectPool<List<T>> s_ListPool = new ObjectPool<List<T>>(null, Clear);
		UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 * L_0 = (UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 5));
		((  void (*) (UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6)->methodPointer)(L_0, (RuntimeObject *)NULL, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 4)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 6));
		ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 * L_1 = (ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 7));
		((  void (*) (ObjectPool_1_tB6CF67DEBE320D035ADE0A7E0AB9B2C8F96E7338 *, UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *, UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8)->methodPointer)(L_1, (UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *)NULL, (UnityAction_1_t4CA653A7B54DA3D30EC370F89843FDF53F602008 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 8));
		((ListPool_1_tD46C00F7C693AF94203B6B004906149A661AA540_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 1)))->set_s_ListPool_0(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countAll()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countAll_m63DF483CE87C8C8576E1B7AC0EF5F6A1F17725C6_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, const RuntimeMethod* method)
{
	{
		// public int countAll { get; private set; }
		int32_t L_0 = (int32_t)__this->get_U3CcountAllU3Ek__BackingField_3();
		return L_0;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::set_countAll(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1_set_countAll_m12C0E8F9D75242332DF8B262A1002E5C9D9BF750_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int countAll { get; private set; }
		int32_t L_0 = ___value0;
		__this->set_U3CcountAllU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countActive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countActive_m1F40AA1D7F704C4AEEA93B35406C516FBF8D6D6B_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, const RuntimeMethod* method)
{
	{
		// public int countActive { get { return countAll - countInactive; } }
		NullCheck((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this);
		int32_t L_0 = ((  int32_t (*) (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		NullCheck((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this);
		int32_t L_1 = ((  int32_t (*) (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)L_1));
	}
}
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Object>::get_countInactive()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ObjectPool_1_get_countInactive_mF5825E870D989664981D381110E4430BAEC30B32_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, const RuntimeMethod* method)
{
	{
		// public int countInactive { get { return m_Stack.Count; } }
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return L_1;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1__ctor_m2D9E8D06AAF43A706B02D87137D55B8B0F64E66A_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnGet0, UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * ___actionOnRelease1, const RuntimeMethod* method)
{
	{
		// private readonly Stack<T> m_Stack = new Stack<T>();
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3));
		((  void (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_m_Stack_0(L_0);
		// public ObjectPool(UnityAction<T> actionOnGet, UnityAction<T> actionOnRelease)
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		// m_ActionOnGet = actionOnGet;
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_1 = ___actionOnGet0;
		__this->set_m_ActionOnGet_1(L_1);
		// m_ActionOnRelease = actionOnRelease;
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_2 = ___actionOnRelease1;
		__this->set_m_ActionOnRelease_2(L_2);
		// }
		return;
	}
}
// T UnityEngine.UI.ObjectPool`1<System.Object>::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * ObjectPool_1_Get_m8D5DE92D04BE91AE7DD9C1B767C183EC9DE98952_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (m_Stack.Count == 0)
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		if (L_1)
		{
			goto IL_0025;
		}
	}
	{
		// element = new T();
		RuntimeObject * L_2 = ((  RuntimeObject * (*) (const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)(/*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_0 = (RuntimeObject *)L_2;
		// countAll++;
		NullCheck((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this);
		int32_t L_3 = ((  int32_t (*) (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		V_1 = (int32_t)L_3;
		int32_t L_4 = V_1;
		NullCheck((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this);
		((  void (*) (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B *)__this, (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		// }
		goto IL_0031;
	}

IL_0025:
	{
		// element = m_Stack.Pop();
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_5 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_5);
		RuntimeObject * L_6 = ((  RuntimeObject * (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		V_0 = (RuntimeObject *)L_6;
	}

IL_0031:
	{
		// if (m_ActionOnGet != null)
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_7 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		if (!L_7)
		{
			goto IL_0045;
		}
	}
	{
		// m_ActionOnGet(element);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_8 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnGet_1();
		RuntimeObject * L_9 = V_0;
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_8);
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
	}

IL_0045:
	{
		// return element;
		RuntimeObject * L_10 = V_0;
		return L_10;
	}
}
// System.Void UnityEngine.UI.ObjectPool`1<System.Object>::Release(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectPool_1_Release_m11F10A7199F0544250785EAA41F819E03D65BC17_gshared (ObjectPool_1_t3EF1AC01FF09BFF602D9F00F7E9C508ED5883E9B * __this, RuntimeObject * ___element0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ObjectPool_1_Release_m11F10A7199F0544250785EAA41F819E03D65BC17_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Stack.Count > 0 && ReferenceEquals(m_Stack.Peek(), element))
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_0 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0);
		int32_t L_1 = ((  int32_t (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		if ((((int32_t)L_1) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_2 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_2);
		RuntimeObject * L_3 = ((  RuntimeObject * (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		RuntimeObject * L_4 = ___element0;
		if ((!(((RuntimeObject*)(RuntimeObject *)L_3) == ((RuntimeObject*)(RuntimeObject *)L_4))))
		{
			goto IL_0030;
		}
	}
	{
		// Debug.LogError("Internal error. Trying to destroy object that is already released to pool.");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29((RuntimeObject *)_stringLiteral04231B44477132B3DBEFE7768A921AE5A13A00FC, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// if (m_ActionOnRelease != null)
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_5 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		if (!L_5)
		{
			goto IL_0044;
		}
	}
	{
		// m_ActionOnRelease(element);
		UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 * L_6 = (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)__this->get_m_ActionOnRelease_2();
		RuntimeObject * L_7 = ___element0;
		NullCheck((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_6);
		((  void (*) (UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((UnityAction_1_t330F97880F37E23D6D0C6618DD77F28AC9EF8FA9 *)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
	}

IL_0044:
	{
		// m_Stack.Push(element);
		Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 * L_8 = (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)__this->get_m_Stack_0();
		RuntimeObject * L_9 = ___element0;
		NullCheck((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_8);
		((  void (*) (Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)((Stack_1_t5697A763CE21E705BB0297FFBE9AFCB5F95C9163 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool ColorTween_get_ignoreTimeScale_m6A06826E19314EFE9783E505C75CFC76E42E8F05_inline (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_IgnoreTimeScale; }
		bool L_0 = __this->get_m_IgnoreTimeScale_5();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float ColorTween_get_duration_mC4A1E3C2EA46A5C657A2B9DA240C796F770ECC5F_inline (ColorTween_t4CBBF5875FA391053DB62E98D8D9603040413228 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Duration; }
		float L_0 = __this->get_m_Duration_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR bool FloatTween_get_ignoreTimeScale_mA7B69D72E1D52EF1890C89D5690D345B852C7239_inline (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_IgnoreTimeScale; }
		bool L_0 = __this->get_m_IgnoreTimeScale_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR float FloatTween_get_duration_mBECFBEC57BDC30B54D0638873BA3313A8F7232F5_inline (FloatTween_tF6BB24C266F36BD80E20C91AED453F7CE516919A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Duration; }
		float L_0 = __this->get_m_Duration_3();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m1D7746BAE25318A4C5BDE3B52A9A140A42D473F6_gshared_inline (ReadOnlyArray_1_t1F3CB644E1578E5098EC5FD9432CB85C834379F8 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB2E0154DBB840B9439940CEACC891E120680725E_gshared_inline (ReadOnlyArray_1_t81FE80E7DBEBF880F60E61AE1C80F608FF7CC712 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E6E3E597FA6E60272498BB4781AE9482E5F3957_gshared_inline (ReadOnlyArray_1_t5335D3C0F14879A1542413A5F85FC68AC4B5B9DF * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m466B72766C74CC98E1B2DF8874F533E84DBE3DD6_gshared_inline (ReadOnlyArray_1_t8A939C92FDF5A05848FE0A06997D6BF52F71E999 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m5E2D0AF28E6E5B592729FCFB159FA58E6F659605_gshared_inline (ReadOnlyArray_1_tE28EED4FA0A40374CCF605F57381C9C73F945CF6 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m8BD37809A3C3DDBCEEC7296C3A30A6829870DBE1_gshared_inline (ReadOnlyArray_1_tEC2E81544444B21F06DFF1768B4F333700C12DBD * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mC0795654A55C336A2D090089E547182C5E85605C_gshared_inline (ReadOnlyArray_1_t22EBE8542BB109EE6A64BDD95D080DEB76D8A65D * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_m03745C89004DFA0F9B2C2FB536568BC937E978A1_gshared_inline (ReadOnlyArray_1_t5C7EEF692B0FB398F38C8FBFDD72BC7935C6112F * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mF7DB0BB0D1AB26BC6EE25EE2AF9E6B5D23BB41EE_gshared_inline (ReadOnlyArray_1_t183FCACD915D99C968E86182F675F0E077438048 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline IL2CPP_METHOD_ATTR int32_t ReadOnlyArray_1_get_Count_mB5B32A341F4B9E5EFE040F21827EC340E4A65E88_gshared_inline (ReadOnlyArray_1_t511E730FAA2F7DA1556F351B7F6950711D292EB9 * __this, const RuntimeMethod* method)
{
	{
		// public int Count => m_Length;
		int32_t L_0 = (int32_t)__this->get_m_Length_2();
		return L_0;
	}
}
