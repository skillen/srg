﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <email>j__TPar <>f__AnonymousType0`2::get_email()
// 0x00000002 <password>j__TPar <>f__AnonymousType0`2::get_password()
// 0x00000003 System.Void <>f__AnonymousType0`2::.ctor(<email>j__TPar,<password>j__TPar)
// 0x00000004 System.Boolean <>f__AnonymousType0`2::Equals(System.Object)
// 0x00000005 System.Int32 <>f__AnonymousType0`2::GetHashCode()
// 0x00000006 System.String <>f__AnonymousType0`2::ToString()
// 0x00000007 <input>j__TPar <>f__AnonymousType1`1::get_input()
// 0x00000008 System.Void <>f__AnonymousType1`1::.ctor(<input>j__TPar)
// 0x00000009 System.Boolean <>f__AnonymousType1`1::Equals(System.Object)
// 0x0000000A System.Int32 <>f__AnonymousType1`1::GetHashCode()
// 0x0000000B System.String <>f__AnonymousType1`1::ToString()
// 0x0000000C <lineItems>j__TPar <>f__AnonymousType2`1::get_lineItems()
// 0x0000000D System.Void <>f__AnonymousType2`1::.ctor(<lineItems>j__TPar)
// 0x0000000E System.Boolean <>f__AnonymousType2`1::Equals(System.Object)
// 0x0000000F System.Int32 <>f__AnonymousType2`1::GetHashCode()
// 0x00000010 System.String <>f__AnonymousType2`1::ToString()
// 0x00000011 <name>j__TPar <>f__AnonymousType3`1::get_name()
// 0x00000012 System.Void <>f__AnonymousType3`1::.ctor(<name>j__TPar)
// 0x00000013 System.Boolean <>f__AnonymousType3`1::Equals(System.Object)
// 0x00000014 System.Int32 <>f__AnonymousType3`1::GetHashCode()
// 0x00000015 System.String <>f__AnonymousType3`1::ToString()
// 0x00000016 <first>j__TPar <>f__AnonymousType4`1::get_first()
// 0x00000017 System.Void <>f__AnonymousType4`1::.ctor(<first>j__TPar)
// 0x00000018 System.Boolean <>f__AnonymousType4`1::Equals(System.Object)
// 0x00000019 System.Int32 <>f__AnonymousType4`1::GetHashCode()
// 0x0000001A System.String <>f__AnonymousType4`1::ToString()
// 0x0000001B <objects>j__TPar <>f__AnonymousType5`1::get_objects()
// 0x0000001C System.Void <>f__AnonymousType5`1::.ctor(<objects>j__TPar)
// 0x0000001D System.Boolean <>f__AnonymousType5`1::Equals(System.Object)
// 0x0000001E System.Int32 <>f__AnonymousType5`1::GetHashCode()
// 0x0000001F System.String <>f__AnonymousType5`1::ToString()
// 0x00000020 <id>j__TPar <>f__AnonymousType6`2::get_id()
// 0x00000021 <name>j__TPar <>f__AnonymousType6`2::get_name()
// 0x00000022 System.Void <>f__AnonymousType6`2::.ctor(<id>j__TPar,<name>j__TPar)
// 0x00000023 System.Boolean <>f__AnonymousType6`2::Equals(System.Object)
// 0x00000024 System.Int32 <>f__AnonymousType6`2::GetHashCode()
// 0x00000025 System.String <>f__AnonymousType6`2::ToString()
// 0x00000026 <query>j__TPar <>f__AnonymousType7`1::get_query()
// 0x00000027 System.Void <>f__AnonymousType7`1::.ctor(<query>j__TPar)
// 0x00000028 System.Boolean <>f__AnonymousType7`1::Equals(System.Object)
// 0x00000029 System.Int32 <>f__AnonymousType7`1::GetHashCode()
// 0x0000002A System.String <>f__AnonymousType7`1::ToString()
// 0x0000002B <id>j__TPar <>f__AnonymousType8`3::get_id()
// 0x0000002C <type>j__TPar <>f__AnonymousType8`3::get_type()
// 0x0000002D <payload>j__TPar <>f__AnonymousType8`3::get_payload()
// 0x0000002E System.Void <>f__AnonymousType8`3::.ctor(<id>j__TPar,<type>j__TPar,<payload>j__TPar)
// 0x0000002F System.Boolean <>f__AnonymousType8`3::Equals(System.Object)
// 0x00000030 System.Int32 <>f__AnonymousType8`3::GetHashCode()
// 0x00000031 System.String <>f__AnonymousType8`3::ToString()
// 0x00000032 UnityEngine.Sprite EmptySprite::Get()
extern void EmptySprite_Get_mF969CF8CC7C5DF8CE9E524C818828E8A68164842 ();
// 0x00000033 System.Boolean EmptySprite::IsEmptySprite(UnityEngine.Sprite)
extern void EmptySprite_IsEmptySprite_m03E33296B60FDDB3B66A58882EA11D98F2D7D852 ();
// 0x00000034 UnityEngine.Vector4 FreeModifier::get_Radius()
extern void FreeModifier_get_Radius_m597D0A4773B74F68383E84400C171DD9A57A6019 ();
// 0x00000035 System.Void FreeModifier::set_Radius(UnityEngine.Vector4)
extern void FreeModifier_set_Radius_mA9CB3D340F28A88EEF4B2236EC1C8AF55DD70E0D ();
// 0x00000036 UnityEngine.Vector4 FreeModifier::CalculateRadius(UnityEngine.Rect)
extern void FreeModifier_CalculateRadius_mFA329C10795C5BBB892983AC03D563A46D053699 ();
// 0x00000037 System.Void FreeModifier::OnValidate()
extern void FreeModifier_OnValidate_mE43A9747175060376B72326252BEB90BD2058DC1 ();
// 0x00000038 System.Void FreeModifier::.ctor()
extern void FreeModifier__ctor_m8582DFCFA70998C596206F2F547A4855DF7E0EC7 ();
// 0x00000039 System.Single OnlyOneEdgeModifier::get_Radius()
extern void OnlyOneEdgeModifier_get_Radius_mC8DBA87D38F115DC596ABF52767BA7303233C0DA ();
// 0x0000003A System.Void OnlyOneEdgeModifier::set_Radius(System.Single)
extern void OnlyOneEdgeModifier_set_Radius_m631A55143B0ACFB07F99BA4FEFB3FADFFE3E3255 ();
// 0x0000003B OnlyOneEdgeModifier_ProceduralImageEdge OnlyOneEdgeModifier::get_Side()
extern void OnlyOneEdgeModifier_get_Side_m1C199404DE5AFB90464191A17675522EFFFBC4AD ();
// 0x0000003C System.Void OnlyOneEdgeModifier::set_Side(OnlyOneEdgeModifier_ProceduralImageEdge)
extern void OnlyOneEdgeModifier_set_Side_mE522A0B44AC052CC813BE1BCA95A5CC566A42454 ();
// 0x0000003D UnityEngine.Vector4 OnlyOneEdgeModifier::CalculateRadius(UnityEngine.Rect)
extern void OnlyOneEdgeModifier_CalculateRadius_m32D4273AE7384895BA1061E00A7E7815EBB35A14 ();
// 0x0000003E System.Void OnlyOneEdgeModifier::.ctor()
extern void OnlyOneEdgeModifier__ctor_mD0A037F75A7DF4411920CD15C7203D775BFBEF50 ();
// 0x0000003F UnityEngine.Vector4 RoundModifier::CalculateRadius(UnityEngine.Rect)
extern void RoundModifier_CalculateRadius_m7BC0D84B13B1663F9F41B934F7C04AD2C12F605A ();
// 0x00000040 System.Void RoundModifier::.ctor()
extern void RoundModifier__ctor_m39C2B72E9D16271E5B0FC665AE2AFD610BFC5985 ();
// 0x00000041 System.Single UniformModifier::get_Radius()
extern void UniformModifier_get_Radius_mA2D9291E1D661BACF9AA62DE7CEED5E3FFD495EF ();
// 0x00000042 System.Void UniformModifier::set_Radius(System.Single)
extern void UniformModifier_set_Radius_m13707EAC06423E88B3DE8C89D3B50855D82BAF50 ();
// 0x00000043 UnityEngine.Vector4 UniformModifier::CalculateRadius(UnityEngine.Rect)
extern void UniformModifier_CalculateRadius_m9420E25FFAA7C1CA43F3EF4C7A245103E070E466 ();
// 0x00000044 System.Void UniformModifier::.ctor()
extern void UniformModifier__ctor_mC47E306E5B93D223BFD422A888A002838807CDD5 ();
// 0x00000045 T[] JsonHelper::getJsonArray(System.String)
// 0x00000046 System.Void JsonHelper::.ctor()
extern void JsonHelper__ctor_m44D9EFAD3CBB1C62453A7A08E545331E802BCB99 ();
// 0x00000047 System.Void BCProduct::.ctor()
extern void BCProduct__ctor_m69382B6B637749C3F63DEFC3101B3A26DA2ABB82 ();
// 0x00000048 System.Void ShopifyQuery::Start()
extern void ShopifyQuery_Start_mF4746E754612667BEC46F4AAA19F6D845B38D0F1 ();
// 0x00000049 System.Void ShopifyQuery::test()
extern void ShopifyQuery_test_m3D8E9C9C1F7B9C2692AABC037F24FBA55E87C38E ();
// 0x0000004A System.Void ShopifyQuery::getShop()
extern void ShopifyQuery_getShop_m5D2E9977F613FB950346CCB352753D12856CA55D ();
// 0x0000004B System.Void ShopifyQuery::getProducts()
extern void ShopifyQuery_getProducts_mB6683F594ECC1FB07CEB7FC3178EDBCA4567AA4E ();
// 0x0000004C System.Void ShopifyQuery::getCustomerAcess(System.String,System.String)
extern void ShopifyQuery_getCustomerAcess_mC5B411A390F2A10326FBF14F57BE47579502F1A8 ();
// 0x0000004D System.Void ShopifyQuery::getCustomer()
extern void ShopifyQuery_getCustomer_m51271441CA205B30BF1903F9AE5A2DADFB9069C4 ();
// 0x0000004E System.Void ShopifyQuery::createNewCustomer(System.String,System.String)
extern void ShopifyQuery_createNewCustomer_mBBAF437D1964F229591CD5B93031FA145D893BA0 ();
// 0x0000004F System.Threading.Tasks.Task`1<System.String> ShopifyQuery::createCheckout()
extern void ShopifyQuery_createCheckout_m476586ADF5999BC4BA6BCDEDE9170935D5EFE11D ();
// 0x00000050 System.Void ShopifyQuery::associateCustomerCheckout(System.String,System.String)
extern void ShopifyQuery_associateCustomerCheckout_mF83152A5324F40E5C0D7260D708ECD5A8CFDD05A ();
// 0x00000051 System.Void ShopifyQuery::testUnitySDK()
extern void ShopifyQuery_testUnitySDK_mEEE49622386DCB1F67185E2F4E0DE0C334137BCC ();
// 0x00000052 System.Void ShopifyQuery::.ctor()
extern void ShopifyQuery__ctor_mC13E528910FE7032DB6866D76F9E56A242BC4EDF ();
// 0x00000053 System.Void LineItem::.ctor()
extern void LineItem__ctor_m23830AFD4B0EF653433FCDFA3CD1E65B1955A47A ();
// 0x00000054 System.Void assocCredentials::.ctor(System.String,System.String)
extern void assocCredentials__ctor_m24AC85114DDF2665A1AF4CED00791A84573D573F ();
// 0x00000055 System.Void AddToCart::AddThisProductToCart()
extern void AddToCart_AddThisProductToCart_m3E76425EA0C961BAE8151935325BCEDC124CF1D4 ();
// 0x00000056 System.Void AddToCart::.ctor()
extern void AddToCart__ctor_mE861A247A475B44C3034D53E0902B86F87207535 ();
// 0x00000057 System.Void AnimateSidebar::Awake()
extern void AnimateSidebar_Awake_m1F303204675A42C32B8F0DBADE93A56EA2B8FAF7 ();
// 0x00000058 System.Void AnimateSidebar::OnEnable()
extern void AnimateSidebar_OnEnable_mCA821698650B40F759F855C082CE70CC2A0E0C12 ();
// 0x00000059 System.Collections.IEnumerator AnimateSidebar::MoveIn(System.Single,System.Single,UnityEngine.AnimationCurve,System.Single)
extern void AnimateSidebar_MoveIn_m23D862B603896E096548A55A26A98AB1046E9FDE ();
// 0x0000005A System.Void AnimateSidebar::DisableSidebar()
extern void AnimateSidebar_DisableSidebar_m28F95A618A36038B45CD690790A5A8E8B81A85E9 ();
// 0x0000005B System.Collections.IEnumerator AnimateSidebar::MoveOut(System.Single,System.Single,UnityEngine.AnimationCurve,System.Single)
extern void AnimateSidebar_MoveOut_m59EFF362CCEFE8B278C68405564E67294428D48D ();
// 0x0000005C System.Void AnimateSidebar::.ctor()
extern void AnimateSidebar__ctor_mF1BFFD6254999228DC0ECA70DF7378E9F8EE63A8 ();
// 0x0000005D System.Void Banners::CreateBanner(System.String,System.String,UnityEngine.RectTransform)
extern void Banners_CreateBanner_mC31223E35C77F28BEAEB9323B75248C4889C37DF ();
// 0x0000005E System.Threading.Tasks.Task`1<UnityEngine.Sprite> Banners::LoadSpriteAsync(System.String)
extern void Banners_LoadSpriteAsync_m3429D181439AE63DC393B66AFFAC731CFC78E147 ();
// 0x0000005F System.Void Banners::CreateHomeBanner(System.String,System.String)
extern void Banners_CreateHomeBanner_mB98A71E022090E26651373E309791FDAF68E8E3C ();
// 0x00000060 System.Void Banners::CreateFeatureBannerOne(System.String,System.String)
extern void Banners_CreateFeatureBannerOne_m11D8ED7BF5ECD2DDA3BBAB5228AAF8B4FB0C500F ();
// 0x00000061 System.Void Banners::CreateFeatureBannerTwo(System.String,System.String)
extern void Banners_CreateFeatureBannerTwo_m48C9B3DBBD02C58B7208B5E6E29402E571442DCD ();
// 0x00000062 System.Void Banners::CreateGamesBannerOne(System.String,System.String)
extern void Banners_CreateGamesBannerOne_mC51F212127D91F1567E2DE94999ADEC8C365435F ();
// 0x00000063 System.Void Banners::CreateGamesBannerTwo(System.String,System.String)
extern void Banners_CreateGamesBannerTwo_m75917C2DF3827B5B3C79DD5E410BF333D8923FEF ();
// 0x00000064 System.Void Banners::.ctor()
extern void Banners__ctor_m3F423CC3C69D01CE54A740EE1F5693B7D5FB6856 ();
// 0x00000065 System.Int32 CartButton::get_ItemsInCart()
extern void CartButton_get_ItemsInCart_mDBEAF969E0449490449A4443A77249C2A4037293 ();
// 0x00000066 System.Void CartButton::set_ItemsInCart(System.Int32)
extern void CartButton_set_ItemsInCart_m1072FEFE8CABF6C28D606B40FA14AF5FDABEC910 ();
// 0x00000067 System.Void CartButton::Awake()
extern void CartButton_Awake_m742EBAB43A182475AF52B541940C5DA89E878A72 ();
// 0x00000068 System.Void CartButton::OpenCartPage()
extern void CartButton_OpenCartPage_m875E065225694367AFC6116EB4DA5C3976413A9F ();
// 0x00000069 System.Void CartButton::UpdateCartNumber()
extern void CartButton_UpdateCartNumber_m32084BDF49780E04D2D3A1787D05464E24597854 ();
// 0x0000006A System.Void CartButton::.ctor()
extern void CartButton__ctor_m30F73C5A9D12A420CEBE3F3C43499A68CEDC9CCA ();
// 0x0000006B System.Void FeaturedBanner::LoadNewImage()
extern void FeaturedBanner_LoadNewImage_mCCAB73364B435BB29012F79766DA5C5A6A08B431 ();
// 0x0000006C System.Void FeaturedBanner::FeaturedBannerClick()
extern void FeaturedBanner_FeaturedBannerClick_m9739744C2653EE6C18886412EC61A0ABBC6750E7 ();
// 0x0000006D System.Void FeaturedBanner::FitToFrame()
extern void FeaturedBanner_FitToFrame_m6C370D63F90983674049E9D547AF15A5802B1554 ();
// 0x0000006E System.Void FeaturedBanner::SetSprite()
extern void FeaturedBanner_SetSprite_m0BA3B82FE898811BDD894212397C358ADD466B7E ();
// 0x0000006F System.Void FeaturedBanner::.ctor()
extern void FeaturedBanner__ctor_m438324DBA3931A85C03C7F7C0D93A80D5ED86C20 ();
// 0x00000070 System.Void GlobalColors::Awake()
extern void GlobalColors_Awake_mD43B3493219401DAF3E22CF33046B74A498A83E0 ();
// 0x00000071 UnityEngine.Color32 GlobalColors::get_ProductBannerPS4()
extern void GlobalColors_get_ProductBannerPS4_mBC409F0B8442845E77D5F8F32F722006B4846E5A ();
// 0x00000072 UnityEngine.Color32 GlobalColors::get_ProductBannerSwitch()
extern void GlobalColors_get_ProductBannerSwitch_m72356A4828E572714ABBFA460BA2EF8427DFD2F1 ();
// 0x00000073 UnityEngine.Color32 GlobalColors::get_ProductBannerPC()
extern void GlobalColors_get_ProductBannerPC_m334CF80EC22D23521064622272CA9950067D34F6 ();
// 0x00000074 UnityEngine.Color32 GlobalColors::get_ProductBannerMisc()
extern void GlobalColors_get_ProductBannerMisc_m10FD7271715738C0B6C84B71B4B1A0FDAC4DA990 ();
// 0x00000075 System.Void GlobalColors::.ctor()
extern void GlobalColors__ctor_m306917E89EB81FED92012A45D1C492152B88FF9C ();
// 0x00000076 System.Boolean MenuButton::get_isSelected()
extern void MenuButton_get_isSelected_mA3A41544D85B49D1493472BAC18CCD39D889C05D ();
// 0x00000077 System.Void MenuButton::set_isSelected(System.Boolean)
extern void MenuButton_set_isSelected_m297B19F93F6BFDF406B2DDF35E0F0328E91D45FD ();
// 0x00000078 System.Void MenuButton::MenuButtonClick()
extern void MenuButton_MenuButtonClick_mECE56C78EB7BB9428C03A2480639FE324A0A51AC ();
// 0x00000079 System.Void MenuButton::HighlightMenuBtn()
extern void MenuButton_HighlightMenuBtn_m2C54A61488B914307DF4440EEB95A13ED51E7B90 ();
// 0x0000007A System.Void MenuButton::SelectMenuBtn()
extern void MenuButton_SelectMenuBtn_m540AF3B1AA4AAF6B1F73AF12EF7D9A4C41697D45 ();
// 0x0000007B System.Void MenuButton::UnselectMenuBtn()
extern void MenuButton_UnselectMenuBtn_mCB04E50D4EFD5A3EC3741E12F0C90065E5969202 ();
// 0x0000007C System.Void MenuButton::.ctor()
extern void MenuButton__ctor_m63139A1271B817481EC61A971685F1D9666491E0 ();
// 0x0000007D System.Void MenuButtons::Awake()
extern void MenuButtons_Awake_m8FB4ED56C066421B158AC7F9EB60F8D9C54D8F53 ();
// 0x0000007E System.Void MenuButtons::Start()
extern void MenuButtons_Start_mC18C8436D178DD5253A6203FF0D55C7754A8219F ();
// 0x0000007F System.Void MenuButtons::NewActiveMenuBtn(MenuButton)
extern void MenuButtons_NewActiveMenuBtn_m303130CA89D8001F0A8F891432B992C395D2DC12 ();
// 0x00000080 System.Void MenuButtons::UnselectLastMenuBtn()
extern void MenuButtons_UnselectLastMenuBtn_m059E15EADAE51F07170188E14784790FD6E6E956 ();
// 0x00000081 System.Void MenuButtons::.ctor()
extern void MenuButtons__ctor_m58BFA0C363AEBD63C9EAEC8F6BB1ACBBF80E2551 ();
// 0x00000082 System.Void Page::.ctor()
extern void Page__ctor_m078A0134BDCCF6EFA0A16033E6B38B46958933D1 ();
// 0x00000083 System.Void Pages::Awake()
extern void Pages_Awake_m4AEB2324C7800F567803FF1B5F7B9B2C130394B2 ();
// 0x00000084 System.Void Pages::Start()
extern void Pages_Start_mFF81B9827A6B45E52376EAFEE96B291B7B61440B ();
// 0x00000085 System.Void Pages::InitAllPages()
extern void Pages_InitAllPages_m37FDE5D97A24B3FB23983D48719C29E030A6E27D ();
// 0x00000086 System.Void Pages::SetPage(Page,System.Boolean)
extern void Pages_SetPage_mCDC7649016C72230CF3398B61FE825FD15B40187 ();
// 0x00000087 System.Void Pages::CloseAllPages()
extern void Pages_CloseAllPages_mC998C12FC71DEA2E332648C192725487E137EE81 ();
// 0x00000088 System.Void Pages::OpenAllPages()
extern void Pages_OpenAllPages_mA005EBE910D873855197DBB4DEDA56AB62274FDD ();
// 0x00000089 System.Void Pages::OpenPage(Pages_PageName)
extern void Pages_OpenPage_m078F6988503E82B8A76FB6521FE18D74238A1B19 ();
// 0x0000008A Pages_PageName Pages::PageLink(System.String)
extern void Pages_PageLink_m42807EBDA92CBC58FFFE0079E0E08066FCB0A59B ();
// 0x0000008B System.Void Pages::.ctor()
extern void Pages__ctor_m1C50FFBF81B984817D7F4B89F105D27021491F93 ();
// 0x0000008C System.Void ProductOnShelf::LoadProduct()
extern void ProductOnShelf_LoadProduct_m415CFDA89C3F695B99E76559507BFB149A58DC07 ();
// 0x0000008D UnityEngine.Color ProductOnShelf::BannerColor()
extern void ProductOnShelf_BannerColor_mADD6E6DAD45705B961E2406A31F2A8CF05595ECB ();
// 0x0000008E System.Void ProductOnShelf::OpenGamePage()
extern void ProductOnShelf_OpenGamePage_m8167E81E582FCA45725C304127F2C8039D26558F ();
// 0x0000008F System.Void ProductOnShelf::.ctor()
extern void ProductOnShelf__ctor_mF18D4B0816013E6DC81E0B1019507A62F52D7095 ();
// 0x00000090 System.Void Products::CreateShelf(System.Collections.Generic.List`1<BCProduct>,UnityEngine.RectTransform)
extern void Products_CreateShelf_m6C3254943C59C6AF6483D3CE1183B9AAC02B0A26 ();
// 0x00000091 System.Threading.Tasks.Task`1<UnityEngine.Sprite> Products::LoadSpriteAsync(System.String)
extern void Products_LoadSpriteAsync_mC38CD0B2F2D3D6D8C8CD333490DD8C31D20D311B ();
// 0x00000092 Products_ProductPlatform Products::Platform(System.String)
extern void Products_Platform_m34C1C72DE5E8D9514E554C2C6ED6B44A834E2D08 ();
// 0x00000093 System.Void Products::CreateAvailableNowShelf(System.Collections.Generic.List`1<BCProduct>)
extern void Products_CreateAvailableNowShelf_m494C37A0E4EF71E5A71AA2C480615B3152ABE541 ();
// 0x00000094 System.Void Products::.ctor()
extern void Products__ctor_m238D48CC213AA0F5BE50A9F62C0E3A62104CE9DA ();
// 0x00000095 System.Void imgHelper::Start()
extern void imgHelper_Start_m39B0EF63453BC90BE8A36F79290B5EA3FC0A785C ();
// 0x00000096 System.Void imgHelper::Update()
extern void imgHelper_Update_mBC747B291990811F95BF86F03E37A7BB2795B8E1 ();
// 0x00000097 System.Void imgHelper::.ctor()
extern void imgHelper__ctor_m3CC0E57EC3C668EAACFF82C8DC41699841D266B4 ();
// 0x00000098 System.Collections.IEnumerator ImageHelper::AssignImage(System.String,UnityEngine.UI.Image,UnityEngine.UI.Image)
extern void ImageHelper_AssignImage_m85A68A92F26C4F88D1016A7235EA75516ADC583F ();
// 0x00000099 System.Void ImageHelper::.cctor()
extern void ImageHelper__cctor_mE52A30A1D447670C992D9351B486B82C59161E19 ();
// 0x0000009A System.Void Pokemon::GetPokemonDetails()
extern void Pokemon_GetPokemonDetails_mDFC407DD8D07CDE269273377CF1424A94AA76A5B ();
// 0x0000009B System.Void Pokemon::GetAllPokemonDetails()
extern void Pokemon_GetAllPokemonDetails_mF308E582472BB0494BB3192475961BC451BB0B41 ();
// 0x0000009C System.Void Pokemon::.ctor()
extern void Pokemon__ctor_mAE34E9ADDBA3D214D58625ABA92A241428695E77 ();
// 0x0000009D System.Void User::OnEnable()
extern void User_OnEnable_mF94083E29267010C318E6A41C66FBD4F9418D0A9 ();
// 0x0000009E System.Void User::OnDisable()
extern void User_OnDisable_mFA6E9C5523B40DA0AC176EF868561AFE6FE5CC04 ();
// 0x0000009F System.Void User::GetQuery()
extern void User_GetQuery_mD4F44ED16D675B9AA90B7EB84416E57C88428661 ();
// 0x000000A0 System.Void User::CreateNewUser()
extern void User_CreateNewUser_m26F9C63BED9DB4C8AB776E25C85B732A38B1C3DD ();
// 0x000000A1 System.Void User::Subscribe()
extern void User_Subscribe_mC0DC37CE2992F3661DAD395138C758E0C585744F ();
// 0x000000A2 System.Void User::DisplayData(GraphQlClient.EventCallbacks.OnSubscriptionDataReceived)
extern void User_DisplayData_mA5A6360E3213A5C0FD5678E7682C42B9E2A75406 ();
// 0x000000A3 System.Void User::CancelSubscribe()
extern void User_CancelSubscribe_mB779B3D6E0E63DC17D61C343DD873C526D715852 ();
// 0x000000A4 System.Void User::.ctor()
extern void User__ctor_mB5B1D42F0980D69CF10E95B3BF591E010A500A53 ();
// 0x000000A5 System.Void GraphQlClient.EventCallbacks.OnRequestBegin::.ctor()
extern void OnRequestBegin__ctor_m4B4E9FBAF93678E92D26970EF91F059AF5B1B421 ();
// 0x000000A6 System.Void GraphQlClient.EventCallbacks.OnRequestEnded::.ctor(System.String)
extern void OnRequestEnded__ctor_mE5C31074EAD9146EFF7EC7D3A2107FEB38E12386 ();
// 0x000000A7 System.Void GraphQlClient.EventCallbacks.OnRequestEnded::.ctor(System.Exception)
extern void OnRequestEnded__ctor_m6DAC497C763EAA84D40680F8FFACCD6E1FC21F27 ();
// 0x000000A8 System.Void GraphQlClient.EventCallbacks.OnSubscriptionHandshakeComplete::.ctor()
extern void OnSubscriptionHandshakeComplete__ctor_m841B9C3E6A61CF06F018091ED3A9694A18E8D984 ();
// 0x000000A9 System.Void GraphQlClient.EventCallbacks.OnSubscriptionDataReceived::.ctor(System.String)
extern void OnSubscriptionDataReceived__ctor_m9538952603448852664A2A052A63C3711AA69D18 ();
// 0x000000AA System.Void GraphQlClient.EventCallbacks.OnSubscriptionCanceled::.ctor()
extern void OnSubscriptionCanceled__ctor_m3FECBBF983DEEC0C6BC219435C197343E8B9F114 ();
// 0x000000AB System.Void GraphQlClient.EventCallbacks.Event`1::add_listeners(GraphQlClient.EventCallbacks.Event`1_EventListener<T>)
// 0x000000AC System.Void GraphQlClient.EventCallbacks.Event`1::remove_listeners(GraphQlClient.EventCallbacks.Event`1_EventListener<T>)
// 0x000000AD System.Void GraphQlClient.EventCallbacks.Event`1::RegisterListener(GraphQlClient.EventCallbacks.Event`1_EventListener<T>)
// 0x000000AE System.Void GraphQlClient.EventCallbacks.Event`1::UnregisterListener(GraphQlClient.EventCallbacks.Event`1_EventListener<T>)
// 0x000000AF System.Void GraphQlClient.EventCallbacks.Event`1::FireEvent()
// 0x000000B0 System.Void GraphQlClient.EventCallbacks.Event`1::.ctor()
// 0x000000B1 System.Void GraphQlClient.Core.GraphApi::SetAuthToken(System.String)
extern void GraphApi_SetAuthToken_m81DC37A4D756FD282C6F01BDEBC48105527D8F86 ();
// 0x000000B2 GraphQlClient.Core.GraphApi_Query GraphQlClient.Core.GraphApi::GetQueryByName(System.String,GraphQlClient.Core.GraphApi_Query_Type)
extern void GraphApi_GetQueryByName_m75544143489C8FB1D15A07CB6C6804359F1C8559 ();
// 0x000000B3 System.Threading.Tasks.Task`1<UnityEngine.Networking.UnityWebRequest> GraphQlClient.Core.GraphApi::Post(GraphQlClient.Core.GraphApi_Query)
extern void GraphApi_Post_m2A34513BB46405BC50224F2ABDEF8B3067304107 ();
// 0x000000B4 System.Threading.Tasks.Task`1<UnityEngine.Networking.UnityWebRequest> GraphQlClient.Core.GraphApi::Post(System.String,GraphQlClient.Core.GraphApi_Query_Type)
extern void GraphApi_Post_mD242073B2AAE8304ED6E1F4FEE3EFB75A77F1DBF ();
// 0x000000B5 System.Threading.Tasks.Task`1<System.Net.WebSockets.ClientWebSocket> GraphQlClient.Core.GraphApi::Subscribe(GraphQlClient.Core.GraphApi_Query,System.String,System.String)
extern void GraphApi_Subscribe_mFF63698359845D79ED957173498A5ABC995300B5 ();
// 0x000000B6 System.Threading.Tasks.Task`1<System.Net.WebSockets.ClientWebSocket> GraphQlClient.Core.GraphApi::Subscribe(System.String,GraphQlClient.Core.GraphApi_Query_Type,System.String,System.String)
extern void GraphApi_Subscribe_m8E35A16D1403F7833954A30FEFD9BDDD145A313F ();
// 0x000000B7 System.Void GraphQlClient.Core.GraphApi::CancelSubscription(System.Net.WebSockets.ClientWebSocket,System.String)
extern void GraphApi_CancelSubscription_mCFAB059969A4C759658EB95938D9C50E0CA99B47 ();
// 0x000000B8 System.String GraphQlClient.Core.GraphApi::JsonToArgument(System.String)
extern void GraphApi_JsonToArgument_m1E98A2E2B4789D6DEC1AD0D96153F537E911A03D ();
// 0x000000B9 System.Void GraphQlClient.Core.GraphApi::.ctor()
extern void GraphApi__ctor_m2F9E6188A9A45AA0C382E9DE20FA98AB2E6806C0 ();
// 0x000000BA System.Void GraphQlClient.Core.EnumInputConverter::WriteJson(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.JsonSerializer)
extern void EnumInputConverter_WriteJson_m139FDF328A3BF2F6D7EF85F0A7924F6A18BDC2BA ();
// 0x000000BB System.Void GraphQlClient.Core.EnumInputConverter::.ctor()
extern void EnumInputConverter__ctor_m99EADBD3776D8B534D5F824CE9CC2B83A75A10C4 ();
// 0x000000BC System.Threading.Tasks.Task`1<UnityEngine.Networking.UnityWebRequest> GraphQlClient.Core.HttpHandler::PostAsync(System.String,System.String,System.String)
extern void HttpHandler_PostAsync_mC464CB2D668D5BBD23A7BFED734FC3669B9A5C45 ();
// 0x000000BD System.Threading.Tasks.Task`1<UnityEngine.Networking.UnityWebRequest> GraphQlClient.Core.HttpHandler::PostAsync(UnityEngine.Networking.UnityWebRequest,System.String)
extern void HttpHandler_PostAsync_m5B6D9C90AED01C17AA4F135C2BFB614CF0D2BEE1 ();
// 0x000000BE System.Threading.Tasks.Task`1<UnityEngine.Networking.UnityWebRequest> GraphQlClient.Core.HttpHandler::GetAsync(System.String,System.String)
extern void HttpHandler_GetAsync_m44D8E55019A91765F48649EB542CE5FFBE2AD1F1 ();
// 0x000000BF System.Threading.Tasks.Task`1<System.Net.WebSockets.ClientWebSocket> GraphQlClient.Core.HttpHandler::WebsocketConnect(System.String,System.String,System.String,System.String,System.String)
extern void HttpHandler_WebsocketConnect_mAED924B5C1DB15F5F0698E52FC53E0C940B57C2E ();
// 0x000000C0 System.Threading.Tasks.Task`1<System.Net.WebSockets.ClientWebSocket> GraphQlClient.Core.HttpHandler::WebsocketConnect(System.Net.WebSockets.ClientWebSocket,System.String,System.String,System.String)
extern void HttpHandler_WebsocketConnect_mAFF90437022793D3A18EACC814B9A354DD65F95F ();
// 0x000000C1 System.Threading.Tasks.Task GraphQlClient.Core.HttpHandler::WebsocketInit(System.Net.WebSockets.ClientWebSocket)
extern void HttpHandler_WebsocketInit_m713340C13B4ACCAAB2ED40F7EC29AD6D0DE0F5F0 ();
// 0x000000C2 System.Threading.Tasks.Task GraphQlClient.Core.HttpHandler::WebsocketSend(System.Net.WebSockets.ClientWebSocket,System.String,System.String)
extern void HttpHandler_WebsocketSend_mB1363DB317EA76CF8E0ED465C4859FDBE9F33FCA ();
// 0x000000C3 System.Void GraphQlClient.Core.HttpHandler::GetWsReturn(System.Net.WebSockets.ClientWebSocket)
extern void HttpHandler_GetWsReturn_m6016B7FB115ADD8EFD2808DABCC1726B32B59017 ();
// 0x000000C4 System.Threading.Tasks.Task GraphQlClient.Core.HttpHandler::WebsocketDisconnect(System.Net.WebSockets.ClientWebSocket,System.String)
extern void HttpHandler_WebsocketDisconnect_m39EB254D457A7AB06AEA8503A1F6732545807081 ();
// 0x000000C5 System.String GraphQlClient.Core.HttpHandler::FormatJson(System.String)
extern void HttpHandler_FormatJson_mDBD01BF5B31E13F024A4B7CA898FCB5BDB517B86 ();
// 0x000000C6 System.Void GraphQlClient.Core.HttpHandler::.ctor()
extern void HttpHandler__ctor_m83AA767A109B6FD8F8B9FE3D19426986A9E00571 ();
// 0x000000C7 System.Void GraphQlClient.Core.Introspection::.cctor()
extern void Introspection__cctor_m7BF4F26AC4B43959364375823299B3713FC9F74E ();
// 0x000000C8 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x000000C9 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865 ();
// 0x000000CA System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96 ();
// 0x000000CB SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E ();
// 0x000000CC System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26 ();
// 0x000000CD System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580 ();
// 0x000000CE System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76 ();
// 0x000000CF System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C ();
// 0x000000D0 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E ();
// 0x000000D1 System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0 ();
// 0x000000D2 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD ();
// 0x000000D3 System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461 ();
// 0x000000D4 System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31 ();
// 0x000000D5 System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60 ();
// 0x000000D6 System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m72DF6AD0574D74648194D898901C0F61C2EF29E8 ();
// 0x000000D7 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_mB44B0D5303F8644FB399963F8342F1915FED1EC2 ();
// 0x000000D8 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1 ();
// 0x000000D9 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4 ();
// 0x000000DA SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653 ();
// 0x000000DB SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE ();
// 0x000000DC SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118 ();
// 0x000000DD System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395 ();
// 0x000000DE System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F ();
// 0x000000DF System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75 ();
// 0x000000E0 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698 ();
// 0x000000E1 System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x000000E2 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x000000E3 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m4CA50FA2E097B82ED352580539F7215F181AE1EA ();
// 0x000000E4 SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3 ();
// 0x000000E5 SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5 ();
// 0x000000E6 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8 ();
// 0x000000E7 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098 ();
// 0x000000E8 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1 ();
// 0x000000E9 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C ();
// 0x000000EA System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC ();
// 0x000000EB System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63 ();
// 0x000000EC System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947 ();
// 0x000000ED System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7 ();
// 0x000000EE SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A ();
// 0x000000EF SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B ();
// 0x000000F0 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84 ();
// 0x000000F1 System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D ();
// 0x000000F2 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4 ();
// 0x000000F3 System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487 ();
// 0x000000F4 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837 ();
// 0x000000F5 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552 ();
// 0x000000F6 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5 ();
// 0x000000F7 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18 ();
// 0x000000F8 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477 ();
// 0x000000F9 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099 ();
// 0x000000FA SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_mFB610B47429EC263B56050700A6D74F38A93FCC1 ();
// 0x000000FB System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E ();
// 0x000000FC System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5 ();
// 0x000000FD System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0 ();
// 0x000000FE System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC ();
// 0x000000FF System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_mE5AC264BA62AA54B0DCDCB165564F5EE28B50653 ();
// 0x00000100 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6 ();
// 0x00000101 System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69 ();
// 0x00000102 SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9 ();
// 0x00000103 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE ();
// 0x00000104 System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD ();
// 0x00000105 System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_m701D46F6554CE60CD9967EA4598C51C1C941ED2B ();
// 0x00000106 System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m736B5C89F41DE7A7CCAC0FC6160CF4FD50B989DC ();
// 0x00000107 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE ();
// 0x00000108 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03 ();
// 0x00000109 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9 ();
// 0x0000010A SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10 ();
// 0x0000010B System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885 ();
// 0x0000010C SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215 ();
// 0x0000010D System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE ();
// 0x0000010E System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647 ();
// 0x0000010F System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D ();
// 0x00000110 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F ();
// 0x00000111 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049 ();
// 0x00000112 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85 ();
// 0x00000113 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C ();
// 0x00000114 System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA ();
// 0x00000115 System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_m68C30F81DBE34CC9512A1CF26E501A9042CEE6B2 ();
// 0x00000116 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mE4E0D0ABDE60706A3BE440D751C80D45E565078E ();
// 0x00000117 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486 ();
// 0x00000118 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD ();
// 0x00000119 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A ();
// 0x0000011A SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF ();
// 0x0000011B System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B ();
// 0x0000011C SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976 ();
// 0x0000011D System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E ();
// 0x0000011E System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF ();
// 0x0000011F System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D ();
// 0x00000120 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7 ();
// 0x00000121 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE ();
// 0x00000122 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15 ();
// 0x00000123 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6 ();
// 0x00000124 System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E ();
// 0x00000125 System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F ();
// 0x00000126 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185 ();
// 0x00000127 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E ();
// 0x00000128 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4 ();
// 0x00000129 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A ();
// 0x0000012A System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF ();
// 0x0000012B System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA ();
// 0x0000012C System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8 ();
// 0x0000012D System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09 ();
// 0x0000012E System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1 ();
// 0x0000012F SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F ();
// 0x00000130 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912 ();
// 0x00000131 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0 ();
// 0x00000132 System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB ();
// 0x00000133 System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0 ();
// 0x00000134 System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399 ();
// 0x00000135 System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68 ();
// 0x00000136 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8 ();
// 0x00000137 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F ();
// 0x00000138 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B ();
// 0x00000139 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887 ();
// 0x0000013A System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931 ();
// 0x0000013B System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5 ();
// 0x0000013C SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A ();
// 0x0000013D System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA ();
// 0x0000013E SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180 ();
// 0x0000013F System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6 ();
// 0x00000140 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A ();
// 0x00000141 System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9 ();
// 0x00000142 System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91 ();
// 0x00000143 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD ();
// 0x00000144 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE ();
// 0x00000145 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9 ();
// 0x00000146 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09 ();
// 0x00000147 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A ();
// 0x00000148 SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7 ();
// 0x00000149 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F ();
// 0x0000014A SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936 ();
// 0x0000014B System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA ();
// 0x0000014C SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79 ();
// 0x0000014D System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60 ();
// 0x0000014E System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922 ();
// 0x0000014F System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98 ();
// 0x00000150 System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000 ();
// 0x00000151 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600 ();
// 0x00000152 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B ();
// 0x00000153 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E ();
// 0x00000154 System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_m0E80AEF0AD9DCA6A8018A55DAA8464FCA2DBCC16 ();
// 0x00000155 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D ();
// 0x00000156 SimpleJSON.JSONNode_Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6 ();
// 0x00000157 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798 ();
// 0x00000158 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188 ();
// 0x00000159 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779 ();
// 0x0000015A SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047 ();
// 0x0000015B System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606 ();
// 0x0000015C SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C ();
// 0x0000015D System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC ();
// 0x0000015E System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670 ();
// 0x0000015F System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F ();
// 0x00000160 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7 ();
// 0x00000161 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95 ();
// 0x00000162 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E ();
// 0x00000163 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43 ();
// 0x00000164 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A ();
// 0x00000165 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857 ();
// 0x00000166 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870 ();
// 0x00000167 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934 ();
// 0x00000168 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465 ();
// 0x00000169 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350 ();
// 0x0000016A System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE ();
// 0x0000016B System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2 ();
// 0x0000016C SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D ();
// 0x0000016D SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2 ();
// 0x0000016E System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F ();
// 0x0000016F SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9 ();
// 0x00000170 System.Void UnityEngine.UI.ProceduralImage.ModifierID::.ctor(System.String)
extern void ModifierID__ctor_m5F106BF20600F0C82C1EBA566340755C75B4281F ();
// 0x00000171 System.String UnityEngine.UI.ProceduralImage.ModifierID::get_Name()
extern void ModifierID_get_Name_m71B85ED39E93CD5752ECCE9321DB4D74E15EC7DE ();
// 0x00000172 UnityEngine.Material UnityEngine.UI.ProceduralImage.ProceduralImage::get_DefaultProceduralImageMaterial()
extern void ProceduralImage_get_DefaultProceduralImageMaterial_m2B93AC078A6EF6961C86FA30BC8397C9FE5A52E5 ();
// 0x00000173 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_DefaultProceduralImageMaterial(UnityEngine.Material)
extern void ProceduralImage_set_DefaultProceduralImageMaterial_m7FA6165E9D9717DF41B972F6778D3A9A3D2D54CD ();
// 0x00000174 System.Single UnityEngine.UI.ProceduralImage.ProceduralImage::get_BorderWidth()
extern void ProceduralImage_get_BorderWidth_m44936B2395491E0FC18F6317716A334CEB333D68 ();
// 0x00000175 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_BorderWidth(System.Single)
extern void ProceduralImage_set_BorderWidth_m41DAAA6F57046E6E0C34E02D692552A482323A07 ();
// 0x00000176 System.Single UnityEngine.UI.ProceduralImage.ProceduralImage::get_FalloffDistance()
extern void ProceduralImage_get_FalloffDistance_m0D2AB1A4C484EA8FB7E450B4CB3082BE5A8E228F ();
// 0x00000177 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_FalloffDistance(System.Single)
extern void ProceduralImage_set_FalloffDistance_mF9A081889C7A954A8D8D10C92CF89EFCA224B4AB ();
// 0x00000178 UnityEngine.UI.ProceduralImage.ProceduralImageModifier UnityEngine.UI.ProceduralImage.ProceduralImage::get_Modifier()
extern void ProceduralImage_get_Modifier_m9ADB9C7BB714FF1C1BA1400CF1C31AA3E9211A36 ();
// 0x00000179 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_Modifier(UnityEngine.UI.ProceduralImage.ProceduralImageModifier)
extern void ProceduralImage_set_Modifier_mBB7D6EA0696D9CC7D7EDC07EB4FFCE36AAF3D23B ();
// 0x0000017A System.Type UnityEngine.UI.ProceduralImage.ProceduralImage::get_ModifierType()
extern void ProceduralImage_get_ModifierType_m1A03DBEE862E01C5FFC4289532E4BA3AB8705994 ();
// 0x0000017B System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_ModifierType(System.Type)
extern void ProceduralImage_set_ModifierType_m1BE072F26D1532B3910CC25B9B65D58F189520E4 ();
// 0x0000017C System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::OnEnable()
extern void ProceduralImage_OnEnable_m6CE8D56086FFCB84EFEA7D100D8B01D23A53BF4B ();
// 0x0000017D System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::OnDisable()
extern void ProceduralImage_OnDisable_m2C8FEAA36A9D13E84F4936B2B9BE481AE26EB350 ();
// 0x0000017E System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::Init()
extern void ProceduralImage_Init_mB795B8891E67E9B232F8461DC64B28996CCC8ACD ();
// 0x0000017F System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::OnVerticesDirty()
extern void ProceduralImage_OnVerticesDirty_m3FA8B1245E66CAA9EB8E68FC38A8A40B3FA55446 ();
// 0x00000180 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::FixTexCoordsInCanvas()
extern void ProceduralImage_FixTexCoordsInCanvas_mBD5B18E472AD9C1033FDFA04352A951F0F8CBE6E ();
// 0x00000181 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::FixTexCoordsInCanvas(UnityEngine.Canvas)
extern void ProceduralImage_FixTexCoordsInCanvas_m3BFC3EEA9B7E5FEC343C2869957275BC6E6C7466 ();
// 0x00000182 UnityEngine.Vector4 UnityEngine.UI.ProceduralImage.ProceduralImage::FixRadius(UnityEngine.Vector4)
extern void ProceduralImage_FixRadius_m3B0A607A8AFDC8BE47CEE0439C807DEF9210CB2B ();
// 0x00000183 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void ProceduralImage_OnPopulateMesh_mBCD26751981759405BF2B209B1990F3387A8AAF9 ();
// 0x00000184 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::OnTransformParentChanged()
extern void ProceduralImage_OnTransformParentChanged_mDF28056385945EA13A634819548121BBD12538AC ();
// 0x00000185 UnityEngine.UI.ProceduralImage.ProceduralImageInfo UnityEngine.UI.ProceduralImage.ProceduralImage::CalculateInfo()
extern void ProceduralImage_CalculateInfo_m93C5501C9DB8B4D1CBE047DB16F1DD83230EE3BA ();
// 0x00000186 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::EncodeAllInfoIntoVertices(UnityEngine.UI.VertexHelper,UnityEngine.UI.ProceduralImage.ProceduralImageInfo)
extern void ProceduralImage_EncodeAllInfoIntoVertices_mE3C8CD10CC1435C87A2AD19F90D5BD1180035ED2 ();
// 0x00000187 System.Single UnityEngine.UI.ProceduralImage.ProceduralImage::EncodeFloats_0_1_16_16(System.Single,System.Single)
extern void ProceduralImage_EncodeFloats_0_1_16_16_mF3640DFFDF2E7A4FFADAEEF85FBF3095B0921304 ();
// 0x00000188 UnityEngine.Material UnityEngine.UI.ProceduralImage.ProceduralImage::get_material()
extern void ProceduralImage_get_material_mE0B65000AE6BE8F93556022653D13A29361502AA ();
// 0x00000189 System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::set_material(UnityEngine.Material)
extern void ProceduralImage_set_material_m768E879F54C8096CDE1926DBA3FEE42133D1C338 ();
// 0x0000018A System.Void UnityEngine.UI.ProceduralImage.ProceduralImage::.ctor()
extern void ProceduralImage__ctor_m3EB96A165C01173AE09C10DC6825070C0E86D477 ();
// 0x0000018B System.Void UnityEngine.UI.ProceduralImage.ProceduralImageInfo::.ctor(System.Single,System.Single,System.Single,System.Single,UnityEngine.Vector4,System.Single)
extern void ProceduralImageInfo__ctor_m887DCAEEC8C986B2C25A5EBD20E08C3D8D8A7892_AdjustorThunk ();
// 0x0000018C UnityEngine.UI.Graphic UnityEngine.UI.ProceduralImage.ProceduralImageModifier::get__Graphic()
extern void ProceduralImageModifier_get__Graphic_m2E3CD53C1477B30D4E79555D2F46FD3E9D28B2D9 ();
// 0x0000018D UnityEngine.Vector4 UnityEngine.UI.ProceduralImage.ProceduralImageModifier::CalculateRadius(UnityEngine.Rect)
// 0x0000018E System.Void UnityEngine.UI.ProceduralImage.ProceduralImageModifier::.ctor()
extern void ProceduralImageModifier__ctor_mBD73645FA6EFE4CA1FE083A68498D14F508CCC38 ();
// 0x0000018F System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA ();
// 0x00000190 System.Void JsonHelper_Wrapper`1::.ctor()
// 0x00000191 System.Void ShopifyQuery_<test>d__7::MoveNext()
extern void U3CtestU3Ed__7_MoveNext_mEC09EF94350342F0F27B65619F569D46244D5F9B_AdjustorThunk ();
// 0x00000192 System.Void ShopifyQuery_<test>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CtestU3Ed__7_SetStateMachine_mFF0D433F51B5BD30946351EFD53DF87BC5A8A376_AdjustorThunk ();
// 0x00000193 System.Void ShopifyQuery_<getShop>d__8::MoveNext()
extern void U3CgetShopU3Ed__8_MoveNext_m03A6ABDA9E04BCC7654A615719297B52D84A6C43_AdjustorThunk ();
// 0x00000194 System.Void ShopifyQuery_<getShop>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CgetShopU3Ed__8_SetStateMachine_m259B96DF59E88B64350AF78AF5251184B4C77BE3_AdjustorThunk ();
// 0x00000195 System.Void ShopifyQuery_<getProducts>d__9::MoveNext()
extern void U3CgetProductsU3Ed__9_MoveNext_mD978CA984FF42201930EC9FC63CBD1FC91CE0905_AdjustorThunk ();
// 0x00000196 System.Void ShopifyQuery_<getProducts>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CgetProductsU3Ed__9_SetStateMachine_m731FF40675262290575355F4E9876526DCB7017C_AdjustorThunk ();
// 0x00000197 System.Void ShopifyQuery_<getCustomerAcess>d__10::MoveNext()
extern void U3CgetCustomerAcessU3Ed__10_MoveNext_mF8924CD85E7288792C00C59036FA30A0C38E5FD4_AdjustorThunk ();
// 0x00000198 System.Void ShopifyQuery_<getCustomerAcess>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CgetCustomerAcessU3Ed__10_SetStateMachine_m672DB2DF2BDF5356C3552F61FC7445EBF22141A9_AdjustorThunk ();
// 0x00000199 System.Void ShopifyQuery_<getCustomer>d__11::MoveNext()
extern void U3CgetCustomerU3Ed__11_MoveNext_mD1B1E0F18872E7A2F033299FDBB463EE57B002C5_AdjustorThunk ();
// 0x0000019A System.Void ShopifyQuery_<getCustomer>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CgetCustomerU3Ed__11_SetStateMachine_m5F5F0D23059F01D75F90A54D4904226F11AE8C57_AdjustorThunk ();
// 0x0000019B System.Void ShopifyQuery_<createNewCustomer>d__12::MoveNext()
extern void U3CcreateNewCustomerU3Ed__12_MoveNext_mC20762E90330627B7E152369A45B9D6F61BF2990_AdjustorThunk ();
// 0x0000019C System.Void ShopifyQuery_<createNewCustomer>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CcreateNewCustomerU3Ed__12_SetStateMachine_mF86C1B2582437A303295198DD2890D0E198CD27C_AdjustorThunk ();
// 0x0000019D System.Void ShopifyQuery_<createCheckout>d__13::MoveNext()
extern void U3CcreateCheckoutU3Ed__13_MoveNext_mCAE52AC09D3BDFE9D31494EB30A040B2849A1098_AdjustorThunk ();
// 0x0000019E System.Void ShopifyQuery_<createCheckout>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CcreateCheckoutU3Ed__13_SetStateMachine_mE46465C839A0F246443CAD527BC39D1B810707BE_AdjustorThunk ();
// 0x0000019F System.Void ShopifyQuery_<associateCustomerCheckout>d__14::MoveNext()
extern void U3CassociateCustomerCheckoutU3Ed__14_MoveNext_m8000FD1E81721500FE1016CBC765A59FB8EB777B_AdjustorThunk ();
// 0x000001A0 System.Void ShopifyQuery_<associateCustomerCheckout>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CassociateCustomerCheckoutU3Ed__14_SetStateMachine_m9E0FEC83C8FE39BDF2E9C92DA44694FDDF15EC71_AdjustorThunk ();
// 0x000001A1 System.Void ShopifyQuery_<testUnitySDK>d__15::MoveNext()
extern void U3CtestUnitySDKU3Ed__15_MoveNext_m623FA2956BD674AB9CA282F16555DD2AA73C346A_AdjustorThunk ();
// 0x000001A2 System.Void ShopifyQuery_<testUnitySDK>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CtestUnitySDKU3Ed__15_SetStateMachine_m7C2DA245799F81F6034572E5CBCF20529932AA77_AdjustorThunk ();
// 0x000001A3 System.Void AnimateSidebar_<MoveIn>d__8::.ctor(System.Int32)
extern void U3CMoveInU3Ed__8__ctor_mF171C7271535F8351321D2765FFED135D8AA6594 ();
// 0x000001A4 System.Void AnimateSidebar_<MoveIn>d__8::System.IDisposable.Dispose()
extern void U3CMoveInU3Ed__8_System_IDisposable_Dispose_mC477FB900662095C9D775B60B5984187CF3363DB ();
// 0x000001A5 System.Boolean AnimateSidebar_<MoveIn>d__8::MoveNext()
extern void U3CMoveInU3Ed__8_MoveNext_m3B619020B06D9BBDCEF639C4B6EA9BEED49F1475 ();
// 0x000001A6 System.Object AnimateSidebar_<MoveIn>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveInU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8DB807A7E75C2093E562EB6422E7DF2CBFE9D00 ();
// 0x000001A7 System.Void AnimateSidebar_<MoveIn>d__8::System.Collections.IEnumerator.Reset()
extern void U3CMoveInU3Ed__8_System_Collections_IEnumerator_Reset_m3CCB5EDB0A028DC8E49A46D269715BF68D19E8BA ();
// 0x000001A8 System.Object AnimateSidebar_<MoveIn>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CMoveInU3Ed__8_System_Collections_IEnumerator_get_Current_m5E51A4374E5A77BA79E175235B9996B3152BBA7E ();
// 0x000001A9 System.Void AnimateSidebar_<MoveOut>d__10::.ctor(System.Int32)
extern void U3CMoveOutU3Ed__10__ctor_mAC5E2A00D61FBC3DA5F58FA5C1B005F7D68AEB93 ();
// 0x000001AA System.Void AnimateSidebar_<MoveOut>d__10::System.IDisposable.Dispose()
extern void U3CMoveOutU3Ed__10_System_IDisposable_Dispose_mDD47322020AB68CCA69D33EF7E53A5E0732204EB ();
// 0x000001AB System.Boolean AnimateSidebar_<MoveOut>d__10::MoveNext()
extern void U3CMoveOutU3Ed__10_MoveNext_m505EDB7A745481DD3D3CA238987AA6AF5E92AED0 ();
// 0x000001AC System.Object AnimateSidebar_<MoveOut>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveOutU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A436BE6AA7DE1A7A2B0D13D17E85E05D0F71C0E ();
// 0x000001AD System.Void AnimateSidebar_<MoveOut>d__10::System.Collections.IEnumerator.Reset()
extern void U3CMoveOutU3Ed__10_System_Collections_IEnumerator_Reset_mE5CD1A00E3B9C7B0EE569EC4D7C4F13D27626739 ();
// 0x000001AE System.Object AnimateSidebar_<MoveOut>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CMoveOutU3Ed__10_System_Collections_IEnumerator_get_Current_m8DE85E58EE6EA2410A321DFC24778065FE8EF2B8 ();
// 0x000001AF System.Void Banners_<CreateBanner>d__6::MoveNext()
extern void U3CCreateBannerU3Ed__6_MoveNext_m850F70C047618942F1205EA2A054D153480B27A6_AdjustorThunk ();
// 0x000001B0 System.Void Banners_<CreateBanner>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateBannerU3Ed__6_SetStateMachine_m38C65DA25F7D09357E21B8ACFDA099288BD3079B_AdjustorThunk ();
// 0x000001B1 System.Void Banners_<LoadSpriteAsync>d__7::MoveNext()
extern void U3CLoadSpriteAsyncU3Ed__7_MoveNext_m8C027D62235337B44C668550C601AB820D3B14A3_AdjustorThunk ();
// 0x000001B2 System.Void Banners_<LoadSpriteAsync>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadSpriteAsyncU3Ed__7_SetStateMachine_m8E3B7711B1654AC590505648E95346FB5D3DF3C5_AdjustorThunk ();
// 0x000001B3 System.Void MenuButtons_<>c::.cctor()
extern void U3CU3Ec__cctor_m403144039879519538A84C85D35376E3A4D54CD0 ();
// 0x000001B4 System.Void MenuButtons_<>c::.ctor()
extern void U3CU3Ec__ctor_mF6B6785B90C26283B9B8D3CD8B31AC5087BCA456 ();
// 0x000001B5 System.Boolean MenuButtons_<>c::<Start>b__4_0(MenuButton)
extern void U3CU3Ec_U3CStartU3Eb__4_0_m673F9209D5EE169410529BD14A2B932D943B69DD ();
// 0x000001B6 System.Void Pages_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m26F027E770FE7C458903A4A5103F94E3815C278D ();
// 0x000001B7 System.Boolean Pages_<>c__DisplayClass8_0::<OpenPage>b__0(Page)
extern void U3CU3Ec__DisplayClass8_0_U3COpenPageU3Eb__0_m6FE7D4AB1B7825A48F5E5230AF90012668BA9CE8 ();
// 0x000001B8 System.Void Products_<CreateShelf>d__4::MoveNext()
extern void U3CCreateShelfU3Ed__4_MoveNext_m7239C750ACCC56E7CC17B195E98C6CE46770E91D_AdjustorThunk ();
// 0x000001B9 System.Void Products_<CreateShelf>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateShelfU3Ed__4_SetStateMachine_m54A7EFB7EF8521CEAA640DDDC50B10EF6AA1004E_AdjustorThunk ();
// 0x000001BA System.Void Products_<LoadSpriteAsync>d__5::MoveNext()
extern void U3CLoadSpriteAsyncU3Ed__5_MoveNext_mC49E9488C7A73E2AFD4EB1F17EE4DA03D75C365A_AdjustorThunk ();
// 0x000001BB System.Void Products_<LoadSpriteAsync>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadSpriteAsyncU3Ed__5_SetStateMachine_m04658E0808A4464CE33CD234E94EB9FB5BBC27CA_AdjustorThunk ();
// 0x000001BC System.Void ImageHelper_<AssignImage>d__1::.ctor(System.Int32)
extern void U3CAssignImageU3Ed__1__ctor_mC845F359D8ECCC848367D98F3EF5E65239387154 ();
// 0x000001BD System.Void ImageHelper_<AssignImage>d__1::System.IDisposable.Dispose()
extern void U3CAssignImageU3Ed__1_System_IDisposable_Dispose_m15BDAA8E622170184A42522EA94AC8BC5A47B78F ();
// 0x000001BE System.Boolean ImageHelper_<AssignImage>d__1::MoveNext()
extern void U3CAssignImageU3Ed__1_MoveNext_mE330D5637F655BF589B58FB732A7CFE73B70AF6C ();
// 0x000001BF System.Object ImageHelper_<AssignImage>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssignImageU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAC5365D28A42F94263A25BB469E278D06C02D1C ();
// 0x000001C0 System.Void ImageHelper_<AssignImage>d__1::System.Collections.IEnumerator.Reset()
extern void U3CAssignImageU3Ed__1_System_Collections_IEnumerator_Reset_mBEF47B6CCD96FF9A36F73CB4834367D2ED496559 ();
// 0x000001C1 System.Object ImageHelper_<AssignImage>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CAssignImageU3Ed__1_System_Collections_IEnumerator_get_Current_mB47CE0D19A553FCF8AC55ECBF96F8C311877879E ();
// 0x000001C2 System.Void Pokemon_<GetPokemonDetails>d__3::MoveNext()
extern void U3CGetPokemonDetailsU3Ed__3_MoveNext_m5D0FFB9838E7E674A540B3AD95EF431208222CCE_AdjustorThunk ();
// 0x000001C3 System.Void Pokemon_<GetPokemonDetails>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetPokemonDetailsU3Ed__3_SetStateMachine_mF02B375D142F7627184FC2ED64BFD5052734D91C_AdjustorThunk ();
// 0x000001C4 System.Void Pokemon_<GetAllPokemonDetails>d__4::MoveNext()
extern void U3CGetAllPokemonDetailsU3Ed__4_MoveNext_m368A83393024CCA8235890229A5B278196294B1A_AdjustorThunk ();
// 0x000001C5 System.Void Pokemon_<GetAllPokemonDetails>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetAllPokemonDetailsU3Ed__4_SetStateMachine_m3B3AA35810C73C5012CAF9F48DAB7D354986E4B7_AdjustorThunk ();
// 0x000001C6 System.Void User_<GetQuery>d__10::MoveNext()
extern void U3CGetQueryU3Ed__10_MoveNext_mC0AFB1E7EB56ECEF3ABE6174E3A8B2873AD73796_AdjustorThunk ();
// 0x000001C7 System.Void User_<GetQuery>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetQueryU3Ed__10_SetStateMachine_mE56DDF57315AF7F7944177AC74D8CB77DDA452E0_AdjustorThunk ();
// 0x000001C8 System.Void User_<CreateNewUser>d__11::MoveNext()
extern void U3CCreateNewUserU3Ed__11_MoveNext_m51FBE38A26740C3C78E2226AD7130F298D81CF7F_AdjustorThunk ();
// 0x000001C9 System.Void User_<CreateNewUser>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateNewUserU3Ed__11_SetStateMachine_m1364A325FE3496BD6B02102A658883CC0E6EF441_AdjustorThunk ();
// 0x000001CA System.Void User_<Subscribe>d__12::MoveNext()
extern void U3CSubscribeU3Ed__12_MoveNext_mFD91B6DDD6376EC0AC9EB89FEFC7A20B296B1F45_AdjustorThunk ();
// 0x000001CB System.Void User_<Subscribe>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSubscribeU3Ed__12_SetStateMachine_m6F952EDD902574D9EAEE5EDC0781B45F4BDBED36_AdjustorThunk ();
// 0x000001CC System.Void GraphQlClient.EventCallbacks.Event`1_EventListener::.ctor(System.Object,System.IntPtr)
// 0x000001CD System.Void GraphQlClient.EventCallbacks.Event`1_EventListener::Invoke(T)
// 0x000001CE System.IAsyncResult GraphQlClient.EventCallbacks.Event`1_EventListener::BeginInvoke(T,System.AsyncCallback,System.Object)
// 0x000001CF System.Void GraphQlClient.EventCallbacks.Event`1_EventListener::EndInvoke(System.IAsyncResult)
// 0x000001D0 System.Void GraphQlClient.Core.GraphApi_Query::SetArgs(System.Object)
extern void Query_SetArgs_mE56D2FFD4866711F783EB7DB7D6E7A7438D6B813 ();
// 0x000001D1 System.Void GraphQlClient.Core.GraphApi_Query::SetArgs(System.String)
extern void Query_SetArgs_m592C897D2A0928F25138ECBEAC85EE0DE38D136E ();
// 0x000001D2 System.Void GraphQlClient.Core.GraphApi_Query::CompleteQuery()
extern void Query_CompleteQuery_mD91D3353A01486739B08033E79E066C2447D8D18 ();
// 0x000001D3 System.String GraphQlClient.Core.GraphApi_Query::GenerateStringTabs(System.Int32)
extern void Query_GenerateStringTabs_mEAF1B7360792B64E3B846456F1F1B710F7D2BEDA ();
// 0x000001D4 System.Void GraphQlClient.Core.GraphApi_Query::.ctor()
extern void Query__ctor_m994B9E9474A7256DFAD96DBEF2F0C48A7E3C801E ();
// 0x000001D5 System.Int32 GraphQlClient.Core.GraphApi_Field::get_Index()
extern void Field_get_Index_m796AA89D70FFD1F5DB34B40ACC0FC6A22D48F76E ();
// 0x000001D6 System.Void GraphQlClient.Core.GraphApi_Field::set_Index(System.Int32)
extern void Field_set_Index_mC2E1BF6CB3DF2D97FA79227C6D53707EBB740F63 ();
// 0x000001D7 System.Void GraphQlClient.Core.GraphApi_Field::.ctor()
extern void Field__ctor_m416556668865E99DB5530F41D563BB018B3FCB25 ();
// 0x000001D8 System.Void GraphQlClient.Core.GraphApi_Field::CheckSubFields(GraphQlClient.Core.Introspection_SchemaClass)
extern void Field_CheckSubFields_m8204E75739749663D9E9566D8C49EFC8713026F2 ();
// 0x000001D9 GraphQlClient.Core.GraphApi_Field GraphQlClient.Core.GraphApi_Field::op_Explicit(GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Type_Field)
extern void Field_op_Explicit_mFEA6B100D884BA684B976D171E88CA2F93A42AE9 ();
// 0x000001DA System.Boolean GraphQlClient.Core.GraphApi_Field::<CheckSubFields>b__11_0(GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Type)
extern void Field_U3CCheckSubFieldsU3Eb__11_0_m6BEB5D53B2867D82019B86C09752AE022B8EEBBA ();
// 0x000001DB System.Void GraphQlClient.Core.GraphApi_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mFF4BF1060A6C8D135FFF3D7F78B79CDB778D1EC4 ();
// 0x000001DC System.Boolean GraphQlClient.Core.GraphApi_<>c__DisplayClass13_0::<GetQueryByName>b__0(GraphQlClient.Core.GraphApi_Query)
extern void U3CU3Ec__DisplayClass13_0_U3CGetQueryByNameU3Eb__0_m7D65E18DA82BB3523D9AC637EC7C047E145F7373 ();
// 0x000001DD System.Void GraphQlClient.Core.GraphApi_<Post>d__14::MoveNext()
extern void U3CPostU3Ed__14_MoveNext_mFC5FDF2A66308F9F896E6BB36FD631A905975624_AdjustorThunk ();
// 0x000001DE System.Void GraphQlClient.Core.GraphApi_<Post>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostU3Ed__14_SetStateMachine_mEB8070681A2E1DB7B1F0A3C0FA66646487BB7189_AdjustorThunk ();
// 0x000001DF System.Void GraphQlClient.Core.GraphApi_<Post>d__15::MoveNext()
extern void U3CPostU3Ed__15_MoveNext_m205BC4F21BDF8A4EA482A306AAC5A29E057F388F_AdjustorThunk ();
// 0x000001E0 System.Void GraphQlClient.Core.GraphApi_<Post>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostU3Ed__15_SetStateMachine_mE2D43D672EB55D744F738C89FA7782C917720577_AdjustorThunk ();
// 0x000001E1 System.Void GraphQlClient.Core.GraphApi_<Subscribe>d__16::MoveNext()
extern void U3CSubscribeU3Ed__16_MoveNext_mDB0F924329F2973639E439A6DE9F887DDC499B57_AdjustorThunk ();
// 0x000001E2 System.Void GraphQlClient.Core.GraphApi_<Subscribe>d__16::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSubscribeU3Ed__16_SetStateMachine_mB304552F72C462DFB852429671A91337CA6B44C0_AdjustorThunk ();
// 0x000001E3 System.Void GraphQlClient.Core.GraphApi_<Subscribe>d__17::MoveNext()
extern void U3CSubscribeU3Ed__17_MoveNext_mDC6EC51838F57043532F15E2D55BC0F260ABA675_AdjustorThunk ();
// 0x000001E4 System.Void GraphQlClient.Core.GraphApi_<Subscribe>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSubscribeU3Ed__17_SetStateMachine_m6113C658D296A76EDA53DEFF1715BC0B8E760221_AdjustorThunk ();
// 0x000001E5 System.Void GraphQlClient.Core.GraphApi_<CancelSubscription>d__18::MoveNext()
extern void U3CCancelSubscriptionU3Ed__18_MoveNext_m331E2C520AD503FDA3391B7DA1B6396B84DC7D1D_AdjustorThunk ();
// 0x000001E6 System.Void GraphQlClient.Core.GraphApi_<CancelSubscription>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCancelSubscriptionU3Ed__18_SetStateMachine_m89910B6EDBD402C74C1FB15CF924EEA854B2BA11_AdjustorThunk ();
// 0x000001E7 System.Void GraphQlClient.Core.HttpHandler_<PostAsync>d__0::MoveNext()
extern void U3CPostAsyncU3Ed__0_MoveNext_mA16A1118311C7EA9B4CE6148ACBCD411C11FD0A2_AdjustorThunk ();
// 0x000001E8 System.Void GraphQlClient.Core.HttpHandler_<PostAsync>d__0::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostAsyncU3Ed__0_SetStateMachine_mF5697FB590A6778CDC64ACA08951114BC091497B_AdjustorThunk ();
// 0x000001E9 System.Void GraphQlClient.Core.HttpHandler_<PostAsync>d__1::MoveNext()
extern void U3CPostAsyncU3Ed__1_MoveNext_m5789B03F8DA862974F15BFB84FCA8CF69E7675F9_AdjustorThunk ();
// 0x000001EA System.Void GraphQlClient.Core.HttpHandler_<PostAsync>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CPostAsyncU3Ed__1_SetStateMachine_mB586F8D1D65FEBA7179030D7438933F5A8E5A201_AdjustorThunk ();
// 0x000001EB System.Void GraphQlClient.Core.HttpHandler_<GetAsync>d__2::MoveNext()
extern void U3CGetAsyncU3Ed__2_MoveNext_mA2E945B171CA937517C5C16C16BA0B1A71ACBBDF_AdjustorThunk ();
// 0x000001EC System.Void GraphQlClient.Core.HttpHandler_<GetAsync>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetAsyncU3Ed__2_SetStateMachine_m7603FA90FB8643331936CD6480349DA31FEBDF43_AdjustorThunk ();
// 0x000001ED System.Void GraphQlClient.Core.HttpHandler_<WebsocketConnect>d__3::MoveNext()
extern void U3CWebsocketConnectU3Ed__3_MoveNext_mA5DDC7EF020B6DD1E05C307838E849E1145BBD52_AdjustorThunk ();
// 0x000001EE System.Void GraphQlClient.Core.HttpHandler_<WebsocketConnect>d__3::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWebsocketConnectU3Ed__3_SetStateMachine_m0DDA656BD5DBCB6BEA02125B316A493A4CE36CAD_AdjustorThunk ();
// 0x000001EF System.Void GraphQlClient.Core.HttpHandler_<WebsocketConnect>d__4::MoveNext()
extern void U3CWebsocketConnectU3Ed__4_MoveNext_m51932D8DC33D58F6E348C9B3F2A4E4CB450A6F58_AdjustorThunk ();
// 0x000001F0 System.Void GraphQlClient.Core.HttpHandler_<WebsocketConnect>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWebsocketConnectU3Ed__4_SetStateMachine_mA516EC6FCF67B6DBF35AA5EA26D5C3B1F4E3BD84_AdjustorThunk ();
// 0x000001F1 System.Void GraphQlClient.Core.HttpHandler_<WebsocketInit>d__5::MoveNext()
extern void U3CWebsocketInitU3Ed__5_MoveNext_m106748CFAB6E118C01DC802D4196584724FFC1E7_AdjustorThunk ();
// 0x000001F2 System.Void GraphQlClient.Core.HttpHandler_<WebsocketInit>d__5::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWebsocketInitU3Ed__5_SetStateMachine_m43F25D84DE24BB8245B3BC5DE86B0BAFD58883D3_AdjustorThunk ();
// 0x000001F3 System.Void GraphQlClient.Core.HttpHandler_<WebsocketSend>d__6::MoveNext()
extern void U3CWebsocketSendU3Ed__6_MoveNext_mD7B583212339CF5B993DCB3F99968BF742BB09A1_AdjustorThunk ();
// 0x000001F4 System.Void GraphQlClient.Core.HttpHandler_<WebsocketSend>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWebsocketSendU3Ed__6_SetStateMachine_m0B8034BBEE3701B86882A302C863E9A887B6FED3_AdjustorThunk ();
// 0x000001F5 System.Void GraphQlClient.Core.HttpHandler_<GetWsReturn>d__7::MoveNext()
extern void U3CGetWsReturnU3Ed__7_MoveNext_mC0C3EE386E279A6827566275AEF4AF24F4B752C0_AdjustorThunk ();
// 0x000001F6 System.Void GraphQlClient.Core.HttpHandler_<GetWsReturn>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CGetWsReturnU3Ed__7_SetStateMachine_m89DBEA4CBBC7224B7E4310090C43B061B90B32D5_AdjustorThunk ();
// 0x000001F7 System.Void GraphQlClient.Core.HttpHandler_<WebsocketDisconnect>d__8::MoveNext()
extern void U3CWebsocketDisconnectU3Ed__8_MoveNext_mD9582B17F1372343936089EB553C947C9CB388FD_AdjustorThunk ();
// 0x000001F8 System.Void GraphQlClient.Core.HttpHandler_<WebsocketDisconnect>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CWebsocketDisconnectU3Ed__8_SetStateMachine_m7602A3BE1A3279F87AB602470D808F2D0A7B5F8A_AdjustorThunk ();
// 0x000001F9 System.Void GraphQlClient.Core.Introspection_SchemaClass::.ctor()
extern void SchemaClass__ctor_m9917D7933A1EF65CD138B20B15ABBDF6AD0C0325 ();
// 0x000001FA System.Boolean SimpleJSON.JSONNode_Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_mE03DD42D0DEDE3493D9405D9EE535AEA5FE446CC_AdjustorThunk ();
// 0x000001FB System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m05355A819BFE7107717A69957C3A255273449385_AdjustorThunk ();
// 0x000001FC System.Void SimpleJSON.JSONNode_Enumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m4E912D002FEBB7AD53C59AFAF90DF4917BD85B02_AdjustorThunk ();
// 0x000001FD System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_Enumerator::get_Current()
extern void Enumerator_get_Current_mE01930C040D8565C5DF7682E6415FC48BD388B94_AdjustorThunk ();
// 0x000001FE System.Boolean SimpleJSON.JSONNode_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m5F7677A228DDBA16F1D18078933A17ABAA823C75_AdjustorThunk ();
// 0x000001FF System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m7A7A4CA011E1ECFB43F7C7DAB985DF2B3DB921EE_AdjustorThunk ();
// 0x00000200 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m1CDBED8B76E22050F7FCED0CEDB8A7B26C94B515_AdjustorThunk ();
// 0x00000201 System.Void SimpleJSON.JSONNode_ValueEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void ValueEnumerator__ctor_m4D8F3303471408B2B130036835DA3AE22F3EA430_AdjustorThunk ();
// 0x00000202 SimpleJSON.JSONNode SimpleJSON.JSONNode_ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk ();
// 0x00000203 System.Boolean SimpleJSON.JSONNode_ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_m80DB8324975D9A8428322BB7B34D1D45ACD5176B_AdjustorThunk ();
// 0x00000204 SimpleJSON.JSONNode_ValueEnumerator SimpleJSON.JSONNode_ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk ();
// 0x00000205 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.List`1_Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m7562FD1BB37B81B6EC866F2D4B91D449D4A2617B_AdjustorThunk ();
// 0x00000206 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2_Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m4C89FED3394614DF05CA0BB2E5348A502F508191_AdjustorThunk ();
// 0x00000207 System.Void SimpleJSON.JSONNode_KeyEnumerator::.ctor(SimpleJSON.JSONNode_Enumerator)
extern void KeyEnumerator__ctor_mC9EAD48CFE6FE0ABE897E6EA4BC1EE0F240758EE_AdjustorThunk ();
// 0x00000208 SimpleJSON.JSONNode SimpleJSON.JSONNode_KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_m5CFAB7F891D0DDC9B3A5DF1DE2038F6DAF292AEB_AdjustorThunk ();
// 0x00000209 System.Boolean SimpleJSON.JSONNode_KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m7DE4E6044266B4C47EDE88397169D03DF60BF244_AdjustorThunk ();
// 0x0000020A SimpleJSON.JSONNode_KeyEnumerator SimpleJSON.JSONNode_KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk ();
// 0x0000020B System.Void SimpleJSON.JSONNode_LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m1A1EE79F7821C0ED795C46EEDB7783176040EBA3 ();
// 0x0000020C System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode_LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m394350654CC60ACA7DBE5CE90AD892927BFC33B2 ();
// 0x0000020D System.Object SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m31927FCDF96A4B90105E25B324E2CAE85134A8ED ();
// 0x0000020E System.Boolean SimpleJSON.JSONNode_LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mFC180A5717730454E62E0286F5D8D9AE72BDE393 ();
// 0x0000020F System.Void SimpleJSON.JSONNode_LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mBF5FB73384994A555175F46E352478281AB91C63 ();
// 0x00000210 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode_LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_mFF6F7F4A9E1900EEDDCF167EB48EFC325A4B0066 ();
// 0x00000211 System.Void SimpleJSON.JSONNode_LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_m06A57EF3840D4EE2704E0CB4DFB8EB9B7251EFA4 ();
// 0x00000212 System.Collections.IEnumerator SimpleJSON.JSONNode_LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m0F3F335982F9A8EA3C287B0D15DBBF65BD5E7D7E ();
// 0x00000213 System.Void SimpleJSON.JSONNode_<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m08A6302695FA8FFED3961CEC013FAE54A844A596 ();
// 0x00000214 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE5C32E35EC7D527C9C11C20F309E38FF92B59D5E ();
// 0x00000215 System.Boolean SimpleJSON.JSONNode_<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m631A849E9C817F907E77C1194938FA563F554D55 ();
// 0x00000216 SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA653A80D0CCEF661004013DF85936DDE4D4F803A ();
// 0x00000217 System.Void SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_mE69EE6153C71479E77F6CDF38ABDCF6984C5ABA6 ();
// 0x00000218 System.Object SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m5E565021D4E7CF1B6D7902ED534C33442460D63C ();
// 0x00000219 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m31645C0A0DDD485DFF26CF141E02DFB3A8D20DAE ();
// 0x0000021A System.Collections.IEnumerator SimpleJSON.JSONNode_<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mD0A55464C40549A568E050A922E19E5969E161A7 ();
// 0x0000021B System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_m0712F6AA6A7F37973B3F66EB37691B9F4783814D ();
// 0x0000021C System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mBBC1E7AEB8F9B7AC04D165ECAE9D183F19F8296A ();
// 0x0000021D System.Boolean SimpleJSON.JSONNode_<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_m0DF534784D92B51FE386064197554F9491459867 ();
// 0x0000021E System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m87F47E29A82391F9EABDC2960423809A63D3DA64 ();
// 0x0000021F System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m948556C997EAEE00AC4050FA6DA476FF8AF2D65A ();
// 0x00000220 SimpleJSON.JSONNode SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m436DE502A96515949F5C5027B1F2810165801B8A ();
// 0x00000221 System.Void SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mD10B0B2BBC87D7BFDC59F237563C3515A19F612F ();
// 0x00000222 System.Object SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m5D325EA5E1D268FA593EB324238DB117FA6CBC34 ();
// 0x00000223 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m057AEFA5ACBFD6128FBAD7D76EFE7830B59B55BE ();
// 0x00000224 System.Collections.IEnumerator SimpleJSON.JSONNode_<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m981A7B811CFD4CAD91222704760D1EF0F3423FA7 ();
// 0x00000225 System.Void SimpleJSON.JSONArray_<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m06E8E8B2FC52FE3018AE3269AAC642BCFA34C707 ();
// 0x00000226 System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m80DF5B4FDDA2D5154F34AFDF6AA28B727EF7D8B2 ();
// 0x00000227 System.Boolean SimpleJSON.JSONArray_<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m57D914F733B5D614D5CAFCDD73DDFA157A03EAA2 ();
// 0x00000228 System.Void SimpleJSON.JSONArray_<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m85D02220871BC7AD5F7FB9F627A25273631A999D ();
// 0x00000229 SimpleJSON.JSONNode SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB ();
// 0x0000022A System.Void SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mF86B3D72C5015C52CB57E5FF8C90CD58BC75192D ();
// 0x0000022B System.Object SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m527FCAE88CA8E2F6E28FBDC52B4C4884832DDD29 ();
// 0x0000022C System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA802939A320EA3BD49E49507CF21D4056A78518A ();
// 0x0000022D System.Collections.IEnumerator SimpleJSON.JSONArray_<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m4A7EE4525D2D51F084E7489AA33ABEC1DCEA6479 ();
// 0x0000022E System.Void SimpleJSON.JSONObject_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mF8A6932A8B490316F55E33EC122C544BB332B12A ();
// 0x0000022F System.Boolean SimpleJSON.JSONObject_<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m78AEA81A69084D7B4B4D6A1DE8558D50B065B7DB ();
// 0x00000230 System.Void SimpleJSON.JSONObject_<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m41067DDF3D08F0BE8CED3D9E7872A7FBA8316CDC ();
// 0x00000231 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_mE9BEDEC6724C22575197031050734765574F3973 ();
// 0x00000232 System.Boolean SimpleJSON.JSONObject_<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m92ED45D5D77C97089D74BC1B35568C8ED33176DC ();
// 0x00000233 System.Void SimpleJSON.JSONObject_<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m4A46673EE413AF8FAD22D64152BEBE5EE76A5E65 ();
// 0x00000234 SimpleJSON.JSONNode SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD50C7203AF0D12B0EDFB7F1AABB9B5BBBD327243 ();
// 0x00000235 System.Void SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mF4C27D1B6113C761A688634D1A4C9C5058280CEC ();
// 0x00000236 System.Object SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m2EE583F6F365FBAA04E82B8963501E8A3DA58C70 ();
// 0x00000237 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m948470928F9C800DBE8DB201F697EE2F36254AA3 ();
// 0x00000238 System.Collections.IEnumerator SimpleJSON.JSONObject_<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m5D486E26CEBA6C8C62417BFB778CAD59F2306BCE ();
// 0x00000239 GraphQlClient.Core.GraphApi_Field_PossibleField GraphQlClient.Core.GraphApi_Field_PossibleField::op_Implicit(GraphQlClient.Core.GraphApi_Field)
extern void PossibleField_op_Implicit_m351720E44CB12EE83E8F412C211DEC2AA1A4E61A ();
// 0x0000023A System.Void GraphQlClient.Core.GraphApi_Field_PossibleField::.ctor()
extern void PossibleField__ctor_mA72EFFC6FC846DB35B0B42CF581C3729CF17F8D4 ();
// 0x0000023B System.Void GraphQlClient.Core.Introspection_SchemaClass_Data::.ctor()
extern void Data__ctor_mA25821B401A728B1F6AA096CBC7D50C1E222BB60 ();
// 0x0000023C System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema::.ctor()
extern void Schema__ctor_mFD9CF215B3762A0C5D6AE57BAA16E5146E51A100 ();
// 0x0000023D System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Directive::.ctor()
extern void Directive__ctor_m59CB9075CAD99EBE2D6DEBEA4D902354ED498676 ();
// 0x0000023E System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Type::.ctor()
extern void Type__ctor_m7FE19E0C082D0212C0DA12E0C0A9EEEAEC1724DD ();
// 0x0000023F System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_InputValue::.ctor()
extern void InputValue__ctor_mE3BD7085A3FC5B9CCEC869895C1B54BC718B4F84 ();
// 0x00000240 System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Type_Field::.ctor()
extern void Field__ctor_mD8144694650ADC59C3FC35FE014F062D457FC34D ();
// 0x00000241 System.Void GraphQlClient.Core.Introspection_SchemaClass_Data_Schema_Type_EnumValue::.ctor()
extern void EnumValue__ctor_mD6B03FA9687B243A4E7D2830A057A263E0D931A6 ();
static Il2CppMethodPointer s_methodPointers[577] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	EmptySprite_Get_mF969CF8CC7C5DF8CE9E524C818828E8A68164842,
	EmptySprite_IsEmptySprite_m03E33296B60FDDB3B66A58882EA11D98F2D7D852,
	FreeModifier_get_Radius_m597D0A4773B74F68383E84400C171DD9A57A6019,
	FreeModifier_set_Radius_mA9CB3D340F28A88EEF4B2236EC1C8AF55DD70E0D,
	FreeModifier_CalculateRadius_mFA329C10795C5BBB892983AC03D563A46D053699,
	FreeModifier_OnValidate_mE43A9747175060376B72326252BEB90BD2058DC1,
	FreeModifier__ctor_m8582DFCFA70998C596206F2F547A4855DF7E0EC7,
	OnlyOneEdgeModifier_get_Radius_mC8DBA87D38F115DC596ABF52767BA7303233C0DA,
	OnlyOneEdgeModifier_set_Radius_m631A55143B0ACFB07F99BA4FEFB3FADFFE3E3255,
	OnlyOneEdgeModifier_get_Side_m1C199404DE5AFB90464191A17675522EFFFBC4AD,
	OnlyOneEdgeModifier_set_Side_mE522A0B44AC052CC813BE1BCA95A5CC566A42454,
	OnlyOneEdgeModifier_CalculateRadius_m32D4273AE7384895BA1061E00A7E7815EBB35A14,
	OnlyOneEdgeModifier__ctor_mD0A037F75A7DF4411920CD15C7203D775BFBEF50,
	RoundModifier_CalculateRadius_m7BC0D84B13B1663F9F41B934F7C04AD2C12F605A,
	RoundModifier__ctor_m39C2B72E9D16271E5B0FC665AE2AFD610BFC5985,
	UniformModifier_get_Radius_mA2D9291E1D661BACF9AA62DE7CEED5E3FFD495EF,
	UniformModifier_set_Radius_m13707EAC06423E88B3DE8C89D3B50855D82BAF50,
	UniformModifier_CalculateRadius_m9420E25FFAA7C1CA43F3EF4C7A245103E070E466,
	UniformModifier__ctor_mC47E306E5B93D223BFD422A888A002838807CDD5,
	NULL,
	JsonHelper__ctor_m44D9EFAD3CBB1C62453A7A08E545331E802BCB99,
	BCProduct__ctor_m69382B6B637749C3F63DEFC3101B3A26DA2ABB82,
	ShopifyQuery_Start_mF4746E754612667BEC46F4AAA19F6D845B38D0F1,
	ShopifyQuery_test_m3D8E9C9C1F7B9C2692AABC037F24FBA55E87C38E,
	ShopifyQuery_getShop_m5D2E9977F613FB950346CCB352753D12856CA55D,
	ShopifyQuery_getProducts_mB6683F594ECC1FB07CEB7FC3178EDBCA4567AA4E,
	ShopifyQuery_getCustomerAcess_mC5B411A390F2A10326FBF14F57BE47579502F1A8,
	ShopifyQuery_getCustomer_m51271441CA205B30BF1903F9AE5A2DADFB9069C4,
	ShopifyQuery_createNewCustomer_mBBAF437D1964F229591CD5B93031FA145D893BA0,
	ShopifyQuery_createCheckout_m476586ADF5999BC4BA6BCDEDE9170935D5EFE11D,
	ShopifyQuery_associateCustomerCheckout_mF83152A5324F40E5C0D7260D708ECD5A8CFDD05A,
	ShopifyQuery_testUnitySDK_mEEE49622386DCB1F67185E2F4E0DE0C334137BCC,
	ShopifyQuery__ctor_mC13E528910FE7032DB6866D76F9E56A242BC4EDF,
	LineItem__ctor_m23830AFD4B0EF653433FCDFA3CD1E65B1955A47A,
	assocCredentials__ctor_m24AC85114DDF2665A1AF4CED00791A84573D573F,
	AddToCart_AddThisProductToCart_m3E76425EA0C961BAE8151935325BCEDC124CF1D4,
	AddToCart__ctor_mE861A247A475B44C3034D53E0902B86F87207535,
	AnimateSidebar_Awake_m1F303204675A42C32B8F0DBADE93A56EA2B8FAF7,
	AnimateSidebar_OnEnable_mCA821698650B40F759F855C082CE70CC2A0E0C12,
	AnimateSidebar_MoveIn_m23D862B603896E096548A55A26A98AB1046E9FDE,
	AnimateSidebar_DisableSidebar_m28F95A618A36038B45CD690790A5A8E8B81A85E9,
	AnimateSidebar_MoveOut_m59EFF362CCEFE8B278C68405564E67294428D48D,
	AnimateSidebar__ctor_mF1BFFD6254999228DC0ECA70DF7378E9F8EE63A8,
	Banners_CreateBanner_mC31223E35C77F28BEAEB9323B75248C4889C37DF,
	Banners_LoadSpriteAsync_m3429D181439AE63DC393B66AFFAC731CFC78E147,
	Banners_CreateHomeBanner_mB98A71E022090E26651373E309791FDAF68E8E3C,
	Banners_CreateFeatureBannerOne_m11D8ED7BF5ECD2DDA3BBAB5228AAF8B4FB0C500F,
	Banners_CreateFeatureBannerTwo_m48C9B3DBBD02C58B7208B5E6E29402E571442DCD,
	Banners_CreateGamesBannerOne_mC51F212127D91F1567E2DE94999ADEC8C365435F,
	Banners_CreateGamesBannerTwo_m75917C2DF3827B5B3C79DD5E410BF333D8923FEF,
	Banners__ctor_m3F423CC3C69D01CE54A740EE1F5693B7D5FB6856,
	CartButton_get_ItemsInCart_mDBEAF969E0449490449A4443A77249C2A4037293,
	CartButton_set_ItemsInCart_m1072FEFE8CABF6C28D606B40FA14AF5FDABEC910,
	CartButton_Awake_m742EBAB43A182475AF52B541940C5DA89E878A72,
	CartButton_OpenCartPage_m875E065225694367AFC6116EB4DA5C3976413A9F,
	CartButton_UpdateCartNumber_m32084BDF49780E04D2D3A1787D05464E24597854,
	CartButton__ctor_m30F73C5A9D12A420CEBE3F3C43499A68CEDC9CCA,
	FeaturedBanner_LoadNewImage_mCCAB73364B435BB29012F79766DA5C5A6A08B431,
	FeaturedBanner_FeaturedBannerClick_m9739744C2653EE6C18886412EC61A0ABBC6750E7,
	FeaturedBanner_FitToFrame_m6C370D63F90983674049E9D547AF15A5802B1554,
	FeaturedBanner_SetSprite_m0BA3B82FE898811BDD894212397C358ADD466B7E,
	FeaturedBanner__ctor_m438324DBA3931A85C03C7F7C0D93A80D5ED86C20,
	GlobalColors_Awake_mD43B3493219401DAF3E22CF33046B74A498A83E0,
	GlobalColors_get_ProductBannerPS4_mBC409F0B8442845E77D5F8F32F722006B4846E5A,
	GlobalColors_get_ProductBannerSwitch_m72356A4828E572714ABBFA460BA2EF8427DFD2F1,
	GlobalColors_get_ProductBannerPC_m334CF80EC22D23521064622272CA9950067D34F6,
	GlobalColors_get_ProductBannerMisc_m10FD7271715738C0B6C84B71B4B1A0FDAC4DA990,
	GlobalColors__ctor_m306917E89EB81FED92012A45D1C492152B88FF9C,
	MenuButton_get_isSelected_mA3A41544D85B49D1493472BAC18CCD39D889C05D,
	MenuButton_set_isSelected_m297B19F93F6BFDF406B2DDF35E0F0328E91D45FD,
	MenuButton_MenuButtonClick_mECE56C78EB7BB9428C03A2480639FE324A0A51AC,
	MenuButton_HighlightMenuBtn_m2C54A61488B914307DF4440EEB95A13ED51E7B90,
	MenuButton_SelectMenuBtn_m540AF3B1AA4AAF6B1F73AF12EF7D9A4C41697D45,
	MenuButton_UnselectMenuBtn_mCB04E50D4EFD5A3EC3741E12F0C90065E5969202,
	MenuButton__ctor_m63139A1271B817481EC61A971685F1D9666491E0,
	MenuButtons_Awake_m8FB4ED56C066421B158AC7F9EB60F8D9C54D8F53,
	MenuButtons_Start_mC18C8436D178DD5253A6203FF0D55C7754A8219F,
	MenuButtons_NewActiveMenuBtn_m303130CA89D8001F0A8F891432B992C395D2DC12,
	MenuButtons_UnselectLastMenuBtn_m059E15EADAE51F07170188E14784790FD6E6E956,
	MenuButtons__ctor_m58BFA0C363AEBD63C9EAEC8F6BB1ACBBF80E2551,
	Page__ctor_m078A0134BDCCF6EFA0A16033E6B38B46958933D1,
	Pages_Awake_m4AEB2324C7800F567803FF1B5F7B9B2C130394B2,
	Pages_Start_mFF81B9827A6B45E52376EAFEE96B291B7B61440B,
	Pages_InitAllPages_m37FDE5D97A24B3FB23983D48719C29E030A6E27D,
	Pages_SetPage_mCDC7649016C72230CF3398B61FE825FD15B40187,
	Pages_CloseAllPages_mC998C12FC71DEA2E332648C192725487E137EE81,
	Pages_OpenAllPages_mA005EBE910D873855197DBB4DEDA56AB62274FDD,
	Pages_OpenPage_m078F6988503E82B8A76FB6521FE18D74238A1B19,
	Pages_PageLink_m42807EBDA92CBC58FFFE0079E0E08066FCB0A59B,
	Pages__ctor_m1C50FFBF81B984817D7F4B89F105D27021491F93,
	ProductOnShelf_LoadProduct_m415CFDA89C3F695B99E76559507BFB149A58DC07,
	ProductOnShelf_BannerColor_mADD6E6DAD45705B961E2406A31F2A8CF05595ECB,
	ProductOnShelf_OpenGamePage_m8167E81E582FCA45725C304127F2C8039D26558F,
	ProductOnShelf__ctor_mF18D4B0816013E6DC81E0B1019507A62F52D7095,
	Products_CreateShelf_m6C3254943C59C6AF6483D3CE1183B9AAC02B0A26,
	Products_LoadSpriteAsync_mC38CD0B2F2D3D6D8C8CD333490DD8C31D20D311B,
	Products_Platform_m34C1C72DE5E8D9514E554C2C6ED6B44A834E2D08,
	Products_CreateAvailableNowShelf_m494C37A0E4EF71E5A71AA2C480615B3152ABE541,
	Products__ctor_m238D48CC213AA0F5BE50A9F62C0E3A62104CE9DA,
	imgHelper_Start_m39B0EF63453BC90BE8A36F79290B5EA3FC0A785C,
	imgHelper_Update_mBC747B291990811F95BF86F03E37A7BB2795B8E1,
	imgHelper__ctor_m3CC0E57EC3C668EAACFF82C8DC41699841D266B4,
	ImageHelper_AssignImage_m85A68A92F26C4F88D1016A7235EA75516ADC583F,
	ImageHelper__cctor_mE52A30A1D447670C992D9351B486B82C59161E19,
	Pokemon_GetPokemonDetails_mDFC407DD8D07CDE269273377CF1424A94AA76A5B,
	Pokemon_GetAllPokemonDetails_mF308E582472BB0494BB3192475961BC451BB0B41,
	Pokemon__ctor_mAE34E9ADDBA3D214D58625ABA92A241428695E77,
	User_OnEnable_mF94083E29267010C318E6A41C66FBD4F9418D0A9,
	User_OnDisable_mFA6E9C5523B40DA0AC176EF868561AFE6FE5CC04,
	User_GetQuery_mD4F44ED16D675B9AA90B7EB84416E57C88428661,
	User_CreateNewUser_m26F9C63BED9DB4C8AB776E25C85B732A38B1C3DD,
	User_Subscribe_mC0DC37CE2992F3661DAD395138C758E0C585744F,
	User_DisplayData_mA5A6360E3213A5C0FD5678E7682C42B9E2A75406,
	User_CancelSubscribe_mB779B3D6E0E63DC17D61C343DD873C526D715852,
	User__ctor_mB5B1D42F0980D69CF10E95B3BF591E010A500A53,
	OnRequestBegin__ctor_m4B4E9FBAF93678E92D26970EF91F059AF5B1B421,
	OnRequestEnded__ctor_mE5C31074EAD9146EFF7EC7D3A2107FEB38E12386,
	OnRequestEnded__ctor_m6DAC497C763EAA84D40680F8FFACCD6E1FC21F27,
	OnSubscriptionHandshakeComplete__ctor_m841B9C3E6A61CF06F018091ED3A9694A18E8D984,
	OnSubscriptionDataReceived__ctor_m9538952603448852664A2A052A63C3711AA69D18,
	OnSubscriptionCanceled__ctor_m3FECBBF983DEEC0C6BC219435C197343E8B9F114,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GraphApi_SetAuthToken_m81DC37A4D756FD282C6F01BDEBC48105527D8F86,
	GraphApi_GetQueryByName_m75544143489C8FB1D15A07CB6C6804359F1C8559,
	GraphApi_Post_m2A34513BB46405BC50224F2ABDEF8B3067304107,
	GraphApi_Post_mD242073B2AAE8304ED6E1F4FEE3EFB75A77F1DBF,
	GraphApi_Subscribe_mFF63698359845D79ED957173498A5ABC995300B5,
	GraphApi_Subscribe_m8E35A16D1403F7833954A30FEFD9BDDD145A313F,
	GraphApi_CancelSubscription_mCFAB059969A4C759658EB95938D9C50E0CA99B47,
	GraphApi_JsonToArgument_m1E98A2E2B4789D6DEC1AD0D96153F537E911A03D,
	GraphApi__ctor_m2F9E6188A9A45AA0C382E9DE20FA98AB2E6806C0,
	EnumInputConverter_WriteJson_m139FDF328A3BF2F6D7EF85F0A7924F6A18BDC2BA,
	EnumInputConverter__ctor_m99EADBD3776D8B534D5F824CE9CC2B83A75A10C4,
	HttpHandler_PostAsync_mC464CB2D668D5BBD23A7BFED734FC3669B9A5C45,
	HttpHandler_PostAsync_m5B6D9C90AED01C17AA4F135C2BFB614CF0D2BEE1,
	HttpHandler_GetAsync_m44D8E55019A91765F48649EB542CE5FFBE2AD1F1,
	HttpHandler_WebsocketConnect_mAED924B5C1DB15F5F0698E52FC53E0C940B57C2E,
	HttpHandler_WebsocketConnect_mAFF90437022793D3A18EACC814B9A354DD65F95F,
	HttpHandler_WebsocketInit_m713340C13B4ACCAAB2ED40F7EC29AD6D0DE0F5F0,
	HttpHandler_WebsocketSend_mB1363DB317EA76CF8E0ED465C4859FDBE9F33FCA,
	HttpHandler_GetWsReturn_m6016B7FB115ADD8EFD2808DABCC1726B32B59017,
	HttpHandler_WebsocketDisconnect_m39EB254D457A7AB06AEA8503A1F6732545807081,
	HttpHandler_FormatJson_mDBD01BF5B31E13F024A4B7CA898FCB5BDB517B86,
	HttpHandler__ctor_m83AA767A109B6FD8F8B9FE3D19426986A9E00571,
	Introspection__cctor_m7BF4F26AC4B43959364375823299B3713FC9F74E,
	NULL,
	JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865,
	JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96,
	JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E,
	JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26,
	JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580,
	JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76,
	JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C,
	JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E,
	JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0,
	JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD,
	JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461,
	JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31,
	JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60,
	JSONNode_get_Inline_m72DF6AD0574D74648194D898901C0F61C2EF29E8,
	JSONNode_set_Inline_mB44B0D5303F8644FB399963F8342F1915FED1EC2,
	JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1,
	JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4,
	JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653,
	JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE,
	JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118,
	JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395,
	JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F,
	JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75,
	JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698,
	NULL,
	NULL,
	JSONNode_get_Linq_m4CA50FA2E097B82ED352580539F7215F181AE1EA,
	JSONNode_get_Keys_mAEC584E7C7F1CCC000C51E279CEA435552023ED3,
	JSONNode_get_Values_m647ABB3BC12AFD94CE4C23531628EFC29A7981D5,
	JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8,
	JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098,
	JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1,
	JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C,
	JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC,
	JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63,
	JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947,
	JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7,
	JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A,
	JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B,
	JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84,
	JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D,
	JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4,
	JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487,
	JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837,
	JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552,
	JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5,
	JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18,
	JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477,
	JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099,
	JSONNode_op_Implicit_mFB610B47429EC263B56050700A6D74F38A93FCC1,
	JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E,
	JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5,
	JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0,
	JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC,
	JSONNode_get_EscapeBuilder_mE5AC264BA62AA54B0DCDCB165564F5EE28B50653,
	JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6,
	JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69,
	JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9,
	JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE,
	JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD,
	JSONArray_get_Inline_m701D46F6554CE60CD9967EA4598C51C1C941ED2B,
	JSONArray_set_Inline_m736B5C89F41DE7A7CCAC0FC6160CF4FD50B989DC,
	JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE,
	JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03,
	JSONArray_GetEnumerator_mB01C410D721F8B673A311AFE4913BD2F1C8E37F9,
	JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10,
	JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885,
	JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215,
	JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE,
	JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647,
	JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D,
	JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F,
	JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049,
	JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85,
	JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C,
	JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA,
	JSONObject_get_Inline_m68C30F81DBE34CC9512A1CF26E501A9042CEE6B2,
	JSONObject_set_Inline_mE4E0D0ABDE60706A3BE440D751C80D45E565078E,
	JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486,
	JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD,
	JSONObject_GetEnumerator_m89934764DE9D9DB8D007DE2FDB956B35A483CE2A,
	JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF,
	JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B,
	JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976,
	JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E,
	JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF,
	JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D,
	JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7,
	JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE,
	JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15,
	JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6,
	JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E,
	JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F,
	JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185,
	JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E,
	JSONString_GetEnumerator_mF8D0B322B157094DFDC176D6B8BD0E2C4BE626E4,
	JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A,
	JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF,
	JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA,
	JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8,
	JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09,
	JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1,
	JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F,
	JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912,
	JSONNumber_GetEnumerator_m3DDB7A5F04D7C6C314D6969ECDB3314385E0F8C0,
	JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB,
	JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0,
	JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399,
	JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68,
	JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8,
	JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F,
	JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B,
	JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887,
	JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931,
	JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5,
	JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A,
	JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA,
	JSONBool_GetEnumerator_m4B8C0CC079A97EB3B0331ECFFB15F7DD0AB7A180,
	JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6,
	JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A,
	JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9,
	JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91,
	JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD,
	JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE,
	JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9,
	JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09,
	JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A,
	JSONNull_CreateOrGet_mFCEA43022679C084EA7BEB23AD203E1B9B33E1D7,
	JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F,
	JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936,
	JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA,
	JSONNull_GetEnumerator_m390B90BACB37AC50B726CE28D07463748A202A79,
	JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60,
	JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922,
	JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98,
	JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000,
	JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600,
	JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B,
	JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E,
	JSONNull__cctor_m0E80AEF0AD9DCA6A8018A55DAA8464FCA2DBCC16,
	JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D,
	JSONLazyCreator_GetEnumerator_mDAA175BC448F491BD873169D142E119DF6733ED6,
	JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798,
	JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188,
	JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779,
	JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047,
	JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606,
	JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C,
	JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC,
	JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670,
	JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F,
	JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7,
	JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95,
	JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E,
	JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43,
	JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A,
	JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857,
	JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870,
	JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934,
	JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465,
	JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350,
	JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE,
	JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2,
	JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D,
	JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2,
	JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F,
	JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9,
	ModifierID__ctor_m5F106BF20600F0C82C1EBA566340755C75B4281F,
	ModifierID_get_Name_m71B85ED39E93CD5752ECCE9321DB4D74E15EC7DE,
	ProceduralImage_get_DefaultProceduralImageMaterial_m2B93AC078A6EF6961C86FA30BC8397C9FE5A52E5,
	ProceduralImage_set_DefaultProceduralImageMaterial_m7FA6165E9D9717DF41B972F6778D3A9A3D2D54CD,
	ProceduralImage_get_BorderWidth_m44936B2395491E0FC18F6317716A334CEB333D68,
	ProceduralImage_set_BorderWidth_m41DAAA6F57046E6E0C34E02D692552A482323A07,
	ProceduralImage_get_FalloffDistance_m0D2AB1A4C484EA8FB7E450B4CB3082BE5A8E228F,
	ProceduralImage_set_FalloffDistance_mF9A081889C7A954A8D8D10C92CF89EFCA224B4AB,
	ProceduralImage_get_Modifier_m9ADB9C7BB714FF1C1BA1400CF1C31AA3E9211A36,
	ProceduralImage_set_Modifier_mBB7D6EA0696D9CC7D7EDC07EB4FFCE36AAF3D23B,
	ProceduralImage_get_ModifierType_m1A03DBEE862E01C5FFC4289532E4BA3AB8705994,
	ProceduralImage_set_ModifierType_m1BE072F26D1532B3910CC25B9B65D58F189520E4,
	ProceduralImage_OnEnable_m6CE8D56086FFCB84EFEA7D100D8B01D23A53BF4B,
	ProceduralImage_OnDisable_m2C8FEAA36A9D13E84F4936B2B9BE481AE26EB350,
	ProceduralImage_Init_mB795B8891E67E9B232F8461DC64B28996CCC8ACD,
	ProceduralImage_OnVerticesDirty_m3FA8B1245E66CAA9EB8E68FC38A8A40B3FA55446,
	ProceduralImage_FixTexCoordsInCanvas_mBD5B18E472AD9C1033FDFA04352A951F0F8CBE6E,
	ProceduralImage_FixTexCoordsInCanvas_m3BFC3EEA9B7E5FEC343C2869957275BC6E6C7466,
	ProceduralImage_FixRadius_m3B0A607A8AFDC8BE47CEE0439C807DEF9210CB2B,
	ProceduralImage_OnPopulateMesh_mBCD26751981759405BF2B209B1990F3387A8AAF9,
	ProceduralImage_OnTransformParentChanged_mDF28056385945EA13A634819548121BBD12538AC,
	ProceduralImage_CalculateInfo_m93C5501C9DB8B4D1CBE047DB16F1DD83230EE3BA,
	ProceduralImage_EncodeAllInfoIntoVertices_mE3C8CD10CC1435C87A2AD19F90D5BD1180035ED2,
	ProceduralImage_EncodeFloats_0_1_16_16_mF3640DFFDF2E7A4FFADAEEF85FBF3095B0921304,
	ProceduralImage_get_material_mE0B65000AE6BE8F93556022653D13A29361502AA,
	ProceduralImage_set_material_m768E879F54C8096CDE1926DBA3FEE42133D1C338,
	ProceduralImage__ctor_m3EB96A165C01173AE09C10DC6825070C0E86D477,
	ProceduralImageInfo__ctor_m887DCAEEC8C986B2C25A5EBD20E08C3D8D8A7892_AdjustorThunk,
	ProceduralImageModifier_get__Graphic_m2E3CD53C1477B30D4E79555D2F46FD3E9D28B2D9,
	NULL,
	ProceduralImageModifier__ctor_mBD73645FA6EFE4CA1FE083A68498D14F508CCC38,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m94E4EDA2F909FACDA1591F7242109AFB650640AA,
	NULL,
	U3CtestU3Ed__7_MoveNext_mEC09EF94350342F0F27B65619F569D46244D5F9B_AdjustorThunk,
	U3CtestU3Ed__7_SetStateMachine_mFF0D433F51B5BD30946351EFD53DF87BC5A8A376_AdjustorThunk,
	U3CgetShopU3Ed__8_MoveNext_m03A6ABDA9E04BCC7654A615719297B52D84A6C43_AdjustorThunk,
	U3CgetShopU3Ed__8_SetStateMachine_m259B96DF59E88B64350AF78AF5251184B4C77BE3_AdjustorThunk,
	U3CgetProductsU3Ed__9_MoveNext_mD978CA984FF42201930EC9FC63CBD1FC91CE0905_AdjustorThunk,
	U3CgetProductsU3Ed__9_SetStateMachine_m731FF40675262290575355F4E9876526DCB7017C_AdjustorThunk,
	U3CgetCustomerAcessU3Ed__10_MoveNext_mF8924CD85E7288792C00C59036FA30A0C38E5FD4_AdjustorThunk,
	U3CgetCustomerAcessU3Ed__10_SetStateMachine_m672DB2DF2BDF5356C3552F61FC7445EBF22141A9_AdjustorThunk,
	U3CgetCustomerU3Ed__11_MoveNext_mD1B1E0F18872E7A2F033299FDBB463EE57B002C5_AdjustorThunk,
	U3CgetCustomerU3Ed__11_SetStateMachine_m5F5F0D23059F01D75F90A54D4904226F11AE8C57_AdjustorThunk,
	U3CcreateNewCustomerU3Ed__12_MoveNext_mC20762E90330627B7E152369A45B9D6F61BF2990_AdjustorThunk,
	U3CcreateNewCustomerU3Ed__12_SetStateMachine_mF86C1B2582437A303295198DD2890D0E198CD27C_AdjustorThunk,
	U3CcreateCheckoutU3Ed__13_MoveNext_mCAE52AC09D3BDFE9D31494EB30A040B2849A1098_AdjustorThunk,
	U3CcreateCheckoutU3Ed__13_SetStateMachine_mE46465C839A0F246443CAD527BC39D1B810707BE_AdjustorThunk,
	U3CassociateCustomerCheckoutU3Ed__14_MoveNext_m8000FD1E81721500FE1016CBC765A59FB8EB777B_AdjustorThunk,
	U3CassociateCustomerCheckoutU3Ed__14_SetStateMachine_m9E0FEC83C8FE39BDF2E9C92DA44694FDDF15EC71_AdjustorThunk,
	U3CtestUnitySDKU3Ed__15_MoveNext_m623FA2956BD674AB9CA282F16555DD2AA73C346A_AdjustorThunk,
	U3CtestUnitySDKU3Ed__15_SetStateMachine_m7C2DA245799F81F6034572E5CBCF20529932AA77_AdjustorThunk,
	U3CMoveInU3Ed__8__ctor_mF171C7271535F8351321D2765FFED135D8AA6594,
	U3CMoveInU3Ed__8_System_IDisposable_Dispose_mC477FB900662095C9D775B60B5984187CF3363DB,
	U3CMoveInU3Ed__8_MoveNext_m3B619020B06D9BBDCEF639C4B6EA9BEED49F1475,
	U3CMoveInU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8DB807A7E75C2093E562EB6422E7DF2CBFE9D00,
	U3CMoveInU3Ed__8_System_Collections_IEnumerator_Reset_m3CCB5EDB0A028DC8E49A46D269715BF68D19E8BA,
	U3CMoveInU3Ed__8_System_Collections_IEnumerator_get_Current_m5E51A4374E5A77BA79E175235B9996B3152BBA7E,
	U3CMoveOutU3Ed__10__ctor_mAC5E2A00D61FBC3DA5F58FA5C1B005F7D68AEB93,
	U3CMoveOutU3Ed__10_System_IDisposable_Dispose_mDD47322020AB68CCA69D33EF7E53A5E0732204EB,
	U3CMoveOutU3Ed__10_MoveNext_m505EDB7A745481DD3D3CA238987AA6AF5E92AED0,
	U3CMoveOutU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3A436BE6AA7DE1A7A2B0D13D17E85E05D0F71C0E,
	U3CMoveOutU3Ed__10_System_Collections_IEnumerator_Reset_mE5CD1A00E3B9C7B0EE569EC4D7C4F13D27626739,
	U3CMoveOutU3Ed__10_System_Collections_IEnumerator_get_Current_m8DE85E58EE6EA2410A321DFC24778065FE8EF2B8,
	U3CCreateBannerU3Ed__6_MoveNext_m850F70C047618942F1205EA2A054D153480B27A6_AdjustorThunk,
	U3CCreateBannerU3Ed__6_SetStateMachine_m38C65DA25F7D09357E21B8ACFDA099288BD3079B_AdjustorThunk,
	U3CLoadSpriteAsyncU3Ed__7_MoveNext_m8C027D62235337B44C668550C601AB820D3B14A3_AdjustorThunk,
	U3CLoadSpriteAsyncU3Ed__7_SetStateMachine_m8E3B7711B1654AC590505648E95346FB5D3DF3C5_AdjustorThunk,
	U3CU3Ec__cctor_m403144039879519538A84C85D35376E3A4D54CD0,
	U3CU3Ec__ctor_mF6B6785B90C26283B9B8D3CD8B31AC5087BCA456,
	U3CU3Ec_U3CStartU3Eb__4_0_m673F9209D5EE169410529BD14A2B932D943B69DD,
	U3CU3Ec__DisplayClass8_0__ctor_m26F027E770FE7C458903A4A5103F94E3815C278D,
	U3CU3Ec__DisplayClass8_0_U3COpenPageU3Eb__0_m6FE7D4AB1B7825A48F5E5230AF90012668BA9CE8,
	U3CCreateShelfU3Ed__4_MoveNext_m7239C750ACCC56E7CC17B195E98C6CE46770E91D_AdjustorThunk,
	U3CCreateShelfU3Ed__4_SetStateMachine_m54A7EFB7EF8521CEAA640DDDC50B10EF6AA1004E_AdjustorThunk,
	U3CLoadSpriteAsyncU3Ed__5_MoveNext_mC49E9488C7A73E2AFD4EB1F17EE4DA03D75C365A_AdjustorThunk,
	U3CLoadSpriteAsyncU3Ed__5_SetStateMachine_m04658E0808A4464CE33CD234E94EB9FB5BBC27CA_AdjustorThunk,
	U3CAssignImageU3Ed__1__ctor_mC845F359D8ECCC848367D98F3EF5E65239387154,
	U3CAssignImageU3Ed__1_System_IDisposable_Dispose_m15BDAA8E622170184A42522EA94AC8BC5A47B78F,
	U3CAssignImageU3Ed__1_MoveNext_mE330D5637F655BF589B58FB732A7CFE73B70AF6C,
	U3CAssignImageU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDAC5365D28A42F94263A25BB469E278D06C02D1C,
	U3CAssignImageU3Ed__1_System_Collections_IEnumerator_Reset_mBEF47B6CCD96FF9A36F73CB4834367D2ED496559,
	U3CAssignImageU3Ed__1_System_Collections_IEnumerator_get_Current_mB47CE0D19A553FCF8AC55ECBF96F8C311877879E,
	U3CGetPokemonDetailsU3Ed__3_MoveNext_m5D0FFB9838E7E674A540B3AD95EF431208222CCE_AdjustorThunk,
	U3CGetPokemonDetailsU3Ed__3_SetStateMachine_mF02B375D142F7627184FC2ED64BFD5052734D91C_AdjustorThunk,
	U3CGetAllPokemonDetailsU3Ed__4_MoveNext_m368A83393024CCA8235890229A5B278196294B1A_AdjustorThunk,
	U3CGetAllPokemonDetailsU3Ed__4_SetStateMachine_m3B3AA35810C73C5012CAF9F48DAB7D354986E4B7_AdjustorThunk,
	U3CGetQueryU3Ed__10_MoveNext_mC0AFB1E7EB56ECEF3ABE6174E3A8B2873AD73796_AdjustorThunk,
	U3CGetQueryU3Ed__10_SetStateMachine_mE56DDF57315AF7F7944177AC74D8CB77DDA452E0_AdjustorThunk,
	U3CCreateNewUserU3Ed__11_MoveNext_m51FBE38A26740C3C78E2226AD7130F298D81CF7F_AdjustorThunk,
	U3CCreateNewUserU3Ed__11_SetStateMachine_m1364A325FE3496BD6B02102A658883CC0E6EF441_AdjustorThunk,
	U3CSubscribeU3Ed__12_MoveNext_mFD91B6DDD6376EC0AC9EB89FEFC7A20B296B1F45_AdjustorThunk,
	U3CSubscribeU3Ed__12_SetStateMachine_m6F952EDD902574D9EAEE5EDC0781B45F4BDBED36_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	Query_SetArgs_mE56D2FFD4866711F783EB7DB7D6E7A7438D6B813,
	Query_SetArgs_m592C897D2A0928F25138ECBEAC85EE0DE38D136E,
	Query_CompleteQuery_mD91D3353A01486739B08033E79E066C2447D8D18,
	Query_GenerateStringTabs_mEAF1B7360792B64E3B846456F1F1B710F7D2BEDA,
	Query__ctor_m994B9E9474A7256DFAD96DBEF2F0C48A7E3C801E,
	Field_get_Index_m796AA89D70FFD1F5DB34B40ACC0FC6A22D48F76E,
	Field_set_Index_mC2E1BF6CB3DF2D97FA79227C6D53707EBB740F63,
	Field__ctor_m416556668865E99DB5530F41D563BB018B3FCB25,
	Field_CheckSubFields_m8204E75739749663D9E9566D8C49EFC8713026F2,
	Field_op_Explicit_mFEA6B100D884BA684B976D171E88CA2F93A42AE9,
	Field_U3CCheckSubFieldsU3Eb__11_0_m6BEB5D53B2867D82019B86C09752AE022B8EEBBA,
	U3CU3Ec__DisplayClass13_0__ctor_mFF4BF1060A6C8D135FFF3D7F78B79CDB778D1EC4,
	U3CU3Ec__DisplayClass13_0_U3CGetQueryByNameU3Eb__0_m7D65E18DA82BB3523D9AC637EC7C047E145F7373,
	U3CPostU3Ed__14_MoveNext_mFC5FDF2A66308F9F896E6BB36FD631A905975624_AdjustorThunk,
	U3CPostU3Ed__14_SetStateMachine_mEB8070681A2E1DB7B1F0A3C0FA66646487BB7189_AdjustorThunk,
	U3CPostU3Ed__15_MoveNext_m205BC4F21BDF8A4EA482A306AAC5A29E057F388F_AdjustorThunk,
	U3CPostU3Ed__15_SetStateMachine_mE2D43D672EB55D744F738C89FA7782C917720577_AdjustorThunk,
	U3CSubscribeU3Ed__16_MoveNext_mDB0F924329F2973639E439A6DE9F887DDC499B57_AdjustorThunk,
	U3CSubscribeU3Ed__16_SetStateMachine_mB304552F72C462DFB852429671A91337CA6B44C0_AdjustorThunk,
	U3CSubscribeU3Ed__17_MoveNext_mDC6EC51838F57043532F15E2D55BC0F260ABA675_AdjustorThunk,
	U3CSubscribeU3Ed__17_SetStateMachine_m6113C658D296A76EDA53DEFF1715BC0B8E760221_AdjustorThunk,
	U3CCancelSubscriptionU3Ed__18_MoveNext_m331E2C520AD503FDA3391B7DA1B6396B84DC7D1D_AdjustorThunk,
	U3CCancelSubscriptionU3Ed__18_SetStateMachine_m89910B6EDBD402C74C1FB15CF924EEA854B2BA11_AdjustorThunk,
	U3CPostAsyncU3Ed__0_MoveNext_mA16A1118311C7EA9B4CE6148ACBCD411C11FD0A2_AdjustorThunk,
	U3CPostAsyncU3Ed__0_SetStateMachine_mF5697FB590A6778CDC64ACA08951114BC091497B_AdjustorThunk,
	U3CPostAsyncU3Ed__1_MoveNext_m5789B03F8DA862974F15BFB84FCA8CF69E7675F9_AdjustorThunk,
	U3CPostAsyncU3Ed__1_SetStateMachine_mB586F8D1D65FEBA7179030D7438933F5A8E5A201_AdjustorThunk,
	U3CGetAsyncU3Ed__2_MoveNext_mA2E945B171CA937517C5C16C16BA0B1A71ACBBDF_AdjustorThunk,
	U3CGetAsyncU3Ed__2_SetStateMachine_m7603FA90FB8643331936CD6480349DA31FEBDF43_AdjustorThunk,
	U3CWebsocketConnectU3Ed__3_MoveNext_mA5DDC7EF020B6DD1E05C307838E849E1145BBD52_AdjustorThunk,
	U3CWebsocketConnectU3Ed__3_SetStateMachine_m0DDA656BD5DBCB6BEA02125B316A493A4CE36CAD_AdjustorThunk,
	U3CWebsocketConnectU3Ed__4_MoveNext_m51932D8DC33D58F6E348C9B3F2A4E4CB450A6F58_AdjustorThunk,
	U3CWebsocketConnectU3Ed__4_SetStateMachine_mA516EC6FCF67B6DBF35AA5EA26D5C3B1F4E3BD84_AdjustorThunk,
	U3CWebsocketInitU3Ed__5_MoveNext_m106748CFAB6E118C01DC802D4196584724FFC1E7_AdjustorThunk,
	U3CWebsocketInitU3Ed__5_SetStateMachine_m43F25D84DE24BB8245B3BC5DE86B0BAFD58883D3_AdjustorThunk,
	U3CWebsocketSendU3Ed__6_MoveNext_mD7B583212339CF5B993DCB3F99968BF742BB09A1_AdjustorThunk,
	U3CWebsocketSendU3Ed__6_SetStateMachine_m0B8034BBEE3701B86882A302C863E9A887B6FED3_AdjustorThunk,
	U3CGetWsReturnU3Ed__7_MoveNext_mC0C3EE386E279A6827566275AEF4AF24F4B752C0_AdjustorThunk,
	U3CGetWsReturnU3Ed__7_SetStateMachine_m89DBEA4CBBC7224B7E4310090C43B061B90B32D5_AdjustorThunk,
	U3CWebsocketDisconnectU3Ed__8_MoveNext_mD9582B17F1372343936089EB553C947C9CB388FD_AdjustorThunk,
	U3CWebsocketDisconnectU3Ed__8_SetStateMachine_m7602A3BE1A3279F87AB602470D808F2D0A7B5F8A_AdjustorThunk,
	SchemaClass__ctor_m9917D7933A1EF65CD138B20B15ABBDF6AD0C0325,
	Enumerator_get_IsValid_mE03DD42D0DEDE3493D9405D9EE535AEA5FE446CC_AdjustorThunk,
	Enumerator__ctor_m05355A819BFE7107717A69957C3A255273449385_AdjustorThunk,
	Enumerator__ctor_m4E912D002FEBB7AD53C59AFAF90DF4917BD85B02_AdjustorThunk,
	Enumerator_get_Current_mE01930C040D8565C5DF7682E6415FC48BD388B94_AdjustorThunk,
	Enumerator_MoveNext_m5F7677A228DDBA16F1D18078933A17ABAA823C75_AdjustorThunk,
	ValueEnumerator__ctor_m7A7A4CA011E1ECFB43F7C7DAB985DF2B3DB921EE_AdjustorThunk,
	ValueEnumerator__ctor_m1CDBED8B76E22050F7FCED0CEDB8A7B26C94B515_AdjustorThunk,
	ValueEnumerator__ctor_m4D8F3303471408B2B130036835DA3AE22F3EA430_AdjustorThunk,
	ValueEnumerator_get_Current_m151356ECDEFD55E1FFF524FE25A0F489878A10AB_AdjustorThunk,
	ValueEnumerator_MoveNext_m80DB8324975D9A8428322BB7B34D1D45ACD5176B_AdjustorThunk,
	ValueEnumerator_GetEnumerator_m3C2328208D593CFC614F7D42AC30DDB1047A9E1A_AdjustorThunk,
	KeyEnumerator__ctor_m7562FD1BB37B81B6EC866F2D4B91D449D4A2617B_AdjustorThunk,
	KeyEnumerator__ctor_m4C89FED3394614DF05CA0BB2E5348A502F508191_AdjustorThunk,
	KeyEnumerator__ctor_mC9EAD48CFE6FE0ABE897E6EA4BC1EE0F240758EE_AdjustorThunk,
	KeyEnumerator_get_Current_m5CFAB7F891D0DDC9B3A5DF1DE2038F6DAF292AEB_AdjustorThunk,
	KeyEnumerator_MoveNext_m7DE4E6044266B4C47EDE88397169D03DF60BF244_AdjustorThunk,
	KeyEnumerator_GetEnumerator_mF33634E45FC6CCD3ACDE273AEB14CAAA35009A91_AdjustorThunk,
	LinqEnumerator__ctor_m1A1EE79F7821C0ED795C46EEDB7783176040EBA3,
	LinqEnumerator_get_Current_m394350654CC60ACA7DBE5CE90AD892927BFC33B2,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m31927FCDF96A4B90105E25B324E2CAE85134A8ED,
	LinqEnumerator_MoveNext_mFC180A5717730454E62E0286F5D8D9AE72BDE393,
	LinqEnumerator_Dispose_mBF5FB73384994A555175F46E352478281AB91C63,
	LinqEnumerator_GetEnumerator_mFF6F7F4A9E1900EEDDCF167EB48EFC325A4B0066,
	LinqEnumerator_Reset_m06A57EF3840D4EE2704E0CB4DFB8EB9B7251EFA4,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m0F3F335982F9A8EA3C287B0D15DBBF65BD5E7D7E,
	U3Cget_ChildrenU3Ed__39__ctor_m08A6302695FA8FFED3961CEC013FAE54A844A596,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mE5C32E35EC7D527C9C11C20F309E38FF92B59D5E,
	U3Cget_ChildrenU3Ed__39_MoveNext_m631A849E9C817F907E77C1194938FA563F554D55,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA653A80D0CCEF661004013DF85936DDE4D4F803A,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_mE69EE6153C71479E77F6CDF38ABDCF6984C5ABA6,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m5E565021D4E7CF1B6D7902ED534C33442460D63C,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m31645C0A0DDD485DFF26CF141E02DFB3A8D20DAE,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mD0A55464C40549A568E050A922E19E5969E161A7,
	U3Cget_DeepChildrenU3Ed__41__ctor_m0712F6AA6A7F37973B3F66EB37691B9F4783814D,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_mBBC1E7AEB8F9B7AC04D165ECAE9D183F19F8296A,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_m0DF534784D92B51FE386064197554F9491459867,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m87F47E29A82391F9EABDC2960423809A63D3DA64,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_m948556C997EAEE00AC4050FA6DA476FF8AF2D65A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m436DE502A96515949F5C5027B1F2810165801B8A,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_mD10B0B2BBC87D7BFDC59F237563C3515A19F612F,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_m5D325EA5E1D268FA593EB324238DB117FA6CBC34,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m057AEFA5ACBFD6128FBAD7D76EFE7830B59B55BE,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m981A7B811CFD4CAD91222704760D1EF0F3423FA7,
	U3Cget_ChildrenU3Ed__22__ctor_m06E8E8B2FC52FE3018AE3269AAC642BCFA34C707,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_m80DF5B4FDDA2D5154F34AFDF6AA28B727EF7D8B2,
	U3Cget_ChildrenU3Ed__22_MoveNext_m57D914F733B5D614D5CAFCDD73DDFA157A03EAA2,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m85D02220871BC7AD5F7FB9F627A25273631A999D,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m5D1768596B7404BC7B060F244F5D88C3528E52BB,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mF86B3D72C5015C52CB57E5FF8C90CD58BC75192D,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m527FCAE88CA8E2F6E28FBDC52B4C4884832DDD29,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mA802939A320EA3BD49E49507CF21D4056A78518A,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m4A7EE4525D2D51F084E7489AA33ABEC1DCEA6479,
	U3CU3Ec__DisplayClass21_0__ctor_mF8A6932A8B490316F55E33EC122C544BB332B12A,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m78AEA81A69084D7B4B4D6A1DE8558D50B065B7DB,
	U3Cget_ChildrenU3Ed__23__ctor_m41067DDF3D08F0BE8CED3D9E7872A7FBA8316CDC,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_mE9BEDEC6724C22575197031050734765574F3973,
	U3Cget_ChildrenU3Ed__23_MoveNext_m92ED45D5D77C97089D74BC1B35568C8ED33176DC,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m4A46673EE413AF8FAD22D64152BEBE5EE76A5E65,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mD50C7203AF0D12B0EDFB7F1AABB9B5BBBD327243,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mF4C27D1B6113C761A688634D1A4C9C5058280CEC,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m2EE583F6F365FBAA04E82B8963501E8A3DA58C70,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m948470928F9C800DBE8DB201F697EE2F36254AA3,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m5D486E26CEBA6C8C62417BFB778CAD59F2306BCE,
	PossibleField_op_Implicit_m351720E44CB12EE83E8F412C211DEC2AA1A4E61A,
	PossibleField__ctor_mA72EFFC6FC846DB35B0B42CF581C3729CF17F8D4,
	Data__ctor_mA25821B401A728B1F6AA096CBC7D50C1E222BB60,
	Schema__ctor_mFD9CF215B3762A0C5D6AE57BAA16E5146E51A100,
	Directive__ctor_m59CB9075CAD99EBE2D6DEBEA4D902354ED498676,
	Type__ctor_m7FE19E0C082D0212C0DA12E0C0A9EEEAEC1724DD,
	InputValue__ctor_mE3BD7085A3FC5B9CCEC869895C1B54BC718B4F84,
	Field__ctor_mD8144694650ADC59C3FC35FE014F062D457FC34D,
	EnumValue__ctor_mD6B03FA9687B243A4E7D2830A057A263E0D931A6,
};
static const int32_t s_InvokerIndices[577] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	114,
	1697,
	2417,
	2934,
	23,
	23,
	731,
	336,
	10,
	32,
	2934,
	23,
	2934,
	23,
	731,
	336,
	2934,
	23,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	27,
	14,
	27,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	2935,
	23,
	2935,
	23,
	197,
	28,
	27,
	27,
	27,
	27,
	27,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	2936,
	2936,
	2936,
	2936,
	23,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	154,
	3,
	23,
	23,
	23,
	23,
	23,
	622,
	3,
	3,
	164,
	94,
	23,
	23,
	1598,
	23,
	23,
	27,
	28,
	112,
	26,
	23,
	23,
	23,
	23,
	2,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	26,
	26,
	23,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	58,
	28,
	58,
	205,
	142,
	27,
	0,
	23,
	197,
	23,
	2,
	1,
	1,
	516,
	359,
	0,
	2,
	154,
	1,
	0,
	23,
	3,
	10,
	34,
	62,
	28,
	27,
	14,
	26,
	10,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	31,
	27,
	26,
	28,
	34,
	28,
	14,
	14,
	14,
	34,
	192,
	2937,
	14,
	2938,
	2939,
	465,
	337,
	10,
	32,
	731,
	336,
	89,
	31,
	14,
	14,
	0,
	0,
	98,
	274,
	97,
	266,
	43,
	94,
	208,
	114,
	2940,
	135,
	135,
	9,
	10,
	4,
	0,
	833,
	0,
	23,
	3,
	89,
	31,
	10,
	89,
	2937,
	34,
	62,
	28,
	27,
	10,
	27,
	34,
	28,
	14,
	192,
	23,
	89,
	31,
	10,
	89,
	2937,
	28,
	27,
	34,
	62,
	10,
	27,
	28,
	34,
	28,
	14,
	192,
	23,
	10,
	89,
	2937,
	14,
	26,
	26,
	192,
	9,
	10,
	10,
	89,
	2937,
	14,
	26,
	465,
	337,
	337,
	26,
	192,
	114,
	9,
	10,
	10,
	89,
	2937,
	14,
	26,
	89,
	31,
	31,
	26,
	192,
	9,
	10,
	4,
	23,
	10,
	89,
	2937,
	14,
	26,
	89,
	31,
	9,
	10,
	192,
	3,
	10,
	2937,
	26,
	27,
	26,
	34,
	62,
	28,
	27,
	26,
	27,
	135,
	135,
	9,
	10,
	10,
	32,
	731,
	336,
	465,
	337,
	89,
	31,
	14,
	14,
	192,
	0,
	26,
	14,
	4,
	154,
	731,
	336,
	731,
	336,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	2946,
	26,
	23,
	2947,
	2948,
	2437,
	14,
	26,
	23,
	2949,
	14,
	2934,
	23,
	94,
	-1,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	3,
	23,
	9,
	23,
	9,
	23,
	26,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	23,
	34,
	23,
	10,
	32,
	23,
	26,
	0,
	9,
	23,
	9,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	89,
	2941,
	2942,
	2943,
	89,
	2941,
	2942,
	2944,
	14,
	89,
	2939,
	2941,
	2942,
	2944,
	14,
	89,
	2938,
	26,
	2943,
	14,
	89,
	23,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	2945,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	0,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[11] = 
{
	{ 0x02000002, { 0, 11 } },
	{ 0x02000003, { 11, 6 } },
	{ 0x02000004, { 17, 6 } },
	{ 0x02000005, { 23, 6 } },
	{ 0x02000006, { 29, 6 } },
	{ 0x02000007, { 35, 6 } },
	{ 0x02000008, { 41, 11 } },
	{ 0x02000009, { 52, 6 } },
	{ 0x0200000A, { 58, 16 } },
	{ 0x0200002A, { 75, 7 } },
	{ 0x06000045, { 74, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[82] = 
{
	{ (Il2CppRGCTXDataType)2, 34734 },
	{ (Il2CppRGCTXDataType)3, 24004 },
	{ (Il2CppRGCTXDataType)2, 34735 },
	{ (Il2CppRGCTXDataType)3, 24005 },
	{ (Il2CppRGCTXDataType)3, 24006 },
	{ (Il2CppRGCTXDataType)2, 34736 },
	{ (Il2CppRGCTXDataType)3, 24007 },
	{ (Il2CppRGCTXDataType)3, 24008 },
	{ (Il2CppRGCTXDataType)3, 24009 },
	{ (Il2CppRGCTXDataType)2, 33620 },
	{ (Il2CppRGCTXDataType)2, 33621 },
	{ (Il2CppRGCTXDataType)2, 34737 },
	{ (Il2CppRGCTXDataType)3, 24010 },
	{ (Il2CppRGCTXDataType)2, 34738 },
	{ (Il2CppRGCTXDataType)3, 24011 },
	{ (Il2CppRGCTXDataType)3, 24012 },
	{ (Il2CppRGCTXDataType)2, 33626 },
	{ (Il2CppRGCTXDataType)2, 34739 },
	{ (Il2CppRGCTXDataType)3, 24013 },
	{ (Il2CppRGCTXDataType)2, 34740 },
	{ (Il2CppRGCTXDataType)3, 24014 },
	{ (Il2CppRGCTXDataType)3, 24015 },
	{ (Il2CppRGCTXDataType)2, 33630 },
	{ (Il2CppRGCTXDataType)2, 34741 },
	{ (Il2CppRGCTXDataType)3, 24016 },
	{ (Il2CppRGCTXDataType)2, 34742 },
	{ (Il2CppRGCTXDataType)3, 24017 },
	{ (Il2CppRGCTXDataType)3, 24018 },
	{ (Il2CppRGCTXDataType)2, 33634 },
	{ (Il2CppRGCTXDataType)2, 34743 },
	{ (Il2CppRGCTXDataType)3, 24019 },
	{ (Il2CppRGCTXDataType)2, 34744 },
	{ (Il2CppRGCTXDataType)3, 24020 },
	{ (Il2CppRGCTXDataType)3, 24021 },
	{ (Il2CppRGCTXDataType)2, 33638 },
	{ (Il2CppRGCTXDataType)2, 34745 },
	{ (Il2CppRGCTXDataType)3, 24022 },
	{ (Il2CppRGCTXDataType)2, 34746 },
	{ (Il2CppRGCTXDataType)3, 24023 },
	{ (Il2CppRGCTXDataType)3, 24024 },
	{ (Il2CppRGCTXDataType)2, 33642 },
	{ (Il2CppRGCTXDataType)2, 34747 },
	{ (Il2CppRGCTXDataType)3, 24025 },
	{ (Il2CppRGCTXDataType)2, 34748 },
	{ (Il2CppRGCTXDataType)3, 24026 },
	{ (Il2CppRGCTXDataType)3, 24027 },
	{ (Il2CppRGCTXDataType)2, 34749 },
	{ (Il2CppRGCTXDataType)3, 24028 },
	{ (Il2CppRGCTXDataType)3, 24029 },
	{ (Il2CppRGCTXDataType)3, 24030 },
	{ (Il2CppRGCTXDataType)2, 33646 },
	{ (Il2CppRGCTXDataType)2, 33647 },
	{ (Il2CppRGCTXDataType)2, 34750 },
	{ (Il2CppRGCTXDataType)3, 24031 },
	{ (Il2CppRGCTXDataType)2, 34751 },
	{ (Il2CppRGCTXDataType)3, 24032 },
	{ (Il2CppRGCTXDataType)3, 24033 },
	{ (Il2CppRGCTXDataType)2, 33652 },
	{ (Il2CppRGCTXDataType)2, 34752 },
	{ (Il2CppRGCTXDataType)3, 24034 },
	{ (Il2CppRGCTXDataType)2, 34753 },
	{ (Il2CppRGCTXDataType)3, 24035 },
	{ (Il2CppRGCTXDataType)3, 24036 },
	{ (Il2CppRGCTXDataType)2, 34754 },
	{ (Il2CppRGCTXDataType)3, 24037 },
	{ (Il2CppRGCTXDataType)3, 24038 },
	{ (Il2CppRGCTXDataType)2, 34755 },
	{ (Il2CppRGCTXDataType)3, 24039 },
	{ (Il2CppRGCTXDataType)3, 24040 },
	{ (Il2CppRGCTXDataType)3, 24041 },
	{ (Il2CppRGCTXDataType)3, 24042 },
	{ (Il2CppRGCTXDataType)2, 33656 },
	{ (Il2CppRGCTXDataType)2, 33657 },
	{ (Il2CppRGCTXDataType)2, 33658 },
	{ (Il2CppRGCTXDataType)3, 24043 },
	{ (Il2CppRGCTXDataType)2, 33796 },
	{ (Il2CppRGCTXDataType)2, 33789 },
	{ (Il2CppRGCTXDataType)3, 24044 },
	{ (Il2CppRGCTXDataType)3, 24045 },
	{ (Il2CppRGCTXDataType)3, 24046 },
	{ (Il2CppRGCTXDataType)2, 33790 },
	{ (Il2CppRGCTXDataType)3, 24047 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	577,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	11,
	s_rgctxIndices,
	82,
	s_rgctxValues,
	NULL,
};
